<?php

namespace App;

class AISActivate extends GeneralModel
{
    protected $table = 'ais_activate';
    protected $fillable = ['phone','status','ref_key'];
    public $timestamps = true;
}
