<?php

namespace App;

class AdminProperty extends GeneralModel
{
    protected $table = 'admin_property';
    protected $fillable = ['user_id','property_id'];
    public $timestamps = true;
}
