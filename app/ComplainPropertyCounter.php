<?php

namespace App;
use App\GeneralModel;
class ComplainPropertyCounter extends GeneralModel
{
    protected $table = 'complain_property_counter';
    public $timestamps = true;
    protected $fillable = ['property_id','year_period','complain_counter'];
}
