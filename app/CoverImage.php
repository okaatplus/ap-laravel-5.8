<?php

namespace App;


class CoverImage extends GeneralModel
{
    protected $table = 'cover_image';
    protected $fillable = ['name','url','cover_type'];
    public $timestamps = true;
}
