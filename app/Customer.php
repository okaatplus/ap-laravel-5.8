<?php

namespace App;

class Customer extends GeneralModel
{
    protected $table = 'customer';
    protected $fillable = [
                'prefix_name',
                'name',
                'id_card',
                'type_id_card',
                'phone',
                'email',
                'delivery_address',
                'person_type',
                'customer_type',
                'person_type',
                'nationality',
                'job',
                'dob',
                'customer_code'
            ];
    public $timestamps = true;

    public function customerProperty () {
        return $this->hasMany('App\CustomerProperty','customer_id','id');
    }

    public function customerRef () {
        return $this->hasOne('App\customerRef','id_card','id_card');
    }
}
