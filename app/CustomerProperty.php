<?php

namespace App;


class CustomerProperty extends GeneralModel
{
    protected $table = 'customer_property';
    public $timestamps = true;

    public function customer () {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function unit () {
        return $this->belongsTo('App\PropertyUnit', 'property_unit_id');
    }

    public function property () {
        return $this->belongsTo('App\Property', 'property_id');
    }
}
