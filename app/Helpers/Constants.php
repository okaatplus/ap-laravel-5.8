<?php
define('VEHICLE_TYPE_TH', serialize(array('ประเภท','รถยนต์','รถจักรยานยนต์','รถจักรยาน','อื่นๆ')));
define('VEHICLE_TYPE_EN', serialize(array('Type','Truck','Motorcycle','Bicycle','Other')));

define('PROPERTY_TYPE_EN',serialize(array('Property type','Housing estate/Village','Townhome','Condo','Semi Detached House','Home Office')));
define('PROPERTY_TYPE_TH',serialize(array('ประเภทโครงการ','บ้านเดี่ยว','ทาวน์โฮม','คอนโด','บ้านแฝด','โฮมออฟฟิศ')));

define('BANK_LIST_TH',serialize(array(
        ''=>'เลือกธนาคาร',
        "KrungSri"=>"ธนาคารกรุงศรีอยุธยา",
        "BBL"=>"ธนาคารกรุงเทพ",
        "KTB"=>"ธนาคารกรุงไทย",
        "KBank"=>"ธนาคารกสิกร",
        "CIMBT"=>"ธนาคารซีไอเอ็มบีไทย",
        "TMB"=>"ธนาคารทหารไทย",
        "TISCO"=>"ธนาคารทิสโก้",
        "THBK"=>"ธนาคารธนชาติ",
        "UOB"=>"ธนาคารยูโอบี",
        "SCBL"=>"ธนาคารสแตนดาร์ดชาร์เตอร์ด",
        "GSB"=>"ธนาคารออมสิน",
        "GHB"=>"ธนาคารอาคารสงเคราะห์",
        "TIBT"=>"ธนาคารอิสลาม",
        "KKPB"=>"ธนาคารเกียรตินาคิน",
        "BAAC"=>"ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร",
        "LHBANK"=>"ธนาคารแลนด์ แอนด์ เฮ้าท์",
        "SCB"=>"ธนาคารไทยพาณิชย์",
        "ICBC"=>"ธนาคารไอซีบีซี")
));

define('BANK_LIST_EN',serialize(array(
        ''=>'Select Bank',
        "KrungSri"=>"Krung Sri Bank",
        "BBL"=>"Bangkok Bank",
        "KTB"=>"Krung Thai Bank",
        "KBank"=>"Kasikorn Bank",
        "CIMBT"=>"CIMB Thai Bank",
        "TMB"=>"TMB Bank",
        "TISCO"=>"Tisco Bank",
        "THBK"=>"Thanachart Bank",
        "UOB"=>"UOB Bank",
        "SCBL"=>"Standard Chartered Bank",
        "GSB"=>"Government Savings Bank",
        "GHB"=>"Government Housing Bank",
        "TIBT"=>"Islamic Bank",
        "KKPB"=>"Kiatnakin Bank",
        "BAAC"=>"Bank for Agriculture and Agricultural Cooperatives",
        "LHBANK"=>"Land And Houses Retail Bank",
        "SCB"=>"Siam Commercial Bank",
        "ICBC"=>"ICBC Bank")
));
define('ACCOUNT_TYPE_EN',serialize(array('Account Type','Saving Account','Current account','Fixed deposit account')));
define('ACCOUNT_TYPE_TH',serialize(array('ประเภทบัญชี','ออมทรัพย์','กระแสรายวัน','บัญชีเงินฝากประจำ')));

define('COMMON_AREA_FEE_TYPE_TH',serialize(array('ไม่จัดเก็บ','อัตราคงที่','ตามขนาดพื้นที่อาศัย','อัตราส่วนกรรมสิทธิ์')));
define('COMMON_AREA_FEE_TYPE_EN',serialize(array('No rate','Static Rate','Area size','Ownership Ratio')));

define('POST_PARCEL_TYPE_TH', serialize(array('ประเภทเอกสาร','จดหมายลงทะเบียน','พัสดุ','EMS')));
define('POST_PARCEL_TYPE_EN', serialize(array('Post or pacel type','Mail','Parcel','EMS')));

define('POST_PARCEL_SIZE_TH', serialize(array('h'=>'ใหญ่', 'm'=>'กลาง', 's'=>'เล็ก', 'e'=>'ซอง', 'd'=>'เอกสาร')));
define('POST_PARCEL_SIZE_EN', serialize(array('Huge','Medium','Small','Envelope','Document')));

define('KEY_CARD_STATUS_TH', serialize(array("0"=>'ปกติ', 1 =>'ระงับใช้งาน')));
define('KEY_CARD_STATUS_EN', serialize(array("0"=>'Active', 1 =>'InActive')));

define('ZONE_EN', serialize(array(
    "1"=>'Pool',
    "2" =>'Fitness',
    "3" =>'Car Park',
    "4" =>'Park',
    "5" =>'Elevator',
    "99" =>'-',
)));

define('ZONE_TH', serialize(array(
    "1"=>'สระว่ายน้ำ',
    "2" =>'ฟิตเนส',
    "3" =>'ลานจอดรถ',
    "4" =>'สวน',
    "5" =>'ลิฟท์',
    "99" =>'-'
)));



