<?php namespace App\Http\Controllers\API;
use Auth;
use Illuminate\Http\Request;
//use Storage;
//use File;
//use League\Flysystem\AwsS3v2\AwsS3Adapter;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PushNotificationController;
# Model
use App\User;
use App\Notification;
use App\Message;
use App\MessageText;
use App\MessageTextFile;
use App\Property;
use App\AISActivate;

class AISPlayCallbackController extends Controller {

	public function __construct () {
        //$this->middleware('jwt.feature_menu:menu_message');
	}

	public function aisActivate(){
        $phone = Request::get('phone');
        $phone_add_country_code = "66".substr($phone, 1);

	    $ais_activate_count = AISActivate::where('phone',$phone_add_country_code)->where('status',"!=","ERROR")->orderBy('updated_at','desc')->count();
	    if($ais_activate_count > 0){
            $is_activate = true;
            $message = "phone activated";
        }else{
            $is_activate = false;
            $message = "phone not activated";
        }

        $results = [
            'status' => true,
            'message' => $message,
            'data' => [
                'is_activate' => $is_activate
            ]
        ];

        return response()->json($results);
    }

	public function callbackFunction(Request $request)  {
        try {
            $api_key = $request->key;
            $phone = $request->phone;
            $status = $request->status;
            $ref_key = $request->ref_key;

            if($api_key == env('AIS_API_KEY',"")){
                $is_valid_key = true;
            }else{
                $is_valid_key = false;
            }

            if($is_valid_key) {
                $ais_activate = new AISActivate();
                $ais_activate->phone = $phone;
                $ais_activate->status = $status;
                $ais_activate->ref_key = $ref_key;
                $ais_activate->save();

                $results = [
                    'status' => true,
                    'message' => 'success',
                ];
            }else{
                $results = [
                    'status' => false,
                    'message' => 'key invalid',
                ];
            }

            return response()->json($results);
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
            ];

            return response()->json($results);
        }
	}

	public function aisEncode(Request $request){
	    $phone = $request->phone;
	    $phone_add_country_code = "66".substr($phone, 1);
	    $salt = env('AIS_SALT_KEY');
	    $string_phone_salt = (int)$phone_add_country_code + $salt;
	    $encode = urlEncode(base64_encode($string_phone_salt));

	    $result = [
            'status' => true,
            'message' => "success",
            'data' =>[
                'phone'=> $phone,
                'phone_add_country_code'=> $phone_add_country_code,
                'encode_string' => $encode,
                'activate_package_url' => env('AIS_PACKAGE_APPLY_URL').$encode,
                'url_streaming' => env('AIS_STREAMING_URL')
            ]
        ];

	    return $result;
    }

    public function aisApplyEncode(){
        $phone = Request::get('phone');
        $phone_add_country_code = "66".substr($phone, 1);
        $salt = env('AIS_SALT_KEY');
        $string_phone_salt = (int)$phone_add_country_code + $salt;
        $encode = urlEncode(base64_encode($string_phone_salt));

        $result = [
            'status' => true,
            'message' => "success",
            'data' =>[
                'phone'=> $phone,
                'phone_add_country_code'=> $phone_add_country_code,
                'encode_string' => $encode,
                'activate_package_url' => env('AIS_PACKAGE_APPLY_URL').$encode,
                'url_streaming' => env('AIS_STREAMING_URL')
            ]
        ];

        return $result;
    }

    public function checkActivate(){
        $phone = Request::get('phone');
        $phone_add_country_code = "66".substr($phone, 1);


    }
}
