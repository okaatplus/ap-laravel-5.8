<?php namespace App\Http\Controllers\API;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Pagination\Paginator;
use Storage;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use App\Http\Controllers\PushNotificationController;
use Carbon\Carbon;
use DateTime;
use Pusher\Pusher;
# Model
use App\Complain;
use App\ComplainCategory;
use App\ComplainFile;
use App\ComplainComment;
use App\Notification;
use App\User;
use App\Property;
use App\ComplainPropertyCounter;
use Auth;
use File;
use GuzzleHttp\Client as GuzzleClient;

class ComplainController extends Controller {

    public function __construct () {

    }

    public function complain () {
        $complains = Complain::with('owner','category','comments','comments.owner')->where('user_id','=',Auth::user()->id)->get()->sortByDesc('created_at');
        $count_new = Complain::where('user_id','=',Auth::user()->id)->where('complain_status','=',0)->count();
        $c_cate = ComplainCategory::all();

        $results = [
            'complain_new_count' => $count_new,
            'complain' => array_values($complains->toArray()),
            'complain_category' => $c_cate
        ];

        return response()->json($results);
    }

    public function complainListAll () {
        $property_id = Request::get('property_id');
        $property_unit_id = Request::get('property_unit_id');

        $complains = Complain::with('owner','category')->where('property_id','=',$property_id)->where('property_unit_id','=',$property_unit_id)->get()->sortByDesc('created_at');
        $count_new = Complain::where('property_id','=',$property_id)->where('property_unit_id','=',$property_unit_id)->where('complain_status','=',0)->count();

        //$complains = Complain::with('owner','category','comments','comments.owner')->where('user_id','=',Auth::user()->id)->get()->sortByDesc('created_at');
        //$count_new = Complain::where('user_id','=',Auth::user()->id)->where('complain_status','=',0)->count();
        $c_cate = ComplainCategory::all();

        $zone_en = unserialize(constant('ZONE_EN'));
        $zone_th = unserialize(constant('ZONE_TH'));

        $complain_zone = [];
        foreach ($zone_en as $i => $item){
            $complain_zone[] = [
                'id' => $i,
                'zone_th' => $zone_th[$i],
                'zone_en' => $zone_en[$i]
            ];
        }

        $data = [
            'complain_new_count' => $count_new,
            'complain' => array_values($complains->toArray()),
            'complain_category' => $c_cate,
            'zone_category' => $complain_zone
        ];

        $results = [
            "status" => true,
            "message"=>"success",
            "data" => $data
        ];

        return response()->json($results);
    }

    public function add () {
        if(Request::isMethod('post')) {
            $complain = new Complain;
            $complain->fill(Request::all());
            $complain->user_id 			= Auth::user()->id;
            $complain->property_id 		= Request::get('property_id');
            $complain->property_unit_id = Request::get('property_unit_id');
            //$is_juristic_complain = Request::get('is_juristic_complain') == "true" ? true : false;
            $complain->is_juristic_complain = true;
            $complain->complain_status 	= 0;
            $complain->title = Request::get('detail');
            $complain->zone = Request::get('zone');

            $complain_counter_label = $this->counterComplain(Request::get('property_id'));

            $complain->complain_no_label = $complain_counter_label;
            $complain->save();

            $this->addCreateComplainNotification($complain);

            $results_all = array(
                "status" => true,
                "message"=>"success",
                "data" => ['cid' =>$complain->id]
            );

            return response()->json($results_all);
        }
    }

    public function addFileComplain () {
        try {
            // Get Post
            $complain = Complain::find(Request::get('cid'));
            $complain->save();

            $attach = [];

            /* New Function */
            if(count(Request::file('attachment'))) {
                foreach (Request::file('attachment') as $key => $file) {
                    $name =  md5($file->getFilename());//getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $targetName = $name.".".$extension;

                    $path = $this->createLoadBalanceDir($file);

                    $isImage = 0;
                    if(in_array($extension, ['jpeg','jpg','gif','png'])) {
                        $isImage = 1;
                    }

                    $attach[] = new ComplainFile([
                        'name' => $targetName,
                        'url' => $path,
                        'file_type' => $file->getClientMimeType(),
                        'is_image'	=> $isImage,
                        'original_name'	=> $file->getClientOriginalName()
                    ]);
                }
                $complain->attachment_count = ++$key;
                $complain->save();
                $complain->complainFile()->saveMany($attach);
            }

            return response()->json(['success' =>'true']);

        } catch(Exception $ex){
            return response()->json(['success' =>'false']);
        }
    }

    public function detail($id) {
        try {
            $complain = Complain::with('owner', 'category', 'complainFile', 'comments', 'comments.owner')->find($id);
            $c_cate = ComplainCategory::all();
            $results = $complain->toArray();
            //$property_admin = User::where('property_id',Auth::user()->property_id)->where('role',3)->first();

            // Change File type in Array
            foreach ($results['complain_file'] as &$value) {
                $splitType = explode(".", $value['name']);
                $value['file_type'] = end($splitType);
                $value['image_full_path'] = env('URL_S3') . '/complain-file/' . $value['url'] . $value['name'];
                /*if(isset($property_admin)) {
                    $value['property_admin'] = $property_admin->toArray();
                }else{
                    $value['property_admin'] = "";
                }*/
            }

            /*$dt = Carbon::parse($results['created_at']);
            $results['created_at'] = $dt->format('d/m/Y H:i:s');*/
            $zone_en = unserialize(constant('ZONE_EN'));
            $zone_th = unserialize(constant('ZONE_TH'));

            $complain_zone = [];
            foreach ($zone_en as $i => $item){
                $complain_zone[] = [
                    'id' => $i,
                    'zone_th' => $zone_th[$i],
                    'zone_en' => $zone_en[$i]
                ];
            }

            $results['zone_category'] = $complain_zone;
            $results['complain_category'] = $c_cate;


            $results_all = array(
                "status" => true,
                "message"=>"success",
                "data" => $results
            );
            return response()->json($results_all);
        }
        catch(Exception $ex){
            return response()->json(['success' =>false,'data'=>[]]);
        }

    }

    public function chiefChangeStatus () {
        try {
            $cid = Request::get('cid');
            $complain = Complain::find($cid);
            if($complain->count() && (Auth::user()->role == 1 || Auth::user()->role == 3 || Auth::user()->is_chief )) {
                $complain->complain_status = Request::get('status');
                $complain->save();
                //Add Notification
                $this->addChangeStatusComplainNotification ($complain);
            }
            return response()->json(['success' =>'true']);
        } catch(Exception $ex){
            return response()->json(['success' =>'false']);
        }
    }

    public function userConfirm(){
        try{
            $cid = Request::get('cid');
            $complain = Complain::find($cid);
            //if($complain->user_id == Auth::user()->id) {
            if(Request::get('status') == "true") {
                // User confirm
                $complain->complain_status = 3;
                $complain->review_rate = Request::get('review_rate');
                $complain->review_comment = Request::get('review_comment');
                $complain->save();
                //Add Notification
                $this->addUserChangeStatusComplainNotification($complain);
            }else{
                // User reject
                $complain->complain_status = 0;
                $complain->save();

                $comment = new ComplainComment([
                    'description' 	=> Request::get('comment'),
                    'user_id'		=> Auth::user()->id,
                    'is_reject'     => true
                ]);

                $complain->comments()->save($comment);
                //Add Notification
                $this->addUserChangeStatusComplainNotification($complain);
            }

            return response()->json(['success' =>'true']);
            /*}else{
                return response()->json(['success' =>'false']);
            }*/
        } catch(Exception $ex){
            return response()->json(['success' =>'false']);
        }
    }

    public function addComment () {
        if (Request::isMethod('post')) {
            $comment = new ComplainComment([
                'description' 	=> Request::get('comment'),
                'user_id'		=> Auth::user()->id
            ]);
            $complain = Complain::with('owner')->find(Request::get('cid'));
            if($complain) {
                $complain->comments()->save($comment);
                //Add Notification
                $this->addComplainCommentNotification ($complain);

                $status = true;
            } else {
                $status = false;
            }
            return response()->json(['success' =>$status]);
        }
    }

    public function getAttach ($id) {
        $file = ComplainFile::find($id);
        $folder = str_replace('/', DIRECTORY_SEPARATOR, $file->url);
        $file_path = 'complain-file'.DIRECTORY_SEPARATOR.$folder.$file->name;
        $exists = Storage::disk('s3')->has($file_path);
        if ($exists) {
            $response = response(Storage::disk('s3')->get($file_path), 200, [
                'Content-Type' => $file->file_type,
                'Content-Length' => Storage::disk('s3')->size($file_path),
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename={$file->original_name}",
                'Content-Transfer-Encoding' => 'binary',
            ]);

            ob_end_clean();

            return $response;
        }
    }

    public function createLoadBalanceDir ($imageFile) {
        $name =  md5($imageFile->getFilename());//getClientOriginalName();
        $extension = $imageFile->getClientOriginalExtension();
        $targetName = $name.".".$extension;

        $folder = substr($name, 0,2);

        $pic_folder = 'complain-file'.DIRECTORY_SEPARATOR.$folder;
        $directories = Storage::disk('s3')->directories('complain-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }

        $full_path_upload = $pic_folder.DIRECTORY_SEPARATOR.$targetName;
        $upload = Storage::disk('s3')->put($full_path_upload, file_get_contents($imageFile), 'public');// public set in photo upload
        if($upload){
            // Success
        }

        return $folder."/";
    }

    public function addComplainCommentNotification($complain) {
        $users = $this->getChief($complain->property_id);

        if($users->count()) {
            foreach ($users as $key=>$user) {
                $title = json_encode(['type' => 'comment', 'c_title' => $complain->title]);
                $notification = Notification::create([
                    'title' => $title,
                    'description' => "",
                    'notification_type' => '5',
                    'subject_key' => $complain->id,
                    'to_user_id' => $user->id,
                    'from_user_id' => Auth::user()->id
                ]);
                $controller_push_noti = new PushNotificationController();
                $controller_push_noti->pushNotification($notification->id);

                $textNoti = $this->convertTitleTolongTxt($notification);

                $dataPusher = [
                    'title' => $textNoti,
                    'notification' => $notification
                ];

                Pusher::trigger(Auth::user()->property_id . "_" . $user->id, 'notification_event', $dataPusher);
            }
        }
    }

    public function addCreateComplainNotification($complain) {
        // Get Admin and Committee
        /*$users = User::where('property_id',Auth::user()->property_id)
            ->where(function ($q) {
                $q ->orWhere('role',1)
                    ->orWhere('is_chief',true);
            })->get();*/

        // Get Admin
        $users = $this->getChief($complain->property_id);


        if($users->count()) {
            $title = json_encode( ['type'=>'complain_created','c_title'=>$complain->title] );
            foreach ($users as $key=>$user) {
                $notification = Notification::create([
                    'title'				=> $title,
                    'description' 		=> "",
                    'notification_type' => '5',
                    'subject_key'		=> $complain->id,
                    'to_user_id'		=> $user->id,
                    'from_user_id'		=> Auth::user()->id,
                    'property_id'       => $complain->property_id,
                    'property_unit_id'       => $complain->property_unit_id,
                ]);
                $controller_push_noti = new PushNotificationController();
                $controller_push_noti->pushNotification($notification->id);

                $textNoti = $this->convertTitleTolongTxt($notification);
            }
            $dataPusher = [
                'title' => $textNoti,
                'notification' => $notification
            ];

            $app_id = env('PUSHER_APP_ID');
            $app_key = env('PUSHER_KEY');
            $app_secret = env('PUSHER_SECRET');
            $app_cluster = env('PUSHER_APP_CLUSTER');

            $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

            $pusher->trigger($complain->property_id, 'notification_event', $dataPusher);

        }
    }

    public function addUserChangeStatusComplainNotification($complain) {
        // Get Just Admin
        $users = $this->getChief($complain->property_id);

        if($users->count()) {
            $status = ['status_rj','status_ip','status_ck','status_cf','status_cls'];
            $title = json_encode( ['type'=>'change_status','c_title'=>$complain->title,'status' => $status[$complain->complain_status]] );
            foreach ($users as $key=>$user) {
                $notification = Notification::create([
                    'title'				=> $title,
                    'description' 		=> "",
                    'notification_type' => '5',
                    'subject_key'		=> $complain->id,
                    'to_user_id'		=> $user->id,
                    'from_user_id'		=> Auth::user()->id
                ]);
                $controller_push_noti = new PushNotificationController();
                $controller_push_noti->pushNotification($notification->id);

                $textNoti = $this->convertTitleTolongTxt($notification);

                $dataPusher = [
                    'title' => $textNoti,
                    'notification' => $notification
                ];
            }
            //Pusher::trigger(Auth::user()->property_id."_".$user->id, 'notification_event', $dataPusher);
            $dataPusher = [
                'title' => $textNoti,
                'notification' => $notification
            ];

            $app_id = env('PUSHER_APP_ID');
            $app_key = env('PUSHER_KEY');
            $app_secret = env('PUSHER_SECRET');
            $app_cluster = env('PUSHER_APP_CLUSTER');

            $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

            $pusher->trigger($complain->property_id, 'notification_event', $dataPusher);

        }
    }

    public function addChangeStatusComplainNotification($complain) {
        $status = ['status_rj','status_ip','status_ck','status_cf','status_cls'];
        $title = json_encode( ['type'=>'change_status','c_title'=>$complain->title,'status' => $status[$complain->complain_status]] );
        $notification = Notification::create([
            'title'				=> $title,
            'description' 		=> "",
            'notification_type' => 5,
            'subject_key'		=> $complain->id,
            'to_user_id'		=> $complain->user_id,
            'from_user_id'		=> Auth::user()->id
        ]);
        $controller_push_noti = new PushNotificationController();
        $controller_push_noti->pushNotification($notification->id);
    }

    public function getChief ($property_id) {
        /*return User::where('property_id',Auth::user()->property_id)
                ->where(function ($q) {
                     $q ->where('role',1)
                         ->orWhere('is_chief',true);
                })->get();*/
        return User::where('property_id',$property_id)->whereIn('role', array(3))->get();
    }

    function convertTitleTolongTxt($notification){
        $data = json_decode($notification->title, true);
        $message_string = "";
        if ($data['type'] == 'comment') {
            $message_string = $notification->sender->name." ".trans('messages.Notification.complain_comment',$data);
        }elseif($data['type'] == 'change_status'){
            $data['status'] = trans('messages.Complain.'.$data['status']);
            $message_string = trans('messages.Notification.complain_change_status',$data);
        } elseif($data['type'] == 'complain_created') {
            $message_string = $notification->sender->name." ".trans('messages.Notification.complain_created_');
        }

        return $message_string;
    }

    public function getJobHistory(){
        // complain on juristic system
        $complains = Complain::with('owner','category')->where('property_id','=',Auth::user()->property_id)->where('property_unit_id','=',Auth::user()->property_unit_id)->get()->sortByDesc('created_at');

        $complain_array = [];
        foreach ($complains as $item_complains){
            $complain_array[] = ([
                "complain_id" =>$item_complains->id,
                "job_id" => $item_complains->complain_no_label,
                "job_status" => $item_complains->complain_status,
                "job_status_title_th" => $item_complains->title,
                "job_status_title_en" => $item_complains->title,
                "job_booking_startdate" => $item_complains->appointment_date,
                "job_booking_enddate" => $item_complains->appointment_date,
                "job_createdate" => $item_complains->created_at->format('Y-m-d H:i:s'),
                "job_asset_id" => null,
                "asset_project_name_th" => null,
                "asset_project_name_en" => null,
                "job_cr" => false,
                "zone" => $item_complains->zone,
                "category" => $item_complains->complain_category_id
            ]);
        }

        $total_job = $complain_array;

        foreach ($total_job as $key => $part) {
            $sort[$key] = strtotime($part['job_createdate']);
        }
        array_multisort($sort, SORT_DESC, $total_job);

        $results = [
            'result' => true,
            'message_th' => 'สำเร็จ',
            'message_en' => 'Success',
            'job_list' => $total_job
        ];

        return response()->json(['status'=>true, 'result'=>$results]);
    }


    public function uploadS3($url){
        $url = Request::get('url_image');

        $headers = [
            //'Content-Type' => 'multipart',
            'Content-Type' => 'application/json',
            'AccessToken' => 'key',
            'Authorization' => 'Bearer '.session()->get('mde_token'),
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

        $r = $client->request('Get', $url);

        $body = $r->getBody();
        $asdad = "";

        /*$pic_folder = 'complain-file'.DIRECTORY_SEPARATOR.$folder;
        $directories = Storage::disk('s3')->directories('complain-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }*/
    }

    function counterComplain($property_id){
        try {
            $property = Property::find($property_id);
            //$property_code = $property->property_code;
            $property_code = "CR-";
            $complain_counter = 0;
            $year_digit = substr(Carbon::now()->year, -2);
            if (isset($property)) {
                $complain_property_counter = ComplainPropertyCounter::where('property_id',$property_id)->where('year_period',$year_digit)->first();
                if(isset($complain_property_counter)) {
                    $complain_counter = $complain_property_counter->complain_counter + 1;

                    $complain_counter_save = ComplainPropertyCounter::find($complain_property_counter->id);
                    $complain_counter_save->complain_counter = $complain_counter;
                    $complain_counter_save->save();
                }else{
                    $complain_counter = 1;

                    $new_complain_counter = new ComplainPropertyCounter;
                    $new_complain_counter->property_id = $property_id;
                    $new_complain_counter->year_period = $year_digit;
                    $new_complain_counter->complain_counter = $complain_counter;
                    $new_complain_counter->save();
                }
            }

            $complain_counter_label = $property_code.$year_digit.str_pad($complain_counter, 4, '0', STR_PAD_LEFT);

            return $complain_counter_label;
        }catch(Exception $ex){
            return 0;
        }
    }

    public function getJuristicManager(){
        try {
            $property_admin = User::where('property_id',Auth::user()->property_id)->where('role',3)->first();

            if($property_admin->profile_pic_name != null){
                $property_admin['profile_full_path'] = env('URL_S3') . '/profile-img/' . $property_admin->profile_pic_path . $property_admin->profile_pic_name;
            }else{
                $property_admin['profile_full_path'] = url('/')."/images/user-2.png";
            }
            return response()->json(['status'=>true, 'result'=>$property_admin]);
        }catch(Exception $ex){
            return response()->json(['status'=>false, 'result'=>[]]);
        }
    }


}
