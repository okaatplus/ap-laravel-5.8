<?php namespace App\Http\Controllers\API;
use GuzzleHttp\Exception\RequestException;
use Request;
use Auth;
use Mail;
use Hash;
use JWTAuth;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;
use DB;
use Carbon\Carbon;

use App\Property;
use App\Post;
use App\PostFile;
use App\PostProperty;
use App\PostTracking;
use App\PostPin;
use App\PostParcel;
use App\UserProperty;
use App\CoverImage;

class CustomerDashboardController extends Controller {

    //use DispatchesJobs;

    public function __construct () {

    }

    public function allPropertyDashboard(){
        $cover_image_data = CoverImage::where('cover_type','u')->first();
        if(isset($cover_image_data)){
            $cover_image = env('URL_S3') . "/cover-file/". $cover_image_data->url . $cover_image_data->name;
        }else {
            $cover_image = url('/') . "/images/cover/cover_image.png";
        }

        $user_property = UserProperty::select('property_unit_id')->where('user_id',Auth::user()->id)->where('approve_status',1)->get();
        $user_property_unit_id_arr = [];
        foreach ($user_property as $user_property_item){
            $user_property_unit_id_arr[] = $user_property_item->property_unit_id;
        }

        $property_list_data = UserProperty::with('property')->with('unit')->where('user_id',Auth::user()->id)->where('approve_status',1)->get();

        $post_parcel = PostParcel::with('forProperty','forUnit')->whereIn('property_unit_id', $user_property_unit_id_arr);
        //$new_post_parcel = $post_parcel->where('status', 0)->orderBy('date_received','desc')->get();
        $post_parcel_data = $post_parcel->orderBy('date_received','asc')->get();
        $new_post_parcel_arr = $post_parcel_data->toArray();
        foreach ($post_parcel_data as &$item) {
            $date_now = Carbon::now();
            $date_received = Carbon::parse($item['date_received']);
            $date_expire = $date_received->addDays(30);
            $day_count = $date_now->diffInDays($date_expire);

            $item['expire_date_count'] = $day_count;
            $item['unit_number'] = $item->forUnit->unit_number;
            $item['property_name_en'] = $item->forProperty->property_name_en;
            $item['property_name_th'] = $item->forProperty->property_name_th;
            $item['brand_name_en'] = $item->forProperty->has_brand->name_en;
            $item['brand_name_th'] = $item->forProperty->has_brand->name_th;

            if($item->type == "1"){
                $item['type_name_th'] = "จดหมายลงทะเบียน";
                $item['type_name_en'] = "Register Parcel";
                //1 = จดหมายลงทะเบียน, 2 = พัสดุ, 3 = EMS
            }

            if($item->type == "2"){
                $item['type_name_th'] = "พัสดุ";
                $item['type_name_en'] = "Parcel";
            }

            if($item->type == "3"){
                $item['type_name_th'] = "EMS";
                $item['type_name_en'] = "EMS";
            }
        }

        foreach ($property_list_data as &$item_property){
            $item_property->property->brand_name_en = $item_property->property->has_brand->name_en;
            $item_property->property->brand_name_th = $item_property->property->has_brand->name_th;
            unset($item_property->property->has_brand);
            $item_property->property->banner_url_full_path = env('URL_S3')."/post-file".$item_property->property->banner_url;
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => [
                'cover_image' => $cover_image,
                'user_property' => $property_list_data,
                'post_parcel' => $post_parcel_data
            ]
        ];

        return response()->json($results);
    }

    public function ownerDashboard(){
        $property_id = Request::get('property_id');
        $property_unit_id = Request::get('property_unit_id');
        $post_parcel_count = PostParcel::where('property_id', $property_id)->where('property_unit_id', $property_unit_id)->where('status', 0)->count();

        $property = Property::find($property_id);
        if($property->banner_url != null){
            $cover_image = env('URL_S3')."/post-file".$property->banner_url;
        }else{
            $cover_image = url('/')."/images/cover/cover_image.png";
        }

        $user_property = UserProperty::where('user_id',Auth::user()->id)
            ->where('property_id',Request::get('property_id'))
            ->where('property_unit_id',Request::get('property_unit_id'))
            ->first();

        if(isset($user_property) && $user_property->user_type == 0){
            // Resident
            $user_property_waiting_count = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                ->where('property_unit_id',Request::get('property_unit_id'))
                ->where('approve_status',0)
                ->count();
        }else{
            $user_property_waiting_count = 0;
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => [
                'post_parcel_count' => $post_parcel_count,
                'new_resident' => $user_property_waiting_count,
                'cover_image' => $cover_image
            ]
        ];

        return response()->json($results);
    }

    public function residentDashboard(){

        $property_id = Request::get('property_id');
        $property_unit_id = Request::get('property_unit_id');
        $post_parcel_count = PostParcel::where('property_id', $property_id)->where('property_unit_id', $property_unit_id)->where('status', 0)->count();

        $user_property = UserProperty::where('user_id',Auth::user()->id)
            ->where('property_id',Request::get('property_id'))
            ->where('property_unit_id',Request::get('property_unit_id'))
            ->first();

        if(isset($user_property) && $user_property->user_type == 0){
            // Resident
            $user_property_waiting_count = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                ->where('property_unit_id',Request::get('property_unit_id'))
                ->where('approve_status',0)
                ->count();
        }else{
            $user_property_waiting_count = 0;
        }

        $user_property = UserProperty::select('property_unit_id')->where('user_id',Auth::user()->id)->get();
        $user_property_unit_id_arr = [];
        foreach ($user_property as $user_property_item){
            $user_property_unit_id_arr[] = $user_property_item->property_unit_id;
        }

        $property_list_data = UserProperty::with('property')->with('unit')->where('user_id',Auth::user()->id)->get();

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => [
                'post_parcel_count' => $post_parcel_count,
                'new_resident' => $user_property_waiting_count,
                'user_property' => $property_list_data
            ]
        ];

        return response()->json($results);
    }

    public function tenantDashboard(){

        $property_id = Request::get('property_id');
        $property_unit_id = Request::get('property_unit_id');
        $post_parcel_count = PostParcel::where('property_id', $property_id)->where('property_unit_id', $property_unit_id)->where('status', 0)->count();

        $user_property = UserProperty::where('user_id',Auth::user()->id)
            ->where('property_id',Request::get('property_id'))
            ->where('property_unit_id',Request::get('property_unit_id'))
            ->first();

        /*if(isset($user_property) && $user_property->user_type == 0){
            // Resident
            $user_property_waiting_count = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                ->where('property_unit_id',Request::get('property_unit_id'))
                ->where('approve_status',0)
                ->count();
        }else{
            $user_property_waiting_count = 0;
        }*/

        $user_property = UserProperty::select('property_unit_id')->where('user_id',Auth::user()->id)->get();
        $user_property_unit_id_arr = [];
        foreach ($user_property as $user_property_item){
            $user_property_unit_id_arr[] = $user_property_item->property_unit_id;
        }

        $property_list_data = UserProperty::with('property')->with('unit')->where('user_id',Auth::user()->id)->get();

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => [
                'post_parcel_count' => $post_parcel_count,
                'user_property' => $property_list_data
            ]
        ];

        return response()->json($results);
    }

    public function featureProperty(){
        $user_property_count = UserProperty::where('user_id',Auth::user()->id)->where('user_type','!=',null)->count();

        if($user_property_count > 0) {
            $user_property_owner = UserProperty::where('user_id',Auth::user()->id)->where('user_type',0)->count();
            if($user_property_owner > 0) {
                $data = array(
                    'menu_dashboard' => true,
                    'menu_news_announcement' => true,
                    'menu_resident' => true,
                    'menu_parcel_tracking' => true,
                    'menu_bill_payment' => false,
                    'menu_digital_form' => false,
                    'menu_service_request' => false,
                    'menu_marketplace' => false,
                    'menu_emergency' => false,
                    'menu_sell_rent' => false,
                    'menu_entertainment' => true,
                    'menu_survey' => true,
                    'menu_maintenance' => true
                );
            }else{
                $data = array(
                    'menu_dashboard' => true,
                    'menu_news_announcement' => true,
                    'menu_resident' => false,
                    'menu_parcel_tracking' => true,
                    'menu_bill_payment' => false,
                    'menu_digital_form' => false,
                    'menu_service_request' => false,
                    'menu_marketplace' => false,
                    'menu_emergency' => false,
                    'menu_sell_rent' => false,
                    'menu_entertainment' => true,
                    'menu_survey' => true,
                    'menu_maintenance' => true
                );
            }
        }else{
            $data = array(
                'menu_dashboard' => false,
                'menu_news_announcement' => true,
                'menu_resident' => false,
                'menu_parcel_tracking' => false,
                'menu_bill_payment' => false,
                'menu_digital_form' => false,
                'menu_service_request' => false,
                'menu_marketplace' => false,
                'menu_emergency' => false,
                'menu_sell_rent' => false,
                'menu_entertainment' => false,
                'menu_survey' => false,
                'menu_maintenance' => false
            );
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($results);
    }

    public function featurePropertyAISPlay(){
        $user_property_count = UserProperty::where('user_id',Auth::user()->id)->where('user_type','!=',null)->count();

        if($user_property_count > 0) {
            $user_property_owner = UserProperty::where('user_id',Auth::user()->id)->where('user_type',0)->count();
            if($user_property_owner > 0) {
                $data = array(
                    'menu_ais_play' => true
                );
            }else{
                $data = array(
                    'menu_ais_play' => true
                );
            }
        }else{
            $data = array(
                'menu_ais_play' => false
            );
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($results);
    }
}
