<?php namespace App\Http\Controllers\API;
use Auth;
use Request;
//use Storage;
//use File;
//use League\Flysystem\AwsS3v2\AwsS3Adapter;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PushNotificationController;
use Pusher\Pusher;
# Model
use App\User;
use App\Notification;
use App\Message;
use App\MessageText;
use App\MessageTextFile;
use App\Property;
use App\UserProperty;

class MessageController extends Controller {

	public function __construct () {
        //$this->middleware('jwt.feature_menu:menu_message');
	}

	public function messageIndex(){
        //$property_list = UserProperty::with('property')->where('user_id',Auth::user()->id)->get();
        $property_list = UserProperty::select('property_id')->where('user_id',Auth::user()->id)->groupBy('property_id')->get();
        $data_arr = [];
        foreach ($property_list as &$item){
            $new_message_counter = Message::where('property_id',$item->property_id)->where('user_id',Auth::user()->id)->where('flag_new_from_admin',true)->count();
            if($item->property->banner_url != ""){
                $item->property->banner_url_full_path = env('URL_S3')."/post-file".$item->property->banner_url;//https://ap-test-site.s3.amazonaws.com/post-file/e6/e6b12e4d77c683caa83be44f776b2166.jpg
            }else{
                $item->property->banner_url_full_path = "";
            }

            // find already chat room
            $message_room = Message::where('property_id',$item->property_id)->where('user_id',Auth::user()->id)->first();
            if(isset($message_room)){
                $message = $message_room;
            }else{
                $message = Message::create([
                    'user_id' 			=> Auth::user()->id,
                    'property_id' 		=> $item->property_id
                ]);
            }
            $data_arr[] = [
                'new_message_counter' => $new_message_counter,
                'property_id' => $item->property,
                'message_id' => $message->id
            ];
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $data_arr
        ];

        return response()->json($results);
    }

	public function message () {
	    /*$property_id = Request::get('property_id');
		$message = $this->checkExistdMessage ($property_id);*/
	    $message_id = Request::get('message_id');
	    $message = Message::find($message_id);

		if($message) {
			$messageTexts = MessageText::with('owner','messageTextFile')->where('message_id',$message->id)->orderBy('created_at','desc')->paginate(30);
            $results = $messageTexts->toArray();

            foreach ($results['data'] as &$item){
                if($item['message_text_file'] != null) {
                    foreach ($item['message_text_file'] as &$item_img){
                        $item_img['image_full_path'] = env('URL_S3') . '/messages-file/' . $item_img['url'] . $item_img['name'];
                    }
                }else{
                    // Nothing to do
                }

                if($item['owner']['profile_pic_name'] != null){
                    $item['owner']['profile_full_path'] = env('URL_S3')."/profile-img/".$item['owner']['profile_pic_path'].$item['owner']['profile_pic_name'];
                }else{
                    $item['owner']['profile_full_path'] ="";
                }
            }

            $message->flag_new_from_admin 	= false;
            $message->save();
        }else{
            $results = array(
                "data" => array(),
                "message" => "Empty message"
            );
        }

        return response()->json($results);
	}

    public function listAllMessage () {
        $property_id = Request::get('property_id');
        $message = $this->checkExistdMessage ($property_id);

        if($message) {
            $messageTexts = MessageText::with('owner')->where('message_id',$message->id)->orderBy('created_at','desc')->get();
            //$results = $messageTexts->toArray();
            $results = array(
                "data" => $messageTexts->toArray()
            );
            $message->flag_new_from_admin 	= false;
            $message->save();

        }else{
            $results = array(
                "data" => array(),
                "message" => "Empty message"
            );
        }

        return response()->json($results);
    }

	public function sendMessage () {
        try {
            if (Request::isMethod('post')) {
                /*$property_id = Request::get('property_id');
                $message = $this->checkExistdMessage ($property_id);*/
                $message_id = Request::get('message_id');
                $message = Message::find($message_id);

                /*if (!$message) {
                    $message = Message::create([
                        'user_id' => Auth::user()->id,
                        'property_id' => $property_id
                    ]);
                }*/
                $messageText = new messageText;
                $messageText->message_id = $message->id;
                $messageText->user_id = Auth::user()->id;
                $messageText->text = Request::get('message_text');
                $messageText->save();

                $message->flag_new_from_user 	= true;
                $message->last_user_message_date = date('Y-m-d H:i:s');
                $message->save(); //Update updated_at

                $this->saveAttachment($messageText);
                //$this->userSendMessageNotification($messageText = array());
                $this->pusherChat($messageText,$message->id);

                return response()->json(['status' => true]);
            }
        }catch(Exception $ex) {
            return response()->json(['status'=>false]);
        }
	}

    public function saveAttachment ($messageText) {
        if(!empty(Request::file('attachment'))) {
            $attach = [];
            foreach (Request::file('attachment') as $key => $file) {
                $name =  md5($file->getFilename());//getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $targetName = $name.".".$extension;

                $path = $this->createLoadBalanceDir($file);
                $isImage = 0;
                if(in_array($extension, ['jpeg','jpg','gif','png'])) {
                    $isImage = 1;
                }
                //Move Image
                $attach[] = new MessageTextFile([
                    'name' => $targetName,
                    'url' => $path,
                    'file_type' => $file->getClientMimeType(),
                    'is_image'	=> $isImage,
                    'original_name'	=> $file->getClientOriginalName()
                ]);
            }
            $messageText->messageTextFile()->saveMany($attach);
        }
    }

    /*public function createLoadBalanceDir ($name) {
        $targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
        $folder = substr($name, 0,2);
        $pic_folder = 'messages-file/'.$folder;
        $directories = Storage::disk('s3')->directories('messages-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".$name;
        Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
        return $folder."/";
    }*/

    public function createLoadBalanceDir ($imageFile) {
        $name =  md5($imageFile->getFilename());//getClientOriginalName();
        $extension = $imageFile->getClientOriginalExtension();
        $targetName = $name.".".$extension;

        $folder = substr($name, 0,2);

        $pic_folder = 'messages-file'.DIRECTORY_SEPARATOR.$folder;
        $directories = Storage::disk('s3')->directories('messages-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }

        $full_path_upload = $pic_folder.DIRECTORY_SEPARATOR.$targetName;
        $upload = Storage::disk('s3')->put($full_path_upload, file_get_contents($imageFile), 'public');// public set in photo upload
        if($upload){
            // Success
            File::delete($imageFile);
        }

        return $folder."/";
    }

	public function checkExistdMessage ($property_id) {
		$message = Message::where('user_id',Auth::user()->id)->where('property_id',$property_id)->first();
		return $message;
	}

    public function checkHaveNewMessage () {
        $message_count = Message::where('user_id',Auth::user()->id)->where('flag_new_from_admin', true)->count();
        if($message_count > 0){
            return response()->json(['status' => true]);
        }

        return response()->json(['status' => false]);
    }

    public function counterNewMessage(){
        $message_count = Message::where('user_id',Auth::user()->id)->where('flag_new_from_admin', true)->count();

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $message_count
        ];

        return response()->json($results);
    }

	public function userSendMessageNotification($messageText) {
		$admins = $this->getAdmin();
		$title = json_encode( ['type'=>'message_sent','text'=> $this->cutText ($messageText->text) ] );
		if( $admins->count() ) {
			/*foreach ($admins as $admin) {
				$notification = Notification::create([
					'title'				=> $title,
					'description' 		=> "",
					'notification_type' => 8,
					'subject_key'		=> $messageText->message_id,
					'to_user_id'		=> $admin->id,
					'from_user_id'		=> Auth::user()->id
				]);
				$controller_push_noti = new PushNotificationController();
				$controller_push_noti->pushNotification($notification->id);
			}*/
            $dataPusher = [
                'title' => "Hello Pusher",
                'notification' => "bbb"
            ];

            $app_id = env('PUSHER_APP_ID');
            $app_key = env('PUSHER_KEY');
            $app_secret = env('PUSHER_SECRET');
            $app_cluster = env('PUSHER_APP_CLUSTER');

            $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

            $pusher->trigger("81862267-1990-45b3-9873-e9f1bc47f5d9_897ffde3-ea5d-4c8b-9e49-dfc39bf6aa51", 'notification_event', $dataPusher);
            return "true";

		}
	}

	public function testPusher(){
        $dataPusher = [
            'title' => "มีข้อความใหม่",
            'notification' => ""
        ];

        $app_id = env('PUSHER_APP_ID');
        $app_key = env('PUSHER_KEY');
        $app_secret = env('PUSHER_SECRET');
        $app_cluster = env('PUSHER_APP_CLUSTER');

        $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

        $pusher->trigger("81862267-1990-45b3-9873-e9f1bc47f5d9", 'notification_event', $dataPusher);
        return "true";
    }

    public function pusherChat($chat_obj,$message_room_id){

        $app_id = env('PUSHER_APP_ID');
        $app_key = env('PUSHER_KEY');
        $app_secret = env('PUSHER_SECRET');
        $app_cluster = env('PUSHER_APP_CLUSTER');

        $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

        $chat_obj_return = $chat_obj;
        $chat_obj_return->owner = $chat_obj->owner;
        $chat_obj_return->messageTextFile = $chat_obj->messageTextFile;

        $message_text_arr = $chat_obj_return->toArray();

        $pusher->trigger($message_room_id, 'chat_message', $message_text_arr);
        return "true";
    }

	public function getAdmin () {
		return User::where('property_id',Auth::user()->property_id)
				->where(function ($q) {
					 $q	->where('role',1)
					 	->whereOr('role',3);
				})->get();
	}

	public function cutText ($text) {
		if(strlen($text) > 30 ) {
			return substr($text,30)."...";
		} else return $text;
	}
}
