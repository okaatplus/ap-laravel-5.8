<?php namespace App\Http\Controllers\API;
use App\UserProperty;
use GuzzleHttp\Exception\RequestException;
use Request;
use Auth;
use Mail;
use Hash;
use JWTAuth;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;
use DB;

use App\Property;
use App\Post;
use App\PostFile;
use App\PostProperty;
use App\PostTracking;
use App\PostPin;
use App\CoverImage;

class NewsController extends Controller {

    //use DispatchesJobs;

    public function __construct () {

    }

    public function ordinaryDashboard(){
        $all_post_list = [];

        // Post Announcement
            //  Get Important Post
            $important_posts = Post::where('publish_state',2)->where('flag_important',true)->where('post_type','n')->get();
            $important_posts_arr = $important_posts->toArray();
            foreach ($important_posts_arr as &$postItem){
                $postItem['is_pin'] = false;
                if($postItem['banner_url'] != null) {
                    $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
                }else{
                    $postItem['banner_url'] = "";
                }

                if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
            }

            // Get pin Post
            $pin_post = PostPin::select('post_id')->where('user_id',Auth::user()->id)->get();
            $array_pin_post = [];
            foreach ($pin_post as $item){
                $array_pin_post[] = $item->post_id;
            }
            $posts_by_pin = Post::whereIn('id',$array_pin_post)->where('post_type','n')->where('publish_state',2)->where('flag_important',false)->get();
            $posts_by_pin_arr = $posts_by_pin->toArray();
            foreach ($posts_by_pin_arr as &$postItem){
                $postItem['is_pin'] = true;
                if($postItem['banner_url'] != null) {
                    $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
                }else{
                    $postItem['banner_url'] = "";
                }

                if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
            }

            // Get Other Post
            /*$post_other = Post::where('publish_state',2)->whereNotIn('id', $array_pin_post)->where('post_type','a')->where('flag_important',false)->get();
            $posts_other_arr = $post_other->toArray();
            foreach ($posts_other_arr as &$postItem){
                $postItem['is_pin'] = true;
                if($postItem['banner_url'] != null) {
                    $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
                }else{
                    $postItem['banner_url'] = "";
                }

                if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
            }*/

            $all_announcement_post_list = array_merge($important_posts_arr,$posts_by_pin_arr);
            //$all_announcement_post_list = array_merge($important_posts_arr,$posts_by_pin_arr,$posts_other_arr);
        // END Post Announcement //

        // Post News
        //  Get Important Post
            $important_news_posts = Post::where('publish_state',2)->where('flag_important',true)->where('post_type','n')->get();
            $important_news_posts_arr = $important_news_posts->toArray();
            foreach ($important_news_posts_arr as &$postItem){
                $postItem['is_pin'] = false;
                if($postItem['banner_url'] != null) {
                    $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
                }else{
                    $postItem['banner_url'] = "";
                }

                if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
            }

            // Get pin Post
            $pin_news_post = PostPin::select('post_id')->where('user_id',Auth::user()->id)->get();
            $array_news_pin_post = [];
            foreach ($pin_news_post as $item){
                $array_news_pin_post[] = $item->post_id;
            }
            $news_posts_by_pin = Post::whereIn('id',$array_news_pin_post)->where('post_type','n')->where('publish_state',2)->where('flag_important',false)->get();
            $news_posts_by_pin_arr = $news_posts_by_pin->toArray();
            foreach ($news_posts_by_pin_arr as &$postItem){
                $postItem['is_pin'] = true;
                if($postItem['banner_url'] != null) {
                    $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
                }else{
                    $postItem['banner_url'] = "";
                }

                if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
            }

            // Get Other Post
            $news_post_other = Post::where('publish_state',2)->whereNotIn('id', $array_pin_post)->where('post_type','n')->where('flag_important',false)->get();
            $news_posts_other_arr = $news_post_other->toArray();
            foreach ($news_posts_other_arr as &$postItem){
                $postItem['is_pin'] = true;
                if($postItem['banner_url'] != null) {
                    $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
                }else{
                    $postItem['banner_url'] = "";
                }

                if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
            }

            //$all_news_post_list = $news_posts_other_arr;
            $all_news_post_list = array_merge($important_news_posts_arr,$news_posts_by_pin_arr,$news_posts_other_arr);
        // END Post News  //

        $cover_image_data = CoverImage::where('cover_type','o')->first();
        if(isset($cover_image_data)){
            $cover_image = env('URL_S3') . "/cover-file/". $cover_image_data->url . $cover_image_data->name;
        }else {
            $cover_image = url('/') . "/images/cover/cover_image.png";
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => [
                'cover_image' => $cover_image,
                'announcement'=>$all_announcement_post_list,
                'news'=>$all_news_post_list
            ]
        ];

        return response()->json($results);
    }

    public function customerNewsAnnouncement(){
        $property_id = Request::get('property_id');

        // Post Announcement
        //  Get Important Post
        $important_posts = Post::where('publish_state',2)->where('property_id',$property_id)
            ->where('flag_important',true)
            ->where('post_type','a')
            ->get();
        $important_posts_arr = $important_posts->toArray();
        foreach ($important_posts_arr as &$postItem){
            $postItem['is_pin'] = false;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        // Get pin Post
        $pin_post = PostPin::select('post_id')->where('user_id',Auth::user()->id)->get();
        $array_pin_post = [];
        foreach ($pin_post as $item){
            $array_pin_post[] = $item->post_id;
        }
        $posts_by_pin = Post::whereIn('id',$array_pin_post)->where('post_type','a')
            ->where('property_id',$property_id)
            ->where('publish_state',2)->where('flag_important',false)
            ->get();
        $posts_by_pin_arr = $posts_by_pin->toArray();
        foreach ($posts_by_pin_arr as &$postItem){
            $postItem['is_pin'] = true;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        // Get Other Post
        $post_other = Post::where('publish_state',2)->whereNotIn('id', $array_pin_post)
            ->where('property_id',$property_id)
            ->where('post_type','a')->where('flag_important',false)->get();
        $posts_other_arr = $post_other->toArray();
        foreach ($posts_other_arr as &$postItem){
            $postItem['is_pin'] = true;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        $all_announcement_post_list = array_merge($important_posts_arr,$posts_by_pin_arr,$posts_other_arr);
        // END Post Announcement //

        // Post News
        //  Get Important Post
        $important_news_posts = Post::where('publish_state',2)->where('flag_important',true)->where('post_type','n')->get();
        $important_news_posts_arr = $important_news_posts->toArray();
        foreach ($important_news_posts_arr as &$postItem){
            $postItem['is_pin'] = false;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        // Get pin Post
        $pin_news_post = PostPin::select('post_id')->where('user_id',Auth::user()->id)->get();
        $array_news_pin_post = [];
        foreach ($pin_news_post as $item){
            $array_news_pin_post[] = $item->post_id;
        }
        $news_posts_by_pin = Post::whereIn('id',$array_news_pin_post)->where('post_type','n')->where('publish_state',2)->where('flag_important',false)->get();
        $news_posts_by_pin_arr = $news_posts_by_pin->toArray();
        foreach ($news_posts_by_pin_arr as &$postItem){
            $postItem['is_pin'] = true;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        // Get Other Post
        $news_post_other = Post::where('publish_state',2)->whereNotIn('id', $array_news_pin_post)->where('post_type','n')->where('flag_important',false)->get();
        $news_posts_other_arr = $news_post_other->toArray();
        foreach ($news_posts_other_arr as &$postItem){
            $postItem['is_pin'] = true;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        $all_news_post_list = array_merge($important_news_posts_arr,$news_posts_by_pin_arr,$news_posts_other_arr);
        // END Post News  //

        $cover_image_data = CoverImage::where('cover_type','u')->first();
        if(isset($cover_image_data)){
            $cover_image = env('URL_S3') . "/cover-file/". $cover_image_data->url . $cover_image_data->name;
        }else {
            $cover_image = url('/') . "/images/cover/cover_image.png";
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => [
                'cover_image' => $cover_image,
                'announcement'=>$all_announcement_post_list,
                'news'=>$all_news_post_list
            ]
        ];

        return response()->json($results);
    }

    /*public function customerNewsAnnouncement(){
        $user_property = UserProperty::select('property_id')->where('user_id',Auth::user()->id)->get();
        $user_property_arr = [];
        foreach ($user_property as $user_property_item){
            $user_property_arr[] = $user_property_item->property_id;
        }

        $all_post_list = [];

        //  Get Important Post
        $important_posts = Post::whereIn('property_id',$user_property_arr)->where('publish_state',2)->where('flag_important',true)->get();
        $important_posts_arr = $important_posts->toArray();
        foreach ($important_posts_arr as &$postItem){
            $postItem['is_pin'] = false;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

    if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
        }

        // Get pin Post
        $pin_post = PostPin::select('post_id')->where('user_id',Auth::user()->id)->get();
        $array_pin_post = [];
        foreach ($pin_post as $item){
            $array_pin_post[] = $item->post_id;
        }
        $posts_by_pin = Post::whereIn('id',$array_pin_post)->whereIn('property_id',$user_property_arr)->where('publish_state',2)->where('flag_important',false)->get();
        $posts_by_pin_arr = $posts_by_pin->toArray();
        foreach ($posts_by_pin_arr as &$postItem){
            $postItem['is_pin'] = true;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

    if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
        }

        // Get Other Post
        $post_other = Post::where('publish_state',2)->whereNotIn('id', $array_pin_post)->whereIn('property_id',$user_property_arr)->where('flag_important',false)->get();
        $posts_other_arr = $post_other->toArray();
        foreach ($posts_other_arr as &$postItem){
            $postItem['is_pin'] = false;
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

    if($postItem['thumbnail_url'] != null) {
                    $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
                }else{
                    $postItem['thumbnail_url'] = "";
                }
        }

        $all_post_list = array_merge($important_posts_arr,$posts_by_pin_arr,$posts_other_arr);

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $all_post_list
        ];

        return response()->json($results);
    }*/

    public function details(){
        $payload = JWTAuth::parseToken()->getPayload();
        $device = $payload->get('device');
        $os = $payload->get('os');
        $id = Request::get('id');
        $post_query = Post::with('owner','postFile')->find($id);

        $post_data = $post_query->toArray();
        //$post_like = [];

        if($post_data['banner_url']) {
            $post_data['banner_url'] = env('URL_S3') . "/post-file" . $post_data['banner_url'];
        }else{
            $post_data['banner_url'] = "";
        }

        if(empty($post_data['description_en'])) {
            $post_data['description_en'] = $post_data['description_th'];
        }

        if(empty($post_data['title_en'])) {
            $post_data['title_en'] = $post_data['title_th'];
        }

        foreach($post_data['post_file'] as &$value)
        {
            $splitType = explode(".",$value['name']);
            $value['file_type'] = end($splitType);
            $value['url_full_path'] = env('URL_S3') . "/post-file/".$value['url'].$value['name'];
        }

        $post = $post_data;


        $check_post_tracking = PostTracking::where('user_id',Auth::user()->id)->where('post_id',$id)->count();
        if($check_post_tracking == 0) {
            // Add Post Tracking
            $post_tracking = new PostTracking();
            $post_tracking->post_id = $id;
            $post_tracking->user_id = Auth::user()->id;
            $post_tracking->device = $device;
            $post_tracking->os = $os;
            $post_tracking->save();
        }

        $relate_category = $post_query->ecosystem;

        $relate_post = Post::where('id','!=',$post_query->id)->where('publish_state',2)->where('post_type','n')->where('ecosystem',$relate_category)->orderBy('approved_at','DESC')->take(4)->get();
        $relate_post_arr = $relate_post->toArray();
        foreach ($relate_post_arr as &$postItem){
            if($postItem['banner_url'] != null) {
                $postItem['banner_url'] = env('URL_S3') . "/post-file" . $postItem['banner_url'];
            }else{
                $postItem['banner_url'] = "";
            }

            if($postItem['thumbnail_url'] != null) {
                $postItem['thumbnail_url'] = env('URL_S3') . "/post-file" . $postItem['thumbnail_url'];
            }else{
                $postItem['thumbnail_url'] = "";
            }
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' =>[
                'post_data' => $post,
                'relate_post' => $relate_post_arr
            ]
        ];

        return response()->json($results);
    }

    public function postPin(){
        try {
            if (Request::isMethod('post')) {
                $post_id = Request::get('id');
                $post = Post::find($post_id);

                $post_pin = PostPin::firstOrNew(
                    array(
                        'user_id' => Auth::user()->id,
                        'post_id' => $post_id,
                        'post_property_id' => $post->id
                    )
                );
                $post_pin->save();

                $results = [
                    'status' => true,
                    'message' => 'success',
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
            ];

            return response()->json($results);
        }
    }

    public function postUnpin(){
        try {
            if (Request::isMethod('post')) {
                $post_id = Request::get('id');

                $post_pin = PostPin::where('post_id',$post_id)->where('user_id',Auth::user()->id)->first();

                $post_pin->delete();

                $results = [
                    'status' => true,
                    'message' => 'success',
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
            ];

            return response()->json($results);
        }
    }

    public function searchNewsAnnouncement(){
        try {
            if (Request::isMethod('post')) {
                $property_list = UserProperty::select('property_id')->where('user_id',Auth::user()->id)->get();
                $array_property_id_list = [];
                foreach ($property_list as $item){
                    $array_property_id_list[] = $item->property_id;
                }

                $posts = Post::whereIn('property_id',$array_property_id_list)->where('publish_state',2);
                $keyword = Request::get('keyword');
                $posts = $posts->where(function ($q) use($keyword) {
                    $q->where('title_th','like',"%".$keyword."%");
                    $q->orwhere('title_en','like',"%".$keyword."%");
                    $q->orWhere('description_th','like',"%".$keyword."%");
                    $q->orWhere('description_en','like',"%".$keyword."%");
                });
                $posts = $posts->orderBy('post.created_at', 'desc')->paginate(15);

                $results = [
                    'status' => true,
                    'message' => 'success',
                    'data' => $posts
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
            ];

            return response()->json($results);
        }

    }

}
