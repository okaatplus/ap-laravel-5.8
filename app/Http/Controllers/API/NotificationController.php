<?php namespace App\Http\Controllers\API;
use GuzzleHttp\Exception\RequestException;
use Request;
use JWTAuth;
use Auth;
use DB;
use Mail;
use Hash;
use Storage;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;

use Carbon\Carbon;
use App\Property;
use App\Post;
use App\PostFile;
use App\PostProperty;
use App\Installation;
use App\Customer;
use App\User;
use App\UserProperty;
use App\CustomerProperty;
use App\PropertyUnit;
use App\Notification;



use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

// Firebase
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;
//use FCM;


class NotificationController extends Controller {

    public function __construct () {

    }

    public function index()
    {
        $all_noti = Notification::where('to_user_id','=',Auth::user()->id)->where('property_id','=',Auth::user()->property_id)
            ->where('property_unit_id','=',Auth::user()->property_unit_id)
            ->where('read_status','=',false)
            ->select('notification_type')->get();

        $counter_noti = [
            'comment' 	=> 0,
            'like'		=> 0,
            'complain'	=> 0,
            'bill'		=> 0,
            'event'		=> 0,
            'vote'		=> 0,
            'parcel'	=> 0,
            'other'		=> 0,
            'announcement'		=> 0
        ];

        foreach ($all_noti as $noti) {
            if( $noti->notification_type == 0 ) $counter_noti['comment']++;
            elseif( $noti->notification_type == 1 ) $counter_noti['like']++;
            elseif( $noti->notification_type == 2 ) $counter_noti['complain']++;
            elseif( $noti->notification_type == 3 ) $counter_noti['bill']++;
            elseif( $noti->notification_type == 4 ) $counter_noti['event']++;
            elseif( $noti->notification_type == 5 ) $counter_noti['vote']++;
            elseif( $noti->notification_type == 6 ) $counter_noti['parcel']++;
            elseif( $noti->notification_type == 7 || $noti->notification_type == 12) $counter_noti['other']++;
            elseif( $noti->notification_type == 11 ) $counter_noti['announcement']++;
            else ;
        }

        if(Request::get('type') != null) {
            if(Request::get('type') == "3") {
                $notis = Notification::with('sender')
                    ->whereIn('notification_type',['3','4','5','6','7','11','12'])
                    ->where('to_user_id', '=', Auth::user()->id)
                    ->where('property_id','=',Auth::user()->property_id)
                    ->where('read_status','=',false)
                    ->orderBy('notification.created_at', 'desc')
                    ->paginate(15);
            }else{
                $notis = Notification::with('sender')
                    ->where('to_user_id', '=', Auth::user()->id)
                    ->where('property_id','=',Auth::user()->property_id)
                    ->where('read_status','=',false)
                    ->where('notification_type', '=', Request::get('type'))
                    ->orderBy('notification.created_at', 'desc')
                    ->paginate(15);
            }
        }else{
            $notis = Notification::with('sender')
                ->where('to_user_id', '=', Auth::user()->id)
                ->where('property_id','=',Auth::user()->property_id)
                ->where('property_unit_id','=',Auth::user()->property_unit_id)
                ->where('read_status','=',false)
                ->orderBy('notification.created_at', 'desc')
                ->paginate(15);
        }

        $results_noti = $notis->toArray();

        foreach($results_noti['data'] as &$value)
        {
            $tempTitle = $value['title'];
            switch ($value['notification_type']) {
                case "0"://Comment
                    $value['title'] = trans('messages.Notification.' . $tempTitle, array(), null, Auth::user()->lang);
                    break;
                case "1"://Like
                    $value['title'] = trans('messages.Notification.' . $tempTitle, array(), null, Auth::user()->lang);
                    break;
                case "2"://Complain
                    $data = json_decode($tempTitle, true);
                    if (isset($data['type']) && $data['type'] == 'comment') {
                        $value['title'] = $value['sender']['name'] . " " .trans('messages.Notification.complain_comment', $data, null, Auth::user()->lang);
                    }elseif($data['type'] == 'change_status'){
                        $data['status'] = trans('messages.Complain.' . $data['status']);
                        $value['title'] = trans('messages.Notification.complain_change_status', $data, null, Auth::user()->lang);
                    } elseif($data['type'] == 'complain_created') {
                        $value['title'] = trans('messages.Notification.complain_created_', array(), null, Auth::user()->lang);
                    }
                    break;
                case "3"://Fees&Bills
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'sticker_approved') {
                        $value['title'] = trans('messages.Notification.sticker_approved_head', array(), null, Auth::user()->lang);
                    } elseif ($data['type'] == 'invoice_created') {
                        $value['title'] = trans('messages.Notification.invoice_created_msg', ['name' => $data['title']], null, Auth::user()->lang);
                    }
                    break;
                case "4"://Event
                    $data = json_decode($tempTitle, true);
                    if( $data['type'] == 'event_created') {
                        $value['title'] = trans('messages.Notification.event_created_msg',['name'=>$data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
                case "5"://Vote
                    $data = json_decode($tempTitle, true);
                    if( $data['type'] == 'vote_created') {
                        $value['title'] = trans('messages.Notification.vote_created_msg',['name'=>$data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
                case "6"://Post&Parcel
                    $data = json_decode($tempTitle, true);
                    if( $data['type'] == 'receive_post_parcel') {
                        $value['title'] = trans('messages.Notification.receive_post_parcel_msg',$data, null, Auth::user()->lang);
                    }
                    break;
                case "7"://Other
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'general') {
                        $value['title'] = $data['n_title'];
                    } elseif ($data['type'] == 'sticker_approved') {
                        $value['title'] = trans('messages.Notification.sticker_approved_head', array(), null, Auth::user()->lang);
                    } elseif ($data['type'] == 'invoice_created') {
                        $value['title'] = trans('messages.Notification.invoice_created_msg', ['name' => $data['title']], null, Auth::user()->lang);
                    } elseif( $data['type'] == 'event_created') {
                        $value['title'] = trans('messages.Notification.event_created_msg',['name'=>$data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    } elseif( $data['type'] == 'vote_created') {
                        $value['title'] = trans('messages.Notification.vote_created_msg',['name'=>$data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    } elseif( $data['type'] == 'receive_post_parcel') {
                        $value['title'] = trans('messages.Notification.receive_post_parcel_msg',$data, null, Auth::user()->lang);
                    } elseif( $data['type'] == 'discussion_created') {
                        $value['title'] = trans('messages.Notification.discussion_created_msg',['name'=> $data['title']], null, Auth::user()->lang);
                    } elseif( $data['type'] == 'discussion_comment') {
                        $value['title'] = trans('messages.Notification.discussion_comment_msg',['name'=> $data['title']], null, Auth::user()->lang);
                    }
                    break;
                case "11"://Post Created
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'post_created') {
                        $value['title'] = trans('messages.Notification.post_created_msg', ['name'=> $data['title_'.Auth::user()->lang]], null, Auth::user()->lang);
                    }
                    break;
                case "12"://Complete Bills Submit
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'transaction_complete') {
                        $value['title'] = trans('messages.Notification.transaction_complete_msg', ['name' => $data['title']], null, Auth::user()->lang);
                    }
                    break;
                case "14"://Privilege Create
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'privilege_created') {
                        $value['title'] = trans('messages.Notification.privilege_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
                case "18":// CR&ASV update job status
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'asv_change_status') {
                        if(Auth::user()->lang == 'en') {
                            $value['title'] = trans('messages.Notification.asv_change_status', ['name' => $data['c_title_en']], null, Auth::user()->lang);
                        }else{
                            $value['title'] = trans('messages.Notification.asv_change_status', ['name' => $data['c_title_th']], null, Auth::user()->lang);
                        }
                    }
                    break;
                default:
                    $value['title'] = $value['sender']['name'] . " " . trans('messages.Notification.' . $tempTitle, array(), null, Auth::user()->lang);
                    break;
            }
        }

        if(Auth::user()->role == 1 || Auth::user()->role == 3) {
            $unit_list = array(''=>'ยูนิต');
            $unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->lists('unit_number','id')->toArray();

            $result = [
                'noti_counter_new' => $counter_noti,
                'noti_detail' => $results_noti,
                'unit_list' => $unit_list,
                //'new_message' => $message_flag
            ];
            return response()->json($result);
        }
        else {
            $result = [
                'noti_counter_new' => $counter_noti,
                'noti_detail' => $results_noti,
                //'new_message' => $message_flag
            ];
            return response()->json($result);
        }
    }

    public function list(){
        $all_noti = Notification::where('to_user_id','=',Auth::user()->id)->orderBy('created_at','DESC')->get();

        $results_noti = $all_noti->toArray();

        foreach($results_noti as &$value) {
            $tempTitle = $value['title'];
            switch ($value['notification_type']) {
                case "0":// News
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'news_created') {
                        $value['title'] = trans('messages.Notification.news_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
                case "1":// Announcement
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'announcement_created') {
                        $value['title'] = trans('messages.Notification.announcement_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;

                case "2":// Post Parcel
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'receive_post_parcel') {
                        $value['title'] = trans('messages.Notification.receive_post_parcel_msg',$data, null, Auth::user()->lang);
                    }
                    break;
                case "3":// request new resident
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'request_new_resident') {
                        $value['title'] = trans('messages.Notification.request_new_resident',$data, null, Auth::user()->lang);
                    }
                    break;
                case "4":// accept new resident
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'accepted_resident') {
                        $value['title'] = trans('messages.Notification.accepted_resident',$data, null, Auth::user()->lang);
                    }
                    break;
                case "5"://Complain
                    $data = json_decode($tempTitle, true);
                    if (isset($data['type']) && $data['type'] == 'comment') {
                        $value['title'] = $value['sender']['name'] . " " .trans('messages.Notification.complain_comment', $data, null, Auth::user()->lang);
                    }elseif($data['type'] == 'change_status'){
                        $data['status'] = trans('messages.Complain.' . $data['status']);
                        $value['title'] = trans('messages.Notification.complain_change_status', $data, null, Auth::user()->lang);
                    } elseif($data['type'] == 'complain_created') {
                        $value['title'] = trans('messages.Notification.complain_created_', array(), null, Auth::user()->lang);
                    }elseif($data['type'] == 'asv_change_status') {
                        if(Auth::user()->lang == 'en') {
                            $value['title'] = $data['c_title_en'];
                        }else{
                            $value['title'] = $data['c_title_th'];
                        }
                    }
                    break;
                case "6"://Survey
                    $data = json_decode($tempTitle, true);
                    if( $data['type'] == 'vote_created') {
                        $value['title'] = trans('messages.Notification.vote_created_msg',['name'=>$data['title_th'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
            }
        }

        $results = [
            'status' => true,
            'message' => "successful",
            'data' => $results_noti
        ];

        return response()->json($results);
    }

    public function listByProperty(){
        $property_id =Request::get('property_id');
        $property_unit_id =Request::get('property_unit_id');

        $all_noti = Notification::where('to_user_id','=',Auth::user()->id)->where('property_id',$property_id)
                                                                            ->where('property_unit_id',$property_unit_id)
                                                                            ->orderBy('created_at','DESC')->get();

        $results_noti = $all_noti->toArray();

        foreach($results_noti as &$value) {
            $tempTitle = $value['title'];
            switch ($value['notification_type']) {
                case "0":// News
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'news_created') {
                        $value['title'] = trans('messages.Notification.news_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
                case "1":// Announcement
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'announcement_created') {
                        $value['title'] = trans('messages.Notification.announcement_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;

                case "2":// Post Parcel
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'receive_post_parcel') {
                        $value['title'] = trans('messages.Notification.receive_post_parcel_msg',$data, null, Auth::user()->lang);
                    }
                    break;
                case "3":// request new resident
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'request_new_resident') {
                        $value['title'] = trans('messages.Notification.request_new_resident',$data, null, Auth::user()->lang);
                    }
                    break;
                case "4":// accept new resident
                    $data = json_decode($tempTitle, true);
                    if ($data['type'] == 'accepted_resident') {
                        $value['title'] = trans('messages.Notification.accepted_resident',$data, null, Auth::user()->lang);
                    }
                    break;
                case "5"://Complain
                    $data = json_decode($tempTitle, true);
                    if (isset($data['type']) && $data['type'] == 'comment') {
                        $value['title'] = $value['sender']['name'] . " " .trans('messages.Notification.complain_comment', $data, null, Auth::user()->lang);
                    }elseif($data['type'] == 'change_status'){
                        $data['status'] = trans('messages.Complain.' . $data['status']);
                        $value['title'] = trans('messages.Notification.complain_change_status', $data, null, Auth::user()->lang);
                    } elseif($data['type'] == 'complain_created') {
                        $value['title'] = trans('messages.Notification.complain_created_', array(), null, Auth::user()->lang);
                    }elseif($data['type'] == 'asv_change_status') {
                        if(Auth::user()->lang == 'en') {
                            $value['title'] = $data['c_title_en'];
                        }else{
                            $value['title'] = $data['c_title_th'];
                        }
                    }
                    break;
                case "6"://Survey
                    $data = json_decode($tempTitle, true);
                    if( $data['type'] == 'vote_created') {
                        $value['title'] = trans('messages.Notification.vote_created_msg',['name'=>$data['title_th'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
            }
        }

        $results = [
            'status' => true,
            'message' => "successful",
            'data' => $results_noti
        ];

        return response()->json($results);
    }

    public function markAsRead(){
        try{
            $notification_id = Request::get('notification_id');
            $notification = Notification::find($notification_id);
            $notification->read_status = true;
            $notification->save();

            $results = [
                'status' => true,
                'message' => "successful",
            ];
            return response()->json($results);
        }catch(Exception $ex) {
            $results = [
            'status' => false,
            'message' => 'Unsuccessful'
            ];
            return response()->json($results);
        }
    }

    public function testPushNotification(){
        /*$device_token_obj = Installation::select('device_token')->where('user_id', '=', $user_id)->whereNotNull('device_token')->get();
        $device_token_array = $device_token_obj->toArray();

        $device_token_list = array();
        foreach ($device_token_array as $item){
            $device_token_list[] = $item['device_token'];
        }*/
        $message_string = "";
        $device_token_list[] = 'dOXk_LASt-k:APA91bF8T3bJCG6X1JJ3XG4Z5E_KD4a6u5nZpEPDfNtog5tL7M24yzk5oK67-kfcjcEwfUxpii-ee26t3vHilxM17nqfMzx3LJC8vmgSa7oxKb1AzFT0fxsqJP3UvYi6AdIZTtZPWmGB';

        $message = array(
            "priority" => "high",
            'notification' => array(
                'title' => "Smart World",
                'text' => "mantest",
                'click_action' => 1
            ),
            'data' => array(
                'notification_id' => 123131231,
                'subject_key' => 123131231,
                'notification_type' => 1
            ),
            'badge' => 1
        );

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);
        $priority = 'high'; // or 'normal'
        $optionBuiler->setPriority($priority);


        $notificationBuilder = new PayloadNotificationBuilder('Smart World');
        $notificationBuilder->setBody($message_string)
            ->setClickAction("notification")
            ->setIcon("ic_icon_notification")
            ->setSound('default')
            ->setBadge(1);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($message);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$token = "eRO-BLvRsjY:APA91bEepdr3EbOjpOdu4z4ThBVgRFjaW3TMhv85-AK4ZIjp0vHplxzgclGNFaP0L9YtEV4mzpv5nrum4rlu34lMPFiZRvyxs67Mp-g5_Aty5hmBmwW3ljJUsi1QNPJMgfoD04CsIDAp";
        //$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $tokens = 'dOXk_LASt-k:APA91bF8T3bJCG6X1JJ3XG4Z5E_KD4a6u5nZpEPDfNtog5tL7M24yzk5oK67-kfcjcEwfUxpii-ee26t3vHilxM17nqfMzx3LJC8vmgSa7oxKb1AzFT0fxsqJP3UvYi6AdIZTtZPWmGB';
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
    }

}
