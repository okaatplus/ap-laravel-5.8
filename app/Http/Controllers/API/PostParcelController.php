<?php namespace App\Http\Controllers\API;
use App\UserProperty;
use GuzzleHttp\Exception\RequestException;
use Request;
use Auth;
use Mail;
use Hash;
use JWTAuth;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;
use DB;
use Carbon\Carbon;

use App\Property;
use App\PostParcel;

class PostParcelController extends Controller {

    //use DispatchesJobs;

    public function __construct () {

    }

    public function newPostParcelList(){
        try {
            $property_id = Request::get('property_id');
            $property_unit_id = Request::get('property_unit_id');
            $post_parcel = PostParcel::where('property_id', $property_id)->where('property_unit_id', $property_unit_id);

            $new_post_parcel = $post_parcel->where('status', 0)->orderBy('date_received','desc')->paginate(15);
            $new_post_parcel_arr = $new_post_parcel->toArray();
            foreach ($new_post_parcel_arr['data'] as &$item) {
                $date_now = Carbon::now();
                $date_received = Carbon::parse($item['date_received']);
                $date_expire = $date_received->addDays(30);
                $day_count = $date_now->diffInDays($date_expire);

                $item['expire_date_count'] = $day_count;

                if($item['type'] == "1"){
                    $item['type_name_th'] = "จดหมายลงทะเบียน";
                    $item['type_name_en'] = "Register Parcel";
                    //1 = จดหมายลงทะเบียน, 2 = พัสดุ, 3 = EMS
                }

                if($item['type'] == "2"){
                    $item['type_name_th'] = "พัสดุ";
                    $item['type_name_en'] = "Parcel";
                }

                if($item['type'] == "3"){
                    $item['type_name_th'] = "EMS";
                    $item['type_name_en'] = "EMS";
                }
            }

            $results = [
                'status' => true,
                'message' => 'success',
                'data' => $new_post_parcel_arr
            ];

            return response()->json($results);
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
                'data' => []
            ];

            return response()->json($results);
        }
    }

    public function historyPostParcelList(){
        try {
            $property_id = Request::get('property_id');
            $property_unit_id = Request::get('property_unit_id');
            $post_parcel = PostParcel::where('property_id', $property_id)->where('property_unit_id', $property_unit_id);

            $history_post_parcel = $post_parcel->where('status', 1)->orderBy('date_received','desc')->paginate(15);

            $history_post_parcel_arr = $history_post_parcel->toArray();
            foreach ($history_post_parcel_arr['data'] as &$item) {
                $date_now = Carbon::now();
                $date_received = Carbon::parse($item['date_received']);
                $date_expire = $date_received->addDays(30);
                $day_count = $date_now->diffInDays($date_expire);

                $item['expire_date_count'] = $day_count;

                if($item['type'] == "1"){
                    $item['type_name_th'] = "จดหมายลงทะเบียน";
                    $item['type_name_en'] = "Register Parcel";
                    //1 = จดหมายลงทะเบียน, 2 = พัสดุ, 3 = EMS
                }

                if($item['type'] == "2"){
                    $item['type_name_th'] = "พัสดุ";
                    $item['type_name_en'] = "Parcel";
                }

                if($item['type'] == "3"){
                    $item['type_name_th'] = "EMS";
                    $item['type_name_en'] = "EMS";
                }
            }

            $results = [
                'status' => true,
                'message' => 'success',
                'data' => $history_post_parcel_arr
            ];

            return response()->json($results);
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
                'data' => []
            ];

            return response()->json($results);
        }
    }

    public function postParcelDetail(){
        try {
            $post_parcel_id = Request::get('post_parcel_id');
            $post_parcel = PostParcel::find($post_parcel_id);

            $post_parcel_arr = $post_parcel->toArray();

            $date_now = Carbon::now();
            $date_received = Carbon::parse($post_parcel_arr['date_received']);
            $date_expire = $date_received->addDays(30);
            $day_count = $date_now->diffInDays($date_expire);

            $post_parcel_arr['expire_date_count'] = $day_count;

            if($post_parcel_arr['type'] == "1"){
                $post_parcel_arr['type_name_th'] = "จดหมายลงทะเบียน";
                $post_parcel_arr['type_name_en'] = "Register Parcel";
                //1 = จดหมายลงทะเบียน, 2 = พัสดุ, 3 = EMS
            }

            if($post_parcel_arr['type'] == "2"){
                $post_parcel_arr['type_name_th'] = "พัสดุ";
                $post_parcel_arr['type_name_en'] = "Parcel";
            }

            if($post_parcel_arr['type'] == "3"){
                $post_parcel_arr['type_name_th'] = "EMS";
                $post_parcel_arr['type_name_en'] = "EMS";
            }

            $results = [
                'status' => true,
                'message' => 'success',
                'data' => $post_parcel_arr
            ];

            return response()->json($results);
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
                'data' => []
            ];

            return response()->json($results);
        }
    }

    public function countNewPostParcel(){
        try {
            $property_id = Request::get('property_id');
            $property_unit_id = Request::get('property_unit_id');
            $post_parcel_count = PostParcel::where('property_id', $property_id)->where('property_unit_id', $property_unit_id)->where('status', 0)->count();

            $results = [
                'status' => true,
                'message' => 'success',
                'data' => $post_parcel_count
            ];

            return response()->json($results);
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
                'data' => []
            ];

            return response()->json($results);
        }
    }

}
