<?php namespace App\Http\Controllers\API;
use GuzzleHttp\Exception\RequestException;
use Request;
use Auth;
use Mail;
use Hash;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;
use DB;
use Carbon\Carbon;

use App\Property;
use App\PropertyBrand;
use App\Page;
use App\User;
use App\otp;
use App\Customer;
use App\CustomerProperty;
use App\UserProperty;
use App\Tenant;
use App\TenantProperty;
use App\Installation;

class RegisterController extends Controller {

    //use DispatchesJobs;

    public function __construct () {

    }

    public function getTermCondition(){
        $term_condition = Page::where('alias',"terms")->first();
        $property_brand_arr = $term_condition->toArray();

        $results = [
            'status' => true,
            'message' => "success",
            'data' => $property_brand_arr
        ];

        return response()->json($results);
    }

    public function getPrivacy(){
        $term_condition = Page::where('alias',"privacy")->first();
        $property_brand_arr = $term_condition->toArray();

        $results = [
            'status' => true,
            'message' => "success",
            'data' => $property_brand_arr
        ];

        return response()->json($results);
    }

    public function checkIdCard(){
        $id_card = Request::get('id_card');
        $type_id_card = (Request::get('type_id_card') != null) ? Request::get('type_id_card') : 0;
        $user_count = User::where('id_card',$id_card)->count();
        $is_owner_resident = false;
        $is_tenant = false;
        if($user_count > 0){
            // nothing to do

        }else{
            // save user id_card
            $user_update = User::find(Auth::user()->id);
            $user_update->id_card = $id_card;
            $user_update->type_id_card = $type_id_card;
            $user_update->save();

            //check customer
            $customer = Customer::where('id_card',$id_card)->first();

            if(isset($customer)){
                // add user property
                $user_update->customer_id = $customer->id;
                $user_update->name = $customer->name;
                $user_update->save();

                $customer_property = CustomerProperty::where('customer_id',$customer->id)->get();

                foreach ($customer_property as $item){
                    $already_user_property = UserProperty::firstOrNew(
                        array(
                            'user_id' => Auth::user()->id,
                            'property_id' => $item->property_id,
                            'property_unit_id' => $item->property_unit_id,
                        )
                    );
                    $already_user_property->approve_status = 1;
                    $already_user_property->user_type = $item->customer_type;
                    $already_user_property->save();
                    $is_owner_resident = true;
                }
            }else{
                $is_owner_resident = false;
            }

            //check tenant
            $tenant = Tenant::where('id_card',$id_card)->first();

            if(isset($tenant)){
                // add user property
                $user_update->tenant_id = $tenant->id;
                $user_update->name = $tenant->name;
                $user_update->save();

                $tenant_property = TenantProperty::where('tenant_id',$tenant->id)->get();

                foreach ($tenant_property as $item){
                    $already_user_property = UserProperty::firstOrNew(
                        array(
                            'user_id' => Auth::user()->id,
                            'property_id' => $item->property_id,
                            'property_unit_id' => $item->property_unit_id,
                        )
                    );
                    $already_user_property->approve_status = 1;
                    $already_user_property->user_type = 2;
                    $already_user_property->save();
                    $is_tenant = true;
                }
            }else{
                $is_tenant = false;
            }
        }

        if($is_tenant || $is_owner_resident){
            $is_customer = true;
        }else{
            $is_customer = false;
        }

        $results = [
            'status' => true,
            'message' => "success",
            'data' => [
                'is_customer'=>$is_customer
            ]
        ];

        return response()->json($results);
    }

    public function register(){
        $email = Request::get('email');
        $password = Request::get('password');
        $mobile = Request::get('mobile');

        if(Request::get('lang') == 'en'){
            $lang = 'en';
        }else{
            $lang = 'th';
        }

        $user_account = new User;
        $user_account->email = $email;
        $user_account->role = 8;
        $user_account->password = Hash::make($password);
        $user_account->lang = $lang;
        $user_account->phone = $mobile;
        $user_account->save();

        $results = [
            'status' => true,
            'message' => "success",
            'data' => $user_account
        ];

        return response()->json($results);
    }

    public function registerAndPasscode(){
        $email = Request::get('email');
        $password = Request::get('password');
        $mobile = Request::get('mobile');

        if(Request::get('lang') == 'en'){
            $lang = 'en';
        }else{
            $lang = 'th';
        }

        $user_account = new User;
        $user_account->email = $email;
        $user_account->role = 8;
        $user_account->password = Hash::make($password);
        $user_account->lang = $lang;
        $user_account->phone = $mobile;
        $user_account->save();

        $device_token = Request::get('device_token');
        $device_uuid = Request::get('device_uuid');
        $passcode = Request::get('passcode');
        $device_type = Request::get('device_type');

        // remove installation by device_uuid
        $installation_remove = Installation::where('device_uuid', '=', $device_uuid)->get();
        foreach ($installation_remove as $item){
            $item->delete();
        }

        $new_device = new Installation();
        $new_device->device_token = $device_token;
        $new_device->device_uuid = $device_uuid;
        $new_device->passcode = $passcode;
        $new_device->user_id = $user_account->id;
        $new_device->lang = $lang;
        $new_device->device_type = $device_type;
        $new_device->save();

        $results = [
            'status' => true,
            'message' => "success",
            'data' => $user_account
        ];

        return response()->json($results);
    }

    function checkEmail() {
        $user = User::where('email','=',Request::get('email'))->first();
        /*$status = false;
        if(isset($user)){
            if($user->verification_code == null){
                if($user->password != null){
                    $status = 0;
                    $message = "email verified";
                }else {
                    $status = 1;
                    $message = "set password not yet";
                }
            }else{
                $status = 2;
                $message = "email verify not yet";
            }
        }else{
            $status = 3;
            $message = "email not use in system";
        }*/

        if(isset($user)){
            $status = 0;
            $message = "email verified";
        }else{
            $status = 1;
            $message = "email not use in system";
        }

        $results = [
            'status' => $status,
            'msg' => $message
        ];
        return response()->json($results);
    }

    public function otp(){
        $mobile = Request::get('mobile');
        $ref_code = $this->generateRefCode();

        // For sandbox test OTP phone number 0899999999
        if($mobile == "0899999999"){
            $otp_code = "0000";
            $ref_code = "0000";

            $timeout = Carbon::now()->addMinute()->timestamp;
            $new_otp = new otp();
            $new_otp->otp = $otp_code;
            $new_otp->ref_code = $ref_code;
            $new_otp->phone_number = $mobile;
            $new_otp->timeout = $timeout;
            $new_otp->save();
        }else {
            $otp_code = $this->generateOTP();

            $timeout = Carbon::now()->addMinute()->timestamp;
            $new_otp = new otp();
            $new_otp->otp = $otp_code;
            $new_otp->ref_code = $ref_code;
            $new_otp->phone_number = $mobile;
            $new_otp->timeout = $timeout;
            $new_otp->save();

            $this->otpSend($otp_code, $ref_code, $mobile, "th");
        }

        $results = [
            'status' => true,
            'message' => "success",
            'data' => [
                'ref_code' => $ref_code,
                'otp_code' => $otp_code
            ]
        ];

        return response()->json($results);
    }

    public function otpSend($otp_code,$ref_code,$phone_nummber,$lang){
        $phone = substr($phone_nummber, 1);
        $phone_country_code = "+66".$phone;

        if($lang == 'en') {
            $data = array(
                "to" => $phone_country_code,
                "from" => "SmartWorld",
                //"text" => "Your OTP for use in Smart World is " . $otp_code . "(Ref Code: " . $ref_code . ")",
                "text" => "Your OTP:".$otp_code." (Ref:".$ref_code.") for use in Smart World",
                "apiKey" => "0ca4e9f4-ced2-46f3-aa43-e5c085a077f4",
                "apiSecret" => "992509b1-fdac-4a55-85f1-c6d07ff6a93b"
            );
        }else{
            $data = array(
                "to" => $phone_country_code,
                "from" => "SmartWorld",
                //"text" => "รหัส OTP สำหรับใช้งานบนระบบ Smart World ของคุณคือ " . $otp_code . "(Ref Code: " . $ref_code . ")",
                "text" => "รหัส OTP:".$otp_code." (Ref:".$ref_code.") สำหรับใช้งานบนระบบ Smart World",
                "apiKey" => "0ca4e9f4-ced2-46f3-aa43-e5c085a077f4",
                "apiSecret" => "992509b1-fdac-4a55-85f1-c6d07ff6a93b"
            );
        }

        $data_string = json_encode($data);

        $ch = curl_init('https://api.apitel.co/sms');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        $http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        if ( $http_code == "200" ){
            //echo 'Successful!  Server responded with:'.$result;
            $message = 'Successful!  Server responded with:'.$result;
        }else{
            //echo 'Failed!  Server responded with:'.$result;
            $message = 'Failed!  Server responded with:'.$result;
        }
        curl_close( $ch );

        return $message;
    }

    public function verifyOTP(){
        $otp = Request::get('otp');
        $mobile = Request::get('mobile');
        $ref_code = Request::get('ref_code');

        $otp = otp::where('phone_number',$mobile)->where('otp',$otp)->where('ref_code',$ref_code)->latest()->first();
        if(isset($otp)){
            $now = Carbon::now();
            $otp_date = $otp->timeout;
            $otp_date_timeout = Carbon::createFromTimestamp($otp_date);
            $diff = $now->diffInMinutes($otp_date_timeout);

            if($diff == 0){

                $otp_update_status = otp::find($otp->id);
                $otp_update_status->delete();

                $status = true;
                $message = 'opt_pass';
            }else{
                $status = false;
                $message = 'opt_timeout';
            }
        }else{
            $status = false;
            $message = 'opt_notmatch';
        }

        $results = [
            'status' => $status,
            'message' => $message
        ];

        return response()->json($results);
    }

    public function listPropertyType(){
        //1=บ้านจัดสรร, 2=โรงแรม, 3=คอนโด, 4=อื่นๆ
        $property_type = [
            [
                'en'=>'House',
                'th'=>'บ้าน',
                'type'=>'1'
            ],
            [
                'en'=>'Townhome',
                'th'=>'ทาวน์โฮม',
                'type'=>'2'
            ],
            [
                'en'=>'Condominium',
                'th'=>'คอนโดมิเนียม',
                'type'=>'3'
            ],
            [
                'en'=>'Semi Detached House',
                'th'=>'บ้านแฝด',
                'type'=>'4'
            ],[
                'en'=>'Home Office',
                'th'=>'โฮมออฟฟิศ',
                'type'=>'5'
            ],

            /*,
            [
                'en'=>'Other',
                'th'=>'อื่นๆ',
                'type'=>'4'
            ],*/
        ];

        $data_all = [
            'property_type' => $property_type
        ];

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $data_all
        ];

        return response()->json($results);
    }

    public function listBrand(){
        $property_type = Request::get('property_type');
        $property_brand = PropertyBrand::where('brand_type',$property_type)->where('is_active',true)->get();
        $property_brand_arr = $property_brand->toArray();

        foreach ($property_brand_arr as &$postItem){
            if($postItem['img_url'] != null) {
                $postItem['img_url'] = url('/')."/images/brand/" . $postItem['img_url'];
            }else{
                $postItem['img_url'] = "";
            }
        }

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $property_brand_arr
        ];

        return response()->json($results);
    }

    public function listProperty(){
        $type = Request::get('type');
        $brand_code = Request::get('brand_code');

        $property = Property::select('id','property_name_th','property_name_en','juristic_person_name_th','juristic_person_name_en')->where('brand',$brand_code)->where('property_type',$type)->get();

        $results = [
            'status' => true,
            'message' => 'success',
            'data' => $property
        ];

        return response()->json($results);
    }

    public function addProperty(){

    }

    function generateOTP() {
        $chars = "123456789";
        $i = 0;
        $pass = '' ;
        while ($i < 4) {
            $num = rand() % 9;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    function generateRefCode(){
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $i = 0;
        $pass = '' ;
        while ($i < 4) {
            $num = rand() % 35;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

}
