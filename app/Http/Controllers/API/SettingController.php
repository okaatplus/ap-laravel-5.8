<?php namespace App\Http\Controllers\API;
use GuzzleHttp\Exception\RequestException;
use Request;
use JWTAuth;
use Auth;
use DB;
use Mail;
use Hash;
use Storage;
use Illuminate\Routing\Controller;
use Pusher\Pusher;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\PushNotificationController;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;

use Carbon\Carbon;
use App\Property;
use App\Post;
use App\PostFile;
use App\PostProperty;
use App\Installation;
use App\Customer;
use App\User;
use App\UserProperty;
use App\CustomerProperty;
use App\PropertyUnit;
use App\Notification;
use App\Tenant;


class SettingController extends Controller {

    public function __construct () {

    }

    public function updateInstall(){
        try{
            if (Request::isMethod('post')) {
                $device_token = Request::get('device_token');
                $device_uuid = Request::get('device_uuid');
                if(Request::get('lang') != null){
                    if(Request::get('lang') == 'en'){
                        $lang = 'en';
                    }else{
                        $lang = 'th';
                    }
                }else{
                    $lang = 'th';
                }


                if(isset($device_token) && $device_token != "") {

                    // Case 01 : Delete App BUT forget logout => device_token : OLD, device_uuid : CHANGE
                    $device_case01 = Installation::where('device_token', '=', $device_token)
                        ->where('device_uuid', '!=', $device_uuid)
                        ->where('user_id', '=', Auth::user()->id);

                    if ($device_case01->count() > 0) {
                        $device_type = $device_case01->first()->device_type;
                        $device_case01->delete();

                        $device = new Installation();
                        $device->user_id = Auth::user()->id;
                        $device->device_token = $device_token;
                        $device->device_type = $device_type;
                        $device->device_uuid = $device_uuid;
                        $device->lang = $lang;
                        $device->save();
                    }

                    // Case 02 : Update OS BUT forget logout => device_token : CHANGE, device_uuid : OLD
                    $device_case02 = Installation::where('device_token', '!=', $device_token)
                        ->where('device_uuid', '=', $device_uuid)
                        ->where('user_id', '=', Auth::user()->id);

                    if ($device_case02->count() > 0) {
                        $device_type = $device_case02->first()->device_type;
                        $device_case02->delete();

                        $device = new Installation();
                        $device->user_id = Auth::user()->id;
                        $device->device_token = $device_token;
                        $device->device_type = $device_type;
                        $device->device_uuid = $device_uuid;
                        $device->lang = $lang;
                        $device->save();
                    }
                }
            }

            $results = [
                'status' => true,
                'message' => "success"
            ];

            return response()->json($results);
        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function checkPasscodeAlready(){
        try{
            $device_uuid = Request::get('device_uuid');
            $installation_count = Installation::where('device_uuid', '=', $device_uuid)
                ->where('user_id', '=', Auth::user()->id)->where('passcode','!=',null)->count();

            if($installation_count > 0){
                $results = [
                    'status' => true,
                    'message' => "Passcode setting already"
                ];
            }else{
                $results = [
                    'status' => false,
                    'message' => "Passcode setting not yet"
                ];
            }
            return response()->json($results);

        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function updatePasscode(){
        try{
            if (Request::isMethod('post')) {
                $device_token = Request::get('device_token');
                $device_uuid = Request::get('device_uuid');
                $passcode = Request::get('passcode');
                $device_type = Request::get('device_type');

                // remove installation by device_uuid
                $installation_remove = Installation::where('device_uuid', '=', $device_uuid)->get();
                foreach ($installation_remove as $item){
                    $item->delete();
                }

                $new_device = new Installation();
                $new_device->device_token = $device_token;
                $new_device->device_uuid = $device_uuid;
                $new_device->passcode = $passcode;
                $new_device->user_id = Auth::user()->id;
                $new_device->lang = Auth::user()->lang;
                $new_device->device_type = $device_type;
                $new_device->save();

                $results = [
                    'status' => true,
                    'message' => "success"
                ];

                /*$installation = Installation::where('device_uuid', '=', $device_uuid)
                    ->where('user_id', '=', Auth::user()->id)->first();

                if(isset($installation)) {
                    $installation_update = Installation::find($installation->id);
                    $installation_update->passcode = $passcode;
                    $installation_update->save();

                    $results = [
                        'status' => true,
                        'message' => "success"
                    ];
                }else{
                    $results = [
                        'status' => false,
                        'message' => "install not found"
                    ];
                }*/
            }else{
                $results = [
                    'status' => false,
                    'message' => "wrong method"
                ];
            }

            return response()->json($results);
        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function customerCheck(){
        try{
                $id_card = Request::get('id_card');
                $type_id_card = (Request::get('type_id_card') != null) ? Request::get('type_id_card') : 0;
                $customer = Customer::where('id_card',$id_card)->first();
                if(isset($customer)){
                    // add user property
                    $user = User::find(Auth::user()->id);
                    $user->customer_id = $customer->id;
                    $user->id_card = $customer->id_card;
                    $user->type_id_card = $type_id_card;
                    $user->save();

                    $customer_property = CustomerProperty::where('customer_id',$customer->id)->get();

                    foreach ($customer_property as $item){
                        $already_user_property = UserProperty::firstOrNew(
                            array(
                                'user_id' => Auth::user()->id,
                                'property_id' => $item->property_id,
                                'property_unit_id' => $item->property_unit_id,
                            )
                        );
                        $already_user_property->approve_status = 1;
                        $already_user_property->user_type = $item->customer_type;
                        $already_user_property->save();
                    }

                    $results = [
                        'status' => true,
                        'message' => "Add property data complete"
                    ];
                }else{
                    $user = User::find(Auth::user()->id);
                    $user->id_card = $id_card;
                    $user->type_id_card = $type_id_card;
                    $user->save();

                    $results = [
                        'status' => false,
                        'message' => "not have customer data"
                    ];
                }

            return response()->json($results);
        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function requestAddProperty(){
        try{
            $property_id = Request::get('property_id');
            $unit_number = Request::get('unit_number');
            $id_card = Request::get('id_card');

            $user = User::find(Auth::user()->id);
            $user->id_card = $id_card;
            $user->save();

            $property_unit = PropertyUnit::where('property_id',$property_id)->where('unit_number',$unit_number)->first();
            if(isset($property_unit)){
                $user_property_count = UserProperty::where('user_id',Auth::user()->id)
                    ->where('property_id',$property_unit->property_id)
                    ->where('property_unit_id',$property_unit->id)
                    ->count();
                if($user_property_count == 0) {
                    $user_property = new UserProperty();
                    $user_property->user_id = Auth::user()->id;
                    $user_property->property_id = $property_unit->property_id;
                    $user_property->property_unit_id = $property_unit->id;
                    $user_property->approve_status = 0;
                    $user_property->save();

                    //$textNoti = $this->convertTitleTolongTxt($notification);

                    $dataPusher = [
                        //'title' => $textNoti,
                        'title' => "aaa",
                        'notification' => "bbb"
                    ];

                    //Pusher::trigger(Auth::user()->property_id . "_" . $user->id, 'notification_event', $dataPusher);

                    // push notification to owner
                    if($user->profile_pic_name != null) {
                        $user_img_url = env('URL_S3')."profile-img/".$user->profile_pic_path.$user->profile_pic_name;
                    }else{
                        $user_img_url = "";
                    }
                    $this->sendCreateNotification ($user_property->id,$user_property->property_id,$user_property->property_unit_id,$user->name, $user_img_url);
                }else{
                    // nothing to do
                }
                $results = [
                    'status' => true,
                    'message' => "success"
                ];
            }else{
                $results = [
                    'status' => false,
                    'message' => "not have unit number in property"
                ];
            }
            return response()->json($results);

        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function profileUpdate(){
        try{
            if (Request::isMethod('post')) {
                $firstname = Request::get('first_name');
                $lastname = Request::get('last_name');
                $name = $firstname." ".$lastname;
                $user = User::find(Auth::user()->id);
                $user->name = $name;
                $user->save();

                if($user->customer_id != null){
                    $customer = Customer::find($user->customer_id);
                    if(isset($customer)) {
                        $customer->name = $name;
                        $customer->save();
                    }
                }

                if($user->tenant_id != null){
                    $tenant = Tenant::find($user->tenant_id);
                    if(isset($tenant)){
                        $tenant->name = $name;
                        $tenant->save();
                    }
                }

                $results = [
                    'status' => true,
                    'message' => "success"
                ];
            }else{
                $results = [
                    'status' => false,
                    'message' => "wrong method"
                ];
            }

            return response()->json($results);
        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function phoneUpdate(){
        try{
            if (Request::isMethod('post')) {
                $phone = Request::get('phone');
                $user = User::find(Auth::user()->id);
                $user->phone = $phone;
                $user->save();

                if($user->customer_id != null){
                    $customer = Customer::find($user->customer_id);
                    if(isset($customer)) {
                        $customer->phone = $phone;
                        $customer->save();
                    }
                }

                if($user->tenant_id != null){
                    $tenant = Tenant::find($user->tenant_id);
                    if(isset($tenant)){
                        $tenant->phone = $phone;
                        $tenant->save();
                    }
                }

                $results = [
                    'status' => true,
                    'message' => "success"
                ];
            }else{
                $results = [
                    'status' => false,
                    'message' => "wrong method"
                ];
            }

            return response()->json($results);
        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }



    public function profileImageUpload(){
        try {
            $user = User::find(Auth::user()->id);
            if(!empty(Request::file('pic_profile'))) {
                $img = Request::file('pic_profile');
                if(!empty($user->profile_pic_name)) {
                    $this->removeFile($user->profile_pic_name);
                }

                $name =  md5($img->getFilename());//getClientOriginalName();
                $extension = $img->getClientOriginalExtension();
                $targetName = $name.".".$extension;

                $path = $this->createLoadBalanceDir(Request::file('pic_profile'));
                $user->profile_pic_name = $targetName;
                $user->profile_pic_path = $path;
            }
            $user->save();

            $results = [
                'status' => true,
                'message' => "success"
            ];

            return response()->json($results);

        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Can not import file"
            ];

            return response()->json($results);
        }
    }

    public function profileView(){
        try{
            $user = User::find(Auth::user()->id);
            $user_data = $user->toArray();
            if($user->name != null) {
                $strng = trim(preg_replace('/\s+/',' ', $user->name));
                $name_array = explode(" ", $strng);
                $firstname = "";
                $lastname = "";

                foreach ($name_array as $i => $item){
                    if($i == 0){
                        $firstname .= $item;
                    }else{
                        $lastname .= $item." ";
                    }
                }

                $user_data['name'];
                $user_data['first_name'] = $firstname;
                $user_data['last_name'] = trim($lastname);
            }else{
                $user_data['name'];
                $user_data['first_name'] = "";
                $user_data['last_name'] = "";
            }

            if($user_data['phone'] == null){
                $user_data['phone_masking'] = "-";
            }else{
                $user_data['phone_masking'] = $this->maskingIdPhone($user_data['phone']);
            }

            if($user_data['id_card'] == null){
                $user_data['id_card_masking'] = "-";
            }else{
                $user_data['id_card_masking'] = $this->maskingIdCard($user_data['id_card']);
            }

            if($user_data['profile_pic_name'] != null) {
                $user_data['profile_img_full_path'] = env('URL_S3') . "/profile-img/" . $user_data['profile_pic_path'] . $user_data['profile_pic_name'];
            }else{
                $user_data['profile_img_full_path'] = "";
            }

            $property_list = UserProperty::select('property_unit_id','property_id')->where('user_id',Auth::user()->id)->get();

            $property_list_data = UserProperty::with('property')->with('unit')->where('user_id',Auth::user()->id)->get();

            $array_property_list = [];
            foreach ($property_list as $item){
                $array_property_list[] = $item->property_unit_id;
                $user_data['property_id'] = $item->property_id;
                $user_data['property_unit_id'] = $item->property_unit_id;
            }
            $property_unit = PropertyUnit::with('property')->whereIn('id',$array_property_list)->get();

            foreach ($property_list_data as &$item_property){
                $item_property->property->brand_name_en = $item_property->property->has_brand->name_en;
                $item_property->property->brand_name_th = $item_property->property->has_brand->name_th;
                unset($item_property->property->has_brand);
                $item_property->property->banner_url_full_path = env('URL_S3')."/post-file".$item_property->property->banner_url;
                if($item_property->user_type == "0"){
                    $item_property->user_type_tag = 'Owner';
                }elseif($item_property->user_type == "1"){
                    $item_property->user_type_tag = 'Resident';
                }elseif($item_property->user_type == "2"){
                    $item_property->user_type_tag = 'Renter';
                }else{
                    $item_property->user_type_tag = 'Unknown';
                }

            }
            $user_data['user_property_data'] = $property_list_data;

            $results = [
                'status' => true,
                'message' => "success",
                'data' => $user_data
            ];

            return response()->json($results);
        }catch (Exception $ex) {
            $results = [
                'status' => false,
                'message' => "Unsuccessful"
            ];

            return response()->json($results);
        }
    }

    public function createLoadBalanceDir ($imageFile) {
        $name =  md5($imageFile->getFilename());//getClientOriginalName();
        $extension = $imageFile->getClientOriginalExtension();
        $targetName = $name.".".$extension;

        $folder = substr($name, 0,2);

        $pic_folder = 'profile-img'.DIRECTORY_SEPARATOR.$folder;
        $directories = Storage::disk('s3')->directories('profile-img'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }

        $full_path_upload = $pic_folder.DIRECTORY_SEPARATOR.$targetName;
        $upload = Storage::disk('s3')->put($full_path_upload, file_get_contents($imageFile), 'public');// public set in photo upload
        if($upload){
            // Success
        }

        return $folder."/";
    }

    public function removeFile ($name) {
        $folder = substr($name, 0,2);
        $file_path = 'profile-img'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$name;
        $exists = Storage::disk('s3')->has($file_path);
        if ($exists) {
            Storage::disk('s3')->delete($file_path);
        }
    }

    public function language () {
        try {
            $user = User::find(Auth::user()->id);
            if (Request::isMethod('post')) {
                $user->lang = Request::get('lang');
                $user->save();

                $installation = Installation::where('user_id',Auth::user()->id)->get();
                foreach ($installation as $item){
                    $item->lang = Request::get('lang');
                    $item->save();
                }

                Auth::loginUsingId($user->id);
            }
            return response()->json(['status'=>true]);
        }catch(Exception $ex) {
            return response()->json(['status'=>false]);
        }
    }

    public function notificationSetting () {
        try {
            $user = User::find(Auth::user()->id);
            if (Request::isMethod('post')) {
                if(Request::get('notification') == "true"){
                    $notification_status = true;
                }else{
                    $notification_status = false;
                }
                $user->notification = $notification_status;
                $user->save();
            }
            return response()->json(['status'=>true]);
        }catch(Exception $ex) {
            return response()->json(['status'=>false]);
        }
    }

    public function sendPusher(){
        $asdad = "";
        $dataPusher = [
            //'title' => $textNoti,
            'title' => "Request Add Property",
            'notification' => "bbb"
        ];

        $app_id = env('PUSHER_APP_ID');
        $app_key = env('PUSHER_KEY');
        $app_secret = env('PUSHER_SECRET');
        $app_cluster = env('PUSHER_APP_CLUSTER');

        $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

        $pusher->trigger("1518183e-8e96-463d-83f3-bd8c876df1e1_1da3aa1a-4c02-4925-b76d-145e07c31677", 'notification_event', $dataPusher);
        return "true";
    }

    public function sendPusherRequestAddProperty($property_id){
        $dataPusher = [
            //'title' => $textNoti,
            'title' => 'มีคำขอเข้าใช้งานใหม่',
            'notification' => ""
        ];

        $app_id = env('PUSHER_APP_ID');
        $app_key = env('PUSHER_KEY');
        $app_secret = env('PUSHER_SECRET');
        $app_cluster = env('PUSHER_APP_CLUSTER');

        $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );

        $pusher->trigger($property_id, 'notification_event', $dataPusher);
        return "true";
    }

    public function sendCreateNotification ($subject_key,$property_id,$unit_id,$name,$img_url) {
        $title = json_encode( ['type' => 'request_new_resident','name'=>$name,'img_url'=>$img_url] );
        $user_property = UserProperty::where('property_unit_id', $unit_id)->where('approve_status',1)->where('user_type',0)->get();
        //$users = User::where('property_unit_id',$unit_id)->whereNull('verification_code')->get();
        foreach ($user_property as $user) {
            $notification = Notification::create([
                'title'				=> $title,
                'notification_type' => '3',
                'subject_key'		=> $subject_key,
                'from_user_id'		=> Auth::user()->id,
                'to_user_id'		=> $user->user_id,
                'property_id'		=> $property_id,
                'property_unit_id'		=> $unit_id,
            ]);
            $controller_push_noti = new PushNotificationController();
            $controller_push_noti->pushNotification($notification->id);
        }

        // Notification to Admin
        $user_admin = User::where('role',3)->where('property_id',$property_id)->get();
        foreach ($user_admin as $user) {
            $notification = Notification::create([
                'title'				=> $title,
                'notification_type' => '3',
                'subject_key'		=> $subject_key,
                'from_user_id'		=> Auth::user()->id,
                'to_user_id'		=> $user->id,
                'property_id'		=> $property_id,
                'property_unit_id'		=> $unit_id,
            ]);
            $controller_push_noti = new PushNotificationController();
            $controller_push_noti->pushNotification($notification->id);
        }
        $this->sendPusherRequestAddProperty($property_id);
        /*$app_id = env('PUSHER_APP_ID');
        $app_key = env('PUSHER_KEY');
        $app_secret = env('PUSHER_SECRET');
        $app_cluster = env('PUSHER_APP_CLUSTER');
        $dataPusher = [
            'title' => "New Request Add Property",
            'notification' => ""
        ];
        $pusher = new Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );
        $pusher->trigger("81862267-1990-45b3-9873-e9f1bc47f5d9", 'notification_event', $dataPusher);*/
    }

    function maskingIdCard($id_card){
        $lenght_id_card_mask = strlen($id_card) - 4;
        $i = 0;
        $string_mask = "";
        while ($i < $lenght_id_card_mask) {
            $string_mask .= "*";
            $i++;
        }

        $id_card_last_3 = substr($id_card, -4);

        return $string_mask.$id_card_last_3;
    }

    function maskingIdPhone($phone){
        $length_phone_mask = strlen($phone) - 4;
        $i = 0;
        $string_mask = "";
        while ($i < $length_phone_mask) {
            $string_mask .= "*";
            $i++;
        }

        $id_card_last_3 = substr($phone, -4);

        return $string_mask.$id_card_last_3;
    }
}
