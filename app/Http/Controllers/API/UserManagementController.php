<?php namespace App\Http\Controllers\API;
use GuzzleHttp\Exception\RequestException;
use Request;
use JWTAuth;
use Auth;
use DB;
use Mail;
use Hash;
use Storage;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use LaravelFCM\Response\Exceptions\ServerResponseException as FCMException;

use Carbon\Carbon;
use App\Property;
use App\Post;
use App\PostFile;
use App\PostProperty;
use App\Installation;
use App\Customer;
use App\User;
use App\UserProperty;
use App\CustomerProperty;
use App\PropertyUnit;
use App\Tenant;
use App\TenantProperty;


class UserManagementController extends Controller {

    public function __construct () {

    }

    public function listWaitingApproveUser(){
        try {
            if (Request::isMethod('post')) {
                $user_property = UserProperty::where('user_id',Auth::user()->id)
                    ->where('property_id',Request::get('property_id'))
                    ->where('property_unit_id',Request::get('property_unit_id'))
                    ->first();

                $all_waiting = [];
                if(isset($user_property) && $user_property->user_type == 0){
                    // Resident
                    $user_property_waiting = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                        ->where('property_unit_id',Request::get('property_unit_id'))
                        ->where('approve_status',0)
                        ->get();

                    $array_user_property_waiting = [];
                    foreach ($user_property_waiting as $item){
                        $array_user_property_waiting[] = $item->user_id;
                    }

                    $user_waiting_list = User::whereIn('id',$array_user_property_waiting)->get();
                    $user_waiting_list_arr = $user_waiting_list->toArray();

                    foreach ($user_waiting_list_arr as &$item){
                        if($item['profile_pic_name'] != null) {
                            $item['profile_img_full_path'] = env('URL_S3') . "/profile-img/" . $item['profile_pic_path'] . $item['profile_pic_name'];
                        }else{
                            $item['profile_img_full_path'] = "";
                        }
                        $item['user_type_tag'] = 'Unknown';

                        if($item['id_card'] == null){
                            $item['id_card_masking'] = "-";
                        }else{
                            $item['id_card_masking'] = $this->maskingIdCard($item['id_card']);
                        }

                        if($item['phone'] == null){
                            $item['phone_masking'] = "-";
                        }else{
                            $item['phone_masking'] = $this->maskingIdPhone($item['phone']);
                        }

                        $item['property_id'] = Request::get('property_id');
                        $item['property_unit_id'] = Request::get('property_unit_id');
                    }

                    /*foreach ($user_waiting_list as &$item){
                        $item['user_type_tag'] = 'Resident';
                    }

                    // Tenant
                    $tenant_property_waiting = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                        ->where('property_unit_id',Request::get('property_id'))
                        ->where('approve_status',0)
                        ->where('user_type',2)
                        ->get();

                    $array_tenant_property_waiting = [];
                    foreach ($tenant_property_waiting as $item){
                        $array_tenant_property_waiting[] = $item->post_id;
                    }

                    $tenant_waiting_list = User::whereIn('id',$array_tenant_property_waiting)->get();
                    foreach ($tenant_waiting_list as &$item){
                        $item['user_type_tag'] = 'Renter';
                    }

                    $all_waiting = array_merge($user_waiting_list,$tenant_waiting_list);*/
                }


                $results = [
                    'status' => true,
                    'message' => 'success',
                    'data' => $user_waiting_list_arr
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
            ];

            return response()->json($results);
        }
    }

    public function listResidentUser(){
        try {
            if (Request::isMethod('post')) {
                $user_property = UserProperty::where('user_id',Auth::user()->id)
                    ->where('property_id',Request::get('property_id'))
                    ->where('property_unit_id',Request::get('property_unit_id'))
                    ->first();

                $all_waiting = [];
                if(isset($user_property) && $user_property->user_type == 0){
                    // Owner
                    $owner_property = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                        ->where('property_unit_id',Request::get('property_unit_id'))
                        ->where('approve_status',1)
                        ->where('user_type',0)
                        ->get();

                    $array_owner_property = [];
                    foreach ($owner_property as $item){
                        $array_owner_property[] = $item->user_id;
                    }

                    if(count($array_owner_property) >0) {
                        $user_owner_list = User::whereIn('id', $array_owner_property)->get();
                        foreach ($user_owner_list as &$item) {
                            $item['user_type_tag'] = 'Owner';
                        }
                        $user_owner_list_arr = $user_owner_list->toArray();
                        $all_waiting = array_merge($all_waiting, $user_owner_list_arr);
                    }

                    // Resident
                    $user_property = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                        ->where('property_unit_id',Request::get('property_unit_id'))
                        ->where('approve_status',1)
                        ->where('user_type',1)
                        ->get();

                    $array_user_property = [];
                    foreach ($user_property as $item){
                        $array_user_property[] = $item->user_id;
                    }

                    if(count($array_user_property) >0) {
                        $user_resident_list = User::whereIn('id', $array_user_property)->get();
                        foreach ($user_resident_list as &$item) {
                            $item['user_type_tag'] = 'Resident';
                        }
                        $user_waiting_list_arr = $user_resident_list->toArray();
                        $all_waiting = array_merge($all_waiting, $user_waiting_list_arr);
                    }

                    // Tenant
                    $tenant_property_waiting = UserProperty::select('user_id')->where('property_id',Request::get('property_id'))
                        ->where('property_unit_id',Request::get('property_unit_id'))
                        ->where('approve_status',1)
                        ->where('user_type',2)
                        ->get();

                    $array_tenant_property_waiting = [];
                    foreach ($tenant_property_waiting as $item){
                        $array_tenant_property_waiting[] = $item->user_id;
                    }

                    if(count($array_tenant_property_waiting) > 0) {
                        $tenant_waiting_list = User::whereIn('id', $array_tenant_property_waiting)->get();
                        foreach ($tenant_waiting_list as &$item) {
                            $item['user_type_tag'] = 'Renter';
                        }
                        $tenant_waiting_list_arr = $tenant_waiting_list->toArray();
                        $all_waiting = array_merge($all_waiting, $tenant_waiting_list_arr);
                    }

                    foreach ($all_waiting as &$item){
                        if($item['profile_pic_name'] != null) {
                            $item['profile_img_full_path'] = env('URL_S3') . "/profile-img/" . $item['profile_pic_path'] . $item['profile_pic_name'];
                        }else{
                            $item['profile_img_full_path'] = "";
                        }

                        if($item['id_card'] == null){
                            $item['id_card_masking'] = "-";
                        }else{
                            $item['id_card_masking'] = $this->maskingIdCard($item['id_card']);
                        }

                        if($item['phone'] == null){
                            $item['phone_masking'] = "-";
                        }else{
                            $item['phone_masking'] = $this->maskingIdPhone($item['phone']);
                        }
                    }
                }

                $results = [
                    'status' => true,
                    'message' => 'success',
                    'data' => $all_waiting
                ];

                return response()->json($results);
            }else{
                $results = [
                    'status' => false,
                    'message' => 'Wrong Method',
                    'data' => []
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful',
                'data' => []
            ];

            return response()->json($results);
        }
    }

    public function approveResident(){
        try {
            if (Request::isMethod('post')) {
                $property_id = Request::get('property_id');
                $property_unit_id = Request::get('property_unit_id');
                $user_id = Request::get('user_id');

                $user_property_approve = UserProperty::where('property_id',$property_id)
                    ->where('property_unit_id',$property_unit_id)
                    ->where('user_id',$user_id)
                    ->first();

                $user_property_approve->user_type = Request::get('user_type');
                $user_property_approve->approve_status = Request::get('approve_status') == "1" ? 1 : 2;
                $user_property_approve->save();

                if(Request::get('user_type') == "0" || Request::get('user_type') == "1" || Request::get('user_type') == "2"){
                    $user_property_approve->load('ofUser');
                    if( $user_property_approve->ofUser ) {
                        $id_card = $user_property_approve->ofUser->id_card;
                        if( $id_card ) {
                            if( $user_property_approve->user_type == 2) {
                                // check tenant data
                                $this->checkTenant($id_card, $user_property_approve);
                            } else {
                                // check customer
                                $this->checkCustomer($id_card, $user_property_approve);
                            }
                        }
                    }
                }

                $results = [
                    'status' => true,
                    'message' => 'success'
                ];

                return response()->json($results);
            }else{
                $results = [
                    'status' => false,
                    'message' => 'Wrong Method'
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful'
            ];

            return response()->json($results);
        }
    }

    public function addResidentUser(){

    }

    public function setActiveResidentUser(){
        try {
            if (Request::isMethod('post')) {
                $update_user_property = UserProperty::where('user_id',Request::get('user_id'))
                    ->where('property_id',Request::get('property_id'))
                    ->where('property_unit_id',Request::get('property_unit_id'))
                    ->first();

                if(isset($update_user_property)){
                    $flag_active = Request::get('is_active') == 'true' ? true : false;
                    $update_user_property->active_status = $flag_active;
                    $update_user_property->save();

                    $results = [
                        'status' => true,
                        'message' => 'Successful'
                    ];
                }else{
                    $results = [
                        'status' => false,
                        'message' => 'User not found'
                    ];
                }

                return response()->json($results);
            }else{
                $results = [
                    'status' => false,
                    'message' => 'Wrong Method'
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful'
            ];

            return response()->json($results);
        }
    }

    public function expireDateResidentUser(){
        try {
            if (Request::isMethod('post')) {
                $update_user_property = UserProperty::where('user_id',Request::get('user_id'))
                    ->where('property_id',Request::get('property_id'))
                    ->where('property_unit_id',Request::get('property_unit_id'))
                    ->first();

                if(isset($update_user_property)){
                    $update_user_property->expire_date = Request::get('expire_date');
                    $update_user_property->save();
                }

                $results = [
                    'status' => true,
                    'message' => 'Successful'
                ];

                return response()->json($results);
            }else{
                $results = [
                    'status' => false,
                    'message' => 'Wrong Method'
                ];

                return response()->json($results);
            }
        }catch(Exception $ex) {
            $results = [
                'status' => false,
                'message' => 'Unsuccessful'
            ];

            return response()->json($results);
        }
    }

    function maskingIdCard($id_card){
        $lenght_id_card_mask = strlen($id_card) - 4;
        $i = 0;
        $string_mask = "";
        while ($i < $lenght_id_card_mask) {
            $string_mask .= "*";
            $i++;
        }

        $id_card_last_3 = substr($id_card, -4);

        return $string_mask.$id_card_last_3;
    }

    function maskingIdPhone($phone){
        $length_phone_mask = strlen($phone) - 4;
        $i = 0;
        $string_mask = "";
        while ($i < $length_phone_mask) {
            $string_mask .= "*";
            $i++;
        }

        $id_card_last_3 = substr($phone, -4);

        return $string_mask.$id_card_last_3;
    }

    public function checkTenant ($id_card, $user_property ) {
        $user = $user_property->ofUser;
        $dup_tenant = Tenant::where('id_card',$id_card)->first();
        if( !$dup_tenant ) {
            // create customer
            $tenant = $this->addTenant($user);
            // add tenant property
        } else {
            $tenant = $dup_tenant;
            // do something with user property may be grant active status to all
        }

        if( !$user->tenant_id ) {
            $user->tenant_id = $tenant->id;
            $user->save();
        }

        // check duplicated tenant property
        $dup_cp = TenantProperty::where('tenant_id',$tenant->id)
            ->where('property_unit_id',$user_property->property_unit_id)
            ->first();
        if( !$dup_cp ) {
            // add tenant property
            $c_p = new TenantProperty;
            $c_p->tenant_id       = $tenant->id;
            $c_p->property_id       = $user_property->property_id;
            $c_p->property_unit_id  = $user_property->property_unit_id;
            //$c_p->customer_type     = $user_property->user_type;
            $c_p->save();
        }

        return true;
    }

    public function addTenant ($user) {
        $t_data = new Tenant;
        $t_data->prefix_name        = "";
        $t_data->name               = $user->name;
        $t_data->id_card            = $user->id_card;
        $t_data->type_id_card       = $user->type_id_card;
        $t_data->phone              = $user->phone;
        $t_data->email              = $user->email;
        $t_data->person_type        = 0;
        //$t_data->nationality        = ;
        //$t_data->job                = empty($customer['n'])?NULL:$customer['n'];
        //$t_data->dob                = empty($customer['o'])?NULL:$customer['o'];
        //$t_data->customer_code      = empty($customer['p'])?NULL:$customer['p'];
        $t_data->save();

        // update customer id in user
        $user->timestamps = false;
        $user->tenant_id = $t_data->id;
        $user->save();

        return $t_data;
    }

    public function checkCustomer ($id_card, $user_property ) {
        $user = $user_property->ofUser;
        $dup_customer = Customer::where('id_card',$id_card)->first();
        if( !$dup_customer ) {
            // create customer
            $customer = $this->addCustomer($user);
            // add customer property
        } else {
            $customer = $dup_customer;
            // do something with user property may be grant active status to all
        }
        if( !$user->customer_id ) {
            $user->customer_id = $customer->id;
            $user->save();
        }

        // check duplicated tenant property
        $dup_cp = CustomerProperty::where('customer_id',$customer->id)
            ->where('property_unit_id',$user_property->property_unit_id)
            ->first();
        if( !$dup_cp ) {
            // add customer property
            $c_p = new CustomerProperty;
            $c_p->customer_id       = $customer->id;
            $c_p->property_id       = $user_property->property_id;
            $c_p->property_unit_id  = $user_property->property_unit_id;
            $c_p->customer_type     = $user_property->user_type;
            $c_p->save();
        }

        return true;
    }

    public function addCustomer ($user) {
        $c_data = new Customer;
        $c_data->prefix_name        = "";
        $c_data->name               = $user->name;
        $c_data->id_card            = $user->id_card;
        $c_data->type_id_card       = $user->type_id_card;
        $c_data->phone              = $user->phone;
        $c_data->email              = $user->email;
        $c_data->person_type        = 0;
        $c_data->save();

        // update customer id in user
        $user->timestamps = false;
        $user->customer_id = $c_data->id;
        $user->save();

        return $c_data;
    }

}
