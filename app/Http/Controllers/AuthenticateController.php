<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rules\In;
use JWTAuth;
use Auth;
use DB;
use DateTime;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTFactory;
use Carbon\Carbon;
use App\Installation;
use App\User;
use App\TimeoutSetting;

class AuthenticateController extends Controller
{
    protected $user;

    public function __construct(JWTAuth $auth)
    {
        //$this->user = $auth->toUser();
    }
    public function index()
    {
        // TODO: show users
    }

    public function authenticate(Request $request)
    {
        $timeout_setting = TimeoutSetting::first();
        $timeout_setting_hour = $timeout_setting->timeout;
        $device_token = $request->get('device_token');
        $device_type = $request->get('device_type');
        $device_uuid = $request->get('device_uuid');
        $email = strtolower($request->get('email'));
        $password = $request->get('password');

        $isFirst = false;
        //$credentials = $request->only('email','password');

        $user = User::where('email','=',$email)->first();
        // Verify code in First time
        /*if(isset($user) && $user->verification_code != null){
            if($user->verification_code == $password){
                $isFirst = true;
                $user->verification_code = null;
                $user->password = bcrypt($password);
                $user->save();
            }
        }*/

        $credentials = array(
            "email" => $email,
            "password" => $password
        );

        if($request->get('remember_me') == "true"){
            $timestamp = Carbon::now()->addYear(5)->timestamp; // Session 5 years on mobile (Remember me)
            $customClaims = ['exp' => $timestamp];
        }else{
            //$timestamp = Carbon::now()->addDays(7)->timestamp;
            $timestamp = Carbon::now()->addYear()->timestamp; // Session 1 years on mobile
            $customClaims = ['exp' => $timestamp];
        }

        //$user = User::where('email','=',$request->get('email'))->first();


        if(isset($user) && $user->role == 1){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->role == 3){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->role == 4){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->role == 5){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->active == false){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        try {
            // verify the credentials and create a token for the user
            //if (! $token = JWTAuth::attempt($credentials,$customClaims)) {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT

        $device_status = $this->addDeviceInstallation($device_token,$device_uuid,$device_type);

        $token = JWTAuth::claims(['device' => $device_uuid,'os'=>$device_type,'exp' => Carbon::now()->addHours($timeout_setting_hour)->timestamp])->attempt($credentials);

        if($device_status) {
            $results = [
                'token' => $token,
                'isFirst' => $isFirst
            ];
            return response()->json($results);
        }else{
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
    }

    public function authenticateByPasscode(Request $request)
    {
        $timeout_setting = TimeoutSetting::first();
        $timeout_setting_hour = $timeout_setting->timeout;

        $device_uuid = $request->get('device_uuid');
        $passcode = $request->get('passcode');

        $installation = Installation::where('device_uuid',$device_uuid)->where('passcode',$passcode)->first();

        if(isset($installation)) {
            $user_id = $installation->user_id;

            $user = User::find($user_id);

            try {
                if ($user) {
                    // Get the token
                    $token = JWTAuth::claims(['exp' => Carbon::now()->addHours($timeout_setting_hour)->timestamp])->fromUser($user);
                    if (!$token) {
                        return response()->json(['error' => 'invalid_credentials'], 401);
                    }
                } else {
                    return response()->json(['error' => 'user_not_found'], 403);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
        }else{
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
        // if no errors are encountered we can return a JWT

        if($token) {
            $results = [
                'token' => $token,
                'isFirst' => false
            ];
            return response()->json($results);
        }else{
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
    }

    public function forgetPasscode(Request $request){
        $timeout_setting = TimeoutSetting::first();
        $timeout_setting_hour = $timeout_setting->timeout;

        $email = strtolower($request->get('email'));
        $password = $request->get('password');

        $credentials = array(
            "email" => $email,
            "password" => $password
        );
        $user = User::where('email','=',$email)->first();

        if(isset($user) && $user->role == 1){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->role == 3){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->role == 4){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->role == 5){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if(isset($user) && $user->active == false){
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        try {
            // verify the credentials and create a token for the user
            //if (! $token = JWTAuth::attempt($credentials,$customClaims)) {
            if (! $token = JWTAuth::claims(['exp' => Carbon::now()->addHours($timeout_setting_hour)->timestamp])->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if($token) {
            $results = [
                'status' => true,
                'message' => "success",
                'token' => $token
            ];
            return response()->json($results);
        }else{
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
    }

    public function authenticateMdm(Request $request){
        $username = strtolower($request->get('username'));
        $password = $request->get('password');
        $ap_api_key = $request->get('api_key');

        if($ap_api_key == env('AP_API_KEY',"")){
            $is_valid_key = true;
        }else{
            $is_valid_key = false;
        }

        $credentials = array(
            "email" => $username,
            "password" => $password
        );

        try {
            // verify the credentials and create a token for the user
            //if (! $token = JWTAuth::attempt($credentials,$customClaims)) {
            if($is_valid_key) {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                } else {
                    $results = [
                        'token' => $token
                    ];
                    return response()->json($results);
                }
            }else{
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
    }

    public function logoutApi(Request $request) {
        try {
            JWTAuth::parseToken()->invalidate();
            
            return response()->json(['success'=>'true']);
        } catch(JWTException $e){
            return response()->json(['success'=>'false']);
        }
    }

    /*public function addDeviceInstallation ($device_token,$device_uuid,$device_type) {
        try {
            $countDevice = Installation::where('device_uuid', $device_uuid)->where('user_id',Auth::user()->id)->count();
            if ($countDevice > 0) {

            }else{
                $this->deleteDeviceInstallationByTokenId($device_token);
            }

            $device = Installation::firstOrNew(
                array(
                    'user_id' => Auth::user()->id,
                    'device_token' => $device_token,
                    'device_type' => $device_type,
                    'device_uuid' => $device_uuid,
                )
            );
            $device->save();

            return true;

        } catch(Exception $ex){
            return false;
        }
    }*/

    public function addDeviceInstallation ($device_token,$device_uuid,$device_type) {
        try {
            $user_installation_data = Installation::where('device_token',$device_token)->where('user_id',Auth::user()->id)->count();
            if($user_installation_data > 0){
                // old user use old device login again
                // nothing to do
            }else{
                $user_installation_count = Installation::where('device_token',$device_token)->count();
                if($user_installation_count > 0) {
                    // new user use OLD device login
                    $this->deleteDeviceInstallationByTokenId($device_token);
                    $device = Installation::firstOrNew(
                        array(
                            'user_id' => Auth::user()->id,
                            'device_token' => $device_token,
                            'device_type' => $device_type,
                            'device_uuid' => $device_uuid,
                        )
                    );
                    $device->save();
                }else{
                    // new user use NEW device login
                    $device = Installation::firstOrNew(
                        array(
                            'user_id' => Auth::user()->id,
                            'device_token' => $device_token,
                            'device_type' => $device_type,
                            'device_uuid' => $device_uuid,
                        )
                    );
                    $device->save();
                }
            }

            return true;

        } catch(Exception $ex){
            return false;
        }
    }

    public function deleteDeviceInstallationByTokenId ($device_token) {
        try {
            $device = Installation::where('device_token', $device_token);
            if (isset($device)) {
                $device->delete();
                return true;
            }else{
                return false;
            }
        } catch(Exception $ex){
            return false;
        }
    }

    public function deleteDeviceInstallationByUuid ($device_uuid) {
        try {
            $device = Installation::where('device_uuid', $device_uuid);
            if (isset($device)) {
                $device->delete();
                return true;
            }else{
                return false;
            }
        } catch(Exception $ex){
            return false;
        }
    }

    public function deleteDeviceInstallation ($device_token,$device_uuid) {
        try {
            $device = Installation::where('device_token', $device_token)->where('device_uuid',$device_uuid);

            if ($device->count() > 0) {
                $device->delete();
                return true;
            }else{
                return false;
            }
        } catch(Exception $ex){
            return false;
        }
    }
}
