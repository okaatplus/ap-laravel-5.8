<?php namespace App\Http\Controllers\CallCenter;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Pagination\Paginator;
use Auth;
use Validator;
# Model
use App;
use DB;
use Excel;
use App\PropertyMember;
use App\UserProperty;
use App\PropertyUnit;
use App\User;
use App\Province;
use App\Rules\notZero;

class PropertyMemberController extends Controller {

	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu','members');
	}

	public function memberlist () {
		$members 		= UserProperty::with('ofUser')
                            ->where('property_id',Auth::user()->property_id)
                            ->where('approve_status',1)
							->orderBy('created_at','DESC')
							->paginate(30);
		$unit_list = array(''=> trans('messages.unit_no') );
		$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
		$member = new PropertyMember;
		return view('call-center.property_members.view')->with(compact('members','unit_list','member'));
	}

	public function memberlistPage () {
		if(Request::ajax()) {
			$members 	= UserProperty::with('ofUser')
							->where('property_id',Auth::user()->property_id)
                            ->where('approve_status',1);

			if(Request::get('type') == 'cm') {
				$members->where('is_chief',true);
			} else if(Request::get('type') == 'gm') {
				$members->where('is_chief',false);
			}

			if(Request::get('active') != '-') {
				$members->where('active',intval(Request::get('active')));
			}

			if(Request::get('unit_id')) {
				$members->where('property_unit_id',Request::get('unit_id'));
			}

			if(Request::get('name')) {
				$members->where('name','like',"%".Request::get('name')."%");
			}

			$members = $members->orderBy('created_at','DESC')->paginate(30);
			return view('call-center.property_members.member-list')->with(compact('members'));
		}
	}


	public function setActive () {
		if(Request::ajax()) {
			$up = UserProperty::find(Request::get('uid'));
			if($up) {
                $up->active_status = Request::get('status');
                $up->save();
				return response()->json(['result'=>true]);
			}
		}
	}

	public function getMember () {
		if(Request::ajax()) {
		    $member = UserProperty::with('ofUser')->find(Request::get('uid'));
            $member = $member->ofUser;
			return view('call-center.property_members.view-member')->with(compact('member'));
		}
	}

    public function editMember () {

        if(Request::ajax()) {
            $up = UserProperty::with('ofUser')->find(Request::get('uid'));
            $member = $up->ofUser;
            return view('call-center.property_members.edit-member')->with(compact('member','up'));
        } else {
            if( Request::isMethod('post')) {

                $member         = User::find(Request::get('id'));
                $member->phone  = Request::get('phone');
                $member->save();
                return redirect('admin/property/members');
            }
        }
    }

	public function newMembers() {
		$p = new Province;
		$provinces = $p->getProvince();
		$users = UserProperty::with('ofUser')->where('property_id', Auth::user()->property_id)->where('approve_status', 0);
		if(Request::ajax()) {

			if(Request::get('name')) {
				$users->where('name','like',"%".Request::get('name')."%");
			}

			if(Request::get('property_unit_id')) {
				$users->where('property_unit_id', Request::get('property_unit_id'));
			}

			$users = $users->orderBy('created_at','desc')->paginate(30);
			return view('call-center.property_members.new-member-list-page')->with(compact('users','provinces'));
		} else {
            $users = $users->orderBy('created_at','desc')->paginate(30);
			$property_unit_list = array(''=> trans('messages.unit_no') );
			$property_unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
			return view('call-center.property_members.new-member-list')->with(compact('users','property_unit_list','provinces'));
		}
	}

}
