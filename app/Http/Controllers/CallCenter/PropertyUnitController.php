<?php namespace App\Http\Controllers\CallCenter;
use App\Property;
use Maatwebsite\Excel\Exceptions\LaravelExcelException;
use Request;
use Illuminate\Routing\Controller;
use Auth;
use Redirect;
use Storage;
use DB;
use File;
use Excel;
use View;
# Model
use App\PropertyUnit;
use App\User;
class PropertyUnitController extends Controller {

	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu','units');
	}

	public function unitList () {
		$units 		= PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true);

		$unit_list = array(''=> trans('messages.unit_no') );
		$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();

		if(Request::ajax()) {

			if(Request::get('unit_id')) {
				$units->where('id',Request::get('unit_id'));
			}

			$units = $units->orderBy(DB::raw('natsortInt(unit_number)'))->paginate(30);
			return view('call-center.property_units.unit-list-page')->with(compact('units','unit_list'));
		}
		else{
			$units = $units->orderBy(DB::raw('natsortInt(unit_number)'))->paginate(30);
			return view('call-center.property_units.unit-list')->with(compact('units','unit_list'));
		}
	}

	public function getUnit () {
		$unit = PropertyUnit::with(
		    'propertyUnitCustomer','propertyUnitCustomer.customer',
            'propertyUnitTenant','propertyUnitTenant.tenant')->find(Request::get('unit_id'));
		return view('call-center.property_units.get-unit')->with(compact('unit'));
	}

	public function add () {
		$unit = new PropertyUnit;
		$unit->fill(Request::all());

		if(Request::get('transferred_date')){
			$unit->transferred_date = Request::get('transferred_date');
		}else{
			$unit->transferred_date = null;
		}
		if(Request::get('insurance_expire_date')){
			$unit->insurance_expire_date = Request::get('insurance_expire_date');
		}else{
			$unit->insurance_expire_date = null;
		}
		
		$unit->property_id = Auth::user()->property_id;
		$unit->property_size = str_replace(',', "", Request::get('property_size'));
		$unit->extra_cf_charge = str_replace(',', "", Request::get('extra_cf_charge'));
		$unit->property_size = ($unit->property_size>0)?$unit->property_size:0;
		$unit->invite_code = $this->generateInviteCode();
		$unit->save();
		return redirect('admin/property/units');
	}

	public function editForm () {
		$unit = PropertyUnit::find(Request::get('unit_id'));
		return view('call-center.property_units.get-unit-edit-form')->with(compact('unit'));
	}
}
