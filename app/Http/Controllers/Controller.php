<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

#use App\SystemLog;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function systemLog ($user, $section, $type, $subject, $subject_id) {
        /*$log = new SystemLog;
        $log->by_user       = $user->id;
        $log->section       = $section;
        $log->type          = $type;
        $log->subject       = $subject;
        $log->subject_id    = $subject_id;
        $log->save();*/
    }
}
