<?php namespace App\Http\Controllers;
use Request;
use Auth;
use Hash;
use Redirect;
use Mail;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;

# Model
use DB;
use App\User;
use App\Property;
use App\PropertyUnit;
use App\RequestedProperty;
use App\Province;

#Jobs
use App\Jobs\SendEmailUser;

class HomeController extends Controller {

    use DispatchesJobs;

	public function __construct () {
		$this->middleware('redirect-user');
	}

	public function login()
    {
        return view('auth.login');
    }

	public function index() {

			$regis_data = array('flag-new-prop'=>0);
			$prop_list = array(trans('messages.Signup.select_property'));
			$uno_list = array(trans('messages.Signup.select_unit'));
			$props = Property::all();
			//$this->sendMailForgot();
			return view('home.index')->with(compact('prop_list','regis_data','uno_list'));
	}

    public function management() {

        $regis_data = array('flag-new-prop'=>0);
        $prop_list = array(trans('messages.Signup.select_property'));
        $uno_list = array(trans('messages.Signup.select_unit'));
        $props = Property::all();
        return view('home.property-mangement-datail')->with(compact('prop_list','regis_data','uno_list'));
    }

    public function resident() {

        $regis_data = array('flag-new-prop'=>0);
        $prop_list = array(trans('messages.Signup.select_property'));
        $uno_list = array(trans('messages.Signup.select_unit'));
        $props = Property::all();
        return view('home.resident-detail')->with(compact('prop_list','regis_data','uno_list'));
    }

    public function about() {

        $regis_data = array('flag-new-prop'=>0);
        $prop_list = array(trans('messages.Signup.select_property'));
        $uno_list = array(trans('messages.Signup.select_unit'));
        $props = Property::all();
        return view('home.about')->with(compact('prop_list','regis_data','uno_list'));
    }

	public function signup()
	{
		if (Request::isMethod('post')) {
			$regis_data = Request::all();
			$vpu = $vu = false;

			$captcha = $regis_data['g-recaptcha-response'];
        	$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".env('G_CAPCHA_SECRET')."&response=".$captcha);
	        $response = json_decode($response);

			if($ads = ""){
				$response->success == 1;
			}

			$save = false;
			if($regis_data['flag-new-prop'] == 1) { // flag-new-prop : 1=request new property, 0=register
				$new_request = new RequestedProperty();
				$data = $regis_data;
				$data['name'] = $regis_data['user']['fname']." ".$regis_data['user']['lname'];
				$data['email'] = strtolower(trim($regis_data['user']['email']));
				$new_request->rules['province'] = 'required';
                $new_request->rules['gender']   = 'required';
				$vpu = $vu = $new_request->validate($data);

				if($response->success == 1 && $vpu->passes()) {
					//Save to Requested Property;
					$new_p = new RequestedProperty();
	        		$new_p->name = $regis_data['user']['fname']." ".$regis_data['user']['lname'];
	        		$new_p->email = strtolower(trim($regis_data['user']['email']));
	        		$new_p->province = $regis_data['province'];
                    $new_p->property_name = $regis_data['new_property_name'];
	        		$new_p->gender = $regis_data['user']['gender'];
	        		$new_p->save();
					//return view('home.success_request');
					Request::session()->put('success.request', true);
					$save = true;
				}

			} else {
				$new_user = new User();
				unset($new_user->rules['password']);
				unset($new_user->rules['password_confirm']);
				$new_user->rules['property_unit_id'] 	= 'not_empty';
				$new_user->rules['property_id'] 		= 'not_empty';
                $new_user->rules['province'] 			= 'required';
				$new_user->rules['gender'] 			    = 'required';
				$regis_data['user']['province'] 		= $regis_data['province'];
				$regis_data['user']['property_unit_id'] = $regis_data['property_unit_id'];
				$regis_data['user']['property_id'] 		= $regis_data['property_id'];
                $regis_data['user']['email'] = strtolower(trim($regis_data['user']['email']));
				$vu = $new_user->validate($regis_data['user']);
				if($response->success == 1 && $vu->passes()) {
					//save to user
                    $name = trim($regis_data['user']['fname'])." ".trim($regis_data['user']['lname']);
	        		$code = $this->generateCode();
	        		$count = User::where('verification_code', '=', $code)->count();
	        		while($count > 0) {
		        		$code = $this->generateCode();
		        		$count = User::where('verification_code', '=', $code)->count();
		        	}
		          	//Save and send verify code to email
		          	User::create([
			            'name' => $name,
			            'email' => strtolower(trim($regis_data['user']['email'])),
			            'property_id' => $regis_data['property_id'],
			            'property_unit_id' => $regis_data['property_unit_id'],
			            'role' => 2,
			            'verification_code' => $code,
                        'gender' => $regis_data['user']['gender'],
						'is_subscribed_newsletter' => (isset($regis_data['subscribe'])) ? true : false
			        ]);
			        Request::session()->put('success.signup', true);
                    $this->dispatch(new SendEmailUser($name, trans('messages.Email.thanks_signup'),$regis_data['user']['email']));
					$save = true;
				}
			}

	       /// ---- Check Saved if not then return error ----- ///
	        if(!$save) {
				//Get property
		        $prop_list = array(trans('messages.Signup.select_property'));
		        $lang = session()->get('lang');
		        if($regis_data['province'] != 0) {
		        	 $props = Property::where('province','=',$regis_data['province'])->get();
					if(!empty($props)) {
						foreach ($props as $prop) {
							$prop_list[$prop->id] = $prop->{'property_name_'.$lang};
						}
					}
		        }
		        // Get Unit number list
		        //dd($regis_data);
		        $uno_list = array(trans('messages.Signup.select_unit'));
		        if(!empty($regis_data['property_id'])) {
		        	$unos = PropertyUnit::where('property_id', $regis_data['property_id'])->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->get();
					if(!empty($unos)) {
						foreach ($unos as $prop) {
							$uno_list[$prop->id] = $prop->unit_number;
						}
					}
		        }

				if( $response->success != 1) {
					if(empty($capcha)) $message = trans('messages.Signup.no_capcha');
					else $message = trans('messages.Signup.invalid_capcha');
					//Make a fake validate to set error message
					$vcap = Validator::make(array('recaptcha' => '0'), array('recaptcha' => 'email'),array('recaptcha.email' => trans('messages.Signup.invalid_capcha')));
					return redirect()->back()->withInput()
							->withErrors($vpu)
							->withErrors($vu,'user')
							->withErrors($vcap,'capcha')->with(compact('prop_list','regis_data','uno_list'));
				} else {
					return redirect()->back()->withInput()
							->withErrors($vpu)
							->withErrors($vu,'user')->with(compact('prop_list','regis_data','uno_list'));
				}
			}
		}

		//on innitial page
		$regis_data = array('flag-new-prop'=>0);
		$prop_list = array(trans('messages.Signup.select_property'));
		$uno_list = array(trans('messages.Signup.select_unit'));

		$p = new Province;
		$provinces = $p->getProvince();

		$props = Property::all();
		return view('home.signup')->with(compact('prop_list','regis_data','uno_list','provinces'));
	}

	public function signupByInviteCode()
	{
		if (Request::isMethod('post')) {
			$regis_data = Request::all();
			$vpu = $vu = false;

			$message = "";

			$captcha = $regis_data['g-recaptcha-response'];
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".env('G_CAPCHA_SECRET')."&response=".$captcha);
			$response = json_decode($response);

			$save = false;

			$new_user = new User();
			//unset($new_user->rules['password']);
			$new_user->rules['invite_code'] = 'required';
			unset($new_user->rules['password_confirm']);
			/*$new_user->rules['property_unit_id'] 	= 'not_empty';
			$new_user->rules['property_id'] 		= 'not_empty';
			$new_user->rules['province'] 			= 'required';
			$new_user->rules['gender'] 			    = 'required';
			$regis_data['user']['province'] 		= $regis_data['province'];
			$regis_data['user']['property_unit_id'] = $regis_data['property_unit_id'];
			$regis_data['user']['property_id'] 		= $regis_data['property_id'];*/
			$regis_data['user_invite']['email'] = strtolower(trim($regis_data['user_invite']['email']));
			$vu = $new_user->validate($regis_data['user_invite']);
			if($response->success == 1 && $vu->passes()) {
				//save to user
				$name = trim($regis_data['user_invite']['fname'])." ".trim($regis_data['user_invite']['lname']);

				//Get Property & PropertyUnit Data
				$invite_code = $regis_data['user_invite']['invite_code'];
				$property_unit = PropertyUnit::with('property')->where('invite_code', '=', $invite_code)->first();

				if (!empty($property_unit)) {
					//Save and send verify code to email
					$user = User::create([
						'name' => $name,
						'email' => strtolower(trim($regis_data['user_invite']['email'])),
						'property_id' => $property_unit->property->id,
						'property_unit_id' => $property_unit->id,
						'role' => 2,
						'password' => Hash::make($regis_data['user_invite']['password']),
						'gender' => $regis_data['user_invite']['gender'],
						'is_subscribed_newsletter' => (isset($regis_data['subscribe'])) ? true : false
					]);

					// generate new InviteCode for PropertyUnit
					$propUnit = PropertyUnit::find($property_unit->id);
					$propUnit->invite_code = $this->generateInviteCode();
					$propUnit->save();

					//$this->dispatch(new SendEmailUser($name, trans('messages.Email.thanks_signup'),$regis_data['user']['email']));
					$save = true;
					Auth::login($user);
					Request::session()->put('success.verify', true);

					$data = [
						'msg' => 'success'
					];
					return $data;
				}else{
					$data = [
						'msg' => 'error',
						'error'=> 'Invite Code Invalid'
					];
					return $data;
				}
			}else{
				if(!$vu->passes()){
					$error_arr = $vu->errors();
					$text_error = "";
					foreach ($error_arr->getMessages() as $item){
						$keys = array_keys($item);
						$text = $item[$keys[0]];
						$text_error .= $text . "<br/>";
					}
					$data = [
						'msg' => 'error',
						'error'=> $text_error
					];
					return $data;
				}

			}

			/// ---- Check Saved if not then return error ----- ///
			if(!$save) {
				if( $response->success != 1) {
					if(empty($captcha)) {
						$message = trans('messages.Signup.no_capcha');
					}
					else {
						$message = trans('messages.Signup.invalid_capcha');
					}
				}else {
					$message = trans('messages.Signup.validate_error');
				}

				$data = [
					'msg' => 'error_captcha',
					'error'=> $message
				];
				return $data;
			}
		}

		$message = trans('messages.Signup.validate_error');
		//on innitial page
		$data = [
			'msg' => 'error_captcha',
			'error'=> $message
		];
		return $data;
	}

	function propertylist () {
		$lang = session()->get('lang');
		if (Request::isMethod('post')) {
			$pid = Request::get('pid');
			$props = Property::where('province','=',$pid)->get();
			echo "<option value='0'>".trans('messages.Signup.select_property')."</option>";
			if(!empty($props)) {
				foreach ($props as $prop) {
					echo "<option value='".$prop->id."'>".$prop->{'property_name_'.$lang}."</option>";
				}
			}
		}
	}

	function property_address () {
		if (Request::ajax()) {
			$prop_id = Request::get('prop_id');
			$prop_ = Property::find($prop_id)->toArray();
			$unos = PropertyUnit::where('property_id','=',$prop_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->get();
			$uno_op = "<option value='0'>".trans('messages.Signup.select_unit')."</option>";
			if(!empty($unos)) {
				foreach ($unos as $prop) {
					$uno_op.="<option value='".$prop->id."'>".$prop->unit_number."</option>";
				}
			}
			echo json_encode(['pa'=>$prop_,'uno_op'=>$uno_op]);
		}
	}

	function generateCode() {
	    $chars = "abcdefghijkmnpqrstuvwxyz123456789";
	    srand((double)microtime()*1000000);
	    $i = 0;
	    $pass = '' ;
	    while ($i < 4) {
	        $num = rand() % 33;
	        $tmp = substr($chars, $num, 1);
	        $pass = $pass . $tmp;
	        $i++;
	    }
	    return $pass;
	}

	function verify() {
	    if (Request::ajax()) {
			$vcode 		= trim(Request::get('code'));
			$email 		= strtolower(trim(Request::get('email')));
			$user 		= User::where('email', '=', $email)->where('verification_code', '=', $vcode)->first();
			if(!empty($user)) {
				$vcode 		= Request::get('code');
				$contents 	= view('home.set_password_form')->with(compact('user','vcode','email'))->render();
				echo json_encode(array('status' => '1','form' => $contents));
			} else {
				echo json_encode(array('status' => '0'));
			}
	    } else if(Request::isMethod('post')) {
	    	$vcode 		= Request::get('code');
	    	$email 		= Request::get('email');
			$user 		= User::where('email', '=', $email)->where('verification_code', '=', $vcode)->first();
			if(!empty($user)) {
				$user_valid = new User();
                unset($user_valid->rules['fname']);
    			unset($user_valid->rules['lname']);
				unset($user_valid->rules['name']);
				unset($user_valid->rules['email']);
				$vu = $user_valid->validate(Request::all());
				if($vu->passes()) {
					$user->password = Hash::make(Request::get('password'));
					$user->verification_code = null;
					$user->save();
					Auth::login($user);
					Request::session()->put('success.verify', true);
					return redirect('feed');
				} else {
					return view('home.set_password_view')->with(compact('user','vcode','email'))->withErrors($vu);
				}
			} else {
				// Wrong Code!! Nothing happen.
				return view('home.verify');
			}
	    } else {
	    	$render_parent = true;
	    	return view('home.verify');
	    }
	}

	function verifyAPI() {
	    if (Request::isMethod('get')) {
			$vcode 		= Request::get('code');
			$user 		= User::where('verification_code', '=', $vcode)->first();
			if(!empty($user)) {
				echo json_encode(array('status' => '1'));
			} else {
				echo json_encode(array('status' => '0'));
			}
	    }
	}

	function sendMailForgot () {
		Mail::send('emails.password', ['token'=>'adddddd'], function ($message) {
			$message->subject('First Email');
		    $message->from('admin@nabour.me', 'Nabour');
		    $message->to('primezigma@gmail.com');
		});
	}

	function email () {
		return view('emails.success_signup')->with('name','aaaa');
	}

	function contact () {
		if(Request::isMethod('post')) {
			$captcha = Request::get('capcha');
        	$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".env('G_CAPCHA_SECRET')."&response=".$captcha);
	        $response = json_decode($response);
	        if($response->success == 1) {
		        Mail::send('emails.contact', [
		        		'content'	=> Request::get('text'),
		        		'name'		=> Request::get('name'),
		        		'email'		=> Request::get('email'),
		        		'property'	=> Request::get('property'),
		        		'phone'		=> Request::get('phone')
		        	], function ($message) {
					$message->subject('CONTACT US');
				    $message->from(Request::get('email'), Request::get('name'));
				    $message->to(env('MAIL_CONTACT'));
				});
				echo "success";
			} else {
				echo "fail";
			}
		}
	}

	function generateInviteCode() {
		$code = $this->randomInviteCodeCharacter();
		$count = PropertyUnit::where('invite_code', '=', $code)->count();
		while($count > 0) {
			$code = $this->randomInviteCodeCharacter();
			$count = PropertyUnit::where('verification_code', '=', $code)->count();
		}
		return $code;
	}

	function randomInviteCodeCharacter(){
		$chars = "abcdefghijkmnpqrstuvwxyz123456789";
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '' ;
		while ($i < 5) {
			$num = rand() % 33;
			$tmp = substr($chars, $num, 1);
			$pass = $pass . $tmp;
			$i++;
		}
		return $pass;
	}

    // test send email
    public function testemailjob(){

        $email = 'sjinadech@gmail.com';

        //SendEmailUser::dispatch('Suttipong Jinadech','Test Job Mails', 'sjinadech@gmail.com'); // Dispatch When run command php artisan queue:work --queue=high,default
        //SendEmailUser::dispatchNow('Suttipong Jinadech','Test Job Mails', 'sjinadech@gmail.com'); // Dispatch Now

        $email_list = ['sjinadech@gmail.com','suttipong.co@gmail.com','suttipong@o-kaatplus.com','sjinadech@gmail.com','suttipong.co@gmail.com','suttipong@o-kaatplus.com'];
        // test looping

        foreach ($email_list as $key=>$item){

            //SendEmailUser::dispatch('Suttipong '.$key,'Test Job Mails'.$key, $item)->onQueue('processing');
            SendEmailUser::dispatch('Suttipong '.$key,'Test Job Mails'.$key, $item);
            //SendEmailUser::dispatchNow('Suttipong '.$key,'Test Job Mails'.$key, $item);

        }

        return "true";
    }
}
