<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use File;
use Request;
use Storage;

use App\CoverImage;

class AppCoverImageController extends Controller
{
    public function __construct () {
        $this->middleware('management');
        view()->share('active_menu', 'cover-image');
    }

    function coverImageList () {
        $user_cover_imgs = CoverImage::where('cover_type','u')->get();
        $ordinary_cover_imgs = CoverImage::where('cover_type','o')->get();
        return view('cover-image.list')->with(compact('user_cover_imgs','ordinary_cover_imgs'));
    }

    public function saveCoverImage () {
        if(Request::isMethod('post')) {
            if (!empty(Request::get('img_post_banner'))) {
                $file = Request::get('img_post_banner');
                $name 	= $file['name'];
                $x 		= Request::get('img-x');
                $y 		= Request::get('img-y');
                $w 		= Request::get('img-w');
                $h 		= Request::get('img-h');
                cropBannerImg ($name,$x,$y,$w,$h);
                $path = $this->createLoadBalanceDir($file['name']);
                $cover = new CoverImage;
                $cover->name        = strtolower($file['name']);
                $cover->url         = $path;
                $cover->cover_type  = Request::get('cover_type');
                $cover->save();
            }
        }

        return redirect('management/admin/app-cover-image');
    }

    function removeCoverImage () {
        if( Request::isMethod('post') ) {
            $remove = Request::get('id');
            $file = CoverImage::find($remove);
            $this->removeFile($file->name);
            $file->delete();
        }
        return response()->json(['result' => true]);
    }

    public function createLoadBalanceDir ($name) {
        $targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
        $folder = substr($name, 0,2);
        $pic_folder = 'cover-file/'.$folder;
        $directories = Storage::disk('s3')->directories('post-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".strtolower($name);
        Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
        return $folder."/";
    }

    public function removeFile ($name) {
        $folder = substr($name, 0,2);
        $file_path = 'cover-file'."/".$folder."/".$name;
        $exists = Storage::disk('s3')->has($file_path);
        if ($exists) {
            Storage::disk('s3')->delete($file_path);
        }
    }
}
