<?php namespace App\Http\Controllers\Management;
use Auth;
use File;
use Request;
use Storage;
use App\Http\Controllers\PostController as GeneralPostController;
use App\Http\Controllers\PushNotificationController;
use Redirect;
use DB;
use Excel;
# Model
use App\Property;
use App\Post;
use App\PostFile;
use App\PostProperty;
use App\UserProperty;
use App\Notification;
use App\User;

class PostController extends GeneralPostController {

	public function __construct () {
		$this->middleware('management');
        view()->share('active_menu', 'news-announcement');
	}

	public function feed () {

        $posts = new Post;
        if(Request::isMethod('post')) {
            if( Request::get('publish_status') != "-") {
                $posts = $posts->where('publish_state',Request::get('publish_status'));
            }

            if( Request::get('post_type') != "-") {
                $posts = $posts->where('post_type',Request::get('post_type'));
            }

            if( Request::get('keyword') ) {
                $keyword = Request::get('keyword');
                $posts = $posts->where(function ($q) use($keyword) {
                    $q->where('title_th','like',"%".$keyword."%");
                    $q->orwhere('title_en','like',"%".$keyword."%");
                    $q->orWhere('description_th','like',"%".$keyword."%");
                    $q->orWhere('description_en','like',"%".$keyword."%");
                });
            }
            if( Request::get('created_date') ) {
                $posts = $posts->where(DB::raw("DATE(created_at)"),'=',date('Y-m-d',strtotime(Request::get('created_date'))));
            }
        }

        $posts = $posts->orderBy('post.created_at', 'desc')->paginate(50);
        if(Request::ajax()) {
            return view('smart-management.post.list-element')->with(compact('posts'));
        } else {
            return view('smart-management.post.list')->with(compact('posts'));
        }
	}


	public function add () {
        $post = new Post;
        $selected_p = [];
        $p_list = Property::pluck('property_name_th','id')->toArray();
        return view('smart-management.post.add')->with(compact('post','p_list','selected_p'));
	}

	public function edit($id) {

        $post = Post::with('postProperty','created_user','updated_user')->find($id);

        if( $post && $post->publish_status && Auth::user()->role > 2) {
            return redirect('admin/news-announcement');
        }

        $p_list = Property::pluck('property_name_th','id')->toArray();

        $selected_p = [];
        if( $post->postProperty ) {
            foreach ($post->postProperty as $p) {
                $selected_p[] = $p->property_id;
            }
        }
        return view('smart-management.post.edit')->with(compact('post','p_list','selected_p'));
	}

	public function save( ) {

        if(Request::isMethod('post')){
            if (Request::get('id')) {
                $post = Post::find(Request::get('id'));
                // not allow to edit when user role is smart admin
                if( $post && $post->publish_status ) {
                    return redirect('admin/news-announcement');
                }
                $post->postProperty()->delete();
                $post->updated_by = Auth::user()->id;
                $action = "edit";
            } else {
                $post = new Post;
                $post->created_by   = Auth::user()->id;
                $post->property_id  = Auth::user()->property_id;
                $action = "create";
            }

            $post->fill(Request::all());

            if( Request::get('approve-flag') &&  $post->publish_state != 2) {
                $post->publish_state = 2;
                $post->approved_at = date('Y-m-d H:i:s');
                $post->approved_by = Auth::user()->id;
                $flag_noti = true;
            } else {
                // no approve
                $flag_noti = false;
            }

            if ( Request::get('flag_important') ) {
                $post->flag_important = true;
            } else {
                $post->flag_important = false;
            }

            if( Request::get('push_notification') !== null ) {
                if (Request::get('push_notification')) {
                    $post->push_notification = true;
                } else {
                    $post->push_notification = false;
                }
            }

            if( !Request::get('expired_date') ) {
                $post->expired_date = null;
            }

            if( !Request::get('publish_in_advance_date') ) {
                $post->publish_in_advance_date = null;
            }

            if(Request::get('remove-banner-flag') && $post->banner_url) {
                $this->removeBannerFile($post->banner_url);
                $post->banner_url = $banner_path = null;
            }

            if (!empty(Request::get('img_post_banner'))) {
                $file = Request::get('img_post_banner');
                $name 	= $file['name'];
                $x 		= Request::get('img-x');
                $y 		= Request::get('img-y');
                $w 		= Request::get('img-w');
                $h 		= Request::get('img-h');
                cropBannerImg ($name,$x,$y,$w,$h);
                $path = $this->createLoadBalanceDir($file['name']);
                $tempUrl = "/%s%s";
                $post->banner_url = sprintf($tempUrl, $path, $file['name']);
            }

            if(Request::get('remove-thumbnail-flag') && $post->thumbnail_url) {
                $this->removeBannerFile($post->thumbnail_url);
                $post->thumbnail_url = null;
            }

            if (!empty(Request::get('img_post_thumbnail'))) {
                $file = Request::get('img_post_thumbnail');
                $name 	= $file['name'];
                $x 		= Request::get('thumbnail-img-x');
                $y 		= Request::get('thumbnail-img-y');
                $w 		= Request::get('thumbnail-img-w');
                $h 		= Request::get('thumbnail-img-h');
                cropBannerImg ($name,$x,$y,$w,$h,100,100);
                $path = $this->createLoadBalanceDir($file['name']);
                $tempUrl = "/%s%s";
                $post->thumbnail_url = sprintf($tempUrl, $path, $file['name']);
            }

            $post->save();

            if (Request::get('property_id') && Request::get('property_applying') != 'A') {
                foreach (Request::get('property_id') as $p_id) {
                    $pp = new PostProperty();
                    $pp->post_id = $post->id;
                    $pp->property_id = $p_id;
                    $pp->save();
                }
            }

            if(!empty(Request::get('attachment'))) {
                $post_file = [];
                foreach (Request::get('attachment') as $file) {
                    //Move Image
                    $path = $this->createLoadBalanceDir($file['name']);
                    $post_file[] = new PostFile([
                            'name' => strtolower($file['name']),
                            'url' => $path,
                            'file_type' => $file['mime'],
                            'is_image'	=> $file['isImage'],
                            'original_name'	=> strtolower($file['originalName'])
                        ]
                    );
                }
                $post->postFile()->saveMany($post_file);
            }

            $remove = Request::get('remove');
            if(!empty($remove['post-file'])) {
                foreach ($remove['post-file'] as $file) {
                    $file = PostFile::find($file);
                    $this->removeFile($file->name);
                    $file->delete();
                }
            }

            // system log
            if(  $action == "create" ) {
                //$this->systemLog(Auth::user(), 'PA', 'C', 'P', $post->id);
            } else {
                //$this->systemLog(Auth::user(), 'PA', 'U', 'P', $post->id);
            }

            if( $flag_noti && $post->push_notification ) {
                    // TODO: Send notification here
                if($post->post_type == 'a') {
                    if ($post->property_applying == 'A') {
                        $all_property = Property::all();
                        foreach ($all_property as $property_item) {
                            //$users = User::where('property_id', $property_item->id)->where('role', 5)->get();
                            $user_property = UserProperty::where('property_id', $property_item->id)->where('approve_status',1)->get();
                            $notification = [];
                            $push_notification = [];
                            foreach ($user_property as $item_user) {
                                $user_id = $item_user->user_id;
                                $user = User::find($user_id);

                                if(isset($user)){
                                    $title = json_encode(['type' => 'announcement_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en]);
                                    $push_notification[] = Notification::create([
                                        'title' => $title,
                                        'description' => "",
                                        'notification_type' => 1,
                                        'subject_key' => $post->id,
                                        'to_user_id' => $item_user->user_id,
                                        'from_user_id' => Auth::user()->id,
                                        'property_id' => $item_user->property_id,
                                        'property_unit_id' => $item_user->property_unit_id
                                    ]);
                                }
                            }
                            if( !empty($push_notification) ) {
                                $controller_push_noti = new PushNotificationController();
                                $controller_push_noti->pushNotificationArray($push_notification);
                            }
                        }
                    } elseif ($post->property_applying == 'S') {
                        // Send Some Property
                        $privilege_property = PostProperty::where('post_id', $post->id)->get();
                        $property_id = [];
                        foreach ($privilege_property as $item_property) {
                            $property_id[] = $item_property->property_id;
                        }

                        $property = Property::whereIn('id', $property_id)->get();
                        foreach ($property as $item_property) {
                            $user_property = UserProperty::where('property_id', $item_property->id)->where('approve_status',1)->get();
                            $notification = [];
                            $push_notification = [];
                            foreach ($user_property as $item_user) {
                                $user_id = $item_user->user_id;
                                $user = User::find($user_id);
                                if (isset($user)) {
                                    $title = json_encode(['type' => 'announcement_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en]);
                                    $push_notification[] = Notification::create([
                                        'title' => $title,
                                        'description' => "",
                                        'notification_type' => 1,
                                        'subject_key' => $post->id,
                                        'to_user_id' => $item_user->user_id,
                                        'from_user_id' => Auth::user()->id,
                                        'property_id' => $item_user->property_id,
                                        'property_unit_id' => $item_user->property_unit_id
                                    ]);
                                }
                            }
                            if( !empty($push_notification) ) {
                                $controller_push_noti = new PushNotificationController();
                                $controller_push_noti->pushNotificationArray($push_notification);
                            }
                        }

                    } elseif ($post->property_applying == 'AE') {
                        // Send All Property Expect Some Property
                        $privilege_property = PostProperty::where('post_id', $post->id)->get();
                        $property_id = [];
                        foreach ($privilege_property as $item_property) {
                            $property_id[] = $item_property->property_id;
                        }

                        $property = Property::whereNotIn('id', $property_id)->get();
                        foreach ($property as $item_property) {
                            $user_property = UserProperty::where('property_id', $item_property->id)->where('approve_status',1)->get();
                            $notification = [];
                            $push_notification = [];
                            foreach ($user_property as $item_user) {
                                $user_id = $item_user->user_id;
                                $user = User::find($user_id);
                                if (isset($user)) {
                                    $title = json_encode(['type' => 'announcement_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$privilege->title_en]);
                                    $push_notification[] = Notification::create([
                                        'title' => $title,
                                        'description' => "",
                                        'notification_type' => 1,
                                        'subject_key' => $privilege->id,
                                        'to_user_id' => $item_user->user_id,
                                        'from_user_id' => Auth::user()->id,
                                        'property_id' => $item_user->property_id,
                                        'property_unit_id' => $item_user->property_unit_id
                                    ]);
                                }
                            }
                            if( !empty($push_notification) ) {
                                $controller_push_noti = new PushNotificationController();
                                $controller_push_noti->pushNotificationArray($push_notification);
                            }
                        }
                    }
                }else{
                    $all_property = Property::all();
                    foreach ($all_property as $property_item) {
                        //$users = User::where('property_id', $property_item->id)->where('role', 5)->get();
                        $user_property = UserProperty::where('property_id', $property_item->id)->get();
                        $notification = [];
                        $push_notification = [];
                        foreach ($user_property as $item_user) {
                            $user_id = $item_user->user_id;
                            $user = User::find($user_id);

                            if(isset($user)){
                                $title = json_encode(['type' => 'news_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en]);

                                $push_notification[] = Notification::create([
                                    'title' => $title,
                                    'description' => "",
                                    'notification_type' => 0,
                                    'subject_key' => $post->id,
                                    'to_user_id' => $item_user->user_id,
                                    'from_user_id' => Auth::user()->id,
                                    'property_id' => $item_user->property_id,
                                    'property_unit_id' => $item_user->property_unit_id
                                ]);
                            }
                        }
                        if( !empty($push_notification) ) {
                            $controller_push_noti = new PushNotificationController();
                            $controller_push_noti->pushNotificationArray($push_notification);
                        }
                    }
                }
            }
        }

        return redirect('management/admin/news-announcement');
    }

    public function approvePost () {

        if (Request::get('pid')) {
            $post = Post::find(Request::get('pid'));
            if ( $post ) {
                if ($post->publish_state == 1) {
                    $post->timestamps = false;
                    $post->publish_state = 2;
                    $post->approved_at = date('Y-m-d H:i:s');
                    $post->approved_by = Auth::user()->id;
                    $post->save();
                    $status = true;
                    $msg = trans('messages.Post.approved_post');

                    // TODO: Send notification here
                    // -- Code here --
                    if($post->post_type == 'a') {
                        if ($post->property_applying == 'A') {
                            $all_property = Property::all();
                            foreach ($all_property as $property_item) {
                                //$users = User::where('property_id', $property_item->id)->where('role', 5)->get();
                                $user_property = UserProperty::where('property_id', $property_item->id)->where('approve_status',1)->get();
                                $notification = [];
                                $push_notification = [];
                                foreach ($user_property as $item_user) {
                                    $user_id = $item_user->user_id;
                                    $user = User::find($user_id);

                                    if(isset($user)){
                                        $title = json_encode(['type' => 'announcement_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en]);

                                        $push_notification[] = Notification::create([
                                            'title' => $title,
                                            'description' => "",
                                            'notification_type' => 1,
                                            'subject_key' => $post->id,
                                            'to_user_id' => $item_user->user_id,
                                            'from_user_id' => Auth::user()->id,
                                            'property_id' => $item_user->property_id,
                                            'property_unit_id' => $item_user->property_unit_id
                                        ]);
                                    }
                                }
                                if( !empty($push_notification) ) {
                                    $controller_push_noti = new PushNotificationController();
                                    $controller_push_noti->pushNotificationArray($push_notification);
                                }
                            }
                        } elseif ($post->property_applying == 'S') {
                            // Send Some Property
                            $privilege_property = PostProperty::where('post_id', $post->id)->get();
                            $property_id = [];
                            foreach ($privilege_property as $item_property) {
                                $property_id[] = $item_property->property_id;
                            }

                            $property = Property::whereIn('id', $property_id)->get();
                            foreach ($property as $item_property) {
                                $user_property = UserProperty::where('property_id', $item_property->id)->where('approve_status',1)->get();
                                $notification = [];
                                $push_notification = [];
                                foreach ($user_property as $item_user) {
                                    $user_id = $item_user->user_id;
                                    $user = User::find($user_id);
                                    if (isset($user)) {
                                        $title = json_encode(['type' => 'announcement_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en]);

                                        $push_notification[] = Notification::create([
                                            'title' => $title,
                                            'description' => "",
                                            'notification_type' => 1,
                                            'subject_key' => $post->id,
                                            'to_user_id' => $item_user->user_id,
                                            'from_user_id' => Auth::user()->id,
                                            'property_id' => $item_user->property_id,
                                            'property_unit_id' => $item_user->property_unit_id
                                        ]);
                                    }
                                }
                                if( !empty($push_notification) ) {
                                    $controller_push_noti = new PushNotificationController();
                                    $controller_push_noti->pushNotificationArray($push_notification);
                                }
                            }

                        } elseif ($post->property_applying == 'AE') {
                            // Send All Property Expect Some Property
                            $privilege_property = PostProperty::where('post_id', $post->id)->get();
                            $property_id = [];
                            foreach ($privilege_property as $item_property) {
                                $property_id[] = $item_property->property_id;
                            }

                            $property = Property::whereNotIn('id', $property_id)->get();
                            foreach ($property as $item_property) {
                                $user_property = UserProperty::where('property_id', $item_property->id)->where('approve_status',1)->get();
                                $notification = [];
                                $push_notification = [];
                                foreach ($user_property as $item_user) {
                                    $user_id = $item_user->user_id;
                                    $user = User::find($user_id);
                                    if (isset($user)) {
                                        $title = json_encode(['type' => 'announcement_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$privilege->title_en]);

                                        $push_notification[] = Notification::create([
                                            'title' => $title,
                                            'description' => "",
                                            'notification_type' => 1,
                                            'subject_key' => $privilege->id,
                                            'to_user_id' => $item_user->user_id,
                                            'from_user_id' => Auth::user()->id,
                                            'property_id' => $item_user->property_id,
                                            'property_unit_id' => $item_user->property_unit_id
                                        ]);
                                    }
                                }
                                if( !empty($push_notification) ) {
                                    $controller_push_noti = new PushNotificationController();
                                    $controller_push_noti->pushNotificationArray($push_notification);
                                }
                            }
                        }
                    }else{
                        $all_property = Property::all();
                        foreach ($all_property as $property_item) {
                            //$users = User::where('property_id', $property_item->id)->where('role', 5)->get();
                            $user_property = UserProperty::where('property_id', $property_item->id)->get();
                            $notification = [];
                            $push_notification = [];
                            foreach ($user_property as $item_user) {
                                $user_id = $item_user->user_id;
                                $user = User::find($user_id);

                                if(isset($user)){
                                    $title = json_encode(['type' => 'news_created', 'title' => '', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en]);

                                    $push_notification[] = Notification::create([
                                        'title' => $title,
                                        'description' => "",
                                        'notification_type' => 0,
                                        'subject_key' => $post->id,
                                        'to_user_id' => $item_user->user_id,
                                        'from_user_id' => Auth::user()->id,
                                        'property_id' => $item_user->property_id,
                                        'property_unit_id' => $item_user->property_unit_id
                                    ]);
                                }
                            }
                            if( !empty($push_notification) ) {
                                $controller_push_noti = new PushNotificationController();
                                $controller_push_noti->pushNotificationArray($push_notification);
                            }
                        }
                    }
                } else {
                    $status = false;
                    $msg = trans('messages.action_fail');
                }
                //$this->addCreatePostNotification($post);
            } else {
                $status = false;
                $msg = trans('messages.Post.post_not_found');
            }
        } else {
            $msg = trans('messages.action_fail');
        }
        return response()->json(['status' => $status, 'message' => $msg]);
    }

    public function rejectPost () {

        if (Request::get('pid')) {
            $post = Post::find(Request::get('pid'));
            if ( $post ) {
                if ($post->publish_state == 1) {
                    $post->timestamps = false;
                    $post->publish_state = 0;
                    $post->send_approved_at = null;
                    $post->send_approved_by = null;
                    $post->save();
                    $status = true;
                    $msg = trans('messages.Post.reject_post_success');
                } else {
                    $status = false;
                    $msg = trans('messages.action_fail');
                }
                //$this->addCreatePostNotification($post);
            } else {
                $status = false;
                $msg = trans('messages.Post.post_not_found');
            }
        } else {
            $msg = trans('messages.action_fail');
        }
        return response()->json(['status' => $status, 'message' => $msg]);
    }

    public function exportPosts () {
        $posts = new Post;
        $posts = $posts->orderBy('created_at')->get();
        $property = Auth::user()->property;
        $filename = "รายการข่าวประชาสัมพันธ์";
        Excel::create($filename, function ($excel) use ($posts, $property) {
            $excel->sheet("Announcements", function ($sheet) use ($posts, $property) {
                $sheet->setWidth(array(
                    'A' => 10,
                    'B' => 50,
                    'C' => 80,
                    'D' => 35,
                    'E' => 15,
                    'F' => 20,
                    'G' => 15,
                    'H' => 15,
                    'I' => 15,
                    'J' => 30,
                    'K' => 20
                ));
                $sheet->loadView('post.export')->with(compact('posts','property'));
            });
        })->export('xlsx');
    }
}
