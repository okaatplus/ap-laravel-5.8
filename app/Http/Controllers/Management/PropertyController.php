<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Property;
use App\Province;

class PropertyController extends Controller
{
    public function __construct () {
        $this->middleware('management');
    }
    
    public function index (Request $r) {

        $props = new Property;
        if($r->get('province')) {
            $props = $props->where('province','=',$r->get('province'));
        }

        if($r->get('name')) {
            $props = $props->where(function ($q) use($r) {
                $q ->where('property_name_th','like',"%".$r->get('name')."%")
                    ->orWhere('property_name_en','like',"%".$r->get('name')."%")
                    ->orWhere('juristic_person_name_th','like',"%".$r->get('name')."%")
                    ->orWhere('juristic_person_name_en','like',"%".$r->get('name')."%");
            });
        }

        $p_rows = $props->orderBy('created_at','desc')->paginate(50);
        $p = new Province;
        $provinces = $p->getProvince();
        if(!$r->ajax()) {
            $property_list = array(''=> trans('messages.Signup.select_property') );
            return view('property.list')->with(compact('p_rows','provinces','property_list'));
        } else {
            return view('property.list-element')->with(compact('p_rows','provinces'));
        }
    }

    public function edit () {

    }
}
