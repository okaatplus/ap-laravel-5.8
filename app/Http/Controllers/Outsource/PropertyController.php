<?php

namespace App\Http\Controllers\Outsource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    public function index () {
        return view('auth_success.index')->with('controller', 'Outsource');
    }
}
