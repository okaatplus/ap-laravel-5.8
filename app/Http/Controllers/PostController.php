<?php namespace App\Http\Controllers;
use Auth;
use File;
use Request;
use Storage;
use Redirect;
use DB;
use Excel;
#use App\Http\Controllers\PushNotificationController;
#Model
use App\Post;
use App\PostFile;
use App\Notification;
#use App\UserProperty;

class PostController extends Controller {

    public function viewPost($id) {
        $post = Post::withCount('tracking')->find($id);
        $trackings = \App\PostTracking::with('user')->where('post_id',$post->id)->paginate(20);
        return view('post.view')->with(compact('post','trackings'));
    }

    public function trackingPage () {
        $trackings = \App\PostTracking::with('user')->where('post_id',Request::get('pid'))->paginate(20);
        return view('post.tracking')->with(compact('trackings'));
    }

    public function delete ($id = 0) {

        if(!Request::isMethod('get')) {

            if($this->deletePost (Request::get('pid')))
            {
                //$this->systemLog(Auth::user(), 'PA', 'D', 'P', Request::get('pid'));
                return response()->json(['status'=>true]);
            }
            else return response()->json(['status'=>false]);

        } else {

            if($this->deletePost ($id)) {
                return redirect('admin/news-announcement');
            }
            else redirect()->back();
        }
    }

	public function deletePost ($id) {
		$post = Post::with('postFile')->find($id);
		if($post) {
            if(!$post->postFile->isEmpty()) {
                foreach ($post->postFile as $file) {
                    $this->removeFile($file->name);
                }
                $post->postFile()->delete();
            }

            if( $post->banner_name ) {
                $this->removeFile($post->banner_name);
            }

            if($post->banner_url) {
                $this->removeBannerFile($post->banner_url);
            }

            $notification = Notification::where('subject_key',$id)->get();
            if($notification->count()) {
                foreach ($notification as $noti) {
                    $noti->delete();
                }
            }
            $post->postProperty()->delete();
            return $post->delete();
		}
	}

	public function printPost ($id) {
        $post = Post::find($id);
        return view('post.print-post')->with(compact('post'));
    }

    public function addCreatePostNotification($post) {
        /*$notification = [];
        $push_notification = [];
		$user_property = UserProperty::where('property_id',Auth::user()->property_id)->whereNotIn('id', [Auth::user()->id])->get();
		foreach ($user_property as $item_user){
		    $user_id = $item_user->user_id;
            $user = User::find($user_id);
            if(isset($user)){
                $title = json_encode( ['type'=>'post_created','title'=>'', 'title_th'=>$post->title_th, 'title_en'=>$post->title_en] );

                if($user->property_unit_id == $item_user->property_unit_id){
                    // User on Current Property
                    $push_notification[] = Notification::create([
                        'title'				=> $title,
                        'description' 		=> "",
                        'notification_type' => 11,
                        'subject_key'		=> $post->id,
                        'to_user_id'		=> $user->id,
                        'from_user_id'		=> Auth::user()->id,
                        'property_id'       => Auth::user()->property_id,
                        'property_unit_id'  => $item_user->property_unit_id
                    ]);
                }else{
                    $notification[] = Notification::create([
                        'title'				=> $title,
                        'description' 		=> "",
                        'notification_type' => 11,
                        'subject_key'		=> $post->id,
                        'to_user_id'		=> $item_user->user_id,
                        'from_user_id'		=> Auth::user()->id,
                        'property_id'       => Auth::user()->property_id,
                        'property_unit_id'  => $item_user->property_unit_id
                    ]);
                }
            }
        }
        if( !empty($push_notification) ) {
            $controller_push_noti = new PushNotificationController();
            $controller_push_noti->pushNotificationArray($push_notification);
        } */
    }
	public function createLoadBalanceDir ($name) {
		$targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
		$folder = substr($name, 0,2);
		$pic_folder = 'post-file/'.$folder;
        $directories = Storage::disk('s3')->directories('post-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".strtolower($name);
        Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
		return $folder."/";
	}

	public function removeFile ($name) {
        $folder = substr($name, 0,2);
        $file_path = 'post-file'."/".$folder."/".$name;
        $exists = Storage::disk('s3')->has($file_path);
        if ($exists) {
            Storage::disk('s3')->delete($file_path);
        }
	}

    public function removeBannerFile ($name) {
        $file_path  = "post-file/".$name;
        $exists     = Storage::disk('s3')->has($file_path);
        if ($exists) {
            Storage::disk('s3')->delete($file_path);
        }
    }

	public function getAttach ($id) {
		$file = PostFile::find($id);
		$file_path = 'post-file'.'/'.$file->url.$file->name;
		$exists = Storage::disk('s3')->has($file_path);
		if ($exists) {
			$response = response(Storage::disk('s3')->get($file_path), 200, [
				'Content-Type' => $file->file_type,
				'Content-Length' => Storage::disk('s3')->size($file_path),
				'Content-Description' => 'File Transfer',
				'Content-Disposition' => "attachment; filename={$file->original_name}",
				'Content-Transfer-Encoding' => 'binary',
			]);
			ob_end_clean();
			return $response;
		}
	}

    public function exportPosts () {
        $posts = Post::where('property_id', Auth::user()->property_id);
        if( Request::get('publish_status') != "-") {
            $posts = $posts->where('publish_status',Request::get('publish_status'));
        }
        if( Request::get('created_date') ) {
            $posts = $posts->where(DB::raw("DATE(created_at)"),'=',date('Y-m-d',strtotime(Request::get('created_date'))));
        }

        $posts = $posts->orderBy('created_at')->get();
        $property = Auth::user()->property;
        $filename = "รายการข่าวประชาสัมพันธ์";
        Excel::create($filename, function ($excel) use ($posts, $property) {
            $excel->sheet("Announcements", function ($sheet) use ($posts, $property) {
                $sheet->setWidth(array(
                    'A' => 10,
                    'B' => 50,
                    'C' => 80,
                    'D' => 35,
                    'E' => 15,
                    'F' => 20,
                    'G' => 15,
                    'H' => 15,
                    'I' => 15,
                    'J' => 30,
                    'K' => 20
                ));
                $sheet->loadView('post.export')->with(compact('posts','property'));
            });
        })->export('xlsx');
    }
}
