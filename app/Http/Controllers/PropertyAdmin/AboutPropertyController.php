<?php namespace App\Http\Controllers\PropertyAdmin;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Pagination\Paginator;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use Auth;
use File;
use Redirect;
use Storage;
# Model
use App\Property;
use App\PropertyFile;
use App\Bank;
use App\Province;
use App\PropertySettings;
use App\DocumentFormatSetting;
use App\DocumentPrefixSetting;
use Carbon\Carbon;

class AboutPropertyController extends Controller {

	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu','about');
		//if(Auth::check() && Auth::user()->role  == 2) Redirect::to('feed')->send();
	}

	public function about () {
		@session_start();
		$_SESSION['juristic'] = Auth::user()->toArray();
		$_SESSION['url'] = url();
		$property = Property::find(Auth::user()->property_id);
		$p = new Province;
		$provinces = $p->getProvince();

		if(Request::isMethod('post')) {
			if(Auth::user()->role  == 1) {
				$tempProperty = Request::all();
				foreach ($tempProperty as &$str) {
					if ($str == "") {
						$str = "-";
					}
				}
				$property->fill($tempProperty);
				$property->min_price = str_replace(',', '', $property->min_price);
				$property->max_price = str_replace(',', '', $property->max_price);
				if (!empty(Request::get('pic_name'))) {
					if (!empty($property->logo_pic_name)) {
						$this->removeFile($property->logo_pic_name);
					}
					$path = $this->createLoadBalanceDir(Request::get('pic_name'));
					$property->logo_pic_name = Request::get('pic_name');
					$property->logo_pic_path = $path;
				}
				$property->save();
			}
			return redirect('admin/property/about');
		}
		return view('about_property.admin-about')->with(compact('property','provinces'));
	}

	public function settings () {
		$property = Property::with('settings')->find(Auth::user()->property_id);
		if(Request::isMethod('post')) {
            //Save settings
            //dd(Request::all());
            $settings = PropertySettings::firstOrNew(array('property_id' => $property->id ));
            $settings->fill(Request::all());
            $settings->auto_reply_allday_flag   = Request::get('auto_reply_allday_flag')?true:false;
            $settings->auto_reply_active        = Request::get('auto_reply_active')?true:false;

            if( $settings->auto_reply_allday_flag ) {
                $settings->auto_reply_start_time = null;
                $settings->auto_reply_end_time = null;
            }
            $settings->save();
			return redirect('admin/property/settings');
		} else {
            $settings = PropertySettings::firstOrNew(array('property_id' => $property->id ));
        }
		return view('about_property.admin-property-settings')->with(compact('property','settings'));
	}

	public function plan () {
		if(Request::isMethod('post')) {
			if(!empty(Request::get('attachment'))) {
				if(Auth::user()->role  == 1) {
					$property = Property::find(Auth::user()->property_id);
					$attach = [];
					foreach (Request::get('attachment') as $key => $file) {
						//Move Image
						$path = $this->createLoadBalanceDir($file['name']);
						$attach[] = new PropertyFile([
							'name' => $file['name'],
							'url' => $path,
							'file_type' => $file['mime'],
							'is_image' => $file['isImage'],
							'original_name' => $file['originalName']
						]);
					}
					$property->propertyFile()->saveMany($attach);
				}
			}
			return redirect('admin/property/plan');
		}
		
		$propertyFile = PropertyFile::where('property_id',Auth::user()->property_id)->get();
		return view('about_property.admin-plan')->with(compact('propertyFile'));
	}

	public function bank () {
		if(Request::isMethod('post')) {
			if(Auth::user()->role  == 1) {
				if(Request::get('id')) {
					$bank = Bank::find(Request::get('id'));
				}
				else {
					$bank = new Bank;
				}
				$bank->fill(Request::all());
				$bank->property_id = Auth::user()->property_id;
                $bank->is_fund_account = (Request::get('is_fund_account'))?true:false;
				$bank->save();
			}
			return redirect('admin/property/bank');
		} else {
			$banks = Bank::where('property_id',Auth::user()->property_id)->where('active',true)->get();
			$bank_obj = New Bank;
			return view('about_property.admin-bank')->with(compact('banks','bank_obj'));
		}

	}

	public function posting () {
		$property = Property::find(Auth::user()->property_id);
		if(Request::isMethod('post')) {
			if(Auth::user()->role  == 1) {
				$property->allow_user_add_event = Request::get('allow_user_add_event') ? true : false;
				$property->allow_user_add_vote = Request::get('allow_user_add_vote') ? true : false;
				$property->allow_user_view_cf_report = Request::get('allow_user_view_cf_report') ? true : false;
				$property->save();
			}
			return redirect('admin/property/posting');
		}else {
			return view('about_property.admin-posting')->with(compact('property'));
		}
	}

    public function printSetting () {
        $property = Property::find(Auth::user()->property_id);
        if(Request::isMethod('post')) {
            if(Auth::user()->role  == 1) {
                $property->document_print_type = Request::get('document_print_type');
                $property->save();
            }
            return redirect('admin/property/print-setting');
        }else {
            return view('about_property.admin-print-setting')->with(compact('property'));
        }
    }

    public function settingRunningDocumentType(){
        $property = Property::find(Auth::user()->property_id);
        $document_format_setting = DocumentFormatSetting::where('property_id',Auth::user()->property_id)->first();
        if(Request::isMethod('post')) {
            $type_format = Request::get('format_type');
            if(Auth::user()->role  == 1) {
                if(isset($document_format_setting)){
                    $document_format_setting_edit = DocumentFormatSetting::find($document_format_setting->id);
                    $document_format_setting_edit->type = $type_format;
                    $document_format_setting_edit->save();
                }else{
                    $new_document_format_setting = new DocumentFormatSetting;
                    $new_document_format_setting->type = $type_format;
                    $new_document_format_setting->property_id = Auth::user()->property_id;
                    $new_document_format_setting->save();
                }

                // Relate Setting Prefix
                $all_type = ['INVOICE','RECEIPT','EXPENSE','PREPAID','WITHDRAWAL','PAYEE'];
                foreach ($all_type as $item) {
                    $document_prefix_setting = DocumentPrefixSetting::where('property_id',Auth::user()->property_id)->where('document_type',$item)->first();
                    if(isset($document_prefix_setting)){
                        $document_prefix_setting_edit = DocumentPrefixSetting::find($document_prefix_setting->id);
                        $example = $this->getExampleRunning($document_prefix_setting_edit->prefix,$property->document_format_setting->type,$document_prefix_setting_edit->is_ce,$document_prefix_setting_edit->year_digit);
                        if($type_format == 0){
                            $document_prefix_setting_edit->running_digit = 8;
                        }else{
                            $document_prefix_setting_edit->running_digit = 5;
                        }
                        $document_prefix_setting_edit->example = $example;
                        $document_prefix_setting_edit->save();
                    }else{
                        $new_document_format_setting = new DocumentPrefixSetting;
                        $year = Carbon::now()->year;
                        $month = str_pad(Carbon::now()->month, 2, '0', STR_PAD_LEFT);
                        $new_document_format_setting->property_id = Auth::user()->property_id;
                        $new_document_format_setting->document_type = $item;
                        $new_document_format_setting->year_start = $year;
                        $new_document_format_setting->month_start = $month;
                        $new_document_format_setting->running_start = 1;
                        if($type_format == 0){
                            $new_document_format_setting->running_digit = 8;
                        }else{
                            $new_document_format_setting->running_digit = 5;
                        }

                        if($item != 'PAYEE'){
                            $new_document_format_setting->prefix = 'NB'.$item[0];
                        }else{
                            $new_document_format_setting->prefix = 'NB'.$item[0].$item[1];
                        }

                        $new_document_format_setting->is_ce = true;
                        $new_document_format_setting->year_digit = 4;
                        $example = $this->getExampleRunning($new_document_format_setting->prefix,$property->document_format_setting->type,$new_document_format_setting->is_ce,$new_document_format_setting->year_digit);
                        $new_document_format_setting->example = $example;
                        $new_document_format_setting->save();
                    }
                }

            }
            return redirect('admin/property/running');
        }else {
            if(isset($document_format_setting) && $property->document_prefix_setting->count()>0){
                foreach ($property->document_prefix_setting as $item){
                    $prefix_setting[$item->document_type] = $item;
                }
                $document_format_setting_type = $document_format_setting->type;
            }else{
                $prefix_setting = null;
                $document_format_setting_type = 0;
            }
            return view('about_property.admin-running-doc-setting')->with(compact('property','prefix_setting','document_format_setting_type'));
        }
    }

    public function settingRunningDocumentFormat(){
        $property = Property::find(Auth::user()->property_id);
        if(Request::isMethod('post')) {

            if(Auth::user()->role  == 1) {
                $document_format_setting = DocumentFormatSetting::where('property_id',Auth::user()->property_id)->first();
                $document_prefix_setting = DocumentPrefixSetting::where('property_id',Auth::user()->property_id)->where('document_type',Request::get('type'))->first();
                if(isset($document_prefix_setting)){
                    $document_prefix_setting_edit = DocumentPrefixSetting::find($document_prefix_setting->id);
                    $document_prefix_setting_edit->prefix = Request::get('prefix');
                    $document_prefix_setting_edit->is_ce = (Request::get('is_ce') == "1")? true : false;
                    $document_prefix_setting_edit->year_digit = Request::get('digit');
                    if($document_format_setting->type == 0){
                        $document_prefix_setting_edit->running_digit = 8;
                    }else{
                        $document_prefix_setting_edit->running_digit = 5;
                    }
                    $example = $this->getExampleRunning($document_prefix_setting_edit->prefix,$property->document_format_setting->type,$document_prefix_setting_edit->is_ce,$document_prefix_setting_edit->year_digit);

                    $document_prefix_setting_edit->example = $example;
                    $document_prefix_setting_edit->save();
                }else{
                    $new_document_prefix_setting = new DocumentPrefixSetting;
                    $year = Carbon::now()->year;
                    $month = str_pad(Carbon::now()->month, 2, '0', STR_PAD_LEFT);
                    $new_document_prefix_setting->property_id = Auth::user()->property_id;
                    $new_document_prefix_setting->document_type = Request::get('type');
                    $new_document_prefix_setting->year_start = $year;
                    $new_document_prefix_setting->month_start = $month;
                    $new_document_prefix_setting->running_start = 1;
                    if($document_format_setting->type == 0){
                        $new_document_prefix_setting->running_digit = 8;
                    }else{
                        $new_document_prefix_setting->running_digit = 5;
                    }
                    $new_document_prefix_setting->prefix = Request::get('prefix');
                    $new_document_prefix_setting->is_ce = (Request::get('is_ce') == "1")? true : false;
                    $new_document_prefix_setting->year_digit = Request::get('digit');
                    $example = $this->getExampleRunning($new_document_prefix_setting->prefix,$property->document_format_setting->type,$new_document_prefix_setting->is_ce,$new_document_prefix_setting->year_digit);
                    $new_document_prefix_setting->example = $example;
                    $new_document_prefix_setting->save();
                }
            }
            return redirect('admin/property/running');
        }else {
            return view('about_property.admin-running-doc-setting')->with(compact('property'));
        }
    }

    public function getExampleRunning($prefix,$type,$is_ce,$year_digit){
        if($is_ce) {
            $year_full = Carbon::now()->year;
        }else{
            $year_full = Carbon::now()->year + 543;
        }

        $year_str_arr = str_split($year_full,2);
        if($year_digit == 2){
            $year = $year_str_arr[1];
        }else{
            $year = $year_str_arr[0].$year_str_arr[1];
        }

        if($type == 1) {
            $result = $prefix . $year . str_pad(1, 5, '0', STR_PAD_LEFT);
        }elseif($type == 2){
            $month = str_pad(Carbon::now()->month, 2, '0', STR_PAD_LEFT);
            $result = $prefix . $year . $month . str_pad(1, 5, '0', STR_PAD_LEFT);
        }else{
            $result = $prefix.str_pad(1, 8, '0', STR_PAD_LEFT);
        }

        return $result;
    }


	public function getform () {
		if(Request::isMethod('post')) {
			$bank_obj = Bank::find(Request::get('bid'));
			return view('about_property.admin-bank-form')->with(compact('bank_obj'));
		}

	}

	public function deleteBank () {
		if(Request::isMethod('post')) {
			if(Auth::user()->role  == 1) {
				$bank = Bank::with('transactionLog')->find(Request::get('bid'));
				if($bank->transactionLog->count()) {
					$bank->active = false;
					$bank->save();
				} else {
					$bank->delete();
				}
				
				return response()->json(['result' => true]);
			}else{
				return response()->json(['result' => false]);
			}
		}
	}


	public function deleteFile () {
		if(Request::isMethod('post')) {
			if(Auth::user()->role  == 1) {
				$file = PropertyFile::find(Request::get('fid'));
				$this->removeFile($file->name);
				$file->delete();
				return response()->json(['result' => true]);
			}else{
				return response()->json(['result' => false]);
			}
		}
	}

	public function createLoadBalanceDir ($name) {
		$targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
		$folder = substr($name, 0,2);
		$pic_folder = 'property-file/'.$folder;
        $directories = Storage::disk('s3')->directories('property-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".$name;
        $upload = Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
		return $folder."/";
	}

	public function removeFile ($name) {
		$folder = substr($name, 0,2);
		$file_path = 'property-file/'.$folder."/".$name;
		if(Storage::disk('s3')->has($file_path)) {
			Storage::disk('s3')->delete($file_path);
		}
	}
}
