<?php

namespace App\Http\Controllers\PropertyAdmin;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Message;
use App\PostParcel;
use App\User;
use App\Customer;
use App\UserProperty;

class DashboardController extends Controller
{
    public function __construct () {
        $this->middleware('smart-admin');
    }

    public function index (Request $r) {

        $new_messages_count     = new Message;
        $post_parcels_count     = new PostParcel;
        $unregister_user_count  = new Customer;
        $waiting_for_approve    = new UserProperty();

        $last_14_days = Carbon::now();
        $last_14_days = $last_14_days->subDays(14)->format('Y-m-d');


        $new_messages_count = $new_messages_count->where('property_id', Auth::user()->property_id);
        $post_parcels_count = $post_parcels_count->where('property_id', Auth::user()->property_id);
        $unregister_user_count = $unregister_user_count->whereHas('customerProperty', function ($q) {
            $q->where('property_id', Auth::user()->property_id);
        });

        // count messages
        $new_messages_count = $new_messages_count->where('flag_new_from_user', true)->count();
        // count parcel
        $post_parcels_count = $post_parcels_count->where('date_received','<=',$last_14_days)->where('status',false)->count();
        // count unregister
        $registered_user = User::whereHas('UserProperty', function ($q) {
            $q->where('property_id', Auth::user()->property_id);
        })->pluck('customer_id')->toArray();
        $unregister_user_count = $unregister_user_count->whereNotIn('id',$registered_user)->count();
        // count waiting approve
        $waiting_for_approve = $waiting_for_approve->where('property_id', Auth::user()->property_id)->where('approve_status',0)->count();

        $registered_user = count($registered_user);
        return view('dashboard.index')->with(compact('new_messages_count','post_parcels_count','unregister_user_count','registered_user','waiting_for_approve'));
    }

    /*public function unApprovalUserList (Request $r) {

        $this->getUnApprovalUserList($r);
        return view('dashboard.un-approve-users-list-element');
    }

    public function getUnApprovalUserList ($r) {
        $un_ap_user_list = Property::withCount('unApprovedUser')->whereHas('users', function ($q) {
            $q->where('approve_status',0)->where('role',8);
        })->where('id', Auth::user()->property_id);
        $un_ap_user_list = $un_ap_user_list->orderBy('un_approved_user_count', 'desc')->paginate(50);
        view()->share(compact('un_ap_user_list'));
    } */
}