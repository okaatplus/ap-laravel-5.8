<?php namespace App\Http\Controllers\PropertyAdmin;
use Auth;
use File;
use Request;
use Storage;
use App\Http\Controllers\PostController as GeneralPostController;
use Redirect;
use DB;
use Excel;
# Model
use App\Post;
use App\PostFile;
use App\PostProperty;

class PostController extends GeneralPostController {

	public function __construct () {
		$this->middleware('smart-admin');
        view()->share('active_menu', 'news-announcement');
	}

	public function feed () {

        $posts = Post::where('property_id','=',Auth::user()->property_id);

        if(Request::isMethod('post')) {
            if( Request::get('publish_status') != "-") {
                $posts = $posts->where('publish_status',Request::get('publish_status'));
            }

            if( Request::get('post_type') != "-") {
                $posts = $posts->where('post_type',Request::get('post_type'));
            }

            if( Request::get('keyword') ) {
                $keyword = Request::get('keyword');
                $posts = $posts->where(function ($q) use($keyword) {
                    $q->where('title_th','like',"%".$keyword."%");
                    $q->orwhere('title_en','like',"%".$keyword."%");
                    $q->orWhere('description_th','like',"%".$keyword."%");
                    $q->orWhere('description_en','like',"%".$keyword."%");
                });
            }
            if( Request::get('created_date') ) {
                $posts = $posts->where(DB::raw("DATE(created_at)"),'=',date('Y-m-d',strtotime(Request::get('created_date'))));
            }
        }

        $posts = $posts->orderBy('post.created_at', 'desc')->paginate(50);
        if(Request::ajax()) {
            return view('post.list-element')->with(compact('posts'));
        } else {
            return view('post.list')->with(compact('posts'));
        }
	}

	public function add () {

        $post = new Post;
		return view('post.add')->with(compact('post'));
	}

	public function edit($id) {

        $post = Post::with('created_user','updated_user')->find($id);

        if( $post && $post->publish_status && Auth::user()->role > 2) {
            return redirect('admin/news-announcement');
        }

        return view('post.edit')->with(compact('post'));
	}

	public function save( ) {

        if(Request::isMethod('post')){

            if (Request::get('id')) {
                $post = Post::find(Request::get('id'));
                // not allow to edit when user role is smart admin
                if( $post && $post->publish_status ) {
                    return redirect('admin/news-announcement');
                }
                $post->updated_by = Auth::user()->id;
                $action = "edit";
            } else {
                $post = new Post;
                $post->created_by   = Auth::user()->id;
                $post->property_id  = Auth::user()->property_id;
                $action = "create";
            }

            $post->fill(Request::all());
            $post->publish_status       = false;
            $post->post_type            = 'a';
            $post->property_applying    = 'S';

            if (Request::get('flag_important')) {
                $post->flag_important = true;
            } else {
                $post->flag_important = false;
            }

            if (Request::get('push_notification')) {
                $post->push_notification = true;
            } else {
                $post->push_notification = false;
            }

            if( !Request::get('expired_date') ) {
                $post->expired_date = null;
            }

            if( !Request::get('publish_in_advance_date') ) {
                $post->publish_in_advance_date = null;
            }

            if(Request::get('remove-banner-flag') && $post->banner_url) {
                $this->removeBannerFile($post->banner_url);
                $post->banner_url = null;
            }

            if (!empty(Request::get('img_post_banner'))) {
                $file = Request::get('img_post_banner');
                $name 	= $file['name'];
                $x 		= Request::get('img-x');
                $y 		= Request::get('img-y');
                $w 		= Request::get('img-w');
                $h 		= Request::get('img-h');
                cropBannerImg ($name,$x,$y,$w,$h);
                $path = $this->createLoadBalanceDir($file['name']);
                $tempUrl = "/%s%s";
                $post->banner_url = sprintf($tempUrl, $path, $file['name']);
            }

            if(Request::get('remove-thumbnail-flag') && $post->thumbnail_url) {
                $this->removeBannerFile($post->thumbnail_url);
                $post->thumbnail_url = null;
            }

            if (!empty(Request::get('img_post_thumbnail'))) {
                $file = Request::get('img_post_thumbnail');
                $name 	= $file['name'];
                $x 		= Request::get('thumbnail-img-x');
                $y 		= Request::get('thumbnail-img-y');
                $w 		= Request::get('thumbnail-img-w');
                $h 		= Request::get('thumbnail-img-h');
                cropBannerImg ($name,$x,$y,$w,$h,100,100);
                $path = $this->createLoadBalanceDir($file['name']);
                $tempUrl = "/%s%s";
                $post->thumbnail_url = sprintf($tempUrl, $path, $file['name']);
            }

            $post->save();

            if (!Request::get('id') || !$post->postProperty->count()) {
                // Save Post property
                $pp = new PostProperty();
                $pp->post_id = $post->id;
                $pp->property_id = $post->property_id;
                $pp->save();
            }

            if(!empty(Request::get('attachment'))) {
                $post_file = [];
                foreach (Request::get('attachment') as $file) {
                    //Move Image
                    $path = $this->createLoadBalanceDir($file['name']);
                    $post_file[] = new PostFile([
                            'name' => strtolower($file['name']),
                            'url' => $path,
                            'file_type' => $file['mime'],
                            'is_image'	=> $file['isImage'],
                            'original_name'	=> strtolower($file['originalName'])
                        ]
                    );
                }
                $post->postFile()->saveMany($post_file);
            }

            $remove = Request::get('remove');
            if(!empty($remove['post-file'])) {
                foreach ($remove['post-file'] as $file) {
                    $file = PostFile::find($file);
                    $this->removeFile($file->name);
                    $file->delete();
                }
            }

            // system log
            if(  $action == "create" ) {
                //$this->systemLog(Auth::user(), 'PA', 'C', 'P', $post->id);
            } else {
                //$this->systemLog(Auth::user(), 'PA', 'U', 'P', $post->id);
            }
        }

        return redirect('admin/news-announcement');
    }

    public function sendApprovePost () {

        if (Request::get('pid')) {
            $post = Post::find(Request::get('pid'));
            if ( $post ) {
                if ($post->publish_state == 0) {
                    $post->timestamps = false;
                    $post->publish_state = 1;
                    $post->send_approved_at = date('Y-m-d H:i:s');
                    $post->send_approved_by = Auth::user()->id;
                    $post->save();
                    $status = true;
                    $msg = trans('messages.Post.send_approval_success');
                } else {
                    $status = false;
                    $msg = trans('messages.action_fail');
                }
                //$this->addCreatePostNotification($post);
            } else {
                $status = false;
                $msg = trans('messages.Post.post_not_found');
            }
        } else {
            $msg = trans('messages.action_fail');
        }
        return response()->json(['status' => $status, 'message' => $msg]);
    }

    public function recallPost () {

        if (Request::get('pid')) {
            $post = Post::find(Request::get('pid'));
            if ( $post ) {
                if ($post->publish_state == 1) {
                    $post->timestamps = false;
                    $post->publish_state = 0;
                    $post->send_approved_at = null;
                    $post->send_approved_by = null;
                    $post->save();
                    $status = true;
                    $msg = trans('messages.Post.recall_post_success');
                } else {
                    $status = false;
                    $msg = trans('messages.action_fail');
                }
                //$this->addCreatePostNotification($post);
            } else {
                $status = false;
                $msg = trans('messages.Post.post_not_found');
            }
        } else {
            $msg = trans('messages.action_fail');
        }
        return response()->json(['status' => $status, 'message' => $msg]);
    }
}
