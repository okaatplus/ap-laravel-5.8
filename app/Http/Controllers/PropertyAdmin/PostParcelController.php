<?php namespace App\Http\Controllers\PropertyAdmin;
use Auth;
use Carbon\Carbon;
use Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\PushNotificationController;
# Model
use DB;
use App\PropertyUnit;
use App\PostParcel;
use App\Property;
use App\Notification;
use App\User;
use App\UserProperty;
use App\Customer;

class PostParcelController extends Controller {
 
	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu', 'parcel-tracking');
	}

	public function postParcelList()
	{
        $tab = Request::get('tab')?Request::get('tab'):'uncollected';

        $post_parcels = PostParcel::where('property_id',Auth::user()->property_id);

        if( $tab == "uncollected" ) {
            $post_parcels = $post_parcels->where('status',0);
        } elseif( $tab == "collected" ) {
            $post_parcels = $post_parcels->where('status',1);
        } else {
            $last_30_days = Carbon::now();
            $last_30_days = $last_30_days->subDays(30)->format('Y-m-d');
            $post_parcels = $post_parcels->where('status',2)->orWhere(function ($q) use ($last_30_days) {
                $q  ->where('date_received','<=',$last_30_days)
                    ->where('status',"!=",1);
            });
        }

		if(Request::isMethod('post')) {

			if(!empty(Request::get('property_unit_id')) && Request::get('property_unit_id') != "-") {
				$post_parcels->where('property_unit_id',Request::get('property_unit_id'));
			}
			if(Request::get('type')) {
				$post_parcels = $post_parcels->where('type',Request::get('type'));
			}

			if(Request::get('date_received')) {
				$post_parcels = $post_parcels->where('date_received',Request::get('date_received'));
			}

			if(Request::get('ref_code')) {
				$post_parcels = $post_parcels->where('ref_code','like','%'.Request::get('ref_code').'%');
			}

            if(Request::get('ems_code')){
                $post_parcels->Where('ems_code', 'LIKE', '%'.Request::get('ems_code').'%');
            }

			$post_parcels = $post_parcels->orderBy('receive_code','desc')->paginate(30);
			return view('post_parcels.list-element')->with(compact('post_parcels','tab'));
		} else {

			$unit_list = array('-'=> trans('messages.unit_no'));
			$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
			$post_parcels = $post_parcels->orderBy('receive_code','desc')->paginate(30);
			return view('post_parcels.list')->with(compact('post_parcels','unit_list','tab'));
		}
	}

	public function add () {
		if(Request::isMethod('post')) {
			$post_parcel = new PostParcel;
			$property = Property::find(Auth::user()->property_id);
			$post_parcel->fill(Request::all());
			$post_parcel->receive_code 	= $property->post_parcel_counter+1;
			$post_parcel->property_id 	= Auth::user()->property_id;

			if( Request::get('to_resident') != 'other') {
                $d = explode('|', Request::get('to_resident'));
                $customer = Customer::find($d[0]);
                $post_parcel->to_name = $customer->name;
                $post_parcel->to_resident_id    = $d[0];
                $post_parcel->to_resident_type  = $d[1];
            } else {
                $post_parcel->to_resident_id    = null;
                $post_parcel->to_resident_type  = null;
            }
			$post_parcel->ref_code = $this->generateRefCode();
            $post_parcel->created_by = Auth::user()->id;
			$post_parcel->save();
			$property->increment('post_parcel_counter');
			$this->sendCreateNotification ($post_parcel->id,$post_parcel->property_id,Request::get('property_unit_id'),$post_parcel->ems_code, receivedNumber($post_parcel->ref_code));
		}
		return redirect('admin/parcel-tracking');
	}

	public function delete ($id) {
		$post_parcel = PostParcel::find($id);
		$post_parcel->delete();
		return redirect('/admin/parcel-tracking');
	}

	public function viewPostParcel () {
		$post_parcel = PostParcel::find(Request::get('id'));
		return view('post_parcels.details')->with(compact('post_parcel'));
	}

	public function delivered () {
		if(Request::isMethod('post')) {
			$post_parcel                    = PostParcel::find(Request::get('id'));
			$post_parcel->status            = 1;
			$post_parcel->receiver_name     = Request::get('receiver_name');
			$post_parcel->delivered_date    = date('Y-m-d H:i:s');
			$post_parcel->delivered_by      = Auth::user()->id;
			$post_parcel->save();
		}
		return redirect('admin/parcel-tracking');
	}

    public function cancelPostParcel () {
        if(Request::isMethod('post')) {
            $post_parcel = PostParcel::find(Request::get('id'));
            $post_parcel->cancel_reason = Request::get('reason');
            $post_parcel->canceled_by = Auth::user()->id;
            $post_parcel->canceled_at = date('Y-m-d H:i:s');
            $post_parcel->status            = 2;
            $post_parcel->save();
        }
        return response()->json(['result' => true]);
    }

	public function sendCreateNotification ($subject_key,$property_id,$unit_id,$ems_code,$receive_code) {
        $title = json_encode( ['type' => 'receive_post_parcel','ems_code'=>$ems_code,'receive_code'=>$receive_code] );
        $user_property = UserProperty::where('property_unit_id', $unit_id)->where('approve_status',1)->get();
        //$users = User::where('property_unit_id',$unit_id)->whereNull('verification_code')->get();
        foreach ($user_property as $user) {
            $notification = Notification::create([
                'title'				=> $title,
                'notification_type' => '2',
                'subject_key'		=> $subject_key,
                'from_user_id'		=> Auth::user()->id,
                'to_user_id'		=> $user->user_id,
                'property_id'		=> $property_id,
                'property_unit_id'		=> $unit_id,
            ]);
            $controller_push_noti = new PushNotificationController();
            $controller_push_noti->pushNotification($notification->id);
        }
	}

    public function printNewList()
    {
        if(Request::isMethod('post')) {
            $post_parcels = PostParcel::where('property_id',Auth::user()->property_id)->where('status',0);
            $date_r = Request::get('date_received');
            if(Request::get('date_received')) {
                $post_parcels = $post_parcels->where('date_received',Request::get('date_received'));
            }

            $post_parcels = $post_parcels->orderBy('date_received','desc')->orderBy('receive_code','asc')->get();

            return view('post_parcels.print-new-post-parcel-list')->with(compact('post_parcels','date_r'));
        }
    }

    public function printLabel()
    {
        if(Request::isMethod('post')) {
            $post_parcels = PostParcel::where('property_id',Auth::user()->property_id)->where('status',0);
            $date_r = Request::get('date_received');
            if(Request::get('date_received')) {
                $post_parcels = $post_parcels->where('date_received',Request::get('date_received'));
            }

            $post_parcels = $post_parcels->orderBy('date_received','desc')->orderBy('receive_code','asc')->get();
            $date_r = date('Y-m-d', strtotime($date_r));
            return view('post_parcels.print-label-post-parcel-list')->with(compact('post_parcels','date_r'));
        }
    }

    public function printNotReceived () {
//        if(Request::isMethod('post')) {
            $post_parcels = PostParcel::where('property_id',Auth::user()->property_id)->where('status',0);
            $post_parcels = $post_parcels->orderBy('date_received','desc')->orderBy('receive_code','asc')->get();
            return view('post_parcels.print-not-received-post-parcel-list')->with(compact('post_parcels'));
        //}
    }

    public function getResident() {
	    if( Request::isMethod('post') ) {
            $resident_list = [];
	        $customer_list = \App\CustomerProperty::where('property_unit_id', Request::get('uid'))->pluck('customer_id')->toArray();
	        if( !empty($customer_list) ) {
                foreach ( $customer_list as $c ) {
                    $customer = \App\Customer::find( $c );
                    $resident_list[$c.'|c'] = $customer->prefix_name." ".$customer->name;
                }
            }

            $tenant_list = \App\TenantProperty::where('property_unit_id', Request::get('uid'))->pluck('tenant_id')->toArray();
            if( !empty($tenant_list) ) {
                foreach ( $tenant_list as $t ) {
                    $tenant = \App\Tenant::find( $t );
                    $resident_list[$t.'|t'] =  $tenant->prefix_name." ".$tenant->name;
                }
            }
            if( !empty($resident_list) ) {
                $result_data = true;
            } else {
                $result_data = 'EMPTY_DATA';
            }
            return response()->json([
                'result'       => $result_data,
                'data'          => $resident_list
            ]);
	    }
    }

    function generateRefCode() {
        $code = $this->randomRefCodeCharacter();
        $count = PostParcel::where('ref_code', '=', $code)->count();
        while($count > 0) {
            $code = $this->randomRefCodeCharacter();
            $count = PropertyUnit::where('ref_code', '=', $code)->count();
        }
        return $code;
    }

    function randomRefCodeCharacter(){
        $chars = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;
        while ($i < 5) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }
}
