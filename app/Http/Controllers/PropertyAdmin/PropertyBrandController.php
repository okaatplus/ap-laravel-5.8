<?php

namespace App\Http\Controllers\PropertyAdmin;

use Request;
use App\Http\Controllers\Controller;
use File;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use Storage;

use App\Propertybrand;

class PropertyBrandController extends Controller
{

    public function index()
    {
        $b = new Propertybrand;


        $brand = new Propertybrand;

        if(Request::get('name')) {
            $b = $b->where(function ($q) {
                $q ->where('name_th','like',"%".Request::get('name')."%")
                    ->orWhere('name_en','like',"%".Request::get('name')."%");
            });
        }

        $p_rows = $b->paginate(50);
        if(!Request::ajax()) {
            return view('PropertyBrand.list_property_brand')->with(compact('p_rows','brand'));
        } else {
            return view('PropertyBrand.list_property_brand_element')->with(compact('p_rows','brand'));

        }

    }


    public function create()
    {
        //dd(Request::all());
        $brand = new Propertybrand;
        $brand->name_th = Request::get('name_th');
        $brand->name_en = Request::get('name_en');
        $brand->img_url = null;
        $brand->brand_type = Request::get('brand_type');

        if (!empty(Request::get('img_post_banner'))) {
            $file = Request::get('img_post_banner');
            $name 	= $file['name'];
            $x 		= Request::get('img-x');
            $y 		= Request::get('img-y');
            $w 		= Request::get('img-w');
            $h 		= Request::get('img-h');
            cropBannerImg ($name,$x,$y,$w,$h);
            $path = $this->createLoadBalanceDir($file['name']);
            $tempUrl = "/%s%s";
            $brand->img_url = sprintf($tempUrl, $path, $file['name']);
        }

        $brand->save();

        return redirect('root/admin/brand/list');
    }


    public function store(Request $request)
    {
        //
    }


    public function view()
    {
        $brand = Propertybrand::find(Request::get('id'));

        return view('PropertyBrand.view_brand')->with(compact('brand'));

    }


    public function edit($id = null)
    {
        $brand = Propertybrand::find($id);

        return view('PropertyBrand.edit_brand')->with(compact('brand'));
    }


    public function update()
    {
        $brand = Propertybrand::find(Request::get('id'));
        $brand->name_th = Request::get('name_th');
        $brand->name_en = Request::get('name_en');
        $brand->img_url = null;
        $brand->brand_type = Request::get('brand_type');
        $brand->save();

        return redirect('root/admin/brand/list');
    }


    public function destroy()
    {
        $brand = Propertybrand::find(Request::get('id'));
        $brand->delete();
        return redirect('root/admin/brand/list');
    }

    public function createLoadBalanceDir ($name) {
        $targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
        $folder = substr($name, 0,2);
        $pic_folder = 'post-file/'.$folder;
        $directories = Storage::disk('s3')->directories('post-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".strtolower($name);
        Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
        return $folder."/";
    }
}
