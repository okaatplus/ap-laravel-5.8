<?php

namespace App\Http\Controllers\PropertyAdmin;

use App\CustomerProperty;
use Illuminate\Routing\Controller;
use Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App;

use App\Customer;
use App\PropertyUnit;

class PropertyCustomerController extends Controller
{
    public function __construct () {
        $this->middleware('smart-admin');
        view()->share('active_menu','members');
    }

    public function customerList () {
        $customers = Customer::whereHas('customerProperty', function ($q) {
            $q->where('property_id', Auth::user()->property_id)->where('customer_status',1);
        })->orderBy('name', 'asc')->paginate(30);
        $unit_list = array(''=> trans('messages.unit_no') );
        $unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
        return view('property_members.customer.customer-list')->with(compact('customers','unit_list'));
    }

    public function customerListPage () {
        if(Request::ajax()) {
            $customers = Customer::whereHas('customerProperty', function ($q) {
                $q->where('property_id', Auth::user()->property_id)->where('customer_status',1);
            });


            if(Request::get('unit_id')) {
                $customers = $customers->whereHas('customerProperty', function ($q) {
                    $q->where('property_unit_id',Request::get('unit_id'));
                });
            }

            if(Request::get('name')) {
                $customers = $customers->where('name','like',"%".Request::get('name')."%");
            }

            $customers = $customers->orderBy('name', 'asc')->paginate(30);
            return view('property_members.customer.customer-list-page')->with(compact('customers'));
        }
    }
    public function getCustomer () {
        if(Request::ajax()) {
            $customer = Customer::with(['customerProperty' => function ($q) {
                $q->where('property_id', Auth::user()->property_id)->where('customer_status',1);;
            }])->find(Request::get('uid'));
            return view('property_members.customer.view-customer')->with(compact('customer'));
        }
    }

    public function editCustomer () {
        if(Request::ajax()) {
            $customer = Customer::with(['customerProperty' => function ($q) {
                $q->where('property_id', Auth::user()->property_id)->where('customer_status',1);;
            }])->find(Request::get('uid'));
            return view('property_members.customer.edit-customer')->with(compact('customer'));
        }
    }

    public function saveCustomer () {
        if(Request::isMethod('post')) {
            $customer = Customer::find(Request::get('id'));
            $customer->fill(Request::all());
            $customer->save();

            $cp = CustomerProperty::where('customer_id',$customer->id)
                ->where('customer_type',0)
                ->where('customer_status',1)
                ->get();

            if( $cp ) {
                // update property_unit owner name
                foreach ($cp AS $c) {
                    $property_unit = PropertyUnit::with('propertyUnitOwner')->find($c->property_unit_id);
                    $owner_name = [];
                    foreach ( $property_unit->propertyUnitOwner as $p_owner ) {
                        $owner_name[] = $p_owner->customer->name;
                    }
                    if( $owner_name ) {
                        $property_unit->owner_name_th = implode(',',$owner_name);
                        $property_unit->owner_name_en = $property_unit->owner_namre_th ;
                        $property_unit->save();
                    }

                }
            }
            return response()->json(['result' => true]);
        }
    }

    public function exportCustomer () {
        $customers = Customer::whereHas('customerProperty', function ($q) {
            $q->where('property_id', Auth::user()->property_id)->where('customer_status',1);
        });

        if(Request::get('unit_id')) {
            $customers = $customers->where('property_unit_id',Request::get('unit_id'));
        }

        if(Request::get('name')) {
            $customers = $customers->where('name','like',"%".Request::get('name')."%");
        }

        $customers = $customers->orderBy('created_at','DESC')->get();

        $property = \App\Property::find(Auth::user()->property_id);
        $filename = trans('messages.Member.customer_page_head')." - ".$property->{'property_name_'.App::getLocale()};
        try {
            Excel::create($filename, function ($excel) use ($customers, $property) {
                $excel->sheet("Sheet 1", function ($sheet) use ($customers, $property) {
                    $sheet->setWidth(array(
                        'A' => 10,
                        'B' => 50,
                        'C' => 30,
                        'D' => 20,
                        'E' => 25,
                        'F' => 17,
                        'G' => 20
                    ));
                    $sheet->loadView('property_members.customer.export')->with(compact('customers','property'));
                });
            })->export('xlsx');
        }catch (LaravelExcelException $ex){
            $error= $ex;
        }
    }
}
