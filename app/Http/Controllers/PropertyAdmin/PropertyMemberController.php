<?php namespace App\Http\Controllers\PropertyAdmin;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Pagination\Paginator;
use Auth;
use Validator;
use App\Http\Controllers\PushNotificationController;
# Model
use App;
use DB;
use Excel;
use App\PropertyMember;
use App\UserProperty;
use App\PropertyUnit;
use App\User;
use App\Province;
use App\Rules\notZero;
use App\Notification;

class PropertyMemberController extends Controller {

	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu','members');
	}

	public function memberlist () {
		$members 		= UserProperty::with('ofUser')
                            ->where('property_id',Auth::user()->property_id)
                            ->where('approve_status',1)
							->orderBy('created_at','DESC')
							->paginate(30);
		$unit_list = array(''=> trans('messages.unit_no') );
		$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
		$member = new PropertyMember;
		return view('property_members.view')->with(compact('members','unit_list','member'));
	}

	public function memberlistPage () {
		if(Request::ajax()) {
			$members 	= UserProperty::with('ofUser')
							->where('property_id',Auth::user()->property_id)
                            ->where('approve_status',1);

			if(Request::get('type') == 'cm') {
				$members->where('is_chief',true);
			} else if(Request::get('type') == 'gm') {
				$members->where('is_chief',false);
			}

			if(Request::get('active') != '-') {
			    $status = Request::get('active')?true:false;
				$members->where('active_status',$status);
			}

			if(Request::get('unit_id')) {
				$members->where('property_unit_id',Request::get('unit_id'));
			}

			if(Request::get('name')) {
				$members->whereHas('ofUser', function ($q) {
				    $q->where('name','like',"%".Request::get('name')."%");
                });
			}

			$members = $members->orderBy('created_at','DESC')->paginate(30);
			return view('property_members.member-list')->with(compact('members'));
		}
	}


	public function setActive () {
		if(Request::ajax()) {
			$up = UserProperty::find(Request::get('uid'));
			if($up) {
                $up->active_status = Request::get('status');
                $up->save();
				return response()->json(['result'=>true]);
			}
		}
	}

	public function getMember () {
		if(Request::ajax()) {
		    $member = UserProperty::with('ofUser')->find(Request::get('uid'));
            $member = $member->ofUser;
			return view('property_members.view-member')->with(compact('member'));
		}
	}

    public function editMember () {

        if(Request::ajax()) {
            $up = UserProperty::with('ofUser')->find(Request::get('uid'));
            $member = $up->ofUser;
            return view('property_members.edit-member')->with(compact('member','up'));
        } else {
            if( Request::isMethod('post')) {

                $member         = User::find(Request::get('id'));
                $member->phone  = Request::get('phone');
                $member->save();
                $up = UserProperty::find(Request::get('up_id'));
                /// need to check customer/tenant also
                if( Request::get('user_type') != "-" ) {
                    if( Request::get('user_type') != 99 ) {
                        $up->user_type = Request::get('user_type');

                        $customer_property = \App\CustomerProperty::where('customer_id',$member->customer_id)
                            ->where('property_unit_id',$up->property_unit_id)->first();
                        if( $customer_property ) {
                            $customer_property->customer_type = $up->user_type;
                            $customer_property->save();
                        }
                    } else {
                        $up->property_unit_id   = null;
                        $up->user_type          = null;
                    }

                }
                if( $up->user_type == 0 && Request::get('is_committee_member')) {
                    $up->is_committee_member = true;
                } else {
                    $up->is_committee_member = false;
                }
                $up->save();
                return redirect('admin/property/members');
            }
        }
    }

	public function newMembers() {
		$p = new Province;
		$provinces = $p->getProvince();
		$users = UserProperty::with('ofUser','unit')->where('property_id', Auth::user()->property_id)->where('approve_status', 0);
		if(Request::ajax()) {

            if(Request::get('name')) {
                $users->whereHas('ofUser', function ($q) {
                    $q->where('name','like',"%".Request::get('name')."%");
                });
            }

			if(Request::get('property_unit_id')) {
				$users->where('property_unit_id', Request::get('property_unit_id'));
			}

			$users = $users->orderBy('created_at','desc')->paginate(30);
			return view('property_members.new-member-list-page')->with(compact('users','provinces'));
		} else {
            $users = $users->orderBy('created_at','desc')->paginate(30);
			$property_unit_list = array(''=> trans('messages.unit_no') );
			$property_unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
			return view('property_members.new-member-list')->with(compact('users','property_unit_list','provinces'));
		}
	}

    public function rejectNewMember () {
        if (Request::isMethod('post')) {
            $user = UserProperty::find(Request::get('upid'));
            $user->approve_status = 2;
            $user->save();
            return response()->json(['result' => true]);
        } else {
            return response()->json(['result' => false]);
        }
    }

    public function approveNewMember () {
        if (Request::isMethod('post')) {
            $user_property = UserProperty::find(Request::get('upid'));
            $user_property->approve_status = 1;
            if( $user_property->user_type === null ) {
                $user_property->user_type = Request::get('type');
                $flag_custom_type = true;
            } else {
                $flag_custom_type = false;
            }
            $user_property->save();

            if( $flag_custom_type ) {
                $user_property->load('ofUser');
                if( $user_property->ofUser ) {
                    $id_card = $user_property->ofUser->id_card;
                    if( $id_card ) {
                        if( $user_property->user_type == 2) {
                            // check tenant data
                            $this->checkTenant($id_card, $user_property);
                        } else {
                            // check customer
                            $this->checkCustomer($id_card, $user_property);
                        }
                    }
                }
            }

            // Send Notification to User
            $title = json_encode( ['type' => 'accepted_resident'] );
            $notification = Notification::create([
                'title'				=> $title,
                'notification_type' => '4',
                'subject_key'		=> $user_property->property_unit_id,
                'from_user_id'		=> Auth::user()->id,
                'to_user_id'		=> $user_property->user_id,
                'property_id'		=> $user_property->property_id,
                'property_unit_id'		=> $user_property->property_unit_id,
            ]);

            $controller_push_noti = new PushNotificationController();
            $controller_push_noti->pushNotification($notification->id);

            return response()->json(['result' => true]);
        } else {
            return response()->json(['result' => false]);
        }
	}
	
	public function addMember() {

        if (Request::isMethod('post')) {
            $data = Request::all();
            $validator = Validator::make($data, [
				'name' 				=> 'required|max:255',
                'email' 			=> 'unique:users',
                'password' 			=> 'alpha_num|min:6|required',
                'password_confirm' 	=> 'alpha_num|min:6|required|same:password'
            ]);

            if ($validator->fails()) {
                $member = $data;
				$unit_list = array(''=> trans('messages.unit_no') );
				$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
                return view('property_members.member-form')->withErrors($validator)->with(compact('member','unit_list'));
            } else {
				$member = new PropertyMember;
				$member->property_id	= Auth::user()->property_id;
				$member->name 			= $data['name'];
				$member->email 			= $data['email'];
				$member->phone 			= $data['phone'];
				$member->password 		= bcrypt($data['password']);
				$member->role 			= 8;
				$member->save();

				$up = new UserProperty;
                $up->user_id        = $member->id;
                $up->property_id    = $member->property_id;
                $up->user_type      = null;
                $up->approve_status = 1;
                $up->save();
                echo "saved";
            }
        }
	}

	public function checkCustomer ($id_card, $user_property ) {
	    $user = $user_property->ofUser;
        $dup_customer = App\Customer::where('id_card',$id_card)->first();
        if( !$dup_customer ) {
            // create customer
            $customer = $this->addCustomer($user);
            // add customer property
        } else {
            $customer = $dup_customer;
            // do something with user property may be grant active status to all
        }
        if( !$user->customer_id ) {
            $user->customer_id = $customer->id;
            $user->save();
        }

        // check duplicated tenant property
        $dup_cp = App\CustomerProperty::where('customer_id',$customer->id)
            ->where('property_unit_id',$user_property->property_unit_id)
            ->first();
        if( !$dup_cp ) {
            // add customer property
            $c_p = new App\CustomerProperty;
            $c_p->customer_id       = $customer->id;
            $c_p->property_id       = $user_property->property_id;
            $c_p->property_unit_id  = $user_property->property_unit_id;
            $c_p->customer_type     = $user_property->user_type;
            $c_p->save();
        }

        return true;
    }

	public function addCustomer ($user) {
        $c_data = new App\Customer;
        $c_data->prefix_name        = "";
        $c_data->name               = $user->name;
        $c_data->id_card            = $user->id_card;
        $c_data->type_id_card       = $user->type_id_card;
        $c_data->phone              = $user->phone;
        $c_data->email              = $user->email;
        $c_data->person_type        = 0;
        $c_data->save();

        // update customer id in user
        $user->timestamps = false;
        $user->customer_id = $c_data->id;
        $user->save();

        return $c_data;
    }

    public function checkTenant ($id_card, $user_property ) {
        $user = $user_property->ofUser;
        $dup_tenant = App\Tenant::where('id_card',$id_card)->first();
        if( !$dup_tenant ) {
            // create customer
            $tenant = $this->addTenant($user);
            // add tenant property
        } else {
            $tenant = $dup_tenant;
            // do something with user property may be grant active status to all
        }

        if( !$user->tenant_id ) {
            $user->tenant_id = $tenant->id;
            $user->save();
        }

        // check duplicated tenant property
        $dup_cp = App\TenantProperty::where('tenant_id',$tenant->id)
            ->where('property_unit_id',$user_property->property_unit_id)
            ->first();
        if( !$dup_cp ) {
            // add tenant property
            $c_p = new App\TenantProperty;
            $c_p->tenant_id       = $tenant->id;
            $c_p->property_id       = $user_property->property_id;
            $c_p->property_unit_id  = $user_property->property_unit_id;
            //$c_p->customer_type     = $user_property->user_type;
            $c_p->save();
        }

        return true;
    }

    public function addTenant ($user) {
        $t_data = new App\Tenant;
        $t_data->prefix_name        = "";
        $t_data->name               = $user->name;
        $t_data->id_card            = $user->id_card;
        $t_data->type_id_card       = $user->type_id_card;
        $t_data->phone              = $user->phone;
        $t_data->email              = $user->email;
        $t_data->person_type        = 0;
        //$t_data->nationality        = ;
        //$t_data->job                = empty($customer['n'])?NULL:$customer['n'];
        //$t_data->dob                = empty($customer['o'])?NULL:$customer['o'];
        //$t_data->customer_code      = empty($customer['p'])?NULL:$customer['p'];
        $t_data->save();

        // update customer id in user
        $user->timestamps = false;
        $user->tenant_id = $t_data->id;
        $user->save();

        return $t_data;
    }

	public function exportMembers () {
        $members    = UserProperty::with('ofUser')
                    ->where('property_id',Auth::user()->property_id)
                    ->where('approve_status',1);

        if(Request::get('unit_id')) {
            $members->where('property_unit_id',Request::get('unit_id'));
        }

        if(Request::get('name')) {
            $members->where('name','like',"%".Request::get('name')."%");
        }

        $members = $members->orderBy('created_at','DESC')->get();

        $property = Auth::user()->property;
		$filename = trans('messages.Member.page_head')." - ".$property->{'property_name_'.App::getLocale()};
		try {
			Excel::create($filename, function ($excel) use ($members, $property) {
				$excel->sheet("Sheet 1", function ($sheet) use ($members, $property) {
					$sheet->setWidth(array(
						'A' => 10,
						'B' => 50,
						'C' => 30,
						'D' => 20,
						'E' => 25,
						'F' => 17,
						'G' => 20
					));
					$sheet->loadView('property_members.export')->with(compact('members','property'));
				});
			})->export('xlsx');
		}catch (LaravelExcelException $ex){
			$error= $ex;
		}
    }

    public function exportNewMembers () {

        $members    = UserProperty::with('ofUser')
            ->where('property_id',Auth::user()->property_id)
            ->where('approve_status',0);

        if(Request::get('name')) {
            $members = $members->where('name','like',"%".Request::get('name')."%");
        }

        if(Request::get('property_unit_id')) {
            $members = $members->where('property_unit_id', Request::get('property_unit_id'));
        }
        $members = $members->orderBy('created_at','DESC')->get();

        $property = Auth::user()->property;
        $filename = trans('messages.AdminUser.new_page_head')." - ".$property->{'property_name_'.App::getLocale()};
        try {
            Excel::create($filename, function ($excel) use ($members, $property) {
                $excel->sheet("Sheet 1", function ($sheet) use ($members, $property) {
                    $sheet->setWidth(array(
                        'A' => 10,
                        'B' => 50,
                        'C' => 30,
                        'D' => 20,
                        'E' => 25,
                        'F' => 17,
                        'G' => 20
                    ));
                    $sheet->loadView('property_members.new-export')->with(compact('members','property'));
                });
            })->export('xlsx');
        }catch (LaravelExcelException $ex){
            $error= $ex;
        }
    }
}
