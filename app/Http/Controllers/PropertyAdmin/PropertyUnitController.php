<?php namespace App\Http\Controllers\PropertyAdmin;

use Maatwebsite\Excel\Exceptions\LaravelExcelException;
use Request;
use Illuminate\Routing\Controller;
use Auth;
use Redirect;
use Storage;
use DB;
use File;
use Excel;
use View;
# Model
use App\PropertyUnit;
use App\User;
use App\Property;
use App\UserProperty;
use App\CustomerProperty;

class PropertyUnitController extends Controller {

	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu','units');
	}

	public function unitList () {
		$units 		= PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true);

		$unit_list = array(''=> trans('messages.unit_no') );
		$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();

		if(Request::ajax()) {

			if(Request::get('unit_id')) {
				$units->where('id',Request::get('unit_id'));
			}

			$units = $units->orderBy(DB::raw('natsortInt(unit_number)'))->paginate(30);
			return view('property_units.unit-list-page')->with(compact('units','unit_list'));
		}
		else{
			$units = $units->orderBy(DB::raw('natsortInt(unit_number)'))->paginate(30);
			return view('property_units.unit-list')->with(compact('units','unit_list'));
		}
	}

	public function getUnit () {
		$unit = PropertyUnit::with(
		    'propertyUnitCustomer','propertyUnitCustomer.customer',
            'propertyUnitTenant','propertyUnitTenant.tenant')->find(Request::get('unit_id'));
		return view('property_units.get-unit')->with(compact('unit'));
	}

	public function add () {
		$unit = new PropertyUnit;
		$unit->fill(Request::all());

		if(Request::get('transferred_date')){
			$unit->transferred_date = Request::get('transferred_date');
		}else{
			$unit->transferred_date = null;
		}
		if(Request::get('insurance_expire_date')){
			$unit->insurance_expire_date = Request::get('insurance_expire_date');
		}else{
			$unit->insurance_expire_date = null;
		}
		
		$unit->property_id = Auth::user()->property_id;
		$unit->property_size = str_replace(',', "", Request::get('property_size'));
		$unit->extra_cf_charge = str_replace(',', "", Request::get('extra_cf_charge'));
		$unit->property_size = ($unit->property_size>0)?$unit->property_size:0;
		$unit->invite_code = $this->generateInviteCode();
		$unit->save();
		return redirect('admin/property/units');
	}

	public function editForm () {
		$unit = PropertyUnit::find(Request::get('unit_id'));
		return view('property_units.get-unit-edit-form')->with(compact('unit'));
	}

	public function edit () {
		$unit = PropertyUnit::find(Request::get('id'));
		$unit->fill(Request::all());

		if(Request::get('transferred_date')){
			$unit->transferred_date = Request::get('transferred_date');
		}else{
			$unit->transferred_date = null;
		}
		if(Request::get('insurance_expire_date')){
			$unit->insurance_expire_date = Request::get('insurance_expire_date');
		}else{
			$unit->insurance_expire_date = null;
		}
		$unit->property_id = Auth::user()->property_id;
		$unit->property_size = str_replace(',', "", Request::get('property_size'));
		$unit->property_size = ($unit->property_size>0)?$unit->property_size:0;
        $unit->address = Request::get('delivery_address');

		$unit->save();
		return redirect('admin/property/units');
	}


	public function deleteMembers() {
		if (Request::isMethod('post')) {
			$user = User::find(Request::get('uid'));
			if(isset($user)) {
				$notis = Notification::where('to_user_id',$user->id)->get();
				if($notis->count()) {
					foreach ($notis as $noti) {
						$noti->delete();
					}
				}
				$user->delete();
			}
			return response()->json(['result'=>true]);
		}
	}
	
	public function deleteTenant () {
		$data = [
			'msg' => 'fail'
		];
		if (Request::isMethod('post')) {
			$tenant = Tenant::find(Request::get('tenant_id'));
			if (isset($tenant)) {
				$tenant->delete();
				$data = [
					'msg' => 'success'
				];
			}
		}

		return $data;
	}

	public function clearUnit () {
		$unit = PropertyUnit::find(Request::get('id'));
		$new_unit = new PropertyUnit;
		$new_unit->fill($unit->toArray());
		// disable property
		$unit->active = false;
		$unit->save();
		// disable user account
		$users_p = UserProperty::where('property_unit_id',Request::get('id'))->get();
		if( $users_p ) {
            foreach ($users_p as &$user_p) {
                $user_p->active_status = false;
                $user_p->save();
            }
        }

        $users_cp = CustomerProperty::where('property_unit_id',Request::get('id'))->get();
		if( $users_cp ) {
            foreach ($users_cp as &$user_cp) {
                $user_cp->customer_status = 0;
                $user_cp->save();
            }
        }

		$new_unit->owner_name_th = Request::get('owner_name_th');
		$new_unit->owner_name_en = Request::get('owner_name_en');
		$new_unit->phone = $new_unit->resident_count = $new_unit->delivery_address = NULL;
		$new_unit->move_in = NULL;
		$new_unit->save();

        $this->checkCustomer(Request::get('id_card'), $new_unit->property_id, $new_unit->id);

		return redirect('admin/property/units');
	}

	public function checkBalance () {
		$unit = PropertyUnit::find(Request::get('unit_id'));
		if( $unit && $unit->balance > 0 ) {
			$r = array(
				'result' => true,
				'balance' => $unit->balance
			);
		} else {
			$r = array(
				'result' => false
			);
		}
		return response()->json($r);
	}

	function generateInviteCode() {
		$code = $this->randomInviteCodeCharacter();
		$count = PropertyUnit::where('invite_code', '=', $code)->count();
		while($count > 0) {
			$code = $this->randomInviteCodeCharacter();
			$count = PropertyUnit::where('invite_code', '=', $code)->count();
		}
		return $code;
	}

    function randomInviteCodeCharacter(){
        $chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;
        while ($i < 4) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

	public function exportPropertyUnit(){
        $filename = "property unit";
        $property = Property::find(Auth::user()->property_id);
        $units 		= PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->get();

        try {
            Excel::create($filename, function ($excel) use ($units, $property) {
               $excel->sheet("Property unit", function ($sheet) use ($units, $property) {
                    $sheet->setWidth(array(
                        'A' => 20,
                        'B' => 30,
                        'C' => 30,
						'D' => 15,
						'E' => 15,
						'F' => 15,
						'G' => 15,
                        'H' => 20,
                        'I' => 15,
                        'J' => 20,
                        'K' => 15,
                        'L' => 30
                    ));
                    $sheet->loadView('property_units.data-export')->with(compact('units', 'property'));
                });

                $excel->setCreator('AP SMART');
            })->export('xlsx');
        }catch (LaravelExcelException $ex){
            $error= $ex;
	    }
    }

    public function checkCustomer ($id_card, $property_id, $property_unit_id ) {
        //$user = $user_property->ofUser;
        $dup_customer = \App\Customer::where('id_card',$id_card)->first();
        if( !$dup_customer ) {
            // create customer
            $customer = $this->addCustomer();
            // add customer property
        } else {
            $customer = $dup_customer;
            // do something with user property may be grant active status to all
        }

        $c_p = new \App\CustomerProperty;
        $c_p->customer_id       = $customer->id;
        $c_p->property_id       = $property_id;
        $c_p->property_unit_id  = $property_unit_id;
        $c_p->customer_type     = 0;
        $c_p->save();

        $user = User::where('id_card',$customer->id_card)->first();
        if( $user ) {
            //$up = UserProperty::where('user_id',$user->id)->where('property_unit_id',$property_unit_id)->first();
            /// Add user property
            $up = new UserProperty;
            $up->user_id            = $user->id;
            $up->property_id        = $property_id;
            $up->property_unit_id   = $property_unit_id;
            $up->user_type          = 0;
            $up->approve_status     = 1;
            $up->save();
        }

        return true;
    }

    public function addCustomer () {
        $c_data = new \App\Customer;
        $c_data->prefix_name        = "";
        $c_data->name               = Request::get('owner_name_th');
        $c_data->id_card            = Request::get('id_card');
        $c_data->type_id_card       = Request::get('identify_type');
        $c_data->person_type        = 0;
        $c_data->save();
        return $c_data;
    }
}
