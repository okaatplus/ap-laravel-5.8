<?php namespace App\Http\Controllers\PropertyAdmin;
use Illuminate\Routing\Controller;
use Request;
use Auth;

class RestoreAdminController extends Controller {
    public function __construct () {
		$this->middleware('smart-admin');
	}

    public function restoreAdmin () {
        if(!Request::session()->has('auth.root_admin'))
        return redirect('/');

        Auth::login(Request::session()->pull('auth.root_admin'));
        
        return redirect('root/admin/property/list');
    }
}