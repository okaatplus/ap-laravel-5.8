<?php namespace App\Http\Controllers\PropertyAdmin;
use Request;
use Auth;
use File;
use Storage;
use App\Http\Controllers\Controller;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use App\Http\Controllers\PushNotificationController;
use DB;
use Excel;
# Model
use App\Vote;
use App\VoteFile;
use App\Choice;
use App\UserChoice;
use App\User;
use App\Notification;
use App\UserProperty;
//use App\SystemLog;
class VoteController extends Controller {

	public function __construct () {
		$this->middleware('smart-admin');
		view()->share('active_menu', 'survey');
	}
	public function index()
	{
        $votes = Vote::where('property_id', Auth::user()->property_id);

        if(Request::isMethod('post')) {
            if( Request::get('publish_status') != "-") {
                $votes = $votes->where('publish_status',Request::get('publish_status'));
            }

            if( Request::get('created_date') ) {
                $votes = $votes->where(DB::raw("DATE(created_at)"),'=',date('Y-m-d',strtotime(Request::get('created_date'))));
            }
        }

        $votes =  $votes->orderBy('vote.start_date','desc')->paginate(50);
        if(Request::ajax()) {
            return view('vote.list-element')->with(compact('votes'));
        } else {
            return view('vote.list')->with(compact('votes'));
        }
	}

	public function add () {
        $vote = new Vote;
        return view('vote.add')->with(compact('vote'));
	}

	public function edit ($id) {
        $vote = Vote::find($id);
        return view('vote.edit')->with(compact('vote'));
	}

	public function save () {

        if(Request::isMethod('post')) {
            $old_publish_status = false;

            if (Request::get('id')) {
                $vote = Vote::find(Request::get('id'));
                $vote->updated_by = Auth::user()->id;
                $old_publish_status = $vote->publish_status;
                $action = "edit";
            } else {
                $vote = new Vote;
                $vote->title_th             = Request::get('title_th');
                $vote->title_en             = Request::get('title_en');
                $vote->description_th       = Request::get('description_th');
                $vote->description_en       = Request::get('description_en');
                $vote->created_by           = Auth::user()->id;
                $vote->property_id          = Auth::user()->property_id;
                $action = "create";
            }

            $vote->fill(Request::all());
            $vote->updated_by = Auth::user()->id;

            if(Request::get('publish_status')) {
                $vote->publish_status = true;
            }

            if(Request::get('remove-banner-flag') && $vote->banner_url) {
                $this->removeBannerFile($vote->banner_url);
                $vote->banner_url = $banner_path = null;
            }

            if (!empty(Request::get('img_post_banner'))) {
                $file = Request::get('img_post_banner');
                $name 	= $file['name'];
                $x 		= Request::get('img-x');
                $y 		= Request::get('img-y');
                $w 		= Request::get('img-w');
                $h 		= Request::get('img-h');
                cropBannerImg ($name,$x,$y,$w,$h);
                $path = $this->createLoadBalanceDir($file['name']);
                $tempUrl = "/%s%s";
                $vote->banner_url = sprintf($tempUrl, $path, $file['name']);
            }

            $vote->save();
              //dd($vote);

            if(!empty(Request::get('attachment'))) {
                $votefile = [];
                foreach (Request::get('attachment') as $file) {
                    //Move Image
                    $path = $this->createLoadBalanceDir($file['name']);
                    $votefile[] = new VoteFile([
                            'name' => strtolower($file['name']),
                            'url' => $path,
                            'file_type' => $file['mime'],
                            'is_image'	=> $file['isImage'],
                            'original_name'	=> strtolower($file['originalName'])
                        ]
                    );
                }
                $vote->voteFile()->saveMany($votefile);
            }
            if(!empty(Request::get('choice'))) {
                $order = 0;
                foreach (Request::get('choice') as $choice) {
                    $choices[] = new Choice (['title' => $choice,'order_choice'=>$order]);
                    $order++;
                }
                $vote->voteChoice()->saveMany($choices);
            }

            if(!empty(Request::get('remove'))) {
                $remove = Request::get('remove');
                // Remove old choices
                if(!empty($remove['choice']))
                    Choice::whereIn('id', $remove['choice'])->delete();
                // Remove old files
                if(!empty($remove['vote-file']))
                    foreach ($remove['vote-file'] as $file) {
                        $file = VoteFile::find($file);
                        $this->removeFile($file->name);
                        $file->delete();
                    }
            }

            if(Request::get('publish_status')){
                if($old_publish_status) {
                    //
                }else{
                    $this->addCreateVoteNotification($vote);
                }
            }else{
                // Nothing to do
            }

            // system log
            if(  $action == "create" ) {
                $this->systemLog(Auth::user(), 'PA', 'C', 'V', $vote->id);
            } else {
                $this->systemLog(Auth::user(), 'PA', 'U', 'V', $vote->id);
            }

        }

        return redirect('admin/survey');
    }

	public function view ($id) {
		$this->markAsRead($id);
		$vote = Vote::with(['creator','voteChoice'=> function ($query) {
			$query->orderBy('choice.order_choice', 'asc');

		},'voteFile','userChoose' => function($query){
						    $query->where('user_id', '=', Auth::user()->id);
						}])
						->find($id);

		return view('vote.view')->with('vote', $vote);
	}

	public function vote () {
		if(Request::isMethod('post')) {
			$voted = UserChoice::where('user_id',Auth::user()->id)->where('vote_id',Request::get('vote_id'))->count();
			if($voted == 0) {
				$choice = Choice::find(Request::get('vote_choice'));
				$user_vote = new UserChoice;
				$user_vote->vote_id = Request::get('vote_id');
				$user_vote->choice_id = Request::get('vote_choice');
				$user_vote->user_id = Auth::user()->id;
				$user_vote->save();
				// choice count increment
				++$choice->choice_count;
				$choice->save();
			}
		}
		return redirect('admin/votes/view/'.Request::get('vote_id'));

	}

	public function delete () {
        if(Request::ajax()) {
            $id = Request::get('vid');
            $vote = Vote::with('creator', 'voteFile')->find($id);
            if ($vote) {
                //remove post report
                $vote->userChoose()->delete();
                $vote->voteChoice()->delete();
                if (!$vote->voteFile->isEmpty()) {
                    foreach ($vote->voteFile as $file) {
                        $this->removeFile($file->name);
                    }
                    $vote->voteFile()->delete();
                }
                if($vote->banner_url) {
                    $this->removeBannerFile($vote->banner_url);
                }

                $notification = Notification::where('subject_key',$id)->get();
                if($notification->count()) {
                    foreach ($notification as $noti) {
                        $noti->delete();
                    }
                }

                $vote->delete();

                $this->systemLog(Auth::user(), 'PA', 'D', 'V', $vote->id);

                return response()->json(['status' => true]);
            }
        }
	}

    public function deleteViaUrl ($id) {
        $vote = Vote::with('creator','voteFile')->find($id);
        if($vote && $vote->creator->id == Auth::user()->id) {
            //remove post report
            $vote->userChoose()->delete();
            $vote->voteChoice()->delete();
            if(!$vote->voteFile->isEmpty()) {
                foreach ($vote->voteFile as $file) {
                    $this->removeFile($file->name);
                }
                $vote->voteFile()->delete();
            }
            $vote->delete();
        }
        return redirect('admin/votes');
    }

	public function getAttach ($id) {
		$file = VoteFile::find($id);
        $file_path = 'vote-file'.'/'.$file->url.$file->name;
        $exists = Storage::disk('s3')->has($file_path);
        if ($exists) {
            $response = response(Storage::disk('s3')->get($file_path), 200, [
                'Content-Type' => $file->file_type,
                'Content-Length' => Storage::disk('s3')->size($file_path),
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename={$file->original_name}",
                'Content-Transfer-Encoding' => 'binary',
            ]);
            ob_end_clean();
            return $response;
        }
	}

	public function createLoadBalanceDir ($name) {
		$targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
		$folder = substr($name, 0,2);
		$pic_folder = 'vote-file/'.$folder;
        $directories = Storage::disk('s3')->directories('vote-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".strtolower($name);
        $upload = Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
		return $folder."/";
	}

	public function removeFile ($name) {
		$folder = substr($name, 0,2);
		$file_path = 'vote-file'.'/'.$folder.'/'.$name;
        Storage::disk('s3')->delete($file_path);
	}

    public function removeBannerFile ($name) {
        $file_path  = "vote-file/".$name;
        $exists     = Storage::disk('s3')->has($file_path);
        if ($exists) {
            Storage::disk('s3')->delete($file_path);
        }
    }

	/*public function addCreateVoteNotification_backup($vote) {
		$users = User::where('property_id',Auth::user()->property_id)->whereNotIn('id', [Auth::user()->id])->get();
		if($users->count()) {
			$title = json_encode( ['type'=>'vote_created','title'=>$vote->title] );
			foreach ($users as $user) {
				$notification = Notification::create([
					'title'				=> $title,
					'description' 		=> "",
					'notification_type' => 5,
					'subject_key'		=> $vote->id,
					'to_user_id'		=> $user->id,
					'from_user_id'		=> Auth::user()->id,
                    'property_id'       => $vote->property_id,
                    'property_unit_id'  => $vote->property_unit_id
				]);
				$controller_push_noti = new PushNotificationController();
        		$controller_push_noti->pushNotification($notification->id);
			}

		}
	}*/

	public function addCreateVoteNotification($vote){
        $notification = [];
        $push_notification = [];
        $user_property = UserProperty::where('property_id',Auth::user()->property_id)->where('approve_status',1)->whereNotIn('id', [Auth::user()->id])->get();
        foreach ($user_property as $item_user){
            $user_id = $item_user->user_id;
            $user = User::find($user_id);

            if(isset($user)){
                $title = json_encode( ['type'=>'vote_created','title'=>'', 'title_th'=>$vote->title_th, 'title_en'=>$vote->title_en] );

                if($user->property_unit_id == $item_user->property_unit_id){
                    // User on Current Property
                    $push_notification[] = Notification::create([
                        'title' => $title,
                        'description' => "",
                        'notification_type' => 6,
                        'subject_key' => $vote->id,
                        'to_user_id' => $user->id,
                        'from_user_id' => Auth::user()->id,
                        'property_id' => Auth::user()->property_id,
                        'property_unit_id' => $item_user->property_unit_id
                    ]);
                }else {
                    $notification[] = Notification::create([
                        'title' => $title,
                        'description' => "",
                        'notification_type' => 6,
                        'subject_key' => $vote->id,
                        'to_user_id' => $user->id,
                        'from_user_id' => Auth::user()->id,
                        'property_id' => Auth::user()->property_id,
                        'property_unit_id' => $item_user->property_unit_id
                    ]);
                }
            }
        }
        if( !empty($push_notification) ) {
            $controller_push_noti = new PushNotificationController();
            $controller_push_noti->pushNotificationArray($push_notification);
        }
    }

	public function getform () {
		$vote = Vote::with('created_user','updated_user')->find(Request::get('id'));
		return view('vote.edit')->with(compact('vote'));
	}

	public function markAsRead ($id) {
		try {
			$notis_counter = Notification::where('subject_key', '=', $id)->where('to_user_id', '=', Auth::user()->id)->get();
			if ($notis_counter->count() > 0) {
				$notis = Notification::find($notis_counter->first()->id);
				$notis->read_status = true;
				$notis->save();
			}
			return true;
		}catch(Exception $ex){
			return false;
		}
	}

    public function exportVotes () {
        $votes = Vote::with('creator','voteChoice')->where('property_id',Auth::user()->property_id);
        if( Request::get('publish_status') != "-") {
            $votes = $votes->where('publish_status',Request::get('publish_status'));
        }

        if( Request::get('created_date') ) {
            $votes = $votes->where(DB::raw("DATE(created_at)"),'=',date('Y-m-d',strtotime(Request::get('created_date'))));
        }
        $votes = $votes->orderBy('vote.start_date','desc')->get();
        $property = Auth::user()->property;
        $filename = "รายการแบบสอบถามในนิติบุคคล";

        Excel::create($filename, function ($excel) use ($votes, $property) {
            $excel->sheet("Events", function ($sheet) use ($votes, $property) {
                $sheet->setWidth(array(
                    'A' => 10,
                    'B' => 40,
                    'C' => 60,
                    'D' => 25,
                    'E' => 25,
                    'F' => 15,
                    'G' => 15,
                    'H' => 15,
                    'I' => 15,
                    'J' => 25,
                    'K' => 15,
                ));
                $sheet->loadView('vote.export')->with(compact('votes','property'));
            });
        })->export('xlsx');
    }
}
