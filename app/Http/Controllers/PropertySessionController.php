<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;

class PropertySessionController extends Controller
{

    public function __construct() {
        $this->middleware('property-session');
    }
    public function directLogin ($id, Request $r) {
        $user = User::find(Auth::user()->id);
        $user->smart_admin_state = true;
        $user->property_id = $id;
        $user->save();
        if( $r->get('menu') ) {
            switch ( $r->get('menu') ) {
                case 'new_user' :
                    return redirect('/admin/property/new-members');
                break;
                case 'chat' :
                    return redirect('/admin/messages');
                break;
                case 'posts-parcels' :
                    return redirect('/admin/parcel-tracking');
                break;
                default :
                    return redirect('/admin/dashboard');
                break;
            }
        }
        if( Auth::user()->role == 7) {
            return redirect('call-center/admin/property/units');
        } else {
            return redirect('/admin/dashboard');
        }
    }

    public function restoreSession () {
        $user = User::find(Auth::user()->id);
        $user->smart_admin_state = false;
        $user->property_id = null;
        $user->save();
        return redirect('/');
    }
}
