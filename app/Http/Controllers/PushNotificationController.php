<?php namespace App\Http\Controllers;
use Auth;
use Request;

use Illuminate\Support\MessageBag;
use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;
use PushNotification;

# Model
use App\Property;
use App\User;
use App\Installation;
use App\Notification;

# Jobs
use App\Jobs\PushNotificationUserAndroid;
use App\Jobs\PushNotificationUserIOS;
use App\Jobs\PushNotificationSender;
use App\Jobs\PushNotificationToDevice;
use App\Jobs\PushNotificationToDeviceSetCommittee;
use App\Jobs\PushNotificationToDeviceMessage;
use App\Jobs\PushNotificationToDeviceUpdateStatusMajor;
use App\Jobs\PushNotificationToDeviceSendMarketing;
use App\Jobs\PushNotificationToDeviceSendGroupMarketing;

class PushNotificationController extends Controller {

    use DispatchesJobs;

    public function __construct () {
    }

    public function pushNotification($notification_id){
        $this->dispatch(new PushNotificationToDevice($notification_id));
    }

    public function pushNotificationSetCommitter($user_id,$status){
        $this->dispatch(new PushNotificationToDeviceSetCommittee($user_id,$status));
    }

    public function pushNotificationMessageSend($user_id){
        $this->dispatch(new PushNotificationToDeviceMessage($user_id));
    }

    public function pushNotificationArray($notification_array){
        foreach ($notification_array as $notification) {
            $this->dispatch(new PushNotificationToDevice($notification->id));
        }
    }

    public function pushNotificationUpdateStatusMajor($user_id,$job_id,$status,$notification_payload){
        $this->dispatch(new PushNotificationToDeviceUpdateStatusMajor($user_id,$job_id,$status,$notification_payload));
    }

    public function PushNotificationToDeviceSendMarketing($notification_type,$subject_id,$lang){
        $this->dispatch(new PushNotificationToDeviceSendMarketing($notification_type,$subject_id,$lang));
    }

    public function PushNotificationToDeviceSendGroupMarketing($notification_key,$notification_type,$subject_id,$lang){
        $this->dispatch(new PushNotificationToDeviceSendGroupMarketing($notification_key,$notification_type,$subject_id,$lang));
    }

}
