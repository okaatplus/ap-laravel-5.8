<?php namespace App\Http\Controllers;
use Request;
use Auth;
use Hash;
use Redirect;
use Mail;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;

# Model
use DB;
use App\User;
use App\Property;
use App\PropertyUnit;
use App\RequestedProperty;
use App\Province;

#Jobs
use App\Jobs\SendEmailUser;

class QueueJobTestController extends Controller {

    use DispatchesJobs;

	public function __construct () {

	}

    // test send email
    public function testemailjob(){

        $email = 'sjinadech@gmail.com';

        //SendEmailUser::dispatch('Suttipong Jinadech','Test Job Mails', 'sjinadech@gmail.com'); // Dispatch When run command php artisan queue:work --queue=high,default
        //SendEmailUser::dispatchNow('Suttipong Jinadech','Test Job Mails', 'sjinadech@gmail.com'); // Dispatch Now

        $email_list = ['sjinadech@gmail.com','suttipong.co@gmail.com','suttipong@o-kaatplus.com','sjinadech@gmail.com','suttipong.co@gmail.com','suttipong@o-kaatplus.com'];
        // test looping

        foreach ($email_list as $key=>$item){

            SendEmailUser::dispatch('Suttipong '.$key,'Test Job Mails'.$key, $item)->onQueue('sendEmailUser');
            //SendEmailUser::dispatch('Suttipong '.$key,'Test Job Mails'.$key, $item);
            //SendEmailUser::dispatchNow('Suttipong '.$key,'Test Job Mails'.$key, $item);

        }

        return "true";
    }
}
