<?php namespace App\Http\Controllers\RootAdmin;
use Request;
use Auth;
use Redirect;
use Illuminate\Routing\Controller;
use App;
use App\Http\Controllers\Officer\AccountController;
# Model
use DB;
use App\Page;
class PagesController extends controller {

    public function __construct () {
        $this->middleware('root-admin');
    }

    public function editPage($alias) {
        @session_start();
        $_SESSION['allow_upload_kc'] = true;
        $page = Page::where('alias',$alias)->first();

        $page_name = ($alias == 'terms')?'แก้ไขหน้า Terms and Conditions':'แก้ไขหน้า Privacy';
        return view('pages.page_helps_edit')->with(compact('page','page_name'));
    }

    public function edit () {
        if(Request::isMethod('post')) {
            $page = Page::find(Request::get('id'));
            $page_name = "";
            if($page){
                $page = Page::find(Request::get('id'));
                $page->content_en = Request::get('content_en');
                $page->content_th = Request::get('content_th');
                $page->save();
                $page_name = ($page->alias == 'terms')?'แก้ไขหน้า Terms and Conditions':'แก้ไขหน้า Privacy';
            }
            return redirect()->back()->withInput()->with(compact('page_name'));
        }
    }
}