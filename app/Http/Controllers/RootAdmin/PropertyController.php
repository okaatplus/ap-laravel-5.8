<?php namespace App\Http\Controllers\RootAdmin;;

use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Routing\Controller;
# Model
use App\Property;
use App\Province;

class PropertyController extends Controller {

    protected $app;

    public function __construct () {
        $this->middleware('root-admin');
    }

    public function index (Request $r)  {
        
        $props = new Property;

        if(Request::get('province')) {
            $props = $props->where('province','=',Request::get('province'));
        }

        if(Request::get('name')) {
            $props = $props->where(function ($q) {
                $q ->where('property_name_th','like',"%".Request::get('name')."%")
                    ->orWhere('property_name_en','like',"%".Request::get('name')."%");
            });
        }

        $p_rows = $props->orderBy('created_at','desc')->paginate(50);
        $p = new Province;
        $provinces = $p->getProvince();
        if(!Request::ajax()) {
            $property_list = array(''=> trans('messages.Signup.select_property') );
            return view('property.list')->with(compact('p_rows','provinces','property_list'));
        } else {
            return view('property.list-element')->with(compact('p_rows','provinces'));
        }

    }

    public function generateCsvIndex ($pid) {
        return view('mdm_csv.csv_index')->with(compact('pid'));
    }

    public function generatePropertyCsv () {

        $property = Property::get();
        return view('mdm_csv.property_csv')->with(compact('property'));
    }

    public function generatePropertyUnitCsv ($pid) {
        $property_unit = \App\PropertyUnit::where('property_id', $pid)->where('active',true)->get();
        return view('mdm_csv.property_unit_csv')->with(compact('property_unit'));
    }

    public function generateCustomerCsv ($pid) {
        $customer = \App\Customer::whereHas('customerProperty', function ($q) use($pid) {
            $q->where('property_id',$pid);
        })->get();
        return view('mdm_csv.customer_csv')->with(compact('customer'));
    }

    public function generateCustomerPropertyCsv ($pid) {
        $c_p = \App\CustomerProperty::where('property_id',$pid)->get();
        return view('mdm_csv.customer_property_csv')->with(compact('c_p'));
    }

    public function generateTenantCsv ($pid) {
        $tenant = \App\Tenant::whereHas('tenantProperty', function ($q) use($pid) {
            $q->where('property_id',$pid);
        })->get();
        return view('mdm_csv.tenant_csv')->with(compact('tenant'));
    }

    public function generateTenantPropertyCsv ($pid) {
        $t_p = \App\TenantProperty::where('property_id',$pid)->get();
        return view('mdm_csv.tenant_property_csv')->with(compact('t_p'));
    }

    public function joinCustomerRef () {
        $this->joinCustomerRefByName(); /*
        $customer = \App\Customer::with('customerRef')->paginate(1000);
        echo "<table>";
        foreach ($customer as $c) {
            echo "<tr>";
            echo "<td>".$c->id_card."</td>";
            echo "<td>".$c->name."</td>";
            echo "<td>".(($c->customerRef)?$c->customerRef->customer_code:"")."</td>";
            echo "</tr>";

            if( !$c->customer_code && $c->customerRef ) {
                $c_ = \App\Customer::find($c->id);
                $c_->customer_code = $c->customerRef->customer_code;
                $c_->save();
            }
        }
        echo "<tr>";
        echo "<td>";
        if( $customer->previousPageUrl() ) {
            echo "<a href='".$customer->previousPageUrl()."'><<< Prev</a>";
        }
        echo "</td>";
        echo "<td></td>";
        echo "<td>";
        if( $customer->nextPageUrl() ) {
            echo " <a href='".$customer->nextPageUrl()."'>Next >>></a>";
        }
        echo "</td>";
        echo "</tr>";
        echo "<table>"; */
    }

    public function joinCustomerRefByName () {
        $customer = DB::table('customer')/*->leftJoin('customer_ref', function($join)
        {
            $join->on('customer.id_card', '=', 'customer_ref.id_card');

        })*/
        ->leftJoin('customer_ref as customer_ref', function($join) {
            $join->on('customer.name', '=', 'customer_ref.name');
        })
        //->select('customer.id','customer.name','customer.id_card','customer_ref.customer_code','customer_ref_2.customer_code')
        ->select('customer.id','customer.name','customer.customer_code','customer.id_card','customer_ref.customer_code as ref_c_code')
        ->get();
        //->paginate(10);
        //dd( $customer->toArray() );
        echo "<table>";
        foreach ($customer as $c) {
            if( $c->ref_c_code != $c->customer_code) {
                echo "<tr>";
                echo "<td>".$c->id_card."</td>";
                echo "<td>".$c->name."</td>";
                echo "<td>".$c->customer_code."</td>";
                echo "<td>".$c->ref_c_code."</td>";
                echo "</tr>";
            }

            //$c->customer_code = $c->customer_code;
            //if( $c->customerRef ) {
            /*if( !$c->customer_code ) {
                $c_ = \App\Customer::find($c->id);
                $c_->customer_code = $c->ref_c_code;
                $c_->save();
            }*/

            //}

        }
        echo "<tr>";
        echo "<td>";
        //if( $customer->previousPageUrl() ) {
        //    echo "<a href='".$customer->previousPageUrl()."'><<< Prev</a>";
        //}
        echo "</td>";
        echo "<td></td>";
        echo "<td>";
        //if( $customer->nextPageUrl() ) {
        //    echo " <a href='".$customer->nextPageUrl()."'>Next >>></a>";
        //}
        echo "</td>";
        echo "</tr>";
        echo "<table>";
        //dd($customer);
    }
}
