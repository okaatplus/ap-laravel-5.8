<?php

namespace App\Http\Controllers\Smart;

use App\UserProperty;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Property;
use App\Message;
use App\PostParcel;
use App\User;
use App\Customer;

class DashboardController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }

    public function index (Request $r) {

        $new_messages_count     = new Message;
        $post_parcels_count     = new PostParcel;
        $unregister_user_count  = new Customer;
        $waiting_for_approve    = new UserProperty();

        $last_14_days = Carbon::now();
        $last_14_days = $last_14_days->subDays(14)->format('Y-m-d');

        if( Auth::user()->role == 2 || Auth::user()->role == 6) {
            $_asp = DB::table('admin_property')->where('user_id',Auth::user()->id)->pluck('property_id');
            if( $_asp ) {
                $_asp = $_asp->toArray();
            }
            $new_messages_count = $new_messages_count->whereIn('property_id', $_asp);
            $post_parcels_count = $post_parcels_count->whereIn('property_id', $_asp);
            $unregister_user_count = $unregister_user_count->whereHas('customerProperty', function ($q) use ( $_asp ) {
                $q->whereIn('property_id', $_asp);
            });

            $waiting_for_approve = $waiting_for_approve->whereIn('property_id', $_asp);
        }
        // count messages
        $new_messages_count = $new_messages_count->where('flag_new_from_user', true)->count();
        // count parcel
        $post_parcels_count = $post_parcels_count->where('date_received','<=',$last_14_days)->where('status',false)->count();
        // count unregister
        $registered_user = User::where('role',8)->whereNotNull('customer_id')->pluck('customer_id')->toArray();
        // count waiting approve
        $waiting_for_approve = $waiting_for_approve->where('approve_status',0)->count();
        $unregister_user_count = $unregister_user_count->whereNotIn('id',$registered_user)->count();
        $registered_user = count($registered_user);
        $this->getUnResponseMessageList();
        return view('dashboard.smart.index')->with(compact('new_messages_count','post_parcels_count','unregister_user_count','registered_user','waiting_for_approve'));
    }

    public function unResponseMessage () {

        $this->getUnResponseMessageList();
        return view('dashboard.smart.un-response-msg-list');
    }

    public function unResponseMessageList () {

        $this->getUnResponseMessageList();
        return view('dashboard.smart.un-response-msg-list-element');
    }

    public function getUnResponseMessageList () {

        $new_messages = Property::withCount('unReadMessages')->whereHas('unReadMessages', function ($q) {
            $q->where('flag_new_from_user', true);
        });
        if( Auth::user()->role == 2 || Auth::user()->role == 6) {
            $_asp = DB::table('admin_property')->where('user_id',Auth::user()->id)->pluck('property_id');
            if( $_asp ) {
                $_asp = $_asp->toArray();
            }
            $new_messages = $new_messages->whereIn('id', $_asp);
        }
        // count messages
        $new_messages = $new_messages->orderBy('un_read_messages_count', 'desc')->paginate(5);
        view()->share(compact('new_messages'));
    }

    public function overduePostsParcels () {
        $this->getOverduePostsParcelsList();
        return view('dashboard.smart.posts-parcels-list');
    }

    public function overduePostsParcelsList () {
        $this->getOverduePostsParcelsList();
        return view('dashboard.smart.posts-parcels-list-element');
    }

    public function getOverduePostsParcelsList () {

        $last_14_days = Carbon::now();
        $last_14_days = $last_14_days->subDays(14)->format('Y-m-d');

        $posts_parcels = Property::withCount('overduePostsParcels')->whereHas('postsParcels', function ($q) use($last_14_days)  {
            $q->where('date_received','<=',$last_14_days)->where('status',false);
        });

        if( Auth::user()->role == 2 || Auth::user()->role == 6) {
            $_asp = DB::table('admin_property')->where('user_id',Auth::user()->id)->pluck('property_id');
            if( $_asp ) {
                $_asp = $_asp->toArray();
            }
            $posts_parcels = $posts_parcels->whereIn('id', $_asp);
        }
        // count messages
        $posts_parcels = $posts_parcels->orderBy('overdue_posts_parcels_count', 'desc')->paginate(5);
        view()->share(compact('posts_parcels'));
    }

    public function unregisterUsers () {
        $this->getUnregisterUsersList();
        return view('dashboard.smart.unregister-users-list');
    }

    public function unregisterUsersList () {
        $this->getUnregisterUsersList();
        return view('dashboard.smart.unregister-users-list-element');
    }

    public function getUnregisterUsersList () {

        $unregister_user_count = new Property;
        if( Auth::user()->role == 2 || Auth::user()->role == 6) {
            $_asp = DB::table('admin_property')->where('user_id',Auth::user()->id)->pluck('property_id');
            if( $_asp ) {
                $_asp = $_asp->toArray();
            }
            $unregister_user_count = $unregister_user_count->withCount('waitingApprovalUser')->whereHas('userProperty', function ($q) {
                $q->where('approve_status',0);
            })->whereIn('id', $_asp);

        } else {

            $unregister_user_count = $unregister_user_count->withCount('waitingApprovalUser')->whereHas('userProperty', function ($q) {
                $q->where('approve_status',0);
            });
        }
        $unregister_user_count = $unregister_user_count->orderBy('waiting_approval_user_count', 'desc')->paginate(5);

        view()->share(compact('unregister_user_count'));
    }
}