<?php namespace App\Http\Controllers\SmartHQAdmin;

use Request;
use Illuminate\Routing\Controller;
use Auth;
use Redirect;
use Validator;
# Model
use DB;
use App\User;
use App\Property;
use App\AdminProperty;

class AdminSystemController extends Controller {

    public function __construct () {
        $this->middleware('smart-hq');
    }

    public function adminList() {
        $officer = new User;
        $property_in = AdminProperty::where('user_id', Auth::user()->id)->pluck('property_id')->toArray();
        $officers = User::where('id','!=',Auth::user()->id)
            ->where('deleted',false);
        
        if( Request::get('name') ) {
            $officers = $officers->where('name', 'like', "%" . Request::get('name') . "%");
        }

        if( Request::get('role') && Request::get('role') != "-") {
            $officers = $officers->where('role',  Request::get('role'));
        } else {
            $officers = $officers->whereIn('role',[3,4,5,7]);
        }

        if( Request::get('active') && Request::get('active') != "-" ) {
            $a = Request::get('active') == 't'?true:false;
            $officers = $officers->where('active', $a);
        }

        if( Request::get('property') ) {
            $pid = Request::get('property');
            $officers = $officers->where(function ($q) use ($pid) {
                $q  ->where('property_id', $pid)
                    ->orWhere( function ($q) use ($pid) {
                        $q->whereHas('AdminProperty', function ($q) use ($pid) {
                            $q->where('property_id',$pid);
                        });
                    });
            });
        } else {
            $officers = $officers->where(function ($q) use ( $property_in ) {
                $q  ->whereIn('property_id', $property_in)
                    ->orWhere( function ($q) use ($property_in) {
                        $q->whereHas('AdminProperty', function ($q) use ($property_in) {
                            $q->whereIn('property_id',$property_in);
                        });
                    });
            });
        }

        $officers =  $officers->orderBy('active','DESC')->orderBy('created_at','DESC')->paginate(30); 
        if(Request::ajax()) {
            return view('smart-hq.officer.officer-list')->with(compact('officers'));
        } else {
            $selected_p = [];
            $p_list = Property::whereIn('id',$property_in)->pluck('property_name_th','id')->toArray();
            $fixed_account = true;
            return view('smart-hq.officer.view-officer-list')->with(compact('officers','officer','p_list','selected_p','fixed_account'));
        }

    }

    public function addAdmin() {

        if (Request::isMethod('post')) {
            $officer = Request::all();
            $validator = Validator::make($officer, [
                'name' => 'required|max:255',
                'email' => 'required|unique:users',
                'password' => 'alpha_num|min:6|required',
                'password_confirm' => 'alpha_num|min:6|required|same:password'
            ]);

            if ($validator->fails()) {
                $selected_p = Request::get('property_id_');
                $fixed_account = $this->checkIsFixedAccount($officer['role']);
                $property_in = AdminProperty::where('user_id', Auth::user()->id)->pluck('property_id')->toArray();
                $p_list = Property::whereIn('id',$property_in);

                if( $officer['admin_property_dimension'] ) {
                    $p_list = $p_list->where('dimension',$officer['admin_property_dimension']);
                }
                $p_list = $p_list->pluck('property_name_th','id')->toArray();
                return view('smart-hq.officer.officer-form')->withErrors($validator)->with(compact('officer','p_list','selected_p','fixed_account'));
            } else {
                $user = $this->createAccount($officer['name'], $officer['email'], $officer['phone'], bcrypt($officer['password']),$officer['admin_property_dimension']);
                $user->role = Request::get('role');

                $fixed_account = $this->checkIsFixedAccount($user->role);

                if($fixed_account) {
                    $user->property_id = Request::get('user_property_id');
                    $user->save();
                } else {
                    $user->save();
                    $this->saveProperty($user);
                }
                echo "saved";
            }
        }
    }

    public function editAdmin() {
        if (Request::isMethod('post')) {
            $officer = User::find(Request::get('id'));
            $request = Request::except('email');

            $rules = ['name' => 'required|max:255'];
            $except = ['email','id'];
            if(!empty($request['password'])) {
                $rules += [
                    'password' => 'alpha_num|min:6|required',
                    'password_confirm' => 'alpha_num|min:6|required|same:password'
                ];
            } else {
                $except[] = 'password';
            }
            $validator = Validator::make($request, $rules);
            $officer->fill(Request::except($except));
            $officer->role = Request::get('role');

            $fixed_account = $this->checkIsFixedAccount($officer->role);

            if ($validator->fails()) {
                $selected_p = Request::get('property_id_');

                $p_list = Property::pluck('property_name_th','id')->toArray();
                return view('smart-hq.officer.officer-form-edit')->withErrors($validator)->with(compact('officer','selected_p','p_list','fixed_account'));
            }else {
                $officer->name = $request['name'];
                if(!empty($request['password'])) {
                    $officer->password = bcrypt($request['password']);
                }

                if($fixed_account) {
                    $officer->load('AdminProperty');
                    $officer->AdminProperty()->delete();
                    $officer->property_id = Request::get('user_property_id');
                } else {
                    $this->saveProperty($officer);
                }

                $officer->save();
                echo "saved";
            }
        }
    }

    public function deleteAdmin(){
        try{
            if(Request::ajax()) {
                $user = User::whereIn('role',[0,1,2])->where('id',Request::get('id'))->first();
                if($user) {
                    $user->email = null;
                    $user->deleted = true;
                    $user->active = false;
                    $user->save();
                    return response()->json(['result'=>true]);
                }
            }else{
                return response()->json(['result'=>false]);
            }
        }catch(Exception $ex){
            return response()->json(['result'=>false]);
        }
    }

    public function viewAdmin () {
        if(Request::ajax()) {
            $member = User::with('AdminProperty')->find(Request::get('uid'));
            return view('smart-hq.officer.view-officer')->with(compact('member'));
        }
    }

    public function getAdmin () {
        if(Request::ajax()) {
            $officer = User::with('AdminProperty')->find(Request::get('uid'));
            $selected_p = [];
            if( $officer ) {
                $selected_p = AdminProperty::where('user_id',$officer->id)->pluck('property_id')->toArray();
            }
            $fixed_account = $this->checkIsFixedAccount($officer->role);
            $property_in = AdminProperty::where('user_id', Auth::user()->id)->pluck('property_id')->toArray();
            $p_list = Property::whereIn('id',$property_in);

            if( $officer->admin_property_dimension ) {
                $p_list = $p_list->where('dimension',$officer->admin_property_dimension);
            }
            $p_list = $p_list->pluck('property_name_th','id')->toArray();
            return view('smart-hq.officer.officer-form-edit')->with(compact('officer','selected_p','p_list','fixed_account'));
        }
    }

    public function createAccount($name,$email,$phone,$password,$dimension = null){
        try{
            $user_create = User::create([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'password' => $password,
                'admin_property_dimension' => $dimension
            ]);

            return $user_create;

        }catch(Exception $ex){
            return false;
        }
    }
    public function saveProperty ( $user ) {
        //dd(Request::get('property_id_'));
        if( !empty(Request::get('property_id_'))) {
            if( $user->AdminProperty ) {
                $user->AdminProperty()->delete();
            }
            foreach (Request::get('property_id_') as $pid ) {
                $user_property = new AdminProperty;
                $user_property->user_id = $user->id;
                $user_property->property_id = $pid;
                $user_property->save();
            }
        }

    }
    public function saveMarketingRoleAccess ($user) {
        /*$access = MarketingRoleAccess::where('user_id',$user->id)->first();
        if( !$access ) $access = new MarketingRoleAccess;
        $access->user_id    = $user->id;
        $access->project    = Request::get('project')?Request::get('project'):0;
        $access->promotion  = Request::get('promotion')?Request::get('promotion'):0;
        $access->life_style = Request::get('life_style')?Request::get('life_style'):0;
        $access->privilege  = Request::get('privilege')?Request::get('privilege'):0;
        $access->save();*/
        return true;
    }

    public function changeStatusAdmin(){
        if ( Request::isMethod('post') ) {
            $app_key = Request::get('app_key');
            if($app_key == $_ENV['APP_KEY']) {
                $user = Request::get('user');
                $officer = User::where('email',$user['email'])->first();
                if($officer) {
                    $officer->active = ($user['status'] == "1") ? true : false;
                    $officer->save();
                }
            }
        }
    }

    public function setActive () {
        if(Request::ajax()) {
            $user = User::find(Request::get('uid'));
            if($user) {
                $user->active = Request::get('status');
                $user->save();

                return response()->json(['result'=>true]);
            }
        }
    }

    function generatePassword() {
        $chars = "abcdefghijkmnpqrstuvwxyz123456789";
        $i = 0;
        $pass = '' ;
        while ($i < 6) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    protected function checkIsFixedAccount ($role) {
        return in_array($role,[3,4,5]);
    }
}
