<?php

namespace App\Http\Controllers\SmartSupervisor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Property;
use App\Province;
use App\AdminProperty;

class PropertyController extends Controller
{
    public function index(Request $r)
    {

        $props = new Property;
        if ($r->get('province')) {
            $props = $props->where('province', '=', $r->get('province'));
        }

        if ($r->get('name')) {
            $props = $props->where(function ($q) use ($r) {
                $q->where('property_name_th', 'like', "%" . $r->get('name') . "%")
                    ->orWhere('property_name_en', 'like', "%" . $r->get('name') . "%")
                    ->orWhere('juristic_person_name_th', 'like', "%" . $r->get('name') . "%")
                    ->orWhere('juristic_person_name_en', 'like', "%" . $r->get('name') . "%");
            });
        }
        $property_in = AdminProperty::where('user_id', Auth::user()->id)->pluck('property_id')->toArray();
        $p_rows = $props->whereIn('id', $property_in)->orderBy('created_at')->paginate(50);
        $p = new Province;
        $provinces = $p->getProvince();
        if (!$r->ajax()) {
            $property_list = array('' => trans('messages.Signup.select_property'));
            return view('property.list')->with(compact('p_rows', 'provinces', 'property_list'));
        } else {
            return view('property.list-element')->with(compact('p_rows', 'provinces'));
        }
    }

    function directLogin($id)
    {
        $user = User::find(Auth::user()->id);
        $user->smart_admin_state = true;
        $user->property_id = $id;
        $user->save();
        return redirect('/admin/news-announcement');
    }
}

