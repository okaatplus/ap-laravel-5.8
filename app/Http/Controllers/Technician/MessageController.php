<?php namespace App\Http\Controllers\Technician;
use Auth;
use Request;
use Illuminate\Routing\Controller;
# Model
use App\User;
use App\Message;
use App\MessageText;
use DB;


class MessageController extends Controller {

	public function __construct () {
		$this->middleware('technician');
		view()->share('active_menu', 'messages');
	}

	public function index () {
        $latest_message = Message::with(['owner','owner.ActiveUnit' => function ($q) {
            $q->where('property_id',Auth::user()->property_id);
        }])
            ->whereHas('hasText')
            ->where('property_id',Auth::user()->property_id)->orderBy('updated_at', 'desc')->orderBy('last_user_message_date','desc')->paginate(20);
        if(Request::ajax()) {
			return view('technician.message.admin-user-message-list')->with(compact('latest_message'));
		} else {
			return view('technician.message.admin-index')->with(compact('latest_message'));
		}
	}

	public function view ($id) {
		$message 	= Message::where('property_id',Auth::user()->property_id)->where('id',$id)->first();
		if( $message ) {
			$messageTexts = MessageText::where('message_id',$id)->orderBy('created_at','desc')->paginate(5);
		}
		if(Request::ajax()) {
			return view('technician.message.admin-message-page')->with(compact('messageTexts','message'));
		} else {
			if($message) {
				$flag_new_message = Message::where('property_id',Auth::user()->property_id)->where('flag_new_from_user',true)->first();
				$flag_new_message = $flag_new_message?true:false;
				view()->share('flag_new_message',$flag_new_message);
			} else {
			    return redirect()->back();
            }
            $user = User::with(['ActiveUnit' => function ($q) {
                $q->where('property_id', Auth::user()->property_id);
            }])->find($message->user_id);

            $log = new \App\MessageViewLog;
            $log->saveLog ($message->id,Auth::user()->id);

			return view('technician.message.admin-view-message')->with(compact('messageTexts','message','user'));
		}
	}

	public function messagePage () {
		$message 	= Message::find(Request::get('mid'));
		if( $message ) {
			$messageTexts = MessageText::where('message_id',Request::get('mid'))->orderBy('created_at','desc')->paginate(5);
			return view('technician.message.admin-message-page')->with(compact('messageTexts','message'));
		}
	}

	public function cutText ($text) {
		if(strlen($text) > 80 ) {
			return mb_substr($text,0,80)."...";
		} else return $text;
	}
}
