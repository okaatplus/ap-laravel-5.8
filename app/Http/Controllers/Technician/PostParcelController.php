<?php namespace App\Http\Controllers\Technician;
use Auth;
use Carbon\Carbon;
use Request;
use Illuminate\Routing\Controller;
# Model
use DB;
use App\PropertyUnit;
use App\PostParcel;


class PostParcelController extends Controller {
 
	public function __construct () {
		$this->middleware('technician');
		view()->share('active_menu', 'parcel-tracking');
	}

	public function postParcelList()
	{
        $tab = Request::get('tab')?Request::get('tab'):'uncollected';

        $post_parcels = PostParcel::where('property_id',Auth::user()->property_id);

        if( $tab == "uncollected" ) {
            $post_parcels = $post_parcels->where('status',0);
        } elseif( $tab == "collected" ) {
            $post_parcels = $post_parcels->where('status',1);
        } else {
            $last_30_days = Carbon::now();
            $last_30_days = $last_30_days->subDays(30)->format('Y-m-d');
            $post_parcels = $post_parcels->where('status',2)->orWhere(function ($q) use ($last_30_days) {
                $q  ->where('date_received','<=',$last_30_days)
                    ->where('status',"!=",1);
            });
        }

		if(Request::isMethod('post')) {

			if(!empty(Request::get('property_unit_id')) && Request::get('property_unit_id') != "-") {
				$post_parcels->where('property_unit_id',Request::get('property_unit_id'));
			}
			if(Request::get('type')) {
				$post_parcels = $post_parcels->where('type',Request::get('type'));
			}

			if(Request::get('date_received')) {
				$post_parcels = $post_parcels->where('date_received',Request::get('date_received'));
			}

			if(Request::get('ref_code')) {
				$post_parcels = $post_parcels->where('ref_code','like','%'.Request::get('ref_code').'%');
			}

            if(Request::get('ems_code')){
                $post_parcels->Where('ems_code', 'LIKE', '%'.Request::get('ems_code').'%');
            }

			$post_parcels = $post_parcels->orderBy('receive_code','desc')->paginate(30);
			return view('technician.post_parcels.list-element')->with(compact('post_parcels','tab'));
		} else {

			$unit_list = array('-'=> trans('messages.unit_no'));
			$unit_list += PropertyUnit::where('property_id',Auth::user()->property_id)->where('active',true)->orderBy(DB::raw('natsortInt(unit_number)'))->pluck('unit_number','id')->toArray();
			$post_parcels = $post_parcels->orderBy('receive_code','desc')->paginate(30);
			return view('technician.post_parcels.list')->with(compact('post_parcels','unit_list','tab'));
		}
	}

    public function viewPostParcel () {
        $post_parcel = PostParcel::find(Request::get('id'));
        return view('technician.post_parcels.details')->with(compact('post_parcel'));
    }
}
