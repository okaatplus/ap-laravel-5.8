<?php

namespace App\Http\Controllers\TopManager;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Excel;

use App\Customer;
use App\CustomerProperty;
use App\Property;
use App\PropertyUnit;
use App\Tenant;
use App\TenantProperty;

class CustomerController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }

    protected function checkCustomerData (Request $r) {

        $file = $r->get('file_name');
        $address = public_path('upload_tmp'.DIRECTORY_SEPARATOR.$file);
        $customers = null;
        Excel::load($address, function($reader) use (&$customers) {
            $results = $reader->get();
            $customers = $results->toArray();
        });

        $p_unit = PropertyUnit::where('property_id',$r->get('pid'))->pluck('unit_number')->toArray();

        if( !empty($p_unit) ) {

            if (!empty($customers)) {

                if (count($customers[0]) == 16) {

                    // roll correct
                    $error_count = 0;
                    $msg = "<div class='scrollable-div'><table class='table table-bordered table-striped' style='min-width: 3300px'>";
                    $msg .= $this->getTHead();
                    foreach ($customers as $customer) {
                        /// Check empty ID Card/Passport No
                        if( trim($customer['b']) != "" && trim($customer['e']) != "" ) {
                            // if not empty name
                            $valid = true;
                            $tb_data = "";
                            /////////// col 1 : Unit number
                            $unit_no = trim($customer['a']);
                            if ($unit_no == "") {
                                $tb_data .= "<td class='not-valid'>" . $customer['a'] . "</td>";
                                $valid = false;
                            } else {
                                if (!in_array($unit_no, $p_unit)) {
                                    $tb_data .= "<td class='not-valid'>" . $customer['a'] . "</td>";
                                    $valid = false;
                                } else {
                                    $tb_data .= "<td>" . $customer['a'] . "</td>";
                                }
                            }

                            /////////// col 2 : ID Card/Passport
                            //$tb_data .= "<td>" . $customer['b'] . "</td>";
                            /*if( trim($customer['b']) == "") {
                                $tb_data .= "<td class='not-valid'></td>";
                                $valid = false;
                            } else {
                                $tb_data .= "<td>" . $customer['b'] . "</td>";
                            } */
                            $tb_data .= "<td>" . $customer['b'] . "</td>";

                            /////////// col 3 : col 2 type
                            switch ($customer['c']) {
                                case '0 = บัตรประจำตัวประชาชน' :
                                case '0' :
                                    $tb_data .= "<td>บัตรประจำตัวประชาชน</td>";
                                    break;
                                case '1 = หนังสือเดินทาง' :
                                case '1' :
                                    $tb_data .= "<td>หนังสือเดินทาง</td>";
                                    break;
                                default :
                                    $tb_data .= "<td></td>";
                                    break;
                            }

                            /////////// col 4 : Prefix
                            $tb_data .= "<td>" . $customer['d'] . "</td>";

                            /////////// col 5 : First name
                            $tb_data .= "<td>" . $customer['e'] . "</td>";

                            /////////// col 6 : Last name
                            $tb_data .= "<td>" . $customer['f'] . "</td>";

                            /////////// col 7 : Phone
                            $tb_data .= "<td>" . $customer['g'] . "</td>";

                            /////////// col 8 : Email
                            /*if (trim($customer['h']) != "") {
                                if (!filter_var($customer['h'], FILTER_VALIDATE_EMAIL)) {
                                    $tb_data .= "<td class='not-valid'>" . $customer['h'] . "</td>";
                                    $valid = false;
                                } else {
                                    $tb_data .= "<td>" . $customer['h'] . "</td>";
                                }
                            } else {
                                $tb_data .= "<td></td>";
                            }*/
                            $tb_data .= "<td>" . $customer['h'] . "</td>";

                            /////////// col 9 : Address
                            $tb_data .= "<td>" . $customer['i'] . "</td>";

                            /////////// col 10 : Delivery address
                            $tb_data .= "<td>" . $customer['j'] . "</td>";

                            /////////// col 11 : Customer person type
                            switch ($customer['k']) {
                                case '0 = บุคคลธรรมดา' :
                                case '0' :
                                    $tb_data .= "<td>บุคคลธรรมดา</td>";
                                    break;
                                default :
                                    $tb_data .= "<td>นิติบุคคล</td>";
                                    break;
                            }

                            /////////// col 12 : Resident type
                            switch ($customer['l']) {
                                case '0 = เจ้าของร่วม' :
                                case '0' :
                                    $tb_data .= "<td>เจ้าของร่วม</td>";
                                    break;
                                case '1 = ผู้อยู่อาศัย' :
                                case '1' :
                                    $tb_data .= "<td>ผู้อยู่อาศัย</td>";
                                    break;
                                default :
                                    $tb_data .= "<td>ผู้เช่า</td>";
                                    break;
                            }

                            /////////// col 13 : Nationality
                            $tb_data .= "<td>" . $customer['m'] . "</td>";

                            /////////// col 14 : Jobs
                            $tb_data .= "<td>" . $customer['n'] . "</td>";

                            /////////// col 15 : DOB
                            if (trim($customer['o']) != "") {
                                $t = trim($customer['o']);
                                if (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $t) || preg_match('/^\d{4}-\d{2}-\d{2}$/', $t)) {
                                    $t = strtotime($t);
                                    $tb_data .= "<td>" . date('Y-m-d', $t) . "</td>";
                                } else {
                                    $tb_data .= "<td class='not-valid'>" . $customer['o'] . "</td>";
                                    $valid = false;
                                }
                            } else {
                                $tb_data .= "<td></td>";
                            }

                            /////////// col 16 : Customer code
                            $tb_data .= "<td>" . $customer['p'] . "</td>";

                            ////////////////// end check by column //////////////////

                            if (!$valid) {
                                $msg .= "<tr class='has-error'>";
                                $error_count++;
                            } else $msg .= "<tr>";
                            $msg .= $tb_data;
                            $msg .= "</tr>";
                        }

                    }

                    $msg .= "</table></div>";
                    if ($error_count) {
                        $results = false;
                        $msg = "<div class='alert alert-danger text-center'>" . trans('messages.Importing.found_invalid_row',['n' => $error_count]) . "</div>" . $msg;
                    } else {
                        $results = true;
                        $msg = "<div class='alert alert-success text-center'>" . trans('messages.Importing.data_valid') . "</div>" . $msg;
                    }

                    $data_checked = true;


                } else {
                    $results = false;
                    $data_checked = false;
                    $msg = trans('messages.Importing.invalid_template');
                }
            } else {
                $results = false;
                $data_checked = false;
                $msg = trans('messages.Importing.no_data');
            }
        }  else {
            $results = false;
            $data_checked = false;
            $msg = trans('messages.Importing.no_property');
        }

        return response()->json(
            array(
                'result' => $results,
                'data_checked' => $data_checked,
                'data' => $msg
            )
        );
    }

    protected function startImportCustomerData (Request $r) {
        $file = $r->get('file_name');
        $address = public_path('upload_tmp'.DIRECTORY_SEPARATOR.$file);

        $property = Property::find($r->get('pid'));


        if( $property ) {

            $customers = null;
            Excel::load($address, function($reader) use (&$customers, &$customers_array, $property) {
                $results = $reader->get();
                $customers = $results->toArray();

                $p_units = PropertyUnit::where('property_id', $property->id)->where('active', true)->pluck('id','unit_number')->toArray();
                $data_update_unit_owner_name = [];

                foreach ($customers as $customer) {

                    if( trim($customer['b']) != "" && trim($customer['e']) != "" ) {
                        // if not empty id card and name
                        $id_card = trim(str_replace(' ', '', $customer['b']));
                        $p_unit_id = $p_units[trim($customer['a'])];

                        /////////// col 3 : col 2 type
                        switch ($customer['c']) {
                            case '0 = บัตรประจำตัวประชาชน' :
                            case '0' :
                                $id_type = 1;
                                break;
                            case '1 = หนังสือเดินทาง' :
                            case '1' :
                                $id_type = 2;
                                break;
                            default :
                                $id_type = NULL;
                                break;
                        }

                        switch ($customer['k']) {
                            case '0 = บุคคลธรรมดา' :
                            case '0' :
                                $c_p_type = 1;
                                break;
                            default :
                                $c_p_type = 2;
                                break;
                        }

                        switch ($customer['l']) {
                            case '0 = เจ้าของร่วม' :
                            case '0' :
                                $c_type = 0;
                                break;
                            case '1 = ผู้อยู่อาศัย' :
                            case '1' :
                                $c_type = 1;
                                break;
                            default :
                                $c_type = 2;
                                break;
                        }

                        if ($c_type == 2) {
                            // if is tenant
                            if ($id_card) {
                                $dup_t = Tenant::where('id_card', $id_card)->first();
                                if (!$dup_t) {
                                    $t_data = new Tenant;
                                    $t_data->prefix_name = empty($customer['d']) ? NULL : $customer['d'];
                                    $t_data->name = $customer['e'] . (empty(trim($customer['f'])) ? "" : " " . $customer['f']);
                                    $t_data->id_card = $id_card;
                                    $t_data->type_id_card = $id_type;
                                    $t_data->phone = empty($customer['g']) ? NULL : $customer['g'];
                                    $t_data->email = empty($customer['h']) ? NULL : $customer['h'];
                                    $t_data->delivery_address = empty($customer['j']) ? NULL : $customer['j'];
                                    $t_data->person_type = $c_p_type;
                                    $t_data->nationality = empty($customer['m']) ? NULL : $customer['m'];
                                    $t_data->job = empty($customer['n']) ? NULL : $customer['n'];
                                    $t_data->dob = empty($customer['o']) ? NULL : $customer['o'];
                                    $t_data->customer_code = empty($customer['p']) ? NULL : $customer['p'];
                                    $t_data->save();
                                } else {
                                    $t_data = $dup_t;
                                }

                            } else {
                                $t_data = new Tenant;
                                $t_data->prefix_name = empty($customer['d']) ? NULL : $customer['d'];
                                $t_data->name = $customer['e'] . (empty(trim($customer['f'])) ? "" : " " . $customer['f']);
                                $t_data->id_card = $id_card;
                                $t_data->type_id_card = $id_type;
                                $t_data->phone = empty($customer['g']) ? NULL : $customer['g'];
                                $t_data->email = empty($customer['h']) ? NULL : $customer['h'];
                                $t_data->address = empty($customer['i']) ? NULL : $customer['i'];
                                $t_data->delivery_address = empty($customer['j']) ? NULL : $customer['j'];
                                $t_data->person_type = $c_p_type;
                                $t_data->nationality = empty($customer['m']) ? NULL : $customer['m'];
                                $t_data->job = empty($customer['n']) ? NULL : $customer['n'];
                                $t_data->dob = empty($customer['o']) ? NULL : $customer['o'];
                                $t_data->customer_code = empty($customer['p']) ? NULL : $customer['p'];
                                $t_data->save();
                            }

                            $cup_t_p = TenantProperty::where('tenant_id', $t_data->id)->where('property_unit_id', $p_unit_id)->first();
                            if (!$cup_t_p) {
                                $c_p = new TenantProperty;
                                $c_p->tenant_id = $t_data->id;
                                $c_p->property_id = $property->id;
                                $c_p->property_unit_id = $p_unit_id;
                                $c_p->save();
                            }

                        } else {

                            if ($id_card) {
                                // Has id card data
                                // then add/find duplicated customer
                                // find duplicated
                                $dup_c = Customer::where('id_card', $id_card)->first();
                                if (!$dup_c) {
                                    $c_data = new Customer;
                                    $c_data->prefix_name = empty($customer['d']) ? NULL : $customer['d'];
                                    $c_data->name = $customer['e'] . (empty(trim($customer['f'])) ? "" : " " . $customer['f']);
                                    $c_data->id_card = $id_card;
                                    $c_data->type_id_card = $id_type;
                                    $c_data->phone = empty($customer['g']) ? NULL : $customer['g'];
                                    $c_data->email = empty($customer['h']) ? NULL : $customer['h'];
                                    $c_data->address = empty($customer['i']) ? NULL : $customer['i'];
                                    $c_data->delivery_address = empty($customer['j']) ? NULL : $customer['j'];
                                    $c_data->person_type = $c_p_type;
                                    $c_data->nationality = empty($customer['m']) ? NULL : $customer['m'];
                                    $c_data->job = empty($customer['n']) ? NULL : $customer['n'];
                                    $c_data->dob = empty($customer['o']) ? NULL : $customer['o'];
                                    $c_data->customer_code = empty($customer['p']) ? NULL : $customer['p'];
                                    $c_data->save();
                                } else {
                                    /// TODO: Question -> Need to update customer data?
                                    ///////
                                    $c_data = $dup_c;
                                    // check and delete duplicated customer property unit data
                                    $dup_c_p = CustomerProperty::where('customer_id', $c_data->id)->where('property_unit_id', $p_unit_id)->first();
                                    if ($dup_c_p) {
                                        $dup_c_p->delete();
                                    }
                                }

                                /// add Customer property if not existed
                                $cup_c_p = CustomerProperty::where('customer_id', $c_data->id)->where('property_unit_id', $p_unit_id)->first();
                                if (!$cup_c_p) {
                                    $c_p = new CustomerProperty;
                                    $c_p->customer_id = $c_data->id;
                                    $c_p->property_id = $property->id;
                                    $c_p->property_unit_id = $p_unit_id;
                                    $c_p->customer_type = $c_type;
                                    $c_p->save();

                                    if ($c_type == 0) {
                                        // set owner data
                                        $owner_name = $c_data->prefix_name . " " . $c_data->name;
                                        $address = empty($customer['i']) ? NULL : $customer['i'];
                                        $delivery_address = $c_data->delivery_address;
                                        // flag set owner
                                        $flag_owner = true;
                                    } else {
                                        $flag_owner = false;
                                    }
                                } else {
                                    $flag_owner = false;
                                }
                            } else {

                                $c_data = new Customer;
                                $c_data->prefix_name = empty($customer['d']) ? NULL : $customer['d'];
                                $c_data->name = $customer['e'] . (empty(trim($customer['f'])) ? "" : " " . $customer['f']);
                                $c_data->id_card = $id_card;
                                $c_data->type_id_card = $id_type;
                                $c_data->phone = empty($customer['g']) ? NULL : $customer['g'];
                                $c_data->email = empty($customer['h']) ? NULL : $customer['h'];
                                $c_data->address = empty($customer['i']) ? NULL : $customer['i'];
                                $c_data->delivery_address = empty($customer['j']) ? NULL : $customer['j'];
                                $c_data->person_type = $c_p_type;
                                $c_data->nationality = empty($customer['m']) ? NULL : $customer['m'];
                                $c_data->job = empty($customer['n']) ? NULL : $customer['n'];
                                $c_data->dob = empty($customer['o']) ? NULL : $customer['o'];
                                $c_data->customer_code = empty($customer['p']) ? NULL : $customer['p'];
                                $c_data->save();

                                $c_p = new CustomerProperty;
                                $c_p->customer_id = $c_data->id;
                                $c_p->property_id = $property->id;
                                $c_p->property_unit_id = $p_unit_id;
                                $c_p->customer_type = $c_type;
                                $c_p->save();

                                // if doesn't has id card number data
                                if ($c_type == 0) {
                                    // if is owner
                                    // set owner name
                                    $owner_name = empty($customer['d']) ? NULL : $customer['d'];
                                    $owner_name .= $customer['e'] . (empty(trim($customer['f'])) ? "" : $customer['f']);

                                    $address = empty($customer['i']) ? NULL : $customer['i'];
                                    $delivery_address = empty($customer['j']) ? NULL : $customer['j'];
                                    // flag set owner
                                    $flag_owner = true;
                                } else {
                                    $flag_owner = false;
                                }
                            }
                            if ($flag_owner) {
                                // set owner name data
                                $data_update_unit_owner_name[$p_unit_id]['owner_name'][] = $owner_name;
                                $data_update_unit_owner_name[$p_unit_id]['address'] = $address;
                                $data_update_unit_owner_name[$p_unit_id]['delivery_address'] = $delivery_address;
                                //$data_update_unit_owner_name[$p_unit_id]['flag_owner']      = $flag_owner;
                                /// adjust property unit deliver address
                            }
                        }
                    }
                }

                if( !empty($data_update_unit_owner_name) ) {
                    // update property unit owner name
                    foreach ($data_update_unit_owner_name as $id => $data) {
                        $property_unit = PropertyUnit::find($id);
                        if( $property_unit ) {
                            $property_unit->owner_name_th       =
                            $property_unit->owner_name_en       = implode(',',$data['owner_name']);
                            $property_unit->address             = $data['address'];
                            $property_unit->delivery_address    = $data['delivery_address'];
                            //if( $data['flag_owner'] ) {
                            $property_unit->type            = 0;
                            //}
                            $property_unit->save();
                        }
                    }
                }
            });

            if( !empty($customers_array) ) {
                $property->property_unit()->saveMany($customers_array);
            }
            File::delete($address);

            return response()->json([
                'result' => true,
                'message' => 'นำเข้าข้อมูลเสร็จสมบูรณ์'
            ]);

        } else {
            return response()->json([
                'result' => false,
                'message' => 'ไม่พบนิติบุคคล'
            ]);
        }
    }

    protected function getTHead () {
        return '<tr>
                    <th width="100px">บ้านเลขที่</th>
                    <th width="300px">หมายเลขประจำตัวประชาชน/หนังสือเดินทาง</th>
                    <th width="200px">ประเภทของการยืนยันตัวตน</th>
                    <th width="90px">คำนำหน้า</th>
                    <th width="200px">ชื่อ</th>
                    <th width="200px">สกุล</th>
                    <th width="150px">เบอร์โทรติดต่อ</th>
                    <th width="300px">อีเมล</th>
                    <th width="400px">ที่อยู่</th>
                    <th width="400px">ที่อยู่สำหรับจัดส่งเอกสาร</th>
                    <th width="170px">ประเภทของผู้อยู่อาศัย</th>
                    <th width="170px">หมวดหมู่ของผู้อยู่อาศัย</th>
                    <th width="100px">สัญชาติไทย</th>
                    <th width="200px">อาชีพ</th>
                    <th width="150px">วัน/เดือน/ปี/เกิด</th>
                    <th width="*">Customer Code</th>
                </tr>';
    }
}
