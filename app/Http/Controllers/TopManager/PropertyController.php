<?php namespace App\Http\Controllers\TopManager;;


use Request;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;
use Excel;
use File;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use Storage;

# Model
use App\Customer;
use App\Tenant;
use App\Property;
use App\CustomerProperty;
use App\Province;
use App\Districts;
use App\Subdistricts;
use App\Propertybrand;
use App\TenantProperty;

class PropertyController extends Controller {

    public function __construct () {
        $this->middleware('auth');
    }

    public function add () {
        $property = new Property;
        $p = new Province;
        $provinces = $p->getProvince();

        $d = new Districts;
        $districts = $d->getDistricts();

        $s = new Subdistricts;
        $subdistricts = $s->getSubdistricts();

        $b = new Propertybrand;
        $brand = $b->getBand();

        if (Request::isMethod('post'))
        {
            $property = Request::except('id','_token');
            $new_prop = new Property;

            $p_rule = array(
                'property_name_th'          => 'required',
                'property_name_en'          => 'required',
                'juristic_person_name_th'   => 'required',
                'juristic_person_name_en'   => 'required'
            );
            $vp = Validator::make($property, $p_rule);
            if($vp->fails() ) {
                $v = $vp->messages()->toArray();
                return redirect()->back()->withInput()->withErrors($v);
            } else {
                $new_prop->fill($property);
                $new_prop->unit_size = str_replace(',', '', $new_prop->unit_size);
                if(empty($new_prop->unit_size)) $new_prop->unit_size = 0;


                if (!empty(Request::get('img_post_banner'))) {
                    $file = Request::get('img_post_banner');
                    $name 	= $file['name'];
                    $x 		= Request::get('img-x');
                    $y 		= Request::get('img-y');
                    $w 		= Request::get('img-w');
                    $h 		= Request::get('img-h');
                    cropBannerImg ($name,$x,$y,$w,$h);
                    $path = $this->createLoadBalanceDir($file['name']);
                    $tempUrl = "/%s%s";
                    $new_prop->banner_url = sprintf($tempUrl, $path, $file['name']);
                }

                $new_prop->save();
                if( Auth::user()->role == 1 )
                return redirect('/management/admin/property/list');
                else
                return redirect('/root/admin/property/list');
            }
        }

        return view('property.add')->with(compact('property','provinces','districts','subdistricts','brand'));
    }



    public function edit ($id) {
        $p = new Province;
        $provinces = $p->getProvince();

        $d = new Districts;
        $districts = $d->getDistricts();

        $s = new Subdistricts;
        $subdistricts = $s->getSubdistricts();

        $b = new Propertybrand;
        $brand = $b->getBand();

        if (Request::isMethod('post'))
        {
            $property = Request::except('_token');
            $p_rule = array(
                'property_name_th'          => 'required',
                'property_name_en'          => 'required',
                'juristic_person_name_th'   => 'required',
                'juristic_person_name_en'   => 'required'
            );

            $vp = Validator::make($property, $p_rule);





            if($vp->fails()) {
                $v = $vp->messages()->toArray();
                return redirect()->back()->withInput()->withErrors($v);
            } else {
                $prop = Property::find($property['id']);
                $prop->fill($property);
                $prop->unit_size = str_replace(',', '', $prop->unit_size);
                if(empty($prop->unit_size)) $prop->unit_size = 0;

                if(Request::get('remove-banner-flag') && $prop->banner_url) {
                    $this->removeBannerFile($prop->banner_url);
                    $prop->banner_url = $banner_path = null;
                }

                if (!empty(Request::get('img_post_banner'))) {
                    $file = Request::get('img_post_banner');
                    $name 	= $file['name'];
                    $x 		= Request::get('img-x');
                    $y 		= Request::get('img-y');
                    $w 		= Request::get('img-w');
                    $h 		= Request::get('img-h');
                    cropBannerImg ($name,$x,$y,$w,$h);
                    $path = $this->createLoadBalanceDir($file['name']);
                    $tempUrl = "/%s%s";
                    $prop->banner_url = sprintf($tempUrl, $path, $file['name']);
                }
                $prop->save();

                if( Auth::user()->role == 1 )
                    return redirect('/management/admin/property/list');
                else
                    return redirect('/root/admin/property/list');
            }
        }
        else {
            $property = Property::with(['property_admin' => function ($query) {
                $query->where('role', '=', '3');
            }])->find($id);
            $user = $property->property_admin;
            if(isset($user)) {
                $property = $property->toArray();
                $property['user'] = $user->toArray();
            }

            return view('property.edit')->with(compact('property','provinces','districts','subdistricts','brand'));
        }

    }

    public function view ($id) {
        $p = new Province;
        $provinces = $p->getProvince();

        $d = new Districts;
        $districts = $d->getDistricts();

        $s = new Subdistricts;
        $subdistricts = $s->getSubdistricts();

        $b = new Propertybrand;
        $brand = $b->getBand();

        $property = Property::find($id);
        return view('property.view')->with(compact('property','provinces','districts','subdistricts','brand'));
    }

    public function removeBannerFile ($name) {
        $file_path  = "post-file/".$name;
        $exists     = Storage::disk('s3')->has($file_path);
        if ($exists) {
            Storage::disk('s3')->delete($file_path);
        }
    }

    public function createLoadBalanceDir ($name) {
        $targetFolder = public_path().DIRECTORY_SEPARATOR.'upload_tmp'.DIRECTORY_SEPARATOR;
        $folder = substr($name, 0,2);
        $pic_folder = 'post-file/'.$folder;
        $directories = Storage::disk('s3')->directories('post-file'); // Directory in Amazon
        if(!in_array($pic_folder, $directories))
        {
            Storage::disk('s3')->makeDirectory($pic_folder);
        }
        $full_path_upload = $pic_folder."/".strtolower($name);
        Storage::disk('s3')->put($full_path_upload, file_get_contents($targetFolder.$name), 'public');
        File::delete($targetFolder.$name);
        return $folder."/";
    }

    public function selectDistrict(){
        if(Request::isMethod('post')) {
            $d = new Districts;
            $d = $d->where('province_id',Request::get('id'));
            $d = $d->get();

            return response()->json($d);
        }
    }

    public function Subdistrict(){
        if(Request::isMethod('post')){

                $s = new Subdistricts;
                $s = $s->where('district_id',Request::get('id'));
                $s = $s->get();
            return response()->json($s);
        }
    }


    public function selectDistrictEdit(){
        if(Request::isMethod('post')) {

            $property = Property::find(Request::get('id'));

            //$p = Districts::find(Request::get('id'));

            $d = new Districts;
            $d = $d->where('province_id',$property['province']);
            $d = $d->get();

            return response()->json($d);
        }
    }

    public function editSubDis(){

             $property = Property::find(Request::get('id'));

             $s = new Subdistricts;
             $s = $s->where('district_id',$property['district']);
             $s = $s->get();

            return response()->json($s);
    }

    public function zip_code(){
        if(Request::isMethod('post')){
            $z = new Subdistricts;
            $z = $z->where('id',Request::get('id'));
            $z = $z->get();

            return response()->json($z);
        }
    }

    public function resetData () {
        if( Request::isMethod('post') ) {
            $property = Property::with('customerProperty', 'userProperty', 'tenantProperty')->find(Request::get('pid'));
            if ( !$property->userProperty->count() ) {
                // delete customer property
                if ( $property->customerProperty->count() ) {
                    foreach ($property->customerProperty as $cp) {
                        $another_cp = CustomerProperty::where('customer_id', $cp->customer_id)
                            ->where('property_id', '!=', $cp->property_id)->count();
                        if (!$another_cp) {
                            // remove customer if not appeared on another property
                            $customer = Customer::find($cp->customer_id);
                            if( $customer ) $customer->delete();
                        }
                        // remove customer property data
                        $cp->delete();
                    }
                }

                // delete tenant property
                if ( $property->tenantProperty->count() ) {
                    foreach ($property->tenantProperty as $tp) {
                        $another_tp = TenantProperty::where('tenant_id', $tp->tenant_id)
                            ->where('property_id', '!=', $tp->property_id)->count();
                        if (!$another_tp) {
                            // remove tenant if not appeared on another property
                            $tenant = Tenant::find($tp->tenant_id);
                            if( $tenant ) $tenant->delete();
                        }
                        // remove tenant property data
                        $tp->delete();
                    }
                }

                // remove property unit
                $property->property_unit()->delete();
            }
        }
        return response()->json(['result'=>true,'message' => 'ล้างข้อมูลเรียบร้อยแล้ว']);
    }
}
