<?php

namespace App\Http\Controllers\TopManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Excel;

use App\Property;
use App\PropertyUnit;

class PropertyUnitController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }

    protected function checkUnitData (Request $r) {

        $file = $r->get('file_name');
        $address = public_path('upload_tmp'.DIRECTORY_SEPARATOR.$file);
        $units = null;
        Excel::load($address, function($reader) use (&$units) {
            $results = $reader->get();
            $units = $results->toArray();
        });

        if(!empty($units)) {
            if(count($units[0]) == 19) {
                // roll correct
                $error_count = 0;
                $msg = "<div class='scrollable-div'><table class='table table-bordered table-striped' style='min-width: 2680px'>";
                $msg .= $this->getTHead();
                foreach($units as $unit) {
                    $valid = true;
                    $tb_data = "";

                /////////// col 1 : unit code
                    $tb_data .= "<td>".$unit['a']."</td>";

                /////////// col 2 : unit number
                    if(trim($unit['b']) == "") {
                        $tb_data .= "<td class='not-valid'>".$unit['b']."</td>";
                        $valid = false;
                    } else {
                        $tb_data .= "<td>".$unit['b']."</td>";
                    }
                /////////// col 3 : building : remove by template
                    //$tb_data .= "<td>".$unit['c']."</td>";
                /////////// col 4 : unit_floor : remove by template
                    //$tb_data .= "<td>".$unit['d']."</td>";
                /////////// col 3 : land_no
                    $tb_data .= "<td>".$unit['c']."</td>";
                /////////// col 4 : type
                    switch ($unit['d']) {
                        case '0=บ้าน/ห้องชุด' :
                        case '0=บ้าน' :
                            $tb_data .= "<td>บ้าน/ห้องชุด</td>";
                            break;
                        case '1=ไม่มีผู้ถือกรรมสิทธิ์' :
                            $tb_data .= "<td>ไม่มีผู้ถือกรรมสิทธิ์</td>";
                            break;
                        default :
                            $tb_data .= "<td>ถือกรรมสิทธิ์โดยเจ้าของโครงการ</td>";
                            break;
                    }

                /////////// col 5 : asset_id
                    $tb_data .= "<td>".$unit['e']."</td>";
                /////////// col 6 : project_id
                    $tb_data .= "<td>".$unit['f']."</td>";
                /////////// col 7 : property size
                    $size = (float)trim($unit['g']);
                    if($size != 0) {
                        if( is_float($size) ) {
                            $tb_data .= "<td>".$size."</td>";
                        } else {
                            $tb_data .= "<td class='not-valid'>".$size."</td>";
                            $valid = false;
                        }
                    } else {
                        $tb_data .= "<td>".$unit['g']."</td>";
                    }

                /////////// col 8 : property size unit
                    $tb_data .= "<td>".$unit['h']."</td>";
                /////////// col 9 : move in date
                    if(trim($unit['i']) != "") {
                        $t = trim($unit['i']);
                        if( preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $t) || preg_match('/^\d{4}-\d{2}-\d{2}$/', $t) ) {
                            $t = strtotime($t);
                            $tb_data .= "<td>".date('Y-m-d', $t)."</td>";
                        } else {
                            $tb_data .= "<td class='not-valid'>".$unit['i']."</td>";
                            $valid = false;
                        }
                    } else {
                        $tb_data .= "<td></td>";
                    }
                    //$tb_data .= "<td>".$unit['i']."</td>";
                /////////// col 10 : transferred date
                    if(trim($unit['j']) != "") {
                        $t = trim($unit['j']);
                        if( preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $t) || preg_match('/^\d{4}-\d{2}-\d{2}$/', $t) ) {
                            $t = strtotime($t);
                            $tb_data .= "<td>".date('Y-m-d', $t)."</td>";
                        } else {
                            $tb_data .= "<td class='not-valid'>".$unit['j']."</td>";
                            $valid = false;
                        }
                    } else {
                        $tb_data .= "<td></td>";
                    }
                /////////// col 11 : insurance
                    if(trim($unit['k']) != "") {
                        $t = trim($unit['k']);
                        if( preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $t) || preg_match('/^\d{4}-\d{2}-\d{2}$/', $t) ) {
                            $t = strtotime($t);
                            $tb_data .= "<td>".date('Y-m-d', $t)."</td>";
                        } else {
                            $tb_data .= "<td class='not-valid'>".$unit['k']."</td>";
                            $valid = false;
                        }
                    } else {
                        $tb_data .= "<td></td>";
                    }

                /////////// col 12 : ownership ratio
                    $or = (float)trim($unit['l']);
                    if( $or ) {
                        if( is_float($or) ) {
                            $tb_data .= "<td>".$or."</td>";
                        } else {
                            $tb_data .= "<td class='not-valid'>".$or."</td>";
                            $valid = false;
                        }
                    } else {
                        $tb_data .= "<td>".$unit['l']."</td>";
                    }

                    /////////// col 13 : water billing
                    if(trim($unit['m']) == "TRUE") {
                        $tb_data .= "<td>จัดเก็บ</td>";
                    } else {
                        $tb_data .= "<td>ไม่จัดเก็บ</td>";
                    }

                    /////////// col 14 : water billing rate
                    if( is_float($unit['n']) ) {
                        $tb_data .= "<td>".trim($unit['n'])."</td>";
                    } else {
                        $tb_data .= "<td>0</td>";

                    }

                    /////////// col 15 : electric billing
                    if(trim($unit['o']) == "TRUE") {
                        $tb_data .= "<td>จัดเก็บ</td>";
                    } else {
                        $tb_data .= "<td>ไม่จัดเก็บ</td>";
                    }

                    /////////// col 16 : electric billing rate
                    if( is_float($unit['p']) ) {
                        $tb_data .= "<td>".trim($unit['p'])."</td>";
                    } else {
                        $tb_data .= "<td>0</td>";

                    }

                    /////////// col 17 : utility charge
                    if( is_float($unit['q']) ) {
                        $tb_data .= "<td>".trim($unit['q'])."</td>";
                    } else {
                        $tb_data .= "<td>0</td>";

                    }

                    /////////// col 18 : extra fc charge
                    if( is_float($unit['r']) ) {
                        $tb_data .= "<td>".trim($unit['r'])."</td>";
                    } else {
                        $tb_data .= "<td>0</td>";
                    }

                    /////////// col 19 : garbage charge
                    if( is_float($unit['s']) ) {
                        $tb_data .= "<td>".trim($unit['s'])."</td>";
                    } else {
                        $tb_data .= "<td>0</td>";
                    }

                ////////////////// end check by column //////////////////

                    if( !$valid ) {
                        $msg .= "<tr class='has-error'>";
                        $error_count++;
                    }

                    else $msg .= "<tr>";
                    $msg .= $tb_data;
                    $msg .= "</tr>";
                }

                $msg .= "</table></div>";
                if( $error_count ) {
                    $results = false;
                    $msg = "<div class='alert alert-danger text-center'>" . trans('messages.Importing.found_invalid_row',['n' => $error_count]) . "</div>".$msg;
                } else {
                    $results = true;
                    $msg = "<div class='alert alert-success text-center'>" . trans('messages.Importing.data_valid') . "</div>".$msg;
                }

                $data_checked = true;

            } else {
                $results = false;
                $data_checked = false;
                $msg = trans('messages.Importing.invalid_template');;
            }
        } else {
            $results = false;
            $data_checked = false;
            $msg = trans('messages.Importing.no_data');
        }

        return response()->json(
            array(
                'result' => $results,
                'data_checked' => $data_checked,
                'data' => $msg
            )
        );
    }

    protected function startImportUnitData (Request $r) {
        $file = $r->get('file_name');
        $address = public_path('upload_tmp'.DIRECTORY_SEPARATOR.$file);

        $property = Property::find($r->get('pid'));

        if( $property ) {

            $units = null;
            Excel::load($address, function($reader) use (&$units, &$units_array) {
                $results = $reader->get();
                $units = $results->toArray();

                foreach ($units as $unit) {

                    switch ($unit['d']) {
                        case '0=บ้าน/ห้องชุด' :
                        case '0=บ้าน' :
                            $type = 0;
                            break;
                        case '1=ไม่มีผู้ถือกรรมสิทธิ์' :
                            $type = 1;
                            break;
                        default :
                            $type = 2;
                            break;
                    }

                    /////////// col 13 : water billing
                    if(trim($unit['m']) == "TRUE") {
                        $unit['m'] = true;
                    } else {
                        $unit['m'] = false;
                    }

                    /////////// col 14 : water billing rate
                    if( !is_float($unit['n']) ) {
                        $unit['n'] = 0;
                    }

                    /////////// col 15 : electric billing
                    if(trim($unit['o']) == "TRUE") {
                        $unit['o'] = true;
                    } else {
                        $unit['o'] = false;
                    }

                    /////////// col 16 : electric billing rate
                    if( !is_float($unit['p']) ) {
                        $unit['p'] = 0;
                    }

                    /////////// col 17 : utility charge
                    if( !is_float($unit['q']) ) {
                        $unit['q'] = 0;
                    }

                    /////////// col 18 : extra fc charge
                    if( !is_float($unit['r']) ) {
                        $unit['r'] = 0;
                    }

                    /////////// col 19 : garbage charge
                    if( !is_float($unit['s']) ) {
                        $unit['s'] = 0;
                    }

                    $units_array[] = new PropertyUnit([
                        'unit_code' 	    => empty($unit['a'])?NULL:$unit['a'],
                        'unit_number' 		=> trim($unit['b']),
                        //'building' 	        => empty($unit['c'])?NULL:$unit['c'],
                        //'unit_floor' 	    => empty($unit['d'])?NULL:$unit['d'],
                        'land_no' 	        => empty($unit['c'])?NULL:$unit['c'],
                        'type' 	            => $type,
                        'asset_project_id' 	=> empty($unit['e'])?NULL:$unit['e'],
                        'asset_id' 	        => empty($unit['f'])?NULL:$unit['f'],
                        'property_size' 	=> empty($unit['g'])?NULL:$unit['g'],
                        'size_label'        => empty($unit['h'])?NULL:$unit['h'],
                        'move_in'           => empty($unit['i'])?NULL:$unit['i'],
                        'transferred_date'  => empty($unit['j'])?NULL:$unit['j'],
                        'insurance_expire_date'  => empty($unit['k'])?NULL:$unit['k'],
                        'ownership_ratio'   => empty($unit['l'])?NULL:$unit['l'],
                        'water_billing'     => $unit['m'],
                        'water_billing_rate' => $unit['n'],
                        'electric_billing'  => $unit['o'],
                        'electric_billing_rate' => $unit['p'],
                        'utility_charge'    => $unit['q'],
                        'extra_cf_charge'   => $unit['r'],
                        'garbage_fee'       => $unit['s']
                    ]);
                }
            });

            if( !empty($units_array) ) {
                $property->property_unit()->saveMany($units_array);
            }
            File::delete($address);

            return response()->json([
                'result' => true,
                'message' => 'นำเข้าข้อมูลเสร็จสมบูรณ์'
            ]);

        } else {
            return response()->json([
                'result' => false,
                'message' => 'ไม่พบนิติบุคคล'
            ]);
        }
    }

    protected function getTHead () {
        return '<tr>
                    <th width="100px">เลขที่ Unit</th>
                    <th width="100px">บ้านเลขที่</th>
                    <th width="100px">แปลงที่</th>
                    <th width="150px">ประเภทของ Unit</th>
                    <th width="150px">Product ID</th>
                    <th width="150px">UnitID</th>
                    <th width="100px">ขนาดพื้นที่</th>
                    <th width="160px">หน่วยของขนาดพื้นที่</th>
                    <th width="160px">วันที่ย้ายเข้าอยู่อาศัย</th>
                    <th width="160px">วันที่โอนกรรมสิทธิ์</th>
                    <th width="160px">วันที่ครบกำหนดระยะประกัน</th>
                    <th width="150px">อัตราส่วนกรรมสิทธิ์</th>
                    <th width="150px">จัดเก็บค่าน้ำหรือไม่</th>
                    <th width="100px">อัตราจัดเก็บค่าน้ำ</th>
                    <th width="150px">จัดเก็บค่าไฟหรือไม่</th>
                    <th width="100px">อัตราจัดเก็บค่าไฟ</th>
                    <th width="200px">อัตราจัดเก็บค่าสาธารณูปโภคเพิ่มเติมจากค่าส่วนกลาง</th>
                    <th width="100px">อัตราค่าส่วนกลางที่เก็บเพิ่ม</th>
                    <th width="*">อัตราบริการจัดเก็บขยะ</th>
                </tr>';
    }
}
