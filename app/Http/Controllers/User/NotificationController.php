<?php namespace App\Http\Controllers\User;
use Request;
use Illuminate\Routing\Controller;
use Auth;
# Model
use DB;
use App\Notification;
use App\Property;
use App\PropertyUnit;
class NotificationController extends Controller {

	public function __construct () {
		$this->middleware('auth');
		view()->share('active_menu', 'noti');
	}
	public function index()
	{
		$property = Property::where('id', Auth::user()->property_id)->first();
		if(Request::ajax()) {
            $notis = Notification::where('to_user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(15);
	    	return view('notification.notification-list',compact('notis','property'));
		} else {
			$notis = Notification::with('sender')
						->where('to_user_id','=',Auth::user()->id)
						->orderBy('notification.created_at', 'desc')
						->paginate(15);
			return view('notification.index',compact('notis','property'));
		}
	}

	public function get()
	{
		if(Request::ajax()) {
			$notis = Notification::with('sender')->find(Request::get('nid'));
			echo view('notification.view')->with(compact('notis'))->render();
		}
	}

	public function markAsRead () {
		if(Request::ajax()) {
			$notis = Notification::find(Request::get('nid'));
			$notis->read_status = true;
			$notis->save();
			return response()->json(['status'=>true]);
		}
	}

	public function NotiPage($value='')
	{
		$notis_head = Notification::where('to_user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(15);
		$property = Property::where('id', Auth::user()->property_id)->first();
		return view('layout.notification-element',compact('notis_head','property'));
	}

	public function NotiLast()
	{
		$notis_head = Notification::where('to_user_id',Auth::user()->id)->orderBy('created_at','desc')->first();
		$property = Property::where('id', Auth::user()->property_id)->first();
		return view('layout.notification-element',compact('notis_head','property'));
	}
}
