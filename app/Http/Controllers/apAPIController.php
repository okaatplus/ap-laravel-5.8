<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerProperty;
use App\Property;
use App\PropertyUnit;
use App\Tenant;
use App\TenantProperty;
use Illuminate\Http\Request;

class apAPIController extends Controller
{
    public function apWorldProperty () {
        $properties = Property::get();
        $data = [];
        if( $properties ) {

            foreach ( $properties as $p ) {

                if( $p->province ) {
                    $p->province = $p->has_province->name_in_th;
                    unset($p->has_province);
                }
                if( $p->district ) {
                    $p->district = $p->has_district->name_th;
                    unset($p->has_district);
                }
                if( $p->sub_district ) {
                    $p->sub_district = $p->has_sub_district->name_th;
                    unset($p->has_sub_district);
                }

                if( $p->has_brand ) {
                    $p->brand = $p->has_brand->name_th;
                    unset($p->has_brand);
                }

                $p->source_type = "Smart World";
                $data[] = $p->toArray();
            }
        }

        return response()->json(array(
            'result' => true,
            'data'  => $data
        ));
    }

    public function apWorldPropertyUnit () {
        $p_units = PropertyUnit::where('active',true)->get();
        $data = [];
        if( $p_units ) {
            foreach ( $p_units as $u ) {
                $u->source_type = "Smart World";
                $data[] = $u->toArray();
            }
        }
        return response()->json(array(
            'result' => true,
            'data'  => $data
        ));
    }

    public function apWorldCustomer () {
        $customers = Customer::get();
        $data = [];
        if( $customers ) {
            foreach ( $customers as $c ) {
                $c->owner_type = "Owner";
                $c->source_type = "Smart World";
                $data[] = $c->toArray();
            }
        }

        $tenants = Tenant::get();
        if( $tenants ) {
            foreach ( $tenants as $t ) {
                $t->owner_type  = "Prospect";
                $t->source_type = "Smart World";
                $data[] = $t->toArray();
            }
        }

        return response()->json(array(
            'result' => true,
            'data'  => $data
        ));
    }

    public function apWorldTenant () {
        $tenants = Tenant::get();
        $data = [];
        if( $tenants ) {
            foreach ( $tenants as $t ) {
                $t->owner_type  = "Prospect";
                $t->source_type = "Smart World";
                $data[] = $t->toArray();
            }
        }
        return response()->json(array(
            'result' => true,
            'data'  => $data
        ));
    }

    public function apWorldCustomerProperty () {
        $customer_ps = CustomerProperty::get();
        $data = [];
        if( $customer_ps ) {
            foreach ( $customer_ps as $cp ) {
                $cp->customer_id        = "Smart World".$cp->customer_id;
                $cp->property_unit_id   = "Smart World".$cp->property_unit_id;
                $cp->property_id        = "Smart World".$cp->property_id;
                $cp->source_type        = "Smart World";
                $data[] = $cp->toArray();
            }
        }

        return response()->json(array(
            'result' => true,
            'data'  => $data
        ));
    }

    public function apWorldTenantProperty () {
        $tenant_ps = TenantProperty::get();
        $data = [];
        if( $tenant_ps ) {
            foreach ( $tenant_ps as $tp ) {
                $tp->tenant_id          = "Smart World".$tp->tenant_id;
                $tp->property_unit_id   = "Smart World".$tp->property_unit_id;
                $tp->property_id        = "Smart World".$tp->property_id;
                $tp->customer_type      = "Tenant";
                $tp->source_type        = "Smart World";
                $data[] = $tp->toArray();
            }
        }

        return response()->json(array(
            'result' => true,
            'data'  => $data
        ));
    }
}
