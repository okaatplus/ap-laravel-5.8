<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $auth;
    public function handle($request, Closure $next, $menu_index = null)
    {
        if(  Auth::guest() || (Auth::check() && Auth::user()->role !== 0 && Auth::user()->role !== 1)) {
            if( $request->ajax() ) {
                return response()->json(['permission_denied']);
            }
            return redirect('/');
        } else {

            if( !Auth::user()->active ) {
                return redirect('auth/logout');
            }

            $this->auth = $request;

            $property = $this->auth->user()->property()->get()->toArray();
            if($property && !$property[0]['active_status']) {
                return redirect('auth/logout');
            }
            // get featured menu setting
            view()->share('is_executive', false);

        }return $next($request);
    }
}
