<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Management
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(  Auth::guest() || Auth::check() && Auth::user()->role !== 1 ) {
            if( $request->ajax() ) {
                return response()->json(['permission_denied']);
            }
            return redirect('/');
        } elseif( !Auth::user()->active ) {
            return redirect('auth/logout');
        }

        if( Auth::user()->smart_admin_state ) {
            if( $request->ajax() ) {
                return response()->json(['permission_denied']);
            }
            return redirect('/');
        }

        return $next($request);
    }
}
