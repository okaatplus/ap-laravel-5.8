<?php namespace App\Http\Middleware;

use Closure;
use App\Notification;
use App\Property;
use Illuminate\Contracts\Auth\Guard;

class Notify {

    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
    	if (!$this->auth->guest() && !$request->ajax()) {
	        $noti_list = Notification::where('to_user_id',$this->auth->user()->id)->orderBy('created_at','desc')->paginate(15);
	        $noti_count = Notification::with('sender')->where('to_user_id',$this->auth->user()->id)->where('read_status',false)->count();
	        view()->share('noti_list',$noti_list);
	        view()->share('noti_count',$noti_count);
	    }
        return $next($request);
    }

}
