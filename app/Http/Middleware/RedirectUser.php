<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() || Auth::viaRemember()) {
            switch (Auth::user()->role) {
                case(0) :
                    return redirect('root/admin/property/list');
                    break;

                case(1) :
                    if( Auth::user()->smart_admin_state )
                        return redirect('admin/dashboard');
                    else
                        return redirect('management/admin/dashboard');
                    break;

                case(2) :
                    if( Auth::user()->smart_admin_state )
                        return redirect('admin/dashboard');
                    else
                        return redirect('smart-supervisor/admin/dashboard');
                    break;

                case(3) :
                    return redirect('admin/dashboard');
                    break;

                case(4) :
                    return redirect('technician/admin/parcel-tracking');
                    break;

                case(5) :
                    return redirect('outsource/admin/messages');
                    break;

                case(6) :
                    return redirect('smart-hq-admin/admin/property/list');
                    break;

                case(7) :
                    if( Auth::user()->smart_admin_state )
                        return redirect('call-center/admin/property/units');
                    else
                        return redirect('call-center/admin/property/list');
                    break;



                default;
                    return redirect('auth/logout');
                    break;

            }
        } 
        
        return $next($request);
    }
}
