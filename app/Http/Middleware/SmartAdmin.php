<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SmartAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $menu_index = null)
    {
        if( Auth::guest() || Auth::check() && ( !in_array( Auth::user()->role,[1,2,3,6,7]) ) ) {
            if( $request->ajax() ) {
                return response()->json(['permission_denied']);
            }
            return redirect('/');
        } elseif( !Auth::user()->active ) {
            return redirect('auth/logout');
        }

        if( in_array(Auth::user()->role,[1,2,6,7]) && !Auth::user()->smart_admin_state ) {
            if( $request->ajax() ) {
                return response()->json(['permission_denied']);
            }
            return redirect('/');
        }

        $property = Auth::user()->property()->get()->toArray();
        if($property && !$property[0]['active_status']) {
            return redirect('auth/logout');
        }

        if( Auth::user()->role == 1 || Auth::user()->role == 2) {
            $is_executive  = true;
        } else {
            $is_executive  = false;
        }

        view()->share('is_executive', $is_executive );
        return $next($request);
    }
}
