<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

use Auth;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;

// Firebase
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;

# Model
use App\Property;
use App\User;
use App\Installation;
use App\Notification;

# Jobs
/*use App\Jobs\PushNotificationUserAndroid;
use App\Jobs\PushNotificationUserIOS;
use App\Jobs\PushNotificationSender;*/

class PushNotificationToDevice extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $notification_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification_id)
    {
        //
        $this->notification_id = $notification_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = Notification::with('sender')->find($this->notification_id);

        $user_id = $notification->to_user_id;

        $user_receive = User::find($user_id);

        $property = Property::find($notification->sender->property_id);

        $countInstallation = Installation::select('device_token')->where('user_id', '=', $user_id)->whereNotNull('device_token')->count();

        if(isset($user_receive) && $user_receive->notification && $countInstallation > 0) {

            $noti_count = Notification::with('sender')->where('to_user_id', '=', $user_id)
                ->where('read_status', '=', false)->count();

            $message_string = "";

            /*if($notification->sender->role == 1 || $notification->sender->role == 3) {
                if ($user_receive->lang == 'en') {
                    $sender_name = $property->juristic_person_name_en;
                }else{
                    $sender_name = $property->juristic_person_name_th;
                }
            }else{
                $sender_name = $notification->sender->name;
            }*/
            $sender_name = "Smart World";

            switch ($notification->notification_type) {
                case "0":// News
                    $data = json_decode($notification->title, true);
                    if ($data['type'] == 'news_created') {
                        $message_string = trans('messages.Notification.news_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, $user_receive->lang);
                    }
                    break;
                case "1":// Announcement
                    $data = json_decode($notification->title, true);
                    if ($data['type'] == 'announcement_created') {
                        $message_string = trans('messages.Notification.announcement_created', ['name' => $data['title'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, $user_receive->lang);
                    }
                    break;

                case "2":// Post Parcel
                    $data = json_decode($notification->title, true);
                    if ($data['type'] == 'receive_post_parcel') {
                        $message_string = trans('messages.Notification.receive_post_parcel',$data, null, $user_receive->lang);
                    }
                    break;
                case "3":// request new resident
                    $data = json_decode($notification->title, true);
                    if ($data['type'] == 'request_new_resident') {
                        $message_string = trans('messages.Notification.request_new_resident',$data, null, $user_receive->lang);
                    }
                    break;
                case "4":// already new resident
                    $data = json_decode($notification->title, true);
                    if ($data['type'] == 'accepted_resident') {
                        $message_string = trans('messages.Notification.accepted_resident',$data, null, $user_receive->lang);
                    }
                    break;
                case "5"://Complain
                    $data = json_decode($notification->title, true);
                    if (isset($data['type']) && $data['type'] == 'comment') {
                        $message_string = $sender_name . " " .trans('messages.Notification.complain_comment', $data, null, $user_receive->lang);
                    }elseif($data['type'] == 'change_status'){
                        $data['status'] = trans('messages.Complain.' . $data['status'], array(), null, $user_receive->lang);
                        $message_string = trans('messages.Notification.complain_change_status', $data, null, $user_receive->lang);
                    } elseif($data['type'] == 'complain_created') {
                        $message_string = trans('messages.Notification.complain_created', array(), null, $user_receive->lang);
                    }
                    break;
                case "6"://Survey
                    $data = json_decode($notification->title, true);
                    if( $data['type'] == 'vote_created') {
                        $message_string = trans('messages.Notification.vote_created_msg',['name'=>$data['title_th'],'title_th'=> $data['title_th'],'title_en'=> $data['title_en']], null, Auth::user()->lang);
                    }
                    break;
                default:
                    $message_string = $sender_name . " " . trans('messages.Notification.' . $notification->title, array(), null, $user_receive->lang);
                    break;
            }


            // Change to FCM Notification
            $device_token_obj = Installation::select('device_token')->where('user_id', '=', $user_id)->whereNotNull('device_token')->get();
            $device_token_array = $device_token_obj->toArray();

            $device_token_list = array();
            foreach ($device_token_array as $item){
                $device_token_list[] = $item['device_token'];
            }

            $message = array(
                "priority" => "high",
                'notification' => array(
                    'title' => "Smart World",
                    'text' => $message_string,
                    'click_action' => $notification->notification_type
                ),
                'data' => array(
                    'notification_id' => $this->notification_id,
                    'subject_key' => $notification->subject_key,
                    'notification_type' => $notification->notification_type
                ),
                'badge' => $noti_count
            );

            $optionBuiler = new OptionsBuilder();
            $optionBuiler->setTimeToLive(60*20);
            $priority = 'high'; // or 'normal'
            $optionBuiler->setPriority($priority);


            $notificationBuilder = new PayloadNotificationBuilder('Smart World');
            $notificationBuilder->setBody($message_string)
                ->setClickAction("notification")
                ->setIcon("ic_icon_notification")
                ->setSound('default')
                ->setBadge($noti_count);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($message);

            $option = $optionBuiler->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            //$token = "eRO-BLvRsjY:APA91bEepdr3EbOjpOdu4z4ThBVgRFjaW3TMhv85-AK4ZIjp0vHplxzgclGNFaP0L9YtEV4mzpv5nrum4rlu34lMPFiZRvyxs67Mp-g5_Aty5hmBmwW3ljJUsi1QNPJMgfoD04CsIDAp";
            //$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $tokens = $device_token_list;
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

            //return Array - you should try to resend the message to the tokens in the array
            //$downstreamResponse->tokensToRetry();
        }
    }
}
