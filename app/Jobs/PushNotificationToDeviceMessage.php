<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

use Auth;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;

// Firebase
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;

# Model
use App\Property;
use App\User;
use App\Installation;
use App\Notification;

# Jobs
/*use App\Jobs\PushNotificationUserAndroid;
use App\Jobs\PushNotificationUserIOS;
use App\Jobs\PushNotificationSender;*/

class PushNotificationToDeviceMessage extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        //
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $user_receive = User::find($this->user_id);

        $countInstallation = Installation::select('device_token')->where('user_id', '=', $this->user_id)->whereNotNull('device_token')->count();

        if(isset($user_receive) && $user_receive->notification && $countInstallation > 0) {

            $message_string = trans('messages.Message.receive_message', array(), null, $user_receive->lang);











            // Change to FCM Notification
            $device_token_obj = Installation::select('device_token')->where('user_id', '=', $this->user_id)->whereNotNull('device_token')->get();
            $device_token_array = $device_token_obj->toArray();

            $device_token_list = array();
            foreach ($device_token_array as $item){
                $device_token_list[] = $item['device_token'];
            }

            /*$message = array(
                //'badge' => $noti_all,
                'locKey' => $message_string,
                'locArgs' => array(
                    'notification_id' => "",
                    'subject_key' => "",
                    'notification_type' => 10
                )
            );*/

            $message = array(
                "priority" => "high",
                'notification' => array(
                    'title' => "SmartWorld",
                    'text' => $message_string,
                    'click_action' => 10
                ),
                'data' => array(
                    'notification_id' => "",
                    'subject_key' => "",
                    'notification_type' => 7
                )
            );

            $optionBuiler = new OptionsBuilder();
            $optionBuiler->setTimeToLive(60*20);
            $priority = 'high'; // or 'normal'
            $optionBuiler->setPriority($priority);

            $notificationBuilder = new PayloadNotificationBuilder('Smart World');
            $notificationBuilder->setBody($message_string)
                ->setClickAction("notification")
                ->setIcon("ic_icon_notification")
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($message);

            $option = $optionBuiler->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            //$token = "eRO-BLvRsjY:APA91bEepdr3EbOjpOdu4z4ThBVgRFjaW3TMhv85-AK4ZIjp0vHplxzgclGNFaP0L9YtEV4mzpv5nrum4rlu34lMPFiZRvyxs67Mp-g5_Aty5hmBmwW3ljJUsi1QNPJMgfoD04CsIDAp";
            //$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $tokens = $device_token_list;
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

            //return Array - you should try to resend the message to the tokens in the array
           // $downstreamResponse->tokensToRetry();
        }
    }
}
