<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

use Auth;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;

// Firebase
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

# Model
use App\Property;
use App\User;
use App\Installation;
use App\Notification;
use App\InstallationGuest;
use App\Model\News;
use App\Model\Promotion;
use App\Model\Lifestyle;

# Jobs
/*use App\Jobs\PushNotificationUserAndroid;
use App\Jobs\PushNotificationUserIOS;
use App\Jobs\PushNotificationSender;*/

class PushNotificationToDeviceSendGroupMarketing extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $notification_type,$subject_id,$notification_key,$lang;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification_key,$notification_type,$subject_id,$lang)
    {
        //
        //$this->user_id = $user_id;
        $this->notification_type = $notification_type;
        $this->subject_id = $subject_id;
        $this->notification_key = $notification_key;
        $this->lang = $lang;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->notification_type) {
            case "15"://News
                $subject = News::find($this->subject_id);
                if($this->lang == 'en') {
                    $message_string = "[News] - ". $subject->title_en;
                }else{
                    $message_string = "[News] - ". $subject->title;
                }
                break;
            case "16"://Promotion
                $subject = Promotion::find($this->subject_id);
                if($this->lang == 'en') {
                    $message_string = "[Promotion] - ". $subject->title_en;
                }else{
                    $message_string = "[Promotion] - ". $subject->title;
                }
                break;
            case "17"://Lifestyle
                $subject = Lifestyle::find($this->subject_id);
                if($this->lang == 'en') {
                    $message_string = "[Lifestyle] - ". $subject->title_en;
                }else{
                    $message_string = "[Lifestyle] - ". $subject->title;
                }
                break;
            default:
                $message_string = "[ประกาศข่าวสาร]";
                break;
        }

        $message = array(
            "priority" => "high",
            'notification' => array(
                'title' => "Smart World",
                'text' => $message_string,
                'click_action' => $this->notification_type
            ),
            'data' => array(
                'notification_id' => "",
                'subject_key' => $this->subject_id,
                'notification_type' => $this->notification_type
            )
        );

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);
        $priority = 'high'; // or 'normal'
        $optionBuiler->setPriority($priority);

        $notificationBuilder = new PayloadNotificationBuilder('Smart World');
        $notificationBuilder->setBody($message_string)
            ->setClickAction("notification")
            ->setIcon("ic_icon_notification")
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($message);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$tokens = $device_token_list;
        //$downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);

        $downstreamResponse = FCM::sendToGroup($this->notification_key, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        $downstreamResponse->tokensFailed();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        //$downstreamResponse->tokensToRetry();
    }
}
