<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

use Auth;
use Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;

// Firebase
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

# Model
use App\Property;
use App\User;
use App\Installation;
use App\Notification;
use App\InstallationGuest;
use App\Model\News;
use App\Model\Promotion;
use App\Model\Lifestyle;

class PushNotificationToDeviceSendMarketing extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $notification_type,$subject_id,$lang;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification_type,$subject_id,$lang)
    {
        //
        //$this->user_id = $user_id;
        $this->notification_type = $notification_type;
        $this->subject_id = $subject_id;
        $this->lang = $lang;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        switch ($this->notification_type) {
            case "0"://News
                $subject = News::find($this->subject_id);
                if($this->lang == 'en') {
                    $message_string = "[News] - ". $subject->title_en;
                }else{
                    $message_string = "[ข่าวสาร] - ". $subject->title_th;
                }
                break;
            default:
                $message_string = "[ประกาศข่าวสาร]";
                break;
        }


            // Change to FCM Notification
            $device_token_obj = Installation::select('device_token')->where('lang',$this->lang)->whereNotNull('device_token')->get();
            $device_token_array = $device_token_obj->toArray();

            $device_token_list = array();
            foreach ($device_token_array as $item){
                $device_token_list[] = $item['device_token'];
            }

            $message = array(
                "priority" => "high",
                'notification' => array(
                    'title' => "Smart World",
                    'text' => $message_string,
                    'click_action' => $this->notification_type
                ),
                'data' => array(
                    'notification_id' => "",
                    'subject_key' => $this->subject_id,
                    'notification_type' => $this->notification_type
                )
            );

            $optionBuiler = new OptionsBuilder();
            $optionBuiler->setTimeToLive(60*20);
            $priority = 'high'; // or 'normal'
            $optionBuiler->setPriority($priority);

            $notificationBuilder = new PayloadNotificationBuilder('Smart World');
            $notificationBuilder->setBody($message_string)
                ->setClickAction("notification")
                ->setIcon("ic_icon_notification")
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($message);

            $option = $optionBuiler->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            $tokens = $device_token_list;
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

            //return Array - you should try to resend the message to the tokens in the array
            //$downstreamResponse->tokensToRetry();
        }
    //}
}
