<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

class PushNotificationUserAndroid extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $device_token, $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($device_token, $message)
    {
        //
        $this->device_token = $device_token;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PushNotification::app(['environment' => 'production',
            //'apiKey' => 'AIzaSyD5Dk1jBsUM5-Tap0C--8xPJZZ7FuDGUAg',
            'apiKey' => 'AIzaSyDWiAg7QSetKpEGJB0kXg3g8Old2Mr_n5s',
            'service' => 'gcm'])
            ->to($this->device_token)
            ->send($this->message);
    }
}
