<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\CountValidator\Exception;
use PushNotification;

class PushNotificationUserIOS extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $device_token, $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($device_token, $message)
    {
        //
        $this->device_token = $device_token;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PushNotification::app(['environment' => 'production',
            //'certificate' => storage_path() . '/cert/push_dev_cer.pem',
            //'certificate' => storage_path() . '/cert/push_dist_cer.pem',
            //'certificate' =>storage_path() . '/cert/'.env('CERT_PUSH_NOTIFICATION_DEV'),
            'certificate' =>storage_path() . '/cert/'.env('CERT_PUSH_NOTIFICATION_PRODUCTION'),
            'passPhrase' => '',
            'service' => 'apns'])
            ->to($this->device_token)
            ->send($this->message);
    }
}
