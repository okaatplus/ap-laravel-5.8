<?php

namespace App;

class MessageViewLog extends GeneralModel
{
    protected $table = 'message_view_log';
    public     $timestamps = false;

    public function viewBy () {
        return $this->belongsTo('App\User', 'viewed_by');
    }
    public function saveLog ($message_id,$user_id) {
        $dup = MessageViewLog::where('message_id',$message_id)->where('viewed_by',$user_id)->first();
        if( $dup ) {
            $ml = $dup;
        } else {
            $ml = new MessageViewLog;
            $ml->message_id = $message_id;
            $ml->viewed_by    = $user_id;
        }
        $ml->latest_view_date = date('Y-m-d H:i:s');
        $ml->save();
    }
}
