<?php

namespace App;
use App\GeneralModel;
class Page extends GeneralModel
{
    protected $table = 'pages';
    protected $fillable = ['content'];
	public $timestamps = true;
}
