<?php

namespace App;
use App\GeneralModel;
use Request;
use Auth;
class Post extends GeneralModel
{
    protected  $table = 'post';
    protected $fillable = ['title_th','title_en','description_th','description_en',
        'post_category','property_applying','audience','expired_date','post_type','ecosystem',
        'video_link','external_link','target_dimension','publish_in_advance_date'
        ];
    // Close timestamp
	public     $timestamps = true;
	protected  $rules = array();
    protected  $messages = array();

    public function created_user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function updated_user()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public function send_approval_user()
    {
        return $this->hasOne('App\User', 'id', 'send_approved_by');
    }

    public function approval_user()
    {
        return $this->hasOne('App\User', 'id', 'approved_by');
    }

    public function owner()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function postFile()
    {
        return $this->hasMany('App\PostFile');
    }

    public function postProperty()
    {
        return $this->hasMany('App\PostProperty','post_id','id');
    }

    public function tracking() {
        return $this->hasMany('App\PostTracking','post_id','id');
    }

    public function trackingCount () {
        return $this->tracking()->selectRaw('post_id, count(*) as count')->groupBy('post_id');
    }

}
