<?php

namespace App;
use App\GeneralModel;
use Request;
use Auth;
class PostParcel extends GeneralModel
{
    protected $table = 'post_parcel';
    protected $fillable = ['ems_code','date_received','type','from_name','to_name','to_resident_id','to_resident_type','receive_code','receiver_name','status','property_id','property_unit_id','note','size', 'ref_code'];
    public $timestamps = true;

    public function forUnit()
    {
        return $this->belongsTo('App\PropertyUnit','property_unit_id','id');
    }

    public function forProperty()
    {
        return $this->belongsTo('App\Property','property_id','id');
    }

    public function toCustomer()
    {
        return $this->belongsTo('App\Customer','to_resident_id','id');
    }

    public function toTenant()
    {
        return $this->belongsTo('App\Tenant','to_resident_id','id');
    }

    public function deliveredBy()
    {
        return $this->belongsTo('App\User','delivered_by','id');
    }

    public function canceledBy()
    {
        return $this->belongsTo('App\User','canceled_by','id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
}
