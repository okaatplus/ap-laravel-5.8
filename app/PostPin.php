<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostPin extends Model
{
    protected $table = "post_pin";
    protected $fillable = ['post_id','post_property_id','user_id'];
}
