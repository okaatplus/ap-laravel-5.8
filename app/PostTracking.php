<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTracking extends Model
{
    protected $table = "post_tracking";
    protected $fillable = ['post_id', 'device', 'os'];

    public function user()
    {
        return $this->hasOne('App\user','id','user_id');
    }
}
