<?php
namespace App;

use Carbon\Carbon;

class Property extends GeneralModel
{
    protected $table = 'property';
    protected $fillable = [
        'property_name_th',
        'property_name_en',
        'area_size',
        'unit_size',
        'construction_by',
        'address_th',
        'address_en',
        'street_th',
        'street_en',
        'province',
        'postcode',
        'lat',
        'lng',
        'about',
        'tel',
        'common_area_fee_type',
        'common_area_fee_rate',
        'property_type',
        'tax_id',
        'fax',
        'address_no',
        'juristic_person_name_th',
        'juristic_person_name_en',
        'common_area_fee_land_type',
        'common_area_fee_land_rate',
        'brand',
        'banner_url',
        'district',
        'sub_district',
        'ap_type',
        'dimension',
        'project_code',
        'management_state',
        'parking_slot',
        'parking_slot_max',
        'parking_floor_no',
        'parking_privilege',
        'common_area_size',
        'ownership_area_size',
        'common_fee_rate',
        'common_fee_period',
        'fund_rate',
        'water_rate',
        'fine_rate',
        'cf_method'
    ];
    // Close timestamp
	public $timestamps = true;

	protected $rules = array(
        'property_name_th'          => 'required',
        'property_name_en'          => 'required',
        'juristic_person_name_th'   => 'required',
        'juristic_person_name_en'   => 'required'
    );

    protected $messages = array(
         //'property_name_th.required' => 'ระบุชื่อภาษาไทย',
         //'property_name_en.required' => 'ระบุชื่อภาษาอังกฤษ'
     );

    public function settings()
    {
        return $this->hasOne('App\PropertySettings','property_id','id');
    }

    public function property_admin()
    {
        return $this->hasOne('App\User');
    }

    public function banks()
    {
        return $this->hasMany('App\Bank');
    }

    public function propertyFile()
    {
        return $this->hasMany('App\PropertyFile');
    }

    public function property_unit()
    {
        return $this->hasMany('App\PropertyUnit');
    }


    public function has_province ()
    {
        return $this->hasOne('App\Province','code','province');
    }

    public function has_district ()
    {
        return $this->hasOne('App\Districts','id','district');
    }

    public function has_sub_district ()
    {
        return $this->hasOne('App\SubDistricts','id','sub_district');
    }

    public function has_brand ()
    {
        return $this->hasOne('App\PropertyBrand','id','brand');
    }

    public function users()
    {
        return $this->hasMany('App\User','property_id','id');
    }

    public function unApprovedUser () {
        return $this->hasMany('App\User','property_id','id')->where('role',8);
    }

    public function unReadMessages () {
        return $this->hasMany('App\Message','property_id','id')->where('flag_new_from_user', true);
    }

    public function postsParcels () {
        return $this->hasMany('App\PostParcel','property_id','id');
    }

    public function overduePostsParcels () {
        $last_14_days = Carbon::now();
        $last_14_days = $last_14_days->subDays(14)->format('Y-m-d');
        return $this->hasMany('App\PostParcel','property_id','id')->where('date_received','<=',$last_14_days)->where('status',false);
    }

    public function customerProperty () {
        return $this->hasMany('App\CustomerProperty','property_id','id');
    }

    public function registeredUser () {
        return $this->hasMany('App\UserProperty','property_id','id')->where('approve_status',1);
    }

    public function waitingApprovalUser () {
        return $this->hasMany('App\UserProperty','property_id','id')->where('approve_status',0);
    }

    public function userProperty () {
        return $this->hasMany('App\UserProperty','property_id','id');
    }

    public function tenantProperty () {
        return $this->hasMany('App\TenantProperty','property_id','id');
    }
}
