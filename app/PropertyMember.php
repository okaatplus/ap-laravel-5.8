<?php
namespace App;
use App\GeneralModel;
class PropertyMember extends GeneralModel
{
    protected   $table      = 'users';
    public      $timestamps = true;
    protected   $fillable   = ['name','email','user_type','phone','property_unit_id'];
    
    public function unit () {
        return $this->hasOne('App\PropertyUnit','id','property_unit_id');
    }

    public function UserProperty () {
        return $this->hasMany('App\UserProperty','user_id','id');
    }

    public function ActiveUnit () {
        return $this->hasMany('App\UserProperty','user_id','id')->whereNotNull('property_unit_id')->where('approve_status',1)->where('active_status',true);
    }
}
