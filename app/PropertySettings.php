<?php

namespace App;
use App\GeneralModel;
class PropertySettings extends GeneralModel
{
    protected $table = 'property_settings';
    public $timestamps = false;
    protected $fillable = [
        'property_id',
        'auto_reply_msg',
        'auto_reply_active',
        'auto_reply_start_time',
        'auto_reply_end_time',
        'user_app_timeout',
        'day_auto_remind_parcel',
        'auto_reply_allday_flag'
    ];
}
