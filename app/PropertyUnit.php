<?php

namespace App;
use App\GeneralModel;
class PropertyUnit extends GeneralModel
{
    protected $table = 'property_unit';
    protected $fillable = [
        'property_id',
        'property_type',
        'property_size',
        'address',
        'street',
        'province',
        'postcode',
        'resident_count',
        'price',
        'unit_number',
        'phone',
        'property_unit_unique_id',
        'active',
        'owner_name_th',
        'owner_name_en',
        'delivery_address',
        'transferred_date',
        'insurance_expire_date',
        'invite_code',
        'unit_soi',
        'type',
        'contact_lang',
        'unit_floor',
        'building',
        'ownership_ratio',
        'email',
        'land_no',
        'asset_id',
        'asset_project_id',
        'unit_code',
        'size_label',
        'water_billing',
        'water_billing_rate',
        'electric_billing',
        'electric_billing_rate',
        'utility_charge',
        'extra_cf_charge',
        'garbage_fee',
        'move_in'
    ];
    // Close timestamp
	public $timestamps = false;

    public function property () {
        return $this->belongsTo('App\Property','property_id','id');
    }

    public function propertyUnitCustomer () {
        return $this->hasMany('App\CustomerProperty', 'property_unit_id');
    }
    public function propertyUnitTenant () {
        return $this->hasMany('App\TenantProperty', 'property_unit_id');
    }

    public function propertyUnitOwner () {
        return $this->hasMany('App\CustomerProperty', 'property_unit_id')->where('customer_type',0)->where('customer_status',1);
    }

}
