<?php
namespace App;
use App\GeneralModel;
use Illuminate\Foundation\Application;
use Illuminate\Database\Eloquent\SoftDeletes;

class Propertybrand extends GeneralModel
{
    use SoftDeletes;
    protected $table = 'property_brand';
    public $timestamps = false;
    protected $fillable     = ['name_th','name_en','img_url','brand_type','deleted_at'];


    public function getBand()
    {
        $lang = app()->getLocale();
        $propertybrand = $this->orderBy('name_'.$lang, 'ASC')->pluck('name_'.$lang,'id')->toArray();
        asort($propertybrand);
        $propertybrandFirst = array(''=> trans('messages.AboutProp.brand'));
        $propertybrandAll = $propertybrandFirst + $propertybrand;
        return $propertybrandAll;
    }
}
