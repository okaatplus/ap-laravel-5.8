<?php

namespace App;

class Tenant extends GeneralModel
{
    protected $table = 'tenant';
    public $timestamps = true;

    public function tenantProperty () {
        return $this->hasMany('App\TenantProperty','tenant_id','id');
    }
}
