<?php

namespace App;

class TenantProperty extends GeneralModel
{
    protected $table = 'tenant_property';
    //protected $fillable = ['user_id','property_id','property_unit_id'];
    public $timestamps = true;

    public function tenant () {
        return $this->belongsTo('App\Tenant', 'tenant_id');
    }

    public function unit () {
        return $this->belongsTo('App\PropertyUnit', 'property_unit_id');
    }

    public function property () {
        return $this->belongsTo('App\Property', 'property_id');
    }
}
