<?php

namespace App;

class TimeoutSetting extends GeneralModel
{
    protected $table = 'timeout_setting';
    protected $fillable = ['timeout','user_id'];
    public $timestamps = true;
}
