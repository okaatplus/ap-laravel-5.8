<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\ResetPasswordNotification;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected   $keyType        = 'string';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'property_id','property_unit_id','role',
        'verification_code','profile_pic_name',
        'profile_pic_path','dob','phone','gender',
        'is_chief','user_type','confirmation_code','type_id_card','admin_property_dimension'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     * @note: This override Authenticatable methodology
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function property()
    {
        return $this->hasOne('App\Property','id','property_id');
    }

    public function property_unit()
    {
        return $this->hasOne('App\PropertyUnit','id','property_unit_id');
    }

    public function installation()
    {
        return $this->hasMany('App\Installation','user_id','id');
    }

    public function position()
    {
        return $this->hasOne('App\OfficerRoleAccess','user_id','id');
    }

    public function UserProperty () {
        return $this->hasMany('App\UserProperty','user_id','id');
    }

    public function AdminProperty () {
        return $this->hasMany('App\AdminProperty','user_id','id');
    }

    public function UserCustomer () {
        return $this->hasOne('App\Customer','id','customer_id');
    }

    public function UserTenant () {
        return $this->hasOne('App\Tenant','id','tenant_id');
    }
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function ActiveUnit () {
        return $this->hasMany('App\UserProperty','user_id','id')->where('approve_status',1)->where('active_status',true);
    }
}
