<?php

namespace App;

class UserProperty extends GeneralModel
{
    protected $table = 'user_property';
    protected $fillable = ['user_id','property_id','property_unit_id','active_status','expire_date'];
    public $timestamps = true;

    public function ofUser () {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function unit () {
        return $this->hasOne('App\PropertyUnit','id','property_unit_id');
    }

    public function property () {
        return $this->hasOne('App\Property','id','property_id');
    }
}
