<?php

namespace App;
use App\GeneralModel;
class Vote extends GeneralModel
{
    protected $table = 'vote';
    protected $fillable = ['property_id','user_id','title_th','description_th','start_date','end_date','start_time','end_time','title_en','description_en'];
    public $timestamps = true;
    public function creator()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function voteFile () {
        return $this->hasMany('App\VoteFile');
    }

    public function voteChoice () {
        return $this->hasMany('App\Choice');
    }

    public function userChoose()
    {
        return $this->hasOne('App\UserChoice');
    }
}
