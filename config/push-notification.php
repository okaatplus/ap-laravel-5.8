<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' =>storage_path() . '/cert/'.env('CERT_PUSH_NOTIFICATION_DEV'),
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>env('FCM_SERVER_KEY'),
        'service'     =>'gcm'
    )

);