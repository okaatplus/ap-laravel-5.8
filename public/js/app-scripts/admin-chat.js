$(function () {

    $('#landing-text').on('click','#load-old', function () {
        getOldMessagePage ($(this).attr('data-date'),false);
    });

    $('.messages-box').on('click', function () {
        $('#txt-msg').focus();
    });

    activateFancy ();

    var root = $('#root-url').val();
    var i = 0;
    $('#attachment').dropzone({
        maxFiles: 1,
        maxFilesize: 5,
        url: root+'/upload-file',
        acceptedFiles: ".png, .jpg, .jpeg",
        init : function () { },
        addedfile: function(file)
        {
            $entry = $('<div>').attr({'class':'preview-img-item chat-preview'}).append(
                $('<div class="progress progress-striped chat-upload-progress"><div class="progress-bar progress-bar-warning"></div></div>')
            );
            file.entry = $entry;
            file.progressBar = $entry.find('.progress-bar');
            file.p_progressBar = $entry.find('.progress');
            $('#previews').append($entry);
        },
        uploadprogress: function(file, progress, bytesSent)
        {
            file.progressBar.width(progress + '%');
        },

        success: function(file,xhr)
        {
            file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success');
            file.p_progressBar.hide();
            var removeButton = $('<span>').attr({'class':'remove-img-preview'}).html('&times');
            var _this = this;


            var isImage = 0;
            if(xhr.mime === "image/jpeg" || xhr.mime === "image/png" || xhr.mime === "image/gif"){
                isImage = 1;
            }


            if(isImage) {
                imgAppend = $('<img>').attr({'src':root+"/upload_tmp/"+xhr.name});
            } else {
                imgAppend = $('<img>').attr({'src':root+"/images/file.png",'width':'80px'});
                fileNameLabel = $('<span>').attr('class','file-label').html(xhr.oldname);
            }
            file.entry.prepend(imgAppend).append( removeButton ).append(
                $('<input>').attr({'type':'hidden','name':'attachment['+i+'][name]','value':xhr.name})
            ).append(
                $('<input>').attr({'type':'hidden','name':'attachment['+i+'][mime]','value':xhr.mime})
            ).append(
                $('<input>').attr({'type':'hidden','name':'attachment['+i+'][originalName]','value':xhr.oldname})
            ).append(
                $('<input>').attr({'type':'hidden','name':'attachment['+i+'][isImage]','value':isImage})
            );

            if(!isImage)  file.entry.append(fileNameLabel);

            removeButton.on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                _this.removeFile(file);
                $(this).parent().remove();
            });
            i++;
            imgAppend.load(function () {
                $( window ).resize();
            })
        },
        error: function(file)
        {
            if(file.accepted) {
                file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                this.removeFile(file);
            } else {
                $('#previews').children(':last').remove();
            }
        }
    });
});

function getOldMessagePage (date,loadnew) {
    if(!loadnew)
        $('#load-old').prepend('<i class="fa-spin fa-spinner"></i> ');
    $.ajax({
        url     : $('#root-url').val()+"/admin/messages/page/old",
        data    : ({ 'mid':$('#hidden-mid').val(), 'lm-date':date }),
        method  : 'post',
        dataType: 'html',
        success: function(h){
            if(loadnew)
                $('#landing-old-text').html(h);
            else {
                $('#landing-old-text').find('#load-old').remove();
                $('#landing-old-text').prepend(h);
                //$('#landing-text-block').perfectScrollbar('update');
            }
            clearMessageBox();
            activateFancy ();
        }
    });
}

function clearMessageBox() {
    $('#txt-msg').val('');
    $('.remove-img-preview').each(function () {
        $(this).trigger('click');
    })
}

function activateFancy () {
    $(".fancybox").fancybox({
        helpers: {
            overlay: {
                locked: false
            }
        },
        'padding'           : 0,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'changeFade'        : 0
    });
}