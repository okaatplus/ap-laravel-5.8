$(function () {
    var target_tmp;
    var root = $('#root-url').val();

    $('body').on('click', '.post-delete', function (e) {
        e.preventDefault();
        $('#modal-delete-post').modal('show');
        target_tmp = $(this).attr('data-id');
    });

    $('.confirm-delete').on('click',function () {
        var _this = $(this);
        var url = (_this.data('url')) ? _this.data('url') : "/admin/news-announcement/remove";

        _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : root+ url,
            data    : 'pid='+target_tmp,
            method  : 'post',
            dataType: 'json',
            success: function(r){
                if(r.status){
                    _this.removeAttr('disabled').find('.fa-spinner').remove();
                    $('#modal-delete-post').modal('hide');
                    searchPage (1);
                }
            }
        });
    });

    $('body').on('click', '.send-approve', function (e) {
        e.preventDefault();
        $('#modal-send-approve-post').modal('show');
        target_tmp = $(this).attr('data-id');
    });

    $('.confirm-send-approval').on('click',function () {
        var _this = $(this);
        var url = (_this.data('url')) ? _this.data('url') : "/admin/news-announcement/send-approve";

        _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : root+ url,
            data    : 'pid='+target_tmp,
            method  : 'post',
            dataType: 'json',
            success: function(r){
                var opts = {
                    "positionClass": "toast-top-right",
                    "showDuration": "300",
                    "hideDuration": "1000"
                };
                if(r.status){
                    toastr.success(r.message, null, opts);
                    searchPage (1);
                }
                else toastr.error(r.message, null, opts);
                _this.removeAttr('disabled').find('.fa-spinner').remove();
                $('#modal-send-approve-post').modal('hide');
            }
        });
    });


    $('body').on('click', '.post-recall', function (e) {
        e.preventDefault();
        $('#modal-recall-post').modal('show');
        target_tmp = $(this).attr('data-id');
    });

    $('.confirm-recall').on('click',function () {
        var _this = $(this);
        var url = (_this.data('url')) ? _this.data('url') : "/admin/news-announcement/recall";

        _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : root+ url,
            data    : 'pid='+target_tmp,
            method  : 'post',
            dataType: 'json',
            success: function(r){
                var opts = {
                    "positionClass": "toast-top-right",
                    "showDuration": "300",
                    "hideDuration": "1000"
                };
                if(r.status){
                    toastr.success(r.message, null, opts);
                    searchPage (1);
                }
                else toastr.error(r.message, null, opts);
                _this.removeAttr('disabled').find('.fa-spinner').remove();
                $('#modal-recall-post').modal('hide');
            }
        });
    })
});
