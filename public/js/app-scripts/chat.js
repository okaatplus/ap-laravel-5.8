$(function () {

    $('#landing-text').on('click','#load-old', function () {
        getMessagePage ($(this).data('page'),false);
    });

    $('.messages-box').on('click', function () {
        $('#txt-msg').focus();
    });
    activateFancy ();
});

function activateFancy () {
    $(".fancybox").fancybox({
        helpers: {
            overlay: {
                locked: false
            }
        },
        'padding'           : 0,
        'transitionIn'      : 'elastic',
        'transitionOut'     : 'elastic',
        'changeFade'        : 0
    });
}