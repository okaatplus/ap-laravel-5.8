var opts = {
    "positionClass": "toast-top-right",
    "showDuration": "300",
    "hideDuration": "1000"
};
$(function () {
    $('body').on('click', '.reset-data-link', function () {
        target_prop = $(this).attr('data-pid');
    });

    $('#confirm-reset-data').on('click', function () {
        $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        resetData(target_prop);
    });
});
function resetData (pid) {
    $.ajax({
        url     : $('#root-url').val()+"/root/admin/property/reset",
        method	: "POST",
        data 	: ({pid:pid}),
        dataType: "json",
        success: function (r) {
            if(r.result) {
                toastr.success(r.message, null, opts);
                propertyPage (1);
                $('#reset-data-modal').modal('hide');
                $('#confirm-reset-data').removeAttr('disabled').find('.fa-spin').remove();

                //alert('ล้างข้อมูลเรียบร้อยแล้ว');
                //location.reload();
            }
        }
    });
}