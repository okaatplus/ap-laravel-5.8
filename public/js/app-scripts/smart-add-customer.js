$(function () {
    $('body').on('click', '.add-customer-link', function () {
        target_prop = $(this).attr('data-pid');
        $('#review-customer-data-content').hide();
        $('#reading-customer-txt').show();
        $('#import-customer').unbind();
    });

    ///// Drop Zone
    var root = $('#root-url').val();
    $('#attachment-customer').dropzone({
        maxFiles: 1,
        maxFilesize: 5,
        url: root+'/upload-file',
        acceptedFiles: ".xls, .xlsx",
        addedfile: function(file)
        {
            $entry = $('<div>').attr({'class':'preview-img-item no-border'}).append(
                $('<div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div>')
            );
            file.entry = $entry;
            file.progressBar = $entry.find('.progress-bar');
            file.p_progressBar = $entry.find('.progress');
            $('#preview-customer-file').append($entry);
        },
        uploadprogress: function(file, progress, bytesSent)
        {
            file.progressBar.width(progress + '%');
        },

        success: function(file,xhr)
        {
            $('#attachment-unit,#reading-customer-txt').show();
            $('#review-customer-data-content').hide();
            $('#preview-customer-file').find('.preview-img-item').remove();
            this.removeFile(file);
            $('#start-import-customer').hide();
            startCheckingCustomerData(xhr.name);
            file_name = xhr.name;
        },
        error: function(file)
        {
            if(file.accepted) {
                file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                this.removeFile(file);
            } else {
                $('#preview-customer-file').children(':last').remove();
            }
        }
    });
    $('#start-import-customer').on('click', function () {
        if( key_check_valid ) {
            $('#cancel-modal-customer').hide();
            $('#modal-import-review-customer-data .modal-dialog').removeAttr('style');
            $('#review-customer-data-content').html('<i class="fa-spin fa-spinner"></i> Importing...');
            var _this = $(this);
            _this.prepend('<span><i class="fa-spin fa-spinner"></i> </span>').attr('disabled', 'disabled');
            $.ajax({
                url: $('#root-url').val() + "/root/admin/property/customer/start-import",
                data: ({file_name: file_name, pid: target_prop}),
                dataType: "json",
                method: 'post',
                success: function (r) {
                    if (r.result) {
                        $('#review-customer-data-content').html(r.message);
                    }
                    key_check_valid = false;
                    _this.removeAttr('disabled').hide().find('.fa-spin').remove();
                    $('#close-modal-customer').show();
                    propertyPage (1);
                },
                statusCode: {
                    500: function() {
                        alert("Internal 500 Error");
                    }
                }
            });
        }
    })
});

function startCheckingCustomerData (file_name) {
    $('#import-customer').on('hidden.bs.modal', function () {
        $('#modal-import-review-customer-data').modal('show');
        $('#modal-import-review-customer-data .modal-dialog').removeAttr('style');
    }).modal('hide');

    $.ajax({
        url: $('#root-url').val() + "/root/admin/property/customer/check-data",
        data: ({file_name:file_name,pid: target_prop}),
        dataType: "json",
        method: 'post',
        success: function (r) {
            if( r.result ) {
                key_check_valid = true;
                $('#start-import-customer').show();
            } else {
                key_check_valid = false;
            }
            if( r.data_checked ) {
                $('#modal-import-review-customer-data .modal-dialog').css('width','96%');
            }
            $('#reading-customer-txt').hide();
            $('#cancel-modal-customer').show();
            $('#close-modal-customer').hide();
            $('#review-customer-data-content').html(r.data).show();
            $(window).resize();
        },
        error: function(){
            //alert('Error : Internal error');
        },
        /*,
        statusCode: {
            500: function(data) {
                alert(data.message);
            }
        }*/
    });
}