$(function () {
    $('body').on('click', '.add-unit-link', function () {
        target_prop = $(this).attr('data-pid');
        $('#review-update-unit-data-content').hide();
        $('#reading-unit-txt').show();
        $('#import-unit').unbind();
    });

    ///// Drop Zone
    var root = $('#root-url').val();
    $('#attachment-unit').dropzone({
        maxFiles: 1,
        maxFilesize: 5,
        url: root+'/upload-file',
        acceptedFiles: ".xls, .xlsx",
        addedfile: function(file)
        {
            $entry = $('<div>').attr({'class':'preview-img-item no-border'}).append(
                $('<div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div>')
            );
            file.entry = $entry;
            file.progressBar = $entry.find('.progress-bar');
            file.p_progressBar = $entry.find('.progress');
            $('#preview-file').append($entry);
        },
        uploadprogress: function(file, progress, bytesSent)
        {
            file.progressBar.width(progress + '%');
        },

        success: function(file,xhr)
        {
            $('#attachment-unit,#reading-unit-txt').show();
            $('#review-unit-data-content').hide();
            $('#preview-file').find('.preview-img-item').remove();
            this.removeFile(file);
            $('#start-import-unit').hide();
            startCheckingUnitData(xhr.name);
            file_name = xhr.name;
        },
        error: function(file)
        {
            if(file.accepted) {
                file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                this.removeFile(file);
            } else {
                $('#preview-file').children(':last').remove();
            }
        }
    });
    $('#start-import-unit').on('click', function () {
        if( key_check_valid ) {
            $('#cancel-modal-unit').hide();
            $('#modal-import-review-data .modal-dialog').removeAttr('style');
            $('#review-unit-data-content').html('<i class="fa-spin fa-spinner"></i> Importing...');
            var _this = $(this);
            _this.prepend('<span><i class="fa-spin fa-spinner"></i> </span>').attr('disabled', 'disabled');
            $.ajax({
                url: $('#root-url').val() + "/root/admin/property/unit/start-import",
                data: ({file_name: file_name, pid: target_prop}),
                dataType: "json",
                method: 'post',
                success: function (r) {
                    if (r.result) {
                        $('#review-unit-data-content').html(r.message);
                    }
                    key_check_valid = false;
                    _this.removeAttr('disabled').hide().find('.fa-spin').remove();
                    $('#close-modal-unit').show();
                    propertyPage (1);
                }
            });
        }
    })
});

function startCheckingUnitData (file_name) {
    $('#import-unit').on('hidden.bs.modal', function () {
        $('#modal-import-review-data').modal('show');
        $('#modal-import-review-data .modal-dialog').removeAttr('style');
    }).modal('hide');

    $.ajax({
        url: $('#root-url').val() + "/root/admin/property/unit/check-data",
        data: ({file_name:file_name}),
        dataType: "json",
        method: 'post',
        success: function (r) {
            if( r.result ) {
                key_check_valid = true;
                $('#start-import-unit').show();
            } else {
                key_check_valid = false;
            }

            if( r.data_checked ) {
                $('#modal-import-review-data .modal-dialog').css('width','96%');
            }

            $('#reading-unit-txt').hide();
            $('#cancel-modal-unit').show();
            $('#close-modal-unit').hide();
            $('#review-unit-data-content').html(r.data).show();
            $(window).resize();
        }
    });
}