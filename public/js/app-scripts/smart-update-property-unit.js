$(function () {
    $('body').on('click', '.update-unit-link', function () {
        target_prop = $(this).attr('data-pid');
        $('#review-unit-data-content').hide();
        $('#reading-update-unit-txt').show();
        $('#update-unit').unbind();
    });

    ///// Drop Zone
    var root = $('#root-url').val();
    $('#attachment-update-unit').dropzone({
        maxFiles: 1,
        maxFilesize: 5,
        url: root+'/upload-file',
        acceptedFiles: ".xls, .xlsx",
        addedfile: function(file)
        {
            $entry = $('<div>').attr({'class':'preview-img-item no-border'}).append(
                $('<div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div>')
            );
            file.entry = $entry;
            file.progressBar = $entry.find('.progress-bar');
            file.p_progressBar = $entry.find('.progress');
            $('#preview-update-unit-file').append($entry);
        },
        uploadprogress: function(file, progress, bytesSent)
        {
            file.progressBar.width(progress + '%');
        },

        success: function(file,xhr)
        {
            $('#attachment-update-unit,#reading-update-unit-txt').show();
            $('#review-update-unit-data-content').hide();
            $('#preview-update-unit-file').find('.preview-img-item').remove();
            this.removeFile(file);
            $('#start-update-unit').hide();
            startCheckingUpdateUnitData(xhr.name);
            file_name = xhr.name;
        },
        error: function(file)
        {
            if(file.accepted) {
                file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                this.removeFile(file);
            } else {
                $('#preview-update-unit-file').children(':last').remove();
            }
        }
    });
    $('#start-update-unit').on('click', function () {
        if( key_check_valid ) {
            $('#cancel-modal-update-unit').hide();
            $('#modal-update-unit-review-data .modal-dialog').removeAttr('style');
            $('#review-update-unit-data-content').html('<i class="fa-spin fa-spinner"></i> Updating...');
            var _this = $(this);
            _this.prepend('<span><i class="fa-spin fa-spinner"></i> </span>').attr('disabled', 'disabled');
            $.ajax({
                url: $('#root-url').val() + "/root/admin/property/unit/start-update",
                data: ({file_name: file_name, pid: target_prop}),
                dataType: "json",
                method: 'post',
                success: function (r) {
                    if (r.result) {
                        $('#review-update-unit-data-content').html(r.message);
                    }
                    key_check_valid = false;
                    _this.removeAttr('disabled').hide().find('.fa-spin').remove();
                    $('#close-modal-update-unit').show();
                    propertyPage (1);
                }
            });
        }
    })
});

function startCheckingUpdateUnitData (file_name) {
    $('#update-unit').on('hidden.bs.modal', function () {
        $('#modal-update-unit-review-data').modal('show');
        $('#modal-update-unit-review-data .modal-dialog').removeAttr('style');
    }).modal('hide');

    $.ajax({
        url: $('#root-url').val() + "/root/admin/property/unit/check-update-data",
        data: ({file_name:file_name,pid: target_prop}),
        dataType: "json",
        method: 'post',
        success: function (r) {
            if( r.result ) {
                key_check_valid = true;
                $('#start-update-unit').show();
            } else {
                key_check_valid = false;
            }

            if( r.data_checked ) {
                $('#modal-update-unit-review-data .modal-dialog').css('width','96%');
            }

            $('#reading-update-unit-txt').hide();
            $('#cancel-modal-update-unit').show();
            $('#close-modal-update-unit').hide();
            $('#review-update-unit-data-content').html(r.data).show();
            $(window).resize();
        }
    });
}