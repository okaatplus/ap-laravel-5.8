$(function () {
    var target_tmp;
    var root = $('#root-url').val();

    $('body').on('click', '.post-delete', function (e) {
        e.preventDefault();
        $('#modal-delete-post').modal('show');
        target_tmp = $(this).attr('data-id');
    });

    $('.confirm-delete').on('click',function () {
        var _this = $(this);
        var url = (_this.data('url')) ? _this.data('url') : "/smart-supervisor/admin/news-announcement/remove";

        _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : root+ url,
            data    : 'pid='+target_tmp,
            method  : 'post',
            dataType: 'json',
            success: function(r){
                var opts = {
                    "positionClass": "toast-top-right",
                    "showDuration": "300",
                    "hideDuration": "1000"
                };
                if(r.status){
                    toastr.success(r.message, null, opts);
                    searchPage (1);
                }
                else toastr.error(r.message, null, opts);
                _this.removeAttr('disabled').find('.fa-spinner').remove();
                $('#modal-delete-post').modal('hide');
            }
        });
    });

    $('body').on('click', '.post-approve', function (e) {
        e.preventDefault();
        $('#modal-approve-post').modal('show');
        target_tmp = $(this).attr('data-id');
    });

    $('.confirm-approve').on('click',function () {
        var _this = $(this);
        var url = (_this.data('url')) ? _this.data('url') : "/smart-supervisor/admin/news-announcement/approve";

        _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : root+ url,
            data    : 'pid='+target_tmp,
            method  : 'post',
            dataType: 'json',
            success: function(r){
                var opts = {
                    "positionClass": "toast-top-right",
                    "showDuration": "300",
                    "hideDuration": "1000"
                };
                if(r.status){
                    toastr.success(r.message, null, opts);
                    searchPage (1);
                }
                else toastr.error(r.message, null, opts);
                _this.removeAttr('disabled').find('.fa-spinner').remove();
                $('#modal-approve-post').modal('hide');
            }
        });
    });

    $('body').on('click', '.post-reject', function (e) {
        e.preventDefault();
        $('#modal-reject-post').modal('show');
        target_tmp = $(this).attr('data-id');
    });

    $('.confirm-reject').on('click',function () {
        var _this = $(this);
        var url = (_this.data('url')) ? _this.data('url') : "/smart-supervisor/admin/news-announcement/reject";

        _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : root+ url,
            data    : 'pid='+target_tmp,
            method  : 'post',
            dataType: 'json',
            success: function(r){
                var opts = {
                    "positionClass": "toast-top-right",
                    "showDuration": "300",
                    "hideDuration": "1000"
                };
                if(r.status){
                    toastr.success(r.message, null, opts);
                    searchPage (1);
                }
                else toastr.error(r.message, null, opts);
                _this.removeAttr('disabled').find('.fa-spinner').remove();
                $('#modal-reject-post').modal('hide');
            }
        });
    });
});
