
$(function () {

    var root = $('#root-url').val();

    var imported = document.createElement('script');
    imported.src = root+'/js/cropper/cropper.min.js';
    document.head.appendChild(imported);

    $('body').on('click', '.remove-banner', function () {
        $('#remove-banner-flag').val(1);
        $(this).parent().remove();
        $('#attachment-banner-crop').show();
    })

    $('#attachment-banner-crop').dropzone({
        uploadMultiple:false,
        url: root+'/upload-file',
        acceptedFiles: "image/*",
        addedfile: function(file)
        {
            $entry = $('<div>').attr({'class':'preview-img-item'}).append(
                $('<div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div>')
            );
            file.entry = $entry;
            file.progressBar = $entry.find('.progress-bar');
            file.p_progressBar = $entry.find('.progress');
            $('#previews-img-banner-crop').append($entry);
        },
        uploadprogress: function(file, progress, bytesSent)
        {
            file.progressBar.width(progress + '%');
        },

        success: function(file,xhr)
        {
            file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success');
            file.p_progressBar.hide();
            var removeButton = $('<span>').attr({'class':'remove-img-preview'}).html('&times');
            var _this = this;
            var img = $('<img>').attr({'src':root+"/upload_tmp/"+xhr.name, 'style':"max-width:100px;"});
            file.entry.prepend(
                img
            ).append( removeButton ).append(
                $('<input>').attr({'type':'hidden','name':'img_post_banner[name]','value':xhr.name})
            );
            removeButton.on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                _this.removeFile(file);
                $(this).parent().remove();
                $('#attachment-banner-crop').show();
                $('.preview-crop,.img-container').hide();
            });
            img.load(function () {
                $( window ).resize();
            })
            $('#attachment-banner-crop').hide();

            file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success').fadeOut(500,function() { $(this).parent().remove()});

            $('.img-container').html('');
            $('.img-container').append($('<img/>').attr({'src':root+"/upload_tmp/"+xhr.name}));

            var $image = $(".img-container img");
            var $x = $("#img-x");
            var $y = $("#img-y");
            var $w = $("#img-w");
            var $h = $("#img-h");

            $('.preview-crop,.img-container').show();

            // Plugin Initialization
            $image.cropper({
                aspectRatio: 1.5,
                preview: '.preview-crop',
                done: function(data) {
                    $x.val( data.x );
                    $y.val( data.y );
                    $w.val( data.width );
                    $h.val( data.height );

                    var w = $('.preview-crop').width();
                    $('.preview-crop').height(w*.6667);
                }
            });
        },
        error: function(file)
        {
            if(file.accepted) {
                file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                this.removeFile(file);
            } else {
                $('#previews').children(':last').remove();
            }
        }
    });

    $('.remove-banner').on('click', function () {
        $('#remove-banner-flag').val(1);
        $(this).parent().remove();
        $('#attachment-banner-crop').show();
    });
})