// Generate a password string
function randString(id){
    var dataSet = $(id).attr('data-character-set').split(',');
    var possible = '';
    if($.inArray('a-z', dataSet) >= 0){
        possible += 'abcdefghijklmnopqrstuvwxyz';
    }
    if($.inArray('A-Z', dataSet) >= 0){
        possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if($.inArray('0-9', dataSet) >= 0){
        possible += '0123456789';
    }
    if($.inArray('#', dataSet) >= 0){
        possible += '!@#$%^&*()_+';
    }
    var text = '';
    for(var i=0; i < 6; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$(function () {
    // Create a new password
    $("body").on('click','.getNewPass', function(){
        var field = $(this).parents('.input-group').find('input[rel="gp"]');
        var confirm = $('#'+field.attr('data-input-confirm'));
        var pass = randString(field);
        field.val(pass);
        confirm.val(pass);

        $('#generated-password').html(pass);
        $('#password-modal').modal('show');
    });
});

