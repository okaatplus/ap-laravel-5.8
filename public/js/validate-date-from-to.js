$(function () {


    $.validator.addMethod('dateAfter', function (value, element, params) {
        // if start date is valid, validate it as well
        var start_date = $(params[0]).val();
        var start_time = $(params[1]).val();

        var end_date = $(element).val();
        var end_time = $('#end-time').val();

        date_start  = new Date(start_date + " " + start_time);
        date_end    = new Date(end_date + " " + end_time);

        if ( date_end > date_start ) {
            $('#end-time').removeClass('error');
            return true;
        } else {
            $('#end-time').addClass('error');
            return false;
        }

    }, 'Must be after corresponding start date');


    $.validator.addMethod('dateAfterIfNotEmptyStart', function (value, element, params) {
        // if start date is valid, validate it as well

        if( $(params[0]).val() != "" ) {
            var start_date = $(params[0]).val();
            var start_time = $(params[1]).val();

            var end_date = $(element).val();
            var end_time = $('#end-time').val();

            date_start  = new Date(start_date + " " + start_time);
            date_end    = new Date(end_date + " " + end_time);

            if ( date_end > date_start ) {
                $('#end-time').removeClass('error');
                return true;
            } else {
                $('#end-time').addClass('error');
                return false;
            }
        } else {
            return true;
        }
    }, 'Must be after corresponding start date');
})