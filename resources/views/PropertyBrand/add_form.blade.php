<div class="row">
    <div class="col-sm-12">
        {!! Form::model($brand,array('url'=>'root/admin/brand/add_brand','method'=>'post','class'=>'form-horizontal','id'=>'p_form')) !!}
        <div class="modal-body">
            <div class="form-group ">
                <label class="col-sm-4 control-label">{{ trans('messages.property_brand.name_th') }}</label>
                <div class="col-sm-8">
                    {!! Form::text('name_th',null,array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group ">
                <label class="col-sm-4 control-label">{{ trans('messages.property_brand.name_en') }}</label>
                <div class="col-sm-8">
                    {!! Form::text('name_en',null,array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" for="profile-image">{{ trans('messages.property_brand.img') }}</label>
                <div class="col-md-4">
                    <div id="attachment-banner-crop" class="drop-property-file dz-clickable" style="margin-bottom: 0; @if(!empty($brand['img_url']) && $brand['img_url']) display: none; @endif">
                        <i class="fa fa-camera"></i>
                    </div>
                    <div class="preview-crop"></div>
                    <div class="post-banner" id="previews-img-banner-crop">
                        @if(!empty($brand['img_url']) && $brand['img_url'] != null)
                            <div class="preview-img-item" style="margin: 0;">
                                <img src="{{ env('URL_S3')."/post-file".$brand['img_url'] }}" width="100%" alt="post-image" />
                                <span class="remove-img-preview remove-banner">×</span>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="img-container">
                        <img class="img-responsive" />
                    </div>
                </div>
                <input type="hidden" id="img-x" name="img-x"/>
                <input type="hidden" id="img-y" name="img-y"/>
                <input type="hidden" id="img-w" name="img-w"/>
                <input type="hidden" id="img-h" name="img-h"/>
                <input type="hidden" id="img-tw" name="img-tw"/>
                <input type="hidden" id="img-th" name="img-th"/>
            </div>

            <div class="form-group ">
                <label class="col-sm-4 control-label">{{ trans('messages.property_brand.brand') }}</label>
                <div class="col-sm-8">
                    {!! Form::select('brand_type',[
                    '' =>'',
                    '0'=>'0',
                    '1'=>'1',
                    '2'=>'2',
                    '3'=>'3',
                    '4'=>'4'
                    ],null,array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default text-right">
                        <button type="botton" class="btn btn-default" data-dismiss="modal">{!! trans('messages.cancel') !!}</button>
                        <button type="botton" class="btn btn-primary click-load" id="">{!! trans('messages.save') !!}</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
