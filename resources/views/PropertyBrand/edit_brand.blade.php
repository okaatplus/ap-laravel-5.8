@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.AboutProp.brand') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/property') }}"><i class="fa-home"></i>Home</a>
                </li>
                <li><a href="#">{{ trans('messages.AboutProp.brand') }}</a></li>
                <li class="active">
                    <strong>{{ trans('messages.AboutProp.brand') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ trans('messages.AboutProp.editbrand') }}</h3>
                </div>

                <div class="panel-body" id="landing-property-list">
                    @include('PropertyBrand.add_form')
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-add-property-unit.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-add-customer.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/nabour-upload-file.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/crop-img.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/nabour-upload-file.js"></script>

    <link rel="stylesheet" href="{{url('/')}}/js/cropper/cropper.min.css">
@endsection
