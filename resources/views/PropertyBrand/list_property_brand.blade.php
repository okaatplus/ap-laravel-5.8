@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.AboutProp.brand') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/property') }}"><i class="fa-home"></i>Home</a>
                </li>
                <li><a href="#">{{ trans('messages.AboutProp.brand') }}</a></li>
                <li class="active">
                    <strong>{{ trans('messages.AboutProp.brand') }}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ trans('messages.search') }}</h3>
                </div>
                <div class="panel-body search-form">
                    <form method="POST" id="search-form" action="#" accept-charset="UTF-8" class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-3 block-input">
                                <input class="form-control" size="25" placeholder="{{ trans('messages.name') }}" name="name">
                            </div>
                            <div class="col-sm-3">
                                {{--{!! Form::select('province', $provinces,null,['id'=>'property-province','class'=>'form-control']) !!}--}}
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="reset" class="btn btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
                                <button type="button" class="btn btn-secondary @if(isset($demo)) d-search-property @else p-search-property @endif">{{ trans('messages.search') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn btn-info btn-primary action-float-right add-property-brand"><i class="fa fa-plus"> </i> {{ trans('messages.AboutProp.brand') }}</button>


    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body" id="landing-property-list">
                    @include('PropertyBrand.list_property_brand_element')
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-active" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.confirm_ban_head') }}</h4>
                </div>
                <div class="modal-body">
                    {!! trans('messages.Member.confirm_ban_msg') !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-inactive" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.confirm_active_head') }}</h4>
                </div>
                <div class="modal-body">
                    {!! trans('messages.Member.confirm_active_msg') !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="import-unit" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Prop_unit.unit_import') }}</h4>
                </div>
                <div class="modal-body">
                    <div id="upload-section">
                        <div id="attachment-unit" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
                            <i class="fa fa-file-excel-o"></i> &nbsp;{{ trans('messages.Prop_unit.unit_import_txt') }}
                        </div>
                        <div class="post-banner" id="preview-file"></div>
                    </div>
                    <div id="import-section" style="display:none;"></div>
                    <div id="import-msg" style="display:none;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-import-review-data" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Prop_unit.unit_import') }}</h4>
                </div>
                <div class="modal-body">
                    <div id="reading-unit-txt">
                        {{ trans('messages.Importing.checking_data') }}
                    </div>
                    <div id="review-unit-data-content">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="start-import-unit">{{ trans('messages.Prop_unit.start_import') }}</button>
                    <button type="button" id="cancel-modal-unit" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" id="close-modal-unit" class="btn btn-white close-import" data-dismiss="modal" style="display: none;">{{ trans('messages.close') }}</button>
                </div>
            </div>
        </div>
    </div>

    <!----- Customer data importing function --------->
    <div class="modal fade" id="import-customer" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Customer.customer_import') }}</h4>
                </div>
                <div class="modal-body">
                    <div id="upload-section">
                        <div id="attachment-customer" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
                            <i class="fa fa-file-excel-o"></i> &nbsp;{{ trans('messages.Customer.customer_import_txt') }}
                        </div>
                        <div class="post-banner" id="preview-customer-file"></div>
                    </div>
                    <div id="import-customer-section" style="display:none;"></div>
                    <div id="import-customer-msg" style="display:none;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-import-review-customer-data" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Customer.customer_import') }}</h4>
                </div>
                <div class="modal-body">
                    <div id="reading-customer-txt">
                        {{ trans('messages.Importing.checking_data') }}
                    </div>
                    <div id="review-customer-data-content">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="start-import-customer">{{ trans('messages.Prop_unit.start_import') }}</button>
                    <button type="button" id="cancel-modal-customer" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" id="close-modal-customer" class="btn btn-white close-import" data-dismiss="modal" style="display: none;">{{ trans('messages.close') }}</button>
                </div>
            </div>
        </div>
    </div>

    {{--view brand--}}
    <div class="modal fade" id="brand-modal" role="dialog" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.AboutProp.detailbrand') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="view-brand" class="form">

                            </div>
                        </div>
                    </div>
                    <span class="v-loading">กำลังค้นหาข้อมูล...</span>
                </div>
            </div>
        </div>
    </div>
    {{--end view brand--}}

    {{--add brand--}}
    <div class="modal fade" id="add-brand-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.property_brand.add') }}</h4>
                </div>
                <div class="modal-body">
                    @include('PropertyBrand.add_form')
                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{--end add brand--}}

    {{--edit brand--}}
    <div class="modal fade" id="edit-modal" role="dialog" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.AboutProp.detailbrand') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="view-edit-brand" class="form">

                            </div>
                        </div>
                    </div>
                    <span class="v-loading1">กำลังค้นหาข้อมูล...</span>
                </div>
            </div>
        </div>
    </div>
    {{--end edit brand--}}

@endsection
@section('script')
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-add-property-unit.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-add-customer.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/nabour-upload-file.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/crop-img.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/nabour-upload-file.js"></script>

    <script>
        $(function(){

            $("#p_form").validate({
                rules: {
                    name_th  	: 'required',
                    name_en 	: 'required',
                    brand_type  : 'required'
                },
                errorPlacement: function(error, element) { element.addClass('error'); }
            });

            $('.view-brand').on('click',function(){
                var id = $(this).data('uid');
                $('#brand-modal').modal('show');
                $('.v-loading').show();
                $('#view-brand').empty();
                $.ajax({
                    url : $('#root-url').val()+"/root/admin/brand/view_brand",
                    method : 'post',
                    dataType : 'html',
                    data : ({'id':id}),
                    success : function(r){
                        $('.v-loading').hide();
                        $('#view-brand').html(r);

                    },error : function(){
                        console.log('ขออภัยคำสั่งไม่สามารถทำงานได้กรุณาตรวจสอบ !!!!');
                    }
                });
            });

            $('.add-property-brand').on('click',function(){
                $('#add-brand-modal').modal('show');
            });

            $('#submit-form').on('click', function () {
                if($("#p_form").valid()) {
                    $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                    $("#p_form").submit();
                }
            });

            $('.edit-brand').on('click',function(){
               var id = $(this).data('uid');
                $('#edit-modal').modal('show');
                $('.v-loading1').show();
                $('#view-edit-brand').empty();
               $.ajax({
                  url : $('#root-url').val()+"/root/admin/brand/edit_brand",
                  method : 'post',
                  dataType : 'html',
                  data : ({'id':id}),
                  success : function (e) {
                      $('.v-loading1').hide();
                      $('#view-edit-brand').html(e);
                  },error : function(){
                      console.log('ขออภัยคำสั่งไม่สามารถทำงานได้กรุณาตรวจสอบ !!!!');
                   }
               });
            });

            $('.delete-brand').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('uid');
                swal({
                        title: "{{ trans('messages.delete_swal') }}",
                        type: "warning",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes!",
                        showCancelButton: true,
                    },
                    function() {
                        $.ajax({
                            url : $('#root-url').val()+"/root/admin/brand/delete_brand",
                            method : 'post',
                            dataType : 'html',
                            data : ({'id':id}),
                            success: function (e) {
                               //window.location.href='/root/admin/brand/list'
                                swal({
                                    title: "Success",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                }, function(){
                                    window.location.href='/root/admin/brand/list'
                                });
                            }
                        });
                    });
            });

            $('.p-search-property').on('click',function () {
                propertyBrandPage (1);
            });


            function propertyBrandPage (page) {
                var data = $('#search-form').serialize()+'&page='+page;
                $('#landing-property-list').css('opacity','0.6');
                $.ajax({
                    url     : $('#root-url').val()+"/root/admin/brand/list",
                    data    : data,
                    dataType: "html",
                    method: 'post',
                    success: function (h) {
                        $('#landing-property-list').css('opacity','1').html(h);
                    }
                })
            }

            $('.reset-s-btn').on('click',function () {
                $(this).closest('form').find("input").val("");
                $(this).closest('form').find("select option:selected").removeAttr('selected');
                propertyBrandPage (1);
            });
        });

    </script>
    <link rel="stylesheet" href="{{url('/')}}/js/cropper/cropper.min.css">
@endsection
