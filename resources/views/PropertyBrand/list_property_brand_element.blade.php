@if($p_rows->count() > 0)
    <?php
    $to   	= $p_rows->total() - (($p_rows->currentPage())*$p_rows->perPage());
    $to     = ($to > 0) ? $to : 1;
    $from   = $p_rows->total() - (($p_rows->currentPage())*$p_rows->perPage())+$p_rows->perPage();
    $allpage = $p_rows->lastPage();
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$p_rows->total()]) !!}<br/><br/>
            </div>
        </div>
        @if($allpage > 1)
            <div class="col-md-6 text-right">
                @if($p_rows->currentPage() > 1)
                    <a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                @endif
                @if($p_rows->lastPage() > 1)
                    <?php echo Form::selectRange('page', 1, $p_rows->lastPage(),$p_rows->currentPage(),['class'=>'form-control p-paginate-select paginate-select']); ?>
                @endif
                @if($p_rows->hasMorePages())
                    <a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                @endif
            </div>
        @endif
    </div>

    <table class="table table-bordered table-striped" id="p-list" width="100%">
        @if(!empty($p_rows))
            <thead>
            <tr>
                <th width="7%">{{ trans('messages.Report.sequence') }}</th>
                <th width="*">{{ trans('messages.AboutProp.brand') }}</th>
                <th width="180px">{{ trans('messages.action') }}</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            @foreach($p_rows as $key => $row)
                <tr>
                    <td class="text-center">{{ ($key+1) }}</td>
                    <td class="name">{{$row->name_th." / ".$row->name_en}}</td>
                    <td>
                            <a href="#" class="btn btn-info view-brand" data-status="0" data-uid="{{ $row->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.view') }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{!! url('root/admin/brand/edit_brand/'.$row->id) !!}" class="btn btn-success" data-uid="{{ $row->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.edit') }}">
                                <i class="fa-edit"></i>
                            </a>
                            <a href="#" class="btn btn-danger delete-brand" data-status="1" data-uid="{{ $row->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.delete') }}">
                                <i class="fa-trash"></i>
                            </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        @else
            <tr><td> Not found </td></tr>
        @endif
    </table>

    <div class="row">
        <div class="col-md-6">
            <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$p_rows->total()]) !!}
            </div>
        </div>
        @if($allpage > 1)
            <div class="col-md-6 text-right">
                @if($p_rows->currentPage() > 1)
                    <a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                @endif
                @if($p_rows->lastPage() > 1)
                    <?php echo Form::selectRange('page', 1, $p_rows->lastPage(),$p_rows->currentPage(),['class'=>'form-control p-paginate-select paginate-select']); ?>
                @endif
                @if($p_rows->hasMorePages())
                    <a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                @endif
            </div>
        @endif
    </div>
@else
    <div class="col-sm-12 text-center">ไม่พบข้อมูล</div><div class="clearfix"></div>
@endif
