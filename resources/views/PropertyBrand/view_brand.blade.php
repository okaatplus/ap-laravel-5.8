<div class="">
    <div class="user-detail">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8"><div class="form-group">
                    @if(!empty($brand->img_url))
                        <img src="{{ env('URL_S3')."/post-file".$brand->img_url }}" style="width: 250px;height: 250px;" alt="" />
                        @else
                        <img src="" style="width: 250px;height: 250px;" alt="" />
                    @endif
                </div></div>
        </div>
        <div class="row">
            <div class="col-md-4"><div class="form-group"><b>{{ trans('messages.property_brand.name') }} :</b></div></div>
            <div class="col-md-8"><div class="form-group">{{ $brand->name_th }}</div></div>
        </div>
        <div class="row">
            <div class="col-md-4"><div class="form-group"><b>{{ trans('messages.property_brand.name') }} :</b></div></div>
            <div class="col-md-8"><div class="form-group">{{ $brand->name_th }}</div></div>
        </div>
    </div>
    <div style="clear:both"></div>
</div>
