<div class="col-sm-9 mailbox-right">
    {!! Form::model($property,['url'=>'admin/property/about','method'=>'post','class'=>'form-horizontal','id'=>'property-form']) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ trans('messages.AboutProp.property_detail') }}</h3>
        </div>
        <div class="panel-body">
           <div class="row">
                <div class="col-sm-12">
                    <div class="form-group @if($errors->has('property_name_th') || $errors->has('property_name_en')) validate-has-error @endif">
                        <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.property_name') }} (th)</label>
                        <div class="col-sm-4">
                            {!! Form::text('property_name_th',null,array('class'=>'form-control','maxlength' => 200)) !!}
                            <?php echo $errors->first('name_th','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                        <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.property_name') }} (en)</label>
                        <div class="col-sm-4">
                            {!! Form::text('property_name_en',null,array('class'=>'form-control','maxlength' => 200)) !!}
                            <?php echo $errors->first('name_en','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                    </div>

                    <div class="form-group @if($errors->has('juristic_person_name_th') || $errors->has('property_name_en')) validate-has-error @endif">
                        <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.juristic_person_name') }} (th)</label>
                        <div class="col-sm-4">
                            {!! Form::text('juristic_person_name_th',null,array('class'=>'form-control','maxlength' => 200)) !!}
                            <?php echo $errors->first('juristic_person_name_th','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                        <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.juristic_person_name') }} (en)</label>
                        <div class="col-sm-4">
                            {!! Form::text('juristic_person_name_en',null,array('class'=>'form-control','maxlength' => 200)) !!}
                            <?php echo $errors->first('juristic_person_name_en','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="profile-image">{{ trans('messages.AboutProp.logo') }}</label>
                        <div class="col-sm-10">
                            <div class="property-profile-img">
                                @if($property->logo_pic_name)
                                <img src="{{ env('URL_S3')."/property-file/".$property->logo_pic_path.$property->logo_pic_name }}" alt="user-image" class="property-img" />
                                @else
                                <img src="{{url('/')}}/images/user-4.png" alt="user-image" class="property-img" />
                                @endif
                            </div>
                             <span id="upload-profile-pic">
                                <i class="fa fa-camera"></i> {{ trans('messages.change') }}
                            </span>
                            <div id="profile-img-input"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.area') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('area_size',null,array('class'=>'form-control')) !!}
                        </div>
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.unit_amount') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('unit_size',null,array('class'=>'form-control','maxlength' => 10)) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.min_price') }}</label>
                        <div class="col-sm-4">
                            {!! Form::select('min_price',[
                                    1 => '1,000,000 - 3,000,000 '.trans('messages.Report.baht'),
                                    2 => '3,000,001 - 5,000,000 '.trans('messages.Report.baht'),
                                    3 => '5,000,001 - 10,000,000 '.trans('messages.Report.baht'),
                                    4 => '10,000,001+',
                                ],null,array('class'=>'form-control')) !!}
                        </div>
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.max_price') }}</label>
                        <div class="col-sm-4">
                            {!! Form::select('max_price',[
                                1 => '1,000,000 - 3,000,000 '.trans('messages.Report.baht'),
                                2 => '3,000,001 - 5,000,000 '.trans('messages.Report.baht'),
                                3 => '5,000,001 - 10,000,000 '.trans('messages.Report.baht'),
                                4 => '10,000,001+',
                            ],null,array('class'=>'form-control')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.tax_id') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('tax_id',null,array('class'=>'form-control','maxlength' => 13)) !!}
                        </div>
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.property_type') }}</label>
                        <div class="col-sm-4">
                            {!! Form::select('property_type',$property_type,null,array('class'=>'form-control')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.constructor') }}</label>
                        <div class="col-sm-10">
                            {!! Form::text('construction_by',null,array('class'=>'form-control','maxlength' => 100)) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.address_no') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('address_no',null,array('class'=>'form-control')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.road') }} (th)</label>
                        <div class="col-sm-4">
                            {!! Form::text('street_th',null,array('class'=>'form-control')) !!}
                        </div>
                         <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.road') }} (en)</label>
                        <div class="col-sm-4">
                            {!! Form::text('street_en',null,array('class'=>'form-control')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.address') }} (th)</label>
                        <div class="col-sm-4">
                            {!! Form::text('address_th',null,array('class'=>'form-control','maxlength' => 200)) !!}
                        </div>
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.address') }} (en)</label>
                        <div class="col-sm-4">
                            {!! Form::text('address_en',null,array('class'=>'form-control','maxlength' => 200)) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.province') }}</label>
                        <div class="col-sm-4">
                            {!! Form::select('province',$provinces,null,array('class'=>'form-control')) !!}
                        </div>

                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.postcode') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('postcode',null,array('class'=>'form-control','maxlength' => 10)) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.tel') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('tel',null,array('class'=>'form-control')) !!}
                        </div>
                        <label class="col-sm-2 control-label">{{ trans('messages.fax') }}</label>
                        <div class="col-sm-4">
                            {!! Form::text('fax',null,array('class'=>'form-control')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.about_property') }}</label>
                        <div class="col-sm-10">
                            {!! Form::textarea('about',null,['class'=>'form-control','row'=>20,'maxlength' => 5000]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.location') }}</label>
                        <div id="search-geo-block">
                            <input id="address" class="controls" type="text" placeholder="Search Box">
                            <input type="button" class="btn btn-primary" id="search-geo-btn" value="SEARCH" />
                        </div>
                        <div class="col-sm-10"><div id="map" style="height:300px;"></div></div>
                        {!! Form::hidden('lat',null,array('id'=>'latbox')) !!}
                        {!! Form::hidden('lng',null,array('id'=>'lngbox')) !!}
                    </div>

                    <div class="row">
                        @if(Auth::user()->role == 1)
                             <div class="col-sm-12 text-right">
                                {!! Form::button(trans('messages.save'),['class'=>'btn btn-primary','id'=>'save-property']) !!}
                            </div>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
</div>
