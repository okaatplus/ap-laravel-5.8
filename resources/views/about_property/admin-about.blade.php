@extends('layouts.base-admin')
@section('content')
<?php
    $lang = App::getLocale();
    $property_type = unserialize(constant('PROPERTY_TYPE_'.strtoupper($lang)));
?>
    <div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.AboutProp.page_about_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <a href="{{ url('admin/property/about') }}"><i class="fa-info-circle"></i>{{ trans('messages.AboutProp.page_about_head') }}</a>
                </li>
			</ol>
    	</div>
    </div>
     <section class="mailbox-env">
        <div class="row">
            {{--@if(Auth::user()->role == 1)--}}
            {{--@include('about_property.admin-about-body')--}}
            {{--@else--}}
            @include('about_property.user-about-body')
            {{--@endif--}}

           	{{--@include('about_property.admin-settings-menu')--}}
            
        </div>
    </section>
@endsection
@section('script')
<script src='https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}' type='text/javascript'></script>
<script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/number.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript">

    var map,marker,geocoder;
    function initialize() {

        @if(!empty($property['lat']) && !empty($property['lng']))
        var latlng = new google.maps.LatLng({{$property['lat']}}, {{$property['lng']}});
        @else
        var latlng = new google.maps.LatLng(16.492660635148514, 100.86205961249993);
        @endif
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            @if(Auth::user()->role == 1)
            draggable: true
            @endif
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            $("#latbox").val(this.getPosition().lat());
            $("#lngbox").val(this.getPosition().lng());
        });

    }

    google.maps.event.addDomListener(window, 'load', initialize);
    @if(Auth::user()->role == 1)
    function codeAddress() {
      var address = document.getElementById('address').value;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var l = results[0].geometry.location;
            map.setCenter(l);
            marker.setPosition(l);
            $('#latbox').val(l.lat());
            $('#lngbox').val(l.lng());
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

    $(function () {
        var root = $('#root-url').val();
        var i = 0;
        $(".price").number(true);

        $.validator.addMethod("notEqual", function(value, element, param) {
          return this.optional(element) || value != param;
        });

        $("#property-form").validate({
            rules: {
                property_name_th    : 'required',
                property_name_en    : 'required',
                juristic_person_name_th    : 'required',
                juristic_person_name_en    : 'required',
                min_price           : {required:true,number:true},
                max_price           : {required:true,number:true},
                unit_size           : 'required',
                address_th          : 'required',
                address_en          : 'required',
                street_th           : 'required',
                street_en           : 'required',
                province            : 'required',
                postcode            : 'required',
                property_type       : {required:true,notEqual:0},
            },
            errorPlacement: function(error, element) { element.addClass('error'); }
        });

        $('#save-property').on('click',function () {
            if($('#property-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#property-form').submit();
            } else {
                var top_;
                top_ = $('.error').first().offset().top;
                 $('html,body').animate({scrollTop: top_-100}, 1000);
            }
        })

        $('#upload-profile-pic').dropzone({
            uploadMultiple:false,
            url: root+'/upload-profile-pic',
            acceptedFiles: "image/*",
            addedfile: function(file)
            {
                $('#profile-img-input').html('');
                $entry = $('<div>').append(
                            $('<div class="progress progress-striped"><div class="progress-bar progress-bar-warning"></div></div>')
                        );
                file.entry = $entry;
                file.progressBar = $entry.find('.progress-bar');
                $('#profile-img-input').html($entry);
            },
            uploadprogress: function(file, progress, bytesSent)
            {
                file.progressBar.width(progress + '%');
            },

            success: function(file,xhr)
            {

                $('.property-img').attr({'src':root+"/upload_tmp/"+xhr.name});
                $('#profile-img-input').append(
                    $('<input>').attr({'type':'hidden','name':'pic_name','value':xhr.name})
                );
                file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-success').fadeOut(500,function() { $(this).parent().remove()});
            },
            error: function(file)
            {
                if(file.accepted) {
                    file.progressBar.removeClass('progress-bar-warning').addClass('progress-bar-red');
                    this.removeFile(file);
                } else {
                    $('#previews').children(':last').remove();
                }
            }
        });

        $('#search-geo-btn').on('click',function () {
            codeAddress();
        })
    })
    @endif
</script>
@endsection
