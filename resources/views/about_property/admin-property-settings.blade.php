@extends('layouts.base-admin')
@section('content')
    <?php
    $common_area_fee_type = unserialize(constant('COMMON_AREA_FEE_TYPE_'.strtoupper(App::getLocale())));
    ?>
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.AboutProp.page_about_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li>
                    <a href="{{ url('admin/property/about') }}"><i class="fa-info-circle"></i>{{ trans('messages.AboutProp.page_about_head') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.AboutProp.page_setting_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ trans('messages.AboutProp.page_setting_head') }}</h3>
        </div>
        {!! Form::model($settings,['url'=>'admin/property/settings', 'method'=>'post','class'=>'form-horizontal'])!!}
        <div class="panel-body">
            <h4>ข้อความอัติโนมัติ</h4>
            <div class="row form-group">
                <label class="col-sm-4 control-label">
                    ใช้ข้อความอัตโนมัติ
                </label>
                <div class="col-sm-8" style="padding-top: 4px;">
                    ปิดใช้งาน
                    <label class="switch round" style="margin: 0 3px -8px;">
                        {{--<input type="checkbox" name="auto_reply_active" value="1" @if($settings->auto_reply_active) checked @endif/>--}}
                        {!! Form::checkbox('auto_reply_active',null) !!}
                        <div class="slider_ round"></div>
                    </label>
                     เปิดใช้งาน
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-4 control-label">
                    ช่วงเวลาที่ใช้ข้อความอัตโนมัติ
                </label>
                <div class="col-sm-2">
                    <label>
                        {{--<input type="checkbox" name="auto_reply_allday_flag" value="1" @if($settings->auto_reply_allday_flag) checked @endif/> ตลอดวัน--}}
                    {!! Form::checkbox('auto_reply_allday_flag',null,null,array('id' => 'set-all-day')) !!} ตลอดวัน
                    </label>
                </div>
                <?php
                    $s_time = $e_time = null;
                    if( $settings->auto_reply_start_time ) $s_time = date('h:i A', strtotime($settings->auto_reply_start_time));
                    if( $settings->auto_reply_end_time ) $e_time = date('h:i A', strtotime($settings->auto_reply_end_time));
                ?>
                <div class="col-sm-3 set-time" @if($settings->auto_reply_allday_flag) style="display: none;" @endif>
                    {!! Form::text('auto_reply_start_time',$s_time,array('class'=>'form-control timepicker','placeholder' => 'เวลาเริ่มข้อความอัตโนมัติ')) !!}
                </div>
                <div class="col-sm-3 set-time" @if($settings->auto_reply_allday_flag) style="display: none;" @endif>
                    {!! Form::text('auto_reply_end_time',$e_time,array('class'=>'form-control timepicker','placeholder' => 'เวลาสิ้นสุดข้อความอัตโนมัติ')) !!}
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-4 control-label">
                    ข้อมความตอบกลับอัตโนมัติ
                </label>
                <div class="col-sm-8">
                    {!! Form::textarea('auto_reply_msg',null,array('class'=>'form-control')) !!}
                </div>
            </div>
            <hr/>
            <h4>Mobile Application</h4>
            <div class="row form-group">
                <label class="col-sm-4 control-label">
                    ระยะเวลาการ session time out
                </label>
                <div class="col-sm-3">

                    {!! Form::text('user_app_timeout',null,array('class'=>'form-control')) !!}
                </div>
                <label class="col-sm-3 control-label text-left"> hrs. </label>
            </div>
            <hr/>
            <h4>Posts & Parcel</h4>
            <div class="row form-group">
                <label class="col-sm-4 control-label">
                    แจ้งเตือนให้มารับจดหมายหรือพัสดุ ทุกๆ
                </label>
                <div class="col-sm-3">
                    {{--{!! Form::text('day_auto_remind_parcel',null,array('class'=>'form-control')) !!}--}}
                    <div class="input-group spinner" data-step="1">
                        <span class="input-group-btn">
                            <button class="btn btn-gray" data-type="decrement">-</button>
                        </span>
                        {!! Form::text('day_auto_remind_parcel',null,array('class'=>'form-control text-center')) !!}
                        {{--<input type="text" class="form-control text-center" value="1"> --}}
                        <span class="input-group-btn">
                            <button class="btn btn-gray" data-type="increment">+</button>
                        </span>
                    </div>
                </div>
                <label class="col-sm-3 control-label text-left"> วัน </label>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <button type="submit" class="btn btn-primary">{{ trans('messages.save') }}</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
@section('script')
    <script type="text/javascript" src="{{url('/')}}/js/number.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.css"></script>
    <script type="text/javascript" src="{{url('/')}}/js/timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#set-all-day').on('change', function () {
                if($(this).is(':checked')) {
                    $('.set-time').hide();
                } else {
                    $('.set-time').show();
                }
            })
        })
    </script>
@endsection
