<div class="col-sm-12 mailbox-right">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $property->{'property_name_'.$lang} }}</h3>
        </div>
        <div class="panel-body p-detail">

            <div class="row">
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.area') }}</label>
                <div class="data col-sm-3">
                    {{ $property->area_size }}
                </div>
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.unit_amount') }}</label>
                <div class="data col-sm-3">
                    {{ $property->unit_size }}
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.tax_id') }}</label>
                    <div class="data col-sm-3">
                        {{ $property->tax_id }}
                    </div>
                    @if($property->property_type)
                    <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.property_type') }}</label>
                    <div class="data col-sm-3">
                        {{ $property_type[$property->property_type] }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.constructor') }}</label>
                <div class="data col-sm-9">
                    {{ $property->construction_by }}
                </div>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.address') }}</label>
                <div class="data col-sm-9">
                    {{ $property->{'address_'.$lang} }}
                </div>
            </div>

            <div class="row">
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.road') }}</label>
                <div class="data col-sm-3">
                    {{ $property->{'street_'.$lang} }}
                </div>
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.province') }}</label>
                <div class="data col-sm-3">
                    {{ $provinces[$property->province] }}
                </div>
            </div>

            <div class="row">
                <label class="col-sm-3 control-label">{{ trans('messages.AboutProp.postcode') }}</label>
                <div class="data col-sm-3">
                    {{ $property->postcode }}
                </div>
                <label class="col-sm-3 control-label">{{ trans('messages.tel') }}</label>
                <div class="data col-sm-3">
                    {{ $property->tel }}
                </div>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label">{{ trans('messages.fax') }}</label>
                <div class="data col-sm-3">
                    {{ $property->fax }}
                </div>
            </div>

            <div class="row">
                <label class="col-sm-12 control-label">{{ trans('messages.AboutProp.about_property') }}</label>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {!! nl2br(e($property->about)) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12"><div id="map" style="height:300px;margin:20px 0px;"></div></div>
            </div>
        </div>
    </div>
</div>
