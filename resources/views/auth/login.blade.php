<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="SMART" />
    <meta name="author" content="" />

    <title>SMART WORLD</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ url('images/smart-icon.png') }}">
    {{--<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Arimo:400,700,400italic">--}}
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-core.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-forms.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-components.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-skins.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/custom.css">
    <link rel="stylesheet" href="{{ url('/') }}/font/ap/font.css">

    <script src="{{ url('/') }}/js/jquery-1.11.1.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-body login-page">
<div class="login-container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <script type="text/javascript">
                jQuery(document).ready(function($)
                {
                    // Reveal Login form
                    setTimeout(function(){ $(".fade-in-effect").addClass('in'); }, 1);
                    // Set Form focus
                    $("form#login .form-group:has(.form-control):first .form-control").focus();
                });
            </script>

            <!-- Add class "fade-in-effect" for login form effect -->
            <form method="POST" action="{!! route('login') !!}" class="login-form fade-in-effect">
            <div class="login-header" style="margin-bottom: 20px;text-align: center;">
                <img src="{{ url('/images/smart-logo.png') }}"/>
            </div>
            <!-- Errors container -->
            <div class="errors-container">
            </div>
            <div class="form-group" style="margin-bottom: 10px;">
                <input type="text" class="form-control" name="email" id="email" autocomplete="off" placeholder="Email"/>
            </div>

            <div class="form-group">
                <input type="password" class="form-control" name="password" id="passwd" autocomplete="off" placeholder="Password" />
            </div>

            <div class="form-group" style="margin-bottom: 0;">
                <button type="submit" class="btn  btn-block text-left">
                    <i class="fa-lock"></i>
                    Log In
                </button>
            </div>
            <div style="text-align: center;color: #fff; margin-top:10px; font-size: 12px; padding-bottom: 10px;">
                <div class="pull-left">
                    <div class="checkbox text-right">
                        <input class="inp-cbx" id="cbx" type="checkbox" name="remember" style="display: none;"/>
                        <label class="cbx" for="cbx">
                                <span>
                                    <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                    </svg>
                                </span>
                            <span style="color: #000;">{{ trans('messages.remember_me') }}</span>
                        </label>
                    </div>
                </div>
                <a href="{{ url('password/reset') }}" class="pull-right lost-pwd">({{ trans('messages.forgot_pass') }})</a>
            </div>
            <!--<div class="login-footer">
                <a href="#">Forgot your password?</a>
            </div> -->
            </form>

        </div>

    </div>

</div>
</body>
</html>