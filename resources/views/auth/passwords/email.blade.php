@extends('layouts.welcome')
@section('content')
<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <form method="POST" action="{{ url('reset/password/email') }}" class="login-form">
        @csrf
        <div class="login-header" style="margin-bottom: 20px; width: 100%; float: left;text-align: center;">
            <a href="{{ url('/') }}" class="logo" style="display: inline-block">
                <img style="max-width: 100%;" src="{{ url('/') }}/images/smart-logo.png"/>
            </a>
        </div>
        <div style="clear: both;"></div>
            @if( session('status') )
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            <div style="clear: both;"></div>
            @endif

           <div class="form-group @if($errors->has('email')) validate-has-error @endif">
                <h3>{{ trans('messages.Password.page_head') }}</h3>
                <div style="margin-bottom: 15px;">{{ trans('messages.Verify.reg_email') }}</div>
                <div>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    <?php echo $errors->first('email','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-block" type="submit">{{ trans('messages.Password.reset_btn') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection
