@extends('layouts.welcome')
@section('content')
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="login-form">
                <div class="login-header" style="margin-bottom: 20px; width: 100%; float: left;text-align: center;">
                    <a href="{{ url('/') }}" class="logo" style="display: inline-block">
                        <img style="max-width: 100%;" src="{{ url('/') }}/images/smart-logo.png"/>
                    </a>
                </div>
                <div style="clear: both;"></div>
                @if( session('status') )
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    <div style="clear: both;"></div>
                @endif
            </div>
        </div>
    </div>
@endsection
