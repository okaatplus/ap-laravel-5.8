@extends('layouts.welcome')
@section('content')
    <form method="POST" action="{{ route('password.update') }}" class="login-form">
        @csrf
        <div class="login-header" style="margin-bottom: 20px; width: 100%; float: left;text-align: center;">
            <a href="{{ url('/') }}" class="logo" style="display: inline-block">
                <img style="max-width: 100%;" src="{{ url('/') }}/images/smart-logo.png"/>
            </a>
        </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <h3 class="bg-hex-text">{{ trans('messages.Password.reset_page_head') }}</h3>
                            <h5 class="bg-hex-text">{{ trans('messages.Password.page_sub_head') }}</h5>
                        </div>
                    </div>
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="row form-group @if($errors->has('email')) validate-has-error @endif">
                        <div class="col-sm-5" style="padding-top: 7px;">{{ trans('messages.Verify.reg_email') }}</div>
                        <div class="col-sm-7">
                            {!! Form::text('email',old('email'),['class'=>'form-control','type'=>'email']) !!}
                            <?php echo $errors->first('email','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                    </div>
                    <div class="row form-group @if($errors->has('password')) validate-has-error @endif">
                        <div class="col-sm-5" style="padding-top: 7px;">{{ trans('messages.Verify.password') }}</div>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" name="password">
                            <?php echo $errors->first('password','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                    </div>

                    <div class="row form-group @if($errors->has('password_confirmation')) validate-has-error @endif">
                        <div class="col-sm-5" style="padding-top: 7px;">{{ trans('messages.Verify.confirm_password') }}</div>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" name="password_confirmation">
                            <?php echo $errors->first('password_confirmation','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block">
                            {{ trans('messages.Verify.set_password_btn') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
