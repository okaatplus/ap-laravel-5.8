{!! Form::model($member,array('url'=>'admin/property/members/add','method'=>'post','class'=>'form-horizontal','id' => 'add-member-form')) !!}
<div class="modal-body">
    <div class="form-group @if($errors->has('name')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.name') }}</label>
        <div class="col-sm-8">
            {!! Form::text('name',null,array('class' => 'form-control')) !!}
            <?php echo $errors->first('name','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.emergency_cal') }}</label>
        <div class="col-sm-8">
            {!! Form::text('phone',null,array('class' => 'form-control')) !!}
        </div>
    </div>
    <!--<div class="form-group @if($errors->has('user_type')) validate-has-error @endif">
            <label class="col-sm-4 control-label">{{ trans('messages.Member.member_type') }} </label>
        <div class="col-md-8">
            {!! Form::select('user_type',[
                    0 => trans('messages.Member.member_type'),
                    1 => trans('messages.Member.member_crew'),
                    2 => trans('messages.Member.member_tenant')
                ],null,array('class' => 'form-control')) 
            !!}
            <?php echo $errors->first('user_type','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
    <div class="form-group @if($errors->has('property_unit_id')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.unit_no') }}</label>
        <div class="col-md-8">
            {!! Form::select('property_unit_id',$unit_list,null,array('class' => 'form-control', 'id' => 'add-member-p-unit')) !!}
            <?php echo $errors->first('property_unit_id','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>-->
    <div class="form-group @if($errors->has('email')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.AdminUser.username') }}</label>
        <div class="col-sm-8">
            {!! Form::text('email',null,array('class' => 'form-control', "data-validate" => "email")) !!}
            <?php echo $errors->first('email','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
    <div class="form-group @if($errors->has('password')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.Verify.password') }}</label>
        <div class="col-sm-8">
            <div class="input-group">
            {!! Form::password('password',array('class' => 'form-control', 'rel' => 'gp',
            'data-character-set' => 'a-z,A-Z,0-9',
            'data-input-confirm' => 'confirm-add-member-pass',
            'autocomplete' => 'new-password'
            )) !!}
                <div class="input-group-btn">
                    <button type="button" class="btn btn-info getNewPass" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.generate_password') }}">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </div>
            <?php echo $errors->first('password','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
    <div class="form-group @if($errors->has('password_confirm')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.Verify.confirm_password') }}</label>
        <div class="col-sm-8">
            {!! Form::password('password_confirm',array('class' => 'form-control','id' => 'confirm-add-member-pass')) !!}
            <?php echo $errors->first('password_confirm','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
</div>
{!! Form::close() !!}
<script>
    $(function () {
        $("select[name='user_type']").selectBoxIt().on('open', function(){
            $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
        });

        $('#add-member-p-unit').select2();
    })
</script>
