@if($members->count())
<?php
	$from   = (($members->currentPage()-1)*$members->perPage())+1;
    $to     = (($members->currentPage()-1)*$members->perPage())+$members->perPage();
    $to     = ($to > $members->total()) ? $members->total() : $to;
    $allpage = $members->lastPage();
    $curPage = $members->currentPage();
 ?>
 <div class="row">
	 <div class="col-sm-6" style="margin-bottom: 10px;"><p> {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$members->total()]) !!}</p></div>
 	@if($allpage > 1)
	<div class="col-md-6 text-right">
		@if($curPage > 1)
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($allpage  > 1)
		<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
		@endif
		@if($members->hasMorePages())
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
		@endif
	</div>
 	@endif
 </div>
<table class="table table-striped">
	<thead>
	<tr>
		<th width="180px">{{ trans('messages.unit_no') }}</th>
		<th width="*">Name</th>
		<th width="140px" class="text-center">Type</th>
		<th class="text-center" width="200px">ID Card</th>
		<th width="150px">Joined</th>
		<th width="150px">Action</th>
	</tr>
	</thead>
	<tbody>
	@foreach($members as $userProperty)
		<tr>
			<td class="text-center">
				@if( $userProperty->OfUser->property_unit )
				{{ $userProperty->OfUser->property_unit->unit_number }}
				@else
					Ordinary user
				@endif
			</td>
			<td class="user-image">
				@if($userProperty->OfUser->profile_pic_name)
					<img style="margin-top:0;" src="{{ env('URL_S3')."/profile-img/".$userProperty->OfUser->profile_pic_path.$userProperty->OfUser->profile_pic_name }}" class="img-circle" alt="user-pic" />
				@else
					<img style="margin-top:0;" src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
				@endif
				<div class="user-detail" style="margin-top:7px;">
					<span style="display: inline-block;margin-top: 5px;" class="name">{{ $userProperty->OfUser->name }}</span>
				</div>
			</td>
			<td class="text-center">
				@if( $userProperty->user_type === null )
					Ordinary user
				@else
					@switch( $userProperty->user_type )
						@case(0) Owner @break
						@case(1) Co-resident @break
						@default Tenant @break
					@endswitch
				@endif
			</td>
			<td class="text-center">
				@if( $userProperty->ofUser->id_card )
                	{{ $userProperty->ofUser->id_card }}
				@else
					-
				@endif
			</td>
			<td>{{ localDate($userProperty->created_at) }}</td>
			<td class="action-links">
				<a href="#" class="btn btn-info view-member" data-uid="{{ $userProperty->id }}" >
					<i class="fa-eye"></i>
				</a>
				<a href="#" class="btn btn-warning edit-member" data-uid="{{ $userProperty->id }}" >
					<i class="fa-pencil"></i>
				</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

<div class="clearfix"></div>
<div class="row">
	<div class="col-sm-6"><p> {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$members->total()]) !!}</p></div>
 	@if($allpage > 1)
	<div class="col-md-6 text-right">
		@if($curPage > 1)
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($allpage  > 1)
		<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
		@endif
		@if($members->hasMorePages())
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
		@endif
	</div>
 	@endif
 </div>
 
@else
<div class="col-sm-12 text-center">{{ trans('messages.Member.member_not_found') }}</div><div class="clearfix"></div>
@endif
