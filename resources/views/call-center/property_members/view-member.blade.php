<div class="user-data-block">
	@if($member->profile_pic_name)
    <img src="{{ env('URL_S3')."/profile-img/".$member->profile_pic_path.$member->profile_pic_name }}" class="img-circle" alt="user-pic" />
    @else
    <img src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
    @endif
    <div class="user-detail">
			<div class="row form-group" style="margin-bottom: 10px;">
    			<div class="col-md-4"><b>{{ trans('messages.name') }} :</b></div>
    			<div class="col-md-8">{{ $member->name }}</div>
			</div>
			<div class="row form-group" style="margin-bottom: 10px;">
    			<div class="col-md-4"><b>{{ trans('messages.email') }} :</b></div>
    			<div class="col-md-8">{{ $member->email }}</div>
			</div>
			<div class="row form-group">
    			<div class="col-md-4"><b>{{ trans('messages.regis_date') }} :</b></div>
    			<div class="col-md-8"><?php echo localDate($member->created_at); ?></div>
			</div>
	</div>
	<div style="clear:both"></div>
</div>
