@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Member.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@include('call-center.property_units.tab-menu')
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body member-list-content">
				<form method="POST" id="search-member-form" action="{{ url('admin/property/members/export') }}" accept-charset="UTF-8" class="form-horizontal">
					<div class="row">
						<div class="col-sm-3 block-input">
							<input class="form-control datepicker" size="25" placeholder="{{ trans('messages.name') }}" name="name">
						</div>

						<div class="col-sm-3 block-input">
							{!! Form::select('unit_id', $unit_list,null,['class'=>'form-control select2', 'id' => 'unit-id']) !!}
						</div>
						<div class="col-sm-3 block-input">
							<select class="form-control" name="active">
								<option value="-">{{ trans('messages.Member.member_account_status') }}</option>
								<option value="1">{{ trans('messages.Member.account_active') }}</option>
								<option value="0">{{ trans('messages.Member.account_inactive') }}</option>
							</select>
						</div>
						<div class="col-sm-3 text-right">
							<button type="reset" id="reset-search" class="btn btn-single btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
							<button type="button" class="btn btn-secondary btn-single search-member">{{ trans('messages.search') }}</button>
						</div>
					</div>
				</form>
				<hr/>
				<div class="tab-pane active" id="member-list">
					<div id="member-list-content">
						@include('call-center.property_members.member-list')
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

    <div class="modal fade" id="modal-active" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.confirm_ban_head') }}</h4>
                </div>
                <div class="modal-body">
                	{!! trans('messages.Member.confirm_ban_msg') !!}
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-inactive" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.confirm_active_head') }}</h4>
                </div>
                <div class="modal-body">
                	{!! trans('messages.Member.confirm_active_msg') !!}
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-member">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.member_detail') }}</h4>
                </div>
                <div class="modal-body">

                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                </div>
            </div>
        </div>
    </div>

	<div class="modal fade" id="modal-edit-member">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{ trans('messages.Member.member_edit') }}</h4>
				</div>
				{!! Form::open(array('url' => 'admin/property/members/edit','id' => 'edit-user-form')) !!}
				<div class="modal-body">

				</div>
				{!! Form::close() !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
					<button type="button" class="btn btn-primary" id="save-edit">{{ trans('messages.save') }}</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="add-member-modal" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">{{ trans('messages.Member.add_member') }}</h4>
					</div>
					<div class="modal-body" id="add-member-form-block">
						@include('call-center.property_members.member-form')
					</div>
					 <div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
						<button type="submit" class="btn btn-primary" id="add-member-btn">{{ trans('messages.confirm') }}</button>
					</div>
				</div>
			</div>
		</div>

	<div class="modal fade" id="password-modal" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body text-center" id="add-member-form-block">
					{{ trans('messages.AdminUser.generate_password_msg_1') }}
					<div class="display-password-block">
						<span id="generated-password"></span>
						<i class="copy-clipboard fa-copy" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.copy_password') }}"></i>
					</div>
					{{ trans('messages.AdminUser.generate_password_msg_2') }}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('messages.ok') }}</button>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('script')
<script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/random-password.js"></script>
<script type="text/javascript">
	$(function (){
		var tmp;

		$("#unit-id").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });

		$('.member-list-content').on('click','.paginate-link', function (e) {
			e.preventDefault();
			searchMember ($(this).data('page'));
		});

		$('.member-list-content').on('change','.paginate-select', function (e) {
			e.preventDefault();
			searchMember ($(this).val());
		});

		$('.search-member').on('click', function () {
			searchMember (1)
		});

		$('#search-member-form').on('keydown', function(e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				searchMember (1);
			}
		});

		$('#reset-search').on('click',function () {
			$(this).closest('form').find("input").val("");
			$(this).closest('form').find("select option:selected").removeAttr('selected');
			$('#unit-id').val('').trigger('change');
			searchMember (1);
		});


		$('.member-list-content').on('click','.view-member', function (e){
			e.preventDefault();
			var _this = $(this);
			_this.html('<i class="fa-spin fa-spinner"></i>');
			var uid = _this.attr('data-uid');
			$.ajax({
				url     : $('#root-url').val()+"/call-center/admin/property/members/get",
                method	: "POST",
                data 	: ({uid:uid}),
                dataType: "html",
                success: function (t) {
                	$('#modal-member .modal-body').html(t);
                	$('#modal-member').modal('show');
                	_this.html('<i class="fa-eye"></i>');
                }
			});
		});

        $('.member-list-content').on('click','.edit-member', function (e){
            e.preventDefault();
            var _this = $(this);
            _this.html('<i class="fa-spin fa-spinner"></i>');
            var uid = _this.attr('data-uid');
            $.ajax({
                url     : $('#root-url').val()+"/call-center/admin/property/members/edit",
                method	: "POST",
                data 	: ({uid:uid}),
                dataType: "html",
                success: function (t) {
                    $('#modal-edit-member .modal-body').html(t);
                    $('#modal-edit-member').modal('show');
                    $("#modal-edit-member select").selectBoxIt().on('open', function(){
                        $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
                    });
                    _this.html('<i class="fa-pencil"></i>');
                }
            });
        });

        $('#save-edit').on('click', function () {
            if($('#user-type').val() == '-') {
                $('#user-typeSelectBoxItContainer').addClass('error');
			} else {
                $(this).prepend('<i class="fa-spin fa-spinner"></i>').prop('disabled',true);
                $('#user-typeSelectBoxItContainer').removeClass('error');
                $('#edit-user-form').submit();
			}
		})
	});

	function searchMember (page) {
		var data = $('#search-member-form').serialize();
		data+= "&page="+page;
        $('#member-list-content').css('opacity','0.6');
		$.ajax({
			url     : $('#root-url').val()+"/call-center/admin/property/members/page",
            method	: "POST",
            data 	: data,
            dataType: "html",
            success: function (t) {
            	$('#member-list-content').html(t);
                $('#member-list-content').css('opacity','1');

				$('[data-toggle="tooltip"]').each(function(i, el)
				{
					var $this = $(el),
						placement = attrDefault($this, 'placement', 'top'),
						trigger = attrDefault($this, 'trigger', 'hover'),
						tooltip_class = $this.get(0).className.match(/(tooltip-[a-z0-9]+)/i);

					$this.tooltip({
						placement: placement,
						trigger: trigger
					});

					if(tooltip_class)
					{
						$this.removeClass(tooltip_class[1]);

						$this.on('show.bs.tooltip', function(ev)
						{
							setTimeout(function()
							{
								var $tooltip = $this.next();
								$tooltip.addClass(tooltip_class[1]);

							}, 0);
						});
					}
				});
            }
		});
	}
</script>
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
@endsection
