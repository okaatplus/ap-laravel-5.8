<?php $cate 	= unserialize(constant('KEY_CARD_STATUS_'.strtoupper(App::getLocale()))); ?>
<div class="col-sm-6">
    <div class="form-group">
        <label class="col-sm-4 control-label" for="number">{{ trans('messages.unit_no') }}</label>
        <div class="col-sm-3">
            {!! Form::hidden('id',$unit->id) !!}
            {!! Form::text('unit_number',$unit->unit_number,array('class'=>'form-control', 'maxlength' => 100 )) !!}
        </div>
        <label class="col-sm-2 control-label" for="number">{{ trans('messages.Prop_unit.building') }}</label>
        <div class="col-sm-3">
            {!! Form::hidden('id',$unit->id) !!}
            {!! Form::text('building',$unit->building,array('class'=>'form-control', 'maxlength' => 100 )) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" for="number">{{ trans('messages.Prop_unit.unit_floor') }}</label>
        <div class="col-sm-3">
            {!! Form::text('unit_floor',$unit->unit_floor,array('class'=>'form-control', 'maxlength' => 100 )) !!}
        </div>
        <label class="col-sm-2 control-label" for="number">{{ trans('messages.Prop_unit.unit_soi') }}</label>
        <div class="col-sm-3">
            {!! Form::text('unit_soi',$unit->unit_soi,array('class'=>'form-control', 'maxlength' => 100 )) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" for="number">Asset Project ID</label>
        <div class="col-sm-3">
        @if( !$unit->asset_project_id )
                {!! Form::text('asset_project_id',$unit->asset_project_id,array('class'=>'form-control', 'maxlength' => 100 )) !!}
        @else
            <div style="padding-top: 8px;">{{ $unit->asset_project_id }}</div>
        @endif
        </div>
        <label class="col-sm-2 control-label" for="number">Asset ID</label>
        <div class="col-sm-3">
            @if( !$unit->asset_project_id )
            {!! Form::text('asset_id',$unit->asset_id,array('class'=>'form-control', 'maxlength' => 100 )) !!}
            @else
                <div style="padding-top: 8px;">{{ $unit->asset_id }}</div>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">Land Number</label>
        <div class="col-sm-8">
            @if( !$unit->land_no )
            {!! Form::text('land_no',$unit->land_no,array('class'=>'form-control','maxlength'=>100)) !!}
            @else
                <div style="padding-top: 8px;">{{ $unit->land_no }}</div>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.owner_name_th') }}</label>
        <div class="col-sm-8">
            {!! Form::text('owner_name_th',$unit->owner_name_th,array('class'=>'form-control','maxlength'=>100)) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.owner_name_en') }}</label>
        <div class="col-sm-8">
            {!! Form::text('owner_name_en',$unit->owner_name_en,array('class'=>'form-control','maxlength'=>100)) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.type') }}</label>
        <div class="col-sm-8">
           {!! Form::select('type',[1 => trans('messages.Prop_unit.property'),2 => trans('messages.Prop_unit.land'),3 => trans('messages.Prop_unit.none'),4 => trans('messages.Prop_unit.store')],$unit->type,array('class'=>'form-control')) !!}
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.unit_area') }}</label>
        <div class="col-sm-8">
            {!! Form::text('property_size',$unit->property_size,array('class'=>'form-control','maxlength'=>10,'id'=>'unit-area')) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.member_amont') }}</label>
        <div class="col-sm-8">
            {!! Form::selectRange('resident_count',1,10,$unit->resident_count,array('class'=>'form-control', 'maxlength' => 2)) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.emergency_cal') }}</label>
        <div class="col-sm-8">
            {!! Form::text('phone',$unit->phone,array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.email') }}</label>
        <div class="col-sm-8">
            {!! Form::text('email',$unit->email,array('class'=>'form-control','maxlength'=>50)) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.transferred_date') }}</label>
        <div class="col-sm-8">
            <div class="input-group">
                {!! Form::text('transferred_date', ($unit->transferred_date != null) ? date('Y/m/d',strtotime($unit->transferred_date)) : null, array('class'=>'form-control datepicker edit_picker_transferred_date','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','data-language'=>App::getLocale())) !!}
                <div class="input-group-addon">
                    <a href="#"><i class="linecons-calendar"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.insurance_expire_date') }}</label>
        <div class="col-sm-8">
            <div class="input-group">
                {!! Form::text('insurance_expire_date', ($unit->insurance_expire_date != null) ? date('Y/m/d',strtotime($unit->insurance_expire_date)) : null,array('class'=>'form-control datepicker edit_picker_insurance_expire_date','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','data-language'=>App::getLocale())) !!}
                <div class="input-group-addon">
                    <a href="#"><i class="linecons-calendar"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.delivery_address') }}</label>
        <div class="col-sm-8">
            {!! Form::textarea('delivery_address',$unit->delivery_address,array('class'=>'form-control','rows'=>3)) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" for="urgent-phone">{{ trans('messages.Prop_unit.contact_lang') }}</label>
        <div class="col-sm-8">
            {!! Form::select('contact_lang',['th' => 'ไทย','en' => 'English'], $unit->contact_lang ,array('class'=>'form-control')) !!}
        </div>
    </div>
</div>