<ul class="nav nav-tabs">
    <li @if(isset($active_menu) && $active_menu == "units") class="active" @endif>
        <a href="{{ url('call-center/admin/property/units') }}">
            <span>{{ trans('messages.Prop_unit.page_head') }}</span>
        </a>
    </li>
    <li @if(Request::is('call-center/admin/property/members')) class="active" @endif>
        <a href="{{ url('call-center/admin/property/members') }}">
            <span>{{ trans('messages.Member.page_head') }}</span>
        </a>
    </li>
</ul>