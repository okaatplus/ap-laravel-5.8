@if($units->count())
<?php
    $allpage = $units->lastPage();
    $curPage = $units->currentPage();
    $lang   = App::getLocale();
?>

 <div class="row" style="margin-bottom: 10px;">
 	@if($allpage > 1)
    <div class="col-sm-6"><p> {{ trans('messages.Prop_unit.unit_total',['no'=>$units->total()]) }}</p></div>
	<div class="col-md-6 text-right">
			@if($curPage > 1)
				<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
			@endif
			@if($allpage  > 1)
			<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
			@endif
			@if($units->hasMorePages())
				<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
			@endif
	</div>
 	@endif
 </div>

 @foreach($units as $unit)
 <div class="invoice-row">
    <div class="invoice-head" style="padding-bottom: 5px; margin-bottom: 3px;">
		<div class="col-sm-12 no-padding">
			<b>{{ trans('messages.unit_no') }}  : {{ $unit->unit_number }}
				@if($unit->unit_floor) {{ trans('messages.Prop_unit.unit_floor') }}  : {{ $unit->unit_floor." " }} @endif
				@if($unit->building) {{ trans('messages.Prop_unit.building') }}  : {{ $unit->building }} @endif
			</b>
		</div>
    </div>

    <div class="col-sm-12">
		<span class="text-bold">{{ trans('messages.Prop_unit.owner_name') }}</span> : @if($unit->{'owner_name_'.$lang}) {{ $unit->{'owner_name_'.$lang} }} @else - @endif
    </div>

    <div class="col-sm-3">
        <span class="text-bold">{{ trans('messages.Prop_unit.type') }}</span> :
        @if($unit->type == 0)
		{{ trans('messages.Prop_unit.property') }}
		@elseif($unit->type == 1)
		{{ trans('messages.Prop_unit.none') }}
        @else
		{{ trans('messages.Prop_unit.hold_by_developer') }}
        @endif
    </div>
    <div class="col-sm-3">
		<span class="text-bold">{{ trans('messages.Prop_unit.unit_area') }}</span> : @if($unit->property_size) {{ $unit->property_size }} @else - @endif
    </div>
	 <div class="col-sm-3">
		 <span class="text-bold">{{ trans('messages.Prop_unit.transferred_date') }}</span> :
		 {{($unit->transferred_date != null) ? localDate($unit->transferred_date) : '-' }}
	 </div>
	 <div class="col-sm-3">
		 <span class="text-bold">{{ trans('messages.Prop_unit.insurance_expire_date') }}</span> :
		 {{($unit->insurance_expire_date != null) ? localDate($unit->insurance_expire_date) : '-' }}
	 </div>
	 <div class="col-sm-3">
		 <span class="text-bold">{{ trans('messages.Prop_unit.emergency_cal') }}</span> :
		 @if($unit->phone) {{$unit->phone}}  @else - @endif
	 </div>
	 <div class="clearfix"></div>
	 <div class="col-sm-9">
		 <span class="text-bold">{{ trans('messages.Prop_unit.delivery_address') }}</span> : @if(!$unit->delivery_address) - @endif
		@if($unit->delivery_address)
			{{ $unit->delivery_address }}
		@endif
	 </div>

	 {{--<div class="clearfix"></div>--}}
	 <div class="col-md-3 action-links text-right">
		 <a data-unit="{{ $unit->id }}" class="btn btn-blue unit-view-button" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.view') }}" style="margin-right: 7px;">
			 <i class="fa-eye"></i>
		 </a>
	 </div>
</div>
@endforeach
<div class="clearfix"></div>
<div class="row">
	@if($allpage > 1)
	<div class="col-md-12 text-right">
		@if($curPage > 1)
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($allpage  > 1)
		<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
		@endif
		@if($units->hasMorePages())
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
		@endif
	@endif
	</div>
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Prop_unit.unit_not_found') }}</div><div class="clearfix"></div>
@endif
