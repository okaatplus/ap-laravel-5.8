<?php
$lang = App::getLocale();
?>
<section class="bills-env signature-row" style="font-size: 20px;">
    <div class="panel panel-default">
        <div class="panel-body">
        	<div class="row">
        		<div class="col-sm-12 text-center">
        			<h2>{{ trans('messages.Complain.page_head') }}</h2>
        		</div>
        	</div>

            <section class="invoice-env">
                <!-- Invoice header -->
                <div>
                     <!-- Invoice Options Buttons -->
                    <div class="invoice-logo" style='width: 60%;float: left;'>
                        <table  style="font-size: 16px !important;">
                            <tr>
                                @if($property->logo_pic_name)
                                <td style="vertical-align:top;">
                                <a href="#" class="logo">
                                    <img src="{{ env('URL_S3')."/property-file/".$property->logo_pic_path.$property->logo_pic_name }}" alt="property-image"/>
                                </a>
                                </td>
                                <td style="vertical-align:top;padding-left:20px;">
                                @else
                                <td style="vertical-align:top;">
                                @endif
                                    <ul class="list-unstyled">
                                        <li><strong>{{ $property->{'juristic_person_name_'.$lang} }}</strong></li>
                                        <li>{{ trans('messages.AboutProp.address_no')." ".$property->address_no}}
                                            @if($property->{'street_'.$lang } != "-")
                                                {{ $property->{'street_'.$lang } }}
                                            @endif
                                            {{$property->{'address_'.$lang} }}
                                        </li>
                                        @if( $property->has_province)
                                        <li>{{
                                                $property->has_province->{'name_'.$lang }." ".
                                                $property->postcode
                                            }}
                                        </li>
                                        @endif
                                        <li>
                                            @if($property->tel != "-")
                                                Tel: {{ $property->tel }}
                                            @endif
                                    
                                            @if($property->fax != "-")
                                                &nbsp;Fax: {{ $property->fax }}
                                            @endif
                                        </li>
                                        <li>
                                            @if($property->tax_id != "-")
                                                {{ trans('messages.AboutProp.tax_id')." : ".$property->tax_id}}
                                            @endif
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="invoice-detail">
                        <table width="100%" style="text-align:right;">
                            <tr>
                                <td>
                                    <b>{{ trans('messages.Complain.complain_detail') }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="list-unstyled">
                                        <li class="upper">{{ trans('messages.Complain.complain_detail') }} :
                                            <strong>
                                                {{$complain->owner->name}}
                                            </strong>
                                        </li>
                                        @if(!$complain->is_juristic_complain)
                                        <li class="upper">{{ trans('messages.unit_no') }} : <strong>{{ $complain->property_unit->unit_number }}</strong></li>
                                        @endif
                                        <li class="upper">{{ trans('messages.Complain.issue_type') }} : <strong>{{ $complain->category->{'name_'.App::getLocale()} }}</strong></li>
                                        <li class="upper">{{ trans('messages.Complain.report_date') }} : <strong>{{ localDate($complain->created_at) }}</strong></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
			</section>
            <hr>
            <section class="invoice-env">
                <ul class="list-unstyled">
                    <li class="upper">{{ trans('messages.Complain.is_appointment_admin') }} :
                        <strong>
                            @if($complain->is_appointment)
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-square-o" aria-hidden="true"></i>
                            @endif
                            &nbsp;{{ trans('messages.Complain.is_appointment_yes') }}
                        </strong>&nbsp;&nbsp;
                        <strong>
                            @if(!$complain->is_appointment)
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-square-o" aria-hidden="true"></i>
                            @endif
                            &nbsp;{{ trans('messages.Complain.is_appointment_no') }}
                        </strong>
                    </li>
                    <li class="upper">{{ trans('messages.Complain.is_deposit_key') }} :
                        <strong>
                            @if($complain->is_deposit_key)
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-square-o" aria-hidden="true"></i>
                            @endif
                            &nbsp;{{ trans('messages.Complain.is_deposit_yes') }}
                        </strong>&nbsp;&nbsp;
                        <strong>
                            @if(!$complain->is_deposit_key)
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-square-o" aria-hidden="true"></i>
                            @endif
                            &nbsp;{{ trans('messages.Complain.is_deposit_no') }}
                        </strong>
                    </li>
                    @if($complain->appointment_date != null)
                        <li class="upper">{{ trans('messages.Complain.appointment_date') }} : <strong>{{ localDate($complain->appointment_date) }}</strong> เวลา : <strong>{{ date('h:i A',strtotime($complain->appointment_date)) }}</strong></li>
                    @else
                        <li class="upper">{{ trans('messages.Complain.appointment_date') }} : <strong>-</strong></li>
                    @endif

                    <li class="upper">{{ trans('messages.Complain.technician_name') }} :
                        @if($complain->technician_name != null)
                            <strong>{{$complain->technician_name}}</strong>
                        @else
                            <span>..........................................</span>
                        @endif
                    </li>
                </ul>
            </section>
            <hr>
            <section class="invoice-env" style="min-height: 600px;">
			<div class="row">
                <div class="col-sm-12">
                    <b>{{ trans('messages.Complain.issue') }} : </b> {{ e($complain->title) }}
                </div>
        	</div>

        	<div class="row">
                <div class="col-sm-12"><b>{{ trans('messages.Complain.complain_detail') }} : </b></div>
        		<div class="col-sm-12">
        			<?php echo nl2br(e($complain->detail)) ?>
        		</div>
        	</div>

            @if($complain->user_appointment_note != null)
                <br>
                <div class="row">
                <div class="col-sm-12"><b>{{ trans('messages.Complain.appointment_more_detail') }} : </b></div>
                <div class="col-sm-12">
                    <?php echo nl2br(e($complain->user_appointment_note)) ?>
                </div>
            </div>
            @endif
            </section>

            @if(!$complain->complainFile->isEmpty())
                <?php $count_img = 0; ?>
                @foreach($complain->complainFile as $file)
                    @if($file->is_image)
                        <?php $count_img++; ?>
                    @endif
                @endforeach
                @if($count_img)
                    <div style="page-break-after: always;"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>
                                {{ trans('messages.attachment') }}
                            </h3>
                        </div>
                        @foreach($complain->complainFile as $file)
                            @if($file->is_image)
                                <div class="col-sm-12">
                                    <img style="object-fit: contain" src="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="img-thumbnail" />
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div style="clear:both;"></div>
                @endif
            @endif

            {{--@if(!$complain->complainFile->isEmpty())
                <div class="mail-single-attachments mail-single-body">
                    <h3>
                        <i class="linecons-attach"></i>
                        {{ trans('messages.attachment') }}
                    </h3>
                    <ul class="list-unstyled list-inline">

                        @foreach($complain->complainFile as $file)
                            <li>
                                @if($file->is_image)
                                    <a href="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="thumb gallery" rel="gal-1">
                                        <img width="145px" src="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="img-thumbnail" />
                                    </a>
                                @else
                                    <div>
                                        <a target="_blank" href="{{ url("/maintainance/get-attachment/".$file->id) }}" class="thumb">
                                            <img src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                                        </a>
                                        <div class="file-name">{{ $file->original_name }}</div>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif--}}

            <div style="page-break-after: always;"></div>
            <div class="row">
                <div class="col-sm-12"><h4>{{ trans('messages.Complain.result_fixed') }} : </h4></div>
                <div class="col-sm-12">
                    <p>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
                    </p>
                </div>
            </div>
            <br><br>
            <table width="100%" style="text-align:center;font-size: 14px; !important;">
                <tr>
                    <td width="33%" style="vertical-align: top;">
                        <ul class="list-unstyled">
                            <li class="upper">
                                {{ trans('messages.Complain.signature') }} ..................................
                            </li>
                            <li class="upper">
                                (................................)
                            </li>
                            <li class="upper">
                                {{ trans('messages.Complain.sign_technician') }}<br/><br/>
                            </li>
                            <li class="upper">{{ trans('messages.Complain.sign_date') }} ..................................</li>
                        </ul>
                    </td>
                    <td width="33%" style="vertical-align: top;">
                        <ul class="list-unstyled">
                            <li class="upper">
                                {{ trans('messages.Complain.signature') }} ..................................
                            </li>
                            <li class="upper">
                                (................................)
                            </li>
                            <li class="upper" style="font-size: 14px;">
                                {{ trans('messages.Complain.sign_juristic_technician') }}
                            </li>
                            <li class="upper">{{ trans('messages.Complain.sign_date') }} ..................................</li>
                        </ul>
                    </td>
                    <td width="33%" style="vertical-align: top;">
                        <ul class="list-unstyled">
                            <li class="upper">
                                {{ trans('messages.Complain.signature') }} ..................................
                            </li>
                            <li class="upper">
                                (................................)
                            </li>
                            <li class="upper" style="font-size: 14px;">
                                {{ trans('messages.Complain.sign_juristic_manager') }}
                            </li>
                            <li class="upper">{{ trans('messages.Complain.sign_date') }} ..................................</li>
                        </ul>
                    </td>
                </tr>
            </table>

        </div>
    </div>
</section>
<style type="text/css">
    .upper {
        padding-top: 5px;
    }
</style>