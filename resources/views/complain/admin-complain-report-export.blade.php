@extends('layouts.blank')
@section('content')
    <tr>
        <td colspan="5"><h2>{{ $head }}</h2></td>
    </tr>
    @include('complain.admin-complain-timeline-report-body')

    <style>
    td .label-danger   {color:#ff0000;}
    td .label-warning  {color:#ffba00;}
    td .label-blue     {color:#0e62c7;}
    td .label-secondary {color:#68b828;}
    td .label-default   {color:#777;}
    </style>
@endsection
