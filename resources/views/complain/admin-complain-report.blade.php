@extends('layouts.base-admin')
@section('content')

<div class="page-title">
    <div class="title-env">
        <h1 class="title">
        {{ trans('messages.Complain.report_head') }}
        </h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1" >
            <li>
                <a href="{{url('/')}}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
            </li>
            <li>
                <a href="#">{{ trans('messages.Complain.page_head') }}</a>
            </li>
            <li class="active">
                <strong>{{ trans('messages.Complain.report_head') }}</strong>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#month" data-toggle="tab" aria-expanded="true">
                    <span >{!! trans('messages.feesBills.monthly_report') !!}</span>
                </a>
            </li>
            <li>
                <a href="#year" data-toggle="tab" aria-expanded="false">
                    <span>{!! trans('messages.feesBills.yearly_report') !!}</span>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="month">
               {!! Form::open(array('url' => array('admin/maintainance/export'),'id' => 'report-m-form','class'=>'form-horizontal','method'=>'post')) !!}
                <div class="row">
                    <div class="col-sm-3 block-input">
                        {!! Form::select('month',getMonth (),null,array('class'=>'form-control')) !!}
                    </div>
                    <div class="col-sm-3 block-input">
                        <?php $yearList = selectYearOption(); ?>
                        <?php array_forget($yearList,""); ?>
                        {!! Form::select('year', $yearList, date('Y'), array('class'=>'form-control')); !!}
                    </div>

                    <div class="col-sm-6 text-right">
                        <button type="button" id="submit-m-report" class="btn btn-secondary btn-single">{{ trans('messages.Report.view_report') }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="tab-pane" id="year">
                {!! Form::open(array('url' => array('admin/maintainance/export'),'id' => 'report-y-form','class'=>'form-horizontal','method'=>'post')) !!}
                <div class="row">
                    <div class="col-sm-3 block-input">
                        {!! Form::select('year', $yearList, date('Y'), array('class'=>'form-control')); !!}
                    </div>
                    <div class="col-sm-9 text-right">
                        <button type="button" id="submit-y-report" class="btn btn-secondary btn-single">{{ trans('messages.Report.view_report') }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div id="chart-month" style="display:none;">
    <div class="row">
        <div class="col-md-6">
            <div class="chart-item-bg">
                <div id="chart-m-pie"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="chart-item-bg">
                <div id="chart-m-status"></div>
            </div>
        </div>

    </div>

    <div class="row flex-row">
        <div class="col-md-8">
            <div class="chart-item-bg">
                <div id="chart-m-stack"></div>
            </div>
        </div>
        <div class="col-md-4 flex-row">
            <div class="chart-item-bg" style="padding-top:20px;align-items: stretch;">
                <div id="chart-m-table" class="table-responsive" >
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center;">{{ trans('messages.type') }}</th>
                                <th style="text-align:center;">{{ trans('messages.amount') }}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right;">{{ trans('messages.feesBills.total') }}</th>
                                <th style="text-align:right;" id="sum-m-all"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="chart-item-bg" style="padding-top:20px;">
            <h2 style="margin-bottom:30px;" id="m-timeline-head"></h2>
                <div id="m-complain-timeline"></div>
            </div>
        </div>
    </div>
</div>

<div id="chart-year" style="display:none;">
    <div class="row">
        <div class="col-md-6">
            <div class="chart-item-bg">
                <div id="chart-y-pie"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="chart-item-bg">
                <div id="chart-y-status"></div>
            </div>
        </div>
    </div>

    <div class="row flex-row">
        <div class="col-md-8">
            <div class="chart-item-bg">
                <div id="chart-y-stack"></div>
            </div>
        </div>
        <div class="col-md-4 flex-row">
            <div class="chart-item-bg" style="padding-top:20px;align-items: stretch;">
                <div id="chart-y-table" class="table-responsive" >
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center;">{{ trans('messages.type') }}</th>
                                <th style="text-align:center;">{{ trans('messages.amount') }}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align:right;">{{ trans('messages.feesBills.total') }}</th>
                                <th style="text-align:right;" id="sum-y-all"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="chart-item-bg" style="padding-top:20px;">
                <h2 style="margin-bottom:30px;" id="y-timeline-head"></h2>
                <div id="y-complain-timeline"></div>
            </div>
        </div>
    </div>
</div>

<section id="chart-none" style="display:none;">
    <div class="panel panel-default">
		<div class="no-report">
         {{ trans('messages.feesBills.not_found_report') }}
	 	</div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/devexpress-web-14.1/js/globalize.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/devexpress-web-14.1/js/dx.chartjs.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script>
var serie_chart = [{ valueField: "0", name: "{{ trans('messages.Complain.status_wt') }}", color:'#cc3f44'},
{ valueField: "1", name: "{{ trans('messages.Complain.status_ip') }}", color:'#ffba00' },
{ valueField: "2", name: "{{ trans('messages.Complain.status_ck') }}", color:'#0e62c7' },
{ valueField: "3", name: "{{ trans('messages.Complain.status_cf') }}", color:'#68b828' },
{ valueField: "4", name: "{{ trans('messages.Complain.status_cls') }}", color:'#777' }];

$(function () {

    $('select').selectBoxIt().on('open', function(){
        $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
    });
})
</script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/complain-report.js"></script>

<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
@endsection
