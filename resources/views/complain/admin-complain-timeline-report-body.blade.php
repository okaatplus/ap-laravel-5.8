    @foreach($reports as $cr)
        <tr>
            <th colspan="5" style="background:#ececec;">{{ trans('messages.Complain.issue')}} : {{ $cr->title }} </th>
        </tr>
        <tr>
            <th style="background:#f3f3f3;">{{ trans('messages.unit_no') }}</th>
            <th style="text-align:center;">@if($cr->property_unit) {{ $cr->property_unit->unit_number }} @else นิติบุคคล @endif</th>
            <th style="background:#f3f3f3;">{{ trans('messages.Complain.report_date') }}</th>
            <th style="text-align:center;">{{ showDateTime($cr->created_at) }}</th>
            <th style="background:#f3f3f3;"></th>
        </tr>

        <tr>
            <th style="background:#f3f3f3;border-bottom: 3px double #dedede;">{{ trans('messages.Complain.status') }}</th>
            <th style="text-align:center;border-bottom: 3px double #dedede;">
                 <?php
                    switch ($cr->complain_status) {
                        case 0 :
                            echo '<span class="label label-danger">'.trans('messages.new').'</span>';
                            break;
                        case 1 :
                            echo '<span class="label label-warning">'.trans('messages.Complain.status_ip').'</span>';
                            break;
                        case 2 :
                            echo '<span class="label label-blue">'.trans('messages.Complain.status_ck').'</span>';
                            break;
                        case 3 :
                            echo '<span class="label label-secondary">'.trans('messages.Complain.status_cf').'</span>';
                            break;
                        default:
                            echo '<span class="label label-default">'.trans('messages.Complain.status_cls').'</span>';
                            break;
                    }
                ?>
            </th>
            <th style="background:#f3f3f3;border-bottom: 3px double #dedede;">{{ trans('messages.Complain.operate_time') }}</td>
            <th style="text-align:center;border-bottom: 3px double #dedede;">
            <?php 
                if($cr->complain_status == 4) 
                    $dh = calOverdue($cr->created_at, $cr->updated_at);
                else
                    $dh = calOverdue($cr->created_at);
            ?>
                {{ $dh }}
            </td>
            <th style="background:#f3f3f3;text-align:center;border-bottom: 3px double #dedede;">&nbsp;</td>
        </tr>

    @if($cr->complainAction->count())
    <?php $sum_working_day = 0; ?>
        <tr>
            <th style="background:#f7f7f7;text-align:center;">{{ trans('messages.Complain.from_state') }}</th>
            <th style="background:#f7f7f7;text-align:center;" align="center">{{ trans('messages.date') }}</th>
            <th style="background:#f7f7f7;text-align:center;" align="center">เป็นสถานะ</th>
            <th style="background:#f7f7f7;text-align:center;" align="center">{{ trans('messages.date') }}</th>
            <th style="background:#f7f7f7;text-align:center;" align="center">{{ trans('messages.Complain.operate_time') }}</th>
        </tr>
        @foreach ($cr->complainAction as $ca )
        <tr>
            <td>
            <?php
               switch ($ca->from_status) {
                    case 0 :
                        echo '<span class="label label-danger">'.trans('messages.new').'</span>';
                        break;
                    case 1 :
                        echo '<span class="label label-warning">'.trans('messages.Complain.status_ip').'</span>';
                        break;
                    case 2 :
                        echo '<span class="label label-blue">'.trans('messages.Complain.status_ck').'</span>';
                        break;
                    case 3 :
                        echo '<span class="label label-secondary">'.trans('messages.Complain.status_cf').'</span>';
                        break;
                    default:
                        echo '<span class="label label-default">'.trans('messages.Complain.status_cls').'</span>';
                        break;
                } 
            ?>
            </td>
            <td align="center">{{ showDateTime($ca->from_date) }}</td>
            <td>

            <?php
                switch ($ca->to_status) {
                    case 0 :
                        echo '<span class="label label-danger">'.trans('messages.new').'</span>';
                        break;
                    case 1 :
                        echo '<span class="label label-warning">'.trans('messages.Complain.status_ip').'</span>';
                        break;
                    case 2 :
                        echo '<span class="label label-blue">'.trans('messages.Complain.status_ck').'</span>';
                        break;
                    case 3 :
                        echo '<span class="label label-secondary">'.trans('messages.Complain.status_cf').'</span>';
                        break;
                    default:
                        echo '<span class="label label-default">'.trans('messages.Complain.status_cls').'</span>';
                        break;
                }
            ?>
            </td>
            <td align="center">{{ showDateTime($ca->action_date) }}</td>
            <td align="right">{{ $ca->working_day." ".trans_choice('messages.day',$ca->working_day)." ".$ca->working_hour." ".trans_choice('messages.hour',$ca->working_hour) }}
            </td>
        </tr>
        @endforeach
    @else
    @endif
    <tr>
        <td colspan="5">&nbsp;</td>
    </tr>
    @endforeach