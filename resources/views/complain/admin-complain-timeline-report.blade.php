<div class="table-responsive">
<table width="100%" style="background:#fff;" class="table table-bordered">
    @include('complain.admin-complain-timeline-report-body')
</table>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="chart-item-bg">
            <div style="padding:15px 0; padding-bottom:0;"><h2> {{ trans('messages.Complain.download_timelime_report') }}</h2></div>
        <div class="row">
            {!! Form::open(array('url' => array('admin/maintainance/export'),'id' => 'downlosd-debtor-report','class'=>'form-horizontal','method'=>'post')) !!}
            <div class="col-sm-8">
            @if($month)
                {{ trans('messages.Complain.m_timeline_dl_label') }} {{ $head }} 
            @else
                {{ trans('messages.Complain.y_timeline_dl_label') }} {{ $head }} 
            @endif
            </div>
            {!! Form::hidden('month',$month) !!}
            {!! Form::hidden('year',$year) !!}
            <div class="col-sm-4 text-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> {{ trans('messages.Report.download') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
        </div>
    </div>
</div>
