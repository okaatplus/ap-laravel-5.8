@extends('layouts.base-admin')
@section('content')
    <section class="mailbox-env hidden-print">
        <div class="row">
            <!-- Email Single -->
            <div class="col-sm-12 mailbox-right">
                <div class="mail-single">
                    <!-- Email Title and Button Options -->
                    <div class="mail-single-header">
                        <h2>
                            {{ e($complain->title) }}
                            @if( $complain->complain_status == 0 )
                            <span class="label label-danger">{{ trans('messages.new') }}</span>
                            @elseif ( $complain->complain_status == 1 )
                            <span class="label label-warning">{{ trans('messages.Complain.status_ip') }}</span>
                            @elseif ( $complain->complain_status == 2 )
                            <span class="label label-blue">{{ trans('messages.Complain.status_ck') }}</span>
                            @elseif ( $complain->complain_status == 3 )
                            <span class="label label-secondary">{{ trans('messages.Complain.status_cf') }}</span>
                            @else
                            <span class="label label-default">{{ trans('messages.Complain.status_cls') }}</span>
                            @endif
                            <a href="{{ URL::previous() }}" class="go-back">
                                <i class="fa-angle-left"></i> {{ trans('messages.back') }}
                            </a>
                        </h2>
                    </div>

                   <!-- Email Info From/Reply -->
                    <div class="mail-single-info">
                        <div class="mail-single-info-user dropdown">
                            <div class="dropdown-toggle" data-toggle="dropdown">
                                @if($complain->owner->profile_pic_name)
                                <img src="{{ env('URL_S3') . "/profile-img/" . $complain->owner->profile_pic_path . $complain->owner->profile_pic_name }}" alt="user-image" class="img-circle" width="38" />
                                @else
                                <img src="{{ url('/') }}/images/user-3.png" class="img-circle" width="38" />
                                @endif
                                <span>{{$complain->owner->name}}</span>
                                <em class="time">{{ LocalDateShort($complain->created_at) }}</em>
                            </div>
                        </div>
                        <div class="mail-single-info-options">
                            <div class="hidden-print div-print" style="display: inline-block;">
                                <a href="javascript:window.print();" class="btn btn-secondary btn-icon btn-block print-bill" style="color: #fff;">
                                    <i class="fa-print"></i>
                                    <span>{{ trans('messages.feesBills.print') }}</span>
                                </a>
                            </div>
                            @if( $complain->complain_status != 4 )
                            <div class="complain-action btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa-gear"></i> {{ trans('messages.Complain.change_status') }} <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu no-spacing change-complain-status" role="menu" data-cid="{{ $complain->id }}">
                                    @if($complain->complain_status == 0)
                                    <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="1">{{ trans('messages.Complain.status_ip') }}</a></li>
                                    @endif
                                    @if($complain->complain_status < 2)
                                    <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="2">{{ trans('messages.Complain.status_ck') }}</a></li>
                                    @endif
                                    @if($complain->complain_status < 3)
                                    <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="4">{{ trans('messages.Complain.status_cls') }}</a></li>
                                    @endif
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Email Body -->
                    <div class="mail-single-body">
                        <p><?php echo nl2br(e($complain->detail)) ?></p>
                    </div>

                    <!-- Email Attachments -->
                    @if(!$complain->complainFile->isEmpty())
                    <div class="mail-single-attachments">
                        <h3>
                            <i class="linecons-attach"></i>
                            {{ trans('messages.attachment') }}
                        </h3>
                        <ul class="list-unstyled list-inline">

                            @foreach($complain->complainFile as $file)
                            <li>
                                @if($file->is_image)
                                <a href="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="thumb gallery" rel="gal-1">
                                    <img width="145px" src="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="img-thumbnail" />
                                </a>
                                @else
                                <div>
                                    <a target="_blank" href="{{ url("/maintainance/get-attachment/".$file->id) }}" class="thumb">
                                        <img src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                                    </a>
                                    <div class="file-name">{{ $file->original_name }}</div>
                                </div>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section class="profile-env hidden-print">
        <section class="user-timeline-stories steam-post">
            <article class="timeline-story">
                <h3>
                    <i class="fa fa-comments"></i>
                    {{ trans('messages.Post.comments') }}
                </h3>
                <ul class="list-unstyled story-comments complain-comment">
                    <?php $comments =  $complain->comments; ?>
                    @include('complain.render_comment')
                </ul>
                <form method="post" action="" class="story-comment-form">
                    <textarea class="form-control input-unstyled autogrow" data-cid="{{ $complain->id }}" name="comment" placeholder="{{ trans('messages.Post.comments') }}..." style="overflow: hidden; word-wrap: break-word; resize: none; height: 62px;"  maxlength="2000"></textarea>
                    <button type="button" class="add-complain-comment btn btn-primary"><i class="fa fa-comment"></i> {{ trans('messages.Post.comment') }}</button>
                </form>
            </article>
        </section>
    </section>

    <div class="modal fade" id="change-status-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Complain.confirm_change_head') }}</h4>
                </div>
                <form method="post" action="{{url('/admin/maintainance/status')}}">
                <div class="modal-body">
                    {{ trans('messages.Complain.confirm_change_msg') }} "<span></span>"<br/><br/>
                    <b>{{ trans('messages.Post.comments') }}</b>
                    {!! Form::textarea('comment',null,['class'=>'form-control','rows'=>5]) !!}
                </div>
                <input type="hidden" name="status" id="c-status"/>
                <input type="hidden" name="cid" value="{{$complain->id}}"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary submit-status">{{ trans('messages.confirm') }}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('complain.admin-complain-print')
@endsection
@section('script')
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript">
        $(function () {
            $(".gallery").fancybox({
                helpers: {
                    overlay: {
                      locked: false
                    }
                },
                beforeLoad: function(){
                        $('html').css('overflow','hidden');
                },
                afterClose: function(){
                   $('html').css('overflow','auto');
                },
                'padding'           : 0,
                'transitionIn'      : 'elastic',
                'transitionOut'     : 'elastic',
                'changeFade'        : 0
            });

            $('.add-complain-comment').on('click', function () {
                if( $(this).prev().val() != "" ) {
                    $(this).attr('disabled','disabled').children('.fa').remove();
                    $(this).prepend('<i class="fa-spin fa-spinner"></i> ');
                    var target = $(this);
                    var cid = $(this).prev().data('cid');
                    var data = target.parent().serialize();
                        data+= "&cid="+ cid;
                    var request = $.ajax({
                    url:  $('#root-url').val()+"/maintainance/comment/add",
                        method: "POST",
                        data: data,
                        dataType: "json"
                    });
                    request.done(function( result ) {
                        if(result.status == true) {
                            target.parent().prev().html(result.content);
                        }
                        target.prev().val('');
                        target.removeAttr('disabled').children('.fa-spin').remove();
                        target.prepend('<i class="fa fa-comment"></i> ');
                    });
                }
            })

            $('.submit-status').on('click',function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $(this).parents('form').submit();
            })
            var temp;
            var status = ({1:'{{ trans('messages.Complain.status_ip') }}', 2:'{{ trans('messages.Complain.status_ck') }}', 4:'{{ trans('messages.Complain.status_cls') }}'});
            $('.change-complain-status li a').on('click', function (){
                temp = $(this).data('status');
                $('#c-status').val(temp);
                $('#change-status-modal .modal-body span').html(status[temp])
            });
        });
    </script>

    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
