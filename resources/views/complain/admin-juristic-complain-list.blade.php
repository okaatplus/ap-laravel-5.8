@if($complains->count())
 <?php
    $from   = (($complains->currentPage()-1)*$complains->perPage())+1;
    $to     = (($complains->currentPage()-1)*$complains->perPage())+$complains->perPage();
    $to     = ($to > $complains->total()) ? $complains->total() : $to;
 ?>
<table class="table mail-table">
     <thead>
    <tr>
        <th class="col-header-options">
            <div class="mail-pagination admin-complain-pagination">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$complains->total()]) !!}
                <div class="next-prev">
                   <a href="#" class="prev-page @if($complains->currentPage() == 1) disabled @endif" data-page="{{($complains->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                    <a href="#" class="next-page @if(!$complains->hasMorePages()) disabled @endif" data-page="{{($complains->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                </div>
            </div>
        </th>
    </tr>
    </thead>
</table>
@foreach($complains as $complain)
<div class="story-row">
    <a href="{{url('/admin/maintainance/juristic/view')."/".$complain->id }}">
        @if($complain->owner->profile_pic_name)
        <img src="{{ env('URL_S3')."/profile-img/".$complain->owner->profile_pic_path.$complain->owner->profile_pic_name }}" alt="user-image" class="img-circle s-profile-img"/>
        @else
        <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle s-profile-img" />
        @endif
        <div class="row">
            <div class="col-sm-7">
                <div class="t-name">
                    <b>{{ $complain->title }} </b>
                    @if( $complain->attachment_count > 0 )
                    <i class="linecons-attach"></i>
                    @endif
                </div>
            </div>
            <div class="col-sm-3">
                @if( $complain->complain_status == 0 )
                <span class="label label-danger">{{ trans('messages.new') }}</span>
                @elseif ( $complain->complain_status == 1 )
                <span class="label label-warning">{{ trans('messages.Complain.status_ip') }}</span>
                @elseif ( $complain->complain_status == 2 )
                <span class="label label-blue">{{ trans('messages.Complain.status_ck') }}</span>
                @elseif ( $complain->complain_status == 3 )
                <span class="label label-secondary">{{ trans('messages.Complain.status_cf') }}</span>
                @else
                <span class="label label-default">{{ trans('messages.Complain.status_cls') }}</span>
                @endif
            </div>
            <div class="col-sm-2 text-right shrink-up">
                <b>{{ localDate($complain->created_at) }}</b>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="t-owner-name">{{ $complain->owner->name }}</div>
            </div>
            <div class="col-sm-3">
                &nbsp;
            </div>
            <div class="col-sm-5">
                <div class="t-owner-name"><i class="fa fa-flag"></i> {{ $complain->category->{'name_'.App::getLocale()} }}</div>
            </div>
        </div>
    </a>
</div>
@endforeach
<table class="table mail-table">
     <!-- mail table footer -->
    <thead>
    <tr>
        <th class="col-header-options" style="border-bottom:0px;">
            <div class="mail-pagination">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$complains->total()]) !!}
                <div class="next-prev">
                    <a href="#" class="prev-page @if($complains->currentPage() == 1) disabled @endif" data-page="{{($complains->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                    <a href="#" class="next-page @if(!$complains->hasMorePages()) disabled @endif" data-page="{{($complains->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                </div>
            </div>
        </th>
    </tr>
    </thead>
</table>
<div class="not-found-status col-sm-12 text-center hide">{{ trans('messages.Complain.complain_not_found') }}</div><div class="clearfix"></div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Complain.have_no_complain') }}</div><div class="clearfix"></div>
@endif
