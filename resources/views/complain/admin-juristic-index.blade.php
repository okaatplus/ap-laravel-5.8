@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
    	<div class="title-env">
    		<h1 class="title">{{ trans('messages.Complain.juristic_head') }}</h1>
    	</div>
		<div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
				<li>
				    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li>
				    <a href="#">{{ trans('messages.Complain.page_head') }}</a>
                </li>
				<li class="active">
					<strong>{{ trans('messages.Complain.juristic_head') }}</strong>
				</li>
			</ol>
    	</div>
    </div>
    <section class="mailbox-env">
        <div class="row">
            <!-- Inbox emails -->
            <div class="col-sm-9 mailbox-right">
                <div class="text-right">
                    <button type="button" data-toggle="modal" data-target="#modal-complain-add" type="button" class="btn btn-primary bg-lg"><i class="fa fa-wrench"></i> {{ trans('messages.Complain.juristic_create_complain') }} </button>
                </div>
                <div class="mail-env" id="admin-complain-list">
                    <!-- mail table -->
                    @include('complain.admin-juristic-complain-list')
                </div>
            </div>
            <!-- Mailbox Sidebar -->
            <div class="col-sm-3 mailbox-left">
                <div class="mailbox-sidebar">
                    <ul class="list-unstyled mailbox-list" id="admin-notification-type-list">
                        <li class="active">
                            <a href="#" data-complain-group="-">
                                {{ trans('messages.all') }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-complain-group="0">
                                {{ trans('messages.Complain.status_wt') }}
                                @if($count_new > 0)
                                <span class="badge badge-green pull-right">{{$count_new}}</span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a href="#" data-complain-group="1">
                                {{ trans('messages.Complain.status_ip') }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-complain-group="2">
                                {{ trans('messages.Complain.status_ck') }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-complain-group="3">
                                {{ trans('messages.Complain.status_cf') }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-complain-group="4">
                                {{ trans('messages.Complain.status_cls') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-complain-add" data-backdrop="static">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'admin/maintainance/juristic/add','class'=>'form-horizontal','id'=>'add-complain-form')) !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Complain.juristic_create_complain') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div  style="margin-bottom:10px;">
                                {{ trans('messages.Complain.juristic_create_complain_descp') }}
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select class="form-control" name="complain_category_id" id="c-cate">
                                        <option value="">{{ trans('messages.type') }}</option>
                                        @foreach($c_cate as $cate)
                                        <option value="{{$cate->id}}">{{$cate->{'name_'.App::getLocale()} }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    {!! Form::text('title',null,array('class'=>'form-control','placeholder'=> trans('messages.title'),'maxlength' => 200)) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    {!! Form::textarea('detail',null,array('class'=>'form-control','placeholder'=> trans('messages.detail'),'maxlength' => 2000)) !!}
                                    <br/><span id="attachment"><i class="fa fa-paperclip"></i> {{ trans('messages.attach') }}</span>
                                    <div class=" field-hint">{{ trans('messages.upload_file_description') }}</div>
                                    <div id="previews"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                    <button type="button" class="btn btn-primary" id="add-complain">{{ trans('messages.Complain.sent_complain') }}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/upload-file.js"></script>

<script type="text/javascript">
    $(window).load(function () {
        $("#c-cate").selectBoxIt().on('open', function(){
            $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
        });

        $.validator.addMethod("notEqual", function(value, element, param) {
          return this.optional(element) || value != param;
        });

        $("#add-complain-form").validate({
            ignore:'',
            rules: {
                complain_category_id    : {notEqual:""},
                title : 'required',
                detail: 'required',
                complain_category_id : 'required'
            },
            errorPlacement: function(error, element) {}
        });

        $('#add-complain').on('click', function () {
            var valid =  $("#add-complain-form").valid();
            if($('#c-cate').val() == "" ) {
                $('#c-cateSelectBoxIt').css('border','1px solid #cc3f44');
                valid = false;
            } else {
                $('#c-cateSelectBoxIt').removeAttr('style');
            }
            if( valid ) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#add-complain-form').submit();
            } else {
                $(window).resize();
            }
        })

        $('.mailbox-list li a').on('click',function (e){
            e.preventDefault();
            if( !$(this).parent().hasClass('active') ) {
                $('.mailbox-list li').removeClass('active');
                $(this).parent().addClass('active');
                var group = $(this).data('complain-group');
                getAdminComplainPage(1,group);
            }
        })

        $('.mail-env').on('click','.next-page,.prev-page', function (e){
            e.preventDefault();
            if(!$(this).hasClass('disabled')) {
                var page = $(this).data('page');
                var type = $('#admin-notification-type-list li.active a').attr('data-complain-group');
                getAdminComplainPage (page,type);
            }
        });

        function getAdminComplainPage (page,type) {
            $('#admin-complain-list').css('opacity','0.7');
        	$.ajax({
                	url 	: $('#root-url').val()+"/admin/maintainance/juristic",
                	dataType: 'html',
                	method	: 'POST',
                	data 	: ({page:page,type:type}),
                	success	: function (t) {
                		$('#admin-complain-list').html(t);
                        $('#admin-complain-list').css('opacity','1');
                	}
                });
        }
    })
</script>
<link rel="stylesheet" href="{{url('/')}}/js/dropzone/css/dropzone.css">
@endsection
