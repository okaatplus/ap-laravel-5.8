@extends('layouts.base-admin')
@section('content')
    <section class="mailbox-env hidden-print">
        <div class="row" style="display: flex;">
            <!-- Email Single -->
            <div class="col-sm-8" style="flex: 1;padding-bottom: 30px;">
                <div class="mail-single" style="height: 100%;">
                    <!-- Email Title and Button Options -->
                    <div class="mail-single-header">
                        <h2>
                            {{ e($complain->title) }}
                            @if( $complain->complain_status == 0 )
                            <span class="label label-danger">{{ trans('messages.new') }}</span>
                            @elseif ( $complain->complain_status == 1 )
                            <span class="label label-warning">{{ trans('messages.Complain.status_ip') }}</span>
                            @elseif ( $complain->complain_status == 2 )
                            <span class="label label-blue">{{ trans('messages.Complain.status_ck') }}</span>
                            @elseif ( $complain->complain_status == 3 )
                            <span class="label label-secondary">{{ trans('messages.Complain.status_cf') }}</span>
                            @else
                            <span class="label label-default">{{ trans('messages.Complain.status_cls') }}</span>
                            @endif
                            <a href="{{ URL::previous() }}" class="go-back">
                                <i class="fa-angle-left"></i> {{ trans('messages.back') }}
                            </a>
                        </h2>
                    </div>

                   <!-- Email Info From/Reply -->
                    <div class="mail-single-info">
                        <div class="mail-single-info-user dropdown">
                            <div>
                                @if($complain->owner->profile_pic_name)
                                <img src="{{ env('URL_S3') . "/profile-img/" . $complain->owner->profile_pic_path . $complain->owner->profile_pic_name }}" alt="user-image" class="img-circle" width="38" height="38" />
                                @else
                                <img src="{{ url('/') }}/images/user-3.png" class="img-circle" width="38" />
                                @endif
                                <div style="margin-left: 50px;">
                                    <span>{{$complain->owner->name}}</span>
                                    @if(isset($complain->property_unit))
                                            <br>{{trans('messages.unit_no')}} : {{$complain->property_unit->unit_number}} <br>
                                    @endif
                                    <em class="time">{{trans('messages.Complain.report_date')}} : {{ LocalDateShort($complain->created_at) }}</em>
                                    <?php
                                        $cate   = unserialize(constant('ZONE_'.strtoupper(App::getLocale())));
                                        $zone_arr = array("0", "1", "2", "3", "4", "5");
                                        $zone = (in_array($complain->zone, $zone_arr)) ? $complain->zone : 99;
                                    ?>
                                    <em class="time"><i class="fa fa-flag"></i> {{ $complain->category->{'name_'.App::getLocale()} }} - {{ trans('messages.Complain.zone') }} {{ $cate[$zone] }}</em>
                                </div>
                            </div>
                        </div>
                        <div class="mail-single-info-options">
                            <div class="hidden-print div-print" style="display: inline-block;">
                                <a href="javascript:window.print();" class="btn btn-secondary btn-icon btn-block print-bill" style="color: #fff;">
                                    <i class="fa-print"></i>
                                    <span>{{ trans('messages.feesBills.print') }}</span>
                                </a>
                            </div>


                                {{-- // Normal Case : Resident Can Checking --}}
                            @if($complain->complain_status != 4)
                                @if(($complain->created_by_admin || $complain->is_juristic_complain))

                                   <div class="complain-action btn-group">
                                       <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                           <i class="fa-gear"></i> {{ trans('messages.Complain.change_status') }}
                                       </button>
                                       <ul class="dropdown-menu no-spacing change-complain-status" role="menu" data-cid="{{ $complain->id }}">
                                           @if($complain->complain_status == 0)
                                           <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="1">{{ trans('messages.Complain.status_ip') }}</a></li>
                                           @endif
                                           @if($complain->complain_status < 2)
                                           <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="2">{{ trans('messages.Complain.status_ck') }}</a></li>
                                           @endif
                                           @if($complain->complain_status < 3)
                                           <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="4">{{ trans('messages.Complain.status_cls') }}</a></li>
                                           @endif
                                       </ul>
                                   </div>
                               @else
                                   @if( $complain->complain_status != 3 )
                                        @if( $complain->complain_status != 2 && $complain->complain_status != 4 )
                                        <div class="complain-action btn-group">
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa-gear"></i> {{ trans('messages.Complain.change_status') }}
                                            </button>
                                            <ul class="dropdown-menu no-spacing change-complain-status" role="menu" data-cid="{{ $complain->id }}">
                                                @if($complain->complain_status == 0)
                                                <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="1">{{ trans('messages.Complain.status_ip') }}</a></li>
                                                @endif
                                                @if($complain->complain_status <= 1)
                                                <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="2">{{ trans('messages.Complain.status_ck') }}</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                        @endif
                                    @else
                                        <div class="complain-action btn-group">
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa-gear"></i> {{ trans('messages.Complain.change_status') }}
                                            </button>
                                            <ul class="dropdown-menu no-spacing change-complain-status" role="menu" data-cid="{{ $complain->id }}">
                                            <li><a href="#" data-toggle="modal" data-target="#change-status-modal" data-status="4">{{ trans('messages.Complain.status_cls') }}</a></li>
                                            </ul>
                                        </div>
                                    @endif
                                @endif
                            @endif
                    </div>
                </div>

                <!-- Email Body -->
                <div class="mail-single-body">
                    <p><?php echo nl2br(e($complain->detail)) ?></p>
                </div>

                    <!-- Email Attachments -->
                    @if(!$complain->complainFile->isEmpty())
                    <div class="mail-single-attachments mail-single-body">
                        <h3>
                            <i class="linecons-attach"></i>
                            {{ trans('messages.attachment') }}
                        </h3>
                        <ul class="list-unstyled list-inline">

                            @foreach($complain->complainFile as $file)
                            <li>
                                @if($file->is_image)
                                <a href="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="thumb gallery" rel="gal-1">
                                    <img width="145px" src="{{env('URL_S3')}}/complain-file/{{$file->url.$file->name}}" class="img-thumbnail" />
                                </a>
                                @else
                                <div>
                                    <a target="_blank" href="{{ url("/maintainance/get-attachment/".$file->id) }}" class="thumb">
                                        <img src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                                    </a>
                                    <div class="file-name">{{ $file->original_name }}</div>
                                </div>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if($complain->user_appointment_note != null)
                        <div class="mail-single-info">
                            <h3>
                                <i class="fa fa-calendar"></i>
                                {{ trans('messages.Complain.appointment_more_detail') }}
                            </h3>
                            <div class="mail-single-info-user">
                                <div style="margin-left: 20px;">
                                    <em class="time">{{ $complain->user_appointment_note }}</em>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-sm-4 mailbox-right" style="padding-left: 0;padding-bottom: 30px;">
                <div class="mail-single" style="height: 100%;">
                    <!-- Email Title and Button Options -->
                    <div class="mail-single-header">
                        <h2>
                            {{ trans('messages.Complain.appointment_title') }}
                        </h2>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="{{ url('/admin/maintainance/update-detail') }}" accept-charset="UTF-8" class="form-horizontal" id="settings-profile-form" novalidate="novalidate"><input name="_token" type="hidden" value="UBIh5panfxlzYRDpTvGyg1zkgbC5O7mliZHAFBdu">
                            <input type="hidden" name="cid" value="{{$complain->id}}"/>
                            <div class="form-group">
                                <label class="control-label more-margin">{{ trans('messages.Complain.appointment_technician_and_user') }}</label>
                                    <div class="form-group gender-input ">
                                        <div class="radio_custom col-sm-12">
                                            @if($complain->is_appointment)
                                                <input id="is_appointment_true" checked="checked" name="is_appointment" type="radio" value="true">
                                                <label for="is_appointment_true">{{ trans('messages.Complain.is_appointment_yes') }}</label>
                                                <input id="is_appointment_false" name="is_appointment" type="radio" value="false">
                                                <label for="is_appointment_false">{{ trans('messages.Complain.is_appointment_no') }}</label>
                                            @else
                                                <input id="is_appointment_true" name="is_appointment" type="radio" value="true">
                                                <label for="is_appointment_true">{{ trans('messages.Complain.is_appointment_yes') }}</label>
                                                <input id="is_appointment_false" checked="checked" name="is_appointment" type="radio" value="false">
                                                <label for="is_appointment_false">{{ trans('messages.Complain.is_appointment_no') }}</label>
                                            @endif
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group-separator"></div>

                            {{--<div class="form-group">
                                <label class="control-label more-margin">{{ trans('messages.Complain.is_deposit_key') }}</label>
                                    <div class="form-group gender-input">
                                        <div class="radio_custom col-sm-12">
                                            @if($complain->is_deposit_key)
                                                <input id="is_deposit_key_true" checked="checked" name="is_deposit_key" type="radio" value="true">
                                                <label for="is_deposit_key_true">{{ trans('messages.Complain.is_deposit_yes') }}</label>
                                                <input id="is_deposit_key_false" name="is_deposit_key" type="radio" value="false">
                                                <label for="is_deposit_key_false">{{ trans('messages.Complain.is_deposit_no') }}</label>
                                            @else
                                                <input id="is_deposit_key_true" name="is_deposit_key" type="radio" value="true">
                                                <label for="is_deposit_key_true">{{ trans('messages.Complain.is_deposit_yes') }}</label>
                                                <input id="is_deposit_key_false" checked="checked" name="is_deposit_key" type="radio" value="false">
                                                <label for="is_deposit_key_false">{{ trans('messages.Complain.is_deposit_no') }}</label>
                                            @endif
                                        </div>
                                    </div>
                            </div>

                            <div class="form-group-separator"></div>--}}
                            <div class="form-group">
                                <label class="control-label more-margin" for="phone">{{ trans('messages.Complain.appointment_date') }}</label>
                                    <div class="form-group">
                                        <div class="col-sm-10 block-input">
                                            <div class="date-and-time">
                                                <?php
                                                    if($complain->appointment_date) $sd = date('Y/m/d',strtotime($complain->appointment_date));
                                                    else $sd = null;
                                                ?>
                                                {!! Form::text('appointment_date',$sd,array('class'=>'form-control datepicker','size'=>25, 'placeholder'=> 'วันที่นัดหมายกับช่าง','data-format'=>'yyyy/mm/dd','data-language'=>App::getLocale() )) !!}

                                                <?php
                                                    $st = date('h:i A',strtotime($complain->appointment_date));
                                                ?>
                                                {!! Form::text('appointment_time', $st, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5"]) !!}
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="form-group-separator"></div>
                            <div class="form-group">
                                <label class="control-label more-margin" for="phone">{{ trans('messages.Complain.technician_name') }}</label>
                                <div class="form-group">
                                    <div class="col-sm-10 block-input">
                                        <div class="date-and-time">
                                            {!! Form::text('technician_name',$complain->technician_name,array('class'=>'form-control','placeholder'=> 'ชื่อช่างปฎิบัติงาน','maxlength' => 200)) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="button" class="btn btn-primary" id="save-detail-complain-btn">บันทึก</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php $comments =  $complain->comments; ?>
    @if($comments->count() > 0)
    <section class="profile-env hidden-print">
        <section class="user-timeline-stories steam-post">
            <article class="timeline-story">
                <h3>
                    <i class="fa fa-comments"></i>
                    {{ trans('messages.Post.comments') }}
                </h3>
                <ul class="list-unstyled story-comments complain-comment">

                    @include('complain.render_comment')
                </ul>
                {{--<form method="post" action="" class="story-comment-form">
                    <textarea class="form-control input-unstyled autogrow" data-cid="{{ $complain->id }}" name="comment" placeholder="{{ trans('messages.Post.comments') }}..." style="overflow: hidden; word-wrap: break-word; resize: none; height: 62px;"  maxlength="2000"></textarea>
                    <button type="button" class="add-complain-comment btn btn-primary"><i class="fa fa-comment"></i> {{ trans('messages.Post.comment') }}</button>
                </form>--}}
            </article>
        </section>
    </section>
    @endif

    @if($complain->review_rate != "" || $complain->review_comment != "")
    <section class="profile-env hidden-print">
        <section class="user-timeline-stories steam-post">
            <article class="timeline-story">
                <h3>
                    <i class="fa fa-comment"></i>
                    {{ trans('messages.Complain.review_title') }}
                </h3>
                <ul class="list-unstyled story-comments complain-comment">
                    <li>
                        <div class="story-comment">
                            <a href="#" class="comment-user-img">
                                @if($complain->owner->profile_pic_name)
                                    <img src="{{ env('URL_S3')."/profile-img/".$complain->owner->profile_pic_path.$complain->owner->profile_pic_name }}" alt="user-image" class="img-responsive img-circle" width="38" height="38"/>
                                @else
                                    <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-responsive img-circle" />
                                @endif
                            </a>
                            <div class="story-comment-content">
                                <span class="story-comment-user-name">
                                    <b>{{ $complain->owner->name }}</b>
                                    @if($complain->review_rate != "")
                                        <?php
                                            $rate_text = "ดีมาก";
                                            if($complain->review_rate == "1"){
                                                $rate_text = "ควรปรับปรุง";
                                            }
                                            if($complain->review_rate == "2"){
                                                $rate_text = "พอใช้";
                                            }
                                            if($complain->review_rate == "3"){
                                                $rate_text = "ดีมาก";
                                            }

                                        ?>
                                        ระดับความพึงพอใจ : {{ $rate_text }}
                                        {{--<input type="hidden" name="rating" class="rating" disabled="disabled"/>
                                        <img width="20px;" src="{{url('/')}}/images/rating/{{$complain->review_rate}}.svg" alt="user-img" class="img-responsive img-circle" />--}}
                                    @endif
                                </span>
                                @if($complain->review_comment != null)
                                    <p>"{{ e($complain->review_comment) }}"</p>
                                @endif
                            </div>
                        </div>
                    </li>
                </ul>
            </article>
        </section>
    </section>
    @endif


    <div class="modal fade" id="change-status-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Complain.confirm_change_head') }}</h4>
                </div>
                <form method="post" action="{{url('/admin/maintainance/status')}}">
                <div class="modal-body">
                    {{ trans('messages.Complain.confirm_change_msg') }} "<span></span>"<br/><br/>
                    {{--<b>{{ trans('messages.Post.comments') }}</b>
                    {!! Form::textarea('comment',null,['class'=>'form-control','rows'=>5]) !!}--}}
                </div>
                <input type="hidden" name="status" id="c-status"/>
                <input type="hidden" name="cid" value="{{$complain->id}}"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary submit-status">{{ trans('messages.confirm') }}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('complain.admin-complain-print')
@endsection
@section('script')
    <script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.th.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="{{url('/')}}/js/bootstrap-rating/bootstrap-rating.js"></script>
    <script type="text/javascript">
        $(function () {
            $('input.rating').rating('rate', '{{ $complain->review_rate }}');
            $(".gallery").fancybox({
                helpers: {
                    overlay: {
                      locked: false
                    }
                },
                beforeLoad: function(){
                        $('html').css('overflow','hidden');
                },
                afterClose: function(){
                   $('html').css('overflow','auto');
                },
                'padding'           : 0,
                'transitionIn'      : 'elastic',
                'transitionOut'     : 'elastic',
                'changeFade'        : 0
            });

            $('.add-complain-comment').on('click', function () {
                if( $(this).prev().val() != "" ) {
                    $(this).attr('disabled','disabled').children('.fa').remove();
                    $(this).prepend('<i class="fa-spin fa-spinner"></i> ');
                    var target = $(this);
                    var cid = $(this).prev().data('cid');
                    var data = target.parent().serialize();
                        data+= "&cid="+ cid;
                    var request = $.ajax({
                    url:  $('#root-url').val()+"/maintainance/comment/add",
                        method: "POST",
                        data: data,
                        dataType: "json"
                    });
                    request.done(function( result ) {
                        if(result.status == true) {
                            target.parent().prev().html(result.content);
                        }
                        target.prev().val('');
                        target.removeAttr('disabled').children('.fa-spin').remove();
                        target.prepend('<i class="fa fa-comment"></i> ');
                    });
                }
            })

            $('.submit-status').on('click',function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $(this).parents('form').submit();
            })

            $('#save-detail-complain-btn').on('click',function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $(this).parents('form').submit();
            })

            var temp;
            var status = ({1:'{{ trans('messages.Complain.status_ip') }}', 2:'{{ trans('messages.Complain.status_ck') }}', 4:'{{ trans('messages.Complain.status_cls') }}'});
            $('.change-complain-status li a').on('click', function (){
                temp = $(this).data('status');
                $('#c-status').val(temp);
                $('#change-status-modal .modal-body span').html(status[temp])
            });
        });
    </script>

    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
