@if($complains->count())
@foreach($complains as $complain)
<div class="story-row complain-tr tr-{{$complain->complain_status}}">
    <a href="{{url('complain/view')."/".$complain->id }}">
        @if($complain->owner->profile_pic_name)
        <img src="{{ env('URL_S3')."/profile-img/".$complain->owner->profile_pic_path.$complain->owner->profile_pic_name }}" alt="user-image" class="img-circle s-profile-img"/>
        @else
        <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle s-profile-img" />
        @endif
        <div class="row">
            <div class="col-sm-7">
                <div class="t-name">
                    <b>{{ $complain->title }} </b>
                    @if( $complain->attachment_count > 0 )
                    <i class="linecons-attach"></i>
                    @endif
                </div>
            </div>
            <div class="col-sm-3">
                @if( $complain->complain_status == 0 )
                <span class="label label-danger">{{ trans('messages.new') }}</span>
                @elseif ( $complain->complain_status == 1 )
                <span class="label label-warning">{{ trans('messages.Complain.status_ip') }}</span>
                @elseif ( $complain->complain_status == 2 )
                <span class="label label-blue">{{ trans('messages.Complain.status_ck') }}</span>
                @elseif ( $complain->complain_status == 3 )
                <span class="label label-secondary">{{ trans('messages.Complain.status_cf') }}</span>
                @else
                <span class="label label-default">{{ trans('messages.Complain.status_cls') }}</span>
                @endif
            </div>
            <div class="col-sm-2 text-right shrink-up">
                <b>{{ localDate($complain->created_at) }}</b>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="t-owner-name">{{ $complain->owner->name }}</div>
            </div>
            <div class="col-sm-3">
                <div class="t-owner-name">{{ $complain->owner->property_unit->unit_number }}</div>
            </div>
            <div class="col-sm-5">
                <div class="t-owner-name"><i class="fa fa-flag"></i> {{ $complain->category->{'name_'.App::getLocale()} }}</div>
            </div>
        </div>
    </a>
</div>
@endforeach
<div class="clearfix"></div>
<div class="not-found-status col-sm-12 text-center hide" style="padding:20px;">{{ trans('messages.Complain.complain_not_found') }}</div>
<div class="clearfix"></div>
@else
<div class="col-sm-12 text-center" style="padding:20px;">{{ trans('messages.Complain.have_no_complain') }}</div><div class="clearfix"></div>
@endif
