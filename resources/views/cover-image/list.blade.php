@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Cover Image</h1>
		</div>
		<div class="breadcrumb-env">
            <a href="#" class="btn btn-primary top-action" data-toggle="modal" data-target="#upload-cover-modal">เพิ่มรูป cover</a>
		</div>
	</div>
    <section class="bills-env">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="no-top-margin">รูป Cover สำหรับผู้ใช้งานที่เป็นลูกค้า</h3>
                <div class="user-cover-image" style="margin-top: 20px;">
                @if( $user_cover_imgs->count() )
                    @foreach( $user_cover_imgs as $img)
                    <div class="preview-img-item" style="margin: 0;width:25%;">
                        <img src="{{ env('URL_S3')."/cover-file/".$img->url.$img->name }}" width="100%" alt="post-image" />
                        <span class="remove-img-preview remove-cover" data-id="{{ $img->id }}">×</span>
                    </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-sm-12 text-center"> ไม่มีข้อมูล </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </section>
    <section class="bills-env">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="no-top-margin">รูป Cover สำหรับผู้ใช้งานทั่วไป</h3>
                <div class="ordinary-cover-image" style="margin-top: 20px;">
                @if( $ordinary_cover_imgs->count() )
                    @foreach( $ordinary_cover_imgs as $img)
                        <div class="preview-img-item" style="margin: 0;width:25%;">
                            <img src="{{ env('URL_S3')."/cover-file/".$img->url.$img->name }}" width="100%" alt="post-image" />
                            <span class="remove-img-preview remove-cover" data-id="{{ $img->id }}">×</span>
                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-sm-12 text-center"> ไม่มีข้อมูล </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="upload-cover-modal" data-backdrop="static">
        <div class="modal-dialog" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">เพิ่มรูป cover</h4>
                </div>
                {!! Form::open(array('url' => 'management/admin/app-cover-image/save','class' => 'form-horizontal', 'id' => 'add-cover-form')) !!}
                <div class="modal-body">
                    <div class="row form-group">
                        <label class="col-sm-2 control-label" for="profile-image">อัพโหลดรูป cover</label>
                        <div class="col-md-4">
                            <div id="attachment-banner-crop" class="drop-property-file dz-clickable" style="margin-bottom: 0; @if(!empty($property['banner_url']) && $property['banner_url']) display: none; @endif">
                                <i class="fa fa-camera"></i>
                            </div>
                            <div class="preview-crop"></div>
                            <div class="post-banner" id="previews-img-banner-crop">
                            </div>

                        </div>
                        <div class="col-sm-5">
                            <div class="img-container">
                                <img class="img-responsive" />
                            </div>
                        </div>
                        <input type="hidden" id="img-x" name="img-x"/>
                        <input type="hidden" id="img-y" name="img-y"/>
                        <input type="hidden" id="img-w" name="img-w"/>
                        <input type="hidden" id="img-h" name="img-h"/>
                        <input type="hidden" id="img-tw" name="img-tw"/>
                        <input type="hidden" id="img-th" name="img-th"/>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2 control-label">ประเภท Cover</div>
                        <div class="col-sm-4">
                            {!! Form::select('cover_type',['u' => 'รูป cover สำหรับผู้ใช้งานที่เป็นลูกค้า','o' => 'รูป cover สำหรับผู้ใช้งานทั่วไป'],null,['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="add-cover-btn">{{ trans('messages.save') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="remove-cover-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">ลบรูป cover</h4>
                </div>
                <div class="modal-body">
                    คุณแน่ใจว่าต้องการลบรูป cover นี้
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="confirm-remove-cover">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/crop-img.js"></script>
<link rel="stylesheet" href="{{url('/')}}/js/cropper/cropper.min.css">
<script type="text/javascript">
    var target,target_p;
    $(function () {
        $('#add-cover-btn').on('click', function () {
            $(this).prepend('<i class="fa-spin fa-spinner"></i> ').attr('disabled','disabled');
            $('#add-cover-form').submit();
        })
        $('.remove-cover').on('click', function () {
            target = $(this).data('id');
            target_p = $(this).parent();
            $('#remove-cover-modal').modal('show');
        });

        $('#confirm-remove-cover').on('click', function () {
            var btn = $(this);
            btn.prepend('<i class="fa-spin fa-spinner"></i>').attr('disabled','disabled');
            $.ajax({
                url     : $('#root-url').val()+"/management/admin/app-cover-image/remove",
                data    : ({id:target}),
                dataType: "json",
                method: 'post',
                success: function (r) {
                   if(r.result ) {
                       target_p.fadeOut(500,function () {
                           $(this).remove();
                       })

                       btn.removeAttr('disabled').find('.fa-spin').remove();
                       $('#remove-cover-modal').modal('hide');
                   }
                }
            })
        })
    })
</script>
@endsection
