@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Dashboard</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>Dashboard</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <a href="{{url('admin/messages')}}" class="dash-link xe-widget xe-counter-block xe-counter-block-success" data-count=".num" data-from="0" data-to="{{ $new_messages_count }}" data-duration="2">
                <div class="xe-upper">
                    <div class="xe-icon">
                        <i class="linecons-comment"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $new_messages_count }}</strong>
                        <span>New messages</span>
                    </div>
                </div>
                <div class="xe-lower">
                    <div class="border"></div>
                    <span>Messages</span>
                    <strong>Messages need to reply</strong>
                </div>
            </a>
        </div>
        <div class="col-sm-4">
            <a href="{{url('admin/parcel-tracking?tab=uncollected')}}" class="dash-link xe-widget xe-counter-block xe-counter-block-info" data-count=".num" data-from="0" data-to="{{ $post_parcels_count }}" data-suffix=" items" data-duration="2">
                <div class="xe-upper">
                    <div class="xe-icon">
                        <i class="linecons-mail"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $post_parcels_count }}</strong>
                        <span>age are over 14 days</span>
                    </div>
                </div>
                <div class="xe-lower">
                    <div class="border"></div>
                    <span>Posts and parcels</span>
                    <strong>Need to remind</strong>
                </div>
            </a>
        </div>
        <div class="col-sm-4">
            <a href="{{url('admin/property/new-members')}}" class="dash-link xe-widget xe-counter-block xe-counter-block-danger" data-count=".num" data-from="0" data-to="{{ $waiting_for_approve }}" data-duration="2">
                <div class="xe-upper">
                    <div class="xe-icon">
                        <i class="linecons-user"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $waiting_for_approve }}</strong>
                        <span>Waiting for approval</span>
                    </div>
                </div>
                <div class="xe-lower">
                    <div class="border"></div>
                    <span>Customer</span>
                    <strong>{{ $registered_user }} of {{ $unregister_user_count+$registered_user }} has registered</strong>
                </div>
            </a>
        </div>
    </div>
    <?php /*
    <div class="row">
        <div class="col-sm-3">
            <a class="dash-link" href="{{ url('admin/property/new-members') }}">
                <div class="xe-widget xe-vertical-counter xe-vertical-counter-primary" data-count=".num" data-from="0" data-to="{{ $un_approved_user }}" data-decimal="." data-suffix="" data-duration="2.5">
                    <div class="xe-icon">
                        <i class="linecons-user"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $un_approved_user }}</strong>
                        <span>New user</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-sm-3">
            <a class="dash-link" href="{{ url('admin/maintainance') }}">
                <div class="xe-widget xe-vertical-counter xe-vertical-counter-danger" data-count=".num" data-from="0" data-to="104" data-decimal="." data-suffix="" data-duration="2.5">
                    <div class="xe-icon">
                        <i class="linecons-note"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">104</strong>
                        <span>New Maintainance</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-sm-3">
            <a class="dash-link" href="{{ url('admin/news-announcement') }}">
                <div class="xe-widget xe-vertical-counter xe-vertical-counter-success" data-count=".num" data-from="0" data-to="2" data-decimal="." data-suffix="" data-duration="2.5">
                    <div class="xe-icon">
                        <i class="linecons-mail"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">2</strong>
                        <span>News pending approval</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-sm-3">
            <a class="dash-link" href="{{ url('admin/parcel-tracking') }}">
                <div class="xe-widget xe-vertical-counter xe-vertical-counter-info" data-count=".num" data-from="0" data-to="203" data-decimal="." data-suffix="" data-duration="2.5">
                    <div class="xe-icon">
                        <i class="linecons-paper-plane"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">203</strong>
                        <span>Posts & Parcels</span>
                    </div>
                </div>
            </a>
        </div>
    </div> */ ?>

@endsection
@section('script')
    <script src="{{ url('/') }}/js/xenon-widgets.js"></script>
    <style>
        .dash-link {
            display: block;
            transition: all 220ms ease-in-out;
        }
        .dash-link:hover {
            box-shadow: 0px 0px 10px #323232;
        }
    </style>
@endsection