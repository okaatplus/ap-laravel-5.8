@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Dashboard</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>Dashboard</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="get-dashboard active xe-widget xe-counter-block xe-counter-block-success" data-count=".num" data-from="0" data-to="{{ $new_messages_count }}" data-duration="2" data-url="{{ url('smart/admin/dashboard/new-messages') }}">
                <div class="xe-upper">
                    <div class="xe-icon">
                        <i class="linecons-comment"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $new_messages_count }}</strong>
                        <span>New messages</span>
                    </div>
                </div>
                <div class="xe-lower">
                    <div class="border"></div>
                    <span>Messages</span>
                    <strong>Messages need to reply</strong>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="get-dashboard xe-widget xe-counter-block xe-counter-block-info" data-count=".num" data-from="0" data-to="{{ $post_parcels_count }}" data-suffix=" items" data-duration="2" data-url="{{ url('smart/admin/dashboard/posts-parcels') }}">
                <div class="xe-upper">
                    <div class="xe-icon">
                        <i class="linecons-mail"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $post_parcels_count }}</strong>
                        <span>age are over 14 days</span>
                    </div>
                </div>
                <div class="xe-lower">
                    <div class="border"></div>
                    <span>Posts and parcels</span>
                    <strong>Need to remind</strong>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="get-dashboard xe-widget xe-counter-block xe-counter-block-danger" data-count=".num" data-from="0" data-to="{{ $waiting_for_approve }}" data-duration="2" data-url="{{ url('smart/admin/dashboard/users') }}">
                <div class="xe-upper">
                    <div class="xe-icon">
                        <i class="linecons-user"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">{{ $waiting_for_approve }}</strong>
                        <span>Waiting for approval</span>
                    </div>
                </div>
                <div class="xe-lower">
                    <div class="border"></div>
                    <span>Customer</span>
                    <strong>{{ $registered_user }} of {{ $unregister_user_count+$registered_user }} has registered</strong>
                </div>
            </div>
        </div>
    </div>

    <div class="chart-item-bg">
        <div class="row">
            <div class="col-sm-12" id="landing-dashboard">
                @include('dashboard.smart.un-response-msg-list')
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ url('/') }}/js/xenon-widgets.js"></script>
    <script>
        $(function () {

            $('#submit-search').on('click',function (){
                searchPage(1);
            });

            $('#search-form').on('keydown', function(e) {
                if(e.keyCode == 13) {
                    e.preventDefault();
                    searchPage (1);
                }
            });

            $('.reset-s-btn').on('click',function () {
                $(this).closest('form').find("input").val("");
                $(this).closest('form').find("select option:selected").removeAttr('selected');
                searchPage (1);
            });

            $('body').on('click','.paginate-link', function (e){
                e.preventDefault();
                searchPage($(this).attr('data-page'));
            });

            $('body').on('change','.paginate-select', function (e){
                e.preventDefault();
                searchPage($(this).val());
            });


            $('.get-dashboard').on('click', function () {
                var url = $(this).attr('data-url');
                if( !$(this).hasClass('active') ) {
                    $('.get-dashboard').removeClass('active');
                    $(this).addClass('active');
                    $('#landing-dashboard').css('opacity', '0.6');
                    $.ajax({
                        url     : url,
                        dataType: "html",
                        method: 'post',
                        success: function (h) {
                            $('#landing-dashboard').html(h);
                            $('#landing-dashboard').css('opacity','1');
                            //$('[data-toggle="tooltip"]').tooltip();
                            //cbr_replace();
                        }
                    })
                }
            })
        })
    </script>
    <style>
        .get-dashboard:hover {
            cursor: pointer;
            box-shadow: 0 0 10px #323232;
        }
    </style>
@endsection