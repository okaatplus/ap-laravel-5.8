@if($posts_parcels->count() > 0)
    <?php
    $from   = (($posts_parcels->currentPage()-1)*$posts_parcels->perPage())+1;
    $to     = (($posts_parcels->currentPage()-1)*$posts_parcels->perPage())+$posts_parcels->perPage();
    $to     = ($to > $posts_parcels->total()) ? $posts_parcels->total() : $to;
	$allpage = $posts_parcels->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>
	<div class="row">
		<div class="col-md-4" style="margin-bottom: 10px;">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$posts_parcels->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($posts_parcels->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts_parcels->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($posts_parcels->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $posts_parcels->lastPage(),$posts_parcels->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($posts_parcels->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts_parcels->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th class="text-center" width="80px">No.</th>
					<th>Property Name</th>
					<th  class="text-center" width="160px">Posts & Parcels</th>
					<th  class="text-center" width="70px">View</th>
				</tr>
				</thead>
				<tbody>
				@foreach($posts_parcels as $key => $p)
				<tr>
					<td class="text-center">{{ $key + $from }}</td>
					<td>{{ $p->property_name_th }}</td>
					<td class="text-right">{{ number_format($p->overdue_posts_parcels_count) }}</td>
					<td class="action-links text-center">
						<a href="{{url('/admin/property/directlogin/'.$p->id.'?menu=posts-parcels')}}" class="btn btn-red">
							<i class="fa-external-link-square"></i>
						</a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$posts_parcels->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($posts_parcels->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts_parcels->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($posts_parcels->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $posts_parcels->lastPage(),$posts_parcels->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($posts_parcels->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts_parcels->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
@else
	<div class="col-sm-12 text-center">{{ trans('messages.no_data') }}</div><div class="clearfix"></div>
@endif
