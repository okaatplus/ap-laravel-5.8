@if($un_ap_user_list->count() > 0)
    <?php
    $from   = (($un_ap_user_list->currentPage()-1)*$un_ap_user_list->perPage())+1;
    $to     = (($un_ap_user_list->currentPage()-1)*$un_ap_user_list->perPage())+$un_ap_user_list->perPage();
    $to     = ($to > $un_ap_user_list->total()) ? $un_ap_user_list->total() : $to;
	$allpage = $un_ap_user_list->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>
	<div class="row">
		<div class="col-md-4" style="margin-bottom: 10px;">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$un_ap_user_list->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($un_ap_user_list->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $un_ap_user_list->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($un_ap_user_list->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $un_ap_user_list->lastPage(),$un_ap_user_list->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($un_ap_user_list->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $un_ap_user_list->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th class="text-center" width="80px">No.</th>
					<th>Property Name</th>
					<th  class="text-center" width="120px">New User</th>
					<th  class="text-center" width="70px">View</th>
				</tr>
				</thead>
				<tbody>
				@foreach($un_ap_user_list as $key => $p)
				<tr>
					<td class="text-center">{{ $key + $from }}</td>
					<td>{{ $p->property_name_th }}</td>
					<td class="text-right">{{ $p->un_approved_user_count }}</td>
					<td class="action-links text-center">
						<a href="{{url('/admin/property/directlogin/'.$p->id.'?menu=new_user')}}" class="btn btn-red">
							<i class="fa-external-link-square"></i>
						</a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$un_ap_user_list->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($un_ap_user_list->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $un_ap_user_list->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($un_ap_user_list->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $un_ap_user_list->lastPage(),$un_ap_user_list->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($un_ap_user_list->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $un_ap_user_list->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
@else
	<div class="col-sm-12 text-center">{{ trans('messages.no_data') }}</div><div class="clearfix"></div>
@endif
