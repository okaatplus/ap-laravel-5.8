@if($new_messages->count() > 0)
    <?php
    $from   = (($new_messages->currentPage()-1)*$new_messages->perPage())+1;
    $to     = (($new_messages->currentPage()-1)*$new_messages->perPage())+$new_messages->perPage();
    $to     = ($to > $new_messages->total()) ? $new_messages->total() : $to;
	$allpage = $new_messages->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>
	<div class="row">
		<div class="col-md-4" style="margin-bottom: 10px;">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$new_messages->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($new_messages->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $new_messages->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($new_messages->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $new_messages->lastPage(),$new_messages->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($new_messages->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $new_messages->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th class="text-center" width="80px">No.</th>
					<th>Property Name</th>
					<th  class="text-center" width="160px">New Messages</th>
					<th  class="text-center" width="70px">View</th>
				</tr>
				</thead>
				<tbody>
				@foreach($new_messages as $key => $p)
				<tr>
					<td class="text-center">{{ $key + $from }}</td>
					<td>{{ $p->property_name_th }}</td>
					<td class="text-right">{{ number_format($p->un_read_messages_count) }}</td>
					<td class="action-links text-center">
						<a href="{{url('/admin/property/directlogin/'.$p->id.'?menu=chat')}}" class="btn btn-red">
							<i class="fa-external-link-square"></i>
						</a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$new_messages->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($new_messages->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $new_messages->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($new_messages->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $new_messages->lastPage(),$new_messages->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($new_messages->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $new_messages->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
@else
	<div class="col-sm-12 text-center">{{ trans('messages.no_data') }}</div><div class="clearfix"></div>
@endif
