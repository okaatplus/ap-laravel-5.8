@if($unregister_user_count->count() > 0)
    <?php
    $from   = (($unregister_user_count->currentPage()-1)*$unregister_user_count->perPage())+1;
    $to     = (($unregister_user_count->currentPage()-1)*$unregister_user_count->perPage())+$unregister_user_count->perPage();
    $to     = ($to > $unregister_user_count->total()) ? $unregister_user_count->total() : $to;
	$allpage = $unregister_user_count->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>
	<div class="row">
		<div class="col-md-4" style="margin-bottom: 10px;">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$unregister_user_count->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($unregister_user_count->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $unregister_user_count->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($unregister_user_count->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $unregister_user_count->lastPage(),$unregister_user_count->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($unregister_user_count->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $unregister_user_count->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th class="text-center" width="80px">No.</th>
					<th>Property Name</th>
					<th  class="text-center" width="260px">Waiting for approval User</th>
					<th  class="text-center" width="70px">View</th>
				</tr>
				</thead>
				<tbody>
				@foreach($unregister_user_count as $key => $p)
				<tr>
					<td class="text-center">{{ $key + $from }}</td>
					<td>{{ $p->property_name_th }}</td>
					<td class="text-right">{{ number_format($p->waiting_approval_user_count) }}</td>
					<td class="action-links text-center">
						<a href="{{url('/admin/property/directlogin/'.$p->id.'?menu=new_user')}}" class="btn btn-red">
							<i class="fa-external-link-square"></i>
						</a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$unregister_user_count->total()]) !!}
			</div>
		</div>
		<div class="col-md-8 text-right">
			@if($allpage > 1)
				@if($unregister_user_count->currentPage() > 1)
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $unregister_user_count->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
				@endif
				@if($unregister_user_count->lastPage() > 1)
					<?php echo Form::selectRange('page', 1, $unregister_user_count->lastPage(),$unregister_user_count->currentPage(),['class'=>'form-control paginate-select']); ?>
				@endif
				@if($unregister_user_count->hasMorePages())
					<a class="btn btn-white paginate-link" href="#" data-page="{{ $unregister_user_count->currentPage()+1 }}">{{ trans('messages.next') }}</a>
				@endif
			@endif
		</div>
	</div>
@else
	<div class="col-sm-12 text-center">{{ trans('messages.no_data') }}</div><div class="clearfix"></div>
@endif
