<div class="block-header" style="margin-bottom: 30px;">
    <h2>Waiting for approval accounts</h2>
</div>
<div id="uap-list">
    @include('dashboard.smart.unregister-users-list-element')
</div>
<script>
    function searchPage (page) {
        var data = 'page='+page;//$('#search-form').serialize()+'&page='+page;
        $('#uap-list').css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/smart/admin/dashboard/users/list",
            data    : data,
            dataType: "html",
            method: 'post',
            success: function (h) {
                $('#uap-list').html(h);
                $('#uap-list').css('opacity','1');
            }
        })
    }
</script>