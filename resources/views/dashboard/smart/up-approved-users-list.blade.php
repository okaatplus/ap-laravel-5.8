<div class="block-header" style="margin-bottom: 30px;">
    <h2> User accounts pending approval</h2>
</div>
<div id="uap-list">
    @include('dashboard.smart.un-approve-users-list-element')
</div>
<script>
    $(function () {

        $('#submit-search').on('click',function (){
            searchPage(1);
        });

        $('#search-form').on('keydown', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                searchPage (1);
            }
        });

        $('.reset-s-btn').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            searchPage (1);
        });

        $('body').on('click','.paginate-link', function (e){
            e.preventDefault();
            searchPage($(this).attr('data-page'));
        });

        $('body').on('change','.paginate-select', function (e){
            e.preventDefault();
            searchPage($(this).val());
        });

    });

    function searchPage (page) {
        var data = 'page='+page;//$('#search-form').serialize()+'&page='+page;
        $('#uap-list').css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/smart/admin/dashboard",
            data    : data,
            dataType: "html",
            method: 'post',
            success: function (h) {
                $('#uap-list').html(h);
                $('#uap-list').css('opacity','1');
                //$('[data-toggle="tooltip"]').tooltip();
                //cbr_replace();
            }
        })
    }
</script>