@if($discussions->count())
 <?php
    $from   = (($discussions->currentPage()-1)*$discussions->perPage())+1;
    $to     = (($discussions->currentPage()-1)*$discussions->perPage())+$discussions->perPage();
    $to     = ($to > $discussions->total()) ? $discussions->total() : $to;
 ?>
<table class="table mail-table">
     <thead>
    <tr>
        <th class="col-header-options">
            <div class="mail-pagination admin-discussion-pagination">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$discussions->total()]) !!}
                <div class="next-prev">
                   <a href="#" class="prev-page @if($discussions->currentPage() == 1) disabled @endif" data-page="{{($discussions->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                    <a href="#" class="next-page @if(!$discussions->hasMorePages()) disabled @endif" data-page="{{($discussions->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                </div>
            </div>
        </th>
    </tr>
    </thead>
</table>
@foreach($discussions as $discussion)
<div class="story-row">
    @if($discussion->owner->profile_pic_name)
    <img src="{{ env('URL_S3')."/profile-img/".$discussion->owner->profile_pic_path.$discussion->owner->profile_pic_name }}" alt="user-image" class="img-circle s-profile-img"/>
    @else
    <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle s-profile-img" />
    @endif
    <div class="row">
        <div class="col-sm-9">
            <div class="t-name">
                <a href="{{url('discussion/view')."/".$discussion->id }}">
                    <b>{{ $discussion->title }}</b>
                    @if( $discussion->attachment_count > 0 )
                     <i class="linecons-attach"></i>
                    @endif
                </a>
            </div>
        </div>
        <div class="col-sm-3 text-right">
            <div class="t-name">
                {{ localDateShort($discussion->created_at) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="t-owner-name">{{ $discussion->owner->name }}</div>
        </div>
        <div class="col-sm-8 text-right t-action">
            @if($discussion->user_id == Auth::user()->id)
            <a class="link-delete-discussion" data-did="{{ $discussion->id }}" data-target="#modal-delete-discussion" data-toggle="modal" href="#"><i class="fa-trash"></i></a>
            @else
            <a class="discussion-report" data-toggle="tooltip" data-placement="left" data-id="{{$discussion->id}}" title="" data-original-title="{{ trans('messages.Post.report')}}"><i class="fa-bullhorn "></i></a>
            @endif
        </div>
    </div>
</div>
@endforeach
<?php /*
<div class="table-responsive dataTables_wrapper" style="border:none;">
    <table class="table mail-table" style="table-layout:fixed;min-width:670px;">
         <!-- mail table header -->
        <tbody>
        @foreach($discussions as $discussion)
            <tr class="discussion-tr tr-{{$discussion->discussion_status}}">
                <td class="col-name" width="*">
                    @if($discussion->owner->profile_pic_name)
                    <img src="{{ env('URL_S3')."/profile-img/".$discussion->owner->profile_pic_path.$discussion->owner->profile_pic_name }}" alt="user-image" class="img-circle mini-profile-img" />
                    @else
                    <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle mini-profile-img" />
                    @endif
                    <a href="{{url('discussion/view')."/".$discussion->id }}"><b>{{ $discussion->owner->name }}</b></a>
                </td>
                <td class="col-subject" width="38%">
                    <a href="{{url('discussion/view')."/".$discussion->id }}">
                        {{ $discussion->title }}
                    </a>
                </td>
                <td class="col-options hidden-sm hidden-xs">
                    @if( $discussion->attachment_count > 0 )
                    <i class="linecons-attach"></i>
                    @endif
                </td>
                <td style="width:23%;text-align:right;">{{ localDateShort($discussion->created_at) }}</td>
                <td width="5%">
                    @if($discussion->user_id == Auth::user()->id)
                    <a class="link-delete-discussion" data-did="{{ $discussion->id }}" data-target="#modal-delete-discussion" data-toggle="modal" href="#"><i class="fa-trash"></i></a>
                    @else
                    <a class="discussion-report" data-toggle="tooltip" data-placement="left" data-id="{{$discussion->id}}" title="" data-original-title="{{ trans('messages.Post.report')}}"><i class="fa-bullhorn "></i></a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
*/ ?>
<table class="table mail-table">
         <!-- mail table footer -->
        <thead>
        <tr>
            <th class="col-header-options" style="border-bottom:0px;">
                <div class="mail-pagination">
                    {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$discussions->total()]) !!}
                    <div class="next-prev">
                        <a href="#" class="prev-page @if($discussions->currentPage() == 1) disabled @endif" data-page="{{($discussions->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                        <a href="#" class="next-page @if(!$discussions->hasMorePages()) disabled @endif" data-page="{{($discussions->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                    </div>
                </div>
            </th>
        </tr>
        </thead>
    </table>
    <div class="not-found-status col-sm-12 text-center hide">{{ trans('messages.Discussion.discussion_not_cound') }}</div><div class="clearfix"></div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Discussion.have_no_discussion') }}</div><div class="clearfix"></div>
@endif
