@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
    	<div class="title-env">
    		<h1 class="title">{{ trans('messages.Discussion.page_head') }}</h1>
    	</div>
		<div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
				<li>
				    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
				<li class="active">
					<strong>{{ trans('messages.Discussion.page_head') }}</strong>
				</li>
			</ol>
    	</div>
    </div>
    <a href="javascript:" data-toggle="modal" data-target="#modal-discussion-add" type="button" class="action-float-right btn btn-primary bg-lg"><i class="fa fa-comment"></i> {{ trans('messages.Discussion.create_discussion') }}</a>
    <section class="mailbox-env">
        <div class="row">
            <!-- Inbox emails -->
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">{{ trans('messages.Discussion.page_sub_head') }}</h3></div>
                    <div class="panel-body" style="padding-top:0;">
                        <div class="mail-env" id="discussion-list">
                            <!-- mail table -->
                            @include('discussion.discussion-list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-discussion-add" data-backdrop="static">
        <div class="modal-dialog">
            {!! Form::open(array('url'=>'discussion/add','class'=>'form-horizontal','id'=>'add-discussion-form')) !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="linecons-pencil"></i> {{ trans('messages.Discussion.page_head') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    {!! Form::text('title',null,array('class'=>'form-control','placeholder'=> trans('messages.title'),'maxlength' => 200)) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    {!! Form::textarea('detail',null,array('class'=>'form-control','placeholder'=> trans('messages.detail'),'maxlength' => 2000)) !!}
                                    <br/><span id="attachment"><i class="fa fa-camera"></i> {{ trans('messages.attach') }}</span>
                                    <div class=" field-hint">{{ trans('messages.upload_file_description') }}</div>
                                    <div id="previews"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                    <button type="button" class="btn btn-primary" id="add-discussion">{{ trans('messages.Discussion.sent_discussion') }}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="modal fade" id="modal-delete-discussion" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-trash"></i> {{ trans('messages.Discussion.confirm_delete_head') }}</h4>
                </div>
                <form method="post" action="{{url('discussion/delete')}}" role="form" id="delete-discussion-form">
                <input type="hidden" name="d-id" id="d-id-hidden"/>
                    <div class="modal-body"> {{ trans('messages.Discussion.confirm_delete_msg') }} </div>
                    <div class="modal-footer">
                        <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                        <button type="botton" id="confirm-delete-discussion" class="btn btn-primary">{{ trans('messages.delete') }}</button>
                    </div>
                </form>
            </div>
         </div>
    </div>
    <div class="modal fade" id="modal-report-discussion" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.report')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="discussion-report-form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-12" for="reason">{{trans('messages.Post.report_reason_label')}}</label>
                                    <div class="col-sm-12">
                                        <textarea id="input-report" name="reason" class="form-control"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.cancel')}}</button>
                    <button type="button" class="btn btn-primary" id="report-discussion-btn">{{trans('messages.Post.report_btn')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-report-discussion-result">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.report')}}</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/upload-file.js"></script>

<script type="text/javascript">
var target_tmp,target_element;
    $(window).load(function () {
        var root = $('#root-url').val();
        $("#c-cate").selectBoxIt().on('open', function(){
            $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
        });
        $("#add-discussion-form").validate({
            rules: {
                title : 'required',
                detail: 'required',
                discussion_category_id : 'required'
            },
            errorPlacement: function(error, element) {}
        });

        var validator = $("#discussion-report-form").validate({
           rules: {
             reason: "required"
           },
           messages: {
             reason: "{{trans('messages.Post.report_validate')}}"

           }
        })

        $('#add-discussion').on('click', function () {
            if( $("#add-discussion-form").valid() ) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#add-discussion-form').submit();
            } else {
                $(window).resize();
            }
        })

        $('.mailbox-list li a').on('click',function (e){
            e.preventDefault();
            if( !$(this).parent().hasClass('active') ) {
                $('.mailbox-list li').removeClass('active');
                $(this).parent().addClass('active');
                var group = $(this).data('discussion-group');
                getAdminComplainPage(1,group);
            }
        })

        $('.mail-env').on('click','.next-page,.prev-page', function (e){
            e.preventDefault();
            if(!$(this).hasClass('disabled')) {
                var page = $(this).data('page');
                var type = $('#admin-notification-type-list li.active a').attr('data-discussion-group');
                getAdminComplainPage (page,type);
            }
        });

        $('#confirm-delete-discussion').on('click',function () {
            $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            $('#delete-discussion-form').submit();
        });

        $('#discussion-list').on('click', '.link-delete-discussion', function () {
            $('#d-id-hidden').val($(this).data('did'));
        });

        $('#discussion-list').on('click','.discussion-report', function (){
            target_tmp      = $(this).data('id');
            target_element  = $(this).children('i');
            checkReport ();
        });

        $('#report-discussion-btn').on('click',function () {
            if($("#discussion-report-form").valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : root+"/report",
                    data    : 'id='+target_tmp+"&reason="+$('#input-report').val()+'&type=4',
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        $('#modal-report-discussion').modal('hide');
                        $('#modal-report-discussion-result .modal-body').html(r.msg);
                        $('#modal-report-discussion-result').modal('show');
                        $('#input-report').val('');
                        $('#report-discussion-btn').removeAttr('disabled').children('.fa-spinner').remove();
                    }
                });
            }

        })


        function getAdminComplainPage (page,type) {
            $('#discussion-list').css('opacity','0.7');
        	$.ajax({
                	url 	: $('#root-url').val()+"/discussion",
                	dataType: 'html',
                	method	: 'POST',
                	data 	: ({page:page,type:type}),
                	success	: function (t) {
                		$('#discussion-list').html(t);
                        $('#discussion-list').css('opacity','1');
                	}
                });
        }

        function checkReport () {
            var loader = $('<i class="fa-spin fa-spinner"></i>');
            target_element.after(loader);
            target_element.hide();
            $.ajax({
                    url     : root+"/report-check",
                    data    : 'id='+target_tmp+'&type=4',
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        if(r.status) {
                            validator.resetForm();
                            $("label.error").hide();
                            $(".error").removeClass("error");
                            $('#modal-report-discussion').modal('show');
                        } else {
                            $('#modal-report-discussion-result .modal-body').html(r.msg);
                            $('#modal-report-discussion-result').modal('show');
                        }
                        loader.remove();
                        target_element.show();
                    }
                });
        }
    });
</script>
<link rel="stylesheet" href="{{url('/')}}/js/dropzone/css/dropzone.css">
@endsection
