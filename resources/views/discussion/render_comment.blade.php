@foreach($comments as $comment)
<?php $comment_owner = ($comment->user_id == Auth::user()->id); ?>
<li>
    <div class="story-comment">
        <a href="#" class="comment-user-img">
             @if($comment->owner->profile_pic_name)
            <img src="{{ env('URL_S3')."/profile-img/".$comment->owner->profile_pic_path.$comment->owner->profile_pic_name }}" alt="user-image" class="img-responsive img-circle" />
            @else
            <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-responsive img-circle" />
            @endif
        </a>
        <div class="story-comment-content">
            <span class="story-comment-user-name">
                <b>{{ $comment->owner->name }}</b>
                <time>{{humanTiming($comment->created_at)}}</time>
                @if($comment->is_reject)
                <span class="reject" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.Complain.reject_reasons_label') }}"></span>
                @endif
            </span>
            <p>{{ e($comment->description) }}</p>
        </div>
    </div>
</li>
@endforeach