@extends('layouts.base-admin')
@section('content')
    <section class="mailbox-env">
        <div class="row">
            <!-- Email Single -->
            <div class="col-sm-12 mailbox-right">
                <div class="mail-single">
                    <!-- Email Title and Button Options -->
                    <div class="mail-single-header">
                        <h2>
                            {{ e($discussion->title) }}
                            <a href="{{url('discussion')}}" class="go-back">
                                <i class="fa-angle-left"></i> {{ trans('messages.back') }}
                            </a>
                        </h2>
                    </div>

                   <!-- Email Info From/Reply -->
                    <div class="mail-single-info">
                        <div class="mail-single-info-user dropdown">
                            <div class="dropdown-toggle" data-toggle="dropdown">
                                @if($discussion->owner->profile_pic_name)
                                <img src="{{ env('URL_S3') . "/profile-img/" . $discussion->owner->profile_pic_path . $discussion->owner->profile_pic_name }}" alt="user-image" class="img-circle" width="38" />
                                @else
                                <img src="{{ url('/') }}/images/user-3.png" class="img-circle" width="38" />
                                @endif
                                <span>{{$discussion->owner->name}}</span>
                                <em class="time">{{ localDateShort($discussion->created_at) }}</em>
                            </div>
                        </div>
                    </div>

                    <!-- Email Body -->
                    <div class="mail-single-body">
                        <p><?php echo nl2br(e($discussion->detail)) ?></p>
                    </div>

                    <!-- Email Attachments -->
                    @if(!$discussion->discussionFile->isEmpty())
                    <div class="mail-single-attachments">
                        <h3>
                            <i class="linecons-attach"></i>
                            {{ trans('messages.attachment') }}
                        </h3>
                        <ul class="list-unstyled list-inline">

                            @foreach($discussion->discussionFile as $file)
                            <li>
                                @if($file->is_image)
                                <a href="{{env('URL_S3')}}/discussion-file/{{$file->url.$file->name}}" class="thumb gallery" rel="gal-1">
                                    <img width="145px" src="{{env('URL_S3')}}/discussion-file/{{$file->url.$file->name}}" class="img-thumbnail" />
                                </a>
                                @else
                                <div>
                                    <a target="_blank" href="{{ url("/discussion/get-attachment/".$file->id) }}" class="thumb">
                                        <img src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                                    </a>
                                    <div class="file-name">{{ $file->original_name }}</div>
                                </div>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <scetion class="profile-env">
        <scetion class="user-timeline-stories steam-post">
            <article class="timeline-story">
                <h3>
                    <i class="fa fa-comment"></i>
                    {{ trans('messages.Post.comments') }}
                </h3>
                <ul class="list-unstyled story-comments discussion-comment">
                    <?php $comments =  $discussion->comments; ?>
                    @include('discussion.render_comment')
                </ul>
                <form method="post" action="" class="story-comment-form">
                    <textarea class="form-control input-unstyled autogrow" data-did="{{ $discussion->id }}" name="comment" placeholder="{{ trans('messages.Post.comments') }}..." style="overflow: hidden; word-wrap: break-word; resize: none; height: 62px;"  maxlength="2000"></textarea>
                    <button type="button" class="add-discussion-comment btn btn-primary"><i class="fa fa-comment"></i> {{ trans('messages.Post.comment') }}</button>
                </form>
            </article>
        </section>
    </section>
    @if($discussion->discussion_status == 2)
    <div class="modal fade" id="change-status-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Complain.confirm_change_head') }}</h4>
                </div>
                <form method="post" action="{{url('/discussion/status')}}" id="user-change-c-status-form">
                <div class="modal-body">
                    {{ trans('messages.Complain.confirm_change_msg') }} "<span></span>"
                    <div class="reject-reason-block">
                        <label style="margin-top:15px;">ระบุเหตุผลเพื่อยืนยันว่าไม่เรียบร้อย</label>
                        <textarea name="comment" class="form-control"></textarea>
                    </div>
                </div>
                <input type="hidden" name="status" id="c-status"/>
                <input type="hidden" name="cid" value="{{$discussion->id}}"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary submit-status">{{ trans('messages.confirm') }}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endif
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
<script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
    $(function () {
        $(".gallery").fancybox({
            helpers: {
                overlay: {
                  locked: false
                }
            },
            beforeLoad: function(){
                    $('html').css('overflow','hidden');
            },
            afterClose: function(){
               $('html').css('overflow','auto');
            },
            'padding'           : 0,
            'transitionIn'      : 'elastic',
            'transitionOut'     : 'elastic',
            'changeFade'        : 0
        });

        $('#user-change-c-status-form').validate({
            rules: {
                comment : { required: function () {
                        if($('#c-status').val() == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            errorPlacement: function(error, element) {}
        })

        $('.add-discussion-comment').on('click', function () {
            if( $(this).prev().val() != "" ) {
                $(this).attr('disabled','disabled').children('.fa').remove();
                $(this).prepend('<i class="fa-spin fa-spinner"></i> ');
                var target = $(this);
                var did = $(this).prev().data('did');
                var data = target.parent().serialize();
                    data+= "&did="+ did;
                var request = $.ajax({
                url:  $('#root-url').val()+"/discussion/comment/add",
                    method: "POST",
                    data: data,
                    dataType: "json"
                });
                request.done(function( result ) {
                    if(result.status == true) {
                        target.parent().prev().html(result.content);
                    }
                    target.prev().val('');
                    target.removeAttr('disabled').children('.fa-spin').remove();
                    target.prepend('<i class="fa fa-comment"></i> ');
                });
            }
        })

        $('.submit-status').on('click',function () {
            if($('#user-change-c-status-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $(this).parents('form').submit();
            }

        })
    });
</script>
<link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
