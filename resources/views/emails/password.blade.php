@extends('layouts.email')
@section('content')
{{ trans('messages.Email.reset_password_msg') }}
<br/><br/>
<center><a style=" background: #FFD800;color: #000;padding:10px 50px;text-align:center;display:inline-block;text-decoration:none;" href="{{ url('password/reset/'.$token) }}"> {{ trans('messages.Email.reset_password_link') }} </a></center>
<br/>
{{ trans('messages.Email.regards') }}
<br/>
{{ trans('messages.Email.ap_team') }}
@endsection
