@extends('layouts.base-admin')
@section('content')
    <section class="profile-env">
        <div class="row">
            <div class="col-sm-12">
                <section class="user-timeline-stories steam-post">
                    <article class="timeline-story">
                        <div class="row">
                            <div class="col-sm-12">
                                    <div class="content" style="text-align: center">
                                        {{--<a href="{{ url('/') }}" class="logo-expanded">
                                            <img src="{{ url('/') }}/images/logo.png" width="160" alt="" />
                                        </a>--}}
                                        <div class="title"></div>
                                        <h1><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ trans('messages.Error.error_title') }}</h1>
                                        <p>{{ trans('messages.Error.error_detail') }}</p>
                                        <h3><a href="javascript:window.history.back();"><i class="fa fa-angle-left"></i> {{ trans('messages.Error.error_text_button') }}</a></h3>
                                    </div>
                            </div>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="{{url('/')}}/collagePlus/jquery.collagePlus.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/collagePlus/jquery.removeWhitespace.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/collagePlus/jquery.collageCaption.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>

@endsection
