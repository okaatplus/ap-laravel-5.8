<!DOCTYPE html>
<html>
    <head>
        <title>Oops.</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #217b6d;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                letter-spacing: 1px;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }

            a {
                color: #217b6d;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <a href="{{ url('/') }}" class="logo-expanded">
                    <img src="{{ url('/') }}/images/logo.png" width="160" alt="" />
                </a>
                <div class="title">Oops!</div>
                {{--<h2>Error404 : Page Not Found</h2>
                <h3>Looks like something went completely wrong!</h3>--}}
                <h2>Error500 : Internal Server Error</h2>
                <h3>Looks like something went completely wrong!</h3>
            </div>
        </div>
    </body>
</html>
