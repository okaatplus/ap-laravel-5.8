@if($event)
<?php
	$d = explode(' ',$event->start_date_time);
	$sd = $d[0];
	$st = $d[1];

	$d = explode(' ',$event->end_date_time);
	$ed = $d[0];
	$et = $d[1];

    $event->start_time  = date("g:i A", strtotime($st));
    $event->end_time    = date("g:i A", strtotime($et));
    $time = time();
?>
<div class="row">
	<div class="col-md-12">
		<div class="detail">{!! nl2br(e($event->description)) !!}</div>
		<div class="event-gallery">
			@foreach($event->eventFile as $img)
			<a class="fancybox event-gallery-item" rel="gal-{{$event->id}}" href="{{ env('URL_S3') }}/event-file/{{$img->url.$img->name}}">
                <img src="{{ env('URL_S3') }}/event-file/{{$img->url.$img->name}}" alt="album-image" />
            </a>
			@endforeach
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<p style="margin-top:10px;">
		<i class="fa fa-user"></i> {{ $event->creator->name }}<br/>
		<i class="fa fa-clock-o"></i>
		@if(strtotime($sd) == strtotime($ed))
		{{ localDateShortNotime( $sd ) . " " . $event->start_time . " - " . $event->end_time}}
		@else
		{{ localDateShortNotime( $sd ) . " " . $event->start_time . " - " . localDateShortNotime( $ed )  . " " . $event->end_time}}
		@endif
		<br/><i class="fa fa-map-marker"></i> {{ $event->location }}
		</p>
	</div>
</div>
@if( strtotime( $event->start_date_time) > $time )
<div class="row">
	<div class="col-md-10 col-md-offset-2">
		<div class="btn-group btn-group-justified" data-event-id="{{ $event->id }}">
			<a type="button" class="btn btn-success btn-sm event-action" @if($event->confirmation && $event->confirmation->confirm_status == 1) disabled @endif data-status="1">
				@if($event->confirmation && $event->confirmation->confirm_status == 1)
				<i class="fa-check"></i>
				@endif
				{{ trans('messages.Event.join') }} [{{$count_go}}]
			</a>
			<a type="button" class="btn btn-warning btn-sm event-action" @if($event->confirmation && $event->confirmation->confirm_status == 2) disabled @endif data-status="2">
				@if($event->confirmation && $event->confirmation->confirm_status == 2)
				<i class="fa-check"></i>
				@endif
				{{ trans('messages.Event.maybe') }} [{{$count_maybe}}]
			</a>
			<a type="button" class="btn btn-black btn-sm event-action" @if($event->confirmation && $event->confirmation->confirm_status == 3) disabled @endif data-status="3">
				@if($event->confirmation && $event->confirmation->confirm_status == 3)
				<i class="fa-check"></i>
				@endif
				{{ trans('messages.Event.notgo') }} [{{$count_not_go}}]
			</a>
		</div>
	</div>
</div>
@else
<div class="row">
	<div class="col-md-12">
		<h5>{{ trans('messages.Vote.sum_interaction') }}</h5>
		<table class="table table-bordered table-striped table-condensed table-hover">
			<tr><th>{{ trans('messages.Event.join') }}</th><td>{{$count_go}}</td></tr>
			<tr><th>{{ trans('messages.Event.maybe') }}</th><td>{{$count_maybe}}</td></tr>
			<tr><th>{{ trans('messages.Event.notgo') }}</th><td>{{$count_not_go}}</td></tr>
		</table>
	</div>
</div>
@endif
@endif
