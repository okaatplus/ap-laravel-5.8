{!! Form::model($event,array('url' => array('events/edit'),'id'=>'edit-event-form')) !!}
{!! Form::hidden('id') !!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::text('title', null, ["class" => "form-control", "placeholder" => trans('messages.title'), "maxlength" => "200"]) !!}
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label text-bold">{{ trans('messages.Event.start_date') }}</label>
                <div class="date-and-time">
                    <?php
                        if($event->start_date_time) $sd = date('Y/m/d',strtotime($event->start_date_time));
                        else $sd = date('Y/m/d');
                    ?>
                    {!! Form::text('start_date', $sd, ["class" => "form-control datepicker", "readonly", "data-start-date" => "0d", "data-format" => "yyyy/mm/dd",'data-language'=>App::getLocale()]) !!}

                    <?php
                        $st = date('h:i A',strtotime($event->start_date_time));
                    ?>
                    {!! Form::text('start_time', $st, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5"]) !!}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label text-bold">{{ trans('messages.Event.end_date') }}</label>
                <div class="date-and-time">

                    <?php
                        if($event->end_date_time) $ed = date('Y/m/d',strtotime($event->end_date_time));
                        else $ed = date('Y/m/d');
                    ?>
                    {!! Form::text('end_date', $ed, ["class" => "form-control datepicker", "readonly", "data-start-date" => "0d", "data-format" => "yyyy/mm/dd",'data-language'=>App::getLocale()]) !!}

                    <?php
                        $et = date('h:i A',strtotime($event->end_date_time));
                    ?>
                    {!! Form::text('end_time', $et, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5"]) !!}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                 {!! Form::text('location', null, ["class" => "form-control", "placeholder" => trans('messages.Event.location'), "maxlength" => "100"]) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::textarea('description',null,["class" => "form-control", "data-html" => "false", "data-color" => "false", "data-stylesheet-url" => url('/')."/css/other/wysihtml5-color.css", "placeholder" => trans('messages.detail'), "maxlength" => "2000", "rows" => 10]) !!}
                <br/>
                <ul class="list-unstyled list-inline form-action-buttons">
                    <li>
                        <button type="button" id="attachment_edit" class="btn btn-unstyled upload">
                            <i class="fa fa-camera"></i> {{ trans('messages.attach') }}
                        </button>
                    </li>
                    <li>
                        <div class="field-hint">{{ trans('messages.upload_image_description') }}</div>
                    </li>
                </ul>
            </div>
            <div id="previews-edit-form">
                @foreach($event->eventFile as $file)
               <div class="preview-img-item">
                    @if($file->is_image)
                     <img width="80px" src="{{env('URL_S3')}}/event-file/{{$file->url.$file->name}}"/>
                    @else
                    <img width="80px" src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                    <span class="file-label">{{ $file->original_name }}</span>
                    @endif
                    <span class="remove-img-preview remove-element" data-e-type="event-file" data-e-id="{{ $file->id }}">×</span>
                </div>
               @endforeach
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
    <button type="button" class="btn btn-primary btn-icon pull-righ" id="save-event-btn">{{ trans('messages.save') }}</button>
</div>
<div class="delete-zone"></div>
{!! Form::close(); !!}
<script type="text/javascript" src="{{url('/')}}/js/xenon-custom.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/edit-upload-file.js"></script>
<script type="text/javascript">
    $(function () {
        $("#edit-event-form").validate({
            rules: {
                title      : "required",
                location   : "required",
                description: "required"
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('error');
                $( window ).resize();
            },
            errorPlacement: function(error, element) {}
        })

        $('.remove-element').on('click', function () {
            var model = $(this).data('e-type');
            var id = $(this).data('e-id');
            $('.delete-zone').append('<input type="hidden" name="remove['+model+'][]" value="'+id+'" />');
            $(this).parent().remove();
        });

        $('#save-event-btn').on('click', function () {
            if( $("#edit-event-form").valid() ) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#edit-event-form').submit();
            }
        })
    })
</script>
