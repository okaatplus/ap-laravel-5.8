@if( ( $events->count() > 0 ) )
    @foreach($events as $event)
    <?php

        $allowJoin = greaterThanNow_($event->end_date_time);
        //$sd = $event->start_date;
        //$st = $event->start_time;
        $d = explode(' ',$event->start_date_time);
        $sd = $d[0];
        $st = $d[1];

        $d = explode(' ',$event->end_date_time);
        $ed = $d[0];
        $et = $d[1];

        $event->start_time  = date("g:i A", strtotime($st));
        $event->end_time    = date("g:i A", strtotime($et));
        $time = time();
        if($allowJoin) :
            if($event->confirmation):
                if( $event->confirmation->confirm_status == 1 ) :
                    $status_text    = trans('messages.Event.join');
                    $status_class   = "timeline-bg-success";
                elseif( $event->confirmation->confirm_status == 2 ) :
                    $status_text    = trans('messages.Event.maybe');
                    $status_class   = "timeline-bg-info";
                else:
                    $status_text    = trans('messages.Event.notgo');
                    $status_class   = "timeline-bg-primary";
                endif;
            else :
                $status_text = trans('messages.new');
                $status_class   = "timeline-bg-warning";
            endif;
        else:
            $status_text    = trans('messages.Event.end');
            $status_class   = "timeline-bg-primary";
        endif;
    ?>
    <li id="event-{{ $event->id }}" data-eid="{{ $event->id }}">
        <time class="cbp_tmtime"><span>{{ $event->start_time }}</span> <span>{{ date('d/m/Y', strtotime($sd)) }}</span></time>
        <div class="cbp_tmicon {{ $status_class }}">
            <i class="fa-calendar"></i>
        </div>
        <div class="cbp_tmlabel event-block-detail">
            <h2><a href="#" class="view-event" data-event-id="{{ $event->id }}">{{ $event->title }} [<span>{{ $status_text }}</span>] </a></h2>
            @if($event->user_id == Auth::user()->id)
            @if( greaterThanNow ($sd,$st) )
            <a class="event-action-cc link-edit-event" data-id="{{$event->id}}" href="#"><i class="fa-pencil"></i></a>
            @endif
            <a class="event-action-cc link-delete-event" data-target="#modal-event-delete" data-toggle="modal" href="#"><i class="fa-trash"></i></a>
            @else
            <a class="event-action-cc event-report" data-toggle="tooltip" data-placement="left" data-id="{{$event->id}}" title="" data-original-title="{{ trans('messages.Post.report')}}"><i class="fa-bullhorn "></i></a>
            @endif
            <blockquote>
                <i class="fa fa-user"></i> {{ $event->creator->name }}<br/>
                <i class="fa fa-clock-o"></i>
                @if(strtotime($sd) == strtotime($ed))
                {{ localDateShortNotime( $sd ) . " " . $event->start_time . " - " . $event->end_time}}
                @else
                {{ localDateShortNotime( $sd ) . " " . $event->start_time . " - " . localDateShortNotime( $ed )  . " " . $event->end_time}}
                @endif
                <br/><i class="fa fa-map-marker"></i> {{ $event->location }}
            </blockquote>
            <p>{{ str_limit($event->description, 400, $end = '...') }}</p>
        </div>
    </li>
    @endforeach
@else
<li>
    <time class="cbp_tmtime"><span>&nbsp;</span> <span>&nbsp;</span></time>
    <div class="cbp_tmicon timeline-bg-gray">
        <i class="fa-ban"></i>
    </div>
    <div class="cbp_tmlabel event-block-detail">
        <div class="col-sm-12 text-center">{{ trans('messages.Event.event_not_found')}}</div>
        <div class="clearfix"></div>
    </div>
</li>
@endif
