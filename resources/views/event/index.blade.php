<?php $allow_post_event = (Auth::user()->property->allow_user_add_event || Auth::user()->role == 1 || Auth::user()->role == 3); ?>
@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Event.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{url('/')}}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Event.page_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>
    <section class="mailbox-env">

            <div class="row">

                <!-- Inbox emails -->
                <div class="col-sm-9 mailbox-right">
                    @if( $allow_post_event )
                    <div class="text-right">
                        <a href="#" data-toggle="modal" data-target="#create-event" type="button" class="btn btn-primary bg-lg">
                            <i class="fa fa-calendar"></i> {{ trans('messages.Event.create_event') }}</a>
                    </div>
                    @endif
                    <ul class="cbp_tmtimeline" id="event-list-content" style="margin-top:7px;">
                        @include('event.event-list')
                    </ul>
                    <div id="loadmoreajaxloader" data-page="2" style="display:none;"><center><p>{{ trans('messages.loading') }}...</p></center></div>
                </div>

                <!-- Mailbox Sidebar -->
                <div class="col-sm-3 mailbox-left">
                    <div class="mailbox-sidebar">
                        <ul class="list-unstyled mailbox-list event-type-list">
                            <li class="active">
                                <a href="#"  data-type="new">
                                    {{ trans('messages.Event.upcoming') }}
                                    @if($events->count() > 0)<span class="badge badge-green pull-right">{{$events->count()}}</span> @endif
                                </a>
                            </li>
                             <li>
                                <a href="#" data-type="1">
                                    {{ trans('messages.Event.joined') }}
                                </a>
                            </li>
                            <li>
                                <a href="#" data-type="my">
                                    {{ trans('messages.Event.created') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        @if( $allow_post_event )
        <div class="modal fade" id="create-event" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Event.create_event') }}</h4>
                </div>
                <form method="post" id="create-event-form" action="{{url('events/add')}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="title" placeholder="{{ trans('messages.title') }}" maxlength="200">
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label text-bold">{{ trans('messages.Event.start_date') }}</label>
                                <div class="date-and-time">
                                    <input type="text" class="form-control datepicker" name="start_date" placeholder="Date" data-format="yyyy/mm/dd" value="{{date('Y/m/d')}}" readonly data-start-date="0d" data-language="{{ App::getLocale() }}">
                                    <input type="text" class="form-control timepicker" name="start_time" placeholder="Time" data-template="dropdown" data-default-time="{{ date('g:i A')}}" data-show-meridian="true" data-minute-step="1" data-second-step="5" readonly/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label text-bold">{{ trans('messages.Event.end_date') }}</label>
                                <div class="date-and-time">
                                    <input type="text" class="form-control datepicker" name="end_date" placeholder="Date" data-format="yyyy/mm/dd" value="{{date('Y/m/d')}}" readonly data-start-date="0d" data-language="{{ App::getLocale() }}">
                                    <input type="text" class="form-control timepicker" name="end_time" placeholder="Time" data-template="dropdown" data-default-time="{{ date('g:i A')}}" data-show-meridian="true" data-minute-step="1" data-second-step="5" readonly data-start-date="0d"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="location" placeholder="{{ trans('messages.Event.location') }}" maxlength="100">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" name="description" id="field-7" placeholder="{{ trans('messages.Event.detail') }}"  maxlength="2000" rows="10"></textarea>
                                <br/>
                                <ul class="list-unstyled list-inline form-action-buttons">
                                    <li>
                                        <button type="button" id="attachment" class="btn btn-unstyled upload">
                                            <i class="fa fa-camera"></i> {{ trans('messages.attach') }}
                                        </button>
                                    </li>
                                    <li>
                                        <div class="field-hint">{{ trans('messages.upload_image_description') }}</div>
                                    </li>
                                </ul>
                            </div>
                            <div id="previews"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="add-event-btn">{{ trans('messages.Event.create_event') }}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    <div class="modal fade" id="event-detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-event-delete" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-trash"></i> {{ trans('messages.Event.confirm_delete_head') }}</h4>
                </div>
                <div class="modal-body"> {{ trans('messages.Event.confirm_delete_msg') }} </div>
                <div class="modal-footer">
                    <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="botton" id="confirm-delete-event" class="btn btn-primary">{{ trans('messages.delete') }}</button>
                </div>
            </div>
         </div>
    </div>

    <div class="modal fade" id="modal-report-event" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.report')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="event-report-form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-12" for="reason">{{trans('messages.Post.report_reason_label')}}</label>
                                    <div class="col-sm-12">
                                        <textarea id="input-report" name="reason" class="form-control"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.cancel')}}</button>
                    <button type="button" class="btn btn-primary" id="report-event-btn">{{trans('messages.Post.report_btn')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-report-event-result">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.report')}}</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-event-edit" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Event.edit_event') }} </h4>
                </div>
                <div class="landing-edit-form">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.th.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/app-scripts/upload-image.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript">
    $(function (){
        var root = $('#root-url').val();
        var target_temp,target_element;
        makeUploadZone('.upload');
        $("#event-report-form").validate({
               rules: {
                 reason: "required"
               },
               messages: {
                 reason: "{{trans('messages.Post.report_validate')}}"

               }
            })
        $('.event-type-list li a').on('click', function (e){
            e.preventDefault();
            if(!$(this).parent().hasClass('active')) {
                $('.cbp_tmtimeline').css('opacity','0.6');
                $('.event-type-list li').removeClass('active');
                $(this).parent().addClass('active');

                var request = $.ajax({
                url:  root+"/events/filter",
                    method: "POST",
                    data: ({type:$(this).data('type')}),
                    dataType: "html"
                });
                request.done(function( result ) {
                    $('.cbp_tmtimeline li').remove();
                    $('.cbp_tmtimeline').append(result);
                    $('.cbp_tmtimeline').css('opacity','1');
                });
            }
        })

        validator = $("#create-event-form").validate({
               rules: {
                    title      : "required",
                    location   : "required",
                    description: "required"
               },
               /*messages: {
                    title: "{{ trans('messages.Event.validate_title') }}",
                    location   : "{{ trans('messages.Event.validate_location') }}",
                    description: "{{ trans('messages.Event.validate_desc') }}"
                },*/
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('error');
                    $( window ).resize();
                },
                errorPlacement: function(error, element) {}
            });

        $('#add-event-btn').on('click', function () {
            if( $("#create-event-form").valid() ) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $("#create-event-form").submit();
            }
        })

        $('.cbp_tmtimeline').on('click','.view-event', function (e){
            e.preventDefault();
            if(!$(this).hasClass('busy')) {
                $(this).addClass('busy');
                var $this = $(this);
                $this.prepend('<i class="fa-spin fa-spinner"></i> ');
                var request = $.ajax({
                    url:  root+"/events/view",
                        method: "POST",
                        data: ({eid:$(this).data('event-id')}),
                        dataType: "json"
                    });
                request.done(function( result ) {
                    if(result.status) {
                        $('#event-detail .modal-body').html(result.detail);
                        $('#event-detail h4').html(result.head);
                        $('#event-detail').modal('show');
                        $(".fancybox").fancybox({
                            helpers: {
                                overlay: {
                                  locked: false
                                }
                            },
                            'padding'           : 0,
                            'transitionIn'      : 'elastic',
                            'transitionOut'     : 'elastic',
                            'changeFade'        : 0
                        });
                        $this.find('.fa-spin').remove();
                        $this.removeClass('busy');
                    }
                });
            }
        })

        $('body').on('click','.event-action', function (e){
            $('.event-action').attr('disabled','disabled').children('i').remove();
            $btn = $(this);
            $tmp_text = $btn.html();
            $btn.html('loading');
            var eid = $(this).parent().data('event-id');
            e.preventDefault();
            var request = $.ajax({
                url:  root+"/events/confirm",
                    method: "POST",
                    data: ({'eid':eid,'status':$(this).data('status')}),
                    dataType: "json"
                });
            request.done(function( result ) {
                if(result.status) {
                    $btn.html($tmp_text);
                    $('.event-action').removeAttr('disabled');
                    $btn.attr('disabled','disabled').prepend('<i class="fa-check"></i>');
                    $('#event-detail').modal('hide');
                    if(result.confirm == 1) {
                        $('#event-' + eid).find('.cbp_tmicon').removeAttr('class').addClass('cbp_tmicon timeline-bg-success');
                        $('#event-' + eid).find('.view-event span').html('{{ trans('messages.Event.join') }}')
                    } else if(result.confirm == 2) {
                        $('#event-' + eid).find('.cbp_tmicon').removeAttr('class').addClass('cbp_tmicon timeline-bg-info');
                        $('#event-' + eid).find('.view-event span').html('{{ trans('messages.Event.maybe') }}')
                    } else {
                        $('#event-' + eid).find('.cbp_tmicon').removeAttr('class').addClass('cbp_tmicon timeline-bg-primary');
                        $('#event-' + eid).find('.view-event span').html('{{ trans('messages.Event.notgo') }}')
                    }
                }
            });
        });

        $(window).scroll(function(){
            if($(window).scrollTop() == $(document).height() - $(window).height()){
                var loader = $('div#loadmoreajaxloader');
                if(!loader.hasClass('loading') && $('.event-type-list li.active a').data('type') == "all") {
                    loader.show().addClass('loading');
                    var page = loader.attr('data-page');
                    $.ajax({
                        url: root+"/events/filter",
                        data: 'page='+page+'&type=all',
                        method: 'post',
                        success: function(html){
                            if(html){
                                $(".cbp_tmtimeline").append(html);
                                loader.attr({'data-page':(parseInt(page)+1)}).hide().removeClass('loading');
                            }else{
                                loader.html('<p>No more event to show.</p>');
                            }
                        }
                    });
                }
            }
        });

        $('#event-list-content').on('click','.link-delete-event',function () {
            target_temp = $(this).parents('li').data('eid');
        });

        $('#confirm-delete-event').on('click',function () {
            $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            window.location.href = root+"/events/delete/"+target_temp;
        })

        $('#event-list-content').on('click','.event-report', function (){
            target_tmp      = $(this).data('id');
            target_element  = $(this).children('i');
            checkReport ();
        });

        $('#report-event-btn').on('click',function () {
            if($("#event-report-form").valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : root+"/report",
                    data    : 'id='+target_tmp+"&reason="+$('#input-report').val()+'&type=2',
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        $('#modal-report-event').modal('hide');
                        $('#modal-report-event-result .modal-body').html(r.msg);
                        $('#modal-report-event-result').modal('show');
                        $('#input-report').val('');
                        $('#report-event-btn').removeAttr('disabled').children('.fa-spinner').remove();
                    }
                });
            }

        })

        $('.cbp_tmtimeline').on('click','.link-edit-event', function (e) {
            e.preventDefault();
            var _btn = $(this);
            _btn.html('<i class="fa-spin fa-spinner"></i>');
            var e_id = $(this).attr('data-id');
            $.ajax({
                    url     : root+"/events/form/get",
                    data    : 'id='+e_id,
                    method  : 'post',
                    dataType: 'html',
                    success: function(r){
                        $('#modal-event-edit .landing-edit-form').html(r);
                        $('#modal-event-edit').on("shown.bs.modal", function () {
                             $(window).resize();
                        }).modal('show');
                        $(window).resize();
                        _btn.html('<i class="fa-pencil"></i>');
                    }
                });
        })

        function checkReport () {
            var loader = $('<i class="fa-spin fa-spinner"></i>');
            target_element.after(loader);
            target_element.hide();
            $.ajax({
                    url     : root+"/report-check",
                    data    : 'id='+target_tmp+'&type=2',
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        if(r.status) {
                            validator.resetForm();
                            $("label.error").hide();
                            $(".error").removeClass("error");
                            $('#modal-report-event').modal('show');
                        } else {
                            $('#modal-report-event-result .modal-body').html(r.msg);
                            $('#modal-report-event-result').modal('show');
                        }
                        loader.remove();
                        target_element.show();
                    }
                });
        }
    })
    </script>
    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
