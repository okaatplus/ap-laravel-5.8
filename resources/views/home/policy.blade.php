@extends('content-home')
@section('content')
<!-- Main Content -->
<main class="cd-main-content-blank">
    <div class="section">

    @if(App::getLocale() == 'en')
        <section class="container">
            <section>
                <p><strong>Privacy Policy </strong></p>
                <p><span style="font-weight: 400;">We, Vaari Digital Co., Ltd. (&ldquo;Vaari&rdquo;), take our user&rsquo;s privacy seriously and we will only collect, record, hold, store, disclose, transfer or use your personal information as outlined below.</span></p>
                <p><span style="font-weight: 400;">Data protection is a matter of trust and your privacy is important to us. We shall therefore only use your name or other information which relates to you in the manner set out in this Privacy Policy. We will only collect information where it is necessary for us to do so and we will only collect information if it is relevant to our dealings with you.</span></p>
                <p><span style="font-weight: 400;">We will only keep your information for as long as we are either required to by law or as is relevant for the purposes for which it was collected.</span></p>
                <p><span style="font-weight: 400;">You can access or visit the Platform (as defined in the Terms of Use) and browse without having to provide personal details. During your visit to the Platform you remain anonymous and at no time can we identify you unless you have an account on the Platform and log on with your username and password.</span></p>
                <p><span style="font-weight: 400;">If you have any comments, suggestions or complaints, you may contact us (and our Data Protection Officer) by e-mail at vaaridigital@gmail.com</span></p>
                <p><span style="font-weight: 400;">Collection of Personal Information</span></p>
                <p><span style="font-weight: 400;">When you create a Vaari account, or otherwise provide us with your personal information through the Platform, the personal information we collect may include yours:</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Name</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Home Address</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Email Address</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Contact Number</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Mobile Number</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Date of Birth</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Gender</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">National ID Card number/Passport Number</span></li>
                </ul>
                <p><span style="font-weight: 400;">You must only submit to us, our authorised agent or the Platform, information which is accurate and complete and not misleading and you must keep it up to date and inform us of changes (more information below). We reserve the right to request for documentation to verify the information provided by you.</span></p>
                <p><span style="font-weight: 400;">We will only be able to collect your personal information if you voluntarily submit the information to us. If you choose not to submit your personal information to us or subsequently withdraw your consent to our use of your personal information, we may not be able to provide you with our Services. You may access and update your personal information submitted to us at any time as described below.</span></p>
                <p><span style="font-weight: 400;">If you provide personal information of any third party to us, we assume that you have obtained the required consent from the relevant third party to share and transfer his/her personal information to us.</span></p>
                <p><span style="font-weight: 400;">If you sign up for our services using your social media account or link your Vaari&rsquo;s account to your social media account or use certain other social media features provided by us, we may access information about you which you have voluntarily provided under your social media account via that social media provider in accordance with the provider's policies and we will manage your personal data which we have collected in accordance with Vaari&rsquo;s privacy policy</span></p>
                <p><strong>Use and Disclosure of Personal Information</strong></p>
                <p><span style="font-weight: 400;">The personal information we collect from you will be used, or shared with third parties (including related companies, third party service providers, and third party sellers), for some or all of the following purposes:</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">To facilitate your use of the Services (as defined in the Terms of Use) and/or access to the Platform;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">To process orders you submit through the Platform, whether the products or services provided by Vaari or a third party seller. Payments that you make through the Platform for products or services, whether sold by Vaari or a third party seller, will be processed by our agent;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">To deliver the products or services you have purchased through the Platform, whether sold by Vaari or a third party seller. We may pass your personal information on to a third party in order to make delivery of the product or service to you (for example to our courier or supplier), whether the product or service is sold through the Platform by Vaari or a third party seller;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">To update you on the delivery of the services or products, whether sold through the Platform by Vaari or a third party seller, and for customer support purposes;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">To compare information, and verify with third parties in order to ensure that the information is accurate;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Further, we will use the information you provide to administer your account (if any) with us; verify and carry out financial transactions in relation to payments you make online; audit the downloading of data from the Platform; improve the layout and/or content of the pages of the Platform and customise them for users; identify visitors on the Platform; carry out research on our users&rsquo; demographics and behavior; provide you with information we think you may find useful or which you have requested from us, including information about our or third party sellers&rsquo; products and services, provided you have indicated that you have not objected to being contacted for these purposes;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">When you register an account with Vaari or otherwise provide us with your personal information through the Platform, we will also use your personal information to send you marketing and/or promotional materials about our or third party sellers&rsquo; products and services from time to time. You can unsubscribe from receiving marketing information at any time by using the unsubscribe function within the electronic marketing material. We may use your contact information to send newsletters from us and from our related companies; and</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">In exceptional circumstances Vaari may be required to disclose personal information, such as when there are grounds to believe that the disclosure is necessary to prevent a threat to life or health, for law enforcement purposes, or for fulfillment of legal and regulatory requirements and requests.;</span></li>
                </ul>
                <p><span style="font-weight: 400;">Vaari may share your personal information with third parties and our affiliates for the abovementioned purposes, specifically, completing a transaction with you, managing your account and our relationship with you, marketing and fulfilling any legal or regulatory requirements and requests as deemed necessary by Vaari. In sharing your personal information with them, we endeavor to ensure that the third parties and our affiliates keep your personal information secure from unauthorised access, collection, use, disclosure, or similar risks and retain your personal information only for as long as they need your personal information to achieve the above mentioned purposes.</span></p>
                <p><span style="font-weight: 400;">Vaari does not engage into the business of selling customers&rsquo; personal information to third parties.</span></p>
                <p><strong>Withdrawal of Consent</strong></p>
                <p><span style="font-weight: 400;">You may communicate your objection to our continual use and/or disclosure of your personal information for any of the purposes and in the manner as stated above at any time by contacting us at our e-mail address below. Please note that if you communicate your objection to our use and/or disclosure of your personal information for the purposes and in the manner as stated above, depending on the nature of your objection, we may not be in a position to continue to provide our products or services to you or perform on any contract we have with you. Our legal rights and remedies are expressly reserved in such event.</span></p>
                <p><strong>Updating Your Personal Information</strong></p>
                <p><span style="font-weight: 400;">You can update your personal information anytime by accessing your account on the Vaari&rsquo;s Platform. If you do not have an account with us, you can do so by contacting us at our provided e-mail.</span></p>
                <p><span style="font-weight: 400;">We take steps to share the updates to your personal information with third parties and our affiliates with whom we have shared your personal information if your personal information is still necessary for the above stated purposes.</span></p>
                <p><strong>Security of Your Personal Information</strong></p>
                <p><span style="font-weight: 400;">Vaari ensures that all information collected will be safely and securely stored. We protect your personal information by:</span></p>
                <p><span style="font-weight: 400;">- Restricting access to personal information</span></p>
                <p><span style="font-weight: 400;">- Maintaining technology products to prevent un-authorized computer access</span></p>
                <p><span style="font-weight: 400;">- Securely destroying your personal information when it is no longer needed for any legal or business purposes</span></p>
                <p><span style="font-weight: 400;">If you believe that your privacy has been reached by Vaari, please contact us at our e-mail address below</span></p>
                <p><span style="font-weight: 400;">[vaaridigital@gmail.com]</span></p>
                <p><span style="font-weight: 400;">Your password is the key to your account. Please use unique numbers, letters and special characters, and do not share your Vaari password to anyone. If you do share your password with others, you will be responsible for all actions taken in the name of your account and the consequences. If you lose control of your password, you may lose substantial control over your personal information and other information submitted to Vaari. You could also be subject to legally binding actions taken on your behalf. Therefore, if your password has been compromised for any reason or if you have grounds to believe that your password has been compromised, you should immediately contact us and change your password. You are reminded to log off of your account and close the browser when finished using a shared computer. &nbsp;</span></p>
                <p><strong>Collection of Computer Data</strong></p>
                <p><span style="font-weight: 400;">Vaari or our authorized service providers may use cookies, web beacons, and other similar technologies for storing information to help provide you with a better, faster, safer and personalized experience when you use the Services and/or access the Platform.</span></p>
                <p><span style="font-weight: 400;">When you visit Vaari&rsquo;s Platform or services, our company servers will automatically recognize and record information that your browser sends whenever you visit a website. This data may include:</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Your computer's IP address</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Browser type</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Webpage you were visiting before you came to our Platform</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">The pages within the Platform which you visit</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">The time spent on those pages, items and information searched for on the Platform, access times and dates, and other statistics.</span></li>
                </ul>
                <p><span style="font-weight: 400;">This information is collected for analysis and evaluation in order to help us improve the Platform and the services and products we provide.</span></p>
                <p><span style="font-weight: 400;">Cookies are small text files (typically made up of letters and numbers) placed in the memory of your browser or device when you visit a website or view a message. They allow us to recognize a particular device or browser and help us to personalize the content to match your preferred interests more quickly, and to make our Services and Platform more convenient and useful to you.</span></p>
                <p><span style="font-weight: 400;">You may be able to manage and delete cookies through your browser or device settings. For more information on how to do so, visit the help material of your browser or device.</span></p>
                <p><span style="font-weight: 400;">Web beacons are small graphic images that may be included on our Service and the Platform. They allow us to count users who have viewed these pages so that we can better understand your preference and interests.</span></p>
                <p><strong>No Spam, Spyware, or Virus</strong></p>
                <p><span style="font-weight: 400;">Spam, spyware or virus is not allowed on Platform. Please set and maintain your communication preferences so that we send communications to you as you prefer. You are not licensed or otherwise allowed to add other users (even a user who has purchased an item from you) to your mailing list (email or physical mail) without their express consent. You should not send any messages which contain spam, spyware or virus via the Platform. If you would like to report any suspicious messages, please contact us at our email address below.</span></p>
                <p><span style="font-weight: 400;">[vaaridigital@gmail.com]</span></p>
                <p><strong>Changes to the Privacy Policy</strong></p>
                <p><span style="font-weight: 400;">Vaari shall regularly review the sufficiency of this Privacy Policy. We reserve the right to modify and change the Privacy Policy at any time. Any changes to this policy will be disclosed or published on the Platform.</span></p>
                <p><strong>Vaari&rsquo;s Right</strong></p>
                <p><span style="font-weight: 400;">YOU ACKNOWLEDGE AND AGREE THAT VAARI HAS THE RIGHT TO DISCLOSE YOUR PERSONAL INFORMATION TO ANY LEGAL, REGULATORY, GOVERNMENTAL, TAX, LAW ENFORCEMENT OR OTHER AUTHORITIES OR THE RELEVANT RIGHT OWNERS, IF VAARI DIGITAL HAS REASONABLE GROUNDS TO BELIEVE THAT DISCLOSURE OF YOUR PERSONAL INFORMATION IS NECESSARY FOR THE PURPOSE OF MEETING ANY OBLIGATIONS, REQUIREMENTS OR ARRANGEMENTS, WHETHER VOLUNTARY OR MANDATORY, AS A RESULT OF COOPERATING WITH AN ORDER, AN INVESTIGATION AND/OR A REQUEST OF ANY NATURE BY SUCH PARTIES. TO THE EXTENT PERMISSIBLE BY APPLICABLE LAW, YOU AGREE NOT TO TAKE ANY ACTION AND/OR WAIVE YOUR RIGHTS TO TAKE ANY ACTION AGAINST VAARI. FOR THE DISCLOSURE OF YOUR PERSONAL INFORMATION IN THESE CIRCUMSTANCES.</span></p>
                <p><strong>Contacting Vaari &nbsp;</strong></p>
                <p><span style="font-weight: 400;">If you wish to withdraw your consent to our use of your personal information, request access and/or correction of your personal information, have any queries, comments or concerns, or require any help on technical or cookie-related matters, please feel free to contact us (and our Data Protection Officer) at [vaaridigital@gmail.com].</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> COLLECTION OF PERSONAL DATA</span></li>
                </ol>
                <p><span style="font-weight: 400;">We may obtain your Personal Data from various sources (e.g. from you or through third parties), including:</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> Information obtained (directly or indirectly) when you register as a user of the Vaari&rsquo;s services including your name, address, telephone number, email address and device information (collectively, "Registration Information").</span></li>
                    <li><span style="font-weight: 400;"> Information obtained (directly or indirectly) during your use of Vaari&rsquo;s service, including your bank account numbers, billing and delivery information, billing address, transaction data, credit/debit card numbers and expiration dates and other information from cheque or money orders (collectively, "Account Information")</span></li>
                    <li><span style="font-weight: 400;"> Registration Information, Account Information or other information may be accessed or collected (automatically or manually) during your registration as a user of the Vaari&rsquo;s services..</span></li>
                </ol>
                <p><span style="font-weight: 400;">The above information obtained by us may constitute your Personal Data. We have taken steps to ensure that we do not collect more information (whether or not such information constitutes Personal Data) from you than is necessary for us to provide you with our services, to perform the functions set out in Part B of this notice, to protect your account, comply with our legal obligations, protect our legal rights, and to operate our business.</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> USE OF PERSONAL DATA</span></li>
                </ol>
                <p><span style="font-weight: 400;">We may use the Personal Data that we obtain about you for the following purposes:</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> Verifying your identity, including during your registration as a user of the Vaari&rsquo;s services.</span></li>
                    <li><span style="font-weight: 400;"> Verifying your eligibility to register as a user of Vaari or to use any of the features and functions of Vaari&rsquo;s services.</span></li>
                    <li><span style="font-weight: 400;"> Processing your registration as a user, allowing you to log-in using your Vaari log-in ID and maintaining and managing your registration.</span></li>
                    <li><span style="font-weight: 400;"> Providing you with Vaari account and related customer services, including facilitating the settlement of purchase products and services, delivering and related purchase services, processing chargeback, sending notices about your transactions, and responding to your queries, feedback, claims or disputes.</span></li>
                    <li><span style="font-weight: 400;"> Improving, and expanding our offerings by way of research and development of new functions of, your Vaari account or other new products and services that Vaari may offer from time to time.</span></li>
                    <li><span style="font-weight: 400;"> Performing research, statistical analysis or surveys, whether orally or in writing, in order to manage and protect our business including our information technology infrastructure, to measure the performance of Vaari services and other services we and offer and to ensure your satisfaction with Vaari services.</span></li>
                    <li><span style="font-weight: 400;"> Analyzing trends, usages and other behaviors (whether on an individualized or aggregated basis), which helps us better understand how you and our collective user base access and use Vaari&rsquo;s services and the underlying activities conducted, including for purposes of improving Vaari services and responding to customer queries and preferences.</span></li>
                    <li><span style="font-weight: 400;"> Managing risk, performing creditworthiness and solvency checks, or assessing, detecting, investigating, preventing and/or remediating fraud or other potentially prohibited or illegal activities and otherwise protecting the integrity of our payment platform.</span></li>
                    <li><span style="font-weight: 400;"> Detecting, investigating, preventing or remediating violations of the Terms and Conditions, any applicable internal policies, relevant industry standards, guidelines, laws or regulations.</span></li>
                    <li><span style="font-weight: 400;"> Making such disclosures as may be required by any law or regulation of any country applicable to us or our affiliate, government official or other third party, including any card association or other payment network. Disclosures may also be made pursuant to any subpoena, court order, regulatory requests or other legal process or requirement in any country applicable to us or our affiliate.</span></li>
                    <li><span style="font-weight: 400;"> Making any disclosure to prevent any harm or financial loss, to report any suspected illegal activity or to deal with any claim or potential claim brought against us or our affiliates.</span></li>
                    <li><span style="font-weight: 400;"> Enabling any due diligence and other appraisals or evaluations for actual or proposed merger, acquisition, financing transactions or joint ventures.</span></li>
                    <li><span style="font-weight: 400;"> Any other legitimate business purposes, such as protecting you and other users of Vaari services from losses, protecting lives, maintaining the security of our systems and products, and protecting any of our other rights and/or properties.</span></li>
                </ol>
                <p><span style="font-weight: 400;">We may also use your Personal Data in other ways for which we provide specific notice at the time of collection or for which you have subsequently consented.</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> DISCLOSURE OF PERSONAL DATA</span></li>
                </ol>
                <p><span style="font-weight: 400;">Your Personal Data held by Vaari will be kept confidential but Vaari may provide such information to the following parties (whether within or outside your country of residence) for the purposes set out in B above:</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> The company wholly or partly owned by Vaari</span></li>
                    <li><span style="font-weight: 400;"> Vaari and its group companies.</span></li>
                    <li><span style="font-weight: 400;"> Any agent, contractor or third party service provider that we work with in providing you with our services, including for fraud prevention, bill collection, data entry, database management, promotions, marketing, customer service, technology services, products and services alerts and payment extension services.</span></li>
                    <li><span style="font-weight: 400;"> Entities with whom Vaari maintain business referral or other commercial arrangements, including third parties and entities belonging to Vaari.</span></li>
                    <li><span style="font-weight: 400;"> Merchants and other organizations, such as card associations, payment networks or financial institutions, to whom or through which payments are made using the Vaari&rsquo;s platform, or such other entities to enable your use of the Vaari&rsquo;s platform.</span></li>
                    <li><span style="font-weight: 400;"> Third party financial institutions, banks, collection agents and credit agencies.</span></li>
                    <li><span style="font-weight: 400;"> Professional advisers, law enforcement agencies, insurers, government and regulatory authorities or any other organizations to which Vaari is under an obligation to make disclosures under the requirements of any applicable law, regulation or commercial arrangement, including arrangements with any card association or payment network.</span></li>
                    <li><span style="font-weight: 400;"> Entities involved in any merger, acquisition, financing transaction or joint venture with us.</span></li>
                    <li><span style="font-weight: 400;"> SECURITY MEASURES AND RETENTION</span></li>
                </ol>
                <p><span style="font-weight: 400;">We take all reasonable steps, including technical, administrative and physical safeguards to help protect your Personal Data that we process from loss, misuse and unauthorized access, disclosure, alteration and destruction.</span></p>
                <p><span style="font-weight: 400;">We will retain and procure our service providers to retain your Personal Data only for so long as is necessary for the purposes set out in this notice and in accordance with all applicable laws and regulatory requirements.</span></p>
                <p><span style="font-weight: 400;">We recommend that you do not divulge your password to anyone. Our personnel will never ask you for your password in an unsolicited phone call or in an unsolicited email. If you share a computer with others, you should not save your log-in information (e.g., user ID and password) on that shared computer.</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> INTERNATIONAL DATA PROCESSING</span></li>
                </ol>
                <p><span style="font-weight: 400;">Your Personal Data may be processed outside your country of residence (including outside Thailand), wherever Vaari, other members of the Vaari Group, and other third parties to whom we may disclose Personal Data under C above operate. The countries where your Personal Data may be processed may not have the same data protection laws as the laws of your country of residence (including Thailand).</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> THIRD PARTY SERVICES AND WEBSITES</span></li>
                </ol>
                <p><span style="font-weight: 400;">Vaari may provide links to other websites and services (including apps operated by third parties) for your convenience and information. These services and websites may operate independently from us and may have their own privacy notices or policies, which we strongly suggest you to review before you use any of their services or conduct any activities on those websites. To the extent that any linked websites you visit are not owned or controlled by us, we are not responsible for their contents, their privacy practices and the quality of their services.</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> CHANGES TO THIS NOTICE</span></li>
                </ol>
                <p><span style="font-weight: 400;">Vaari reserves the exclusive right to change, amend or revise this notice from time to time.</span></p>
                <ol>
                    <li><span style="font-weight: 400;"> FURTHER INFORMATION</span></li>
                </ol>
                <p><span style="font-weight: 400;">If you wish to contact us, including if you wish to exercise any rights you may have under applicable law (such as exercising any right to opt-out of data processing), please do so by contacting the Data Protection Officer by the following means: vaaridigital@gmail.com</span></p>
                <p><span style="font-weight: 400;">Vaari may charge a reasonable fee for processing any of your requests if permitted by applicable law.</span><br /><br /></p>
            </section>
            <section></section>
        </section>
    @else
        <section class="container">
            <section>
                <p><strong>นโยบายความเป็นส่วนตัว</strong></p>
                <p><strong>เรา บริษัท วาริ ดิจิตอล จำกัด (&ldquo;วาริ&rdquo;) ถือปฏิบัติอย่างเคร่งครัดว่าเรื่องความเป็นส่วนตัวของลูกค้าเป็นเรื่องสำคัญ และเราจะจัดเก็บ บันทึก ยึดถือ เก็บรักษา เปิดเผย โอน หรือใช้ข้อมูลส่วนตัวของท่านตามที่กำหนดไว้ด้านล่างนี้</strong></p>
                <p><span style="font-weight: 400;">การปกป้องรักษาข้อมูลเป็นเรื่องเกี่ยวกับความน่าเชื่อถือและความไว้วางใจ ความเป็นส่วนตัวของท่านถือเป็นเรื่องสำคัญของเรา เราจึงจะใช้ชื่อหรือข้อมูลอื่น ๆ ที่เกี่ยวข้องกับท่าน เพียงเฉพาะสำหรับกิจการที่ได้กำหนดไว้ในนโยบายความเป็นส่วนตัวนี้เท่านั้น เราจะจัดเก็บข้อมูลเพียงเท่าที่จำเป็นสำหรับการดำเนินการดังกล่าว และเราจะจัดเก็บเพียงข้อมูลที่เกี่ยวข้องกับการติดต่อทางธุรกิจระหว่างท่านกับเราเท่านั้น</span></p>
                <p><span style="font-weight: 400;">เราจะเก็บรักษาข้อมูลของท่านในระยะเวลาเพียงเท่าที่กฎหมายอนุญาตให้กระทำหรือในระยะเวลาเท่าที่จำเป็นสำหรับวัตถุประสงค์ของการจัดเก็บข้อมูลนั้น ๆ</span></p>
                <p><span style="font-weight: 400;">ท่านสามารถเข้าถึงหรือเยี่ยมชมแพลตฟอร์ม (Platform) (ตามความหมายที่ให้ไว้ในข้อตกลงการใช้บริการ) และเลือกชมสินค้าโดยไม่จำเป็นต้องให้ข้อมูลส่วนตัวใด ๆ ระหว่างที่ท่านเยี่ยมชมแพลตฟอร์ม สถานะของท่านจะไม่เป็นที่เปิดเผย และเราจะไม่สามารถระบุตัวตนของท่านได้ตลอดเวลาที่ท่านเข้าเยี่ยมชมแพลตฟอร์ม เว้นแต่ท่านจะได้มีบัญชีบนแพลตฟอร์มและท่านได้เข้าสู่ระบบโดยใช้ชื่อบัญชีและรหัสผ่าน</span></p>
                <p><span style="font-weight: 400;">หากท่านมีความคิดเห็น คำแนะนำหรือคำติชม ท่านสามารถติดต่อเรา (และเจ้าหน้าที่คุ้มครองข้อมูล) โดยทางอีเมล์: vaaridigital@gmail.com</span></p>
                <p><strong>การจัดเก็บข้อมูลส่วนตัว</strong></p>
                <p><span style="font-weight: 400;">เมื่อท่านสร้างบัญชีวาริ หรือให้ข้อมูลส่วนตัวของท่านผ่านทางแพลตฟอร์ม ข้อมูลที่เราจัดเก็บอาจรวมถึงข้อมูลต่าง ๆ เหล่านี้ของท่าน</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">ชื่อ</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">ที่อยู่</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">อีเมล์</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">หมายเลขติดต่อ</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">หมายเลขโทรศัพท์เคลื่อนที่</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">วันเดือนปีเกิด</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เพศ</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เลขที่บัตรประจำตัวประชาชน/หนังสือเดินทาง</span></li>
                </ul>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">ท่านจะต้องจัดส่งข้อมูลให้แก่เรา ตัวแทนผู้รับมอบอำนาจของเรา หรือจัดส่งเข้าสู่แพลตฟอร์มเท่านั้น และข้อมูลดังกล่าวจะต้องเป็นข้อมูลที่ ถูกต้องแท้จริง ครบถ้วน และไม่ทำให้เกิดความเข้าใจผิดใด ๆ ท่านจะต้องอัพเดทข้อมูลของท่านให้เป็นปัจจุบัน และแจ้งให้เราทราบเกี่ยวกับการเปลี่ยนแปลงข้อมูลต่าง ๆ (รายละเอียดตามที่จะได้กล่าวข้างล่าง) เราสงวนสิทธิที่จะร้องขอเอกสารเพิ่มเติมเพื่อตรวจสอบยืนยันว่าข้อมูลที่ท่านให้มานั้นถูกต้อง</span></p>
                <p><span style="font-weight: 400;">เราจะสามารถจัดเก็บข้อมูลส่วนตัวของท่านหากท่านส่งมอบข้อมูลให้เราด้วยความสมัครใจของท่านเท่านั้น หากท่านเลือกจะไม่ส่งข้อมูลส่วนตัวของท่านให้แก่เราหรือเลือกที่จะเพิกถอนความยินยอมในการใช้ข้อมูลส่วนตัวของท่าน เราอาจไม่สามารถให้บริการของเราแก่ท่านได้ ท่านอาจจะต้องเข้าถึงและปรับเปลี่ยนข้อมูลส่วนตัวของท่านที่ท่านได้ส่งมอบให้เราได้ตลอดเวลาตามที่ได้กำหนดไว้ด้านล่างนี้</span></p>
                <p><span style="font-weight: 400;">หากท่านได้ส่งมอบข้อมูลของบุคคลภายนอกให้แก่เรา เราจะถือว่าท่านได้รับความยินยอมหรือได้รับอนุญาตจากบุคคลภายนอกผู้เกี่ยวข้องที่จะเปิดเผยและส่งมอบข้อมูลส่วนตัวนั้น ๆ แก่เรา</span></p>
                <p><span style="font-weight: 400;">หากท่านสมัครเพื่อใช้งานวาริโดยผ่านทางบัญชีโซเชียลมีเดีย (Social Media) หรือเชื่อมต่อบัญชีวาริกับบัญชีโซเชียลมีเดียของท่าน หรือใช้บริการอื่น ๆ ของวาริ เราอาจเข้าถึงข้อมูลเกี่ยวกับท่านที่ท่านได้ให้ไว้โดยสมัครใจกับบัญชีโซเชียลมีเดียของท่านผ่านทางผู้ให้บริการโซเชียลมีเดียรายนั้น ๆ ภายใต้นโยบายต่าง ๆ ของผู้ให้บริการ เราจะจัดการข้อมูลส่วนตัวของท่านที่ได้รับมาดังกล่าวตามนโยบายความเป็นส่วนตัวของวาริ</span></p>
                <p><strong>การใช้และการเปิดเผยข้อมูลส่วนตัว</strong></p>
                <p><span style="font-weight: 400;">เราจะใช้ข้อมูลส่วนที่เราได้รับจากท่านหรือส่งต่อแก่บุคคลภายนอก (รวมถึงบริษัทที่เกี่ยวข้อง ผู้ให้บริการภายนอก ผู้ขายที่เป็นบุคคลภายนอก) เพื่อประโยชน์ของกิจการทั้งหมดหรือแต่บางส่วนดังจะได้กล่าวต่อไปนี้</span></p>
                <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เพื่อดำเนินการหรือสนับสนุนการใช้บริการ (Services) (ตามความหมายที่ได้ให้ไว้ในข้อตกลงการใช้บริการ) และ/หรือ การใช้แพลตฟอร์ม</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เพื่อดำเนินการตามคำสั่งซื้อของท่านที่ส่งผ่านทางแพลตฟอร์ม ไม่ว่าจะเป็นการขายสินค้าหรือบริการโดยวาริหรือผู้ขายที่เป็นบุคคลภายนอก การจ่ายเงินของท่านผ่านทางแพลตฟอร์มสำหรับสินค้าหรือบริการ ไม่ว่าจะขายโดยวาริหรือผู้ขายที่เป็นบุคคลภายนอกจะถูกจัดการโดยตัวแทนของเรา</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เพื่อการจัดส่งสินค้าหรือบริการที่ท่านได้ซื้อผ่านทางแพลตฟอร์ม ไม่ว่าจะขายโดยวาริหรือผู้ขายที่เป็นบุคคลภายนอก เราอาจส่งต่อข้อมูลส่วนตัวของท่านแก่บุคคลภายนอกเพื่อการจัดส่งสินค้าหรือบริการให้แก่ท่าน (เช่นส่งให้แก่ผู้ดำเนินการจัดส่งหรือผู้จัดจำหน่ายสินค้าหรือบริการของเรา) ไม่ว่าจะขายโดยวาริหรือผู้ขายที่เป็นบุคคลภายนอก</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เพื่อแจ้งให้ท่านทราบเกี่ยวการจัดส่งสินค้าหรือบริการ ไม่ว่าจะขายผ่านแพลตฟอร์มโดยวาริหรือผู้ขายที่เป็นบุคคลภายนอก และเพื่อการบริการลูกค้าอื่น ๆ</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เพื่อเปรียบเทียบข้อมูลและตรวจสอบกับบุคคลภายนอกเพื่อรับรองความถูกต้องของข้อมูล</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">อีกทั้ง เราจะใช้ข้อมูลที่ท่านได้ส่งมอบให้เพื่อจัดการบัญชีการใช้งานของท่านที่ท่านมีกับเรา (ถ้ามี) เพื่อตรวจสอบและจัดการการธุรกรรมทางการเงินอันเกี่ยวกับการจ่ายเงินที่ท่านได้จัดทำขึ้นทางอินเตอร์เน็ตหรือออนไลน์ เพื่อตรวจสอบการดาวน์โหลดข้อมูลจากแพลตฟอร์ม เพื่อพัฒนาแผนผัง การจัดหน้าและเนื้อหาของหน้าต่าง ๆ ของแพลตฟอร์มและปรับแต่งสำหรับผู้ใช้ เพื่อระบุตัวตนผู้เข้าใช้ เพื่อการวิเคราะห์สถิติและพฤติกรรมของผู้ใช้ เพื่อจัดส่งข้อมูลที่เราคาดว่าท่านว่ามีประโยชน์สำหรับท่านหรือที่ท่านได้ร้องขอจากเราให้แก่ท่าน รวมถึงข้อมูลเกี่ยวกับสินค้าและบริการของเราและของผู้ขายที่เป็นบุคคลภายนอก เว้นแต่ท่านจะได้ปฏิเสธการติดต่อเพื่อวัตถุประสงค์ดังกล่าว</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">เมื่อท่านได้สมัครเพื่อเปิดบัญชีการใช้บริการกับวาริหรือได้ส่งมอบข้อมูลส่วนตัวของท่านผ่านทางแพลตฟอร์ม เราใช้ข้อมูลส่วนตัวของท่านเพื่อส่งข่าวสารทางการตลาดและโปรโมชั่นต่าง ๆ เกี่ยวกับสินค้าหรือบริการของเราหรือของผู้ขายที่เป็นบุคคลภายนอกเป็นครั้งคราว ท่านสามารถยกเลิกการรับข้อมูลการตลาดได้ตลอดเวลา โดยการใช้ระบบการยกเลิกการรับข้อมูลที่อยู่ในข้อมูลทางการตลาดที่จัดส่งให้ เราอาจใช้ข้อมูลการติดต่อของท่านเพื่อส่งเอกสารข่าวสารจากเราหรือจากบริษัทที่เกี่ยวข้องของเรา; และ</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">ในกรณีที่เป็นข้อยกเว้นและวาริถูกขอให้เปิดเผยข้อมูลส่วนตัว เช่นในกรณีที่มีมูลให้เชื่อว่าการเปิดเผยดังกล่าวอาจป้องกันการข่มขู่ต่อชีวิตหรือร่างกาย สำหรับวัตถุประสงค์ในการบังคับใช้กฎหมาย หรือการปฏิบัติตามกฎหมาย ระเบียบ ข้อบังคับหรือการร้องขออื่นใด</span></li>
                </ul>
                <p><span style="font-weight: 400;">วาริอาจส่งมอบข้อมูลส่วนตัวของท่านให้แก่บุคคลภายนอกหรือบริษัทในเครือของเราสำหรับวัตถุประสงค์ดังกล่าวมาข้างต้น โดยเฉพาะอย่างยิ่งเพื่อดำเนินธุรกรรมต่าง ๆ กับท่านให้สำเร็จ จัดการบัญชีการใช้บริการของท่าน จัดการความสัมพันธ์อื่น ๆ กับท่าน การตลาด การปฏิบัติตามกฎหมาย ระเบียบข้อบังคับ การร้องขออื่นๆ ที่วาริเห็นว่าจำเป็น ในการแบ่งปันข้อมูลให้แก่บุคคลดังกล่าว วาริจะพยายามอย่างถึงที่สุดเพื่อรับรองว่าบุคคลภายนอกและบริษัทในเครือของเราจะรักษาและปกป้องข้อมูลส่วนตัวของท่านจากการเข้าถึง การจัดเก็บ การใช้ การเปิดเผย โดยไม่ได้รับอนุญาต หรือความเสี่ยงอื่น ๆ ในลักษณะเดียวกัน และจะต้องเก็บข้อมูลส่วนตัวของท่านในระยะเวลาเท่าที่จำเป็นสำหรับกิจการที่ได้กล่าวไว้ด้านบนนี้เท่านั้น</span></p>
                <p><span style="font-weight: 400;">วาริไม่เคยและไม่ได้ทำธุรกิจในการค้าข้อมูลส่วนตัวของลูกค้าแก่บุคคลภายนอก</span></p>
                <p><strong>การเพิกถอนความยินยอม</strong></p>
                <p><span style="font-weight: 400;">ท่านอาจส่งคำคัดค้านการใช้หรือการเปิดเผยข้อมูลส่วนตัวของท่านสำหรับกิจการต่าง ๆ หรือเพื่อวัตถุประสงค์ที่ระบุไว้ด้านบน แก่เราได้ตลอดเวลา โดยติดต่อเราผ่านทางอีเมล์ที่ระบุไว้ด้านล่างนี้ โปรดทราบว่าหากท่านได้ส่งคำคัดค้านการใช้หรือการเปิดเผยข้อมูลส่วนตัวของท่านสำหรับกิจการต่าง ๆ หรือเพื่อวัตถุประสงค์ที่ระบุไว้ด้านบน ขึ้นอยู่กับพฤติการณ์ของคำคัดค้าน เราอาจไม่อยู่ในสถานะที่จะจัดส่งสินค้าหรือบริการให้แก่ท่านหรือปฏิบัติตามสัญญาต่อท่านได้ เราขอสงวนสิทธิทางกฎหมายและการเยียวยาไว้โดยชัดแจ้งในกรณีซึ่งเราไม่สามารถจัดส่งสินค้าหรือบริการให้แก่ท่านหรือตามปฏิบัติตามสัญญาต่อท่านได้อันเกิดจากคำคัดค้านดังกล่าว</span></p>
                <p><strong>การอัพเดทหรือปรับเปลี่ยนข้อมูลส่วนตัวของท่าน</strong></p>
                <p><span style="font-weight: 400;">ท่านสามารถอัพเดทหรือปรับเปลี่ยนข้อมูลส่วนตัวของท่านได้ตลอดเวลาโดยการเข้าสู่บัญชีการใช้บริการของท่านบนแพลตฟอร์มของวาริ หากท่านไม่มีบัญชีการใช้บริการกับเราท่านอาจทำการอัพเดทหรือปรับเปลี่ยนข้อมูลส่วนตัวได้โดยการติดต่อเราผ่านทางอีเมล์ที่เราได้ให้ไว้</span></p>
                <p><span style="font-weight: 400;">เราจะดำเนินการส่งข้อมูลส่วนตัวที่ได้รับการปรับเปลี่ยนแล้วแก่บุคคลภายนอกหรือบริษัทในเครือที่เราได้แบ่งปันข้อมูลส่วนตัวของท่าน หากข้อมูลส่วนตัวของท่านยังคงจำเป็นต่อกิจการที่กล่าวไว้ข้างบน</span></p>
                <p><strong>ความปลอดภัยของข้อมูลส่วนตัวของท่าน</strong></p>
                <p><span style="font-weight: 400;">วาริรับรองว่าข้อมูลที่จัดเก็บทั้งหมดจะถูกจัดเก็บอย่างปลอดภัย เราป้องกันข้อมูลส่วนตัวของท่านโดย</span></p>
                <p><span style="font-weight: 400;">- จำกัดการเข้าถึงข้อมูลส่วนตัว</span></p>
                <p><span style="font-weight: 400;">- จัดให้มีวิธีการทางเทคโนโลยีเพื่อป้องกันไม่ให้มีการเข้าสู่ระบบคอมพิวเตอร์ที่ไม่ได้รับอนุญาต</span></p>
                <p><span style="font-weight: 400;">- จัดการทำลายข้อมูลส่วนตัวของท่านเพื่อความปลอดภัยเมื่อข้อมูลดังกล่าวไม่จำเป็นสำหรับวัตถุประสงค์ทางกฎหมายและธุรกิจอีกต่อไป</span></p>
                <p><span style="font-weight: 400;">หากท่านมีเหตุให้เชื่อว่าความเป็นส่วนตัวของท่านได้ถูกละเมิดโดยวาริ กรุณาติดต่อเราผ่านทางอีเมล์ที่ระบุไว้ข้างล่างนี้</span></p>
                <p><span style="font-weight: 400;">vaaridigital@gmail.com</span></p>
                <p><span style="font-weight: 400;">รหัสผ่านของท่านเป็นกุญแจสำหรับบัญชีการใช้บริการของท่าน กรุณาใช้ตัวเลข ตัวอักษรหรือสัญลักษณ์ที่แตกต่างกัน และไม่แบ่งปันรหัสผ่านวาริแก่ผู้อื่น หากท่านได้แบ่งปันรหัสผ่านแก่ผู้อื่น ท่านจะรับผิดชอบต่อการกระทำต่าง ๆ ที่ได้กระทำลงในนามหรือผ่านทางบัญชีการใช้บริการของท่านและผลที่ตามมา หากท่านไม่สามารถควบคุมรหัสผ่านของท่านได้ ท่านอาจไม่สามารถควบคุมข้อมูลส่วนตัวของท่านหรือข้อมูลอื่น ๆ ที่ส่งให้แก่วาริ ท่านอาจต้องยอมรับนิติกรรมใด ๆ ที่ทำลงในนามของท่าน ดังนั้นหากรหัสผ่านของท่านถูกเปิดเผยหรือไม่เป็นความลับอีกต่อไปไม่ว่าด้วยเหตุผลใด ๆ หรือท่านมีเหตุที่อาจเชื่อได้ว่ารหัสผ่านนั้นถูกเปิดเผยหรือไม่เป็นความลับอีกต่อไป ท่านควรที่จะติดต่อเราและเปลี่ยนรหัสผ่านของท่าน เราขอเตือนให้ท่านออกจากระบบบัญชีการใช้บริการ (log off) และปิดบราวเซอร์ (Browser) ทุกครั้งที่ท่านใช้คอมพิวเตอร์สาธารณะ</span></p>
                <p><strong>การเก็บข้อมูลคอมพิวเตอร์</strong></p>
                <p><span style="font-weight: 400;">วาริหรือผู้ให้บริการที่ได้รับอนุญาตอาจจะใช้ โปรแกรม คุกกี้ (cookies) เวป บีคอน (web beacons) หรือโปรแกรมอื่น ๆ ที่คล้ายคลึงกันเพื่อจัดเก็บข้อมูล เพื่อช่วยให้เราสามารถจัดให้มีการบริการที่ดีขึ้น เร็วขึ้น ปลอดภัยขึ้น และเพื่อความเป็นส่วนตัวของท่าน เมื่อท่านใช้บริการ และ/หรือเข้าสู่แพลตฟอร์ม</span></p>
                <p><span style="font-weight: 400;">เมื่อท่านเข้าเยี่ยมชมวาริ เซิร์ฟเวอร์ (Servers) ของบริษัทเราทำการจดจำและบันทึกข้อมูลที่บราวเซอร์ของท่านส่งเข้ามาโดยอัตโนมัติเมื่อท่านเข้าเว็บไซต์ของเรา ข้อมูลเหล่านี้อาจหมายรวมถึง</span></p>
                <p><span style="font-weight: 400;">- ที่อยู่ ไอพี แอดเดรส (IP address) ของคอมพิวเตอร์ของท่าน</span></p>
                <p><span style="font-weight: 400;">- ชนิดของเบราว์เซอร์ของท่าน/li&gt;</span></p>
                <p><span style="font-weight: 400;">- หน้าเว็บไซต์ที่ท่านเข้าถึงก่อนที่ท่านจะเข้าสู่แพลตฟอร์ม</span></p>
                <p><span style="font-weight: 400;">- หน้าเว็บไซต์ที่ท่านเข้าชมในแพลตฟอร์ม</span></p>
                <p><span style="font-weight: 400;">- จำนวนเวลาที่ท่านใช้ในการชมหน้าเว็บไซต์ดังกล่าว สินค้าหรือข้อมูลที่ท่านค้นหาในแพลตฟอร์ม เวลาเข้าชมวันที่ และข้อมูลทางสถิติอื่น ๆ</span></p>
                <p><span style="font-weight: 400;">ข้อมูลต่าง ๆ เหล่านี้ จะถูกจัดเก็บเพื่อการวิเคราะห์และประเมินเพื่อช่วยให้เราพัฒนาแพลตฟอร์ม การบริการและสินค้าที่เราจัดหาให้ดีขึ้น</span></p>
                <p><span style="font-weight: 400;">โปรแกรมคุกกี้ ( cookies) เป็นข้อมูลไฟล์ตัวอักษรขนาดเล็ก (โดยปกติจะมีเพียงตัวอักษรและตัวเลข) ที่จัดวางไว้บนหน่วยความจำของเบราว์เซอร์ของท่านหรือบนอุปกรณ์ของท่านเมื่อท่านเข้าชมเว็บไซต์หรือเรียกดูข้อความ โปรแกรมนี้ช่วยให้เราจดจำอุปกรณ์หรือเบราว์เซอร์โดยจำเพาะได้ และช่วยให้เราสามารถจัดทำเนื้อหาให้เหมาะสมกับความสนใจส่วนบุคคลของท่านได้รวดเร็วขึ้น และช่วยให้การบริการและแพลตฟอร์มสะดวกสบายและเป็นประโยชน์ต่อท่านมากขึ้น</span></p>
                <p><span style="font-weight: 400;">คุณอาจจัดการและลบโปรแกรมคุกกี้ (cookies) ผ่านทางเบราว์เซอร์หรือการตั้งค่าอุปกรณ์ของคุณ สำหรับข้อมูลเพิ่มเติมเกี่ยวกับวิธีการดังกล่าวโปรดไปที่เนื้อหาวิธีการช่วยเหลือของเบราว์เซอร์หรืออุปกรณ์ของคุณ</span></p>
                <p><span style="font-weight: 400;">โปรแกรมเวป บีคอน (web beacons) เป็นรูปภาพกราฟฟิคขนาดเล็กที่อาจรวมอยู่ในการบริการหรือบนแพลตฟอร์ม โปรแกรมนี้ช่วยให้เรานับจำนวนผู้ใช้ที่ได้เข้าชมหน้าเว็บไซต์ต่าง ๆ เพื่อให้เราได้เข้าใจความต้องการและความสนใจของท่านมากขึ้น</span></p>
                <p><strong>ไม่มีข้อมูลขยะสแปม สปายแวร์ ไวรัส</strong></p>
                <p><span style="font-weight: 400;">ข้อมูลขยะ (Spam) สปายแวร์ (Spyware) หรือ ไวรัส (Virus) นั้นไม่ได้รับอนุญาตบนแพลตฟอร์ม กรุณาตั้งค่าและคงสภาพความต้องการ (Preference) ในการติดต่อสื่อสารเพื่อที่เราจะได้ส่งข้อมูลการติดต่อสื่อสารตามที่ท่านต้องการ ท่านไม่ได้รับอนุญาตหรือยอมรับให้เพิ่มผู้ใช้รายอื่น (ไม่ว่าจะเป็นผู้ใช้ที่เคยซื้อสินค้าจากท่าน) เข้าสู่รายชื่อผู้รับของท่าน (ไม่ว่าอีเมล์หรือจดหมาย) เว้นแต่จะได้รับอนุญาตโดยชัดแจ้ง ท่านจะไม่ส่งข้อความใด ๆ ที่มี ข้อมูลขยะ สปายแวร์ หรือไวรัส ผ่านทางแพลตฟอร์ม หากท่านต้องการรายงานข้อความที่น่าสงสัยให้เราทราบ กรุณาติดต่อผ่านทางอีเมล์ ที่ระบุด้านล่างนี้</span></p>
                <p><span style="font-weight: 400;">vaaridigital@gmail.com</span></p>
                <p>&nbsp;</p>
                <p><strong>การเปลี่ยนแปลงนโยบายความเป็นส่วนตัว</strong></p>
                <p><span style="font-weight: 400;">วาริจะตรวจสอบประสิทธิภาพของนโยบายความเป็นส่วนตัวเป็นประจำ เราขอสงวนสิทธิในการเปลี่ยนแปลงนโยบายความเป็นส่วนได้ตลอดเวลา การเปลี่ยนแปลงนโยบายดังกล่าวจะถูกประกาศหรือตีพิมพ์ไว้ในแพลตฟอร์ม</span></p>
                <p><strong>สิทธิของวาริ</strong></p>
                <p><span style="font-weight: 400;">ท่านรับทราบและตกลงว่า วาริมีสิทธิในการเปิดเผยข้อมูลส่วนตัวแก่ หน่วยงานผู้มีอำนาจทางกฎหมาย หน่วยงานการกำกับควบคุมดูแล หน่วยงานของรัฐ หน่วยงานเกี่ยวกับภาษี หน่วยงานในการบังคับใช้กฎหมาย และหน่วยงานอื่น ๆ ที่เกี่ยวข้อง หรือเจ้าของสิทธิต่าง ๆ ที่เกี่ยวข้อง หากวาริมีเหตุอันควรเชื่อได้ว่าการเปิดเผยข้อมูลส่วนตัวของท่านนั้นจำเป็นสำหรับการปฏิบัติตามหน้าที่ ความรับผิดชอบ การจัดการและข้อตกลง ไม่ว่าโดยสมัครใจหรือการบังคับ เพื่อวัตถุประสงค์ในการให้ความร่วมมือกับคำสั่ง การสอบสวน และ/หรือการร้องขอในรูปแบบต่าง ๆ ของหน่วยงานนั้น ๆ ภายใต้การบังคับของกฎหมายที่เกี่ยวข้อง ท่านตกลงว่าจะไม่ฟ้องร้องดำเนินคดีหรือดำเนินการใด ๆ ต่อวาริ และสละสิทธิเรียกร้องหรือสิทธิอื่นใดอันมีต่อ วาริ ที่อาจเกิดขึ้นจากการเปิดเผยข้อมูลส่วนตัวของท่านภายใต้สถานการณ์ดังกล่าวข้างต้น</span></p>
                <p><strong>การติดต่อวาริ</strong></p>
                <p><span style="font-weight: 400;">หากท่านต้องการเพิกถอนความยินยอมในการใช้ข้อมูลส่วนตัวของท่าน ขอเรียกดูข้อมูลและ/หรือข้อมูลส่วนตัว มีคำถาม ความคิดเห็น ข้อกังวล หรือขอความช่วยเหลือทางด้านเทคนิค หรือเกี่ยวกับโปรแกรมคุกกี้ (cookies) กรุณาติดต่อเรา (และเจ้าหน้าที่คุ้มครองข้อมูล) ได้ผ่านทางอีเมล์ [vaaridigital@gmail.com]</span></p>
                <ol>
                    <li><strong> การเก็บรวบรวมข้อมูลส่วนตัว</strong></li>
                </ol>
                <p><span style="font-weight: 400;">เราอาจขอข้อมูลส่วนตัวของคุณจากแหล่งต่างๆ (เช่น จากคุณหรือผ่านทางบุคคลภายนอก) ซึ่งรวมทั้ง</span></p>
                <ol>
                    <li><span style="font-weight: 400;">ข้อมูลที่ได้มา (โดยทางตรงหรือทางอ้อม) เมื่อคุณลงทะเบียนเป็นผู้ใช้บริการวาริ ซึ่งได้แก่ชื่อ ที่อยู่ หมายเลขโทรศัพท์ ที่อยู่อีเมล และข้อมูลของอุปกรณ์ (เรียกรวมว่า&nbsp;</span><strong>"ข้อมูลการลงทะเบียน"</strong><span style="font-weight: 400;">)</span></li>
                    <li><span style="font-weight: 400;">ข้อมูลที่ได้มา (โดยทางตรงหรือทางอ้อม) ในระหว่างการใช้บริการวาริของคุณ ซึ่งได้แก่หมายเลขบัญชีธนาคาร ข้อมูลการจัดส่งสินค้าและการออกบิล ที่อยู่ในการจัดส่งบิล ข้อมูลการทำธุรกรรม หมายเลขบัตรเครดิต/เดบิต และวันที่หมดอายุ และข้อมูลอื่นๆ จากธนาณัติหรือเช็ค (เรียกรวมว่า&nbsp;</span><strong>"ข้อมูลบัญชี"</strong><span style="font-weight: 400;">)</span></li>
                    <li><span style="font-weight: 400;">ข้อมูลการลงทะเบียน ข้อมูลบัญชี หรือข้อมูลอื่นๆ อาจสามารถเข้าถึงหรือเก็บรวบรวมได้ (โดยอัตโนมัติหรือไม่ใช่โดยอัตโนมัติ) ในระหว่างการลงทะเบียนเป็นผู้ใช้บริการวาริ และ/หรือในระหว่างการใช้บริการวาริของคุณ</span></li>
                </ol>
                <p><span style="font-weight: 400;">ข้อมูลข้างต้นที่เราได้มาสามารถประกอบกันเป็นข้อมูลส่วนตัวของคุณ เราดำเนินการตามขั้นตอนเพื่อตรวจสอบให้แน่ใจว่าเราจะไม่เก็บรวบรวมข้อมูลจากคุณเพิ่มเติม (ไม่ว่าข้อมูลนั้นจะประกอบกันเป็นข้อมูลส่วนตัวหรือไม่ก็ตาม) จนเกินความจำเป็นสำหรับเราที่จะมอบการบริการของเราให้แก่คุณ หรือในการดำเนินการตามที่ระบุในส่วน B ของประกาศฉบับนี้ เพื่อเป็นการปกป้องบัญชีของคุณ ปฏิบัติตามข้อผูกพันทางกฎหมายของเรา ปกป้องสิทธิตามกฎหมายของเรา และดำเนินธุรกิจของเรา</span></p>
                <ol>
                    <li><strong> การใช้ข้อมูลส่วนตัว</strong></li>
                </ol>
                <p><span style="font-weight: 400;">เราสามารถใช้ข้อมูลส่วนตัวเกี่ยวกับคุณที่เราได้มาเพื่อจุดประสงค์ดังต่อไปนี้</span></p>
                <ol>
                    <li><span style="font-weight: 400;">ตรวจสอบยืนยันตัวตนของคุณ รวมทั้งในระหว่างการลงทะเบียนเป็นผู้ใช้บริการวาริของคุณ</span></li>
                    <li><span style="font-weight: 400;">ตรวจสอบยืนยันความเหมาะสมของคุณสมบัติของคุณในการลงทะเบียนเป็นผู้ใช้บริการวาริ หรือเพื่อใช้งานฟีเจอร์และฟังก์ชั่นใดๆ ของวารินี้</span></li>
                    <li><span style="font-weight: 400;">การดำเนินการเพื่อลงทะเบียนเพื่อเป็นผู้ใช้งาน การอนุญาตให้คุณสามารถเข้าสู่ระบบด้วยการใช้ ID การเข้าระบบของวาริ และการคงรักษาและจัดการการลงทะเบียนของคุณ</span></li>
                    <li><span style="font-weight: 400;">การมอบบัญชีผู้ใช้งานวาริและการบริการที่เกี่ยวข้องให้แก่คุณ รวมทั้งการอำนวยความสะดวกในการชำระบัญชีตามราคาซื้อสินค้าและบริการต่างๆ การขนส่งและการบริการที่เกี่ยวข้องเพื่อทำการซื้อ การทำเรื่องขอเงินคืน (charge-backs) การส่งหนังสือแจ้งเกี่ยวกับการทำธุรกรรมของคุณ และการตอบคำถามหรือข้อสงสัยของคุณ คำติชม การเรียกร้อง หรือข้อพิพาทต่างๆ</span></li>
                    <li><span style="font-weight: 400;">การปรับปรุงและขยายการบริการของเราด้วยวิธีการวิจัยและพัฒนาฟังก์ชั่นใหม่ๆ สำหรับวาริ หรือการใช้ผลิตภัณฑ์และบริการใหม่ๆ ที่จะเราสามารถเสนอขายได้เป็นครั้งคราว</span></li>
                    <li><span style="font-weight: 400;">ดำเนินการทำการวิจัย การวิเคราะห์เชิงสถิติ หรือการสำรวจต่างๆ ไม่ว่าโดยปากเปล่าหรือเป็นลายลักษณ์อักษร เพื่อที่จะจัดการและปกป้องธุรกิจของเรา รวมทั้งโครงสร้างพื้นฐานด้านเทคโนโลยีสารสนเทศของเรา เพื่อวัดประเมินผลการดำเนินงานของบริการวาริ และการบริการอื่นๆ ที่เรามอบให้ และเพื่อตรวจสอบให้แน่ใจว่าคุณรู้สึกพึงพอใจในการบริการของเรา</span></li>
                    <li><span style="font-weight: 400;">การวิเคราะห์แนวโน้มต่างๆ การใช้งานและพฤติกรรมอื่นๆ (ไม่ว่าจะเป็นแบบแยกเป็นรายบุคคลหรือแบบรวม) ซึ่งช่วยให้เราเข้าใจวิธีที่คุณและฐานผู้ใช้งานโดยรวมของเราเข้าถึงและใช้บริการวาริ และกิจกรรมเชิงพาณิชย์ที่สำคัญที่ดำเนินการ รวมทั้งเพื่อจุดประสงค์ในการพัฒนาปรับปรุงการบริการของเราให้ดีขึ้น และตอบข้อสงสัย และตอบสนองกับรสนิยมของลูกค้า</span></li>
                    <li><span style="font-weight: 400;">การจัดการความเสี่ยง ดำเนินการตรวจสอบความน่าเชื่อถือด้านการเงินและความสามารถในการชำระหนี้ หรือการประเมิน ตรวจหา ตรวจสอบ ป้องกันไม่ให้เกิดขึ้น และ/หรือการแก้ไขเยียวยาการฉ้อโกง หรือการกระทำที่ผิดกฎหมายหรือการกระทำต้องห้ามที่เป็นไปได้อื่นๆ และอีกนัยหนึ่งคือเพื่อทำการปกป้องความสมบูรณ์ของแพลตฟอร์มการชำระเงินของเรา</span></li>
                    <li><span style="font-weight: 400;">การตรวจหา ตรวจสอบ ป้องกันไม่ให้เกิดขึ้น หรือการแก้ไขเยียวยาการละเมิดเงื่อนไขและข้อตกลง นโยบายภายในที่เกี่ยวข้องใดๆ มาตรฐานด้านอุตสาหกรรม แนวทาง กฎหมายหรือกฎข้อบังคับที่เกี่ยวข้อง</span></li>
                    <li><span style="font-weight: 400;">การเปิดเผยข้อมูลนี้ ตามที่กฎหมายหรือกฎข้อบังคับของประเทศใดก็ตามสามารถกำหนดให้เราหรือบริษัทในเครือของเราต้องกระทำ ตามคำสั่งของเจ้าหน้าที่รัฐหรือกลุ่มบุคคลภายนอก รวมทั้งบริษัทผู้ให้บริการบัตรหรือเครือข่ายการชำระเงินอื่นๆ การเปิดเผยข้อมูลนี้ยังสามารถทำขึ้นโดยเป็นไปตามหมายศาล คำสั่งศาล การร้องขอตามกฎหมาย หรือข้อกำหนดหรือกระบวนการทางกฎหมายอื่นๆ ในประเทศใดๆ ที่สามารถใช้ปฏิบัติกับเราหรือบริษัทในเครือของเราได้</span></li>
                    <li><span style="font-weight: 400;">การทำการเปิดเผยข้อมูลเพื่อป้องกันไม่ให้เกิดอันตรายหรือการสูญเสียทางการเงิน เพื่อรายงานเกี่ยวกับการกระทำที่ต้องสงสัยว่าผิดกฎหมายใดๆ หรือเพื่อจัดการกับข้อเรียกร้องค่าเสียหายใดๆ หรือการเรียกร้องค่าเสียหายที่อาจเกิดขึ้นได้กับเราหรือบริษัทในเครือของเรา</span></li>
                    <li><span style="font-weight: 400;">ดำเนินการสอบทานธุรกิจใดๆ และการประเมินหรือการประเมินผลอื่นๆ สำหรับการควบรวมกิจการที่เป็นข้อเสนอหรือที่เกิดขึ้นจริง การซื้อกิจการ การทำธุรกรรมเพื่อให้เงินทุนหรือการร่วมทุน</span></li>
                    <li><span style="font-weight: 400;">จุดประสงค์ทางธุรกิจที่ชอบด้วยกฎหมายอื่นใด เช่นการปกป้องคุณและผู้ใช้งานคนอื่นของวาริจากการสูญเสีย การปกป้องชีวิต การรักษาความปลอดภัยของระบบและผลิตภัณฑ์ของเรา และการปกป้องสิทธิ์และ/หรือทรัพย์สินอื่นใดของเรา</span></li>
                </ol>
                <p><span style="font-weight: 400;">และเรายังสามารถใช้ข้อมูลส่วนตัวของคุณในรูปแบบอื่นๆ ซึ่งเราจะส่งหนังสือแจ้งเป็นการเฉพาะให้ในเวลาที่ทำการรวบรวมหรือตามที่คุณจะให้การยินยอมในเวลาต่อมา</span></p>
                <ol>
                    <li><strong> การเปิดเผยข้อมูลส่วนตัว</strong></li>
                </ol>
                <p><span style="font-weight: 400;">ข้อมูลส่วนตัวของคุณที่วาริเก็บไว้จะถูกเก็บรักษาไว้เป็นความลับ แต่วาริสามารถมอบข้อมูลนั้นให้แก่กลุ่มต่างๆ ต่อไปนี้ (ไม่ว่าภายในหรือภายนอกประเทศที่อยู่อาศัยของคุณก็ตาม) เพื่อจุดประสงค์ตามที่ระบุไว้ในข้อ B ข้างต้น:</span></p>
                <ol>
                    <li><span style="font-weight: 400;">บริษัทที่วาริเป็นเจ้าของบางส่วนหรือทั้งหมด</span></li>
                    <li><span style="font-weight: 400;">วาริและบริษัทอื่นๆ ในเครือของกลุ่มบริษัทวาริ</span></li>
                    <li><span style="font-weight: 400;">ตัวแทน ผู้รับเหมาหรือผู้ให้บริการภายนอกที่เราทำงานร่วมกันเพื่อให้บริการของเราแก่คุณ ซึ่งรวมทั้งการป้องกันการฉ้อโกง การเก็บรวบรวมบิล การป้อนข้อมูล การบริหารจัดการฐานข้อมูล การส่งเสริมการขาย การทำการตลาด การบริการลูกค้า บริการเทคโนโลยี การแจ้งเตือนเมื่อมีผลิตภัณฑ์หรือการบริการต่างๆ และบริการขยายเวลาการชำระเงิน</span></li>
                    <li><span style="font-weight: 400;">องค์กรที่วาริยังคงการแนะนำบอกต่อธุรกิจหรือทำข้อตกลงทางการค้าอื่นๆ ต่อไป รวมทั้งกลุ่มบุคคลภายนอกและองค์กรที่เป็นของกลุ่มบริษัทวาริ </span></li>
                    <li><span style="font-weight: 400;">ผู้ค้าและองค์กรอื่นๆ เช่นบริษัทให้บริการบัตร เครือข่ายการชำระเงิน หรือสถาบันทางการเงิน ที่ชำระเงินให้หรือทำการชำระเงินผ่านทางผู้ค้าและองค์กรเหล่านี้ ด้วยการใช้แพลตฟอร์มของวาริ หรือองค์กรเหล่านั้นอื่นๆ เพื่อทำให้คุณสามารถใช้แพลตฟอร์มของวาริได้</span></li>
                    <li><span style="font-weight: 400;">องค์กรสินเชื่อและตัวแทนการเรียกเก็บเงิน ธนาคาร สถาบันทางการเงินที่เป็นกลุ่มบุคคลภายนอก</span></li>
                    <li><span style="font-weight: 400;">ที่ปรึกษามืออาชีพ องค์กรบังคับใช้กฎหมาย ผู้รับประกัน รัฐบาลและเจ้าหน้าที่ที่มีอำนาจควบคุม หรือองค์กรอื่นใดที่วาริอยู่ภายใต้ข้อผูกมัดให้ทำการเปิดเผยข้อมูลภายใต้ข้อกำหนดของกฎหมาย กฎระเบียบหรือข้อตกลงเชิงพาณิชย์ที่เกี่ยวข้อง ซึ่งรวมทั้งการทำข้อตกลงกับบริษัทบัตรหรือเครือข่ายการชำระเงิน</span></li>
                    <li><span style="font-weight: 400;">องค์กรที่เกี่ยวข้องในการการควบรวมกิจการ การซื้อกิจการ การทำธุรกรรมเพื่อให้เงินทุนหรือการร่วมทุนกับเรา</span></li>
                    <li><strong> มาตรการในการรักษาความปลอดภัยและการเก็บรักษา</strong></li>
                </ol>
                <p><span style="font-weight: 400;">เราดำเนินการตามขั้นตอนที่เหมาะสมทุกประการ รวมทั้งการดูแลปกป้องด้านกายภาพ การบริหารจัดการ และทางด้านเทคนิค เพื่อช่วยปกป้องข้อมูลส่วนตัวของคุณที่เราจัดการ จากการสูญเสีย การนำไปใช้ในทางที่ผิด และการเข้าถึง การเปิดเผย การเปลี่ยนแปลงและการทำลายทิ้งโดยไม่ได้รับอนุญาต</span></p>
                <p><span style="font-weight: 400;">เราจะเก็บรักษาและจัดการให้ผู้ให้บริการของเราเก็บรักษาข้อมูลส่วนตัวของคุณในระยะเวลาเท่าที่จำเป็นเท่านั้น เพื่อจุดประสงค์ตามที่ระบุในประกาศฉบับนี้ และอย่างสอดคล้องตามกฎหมายและข้อกำหนดตามกฎหมายที่เกี่ยวข้องทั้งหมด</span></p>
                <p><span style="font-weight: 400;">เราขอแนะนำให้คุณอย่าเปิดเผยรหัสผ่านของคุณให้ใครทราบ พนักงานของเราจะไม่ถามรหัสผ่านของคุณด้วยการโทรหาหรือส่งอีเมลที่คุณไม่ได้เรียกร้องหรือยินยอม ถ้าคุณใช้คอมพิวเตอร์ร่วมกับผู้อื่น คุณไม่ควรเก็บรักษาข้อมูลการเข้าระบบของคุณ (เช่น ID ผู้ใช้งานและรหัสผ่าน) ไว้ในเครื่องคอมพิวเตอร์ที่ใช้งานร่วมกันนั้น</span></p>
                <ol>
                    <li><strong> การประมวลผลข้อมูลในต่างประเทศ</strong></li>
                </ol>
                <p><span style="font-weight: 400;">ข้อมูลส่วนตัวของคุณอาจถูกประมวลผลนอกประเทศที่คุณอาศัยอยู่ (รวมทั้งนอกประเทศไทย) ในสถานที่ใดก็ตามที่วาริ บริษัทอื่นๆ ในเครือ และกลุ่มบุคคลภายนอกอื่นๆ ที่เราสามารถเปิดเผยข้อมูลส่วนตัวภายใต้ข้อ C ข้างต้นนั้นดำเนินกิจการอยู่ ในประเทศต่างๆ ที่ข้อมูลส่วนตัวของคุณสามารถถูกประมวลผลได้นั้นอาจไม่มีกฎหมายปกป้องข้อมูลที่เหมือนกันกับกฎหมายของประเทศที่คุณอาศัยอยู่ (รวมทั้งประเทศไทย)</span></p>
                <ol>
                    <li><strong> เว็บไซต์และการบริการของกลุ่มบุคคลภายนอก</strong></li>
                </ol>
                <p><span style="font-weight: 400;">วาริอาจจะให้ลิงค์เชื่อมโยงไปยังเว็บไซต์และการบริการอื่น (รวมทั้งแอพพลิเคชั่นที่ดำเนินงานโดยกลุ่มบุคคลภายนอก) เพื่อความสะดวกและให้ข้อมูลแก่คุณ การบริการและเว็บไซต์เหล่านี้อาจดำเนินงานเป็นอิสระจากเรา และอาจมีประกาศและนโยบายความเป็นส่วนตัวที่เป็นของตัวเอง ซึ่งเราขอแนะนำอย่างยิ่งให้คุณทบทวนตรวจสอบก่อนที่จะใช้งานการบริการเหล่านั้นหรือทำกิจกรรมใดๆ บนเว็บไซต์เหล่านั้น ตามขอบเขตที่เราไม่ได้เป็นเจ้าของหรือควบคุมเว็บไซต์ที่ถูกลิงค์ไปซึ่งคุณเข้าเยี่ยมชมนั้น เราจะไม่รับผิดชอบกับเนื้อหา การปฏิบัติเกี่ยวกับความเป็นส่วนตัว และคุณภาพของการบริการต่างๆ เหล่านั้น</span></p>
                <ol>
                    <li><strong> การเปลี่ยนแปลงประกาศฉบับนี้</strong></li>
                </ol>
                <p><span style="font-weight: 400;">วาริสงวนสิทธิ์ที่จะทำการเปลี่ยนแปลง แก้ไข หรือทบทวนประกาศฉบับนี้เป็นครั้งคราว</span></p>
                <ol>
                    <li><strong> ข้อมูลเพิ่มเติม</strong></li>
                </ol>
                <p><span style="font-weight: 400;">ถ้าคุณต้องการติดต่อเรา รวมทั้งถ้าคุณต้องการที่จะใช้สิทธิ์ใดๆ ที่คุณอาจมีอยู่ภายใต้กฎหมายที่เกี่ยวข้อง (อย่างเช่นการใช้สิทธิ์ที่จะเลือกไม่ดำเนินการประมวลผลข้อมูล) กรุณาทำการติดต่อเจ้าหน้าที่ฝ่ายปกป้องข้อมูล (Data Protection Officer) โดยใช้วิธีต่อไปนี้ vaaridigital@gmail.com </span></p>
                <p><span style="font-weight: 400;">วาริสามารถเรียกเก็บค่าธรรมเนียมตามความเหมาะสม สำหรับการดำเนินการตามคำร้องขอของคุณอย่างหนึ่งอย่างใด ถ้ากฎหมายที่เกี่ยวข้องอนุญาตให้สามารถกระทำได้</span><br /><br /></p>
            </section>
            <section></section>
        </section>
    @endif
    </div>
</main>
@endsection