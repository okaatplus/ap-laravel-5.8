<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="SMART" />
    <meta name="author" content="" />

    <title>SMART WORLD</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ url('images/smart-icon.png') }}">

    {{--<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Arimo:400,700,400italic">--}}
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-core.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-forms.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-components.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/menu-style.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/ap-theme.css">
    {{--<link rel="stylesheet" href="{{ url('/') }}/css/xenon-admin-skins.css">--}}
    <link rel="stylesheet" href="{{ url('/') }}/css/custom.css?version={{time()}}">
    {{-- <link rel="stylesheet" href="{{ url('/') }}/font/thaisans/font.css"> --}}
    <link rel="stylesheet" href="{{ url('/') }}/font/ap/font.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/elusive/css/elusive.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/simple-line-icons/css/simple-line-icons.css">

    <!-- SweetAlert Library -->
    <script src="{{ url('/') }}/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/sweetalert/dist/sweetalert.css">

    <script src="{{ url('/') }}/js/jquery-1.11.1.min.js"></script>

    <!-- PNotify -->
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/js/pnotify/pnotify.custom.min.css">
    <script type="text/javascript" src="{{ url('/') }}/js/pnotify/pnotify.custom.min.js"></script>

    <!-- Bootstarp Rating -->
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/js/bootstrap-rating/bootstrap-rating.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-body app header-fixed">
    <!-- Page Loading Overlay -->
    <div class="page-loading-overlay">
        <div class="loader-2"></div>
    </div>
    @include('layouts.core-header')
    <input type="hidden" id="root-url" value="{{url('/')}}"/>
    <div class="page-container app-body"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
        <!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
        <!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
        <!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
        <div class="sidebar-menu toggle-others fixed">
            <div class="sidebar-menu-inner">
                @switch( Auth::user()->role )
                    @case(0)
                    @include('menu-layout.super-admin.main-menu')
                    @break

                    @case(1)
                    @if( Auth::user()->smart_admin_state )
                        @include('menu-layout.smart-admin.main-menu')
                    @else
                    {{--@include('menu-layout.property-admin.main-menu')--}}
                    @include('menu-layout.management.main-menu')
                    @endif
                    @break

                    @case(2)
                    @if( Auth::user()->smart_admin_state )
                        @include('menu-layout.smart-admin.main-menu')
                    @else
                        @include('menu-layout.smart-supervisor.main-menu')
                    @endif
                    @break

                    @case(3)
                    @include('menu-layout.smart-admin.main-menu')
                    @break

                    @case(4)
                    @include('menu-layout.technician.main-menu')
                    @break

                    @case(5)
                    @include('menu-layout.outsource.main-menu')
                    @break

                    @case(6)
                    @if( Auth::user()->smart_admin_state )
                        @include('menu-layout.smart-admin.main-menu')
                    @else
                        @include('menu-layout.smart-hq-admin.main-menu')
                    @endif
                    @break

                    @default
                    @if( Auth::user()->smart_admin_state )
                        @include('menu-layout.call-center.p-admin-main-menu')
                    @else
                        @include('menu-layout.call-center.main-menu')
                    @endif

                @endswitch
            </div>
        </div>

        <div class="main-content">
            {{--@include('menu-layout.user-nav')--}}
            <script>
            jQuery(document).ready(function($)
            {
                $('a[href="#layout-variants"]').on('click', function(ev)
                {
                    ev.preventDefault();

                    var win = {top: $(window).scrollTop(), toTop: $("#layout-variants").offset().top - 15};

                    TweenLite.to(win, .3, {top: win.toTop, roundProps: ["top"], ease: Sine.easeInOut, onUpdate: function()
                        {
                            $(window).scrollTop(win.top);
                        }
                    });
                });
            });
            </script>
            @yield('content')
            <!-- Main Footer -->
            <!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
            <!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
            <!-- Or class "fixed" to  always fix the footer to the end of page -->
            <footer class="main-footer sticky footer-type-1">

                <div class="footer-inner">

                    <!-- Add your copyright text here -->
                    <div class="footer-text">
                        &copy; {{ date('Y')}}
                        <strong>SMART WORLD</strong>
                    </div>
                    <!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
                    <div class="go-up">

                        <a href="#" rel="go-top">
                            <i class="fa-angle-up"></i>
                        </a>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- Bottom Scripts -->
    <script src="{{ url('/') }}/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/js/TweenMax.min.js"></script>
    <script src="{{ url('/') }}/js/resizeable.js"></script>
    <script src="{{ url('/') }}/js/joinable.js"></script>
    <script src="{{ url('/') }}/js/xenon-api.js"></script>
    <script src="{{ url('/') }}/js/xenon-toggles.js"></script>
    <script src="{{ url('/') }}/js/jquery.cookie.js"></script>
    <script src="{{ url('/') }}/js/howler/howler.min.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="{{ url('/') }}/js/xenon-custom.js"></script>
    <script src="{{ url('/') }}/js/app-scripts/general-notification.js"></script>

    <script>
        $(function () {
            $('.sidebar-toggler').on('click', function () {
                $('.sidebar-menu').toggleClass('collapsed');
            })
            $('.sidebar-toggler').on('click', function () {
                $.cookie("menu_collapsed", $('.sidebar-menu').hasClass('collapsed'), {expires: 1000});
            });
            $(window).load(function () {
                if ($.cookie("menu_collapsed") == 'true') {
                    $('.sidebar-menu').addClass('collapsed');
                    $('#main-menu li.active').removeClass('expanded').find('ul').hide();
                }
            })
        });
    </script>

    @if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3 || Auth::user()->role == 6)
        <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
        <script>
            Pusher.logToConsole = true;
            var pusher = new Pusher("{{ env('PUSHER_KEY') }}", {
                cluster: 'ap1',
                encrypted: true
            });

            var channel = pusher.subscribe('{{ Auth::user()->property_id }}');
            var sound = new Howl({
                src: ['{{ url('/') }}/audio/nb_notification_01.wav']
            });
            channel.bind('notification_event', function(data) {
                var noti_num = parseInt($("#notification_count").text()) + 1;
                $("#notification_count").text(noti_num);
                $("#notification_count_inner").text(noti_num);
                getNotificationPageFirst(1);
                sound.play();

                var notice = new PNotify({
                    //title: data.title,
                    text: data.title,
                    icon: true,
                    hide: false,
                    type: "info",
                    buttons: {
                        closer: true,
                        closer_hover: false,
                        sticker: false
                    }
                });
                notice.get().bind("click", data, function(e) {
                    if($(e.target).is('.ui-pnotify-closer *, .ui-pnotify-sticker *')){
                        return;
                    }else {
                        var root = $('#root-url').val();
                        var r = $.ajax({
                            url:  root+"/notification/markasread",
                            data:({'nid':data.notification.id}),
                            dataType: "json",
                            method: 'post'
                        });
                        r.done(function( result ) {
                            if(result.status == true) {
                                if (data.notification.notification_type == 13) {
                                    document.location = "{{ url('admin/fees-bills/view/') }}" + '/' + data.notification.subject_key;
                                } else if (data.notification.notification_type == 2) {
                                    document.location = "{{ url('admin/complain/view/') }}" + '/' + data.notification.subject_key;
                                }
                            }
                        });
                    }
                });
            });

            function getNotificationPageFirst(page) {
                /*var r = $.ajax({
                    url:  $('#root-url').val()+"/notification/page",
                    data:({'page':page}),
                    dataType: "html",
                    method: 'post'
                });
                r.done(function( result ) {
                    $('.noti-list li').remove();
                    $('#noti-page').remove();
                    $('.noti-list').append( result );
                    $('.ps-scrollbar').perfectScrollbar('update');
                });
            }

            */
            }
            /*
            function markAsRead (noti_id) {
                var root = $('#root-url').val();
                var r = $.ajax({
                    url: root + "/notification/markasread",
                    data: ({'nid': noti_id}),
                    dataType: "json",
                    method: 'post'
                });
                r.done(function (result) {
                    if (result.status == true) {

                    }
                })
            }*/
        </script>
    @endif
    @yield('script')

</body>
</html>
