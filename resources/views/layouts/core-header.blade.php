<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" id="sidebar-lg-show" data-toggle="sidebar">
        {{--<span class="navbar-toggler-icon"></span>--}}
        <i class="fa-bars"></i>
    </button>
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{ url('/') }}/images/smart-logo.png"  alt="AP Logo">
        <img class="navbar-brand-minimized" src="{{ url('/') }}/images/smart-logo.png" alt="AP Logo">
    </a>
    
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <span class="nav-link">
                (
                @switch( Auth::user()->role)
                    @case(0)
                    {{ 'Super Admin' }}
                    @break
                    @case(1)
                    {{ 'SMART Management' }}
                    @break
                    @case(2)
                    {{ 'SMART Supervisor' }}
                    @break
                    @case(3)
                    {{ 'SMART Admin' }}
                    @break
                    @case(4)
                    {{ 'Technician' }}
                    @break
                    @case(5)
                    {{ 'Outsource' }}
                    @break
                    @case(6)
                    {{ 'SMART HQ Admin' }}
                    @break
                    @default
                    {{ 'Call Center' }}
                    @break
                @endswitch
                )
            </span>
        </li>
        <!-- <li class="nav-item px-3">
            <a class="nav-link" href="#">Users</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Settings</a>
        </li> -->
    </ul>

    <ul class="nav navbar-nav ml-auto">
        @if( Auth::user()->property_id)
        <li><span class="property-name-head">{{ Auth::user()->property->property_name_th }}</span></li>
        @endif
        <?php /*<li class="nav-item dropdown d-md-down-none">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-bell"></i>
                <span class="badge badge-pill badge-danger">5</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div class="dropdown-header text-center">
                    <strong>You have 5 notifications</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="icon-user-follow text-success"></i> New user registered</a>
                <a class="dropdown-item" href="#">
                    <i class="icon-user-unfollow text-danger"></i> User deleted</a>
                <a class="dropdown-item" href="#">
                    <i class="icon-chart text-info"></i> Sales report is ready</a>
                <a class="dropdown-item" href="#">
                    <i class="icon-basket-loaded text-primary"></i> New client</a>
                <a class="dropdown-item" href="#">
                    <i class="icon-speedometer text-warning"></i> Server overloaded</a>
                <div class="dropdown-header text-center">
                    <strong>Server</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <div class="text-uppercase mb-1">
                        <small>
                            <b>CPU Usage</b>
                        </small>
                    </div>
                    <small class="text-muted">348 Processes. 1/4 Cores.</small>
                </a>
                <a class="dropdown-item" href="#">
                    <div class="text-uppercase mb-1">
                        <small>
                            <b>Memory Usage</b>
                        </small>
                    </div>
                    <small class="text-muted">11444GB/16384MB</small>
                </a>
                <a class="dropdown-item" href="#">
                    <div class="text-uppercase mb-1">
                        <small>
                            <b>SSD 1 Usage</b>
                        </small>
                    </div>
                    <small class="text-muted">243GB/256GB</small>
                </a>
            </div>
        </li> */ ?>
        @if(Auth::user()->role == 3)
            @include('notification.notification-header-list')
        @endif
        <li class="nav-item dropdown">
            <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{--<img src="{{ url('/') }}/images/user-4.png" alt="user-image" class="img-avatar"/>--}}
                @if(Auth::user()->profile_pic_name)
                    <img src="{{ env('URL_S3')."/profile-img/".Auth::user()->profile_pic_path.Auth::user()->profile_pic_name }}" alt="user-image" class="img-avatar" />
                @else
                    <img src="{{ url('/') }}/images/user-4.png" alt="user-image" class="img-avatar"/
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>

                @if( Auth::user()->role == 3 || ( in_array(Auth::user()->role, [1,2,6,7]) && Auth::user()->smart_admin_state) )

                    <a class="dropdown-item" href="{{ url('admin/property/about') }}">
                        <i class="fa-info-circle"></i> {{ trans('messages.AboutProp.page_about_head') }}
                    </a>
                    @if(Auth::user()->role != 7)
                    <a class="dropdown-item" href="{{ url('admin/property/settings') }}">
                        <i class="fa-gear"></i> {{ trans('messages.AboutProp.page_setting_head') }}
                    </a>
                    @endif
                    @if( !$is_executive )
                    <a class="dropdown-item" href="{{url('settings')}}">
                        <i class="fa-user"></i> {{trans('messages.Settings.profile_settings')}}
                    </a>
                    @endif

                    @if(Auth::user()->smart_admin_state)
                    <a class="dropdown-item" href="{{url('/admin/property/restoresession')}}">
                        <i class="fa-lock"></i> {{trans('messages.Auth.restore_admin')}}
                    </a>
                    @else
                    <a class="dropdown-item" href="{{ url('auth/logout') }}">
                        <i class="fa-lock"></i> {{trans('messages.Auth.logout')}}
                    </a>
                    @endif

                @else
                    <a class="dropdown-item" href="{{url('settings')}}">
                        <i class="fa-user"></i> {{trans('messages.Settings.profile_settings')}}
                    </a>
                    <a class="dropdown-item" href="{{ url('auth/logout') }}">
                        <i class="fa-lock"></i> {{trans('messages.Auth.logout')}}
                    </a>
                @endif
            </div>
        </li>
    </ul>
</header>