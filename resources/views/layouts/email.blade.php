<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body class="page-body" style="color:#666;line-height:20px;font-size:14px;">
    <div class="content" style="border: 2px solid #FFD800; border-radius: 5px; background-color: #fff;">
        <div class="logo" style="padding: 20px; text-align: center; background-color: #FFD800;">
            <a href="{{ url('/') }}"><img width="180px" src="{{ url('images/smart-logo.png') }}" /></a>
        </div>
        <div class="email-text" style="padding: 20px;">
            @yield('content')
        </div>
    </div>
</body>
</html>
