<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Xenon Boostrap Admin Panel" />
    <meta name="author" content="" />
    <title>Nabour</title>
    <link rel="icon" type="image/png" href="{{ url('/') }}/home-theme/img/favicon/favicon-16x16.png" sizes="16x16">
    @yield('print-css')
</head>
<style> td { color: #000 !important; }</style>
<body>
        @yield('content')
    <script src="{{ url('/') }}/js/jquery-1.11.1.min.js"></script>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72495831-1', 'auto');
        ga('send', 'pageview');
        @yield('script')
    </script>
</body>
</html>
