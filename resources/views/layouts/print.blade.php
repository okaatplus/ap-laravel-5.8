<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Xenon Boostrap Admin Panel" />
    <meta name="author" content="" />

    <title>SMART WORLD</title>
    <link rel="shortcut icon" href="{{ url('images/smart-icon.png') }}">
    <!-- Print Invoice & Receipt -->
    <link rel="stylesheet" href="{{url('/')}}/css/print/print.css?version={{time()}}">
    {{--<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Arimo:400,700,400italic">--}}
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-core.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-forms.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/xenon-components.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/custom.css?version={{time()}}">
    <link rel="stylesheet" href="{{ url('/') }}/font/ap/font.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/fonts/elusive/css/elusive.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="print-only-content">
        @yield('content')
    <script src="{{ url('/') }}/js/jquery-1.11.1.min.js"></script>
    <script>
        $(window).load( function () {
            window.print();
            setTimeout(function(){window.close();}, 1);
        });
    </script>
</body>
</html>
