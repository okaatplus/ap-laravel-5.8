<table border="1">
    <tr>
        <th>UnitOwnerCode__c</th>
        <!--
            1. AP Project เก็บค่า
            "CRM3-"+ActiveCustomerCode+"-CRM12-"+ProductID+"-CRM12-"+AssetCode
            โดย
            - ActiveCustomerCode คือ รหัสลูกค้าของระบบ CRM3
            - ProductID คือ รหัสของ Project จาก CRM1,2 ที่เก็บอยู่บนระบบ (Project_code)
            - AssetCode คือ รหัสของ Unit จาก CRM1,2 ที่เก็บอยู่บนระบบ (land_no)
            2. Non-AP Project เก็บค่า
            "SMARTWorld-"+customer_id+"-"+UnitCode
        -->
        <th>SourceType__c</th>
        <th>customer_id</th>
        <!--
            1. AP Project Customer จาก CRM3
            "CRM3-"+ActiveCustomerCode
            โดย
            - ActiveCustomerCode คือ รหัสลูกค้าของระบบ CRM3
            2. Non-AP Project Customer
            SourceType__c+"-"+customer_id
        -->
        <th>Status__c</th>
        <th>ProjectCode__c</th>
        <!--
            Project ของ AP: "CRM12-"+Project_code
            Project ของNon- AP: "SMARTWorld-"+ID
        -->
        <th>UnitCode__c</th>
        <!--
            1. AP Project เก็บค่า
            ProjectCode+"-CRM12-"+land_no
            2. Non-AP Project เก็บค่า
            ProjectCode+"-SMARTWorld-"+ID
        -->
        <th>Type__c</th>
    </tr>
    @foreach( $c_p as $c )
    <tr>
        <td>
            @if( $c->customer->customer_code )
                {{  "CRM3-".$c->customer->customer_code."-CRM12-".$c->property->project_code."-CRM12-".$c->unit->land_no }}
            @else
                {{ "SMARTWorld-".$c->customer_id."-".$c->property_unit_id }}
            @endif
        </td>
        <td>SMARTWorld</td>
        <td>
            @if( $c->customer->customer_code )
                {{ "CRM3-". $c->customer->customer_code }}
            @else
                {{ "SMARTWorld-". $c->customer_id }}
            @endif
        </td>
        <td>1</td>

        @if($c->property->ap_type == 'AP' || $c->property->ap_type == 'Old AP')
            <td>{{ "CRM12-".$c->property->project_code }}</td>
        @else
            <td>{{ 'SMARTWorld-'.$c->property->id }}</td>
        @endif

        @if($c->property->ap_type == 'AP' || $c->property->ap_type == 'Old AP')
            <td>{{ $c->property->project_code."-CRM12-".$c->unit->land_no }}</td>
        @else
            <td>{{ $c->property->project_code.'-SMARTWorld-'.$c->unit->property_unit_unique_id }}</td>
        @endif
        <td>
            {{ $c->customer_type+1 }}
        </td>
    </tr>
    @endforeach
</table>