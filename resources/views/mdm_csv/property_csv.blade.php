<table border="1">
    <tr>
        <th>id</th>
        <th>property_name_th</th>
        <th>property_name_en</th>
        <th>juristic_person_name_th</th>
        <th>juristic_person_name_en</th>
        <th>area_size</th>
        <th>area_size</th>
        <th>area_size</th>
        <th>area_size</th>
        <th>unit_size</th>
        <th>construction_by</th>
        <th>address_th</th>
        <th>street_th</th>
        <th>province</th>
        <th>postcode</th>
        <th>lat</th>
        <th>lng</th>
        <th>tel</th>
        <th>active_status</th>
        <th>created_at</th>
        <th>group</th>
        <th>property_ap_type</th>
        <th>updated_at</th>
        <th>ProjectCode</th>
        <!--
            Project ของ AP: ""CRM12-""+Project_code
            Project ของ Non- AP: ""SMARTWorld-""+ID"
        -->
        <th>Source Type</th>
        <th>IsManagedBySMART__c</th>
    </tr>
    @foreach( $property as $p )
    <tr>
        <td>{{ $p->id }}</td>
        <td>{{ $p->property_name_th }}</td>
        <td>{{ $p->property_name_en }}</td>
        <td>{{ $p->juristic_person_name_th }}</td>
        <td>{{ $p->juristic_person_name_en }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td>{{ $p->area_size }}</td>
        <td>{{ $p->unit_size }}</td>
        <td>{{ $p->construction_by }}</td>
        <td>{{ $p->address_th }}</td>
        <td>{{ $p->street_th }}</td>
        @if($p->has_province)
            <td>{{ $p->has_province->name_th }}</td>
        @else
            <td></td>
        @endif
        <td>{{ $p->postcode }}</td>
        <td>{{ $p->lat }}</td>
        <td>{{ $p->lng }}</td>
        <td>{{ $p->tel }}</td>
        <td>{{ $p->active_status }}</td>
        <td>{{ $p->created_at }}</td>

        @if($p->has_brand)
            <td>{{ $p->has_brand->name_th }}</td>
        @else
            <td></td>
        @endif

        @if($p->ap_type == 'AP' || $p->ap_type == 'Old AP')
            <td>1</td>
        @else
            <td>0</td>
        @endif
        <td>{{ $p->updated_at }}</td>

        @if($p->ap_type == 'AP' || $p->ap_type == 'Old AP')
            <td>{{ "CRM12-".$p->project_code }}</td>
        @else
            <td>{{ "SMARTWorld-".$p->updated_at }}</td>
        @endif
        <td>SMARTWorld</td>
        <td>TRUE</td>
    </tr>
    @endforeach
</table>