<table border="1">
    <tr>
        <th>unit_number</th>
        <th>unit_floor</th>
        <th>unit_size</th>
        <th>unit_size_type</th>
        <th>building</th>
        <th>land_no</th>
        <th>UnitCode</th>
        <!--
            1. AP Project เก็บค่า
            ProjectCode+""-CRM12-""+land_no
            2. Non-AP Project เก็บค่า
            ProjectCode+""-SMARTWorld-""+ID"
        -->
        <th>land_no</th>
        <!--
            AP Project: land_no
            Non-AP Project: Id
        -->
        <th>Project__c</th>
        <!--
            Project ของ AP: "CRM12-"+Project_code
            Project ของNon- AP: "SMARTWorld-"+ID
        -->
        <th>SourceType__c</th>
    </tr>
    @foreach( $property_unit as $p )
    <tr>
        <td>{{ $p->unit_number }}</td>
        <td>{{ $p->unit_floor }}</td>
        <td>{{ $p->property_size }}</td>
        <td>{{ $p->size_label }}</td>
        <td>{{ $p->building }}</td>
        <td>{{ $p->land_no }}</td>
        <!-- unit code -->
        @if($p->property->ap_type == 'AP' || $p->property->ap_type == 'Old AP')
            <td>{{ $p->property->project_code."-CRM12-".$p->land_no }}</td>
        @else
            <td>{{ "SMARTWorld-".$p->property_unit_unique_id }}</td>
        @endif
        <!-- Land number -->
        @if($p->property->ap_type == 'AP' || $p->property->ap_type == 'Old AP')
            <td>{{ $p->land_no }}</td>
        @else
            <td>{{ $p->id }}</td>
        @endif

        <!-- Project__c -->
        @if($p->property->ap_type == 'AP' || $p->property->ap_type == 'Old AP')
            <td>{{ "CRM12-".$p->property->project_code }}</td>
        @else
            <td>{{ "SMARTWorld-".$p->id }}</td>
        @endif

        <td>SMARTWorld</td>
    </tr>
    @endforeach
</table>