<table border="1">
    <tr>
        <th>id</th>
        <th>prefix_name</th>
        <th>name</th>
        <th>id_card</th>
        <th>type_id_card</th>
        <th>phone</th>
        <th>email</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>delivery_address</th>
        <th>person_type</th>
        <th>nationality</th>
        <th>job</th>
        <th>dob</th>
        <th>created_at</th>
        <th>updated_at</th>
        <th>CustomerStatus__c</th>
        <th>CustomerCode__c</th>
        <th>Status__c</th>
        <th>SourceType__c</th>
        <th>Notification</th>
        <th>SmartWorldStatus__c</th>

    </tr>
    @foreach( $tenant as $c )
        @php
            if (!filter_var($c->email, FILTER_VALIDATE_EMAIL)) {
               $email = "";
            } else {
                $email = $c->email;
            }
        @endphp
    <tr>
        <td>{{ $c->id }}</td>
        <td>{{ $c->prefix_name }}</td>
        <td>{{ $c->name }}</td>
        <td>{{ $c->id_card }}</td>
        <td>{{ $c->type_id_card }}</td>
        <td>{{ $c->phone }}</td>
        <td>{{ $email }}</td>
        <td>{{ $c->delivery_address }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>{{ $c->person_type }}</td>
        <td>{{ $c->nationality }}</td>
        <td>{{ $c->job }}</td>
        <td>{{ $c->dob }}</td>
        <td>{{ $c->created_at }}</td>
        <td>{{ $c->updated_at }}</td>
        <td>Prospect</td>
        <td>{{ "SMARTWorld-".$c->id }}</td>
        <td>1</td>
        <td>SMARTWorld</td>
        <td>TRUE</td>
        <td>Active</td>
    </tr>
    @endforeach
</table>