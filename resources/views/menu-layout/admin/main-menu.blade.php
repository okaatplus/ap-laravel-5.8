<ul id="main-menu" class="main-menu">
    <!-- add class "multiple-expanded" to allow multiple submenus to open -->
    <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
    <li class="has-sub {{ (Request::is('root/admin/property/*') ? 'active expanded' : '') }}">
        <a href="">
            <i class="fa-home"></i>
            <span class="title">นิติบุคคล</span>
        </a>
        <ul {!! (Request::is('root/admin/property/*') ? 'style="display:block;"' : '') !!}>
        	<li class="{{ (Request::is('root/admin/property/list') ? 'active' : '') }}">
				<a href="{{ url('root/admin/property/list') }}">
					<span class="title">รายชื่อนิติบุคคล</span>
				</a>
			</li>
			<li class="">
				<a href="{{ url('root/admin/property/add') }}">
					<span class="title">เพิ่มนิติบุคคล</span>
				</a>
			</li>
            {{--<li class="{{ (Request::is('root/admin/clear-db-master/*') ? 'active' : '') }}">
                <a href="{{url('root/admin/clear-db-master')}}">
                    <span class="title">ล้างข้อมูลนิติบุคคล</span>
                </a>
            </li>--}}
		</ul>
    </li>
    <li class="has-sub {{ (Request::is('root/admin/users/*') ? 'active expanded' : '') }}">
        <a href="">
            <i class="fa fa-group"></i>
            <span class="title">{{ trans('messages.AdminUser.page_head') }}</span>
        </a>
        <ul style="{{ (Request::is('root/admin/users/*') ? 'display:block;' : '') }}">
            <li class="{{ (Request::is('root/admin/users/all') ? 'active' : '') }}">
                <a href="{{ url('root/admin/users/all') }}">
                    <i class="fa fa-user"></i>
                    <span class="title">{{ trans('messages.AdminUser.all_page_head') }}</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ (Request::is('root/admin/helps/*') ? 'active' : '') }}">
        <a href="{{url('root/admin/post')}}">
            <i class="fa fa-paper-plane"></i>
            <span class="title">โพส</span>
        </a>
    </li>
    <li class="has-sub {{ (Request::is('root/admin/receipt*') ? 'active expanded' : '') }}">
        <a href="">
            <i class="fa fa-money"></i>
            <span class="title">จัดการเอกสารการเงิน</span>
        </a>
        <ul {!! (Request::is('root/admin/receipt*') ? 'style="display:block;"' : '') !!}>
            <li class="{{ (Request::is('root/admin/receipt') ? 'active' : '') }}">
                <a href="{{ url('root/admin/receipt') }}">
                    <span class="title">ลบข้อมูล/คืนสถานะ ใบเสร็จรับเงิน</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ (Request::is('root/admin/page/*') ? 'active' : '') }}">
        <a href="#">
            <i class="fa fa-file-o"></i>
            <span class="title">แก้ไขหน้าช่วยเหลือ</span>
        </a>

        <ul style="{{ (Request::is('root/admin/page/*') ? 'display:block;' : '') }}">
            <li class="{{ (Request::is('root/admin/page/helps-property') ? 'active' : '') }}">
                <a href="{{ url('root/admin/page/helps-property') }}">
                    <i class="fa fa-file-code-o"></i>
                    <span class="title">นิติบุคคล</span>
                </a>
            </li>
            <li class="{{ (Request::is('root/admin/page/helps/resident') ? 'active' : '') }}">
                <a href="{{ url('root/admin/page/helps-resident') }}">
                    <i class="fa fa-file-code-o"></i>
                    <span class="title">ลูกบ้าน</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{url('auth/logout')}}">
            <i class="fa-power-off"></i>
            <span class="title">{{ trans('messages.Auth.logout') }}</span>
        </a>
    </li>
</ul>
