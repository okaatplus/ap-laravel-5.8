<ul id="main-menu" class="main-menu">

    <li class="{{ (Request::is('call-center/admin/property/list') ? 'active' : '') }}">
        <a href="{{ url('call-center/admin/property/list') }}">
            <i class="fa fa-home"></i>
            <span class="title">{{ trans('messages.AdminProp.prop_list') }}</span>
        </a>
    </li>
</ul>
