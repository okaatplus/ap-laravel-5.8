<ul id="main-menu" class="main-menu">
    <li class="@if( isset($active_menu) && ($active_menu == "members" || $active_menu == "units" ) ) active @endif">
        <a href="{{url('call-center/admin/property/units')}}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.user_management') }}</span>
        </a>
    </li>
</ul>
