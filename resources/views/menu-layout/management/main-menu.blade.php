<ul id="main-menu" class="main-menu">
    <li class="{{ (Request::is('management/admin/dashboard') ? 'active' : '') }}">
        <a href="{{url('management/admin/dashboard')}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">{{ trans('messages.Menu.dashboard') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('management/admin/app-cover-image*') ? 'active' : '') }}">
        <a href="{{ url('management/admin/app-cover-image') }}">
            <i class="fa fa-image"></i>
            <span class="title">{{ trans('messages.Menu.cover_image') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('management/admin/news-announcement*') ? 'active' : '') }}">
        <a href="{{ url('management/admin/news-announcement') }}">
            <i class="fa fa-bullhorn"></i>
            <span class="title">{{ trans('messages.Menu.news_and_announcement') }}</span>
        </a>
    </li>

    <li class="@if(Request::is('management/admin/property/*')) active @endif">
        <a href="{{ url('management/admin/property/list') }}">
            <i class="fa-home"></i>
            <span class="title">{{ trans('messages.AdminProp.prop_list') }}</span>
        </a>
    </li>

    <li class="{{ (Request::is('management/admin/admin-system/*') ? 'active' : '') }}">
        <a href="{{url('management/admin/admin-system/list')}}">
            <i class="fa fa-user"></i>
            <span class="title">{{ trans('messages.Menu.user_management') }}</span>
            {{--<span class="title">{{ trans('messages.AdminProp.BuildingManager.manager') }}</span>--}}
        </a>
    </li>
</ul>
