<ul class="dropdown-menu user-profile-menu list-unstyled">
    <li>
        <a href="{{ url('admin/property/about') }}">
            <i class="fa-info-circle"></i> {{ trans('messages.AboutProp.page_about_head') }}
        </a>
    </li>
    <li>
        <a href="{{url('settings')}}">
            <i class="fa-user"></i> {{trans('messages.Settings.profile_settings')}}
        </a>
    </li>
    @if(Auth::user()->smart_admin_state)
     <li>
        <a href="{{url('/admin/property/restoresession')}}">
            <i class="fa-lock"></i> {{trans('messages.Auth.restore_admin')}}
        </a>
    </li>
    @else
    <li class="last">
        <a href="{{ url('auth/logout') }}">
            <i class="fa-lock"></i> {{trans('messages.Auth.logout')}}
        </a>
    </li>
    @endif
</ul>