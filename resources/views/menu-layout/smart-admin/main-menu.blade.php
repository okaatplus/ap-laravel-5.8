<?php /*<ul id="main-menu" class="main-menu">
    <li class="@if(isset($active_menu) && $active_menu == "feed") active @endif">
        <a href="{{ url('/') }}">
            <i class="fa fa-newspaper-o"></i>
            <span class="title">{{trans('messages.Post.page_head')}}</span>
        </a>
    </li>
    @if($featured_menu['menu_utility'])
    <li class="@if(Request::is('meter/*')) active expanded @endif">
        <a href="#">
            <i class="fa fa-file-text-o"></i>
            <span class="title">{{trans('messages.Meter.record_meter')}}</span>
        </a>
        <ul @if(Request::is('meter/*')) style="display:block;" @endif>
            <li class="@if(Request::is('meter/water')) active @endif">
                <a href="{{url('meter/water')}}">
                    <i class="fa fa-tint"></i>
                    <span class="title">{{trans('messages.Meter.water')}}</span>
                </a>
            </li>
            <li class="@if(Request::is('meter/electric')) active @endif">
                <a href="{{url('meter/electric')}}">
                    <i class="fa fa-lightbulb-o"></i>
                    <span class="title">{{trans('messages.Meter.electric')}}</span>
                </a>
            </li>
            <li class="@if(Request::is('admin/meter/util/list')) active @endif">
                <a href="{{url('admin/meter/util/list')}}">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">{{trans('messages.Meter.util_head')}}</span>
                </a>
            </li>
            <li class="@if(Request::is('meter/util/units')) active @endif">
                <a href="{{url('meter/util/units')}}">
                    <i class="fa fa-gear"></i>
                    <span class="title">{{trans('messages.Prop_unit.utility_setting')}}</span>
                </a>
            </li>
            <li class="@if(Request::is('meter/util/report')) active @endif">
                <a href="{{url('meter/util/report')}}">
                    <i class="fa fa-gear"></i>
                    <span class="title">{{trans('messages.Meter.meter_report')}}</span>
                </a>
            </li>
        </ul>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_complain'])
    <li class="@if(isset($active_menu) && $active_menu == "complain") active @endif">
        <a href="#">
            <i class="fa fa-wrench"></i>
            <span class="title">{{ trans('messages.Complain.page_head') }}</span>
        </a>
        <ul @if(isset($active_menu) && $active_menu == "complain") style="display:block;" @endif>
            <li class="@if(Request::is('admin/complain')) active @endif">
                <a href="{{url('admin/complain')}}">
                    <span class="title">{{ trans('messages.Complain.user_head') }}</span>
                    @if($complain_count > 0)
                    <span class="label label-success pull-right">{{ $complain_count }}</span>
                    @endif
                </a>
            </li>
            <li class="@if(Request::is('admin/complain/juristic')) active @endif">
                <a href="{{url('admin/complain/juristic')}}">
                    <span class="title">{{ trans('messages.Complain.juristic_head') }}</span>
                    @if($j_complain_count > 0)
                    <span class="label label-success pull-right">{{ $j_complain_count }}</span>
                    @endif
                </a>
            </li>
            <li class="@if(Request::is('admin/complain/report')) active @endif">
                <a href="{{url('admin/complain/report')}}">
                    <span class="title">{{ trans('messages.Complain.report_head') }}</span>
                </a>
            </li>
        </ul>
    </li>
    @endif

    @if((Auth::user()->role == 1 && ($featured_menu && $featured_menu['menu_finance_group'] == true)) || (Auth::user()->role == 3 && ($featured_menu && $featured_menu['menu_finance_group'] == true)))
    <li class="@if(isset($active_menu) && ($active_menu == "bill" || $active_menu == "expenses" || $active_menu == "bill-report" || $active_menu == "finance")) expanded active @endif">
        <a href="#">
            <i class="fa fa-money"></i>
            <span class="title">{{ trans('messages.feesBills.page_head') }}</span>
        </a>
        <ul @if(isset($active_menu) && ($active_menu == "bill" || $active_menu == "expenses" || $active_menu == "finance" || $active_menu == "bill-report")) style="display:block;" @endif>
            <li class="has-sub @if(isset($active_menu) && $active_menu == "bill") active @endif">
                <a href="#">
                    <i class="fa fa-plus"></i>
                    <span class="title">{{ trans('messages.feesBills.income')}}</span>
                </a>
                <ul @if(isset($active_menu) && $active_menu == "bill") style="display:block;" @endif>
                    <li class="@if(Request::is('admin/fees-bills/invoice') || Request::is('admin/fees-bills/revision*')) active @endif">
                        <a href="{{url('admin/fees-bills/invoice')}}">
                            <span class="title">{{ trans('messages.feesBills.invoice_title') }}</span>
                        </a>
                    </li>
                    <li class="@if(Request::is('admin/fees-bills/invoice/overdue')) active @endif">
                        <a href="{{url('admin/fees-bills/invoice/overdue')}}">
                            <span class="title">{{ trans('messages.feesBills.overdue_page_head') }}</span>
                            @if($overdue_bills_count > 0)
                            <span class="label label-success pull-right" style="float:right;margin-right:-2px;">{{ $overdue_bills_count }}</span>
                            @endif
                        </a>
                    </li>

                    <li class="@if(Request::is('admin/fees-bills/bill-collector')) active @endif">
                        <a href="{{url('admin/fees-bills/bill-collector')}}">
                            <span class="title">{{ trans('messages.feesBills.bill_collector.page_head') }}</span>
                        </a>
                    </li>

                    @if($property_featured_menu && $property_featured_menu['aggregate_invoice'])
                    <li class="@if(Request::is('admin/fees-bills/invoice/aggregate')) active @endif">
                        <a href="{{url('admin/fees-bills/invoice/aggregate')}}">
                            <span class="title">{{ trans('messages.feesBills.Aggregate.page_head') }}</span>
                        </a>
                    </li>
                    @endif

                    @if($featured_menu && $featured_menu['menu_prepaid'])
                    <li class="@if(Request::is('admin/prepaid*')) active @endif">
                        <a href="{{url('admin/prepaid')}}">
                            <span class="title">{{ trans('messages.Prepaid.page_head') }}</span>
                        </a>
                    </li>
                    @endif

                    @if($featured_menu && $featured_menu['menu_revenue_record'])
                    <li class="@if(Request::is('admin/revenue-record*')) active @endif">
                        <a href="{{url('admin/revenue-record')}}">
                            <span class="title">{{ trans('messages.RevenueRecord.page_head') }}</span>
                        </a>
                    </li>
                    @endif

                    @if($featured_menu && $featured_menu['menu_retroactive_receipt'])
                    <li class="@if(Request::is('admin/retroactive-receipt*')) active @endif">
                        <a href="{{url('admin/retroactive-receipt')}}">
                            <span class="title">{{ trans('messages.retroactiveReceipt.page_head') }}</span>
                        </a>
                    </li>
                    @endif

                    @if($featured_menu && $featured_menu['menu_cash_on_hand'])
                    <li class="@if(Request::is('admin/cash-box*')) active @endif">
                        <a href="{{url('admin/cash-box/cash-list')}}">
                            <span class="title">{{ trans('messages.CashBox.page_head') }}</span>
                        </a>
                    </li>
                    @endif

                    <li class="@if(Request::is('admin/fees-bills/receipt')) active @endif">
                        <a href="{{url('admin/fees-bills/receipt')}}">
                            <span class="title">{{ trans('messages.feesBills.receipt') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="has-sub @if(isset($active_menu) && $active_menu == "expenses") expanded @endif">
                <a href="#">
                    <i class="fa fa-minus"></i>
                    <span class="title">{{ trans('messages.feesBills.expense')}}</span>
                </a>
                <ul @if(isset($active_menu) && $active_menu == "expenses") style="display:block;" @endif>

                    @if($featured_menu && $featured_menu['menu_pettycash'])
                    <li class="@if(Request::is('admin/expenses/withdrawal/*')) active @endif">
                        <a href="{{ url('admin/expenses/withdrawal/list') }}">
                            <span class="title">{{ trans('messages.Expenses.withdrawal_head') }}</span>
                        </a>
                    </li>
                    <li class="@if(Request::is('admin/expenses/pettycash'))) active @endif">
                        <a href="{{ url('admin/expenses/pettycash') }}">
                            <span class="title">{{ trans('messages.PettyCash.page_head') }}</span>
                        </a>
                    </li>
                    @endif

                    <li class="@if( (Request::is('admin/expenses') || Request::is('admin/expenses/view/*')) && !Request::is('admin/expenses/payee')) active @endif">
                        <a href="{{ url('admin/expenses') }}">
                            <span class="title">{{ trans('messages.Expenses.page_expense_head') }}</span>
                        </a>
                    </li>

                    <li class="@if(Request::is('admin/expenses/payee')) active @endif">
                        <a href="{{ url('admin/expenses/payee') }}">
                            <span class="title">{{ trans('messages.Payee.page_head') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @if($featured_menu && $featured_menu['menu_fund'])
            <li class="@if(Request::is('admin/finance/fund')) active @endif">
                <a href="{{url('admin/finance/fund')}}">
                    <i class="fa fa-support"></i>
                    <span class="title">{{ trans('messages.Fund.page_head') }}</span>
                </a>
            </li>
            @endif

            @if($featured_menu && $featured_menu['menu_statement_of_cash'])
            <li class="@if(Request::is('admin/statement-of-cash/*')) active @endif">
                <a href="{{url('admin/statement-of-cash/list')}}">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">{{ trans('messages.Statement.page_head') }}</span>
                </a>
            </li>
            @endif

            <li class="@if(Request::is('admin/fees-bills/report')) active @endif">
                <a href="{{url('admin/fees-bills/report')}}">
                    <i class="fa fa-bar-chart"></i>
                    <span class="title">{{ trans('messages.feesBills.report') }}</span>
                </a>
            </li>
        </ul>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_committee_room'])
    <li class="@if(isset($active_menu) && $active_menu == "discussion") active @endif">
        <a href="{{ url('discussion') }}">
            <i class="fa fa-comment"></i>
            <span class="title">{{trans('messages.Discussion.page_head')}}</span>
        </a>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_parcel'])
    <li class="@if(isset($active_menu) && $active_menu == "post-parcel") active @endif">
        <a href="{{ url('admin/post-and-parcel')}}">
            <i class="fa fa-envelope"></i>
            <span class="title">{{ trans('messages.PostParcel.page_head') }}</span>
        </a>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_message'])
    <li class="@if(isset($active_menu) && $active_menu == "messages") active @endif">
        <a href="{{url('admin/messages')}}">
            <i class="fa fa-comment-o"></i>
            <span class="title">{{ trans('messages.Message.page_head') }}</span>
            @if($flag_new_message)
            <span class="pull-right message-active"></span>
            @endif
        </a>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_event'])
    <li class="@if(isset($active_menu) && $active_menu == "event") active @endif">
        <a href="{{url('events')}}">
            <i class="fa fa-calendar"></i>
            <span class="title">{{ trans('messages.Event.page_head') }}</span>
        </a>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_vote'])
    <li class="@if(isset($active_menu) && $active_menu == "vote") active @endif">
        <a href="{{url('votes')}}">
            <i class="fa fa-pie-chart"></i>
            <span class="title">{{ trans('messages.Vote.page_head') }}</span>
        </a>
    </li>
    @endif

    @if(Auth::user()->role == 1 || (Auth::user()->role == 3 && ($featured_menu && $featured_menu['menu_property_member'])))
    <li class="@if(isset($active_menu) && ($active_menu == "members" || $active_menu == "units")) active @endif">
        <a href="#">
            <i class="fa fa-group"></i>
            <span class="title">{{ trans('messages.Member.page_head_group') }}</span>
        </a>
        <ul @if(isset($active_menu) && ($active_menu == "members" || $active_menu == "units")) style="display:block;" @endif>
            <li class="@if(isset($active_menu) && $active_menu == "units") active @endif">
                <a href="{{url('admin/property/units')}}">
                    <i class="fa-home"></i>
                    <span class="title">{{ trans('messages.Prop_unit.page_head') }}</span>
                </a>
            </li>
            <li  class="@if(Request::is('admin/property/members')) active @endif">
                <a href="{{url('admin/property/members')}}">
                    <i class="fa fa-male"></i>
                    <span class="title">{{ trans('messages.Member.page_head') }}</span>
                </a>
            </li>
        </ul>
    </li>
    @endif

    @if(Auth::user()->role == 1)
    <li class="@if(Request::is('admin/property/officer')) active @endif">
        <a href="{{url('admin/property/officer')}}">
            <i class="fa fa-suitcase"></i>
            <span class="title">{{ trans('messages.Officer.page_head') }}</span>
        </a>
    </li>
    @endif

    @if($featured_menu && $featured_menu['menu_vehicle'])
    <li class="@if(isset($active_menu) && $active_menu == "vehicle") active @endif">
        <a href="{{url('admin/vehicle')}}">
            <i class="fa-automobile"></i>
            <span class="title">{{ trans('messages.Vehicle.page_head') }}</span>
            @if($request_sticker_count > 0)
            <span class="label label-success pull-right">{{ $request_sticker_count }}</span>
            @endif
        </a>
    </li>
    @endif

    @if($featured_menu && ($featured_menu['menu_committee_room'] || $featured_menu['menu_event'] || $featured_menu['menu_vote'] ))
    <li class="@if(isset($active_menu) && $active_menu == "report") active @endif">
        <a href="#">
            <i class="fa fa-eye-slash"></i>
            <span class="title">{{trans('messages.Post.report')}}</span>
        </a>
        <ul @if(isset($active_menu) && $active_menu == "report") style="display:block;" @endif>
            <li class="@if(Request::is('admin/report/post')) active @endif">
                <a href="{{ url('admin/report/post') }}">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ trans('messages.Post.page_head') }}</span>
                </a>
            </li>
            @if($featured_menu['menu_event'])
            <li class="@if(Request::is('admin/report/event')) active @endif">
                <a href="{{ url('admin/report/event') }}">
                    <i class="fa fa-calendar"></i>
                    <span class="title">{{ trans('messages.Event.page_head') }}</span>
                </a>
            </li>
            @endif
            @if($featured_menu['menu_vote'])
            <li class="@if(Request::is('admin/report/vote')) active @endif">
                <a href="{{ url('admin/report/vote') }}">
                    <i class="fa fa-pie-chart"></i>
                    <span class="title">{{ trans('messages.Vote.page_head') }}</span>
                </a>
            </li>
            @endif
            @if($featured_menu['menu_committee_room'])
            <li class="@if(Request::is('admin/report/discussion')) active @endif">
                <a href="{{ url('admin/report/discussion') }}">
                    <i class="fa fa-comment"></i>
                    <span class="title">{{ trans('messages.Discussion.page_head') }}</span>
                </a>
            </li>
            @endif
        </ul>
    </li>
    @endif

    @if(Auth::user()->role == 1 || (Auth::user()->role == 3 && ($featured_menu && $featured_menu['menu_property_setting'])))
    <li class="@if(isset($active_menu) && $active_menu == "about") active @endif">
        <a href="{{ url('admin/property/about')}}">
            <i class="fa fa-info-circle"></i>
            <span class="title">{{ trans('messages.AboutProp.page_about_head') }}</span>
        </a>
    </li>
    @endif

    <li class="@if(isset($active_menu) && $active_menu == "settings") active @endif">
        <a href="{{url('settings')}}">
            <i class="fa fa-gears"></i>
            <span class="title">{{ trans('messages.Settings.page_head') }}</span>
        </a>
    </li>
    <li>
    <li>
        <a href="{{url('helps/helps-property')}}" target='_blank'>
            <i class="el-help" style="margin-right: 5px;"></i>
            <span class="title">{{ trans('messages.help') }}</span>
        </a>
    </li>
    <li>
        <a href="{{url('auth/logout')}}">
            <i class="fa-power-off"></i>
            <span class="title">{{ trans('messages.Auth.logout') }}</span>
        </a>
    </li>
</ul> */ ?>

<ul id="main-menu" class="main-menu">
    <li class="{{ (Request::is('admin/dashboard') ? 'active' : '') }}">
        <a href="{{ url('admin/dashboard') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.dashboard') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('admin/messages/*') ? 'active' : '') }}">
        <a href="{{ url('admin/messages') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.chat') }}</span>
        </a>
    </li>
    <?php /* <li class="">
        <a href="{{ url('admin/dashboard') }}">
            <span class="title">Bill Payment</span>
        </a>
    </li> */ ?>
    <li class="@if(isset($active_menu) && $active_menu == "parcel-tracking") active @endif">
        <a href="{{ url('admin/parcel-tracking') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.post_parcel') }}</span>
        </a>
    </li>
    <li class="@if(isset($active_menu) && $active_menu == "news-announcement") active @endif">
        <a href="{{ url('admin/news-announcement') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.news_and_announcement') }}</span>
        </a>
    </li>

    <li class="@if(isset($active_menu) && $active_menu == "maintainance") active @endif">
        <a href="{{ url('admin/maintainance') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.maintainance') }}</span>
        </a>
    </li>

    <li class="@if(isset($active_menu) && $active_menu == "survey") active @endif">
        <a href="{{ url('admin/survey') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.survey') }}</span>
        </a>
    </li>
    <li class="@if( isset($active_menu) && ($active_menu == "members" || $active_menu == "units" ) ) active @endif">
        <a href="{{url('admin/property/units')}}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.user_management') }}</span>
        </a>
    </li>
    <!--<li class="@if(isset($active_menu) && ($active_menu == "members" || $active_menu == "units")) active @endif">
        <a href="#">
            <i class="fa fa-group"></i>
            <span class="title">{{ trans('messages.Member.page_head_group') }}</span>
        </a>
        <ul @if(isset($active_menu) && ($active_menu == "members" || $active_menu == "units")) style="display:block;" @endif>
            <li class="@if(isset($active_menu) && $active_menu == "units") active @endif">
                <a href="{{url('admin/property/units')}}">
                    <span class="title">{{ trans('messages.Prop_unit.page_head') }}</span>
                </a>
            </li>
            <li  class="@if(Request::is('admin/property/members')) active @endif">
                <a href="{{url('admin/property/members')}}">
                    <span class="title">{{ trans('messages.Member.page_head') }}</span>
                </a>
            </li>
            <li  class="@if(Request::is('admin/property/new-members')) active @endif">
                <a href="{{url('admin/property/new-members')}}">
                    <span class="title">{{ trans('messages.AdminUser.new_page_head') }}</span>
                </a>
            </li>
        </ul>
    </li> -->
    <?php /*
    <li class="">
        <a href="{{ url('admin/digital-form') }}">
            <span class="title">Digital Form</span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/service-request') }}">
            <span class="title">Service request</span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/marketplace') }}">
            <span class="title">Marketplace</span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/sale-rent') }}">
            <span class="title">Sale and Rent</span>
        </a>
    </li>

    <li>
        <a href="{{url('auth/logout')}}">
            <i class="fa-power-off"></i>
            {{--<span class="title">{{ trans('messages.Auth.logout') }}</span>--}}
            <span class="title">Log out</span>
        </a>
    </li>
 */ ?>
</ul>
