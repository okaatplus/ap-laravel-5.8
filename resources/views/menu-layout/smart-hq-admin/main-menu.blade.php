<ul id="main-menu" class="main-menu">
    <li class="{{ (Request::is('ssmart-hq-admin/admin/dashboard') ? 'active' : '') }}">
        <a href="{{url('smart-hq-admin/admin/dashboard')}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">{{ trans('messages.Menu.dashboard') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('smart-hq-admin/admin/news-announcement*') ? 'active' : '') }}">
        <a href="{{ url('smart-hq-admin/admin/news-announcement') }}">
            <i class="fa fa-bullhorn"></i>
            <span class="title">{{ trans('messages.Menu.news_and_announcement') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('smart-hq-admin/admin/property/list') ? 'active' : '') }}">
        <a href="{{ url('smart-hq-admin/admin/property/list') }}">
            <i class="fa fa-home"></i>
            <span class="title">{{ trans('messages.AdminProp.prop_list') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('smart-hq-admin/admin/admin-system/*') ? 'active' : '') }}">
        <a href="{{url('smart-hq-admin/admin/admin-system/list')}}">
            <i class="fa fa-user"></i>
            <span class="title">{{ trans('messages.Menu.user_management') }}</span>
        </a>
    </li>
</ul>
