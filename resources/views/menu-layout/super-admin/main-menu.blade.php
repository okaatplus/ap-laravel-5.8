<ul id="main-menu" class="main-menu">
    <!-- add class "multiple-expanded" to allow multiple submenus to open -->
    <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
    <li class="@if(Request::is('root/admin/property/*')) active @endif">
        <a href="{{ url('root/admin/property/list') }}">
            <i class="fa-home"></i>
            <span class="title">{{ trans('messages.AdminProp.prop_list') }}</span>
        </a>
    </li>

    <li class="{{ (Request::is('root/admin/admin-system/*') ? 'active' : '') }}">
        <a href="{{url('root/admin/admin-system/list')}}">
            <i class="fa fa-user"></i>
            <span class="title">{{ trans('messages.Menu.user_admin') }}</span>
        </a>
    </li>

    <li class="{{ (Request::is('root/admin/page/terms') ? 'active' : '') }}">
        <a href="{{url('root/admin/page/terms')}}">
            <i class="fa fa-file-o"></i>
            <span class="title">{{ trans('messages.Menu.edit_term_condition') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('root/admin/page/privacy') ? 'active' : '') }}">
        <a href="{{url('root/admin/page/privacy')}}">
            <i class="fa fa-file-o"></i>
            <span class="title">{{ trans('messages.Menu.edit_privacy') }}</span>
        </a>
    </li>
    <li class="{{ (Request::is('root/admin/brand/*') ? 'active' : '') }}">
        <a href="{{url('root/admin/brand/list')}}">
            <i class="fa fa-edit"></i>
            <span class="title">{{ trans('messages.AboutProp.brand') }}</span>
        </a>
    </li>
    <li>
        <a href="{{url('auth/logout')}}">
            <i class="fa-power-off"></i>
            <span class="title">{{ trans('messages.Auth.logout') }}</span>
        </a>
    </li>
</ul>
