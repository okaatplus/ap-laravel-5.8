<ul id="main-menu" class="main-menu">
    <li class="@if(isset($active_menu) && $active_menu == "parcel-tracking") active @endif">
        <a href="{{ url('technician/admin/parcel-tracking') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.post_parcel') }}</span>
        </a>
    </li>
    <li class="@if(isset($active_menu) && $active_menu == "messages") active @endif">
        <a href="{{ url('technician/admin/messages') }}">
            <i class="fa-circle-o"></i>
            <span class="title">{{ trans('messages.Menu.chat') }}</span>
        </a>
    </li>
</ul>
