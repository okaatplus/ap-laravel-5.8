@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Message.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="/"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Message.page_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <section class="mailbox-env">
        <div class="row" style="display: flex;">
            <div class="col-md-3 mailbox-left">
                <div class="panel panel-default" style="padding-bottom:0;margin-bottom: 0;">
                    <div class="panel-body search-form">
                        <div class="member-search row">
                            <div class="col-sm-12 block-input">
                            	<input class="form-control datepicker" size="25" placeholder="{{ trans('messages.name') }}" name="name" id="member-name-input">
                            </div>
                            <div class="col-sm-12 block-input">
                            	{!! Form::select('unit_id', $unit_list,null,['class'=>'form-control select2','id'=>'member-address-input']) !!}
                            </div>
                        </div>
                        <div class="member-list row" id="member-list">
                            @include('message.user-list')
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 mailbox-right" style="flex: 1;">
                <div class="panel panel-default" style="height: 100%;">
                    <div class="panel-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-10 block-input">
                                    <input class="form-control" size="25" placeholder="Keyword" name="keyword" id="msg-key">
                                </div>
                                <div class="col-sm-2 block-input">
                                    <button type="button" class="btn btn-primary" id="search-msg">Search</button>
                                </div>
                            </div>
                        </form>
                        <hr style="margin: 5px 0;"/>
                        <div id="messages-list">
                            @include('message.admin-user-message-list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-message" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Message.send_message_head')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="post-report-form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-12" for="reason">{{ trans('messages.Message.admin_send_message_label') }}<span id="user-name"></span></label>
                                    <div class="col-sm-12">
                                        <textarea id="input-message" name="text" class="form-control"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.cancel')}}</button>
                    <button type="button" class="btn btn-primary" id="send-message">{{trans('messages.Message.send_message')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
<script>
    var uid;
    $(function () {
        $('.member-list').on('click','.message-user', function () {
            $('#user-name').html($(this).find('.name').html());
            uid = $(this).data('uid');
        });

        $("#member-address-input").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });

        $('#send-message').on('click',function () {
            var btn = $(this);
            if($("#input-message").val() !== "" ) {
                btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : $('#root-url').val()+"/admin/message/user/send",
                    data    : ({ 'text':$("#input-message").val(),'uid':uid }),
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        btn.removeAttr('disabled').find('.fa-spinner').remove();
                        $("#input-message").val('');
                        $('#modal-message').modal('hide');
                        loadMessagePage(1);
                    }
                });
            }
        })

        $('#member-name-input').on('keydown',function () {
            getMember(1);
        });
        $('#member-address-input').on('change',function () {
            getMember(1);
        })

        $('#member-list').on('click','.next-page,.prev-page',function (e) {
            e.preventDefault();
            getMember($(this).data('page'));
        })

        $('#messages-list').on('click','.paginate-link',function (e) {
            loadMessagePage($(this).data('page'));
        });

        $('#messages-list').on('change','.paginate-select',function (e) {
            loadMessagePage($(this).val());
        });

        $('#search-msg').on('click',function (e) {
            loadMessagePage(1);
        });
    })

    function loadMessagePage (page) {
        var k = $('#msg-key').val();
        $("#messages-list").css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/admin/messages",
            data    : ({ 'page':page,'keyword':k }),
            method  : 'post',
            dataType: 'html',
            success: function(h){
                $("#messages-list").html(h).css('opacity','1');
            }
        });
    }

    function getMember (page) {
        $("#member-list").css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/admin/message/member/get",
            data    : ({ 'page':page,'name':$('#member-name-input').val(),'unit_id':$('#member-address-input').val() }),
            method  : 'post',
            dataType: 'html',
            success: function(h){
                 $("#member-list").html(h).css('opacity','1');
            }
        });

    }
</script>
<link rel="stylesheet" href="{{url('/')}}/js/datatables/dataTables.bootstrap.css"/>
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
@endsection
