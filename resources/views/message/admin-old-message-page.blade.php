
@if($messageTexts && $messageTexts->count())
    <?php /* @if( $messageTexts->hasMorePages() )
    <div id="load-old" data-page="{{ $messageTexts->currentPage() +1 }}">  {{ trans('messages.Message.load_message') }} </div>
    @endif */ ?>
    <div id="landing-old-text">
        <div id="load-old" data-date="{{ $messageTexts->last()->created_at }}">  {{ trans('messages.Message.load_message') }} </div>
    </div>
    @include('message.message-list-element')
@endif
