@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Message.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Message.page_head') }}</strong>
                </li>
            </ol>
      </div>
    </div>
    <section class="mailbox-env">
        <div class="row">
            <div class="col-md-8 mailbox-left">
                <div class="panel panel-default">
                    @include('message.message-block')
                </div>
            </div>
            <div class="col-md-4 mailbox-right">
                @include('message.user-profile')
                @include('message.view-log-block')
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/app-scripts/admin-chat.js?v={{time()}}"></script>
    <script type="text/javascript" src="{{url('/')}}/js/autosize.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script>
    $(function () {
        autosize($('#txt-msg'));
        //$('#landing-text-block').perfectScrollbar();
        $('#send-message').on('click',function () {
            var btn = $(this);
            if( $("#txt-msg").val() !== "" || $('.preview-img-item').length ) {
                btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : $('#root-url').val()+"/admin/message/send",
                    data    : $('.message-form').serialize(),//({ 'mid':$('#hidden-mid').val(),'text':$("#input-message").val() }),
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        btn.removeAttr('disabled').find('.fa-spinner').remove();
                        $("#txt-msg").val('');
                        $('.remove-img-preview').trigger('click');
                        $('#landing-new-message').append(r.box);
                        var d = $('#landing-text-block');
                        d.scrollTop(d.prop("scrollHeight"));
                    }
                });
            }
        });

        var channel2 = pusher.subscribe('{{ $message->id }}');
        channel2.bind('chat_message', function(data) {
            $.ajax({
                url     : $('#root-url').val()+"/admin/message/receive",
                data    : "message_text_id="+data.id,
                method  : 'post',
                dataType: 'json',
                success: function(r){
                    //btn.removeAttr('disabled').find('.fa-spinner').remove();
                    $("#txt-msg").val('');
                    $('.remove-img-preview').trigger('click');
                    $('#landing-new-message').append(r.box);
                    var d = $('#landing-text-block');
                    d.scrollTop(d.prop("scrollHeight"));
                }
            });
        });
    });
</script>
    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
