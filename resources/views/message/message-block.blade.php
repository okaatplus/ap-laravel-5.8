<div class="panel-body">
    <div id="landing-text-block">
        <div id="landing-text" class="admin-view">
            @include('message.admin-old-message-page')
            <div id="landing-new-message"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr/>
    <form method="post" action="" class="message-form">
        <div class="messages-box">
            <textarea name="text" id="txt-msg"></textarea>
            <div id="previews"></div>
            <div class="chat-action">
                <i class="fa fa-file-image-o" id="attachment"></i>
            </div>
        </div>
        <button type="button" id="send-message" class="btn btn-primary pull-right send-message"><i class="fa fa-comment"></i> {{ trans('messages.Message.send_message')}}</button>
        @php
            if( isset($messageTexts) && $messageTexts->count() > 0 ){
                $mid = $messageTexts->first()->message_id;
            }else
            {
                $mid = "";
            }
        @endphp
        <input type="hidden" name="mid" id="hidden-mid" value="{{ $mid }}"/>
        <input type="hidden" name="uid" id="hidden-uid" value="{{ $user->id }}"/>
    </form>
</div>