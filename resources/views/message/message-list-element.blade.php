@foreach($messageTexts->reverse() as $message)
    <div class="col-md-12">
        <div class="message @if($message->is_admin_reply) admin-reply @else user-reply @endif">
            <p>{!! nl2br(e($message->text)) !!}</p>
            @if( $message->messageTextFile )
            <div>
                @foreach( $message->messageTextFile as $file )
                <a class="fancybox" rel="gal-{{$message->message_id}}" href="{{ env('URL_S3') }}/messages-file/{{$file->url.$file->name}}">
                    <img src="{{ env('URL_S3') }}/messages-file/{{ $file->url.$file->name }}" alt="album-image" />
                </a>
                @endforeach
            </div>
            @endif
            <time>{{ chatTime($message->created_at)  }}</time>
            @if ($message->read_status && $message->is_admin_reply) <read>Read</read> @endif
            @if( $message->is_admin_reply )
            <span class="res-by">{{ $message->owner->name }}</span>
            @endif
        </div>
    </div>
@endforeach