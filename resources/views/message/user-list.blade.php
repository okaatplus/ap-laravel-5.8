@if($members->count())

<div class="user-list-pagination">
    <div class="all-member">({{ $members->total() }})</div>
    <div class="next-prev">
       <a href="#" class="prev-page @if($members->currentPage() == 1) disabled @endif" data-page="{{ $members->currentPage()-1 }}"><i class="fa-angle-left"></i></a>
        <a href="#" class="next-page @if(!$members->hasMorePages()) disabled @endif" data-page="{{ $members->currentPage()+1 }}"><i class="fa-angle-right"></i></a>
    </div>
    <div class="clearfix"></div>
</div>
@foreach($members as $member)
<a href="{{ url('admin/message/send/'.$member->id) }}" class="div-user-image user-image message-user" data-uid="{{ $member->id }}">
    @if($member->profile_pic_name)
    <img style="margin-top:0;" src="{{ env('URL_S3')."/profile-img/".$member->profile_pic_path.$member->profile_pic_name }}" class="img-circle" alt="user-pic" />
    @else
    <img style="margin-top:0;" src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
    @endif
    <div class="user-detail" style="margin-top:7px;">

        <?php
            $unit_number_arr = [];
            foreach ($member->ActiveUnit as $au) {
                $unit_number_arr[] = $au->unit->unit_number;
            }
        ?>
        <span class="name">{{ $member->name }}</span><br>
            <span style="font-size: 12px;">{{ trans('messages.unit_no') }} : {{ implode(',',$unit_number_arr) }}</span>
    </div>
</a>
@endforeach
@else
<div class="no-member">{{ trans('messages.Member.member_not_found') }}</div>
@endif
