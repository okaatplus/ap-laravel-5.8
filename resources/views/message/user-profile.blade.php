<?php
    $unit_number_arr = [];
    $state = [];
    foreach ($user->ActiveUnit as $au) {
        $unit_number_arr[] = $au->unit->unit_number;
        //user_type
        if($au->user_type == 0)
            $state[] = trans('messages.Member.member_owner');
        elseif($au->user_type == 1)
            $state[] = trans('messages.Member.member_crew');
        else
            $state[] = trans('messages.Member.member_tenant');
    }
?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="mr-3 float-left">
            <div class="avatar">
                @if($user->profile_pic_name)
                    <img style="height:36px;margin-top:0;" src="{{ env('URL_S3')."/profile-img/".$user->profile_pic_path.$user->profile_pic_name }}" class="img-circle" alt="user-pic" />
                @else
                    <img style="height:36px;margin-top:0;" style="margin-top:0;" src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
                @endif
                <span class="avatar-status badge-success"></span>
            </div>
        </div>
        <div class="text-truncate font-weight-bold">
            <b style="font-size: 18px;">{{ $user->name }}</b>
        </div>
        <div class="small text-muted text-truncate">
            {{ trans('messages.unit_no') }} : {{ implode(',',$unit_number_arr) }}
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-3">
                <b>{{ trans('messages.Member.member_account_status') }} :</b>
            </div>
            <div class="col-xs-9">
                {{ implode(',',$state) }}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <b>{{ trans('messages.email') }} :</b>
            </div>
            <div class="col-xs-9">
                {{ $user->email }}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <b>{{ trans('messages.tel') }} :</b>
            </div>
            <div class="col-xs-9">
                {{ $user->phone }}
            </div>
        </div>
    </div>
</div>