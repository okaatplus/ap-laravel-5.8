<div class="panel panel-default">
    <div class="panel-heading">
        Latest seen by
    </div>
    <div class="panel-body" style="padding-top: 10px;">
        <div class="clearfix"></div>
        @foreach( $message->hasViewLog  as $log)
            @php $viewer = $log->viewBy; @endphp
            <div class="mr-3 float-left">
                <div class="avatar">
                    @if($viewer->profile_pic_name)
                        <img style="height:36px;margin-top:6px;" src="{{ env('URL_S3')."/profile-img/".$viewer->profile_pic_path.$viewer->profile_pic_name }}" class="img-circle" alt="user-pic" />
                    @else
                        <img style="height:36px;margin-top:0;" style="margin-top:6px;" src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
                    @endif
                </div>
            </div>
            <div class="text-truncate">
                <b>{{ $viewer->name }}</b>
            </div>
            <div class="small text-muted text-truncate">
                {{ trans('messages.Role.'.$viewer->role) }} <span style="float: right;">{{ chatTime($log->latest_view_date)  }}</span>
            </div>
            <div class="clearfix" style="border-bottom: 1px solid #f8f8f8;margin-bottom: 10px;"></div>
        @endforeach
    </div>
</div>