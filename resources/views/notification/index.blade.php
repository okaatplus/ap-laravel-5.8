@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Noti.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{url('/')}}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
				<li class="active">
					<strong>{{ trans('messages.Noti.page_head') }}</strong>
				</li>
			</ol>
		</div>
	</div>
        <section class="mailbox-env">
            <div class="row">
                <div class="col-sm-12 mailbox-right" style="position:relative;">
                    <div class="mail-env" id="noti-list-content">
                        @include('notification.notification-list')
                    </div>
                </div>
            </div>
        </section>
    <div class="modal fade" id="modal-notification-detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="notification-content"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                </div>
            </div>
         </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/notification.js"></script>
@endsection
