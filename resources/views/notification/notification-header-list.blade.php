
<li class="nav-item dropdown d-md-down-none">
    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="icon-bell"></i>
        @if( $noti_count )
            <span class="badge badge-pill badge-info">{{ $noti_count }}</span>
        @endif
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
        <div class="dropdown-header text-center">
            <strong>You have {{ $noti_count }} Notifications</strong>
        </div>
        <div class="header-notif-list" style="max-height: 298px; overflow-y: auto;">
        @foreach($noti_list as $noti)
        @php
            $url = $class_read = "";
            $data = json_decode($noti->title,true);
            if($noti->read_status == 0) $class_read = "mark-as-read";

            // set url
            if($noti->notification_type == 3)
            $url = '/admin/property/new-members';
        @endphp
        <a class="dropdown-item  {{ $class_read }}" href="{{ url($url) }}" data-noti-id="{{$noti->id}}">
            <div class="message-bag">
                <div class="mr-3 float-left">
                    <div class="avatar">
                        @if($noti->sender->profile_pic_name)
                            <img src="{{ env('URL_S3')."/profile-img/".$noti->sender->profile_pic_path.$noti->sender->profile_pic_name }}" alt="user-image" class="img-avatar" />
                        @else
                            <img src="{{ url('/') }}/images/user-4.png" alt="user-image" class="img-avatar"/>
                        @endif
                    </div>
                </div>
                <div>
                    <small class="text-muted">{{ $noti->sender->name }}</small>
                    <small class="text-muted float-right mt-1">{{ humanTiming($noti->created_at) }}</small>
                </div>
                <div class="text-truncate font-weight-bold">
                    {{ trans('messages.Notification.'.$data['type']) }}
                </div>
            </div>
        </a>
        @endforeach
        </div>
        @if( $noti_list->count() )
        <a class="dropdown-item text-center" href="{{ url('notification') }}" style="border-top: 1px solid #ddd;">
            <strong>View all Notifications</strong>
        </a>
        @endif
    </div>
</li>
