 @if($notis->count() > 0)
 <?php
    $from   = (($notis->currentPage()-1)*$notis->perPage())+1;
    $to     = (($notis->currentPage()-1)*$notis->perPage())+$notis->perPage();
    $to     = ($to > $notis->total()) ? $notis->total() : $to;
    $lang   = App::getLocale(); ?>
 <table class="table mail-table">
    <thead>
        <tr>
            <th class="col-header-options">
                <div class="mail-pagination">
                    {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$notis->total()]) !!}
                    <div class="next-prev">
                       <a href="#" class="prev-page @if($notis->currentPage() == 1) disabled @endif" data-page="{{($notis->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                        <a href="#" class="next-page @if(!$notis->hasMorePages()) disabled @endif" data-page="{{($notis->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                    </div>
                </div>
            </th>
        </tr>
    </thead>
 </table>
 <div class="table-responsive dataTables_wrapper" style="border:none;">
 <table class="table mail-table" style="min-width:640px;table-layout:fixed;">
        <!-- email list -->
        <tbody>
        	@foreach($notis as $noti)
                @php
                    $url = $class_read = "";
                    $data = json_decode($noti->title,true);
                    if($noti->read_status == 0) $class_read = "mark-as-read";

                    // set url
                    if($noti->notification_type == 3)
                    $url = '/admin/property/new-members';
                @endphp

                <tr class="@if($noti->read_status == 0) unread @endif">
                    <td class="col-name" width="35%">
                        @if($noti->sender->profile_pic_name)
                            <img src="{{ env('URL_S3')."/profile-img/".$noti->sender->profile_pic_path.$noti->sender->profile_pic_name }}" alt="user-image" class="img-circle mini-profile-img" />
                        @else
                            <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle mini-profile-img" />
                        @endif
                        <a href="{{ url($url) }}" class="mark-as-read" data-noti-id="{{$noti->id}}">
                            {{ $noti->sender->name }}
                        </a>
                    </td>
                    <td class="col-subject" width="42%">
                        <a href="{{ url($url) }}" class="mark-as-read" data-noti-id="{{$noti->id}}">
                            @if($noti->read_status == 0)<span class="label label-danger">ใหม่</span>@endif
                            {{ trans('messages.Notification.'.$data['type']) }}
                        </a>

                    </td>
                    <td style="text-align:right;padding-right:20px;">{{ humanTiming($noti->created_at) }}</td>
                </tr>
        	@endforeach
        </tbody>
    </table>
</div>
<table class="table mail-table">
    <thead>
        <tr>
            <th class="col-header-options" style="border-bottom:0px;">
                <div class="mail-pagination">
                    {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$notis->total()]) !!}
                    <div class="next-prev">
                        <a href="#" class="prev-page @if($notis->currentPage() == 1) disabled @endif" data-page="{{($notis->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                        <a href="#" class="next-page @if(!$notis->hasMorePages()) disabled @endif" data-page="{{($notis->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                    </div>
                </div>
            </th>
        </tr>
    </thead>
</table>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Noti.noti_not_found') }}</div><div class="clearfix"></div>
@endif
