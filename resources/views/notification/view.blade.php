
 <?php $data = json_decode($notis->title,true); ?>

<?php 
	if( $data['type'] == 'general') {
		$t = $data['n_title'];
		$d = nl2br(e($notis->description));
	} elseif ( $data['type'] == 'sticker_approved' ) {
		$t = trans('messages.Notification.sticker_approved_head');
		$d = trans('messages.Notification.sticker_approved_msg',['plate'=> $data['plate_sn']]);
	} elseif( $data['type'] == 'invoice_created') {
		$t = trans('messages.Notification.invoice_created_head');
		$d = trans('messages.Notification.invoice_created_msg',['name'=> $data['title']]);
	} elseif( $data['type'] == 'event_created') {
		$t = trans('messages.Notification.event_created_head');
		$d = trans('messages.Notification.event_created_msg',['name'=> $data['title']]);
	} elseif( $data['type'] == 'vote_created') {
		$t = trans('messages.Notification.vote_created_head');
		$d = trans('messages.Notification.vote_created_msg',['name'=> $data['title']]);
	} elseif( $data['type'] == 'receive_post_parcel') {
		$t = trans('messages.Notification.receive_post_parcel_head');
		$d = trans('messages.Notification.receive_post_parcel_msg',$data);
	}		
?>
<div class="row">
	<div class="col-md-4"><div class="form-group"><b>{{ trans('messages.title') }}</b></div></div>
	<div class="col-md-8"><div class="form-group">{!! $t !!}</div></div>
</div>
<div class="row">
	<div class="col-md-4"><div class="form-group"><b>{{ trans('messages.detail') }}</b></div></div>
	<div class="col-md-8"><div class="form-group">{!! $d !!}</div></div>
</div>
<div class="row">
	<div class="col-md-4"><div class="form-group"><b>{{ trans('messages.Noti.sender') }}</b></div></div>
	<div class="col-md-8"><div class="form-group">{{ $notis->sender->name }}</div></div>
</div>
<div class="row">
	<div class="col-md-4"><div class="form-group"><b>{{ trans('messages.Noti.send_date') }}</b></div></div>
	<div class="col-md-8"><div class="form-group">{{ date('d F Y - H:i',strtotime($notis->created_at)) }}</div></div>
</div>