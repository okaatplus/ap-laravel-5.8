@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Message.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="/"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Message.page_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <section class="mailbox-env">
        <div class="row" style="display: flex;">
            <div class="col-md-12" style="flex: 1;">
                <div class="panel panel-default" style="height: 100%;">
                    <div class="panel-body" id="messages-list">
                        @include('outsource.message.admin-user-message-list')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
<script>
    $(function () {

        $('#messages-list').on('click','.paginate-link',function (e) {
            loadMessagePage($(this).data('page'));
        });

        $('#messages-list').on('change','.paginate-select',function (e) {
            loadMessagePage($(this).val());
        })
    })

    function loadMessagePage (page) {
        $("#messages-list").css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/outsource/admin/messages",
            data    : ({ 'page':page }),
            method  : 'post',
            dataType: 'html',
            success: function(h){
                $("#messages-list").html(h).css('opacity','1');
            }
        });
    }
</script>
@endsection
