@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ $page_name }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{ url('/') }}"><i class="fa-home"></i>Home</a>
				</li>
				<li class="active">
					<strong>{{ $page_name }}</strong>
				</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
					</div>
				</div>
				<div class="panel-body">
					{!! Form::model($page,array('url' => array('root/admin/page/edit'),'class'=>'form-horizontal')) !!}
					<div class="row">
						<label class="col-sm-3 control-label text-left">{{ trans('messages.text') }} (en)</label>
						<div class="col-sm-12 block-input">
							{!! Form::textarea('content_en', null,['id'=>'page-content','class'=>'form-control']) !!}
						</div>
						{!! Form::hidden('id') !!}
					</div>

					<div class="row">
						<label class="col-sm-3 control-label text-left">{{ trans('messages.text') }} (th)</label>
						<div class="col-sm-12 block-input">
							{!! Form::textarea('content_th', null,['id'=>'page-content','class'=>'form-control']) !!}
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 block-input text-right">
							<a onclick="window.location.reload(true);" class="btn btn-white">ยกเลิก</a>
							<input type="submit" class="submit-btn btn btn-primary" value="บันทึก" />
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
	<script src="{{ url('js/tinymce/tinymce.min.js') }}"></script>
	<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
	<script>
		tinymce.init({
			selector:"textarea",
			plugins:"link image media",
			relative_urls: false,
			remove_script_host: false,
			height: 300,
			theme: 'modern',
			content_css : ['{{ url('/') }}/help/css/theDocs.all.min.css','{{ url('/') }}/help/css/custom.css','{{ url('/') }}/font/thaisans/font.css','{{ url('/') }}/home-theme/css/helps-media-query.css'],
			content_style: "body { padding: 20px 40px; font-size:22px !important;}",
			plugins: [
				'advlist autolink lists link image charmap print preview hr anchor pagebreak',
				'searchreplace wordcount visualblocks visualchars code fullscreen',
				'insertdatetime media nonbreaking save table contextmenu directionality',
				'emoticons template paste textcolor colorpicker textpattern imagetools'
			],
			toolbar: 'insertfile undo redo | styleselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image |',
			image_advtab: true,
			file_browser_callback: function(field, url, type, win) {
				tinyMCE.activeEditor.windowManager.open({
					file: '{{ url("storage/kcfinder/browse.php") }}?opener=tinymce4&field=' + field + '&type=' + type,
					title: 'Image contents',
					width: 700,
					height: 500,
					inline: true,
					close_previous: false
				}, {
					window: win,
					input: field
				});
				return false;
			}
		});
	</script>
@endsection
