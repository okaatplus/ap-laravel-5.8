@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Payee.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{url('/')}}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
                <li>
                    <a href="#">{{ trans('messages.feesBills.expense') }}</a>
                </li>
				<li class="active">
					<strong>{{ trans('messages.Payee.page_head') }}</strong>
				</li>
			</ol>
		</div>
	</div>
    <a href="#" data-toggle="modal" data-target="#add-payee-modal" class="action-float-right btn btn-primary">{{ trans('messages.Payee.add_payee_label') }} </a><span></span>
    <section>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('messages.Payee.page_head') }}</h3>
            </div>
             <div class="panel-body" id="panel-payee-list">
               @include('payee.payee-list-element')
            </div>
        </div>
    </section>

    <div class="modal fade modal-inline-form" id="add-payee-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('url' => array('admin/expenses/payee/add'),'class'=>'form-horizontal','id'=>'create-payee-form','method'=>'post')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Payee.add_payee_label') }}</h4>
                </div>
                <div class="modal-body">
                    <div style="padding:0px 15px;">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.Payee.name') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::text('name',null,array('class'=>'form-control','maxlength'=>'100')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.AboutProp.address') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::textarea('address',null,array('class'=>'form-control','maxlength'=>'200','rows'=>'5')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.tel') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::text('phone',null,array('class'=>'form-control')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.fax') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::text('fax',null,array('class'=>'form-control','maxlength'=>'50')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.email') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::text('email',null,array('class'=>'form-control','maxlength'=>'50')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.AboutProp.tax_id') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::text('tax',null,array('class'=>'form-control','maxlength'=>'50')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.Payee.note') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::textarea('note',null,array('class'=>'form-control','maxlength'=>'200','rows'=>'3')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="add-payee-btn">{{ trans('messages.add') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade modal-inline-form" id="edit-payee-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('url' => array('admin/expenses/payee/edit'), 'class'=>'form-horizontal','id'=>'edit-payee-form','method'=>'post')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Payee.add_payee_label') }}</h4>
                </div>
                <div class="modal-body" id="payee-content">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="edit-payee-btn">{{ trans('messages.save') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/number.js"></script>
<script type="text/javascript">
    $(function () {
        $('#create-payee-form').validate({
            rules: {
                name    : 'required',
                address  : 'required',
                phone   : 'required',
                email   : 'email'
            },
            errorPlacement: function(error, element) {}
        })

        $('#edit-payee-form').validate({
            rules: {
                name    : 'required',
                address  : 'required',
                phone   : 'required',
                email   : 'email'
            },
            errorPlacement: function(error, element) {}
        })

        $('#add-payee-btn').on('click',function () {
            if($('#create-payee-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#create-payee-form').submit();
            }
        })

        $('#panel-payee-list').on('click','.edit-payee' ,function (e){
            e.preventDefault();
            var btn = $(this);
            btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i>').find('.fa-edit').remove();
            var id = $(this).data('payee-id');
            $.ajax({
                url : $('#root-url').val()+"/admin/expenses/payee/edit",
                method : 'post',
                dataType: 'html',
                data : ({'id':id}),
                success: function (r) {
                    $('#payee-content').html(r);
                    $('#edit-payee-modal').modal('show');
                    btn.removeAttr('disabled').append('<i class="fa-edit"></i>').find('.fa-spin').remove();
                }
            })
        })

        $('#edit-payee-btn').on('click',function () {
            if($('#edit-payee-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i>');
                $('#edit-payee-form').submit();
            }
        })

        $('#panel-payee-list').on('click','.paginate-link', function (e){
            e.preventDefault();
            var page = $(this).data('page');
            searchpayee (page);
        });

        $('#panel-payee-list').on('change','.paginate-select', function (e){
            searchpayee ($(this).val());
        });

        function searchpayee (page) {
            var data = $('#v-search').serialize();
            data+= "&page="+page;
            $('#panel-payee-list').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/admin/expenses/payee",
                method  : "POST",
                data    : data,
                dataType: "html",
                success: function (t) {
                    $('#panel-payee-list').html(t).css('opacity','1');
                }
            })
        }
    })
</script>
<style type="text/css">
.cbr-replaced {
    margin-top: -5px;
}
</style>
@endsection
