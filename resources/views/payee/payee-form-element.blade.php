<div style="padding:0px 15px;">
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.Payee.name') }}</label>
            <div class="col-sm-8">
                {!! Form::text('name',$payee->name,array('class'=>'form-control','maxlength'=>'100')) !!}
                {!! Form::hidden('id',$payee->id) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.AboutProp.address') }}</label>
            <div class="col-sm-8">
                {!! Form::textarea('address',$payee->address,array('class'=>'form-control','maxlength'=>'500','rows'=>'5')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.tel') }}</label>
            <div class="col-sm-8">
                {!! Form::text('phone',$payee->phone,array('class'=>'form-control','maxlength'=>'100')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.fax') }}</label>
            <div class="col-sm-8">
                {!! Form::text('fax',$payee->fax,array('class'=>'form-control','maxlength'=>'50')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.email') }}</label>
            <div class="col-sm-8">
                {!! Form::text('email',$payee->email,array('class'=>'form-control','maxlength'=>'100')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.AboutProp.tax_id') }}</label>
            <div class="col-sm-8">
                {!! Form::text('tax',$payee->tax,array('class'=>'form-control','maxlength'=>'100')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label col-sm-4">{{ trans('messages.Payee.note') }}</label>
            <div class="col-sm-8">
                {!! Form::textarea('note',$payee->note,array('class'=>'form-control','maxlength'=>'200','rows'=>'3')) !!}
            </div>
        </div>
    </div>
</div>