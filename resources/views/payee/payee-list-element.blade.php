@if($payees->count())
<?php
    $allpage = $payees->lastPage();
    $curPage = $payees->currentPage();
    $from   = (($payees->currentPage()-1)*$payees->perPage())+1;
    $to     = (($payees->currentPage()-1)*$payees->perPage())+$payees->perPage();
    $to     = ($to > $payees->total()) ? $payees->total() : $to;
 ?>
<div class="row">
    <div class="col-md-4" style="margin-bottom: 10px;">
        <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
            {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$payees->total()]) !!}
        </div>
    </div>
    <div class="col-md-8">
        <div class="text-right" >
            @if($allpage > 1)
                @if($payees->currentPage() > 1)
                    <a class="btn btn-white paginate-link" href="#" data-page="{{ $payees->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                @endif
                @if($payees->lastPage() > 1)
                    <?php echo Form::selectRange('page', 1, $payees->lastPage(),$payees->currentPage(),['class'=>'form-control paginate-select']); ?>
                @endif
                @if($payees->hasMorePages())
                    <a class="btn btn-white paginate-link" href="#" data-page="{{ $payees->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                @endif
            @endif
        </div>
    </div>
</div>
@foreach( $payees as $payee )
<div class="invoice-row">
    <div class="invoice-head">
		<span class="check-delete">
			<label>
				{{NB_PAYEE.payeeNumber($payee->payee_no)}} : {{ $payee->name }}
			</label>
		</span>
    </div>
    <div class="col-md-4">{{ trans('messages.tel') }} : {{ ($payee->phone != "")? $payee->phone : "-" }}</div>
    <div class="col-md-4">{{ trans('messages.fax') }} : {{ ($payee->fax != "")? $payee->fax : "-" }}</div>
    <div class="col-md-4">{{ trans('messages.email') }} : {{ ($payee->email != "")? $payee->email : "-" }}</div>
    <div class="col-md-4">{{ trans('messages.AboutProp.tax_id') }} : {{ ($payee->tax != "")? $payee->tax : "-" }} </div>
    <div class="col-md-4">{{ trans('messages.AboutProp.address') }} : {{ ($payee->address != "")? $payee->address : "-" }} </div>
    <div class="col-md-4">{{ trans('messages.Payee.note') }} : {{ ($payee->note != "")? $payee->note : "-" }}</div>
    <div class="col-md-12 action-links text-right">
        <a href="#" class="edit-payee btn btn-warning" data-payee-id="{{ $payee->id }}">
            <i class="fa-edit"></i>
        </a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="col-md-4" style="margin-bottom: 10px;">
        <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
            {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$payees->total()]) !!}
        </div>
    </div>
    <div class="col-md-8">
        <div class="text-right" >
            @if($allpage > 1)
                @if($payees->currentPage() > 1)
                    <a class="btn btn-white paginate-link" href="#" data-page="{{ $payees->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                @endif
                @if($payees->lastPage() > 1)
                    <?php echo Form::selectRange('page', 1, $payees->lastPage(),$payees->currentPage(),['class'=>'form-control paginate-select']); ?>
                @endif
                @if($payees->hasMorePages())
                    <a class="btn btn-white paginate-link" href="#" data-page="{{ $payees->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                @endif
            @endif
        </div>
    </div>
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Payee.payee_not_found') }}</div><div class="clearfix"></div>
@endif