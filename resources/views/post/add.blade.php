@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
				<li><a href="{{ url('admin/news-announcement') }}">{{ trans('messages.Post.page_head') }}</a></li>
				<li class="active">
					<strong>{{ trans('messages.Post.add_post') }}</strong>
				</li>
			</ol>
		</div>
	</div>

	{!! Form::model($post,array('url' => array('admin/news-announcement/save'),'class'=>'form-horizontal')) !!}
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">{{ trans('messages.Post.category.1') }}</h3>
				</div>
				<div class="panel-body">
					@include('post.post-form')
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default text-right">
				<a class="btn btn-gray" href="{{url('admin/news-announcement')}}">{{trans('messages.back')}}</a>
				{!! Form::button(trans('messages.Post.save_draft'), ['class'=>'btn btn-warning','id'=>'save-draft-form']) !!}
			</div>
		</div>
	</div>
	{!! Form::close() !!}
@endsection
