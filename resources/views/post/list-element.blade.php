@if($posts->count() > 0)
    <?php
    $key = $from   = (($posts->currentPage()-1)*$posts->perPage())+1;
    $to     = (($posts->currentPage()-1)*$posts->perPage())+$posts->perPage();
    $to     = ($to > $posts->total()) ? $posts->total() : $to;
    $allpage = $posts->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>
	<div class="row">
		<div class="col-md-4" style="margin-bottom: 10px;">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$posts->total()]) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="text-right" >
				@if($allpage > 1)
					@if($posts->currentPage() > 1)
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
					@endif
					@if($posts->lastPage() > 1)
                        <?php echo Form::selectRange('page', 1, $posts->lastPage(),$posts->currentPage(),['class'=>'form-control paginate-select']); ?>
					@endif
					@if($posts->hasMorePages())
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts->currentPage()+1 }}">{{ trans('messages.next') }}</a>
					@endif
				@endif
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="p-list" width="100%">
				<thead>
				<tr>
					<th width="7%">{{ trans('messages.Report.sequence') }}</th>
					<th>{{ trans('messages.Post.page_head') }}</th>
					<th width="18%">{{ trans('messages.Post.post_type') }}</th>
					<th width="10%">{{ trans('messages.Post.created_at') }}</th>
					<th width="13%" class="text-center">{{ trans('messages.Post.publish_status') }}</th>
					<th width="230px" class="text-center">{{ trans('messages.action') }}</th>
				</tr>
				</thead>
			@foreach($posts as $post)
				<tr>
					<td class="text-center">{{ $key++ }}</td>
					<td>{{ $post->title_th }}</td>
					<td>{{ trans('messages.Post.type.'.$post->post_type) }}</td>
					<td>
						{{ localDateShortNotime( $post->created_at) }}
					</td>
					<td class="text-center">
						@if( $post->publish_state == 2 )
							{{ trans('messages.Post.publish') }}
						@elseif( $post->publish_state == 1 )
							{{ trans('messages.Post.waiting_approve') }}
						@else
							{{ trans('messages.Post.draft') }}
						@endif
					</td>
					<td class="action-links text-center">
						@if( $post->publish_state == 0 )
						<a class="btn btn-success send-approve" href="#" data-id="{{ $post->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.Post.send_approve') }}">
							<i class="fa fa-check-circle-o"></i>
						</a>
						@else
						<a class="btn btn-default disabled" href="javascript:">
							<i class="fa fa-check-circle-o"></i>
						</a>
						@endif

						<a class="btn btn-info" target="_blank" href="{{ url('admin/news-announcement/view') }}/{{ $post->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.view') }}">
							<i class="fa fa-eye"></i>
						</a>

						@if( $post->publish_state == 0 )
						<a class="btn btn-warning" href="{{ url('admin/news-announcement/edit') }}/{{ $post->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.edit') }}">
							<i class="fa fa-pencil"></i>
						</a>
						<a class="btn btn-danger post-delete" href="#" data-id="{{ $post->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.delete') }}">
							<i class="fa fa-trash-o"></i>
						</a>
						@else
						<a class="btn btn-default disabled" href="javascript:">
							<i class="fa fa-pencil"></i>
						</a>
						<a class="btn btn-default disabled" href="javascript:">
							<i class="fa fa-trash-o"></i>
						</a>
						@endif

						@if($post->publish_state == 1)
						<a class="btn btn-blue post-recall" href="#" data-id="{{ $post->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.Post.recall') }}">
							<i class="fa fa-recycle"></i>
						</a>
						@else
						<a class="btn btn-default disabled" href="javascript:">
							<i class="fa fa-recycle"></i>
						</a>
						@endif
					</td>
				</tr>
			@endforeach
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$posts->total()]) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="text-right" >
				@if($allpage > 1)
					@if($posts->currentPage() > 1)
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
					@endif
					@if($posts->lastPage() > 1)
                        <?php echo Form::selectRange('page', 1, $posts->lastPage(),$posts->currentPage(),['class'=>'form-control paginate-select']); ?>
					@endif
					@if($posts->hasMorePages())
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $posts->currentPage()+1 }}">{{ trans('messages.next') }}</a>
					@endif
				@endif
			</div>
		</div>
	</div>
@else
	<div class="col-sm-12 text-center">{{ trans('messages.no_data') }}</div><div class="clearfix"></div>
@endif
