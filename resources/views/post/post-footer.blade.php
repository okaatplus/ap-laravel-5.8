<div class="row form-group">
    <div class="col-md-12">
        @if($post && $post->created_user)
            <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                <li class="field-hint"><b>{{ trans('messages.Post.created_by') }}</b> {{ $post->created_user->name }}</li>
                <li class="field-hint" ><b>{{ trans('messages.Post.created_at') }}</b> {{ showDateTime($post->created_at) }}</li>
            </ul>
        @endif
        @if($post && $post->updated_user)
            <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                <li class="field-hint" ><b>{{ trans('messages.Post.updated_by') }}</b> {{ $post->updated_user->name }}</li>
                <li class="field-hint" ><b>{{ trans('messages.Post.updated_at') }}</b> {{ showDateTime($post->updated_at) }}</li>
            </ul>
        @endif
        @if($post && $post->send_approval_user)
            <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                <li class="field-hint" ><b>{{ trans('messages.Post.send_approval_by') }}</b> {{ $post->send_approval_user->name }}</li>
                <li class="field-hint" ><b>{{ trans('messages.Post.send_approval_at') }}</b> {{ showDateTime($post->send_approved_at) }}</li>
            </ul>
        @endif
        @if($post && $post->approval_user)
            <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                <li class="field-hint" ><b>{{ trans('messages.Post.approved_by') }}</b> {{ $post->approval_user->name }}</li>
                <li class="field-hint" ><b>{{ trans('messages.Post.approved_at') }}</b> {{ showDateTime($post->approved_at) }}</li>
            </ul>
        @endif
    </div>
</div>