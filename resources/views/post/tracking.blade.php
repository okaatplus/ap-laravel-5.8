@if($trackings->count())
    <?php
    $key = $from   = (($trackings->currentPage()-1)*$trackings->perPage())+1;
    $to     = (($trackings->currentPage()-1)*$trackings->perPage())+$trackings->perPage();
    $to     = ($to > $trackings->total()) ? $trackings->total() : $to;
    $allpage = $trackings->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>
    <div class="row">
        <div class="col-md-4" style="margin-bottom: 10px;">
            <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$trackings->total()]) !!}
            </div>
        </div>
        <div class="col-md-8">
            <div class="text-right" >
                @if($allpage > 1)
                    @if($trackings->currentPage() > 1)
                        <a class="btn btn-white paginate-link" href="#" data-page="{{ $trackings->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                    @endif
                    @if($trackings->lastPage() > 1)
                        <?php echo Form::selectRange('page', 1, $trackings->lastPage(),$trackings->currentPage(),['class'=>'form-control paginate-select']); ?>
                    @endif
                    @if($trackings->hasMorePages())
                        <a class="btn btn-white paginate-link" href="#" data-page="{{ $trackings->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped" id="p-list" width="100%">
            <thead>
            <tr>
                <th width="80px">{{ trans('messages.Report.sequence') }}</th>
                <th width="41%">{{ trans('messages.user') }}</th>
                <th width="80px">{{ trans('messages.Report.sequence') }}</th>
                <th width="*">{{ trans('messages.user') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($trackings as $i => $t)
            @if( $i%2 == 0)
            <tr>
            @endif
                <td>{{ $key++ }}</td>
                <td>{{ $t->user->name }}</td>
            @if( $i%2 != 0)
                </tr>
            @endif
            @endforeach
            @if( $trackings->count() % 2 != 0)
                <td></td>
                <td></td>
                </tr>
            @endif
            </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$trackings->total()]) !!}
            </div>
        </div>
        <div class="col-md-8">
            <div class="text-right" >
                @if($allpage > 1)
                    @if($trackings->currentPage() > 1)
                        <a class="btn btn-white paginate-link" href="#" data-page="{{ $trackings->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                    @endif
                    @if($trackings->lastPage() > 1)
                        <?php echo Form::selectRange('page', 1, $trackings->lastPage(),$trackings->currentPage(),['class'=>'form-control paginate-select']); ?>
                    @endif
                    @if($trackings->hasMorePages())
                        <a class="btn btn-white paginate-link" href="#" data-page="{{ $trackings->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                    @endif
                @endif
            </div>
        </div>
    </div>
</table>
@endif