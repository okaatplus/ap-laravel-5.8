<?php
    $parcel_type   = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
?>
@if($post_parcels->count())
<?php
    $allpage = $post_parcels->lastPage();
    $curPage = $post_parcels->currentPage();
    $start = $from   = (($post_parcels->currentPage()-1)*$post_parcels->perPage())+1;
    $to     = (($post_parcels->currentPage()-1)*$post_parcels->perPage())+$post_parcels->perPage();
    $to     = ($to > $post_parcels->total()) ? $post_parcels->total() : $to;
 ?>
<div class="row">
    <div class="col-md-4" style="margin-bottom: 10px;">
        <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
            {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$post_parcels->total()]) !!}
        </div>
    </div>
    <div class="col-md-8">
        <div class="text-right" >
            @if($allpage > 1)
                @if($post_parcels->currentPage() > 1)
                    <a class="btn btn-white paginate-link" href="#" data-page="{{ $post_parcels->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
                @endif
                @if($post_parcels->lastPage() > 1)
                    <?php echo Form::selectRange('page', 1, $post_parcels->lastPage(),$post_parcels->currentPage(),['class'=>'form-control paginate-select']); ?>
                @endif
                @if($post_parcels->hasMorePages())
                    <a class="btn btn-white paginate-link" href="#" data-page="{{ $post_parcels->currentPage()+1 }}">{{ trans('messages.next') }}</a>
                @endif
            @endif
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th style="width: 60px;">No.</th>
            <th style="width: 120px;">{{ trans('messages.PostParcel.receive_code') }}</th>
            <th style="width: 130px;">{{ trans('messages.PostParcel.ref_code') }}</th>
            <th>{{ trans('messages.PostParcel.from_name') }}</th>
            <th>{{ trans('messages.PostParcel.to_name') }}</th>
            <th style="width: 150px;">{{ trans('messages.unit_no') }}</th>
            <th style="width: 145px;">{{ trans('messages.PostParcel.ems_code') }}</th>
            <th style="width: 135px;">{{ trans('messages.PostParcel.date_received') }}</th>
            <th class="text-center" style="width: 130px;">{{ trans('messages.action') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $post_parcels as $post_parcel )
             <tr>
                 <td>{{ $start++ }}</td>
                 <td>{{ receivedNumber($post_parcel->receive_code) }}</td>
                 <td>{{ $post_parcel->ref_code }}</td>
                 <td>{{ $post_parcel->from_name }}</td>
                 <td>
                     @if( $post_parcel->to_resident_id )
                         @if( $post_parcel->to_resident_type =='c' )
                            {{ $post_parcel->toCustomer->prefix_name." ".$post_parcel->toCustomer->name }}
                         @else
                            {{ $post_parcel->toTenant->prefix_name." ".$post_parcel->toTenant->name }}
                         @endif
                     @else
                        {{ $post_parcel->to_name }}
                     @endif
                 </td>
                 <td>{{ $post_parcel->forUnit->unit_number }}</td>
                 <td>{{ $post_parcel->ems_code }}</td>
                 <td>{{ LocalDateShort($post_parcel->date_received) }}</td>
                 <td class="action-links text-center">
                     @if( $tab != "returned" )
                     <a href="#" class="delete-post-parcel btn btn-danger" data-toggle="modal" data-target="#cancel-post-parcel-modal" data-id="{{ $post_parcel->id }}">
                         <i class="fa-ban"></i>
                     </a>
                     @endif
                     @if($post_parcel->status == 0)
                         <a href="#" data-original-title="{{ trans('messages.PostParcel.change_to_delivered') }}" class="mark-as-delivered btn btn-success" data-toggle="tooltip" data-placement="top" data-id="{{ $post_parcel->id }}">
                             <i class="fa-send"></i>
                         </a>
                     @else
                         <a class="btn btn-default disabled" href="javascript:">
                             <i class="fa fa-send"></i>
                         </a>
                     @endif
                     <a href="#" data-original-title="{{ trans('messages.PostParcel.view_details') }}" class="view_pp_detail btn btn-info" data-toggle="tooltip" data-placement="top" data-id="{{ $post_parcel->id }}">
                         <i class="fa-eye"></i>
                     </a>
                 </td>
             </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    @if($allpage > 1)
        <div class="col-md-4" style="margin-bottom: 10px;">
            <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
                {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$post_parcels->total()]) !!}
            </div>
        </div>
    <div class="col-sm-8 text-right">
        @if($curPage > 1)
            <a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
        @endif
        @if($allpage  > 1)
            <?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
        @endif
        @if($post_parcels->hasMorePages())
            <a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
        @endif
    </div>
    @endif
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.PostParcel.not_found') }}</div><div class="clearfix"></div>
@endif
