@extends('layouts.print-a4')
@section('content')
    <?php
        $lang           = App::getLocale();
        $parcel_type    = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
    ?>
    @foreach( $post_parcels as $key => $pp)
    <div class="card">
        <div class="head">
            {{ Auth::user()->property->{'juristic_person_name_'.$lang} }}
        </div>
        <div class="sub-head">
            {{ trans('messages.PostParcel.inform_card_head') }}
        </div>

        <table width="100%" style="table-layout: fixed;">
            <tr>
                <th width="110px">{{ trans('messages.PostParcel.receive_code') }} :</th>
                <td width="110px">{{ receivedNumber($pp->receive_code) }}</td>
                <th>{{ trans('messages.unit_no_s') }} :</th>
                <td>{{ $pp->forUnit->unit_number }}</td>
            </tr>
            <tr>
                <th>{{ trans('messages.PostParcel.type') }} :</th>
                <td>{{ $parcel_type[$pp->type] }}</td>
                <th>{{ trans('messages.PostParcel.date_received_2') }} :</th>
                <td>{{ localDateShort($date_r) }}</td>
            </tr>
            <tr>
                <th>{{ trans('messages.PostParcel.ems_code') }} :</th>
                <td colspan="3">{{ $pp->ems_code }}</td>
            </tr>
            <tr>
                <th>{{ trans('messages.PostParcel.from_name') }} :</th>
                <td colspan="3">{{ $pp->from_name }}</td>
            </tr>
            <tr>
                <th>{{ trans('messages.PostParcel.to_name') }} :</th>
                <td colspan="3">{{ $pp->to_name }}</td>
            </tr>
            <tr>
                <th>หมายเหตุ :</th>
                <td colspan="3">{{ $pp->note }}</td>
            </tr>
        </table>
        <div class="suggest">
            {{ trans('messages.PostParcel.receiving_suggest') }}
        </div>
        <div class="envelope">
            <img src="{{ url('images/envenlop.png') }}" />
        </div>
        <div class="p-logo">
            <img src="{{ url('images/logo.png') }}" />
        </div>
    </div>
        @if( ($key + 1) % 10 == 0)
            <div style="clear: both; page-break-after: always;"></div>
        @endif
    @endforeach
    <style>
        body {
            font-size: 11px;
            line-height: 14px !important;
        }
        .head {
            text-align: center;
            font-size: 13px;
            line-height: 15px;
            font-weight: bold;
        }
        .sub-head {
            font-size: 12px;
            line-height: 14px;
            margin-top: 5px;
            margin-bottom: 15px;
            text-align: center;
        }
        .card {
            width: 50%;
            height: 190px;
            float: left;
            border: 1px solid #ddd;
            box-sizing: border-box;
            padding: 8px;
            position: relative;
        }
        table td, table th {
            padding-top: 2px;
        }
        .envelope {
            position: absolute;
            width: 20px;
            height: 20px;
            top:4px;
            right: 4px;
        }
        .envelope img {
            width: 100%;
        }
        .p-logo {
            position: absolute;
            width: 20px;
            height: 20px;
            bottom:4px;
            right: 4px;
        }
        .suggest {
            position: absolute;
            bottom: 3px;
            left: 10px;
            font-size: 8px;
            color: #7f7f7f !important;
        }
    </style>
@endsection
