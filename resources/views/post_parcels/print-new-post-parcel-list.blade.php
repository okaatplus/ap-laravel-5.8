@extends('layouts.print-a4')
@section('content')
    <?php
        $lang           = App::getLocale();
        $parcel_type    = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
    ?>
    <section class="bills-env signature-row" style="font-size: 20px;">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row" style="margin-bottom: 10px;border-bottom: 1px solid #000; padding-bottom: 20px;">
                    <div class="col-sm-12 text-center">
                        <b style="font-size: 15px;">
                            {{ Auth::user()->property->{'juristic_person_name_'.$lang} }}
                        </b><br/>
                        {{ trans('messages.PostParcel.receive_form_head') }} <br/>
                        {{ trans('messages.date').' '. localDate($date_r) }}
                    </div>
                </div>
            </div>
        </div>
        <!-- Invoice header -->
        <table width="100%" style="font-size: 13px;line-height: 20px;color: #000;">
            <tr>
                <th width="5%">No.</th>
                <th width="10%">{{ trans('messages.PostParcel.receive_code_s') }}</th>
                <th>{{ trans('messages.PostParcel.ems_code') }}</th>
                <th width="12%">{{ trans('messages.unit_no_s') }}</th>
                <th>{{ trans('messages.PostParcel.type') }}</th>
                <th>{{ trans('messages.PostParcel.from_name') }}</th>
                <th>{{ trans('messages.PostParcel.to_name') }}</th>
                <th width="10%">{{ trans('messages.PostParcel.receiver_name_2') }}</th>
                <th width="10%">{{ trans('messages.PostParcel.receive_date') }}</th>
            </tr>
            @foreach( $post_parcels as $key => $pp)
                <tr>
                    <td class="text-center">{{ $key + 1 }}</td>
                    <td>{{ receivedNumber($pp->receive_code) }}</td>
                    <td>{{ $pp->ems_code }}</td>
                    <td>{{ $pp->forUnit->unit_number }}</td>
                    <td class="type">{{ $parcel_type[$pp->type] }}</td>
                    <td>{{ $pp->from_name }}</td>
                    <td>{{ $pp->to_name }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
        </table>

        <div class="row" style="margin-top: 30px;">
            <div class="col-sm-12 text-right">
                {{ trans('messages.data_at')." ".localDate(date('Y-m-d')). " ". date(' H:i') }}
            </div>
        </div>
        <div class="row signature-row">
            <div class="col-md-12 text-right">
                <div class="sig-label text-right">{{ trans('messages.feesBills.signature') }}</div>
                <div class="sig-label"><span class="signature-sign"></span></div>
            </div>
            <div class="col-md-12 text-right">
                <div class="sig-label">(<span class="signature-sign"></span>)</div>
            </div>
            <div class="col-md-12 text-right" style="margin-top: 10px;">
                <div class="sig-label" style="padding-right: 40px;">เจ้าหน้าที่</div>
            </div>
        </div>
    </section>
    <style>
        table th,table td {
            font-size: 12px !important;
            border: 1px solid #ddd;
            padding: 3px;
            line-height: 14px;
        }
        table th {
            text-align: center;
            background: #ddd !important;
        }
        .type {
            font-size: 10px !important;
        }
        table tr:nth-child(odd) {
            background: #fdfdfd !important;
        }
    </style>
@endsection
