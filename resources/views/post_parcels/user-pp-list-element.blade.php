<?php
    $parcel_type   = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
?>
@if($post_parcels->count())
<?php
    $allpage = $post_parcels->lastPage();
    $curPage = $post_parcels->currentPage();
 ?>
 <p>{{ trans('messages.PostParcel.post_parcel_total',['no' => $post_parcels->total()]) }}</p><br/>
 @foreach( $post_parcels as $post_parcel )
 <div class="invoice-row">
 	<div class="invoice-head">
        <b>{{ trans('messages.PostParcel.receive_code') }} : {{ receivedNumber($post_parcel->receive_code) }}</b>
    </div>
    <div class="col-md-4">{{ trans('messages.PostParcel.date_received') }} : {{ LocalDateShort($post_parcel->date_received) }}</div>
    <div class="col-md-4">{{ trans('messages.PostParcel.ems_code') }} : {{ $post_parcel->ems_code }}</div>
    <div class="col-md-4">{{ trans('messages.PostParcel.type') }} : {{ $parcel_type[$post_parcel->type] }}</div>
    <div class="col-md-4">{{ trans('messages.PostParcel.from_name') }} : {{ $post_parcel->from_name }}</div>
    <div class="col-md-4">{{ trans('messages.PostParcel.status') }} :
        @if($post_parcel->status)
        {{ trans('messages.PostParcel.status_1') }}
        @else
        {{ trans('messages.PostParcel.status_0') }}
        @endif
    </div>
    <div class="col-md-4 action-links text-right">
        <a href="#" data-original-title="{{ trans('messages.PostParcel.view_details') }}" class="view_pp_detail btn btn-success" data-toggle="tooltip" data-placement="top" data-id="{{ $post_parcel->id }}">
            <i class="fa-eye"></i>
        </a>
    </div>
</div>
@endforeach
<div class="row">
    @if($allpage > 1)
    <div class="col-sm-12 text-right">
        <ul class="pagination no-margin post-parcel-pagination">
            @if($curPage > 1)
            <li>
                <a href="#" data-page="{{ $curPage-1 }}">
                    <i class="fa-angle-left"></i>
                </a>
            </li>
            @endif

            @for($i = 1; $i <= $allpage; $i++)
            <li @if($curPage == $i) class="active" @endif>
                <a href="#" data-page="{{ $i }}">{{$i}}</a>
            </li>
            @endfor
            @if($post_parcels->hasMorePages())
            <li>
                <a href="#" data-page="{{ $curPage+1 }}">
                    <i class="fa-angle-right"></i>
                </a>
            </li>
            @endif
        </ul>
    </div>
    @endif
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.PostParcel.not_found') }}</div><div class="clearfix"></div>
@endif
