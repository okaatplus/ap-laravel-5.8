@extends('layouts.base-admin')
@section('content')
<?php
    $parcel_type   = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.PostParcel.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{url('/')}}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
				<li class="active">
					<strong>{{ trans('messages.PostParcel.page_head') }}</strong>
				</li>
			</ol>
		</div>
	</div>
     <section>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('messages.search') }}</h3>
            </div>
            <div class="panel-body search-form">
                {!! Form::open(array('url' => array('post-and-parcel'),'class'=>'form-horizontal','method'=>'post','id'=>'pp-search')) !!}
                <div class="row">
                    <div class="col-sm-3 block-input">
                        <div class="input-group">
                            <span class="input-group-addon">RC - </span>
                            {!! Form::text('receive_code',null,array('class'=>'form-control','placeholder'=>trans('messages.PostParcel.receive_code'),'style'=>'position:static;')) !!}
                        </div>
                    </div>
                     <div class="col-sm-3 block-input">
                        {!! Form::text('date_received',null,array('class'=>'form-control datepicker','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','placeholder'=>trans('messages.PostParcel.date_received'),'data-language'=>App::getLocale())) !!}
                    </div>
                    <div class="col-sm-3 block-input">
                        {!! Form::select('type',$parcel_type,null,array('class'=>'form-control')) !!}
                    </div>
                     <div class="col-sm-3 block-input">
                        {!! Form::select('status',[0=>trans('messages.PostParcel.status'),'1'=>trans('messages.PostParcel.status_0'),'2'=>trans('messages.PostParcel.status_1')],null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="pull-right">
                    <button type="reset"  class="btn btn-white btn-single reset-s-btn">{{ trans('messages.reset') }}</button>
                    <button type="button" id="submit-search" class="btn btn-secondary btn-single">{{ trans('messages.search') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

    <section>
        <div class="panel panel-default">
             <div class="panel-body" id="panel-post-parcel-list">
               @include('post_parcels.user-pp-list-element')
            </div>
        </div>
    </section>
     <div class="modal fade" id="view-post-parcel-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.PostParcel.page_head') }}</h4>
                </div>
                <div class="modal-body form-horizontal content" id="post-parcel-content">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.ok') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.th.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
<script type="text/javascript">
    $(function () {
        var target;

        $('#submit-search').on('click',function () {
            searchPostParcel (1);
        });

        $('#pp-search').on('keydown', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                searchPostParcel (1);
            }
        });

        $('.reset-s-btn').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            searchPostParcel (1);
        });

        $('#panel-post-parcel-list').on('click','.post-parcel-pagination li a', function (e) {
            e.preventDefault();
            searchPostParcel ($(this).data('page'));
        });

        $('#panel-post-parcel-list').on('click', '.view_pp_detail', function (e) {
            e.preventDefault();
            target = $(this).data('id');
            getPostParcel ($(this));
        });

        function searchPostParcel (page) {
            var data = $('#pp-search').serialize();
            data+= "&page="+page;
            $('#panel-post-parcel-list').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/parcel-tracking",
                method  : "POST",
                data    : data,
                dataType: "html",
                success: function (t) {
                    $('#panel-post-parcel-list').html(t).css('opacity','1');
                    $('[data-toggle="tooltip"]').tooltip();
                }
            })
        }

        function getPostParcel (btn) {
            btn.find('.fa-eye').remove();
            btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i>');
            $.ajax({
                url     : $('#root-url').val()+"/parcel-tracking/view",
                method  : "POST",
                data    : ({id:target}),
                dataType: "html",
                success: function (t) {
                    $('#post-parcel-content').html(t);
                    $('#view-post-parcel-modal').modal('show');
                    btn.removeAttr('disabled').find('.fa-spin').remove();
                    btn.append('<i class="fa-eye"></i>');
                }
            })
        }
    })
</script>
<style type="text/css">
.cbr-replaced {
    margin-top: -5px;
}
</style>
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
@endsection
