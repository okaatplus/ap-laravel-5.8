<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('messages.AboutProp.property_detail') }}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>ข้อมูลโครงการ</h3>
                        <input type="hidden" class="property_id" value="{!! $property['id'] !!}">
                        <input type="hidden" class="dis" value="{!! $property['district'] !!}">
                        <input type="hidden" class="subdis" value="{!! $property['sub_district'] !!}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.project_code') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('project_code',null,array('class'=>'form-control','maxlength' => 200, 'placeholder'=> trans('messages.mandatory') )) !!}
                                <?php echo $errors->first('project_code','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group @if($errors->has('property_name_th') || $errors->has('property_name_en')) validate-has-error @endif">
                            <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.property_name') }} (th)</label>
                            <div class="col-sm-4">
                                {!! Form::text('property_name_th',null,array('class'=>'form-control','maxlength' => 200, 'placeholder'=> trans('messages.mandatory') )) !!}
                                <?php echo $errors->first('property_name_th','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                            </div>
                            <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.property_name') }} (en)</label>
                            <div class="col-sm-4">
                                {!! Form::text('property_name_en',null,array('class'=>'form-control','maxlength' => 200, 'placeholder'=> trans('messages.mandatory') )) !!}
                                <?php echo $errors->first('property_name_en','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('juristic_person_name_th') || $errors->has('juristic_person_name_en')) validate-has-error @endif">
                            <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.juristic_person_name') }} (th)</label>
                            <div class="col-sm-4">
                                {!! Form::text('juristic_person_name_th',null,array('class'=>'form-control','maxlength' => 200, 'placeholder'=> trans('messages.mandatory'))) !!}
                                <?php echo $errors->first('juristic_person_name_th','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                            </div>
                            <label class="col-sm-2 control-label" for="field-1">{{ trans('messages.AboutProp.juristic_person_name') }} (en)</label>
                            <div class="col-sm-4">
                                {!! Form::text('juristic_person_name_en',null,array('class'=>'form-control','maxlength' => 200, 'placeholder'=> trans('messages.mandatory'))) !!}
                                <?php echo $errors->first('juristic_person_name_en','<span id="name-error" class="validate-has-error">:message</span>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="profile-image">{{ trans('messages.AboutProp.banner') }}</label>
                            <div class="col-md-4">
                                <div id="attachment-banner-crop" class="drop-property-file dz-clickable" style="margin-bottom: 0; @if(!empty($property['banner_url']) && $property['banner_url']) display: none; @endif">
                                    <i class="fa fa-camera"></i>
                                </div>
                                <div class="preview-crop"></div>
                                <div class="post-banner" id="previews-img-banner-crop">
                                    @if(!empty($property['banner_url']) && $property['banner_url'] != null)
                                        <div class="preview-img-item" style="margin: 0;">
                                            <img src="{{ env('URL_S3')."/post-file".$property['banner_url'] }}" width="100%" alt="post-image" />
                                            <span class="remove-img-preview remove-banner">×</span>
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="col-sm-5">
                                <div class="img-container">
                                    <img class="img-responsive" />
                                </div>
                            </div>
                            <input type="hidden" id="img-x" name="img-x"/>
                            <input type="hidden" id="img-y" name="img-y"/>
                            <input type="hidden" id="img-w" name="img-w"/>
                            <input type="hidden" id="img-h" name="img-h"/>
                            <input type="hidden" id="img-tw" name="img-tw"/>
                            <input type="hidden" id="img-th" name="img-th"/>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.area') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('area_size',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.unit_amount') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('unit_size',null,array('class'=>'form-control price','maxlength' => 10, 'placeholder'=> trans('messages.mandatory'))) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.tax_id') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('tax_id',null,array('class'=>'form-control','maxlength' => 13)) !!}
                            </div>
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.property_type') }}</label>
                            <div class="col-sm-4">
                                {!! Form::select('property_type',$property_type,null,array('class'=>'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ประเภทโครงการของ AP</label>
                            <div class="col-sm-4">
                                {!! Form::select('ap_type',['AP' => 'AP','None AP' => 'None AP','Old AP' => 'AP โครงการเก่า'],null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.management_state') }}</label>
                            <div class="col-sm-4">
                                {!! Form::select('management_state',[
                                    0 => 'โครงการระหว่างขาย',
                                    1 => 'โครงการปิดการขาย ',
                                    2 => 'โครงการปิดการขาย รอจัดตั้งนิติบุคคลฯ',
                                    3 => 'โครงการเป็นนิติบุคคลฯแล้ว',
                                ],null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.brand') }}</label>
                            <div class="col-sm-4">
                                {!! Form::select('brand',$brand,null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-2 control-label">ลักษณะโครงการ</label>
                            <div class="col-sm-4">
                                {!! Form::select('dimension',['v' => 'แนวราบ', 'h' => 'แนวสูง'],null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.constructor') }}</label>
                            <div class="col-sm-10">
                                {!! Form::text('construction_by',null,array('class'=>'form-control','maxlength' => 100)) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.address_no') }}</label>
                            <div class="col-sm-10">
                                {!! Form::text('address_no',null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.road') }} (th)</label>
                            <div class="col-sm-4">
                                {!! Form::text('street_th',null,array('class'=>'form-control', 'placeholder'=> trans('messages.mandatory'))) !!}
                            </div>
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.road') }} (en)</label>
                            <div class="col-sm-4">
                                {!! Form::text('street_en',null,array('class'=>'form-control', 'placeholder'=> trans('messages.mandatory'))) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.province') }}</label>
                            <div class="col-sm-4">
                                    {!! Form::select('province',$provinces,null,array('class'=>'form-control province')) !!}
                            </div>

                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.district') }}</label>
                            <div class="col-sm-4">
                                {!! Form::select('district',$districts,null,array('class'=>'form-control district')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.subdistricts') }}</label>
                            <div class="col-sm-4">
                                {!! Form::select('sub_district',$subdistricts,null,array('class'=>'form-control subdistricts')) !!}
                            </div>

                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.postcode') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('postcode',null,array('class'=>'form-control postcode','maxlength' => 10, 'placeholder'=> trans('messages.mandatory'))) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.tel') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('tel',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-2 control-label">{{ trans('messages.fax') }}</label>
                            <div class="col-sm-4">
                                {!! Form::text('fax',null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{ trans('messages.AboutProp.location') }}</label>
                            <div id="search-geo-block">
                                <input id="address" class="controls" type="text" placeholder="Search Box">
                                <input type="button" class="btn btn-primary" id="search-geo-btn" value="SEARCH" />
                            </div>
                            <div class="col-sm-10"><div id="map" style="height:300px;"></div></div>
                            {!! Form::hidden('lat',null,array('id'=>'latbox')) !!}
                            {!! Form::hidden('lng',null,array('id'=>'lngbox')) !!}
                        </div>
                        <hr>
                        <h3>ข้อมูลทั่วไป</h3>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">จำนวนที่จอดรถ</label>
                            <div class="col-sm-3">
                                {!! Form::text('parking_slot',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-4 control-label">จำนวนที่จอดรถ (รวมจอดซ้อนคัน)</label>
                            <div class="col-sm-3">
                                {!! Form::text('parking_slot_max',null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">จำนวนชั้นที่จอดรถ</label>
                            <div class="col-sm-3">
                                {!! Form::text('parking_floor_no',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-4 control-label">จำนวนสิทธิที่จอดรถ</label>
                            <div class="col-sm-3">
                                {!! Form::text('parking_privilege',null,array('class'=>'form-control')) !!}
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">พื้นที่ส่วนกลาง</label>
                            <div class="col-sm-3">
                                {!! Form::text('common_area_size',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-4 control-label">พื้นที่อัตรส่วนกรรมสิทธิ์</label>
                            <div class="col-sm-3">
                                {!! Form::text('ownership_area_size',null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ค่าส่วนกลาง ตรม.ละ</label>
                            <div class="col-sm-3">
                                {!! Form::text('common_fee_rate',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-4 control-label">รอบการจัดเก็บเงินค่าส่วนกลาง(เดือน)</label>
                            <div class="col-sm-3">
                                {!! Form::text('common_fee_period',null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">เงินกองทุน ตรม.ละ</label>
                            <div class="col-sm-3">
                                {!! Form::text('fund_rate',null,array('class'=>'form-control')) !!}
                            </div>
                            <label class="col-sm-4 control-label">ค่าน้ำ หน่วยละ (ลบ.ม.)</label>
                            <div class="col-sm-3">
                                {!! Form::text('water_rate',null,array('class'=>'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">อัตราค่าปรับ(%)</label>
                            <div class="col-sm-3">
                                {!! Form::text('fine_rate',null,array('class'=>'form-control')) !!}
                            </div>
                            <?php
                            $common_area_fee_type = unserialize(constant('COMMON_AREA_FEE_TYPE_'.strtoupper(App::getLocale())));
                            ?>
                            <label class="col-sm-4 control-label">วิธีการจัดเก็บค่าส่วนกลาง</label>
                            <div class="col-sm-3">
                                {!! Form::select('cf_method',$common_area_fee_type,null,array('class'=>'form-control')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="land-label" value="{{ trans('messages.Prop_unit.land') }}"/>
<input type="hidden" id="property-label" value="{{ trans('messages.Prop_unit.property') }}"/>
<input type="hidden" id="property-none-label" value="{{ trans('messages.Prop_unit.none') }}"/>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}" ></script>
<script type="text/javascript" src="{{url('/')}}/js/number.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery.floatThead.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/property-form.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/crop-img.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/nabour-upload-file.js"></script>
<script type="text/javascript">

    var map,marker,geocoder;
    function initialize() {

                @if(!empty($property['lat']) && !empty($property['lng']))
        var latlng = new google.maps.LatLng({{$property['lat']}}, {{$property['lng']}});
                @else
        var latlng = new google.maps.LatLng(16.492660635148514, 100.86205961249993);
        @endif
            geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            $("#latbox").val(this.getPosition().lat());
            $("#lngbox").val(this.getPosition().lng());
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    function codeAddress() {
        var address = document.getElementById('address').value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var l = results[0].geometry.location;
                map.setCenter(l);
                marker.setPosition(l);
                $('#latbox').val(l.lat());
                $('#lngbox').val(l.lng());
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    $(function () {
        $('#search-geo-btn').on('click',function () {
            codeAddress();
        });

        $('#add-unit-later').on('change', function () {
            if($(this).is(':checked')) {
                $(this).parents('.panel-default').addClass('collapsed');
            } else {
                $(this).parents('.panel-default').removeClass('collapsed');
            }

        })

        $('body').on('click','#submit-form', function (e) {
            validateForm ();
            if($('#add-unit-later').length && !$('#add-unit-later').is(':checked')) {
                if($('#unit-csv-data').val() != '') {
                    allGood = true;
                } else allGood = checkField ();
            } else {
                allGood = true;
            }


            if($('#p_form').valid() && allGood ) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#p_form').submit();
            } else {
                var top_;
                if(!$('#p_form').valid()) top_ = $('.error').first().offset().top;
                else top_ = $('#prop_list').offset().top;
                $('html,body').animate({scrollTop: top_-100}, 1000);
            }
        })
    })

    @if(!empty($property))
            $('.subdistricts').attr("disabled", false);
            $('.district').attr("disabled", false);
         @else
            $('.subdistricts').attr("disabled", true);
            $('.district').attr("disabled", true);
    @endif


    $('.province').on('change',function(){
        $('.district').attr("disabled", false);
        var id;
        id = $(this).val();
        $.ajax({
            url : $('#root-url').val()+"/root/admin/select/district",
            method : 'post',
            dataType: 'html',
            data : ({'id':id}),
            success: function (e) {
                $(".district").html('');
                $(".district").append("<option value=''>อำเภอ/เขต</option>");
                $.each($.parseJSON(e), function(i, val){
                    $(".district").append("<option value='"+val.id+"'>"+val.name_th+" "+val.name_en+"</option>");
                });
            },
            error : function () {


            }
        })
    })

    function SelectDistrict(id){
        $('.subdistricts').attr("disabled", false);
        var id = id;
        //console.log(id);
        $.ajax({
            url : $('#root-url').val()+"/root/admin/select/subdistrict",
            method : 'post',
            dataType : 'html',
            data : ({'id':id}),
            success : function(e){
                $('.subdistricts').html('');
                $.each($.parseJSON(e),function(i,val){
                    $('.subdistricts').append("<option value='"+val.id+"'>"+val.name_th+" "+val.name_en+"</option>");
                    $('.postcode').val(val.zip_code);
                });

            },error : function(){

            }
        })
    }

    $('body').ready(function(){
        var id = $('.property_id').val();
        var dis = $('.dis').val();
        var subdis = $('.subdis').val();
        var select;
        //console.log(dis);
        $.ajax({
            url : $('#root-url').val()+"/root/admin/select/district/edit",
            method : 'post',
            dataType : 'html',
            data : ({'id':id}),
            success : function(e){
                $('.district').html('');
                $('.district').append("<option value=''>ตำบล</option>");
                $.each($.parseJSON(e),function(i,val){
                    if(val.id == dis){
                        select = "selected";
                        $('.district').append("<option value='"+val.id+"' selected='"+select+"'>"+val.name_th+" "+val.name_en+"</option>");
                    }else {
                        select = "";
                        $('.district').append("<option value='"+val.id+"'>"+val.name_th+" "+val.name_en+"</option>");
                    }
                    //console.log(select);
                    $('.postcode').val(val.zip_code);
                    //console.log(val.id);
                });
            },error : function(){
                console.log('aa');
            }
        })
        ////////////////////////////////
        $.ajax({
            url : $('#root-url').val()+"/root/admin/select/editSubDis",
            method : 'post',
            dataType : 'html',
            data : ({'id':id}),
            success : function(e){
                $('.subdistricts').html('');
                $('.subdistricts').append("<option value=''>ตำบล</option>");
                $.each($.parseJSON(e),function(i,val){
                    if(val.id == subdis){
                        select = "selected";
                        $('.subdistricts').append("<option value='"+val.id+"' selected='"+select+"'>"+val.name_th+" "+val.name_en+"</option>");
                    }else {
                        select = "";
                        $('.subdistricts').append("<option value='"+val.id+"'>"+val.name_th+" "+val.name_en+"</option>");
                    }
                    $('.postcode').val(val.zip_code);
                });
            },error : function(){
                console.log('aa');
            }
        })
    })

    $('.district').on('change',function(){
        $('.subdistricts').attr("disabled", false);
        var id = $(this).val();
        console.log(id);
        $.ajax({
            url : $('#root-url').val()+"/root/admin/select/subdistrict",
            method : 'post',
            dataType : 'html',
            data : ({'id':id}),
            success : function(e){
                //console.log(e);
                $('.subdistricts').html('');
                $('.subdistricts').append("<option value=''>ตำบล</option>");
                $.each($.parseJSON(e),function(i,val){
                    $('.subdistricts').append("<option value='"+val.id+"'>"+val.name_th+" "+val.name_en+"</option>");
                    $('.postcode').val(val.zip_code);
                });
            },error : function(){
                console.log('aa');
            }
        })
    })

    $('.subdistricts').on('change',function(){
        var id = $(this).val();
        //console.log(id);
        $.ajax({
            url : $('#root-url').val()+"/root/admin/select/zip_code",
            method : 'post',
            dataType : 'html',
            data : ({'id':id}),
            success : function(e){
                $.each($.parseJSON(e),function(i,val){
                    $('.postcode').val(val.zip_code);
                });
                //console.log(e);

            },error : function(){
                //console.log('aa');
            }
        })
    })

</script>
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="{{url('/')}}/js/cropper/cropper.min.css">

<style>
    .unit-table th {
        background: #fff;
    }
    .remove-row {
        border: 1px solid red;
        background: #f99;
        color: #fff;
        font-size: 11px;
        margin-top: 5px;
    }
    #add-unit-row {
        background-color: #2dbca6;
        color: #fff;
        padding: 5px 10px;
        border: none;
        line-height: 20px;
    }
    #p_form .error,#p_form .error__ {
        border: 1px solid #a94442;
    }
    .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #e2e2e2;
    }
    .form-control::-moz-placeholder { /* Firefox 19+ */
        color: #e2e2e2;
    }
    .form-control:-ms-input-placeholder { /* IE 10+ */
        color: #e2e2e2;
    }
    .form-control:-moz-placeholder { /* Firefox 18- */
        color: #e2e2e2;
    }
</style>
