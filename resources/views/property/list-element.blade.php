 @if($p_rows->count() > 0)
 <?php
	$to   	= $p_rows->total() - (($p_rows->currentPage())*$p_rows->perPage());
 	$to     = ($to > 0) ? $to : 1;
	$from   = $p_rows->total() - (($p_rows->currentPage())*$p_rows->perPage())+$p_rows->perPage();
    $allpage = $p_rows->lastPage();
 ?>
 <div class="row">
 <div class="col-md-6">
		<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
			{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$p_rows->total()]) !!}<br/><br/>
		</div>
	</div>
	@if($allpage > 1)
	<div class="col-md-6 text-right">
		@if($p_rows->currentPage() > 1)
			<a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($p_rows->lastPage() > 1)
		<?php echo Form::selectRange('page', 1, $p_rows->lastPage(),$p_rows->currentPage(),['class'=>'form-control p-paginate-select paginate-select']); ?>
		@endif
		@if($p_rows->hasMorePages())
			<a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()+1 }}">{{ trans('messages.next') }}</a>
		@endif
	</div>
	@endif
</div>

<table class="table table-bordered table-striped" id="p-list" width="100%">
	@if(!empty($p_rows))
	<thead>
		<tr>
			<th width="7%">{{ trans('messages.Report.sequence') }}</th>
			<th width="*">{{ trans('messages.AdminProp.prop_list') }}</th>
			<th width="180px">{{ trans('messages.action') }}</th>
		</tr>
	</thead>
	<tbody class="middle-align">
		@foreach($p_rows as $key => $row)
		<tr>
			<td class="text-center">{{ ($key+1) }}</td>
			<td class="name">{{$row->property_name_th." / ".$row->property_name_en}}</td>
			<td>
				<div class="btn-group left-dropdown"> 
					<button type="button" class="btn btn-success" data-toggle="dropdown"><span class="caret"></span>&nbsp;&nbsp; {{ trans('messages.AdminProp.action') }}</button>
					<ul class="dropdown-menu dropdown-menu-right" role="menu">
						<li>
							<a href="{{url('/root/admin/property/view/'.$row->id)}}">
								<i class="fa-eye"></i> {{ trans('messages.view') }}
							</a>
						</li>
					@if( Auth::user()->role != 7)
						<li>
							<a href="{{url('/root/admin/property/edit/'.$row->id)}}">
								<i class="fa-edit"></i> {{ trans('messages.edit') }}
							</a>
						</li>
						@if( !$row->property_unit->count() )
						<li>
							<a href="#"  class="add-unit-link" data-toggle="modal" data-target="#import-unit" data-pid="{{ $row->id }}">
								<i class="fa-home"></i> {{ trans('messages.Prop_unit.unit_import') }}
							</a>
						</li>
						@else
							@if( Auth::user()->role == 0)
								<li>
									<a href="#" class="update-unit-link" data-toggle="modal" data-target="#update-unit" data-pid="{{ $row->id }}">
										<i class="fa-home"></i> {{ trans('messages.Prop_unit.unit_update') }}
									</a>
								</li>
								<li>
									<a href="{{ url('root/admin/mdm/index/'.$row->id) }}" target="_blank">
										<i class="fa-file-archive-o"></i> CSV For MDM
									</a>
								</li>
							@endif
						@endif
						<li>
							<a href="#"  class="add-customer-link" data-toggle="modal" data-target="#import-customer" data-pid="{{ $row->id }}">
								<i class="fa-user"></i> {{ trans('messages.Customer.customer_import') }}
							</a>
						</li>
						@if( $row->property_unit->count() )
						<li>
							@php
							if( !$row->userProperty->count() ) {
								$allow_approve_class = "reset-data-link";
								$modal_target = "#reset-data-modal";
							} else {
								$allow_approve_class = 'disabled-link';
								$modal_target = "#not-allowed-reset-modal";
							}
							@endphp
							<a href="#"  class="{{ $allow_approve_class }}" data-toggle="modal" data-target="{{ $modal_target }}" data-pid="{{ $row->id }}">
								<i class="fa-trash"></i> ล้างข้อมูลที่เคยนำเข้า
							</a>
						</li>
						@endif
					@endif
						<li>
							<a href="{{url('/admin/property/directlogin/'.$row->id)}}">
								<i class="fa-user"></i> {{ trans('messages.AdminProp.login_as_admin') }}
							</a>
						</li>
					</ul> 
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
	@else
	<tr><td> Not found </td></tr>
	@endif
</table>

<div class="row">
	<div class="col-md-6">
		<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
			{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$p_rows->total()]) !!}
		</div>
	</div>
	@if($allpage > 1)
	<div class="col-md-6 text-right">
		@if($p_rows->currentPage() > 1)
			<a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($p_rows->lastPage() > 1)
		<?php echo Form::selectRange('page', 1, $p_rows->lastPage(),$p_rows->currentPage(),['class'=>'form-control p-paginate-select paginate-select']); ?>
		@endif
		@if($p_rows->hasMorePages())
			<a class="btn btn-white p-paginate-link paginate-link" href="#" data-page="{{ $p_rows->currentPage()+1 }}">{{ trans('messages.next') }}</a>
		@endif
	</div>
	@endif
</div>
@else
<div class="col-sm-12 text-center">ไม่พบข้อมูล</div><div class="clearfix"></div>
@endif
