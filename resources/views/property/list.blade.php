@extends('layouts.base-admin')
@section('content')
<div class="page-title">
	<div class="title-env">
		<h1 class="title">{{ trans('messages.property') }}</h1>
	</div>
	<div class="breadcrumb-env">
		@if(Auth::user()->role > 1)
		<ol class="breadcrumb" >
			<li>
				<a href="{{ url('/property') }}"><i class="fa-home"></i>Home</a>
			</li>
			<li><a href="#">{{ trans('messages.property') }}</a></li>
			<li class="active">
				<strong>{{ trans('messages.AdminProp.prop_list') }}</strong>
			</li>
		</ol>
		@else
			<a href="{{ url('root/admin/property/add') }}" data-toggle="modal" class="top-action btn btn-primary">
				<i class="fa-home"></i> {{ trans('messages.AdminProp.page_head_add') }}
			</a>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('messages.search') }}</h3>
            </div>
            <div class="panel-body search-form">
            	 <form method="POST" id="search-form" action="#" accept-charset="UTF-8" class="form-horizontal">
                        <div class="row">
							<div class="col-sm-3 block-input">
                        		<input class="form-control" size="25" placeholder="{{ trans('messages.name') }}" name="name">
							</div>
							<div class="col-sm-3">
								{!! Form::select('province', $provinces,null,['id'=>'property-province','class'=>'form-control']) !!}
							</div>
							<div class="col-sm-6 text-right">
								<button type="reset" class="btn btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
								<button type="button" class="btn btn-secondary @if(isset($demo)) d-search-property @else p-search-property @endif">{{ trans('messages.search') }}</button>
							</div>
						</div>
                  </form>
            </div>
        </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body" id="landing-property-list">
				@include('property.list-element')
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-active" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{ trans('messages.Member.confirm_ban_head') }}</h4>
			</div>
			<div class="modal-body">
				{!! trans('messages.Member.confirm_ban_msg') !!}
			</div>
			 <div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-inactive" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{ trans('messages.Member.confirm_active_head') }}</h4>
			</div>
			<div class="modal-body">
				{!! trans('messages.Member.confirm_active_msg') !!}
			</div>
			 <div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="import-unit" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Prop_unit.unit_import') }}</h4>
			</div>
			<div class="modal-body">
				<div id="upload-section">
					<div id="attachment-unit" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
						<i class="fa fa-file-excel-o"></i> &nbsp;{{ trans('messages.Prop_unit.unit_import_txt') }}
					</div>
					<div class="post-banner" id="preview-file"></div>
				</div>
				<div id="import-section" style="display:none;"></div>
				<div id="import-msg" style="display:none;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-import-review-data" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Prop_unit.unit_import') }}</h4>
			</div>
			<div class="modal-body">
				<div id="reading-unit-txt">
					{{ trans('messages.Importing.checking_data') }}
				</div>
				<div id="review-unit-data-content">

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="start-import-unit">{{ trans('messages.Prop_unit.start_import') }}</button>
				<button type="button" id="cancel-modal-unit" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="button" id="close-modal-unit" class="btn btn-white close-import" data-dismiss="modal" style="display: none;">{{ trans('messages.close') }}</button>
			</div>
		</div>
	</div>
</div>

	<!----- Customer data importing function --------->
<div class="modal fade" id="import-customer" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Customer.customer_import') }}</h4>
			</div>
			<div class="modal-body">
				<div id="upload-section">
					<div id="attachment-customer" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
						<i class="fa fa-file-excel-o"></i> &nbsp;{{ trans('messages.Customer.customer_import_txt') }}
					</div>
					<div class="post-banner" id="preview-customer-file"></div>
				</div>
				<div id="import-customer-section" style="display:none;"></div>
				<div id="import-customer-msg" style="display:none;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-import-review-customer-data" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Customer.customer_import') }}</h4>
			</div>
			<div class="modal-body">
				<div id="reading-customer-txt">
					{{ trans('messages.Importing.checking_data') }}
				</div>
				<div id="review-customer-data-content">

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="start-import-customer">{{ trans('messages.Prop_unit.start_import') }}</button>
				<button type="button" id="cancel-modal-customer" class="btn btn-white close-import" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="button" id="close-modal-customer" class="btn btn-white close-import" data-dismiss="modal" style="display: none;">{{ trans('messages.close') }}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="reset-data-modal" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> ล้างข้อมูลที่เคยนำเข้า </h4>
			</div>
			<div class="modal-body">
				<div id="reading-customer-txt">
					คุณแน่ใจว่าต้องการลบข้อมูลที่พักอาศัยและข้อมูลลูกค้าที่เคยได้นำเข้าระบบก่อนหน้านี้ การล้างข้อมูลจะทำให้ข้อมูลที่พักอาศัยและข้อมูลลูกค้าทั้งหมดหายไป <span style="color:red;">โปรดเรียกใช้ด้วยความระมัดระวัง</span> ถ้าแน่ใจว่าต้องการลบต่อไปกรุณากดปุ่ม ยืนยัน
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="button" id="confirm-reset-data" class="btn btn-primary" data-dismiss="modal">{{ trans('messages.confirm') }}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="not-allowed-reset-modal" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> ไม่สามารถล้างข้อมูล </h4>
			</div>
			<div class="modal-body">
				<div id="reading-customer-txt">
					ไม่สามารถล้างข้อมูลได้ เนื่องจากมีลูกบ้านได้เข้ามาใช้งานในระบบแล้ว
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
			</div>
		</div>
	</div>
</div>

	<!--- Edit unit --->

<div class="modal fade" id="update-unit" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Prop_unit.unit_update') }}</h4>
			</div>
			<div class="modal-body">
				<div id="upload-section">
					<div id="attachment-update-unit" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
						<i class="fa fa-file-excel-o"></i> &nbsp;{{ trans('messages.Prop_unit.unit_update_txt') }}
					</div>
					<div class="post-banner" id="preview-update-unit-file"></div>
				</div>
				<div id="update-unit-section" style="display:none;"></div>
				<div id="update-unit-msg" style="display:none;"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white close-update-unit" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-update-unit-review-data" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-paper"></i> {{ trans('messages.Prop_unit.unit_update') }}</h4>
			</div>
			<div class="modal-body">
				<div id="reading-update-unit-txt">
					{{ trans('messages.Importing.checking_data') }}
				</div>
				<div id="review-update-unit-data-content">

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="start-update-unit">{{ trans('messages.Prop_unit.start_update') }}</button>
				<button type="button" id="cancel-modal-update-unit" class="btn btn-white close-update-unit" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="button" id="close-modal-update-unit" class="btn btn-white close-update-unit" data-dismiss="modal" style="display: none;">{{ trans('messages.close') }}</button>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
	<script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/dropzone/dropzone.min.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-add-property-unit.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-update-property-unit.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/smart-add-customer.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/reset-property-data.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
	<script>
	var target_prop;
	var file_name;
	var key_check_valid = false;
	$(function () {
		$('.panel-body').on('click','.p-paginate-link', function (e){
			e.preventDefault();
			propertyPage($(this).attr('data-page'));
		})

		$('.panel-body').on('change','.p-paginate-select', function (e){
			e.preventDefault();
			propertyPage($(this).val());
		})

		$('.p-search-property').on('click',function () {
			propertyPage (1);
		});

		$('.reset-s-btn').on('click',function () {
			$(this).closest('form').find("input").val("");
			$(this).closest('form').find("select option:selected").removeAttr('selected');
			propertyPage (1);
		});
	});

	function propertyPage (page) {
		var data = $('#search-form').serialize()+'&page='+page;
		$('#landing-property-list').css('opacity','0.6');
		$.ajax({
			@if(Auth::user()->role == 0)
			url     : $('#root-url').val()+"/root/admin/property/list",
			@elseif(Auth::user()->role == 1)
			url     : $('#root-url').val()+"/management/admin/property/list",
			@else
			url     : $('#root-url').val()+"/smart-supervisor/admin/property/list",
			@endif
			data    : data,
			dataType: "html",
			method: 'post',
			success: function (h) {
				$('#landing-property-list').css('opacity','1').html(h);
			}
		})
	}

	/*function changeActiveStatus (pid,flag) {
		$.ajax({
			url     : $('#root-url').val()+"/root/admin/property/status",
			method	: "POST",
			data 	: ({pid:pid,status:flag}),
			dataType: "json",
			success: function (r) {
				if(r.result) {
					location.reload();
				}
			}
		});
	} */
	</script>
	<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
	<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
	<style>
		tr.has-error {
			background: #ffbfbf !important;
		}
		td.not-valid {
			background: red;
			color: #fff !important;
		}
		/*#review-unit-data-content,
		#review-customer-data-content{

		}*/
		#review-unit-data-content table th,
		#review-customer-data-content table th {
			padding: 5px 0 !important;
			text-align: center;
		}
		.scrollable-div {
			overflow-x: auto;
			max-height: 400px;
			overflow-y: auto;
		}
		.modal-dialog {
			transition: 0.3s !important;
		}
	</style>
@endsection
