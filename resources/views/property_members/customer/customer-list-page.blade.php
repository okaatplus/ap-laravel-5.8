@if($customers->count())
<?php
	$start = $from   = (($customers->currentPage()-1)*$customers->perPage())+1;
    $to     = (($customers->currentPage()-1)*$customers->perPage())+$customers->perPage();
    $to     = ($to > $customers->total()) ? $customers->total() : $to;
    $allpage = $customers->lastPage();
    $curPage = $customers->currentPage();
 ?>
 <div class="row">
	 <div class="col-sm-6" style="margin-bottom: 10px;"><p> {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$customers->total()]) !!}</p></div>
 	@if($allpage > 1)
	<div class="col-md-6 text-right">
		@if($curPage > 1)
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($allpage  > 1)
		<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
		@endif
		@if($customers->hasMorePages())
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
		@endif
	</div>
 	@endif
 </div>
<table class="table table-striped">
	<thead>
	<tr>
		<th width="80px">{{ trans('messages.Report.sequence') }}</th>
		<th width="*">Name</th>
		<th width="180px" class="text-center">ประเภทการยืนยันตัวตน</th>
		<th class="text-center" width="200px">ID Card/Passport</th>
		<th class="text-center" width="150px">Action</th>
	</tr>
	</thead>
	<tbody>
	@foreach($customers as $c)
		<tr>
			<td class="text-center">
				{{ $start++ }}
			</td>
			<td>
				{{ $c->name }}
			</td>
			<td class="text-center">
				@switch ($c->type_id_card)
					@case(1) บัตรประจำตัวประชาชน @break
					@case(2) หนังสือเดินทาง @break
					@default - @break
				@endswitch
			</td>
			<td class="text-center">
				{{ $c->id_card }}
			</td>
			<td class="text-center action-links">
				<a href="#" class="btn btn-info view-member" data-uid="{{ $c->id }}" >
					<i class="fa-eye"></i>
				</a>
				<a href="#" class="btn btn-warning edit-member" data-uid="{{ $c->id }}" >
					<i class="fa-pencil"></i>
				</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

<div class="clearfix"></div>
<div class="row">
	<div class="col-sm-6"><p> {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$customers->total()]) !!}</p></div>
 	@if($allpage > 1)
	<div class="col-md-6 text-right">
		@if($curPage > 1)
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
		@endif
		@if($allpage  > 1)
		<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
		@endif
		@if($customers->hasMorePages())
			<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
		@endif
	</div>
 	@endif
 </div>
 
@else
<div class="col-sm-12 text-center">{{ trans('messages.Member.member_not_found') }}</div><div class="clearfix"></div>
@endif
