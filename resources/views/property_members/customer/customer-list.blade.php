@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Member.customer_page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">

		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@include('property_units.tab-menu')
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body member-list-content">
				<form method="POST" id="search-member-form" action="{{ url('admin/property/customer/export') }}" accept-charset="UTF-8" class="form-horizontal">
					<div class="row">
						<div class="col-sm-3 block-input">
							<input class="form-control datepicker" size="25" placeholder="{{ trans('messages.name') }}" name="name">
						</div>

						<div class="col-sm-3 block-input">
							{!! Form::select('unit_id', $unit_list,null,['class'=>'form-control select2', 'id' => 'unit-id']) !!}
						</div>
						<div class="col-sm-6 text-right">
							<button type="reset" id="reset-search" class="btn btn-single btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
							<button type="button" class="btn btn-secondary btn-single search-member">{{ trans('messages.search') }}</button>
							<button type="submit" class="btn btn-secondary btn-single">{{ trans('messages.Report.download') }}</button>
						</div>
					</div>
				</form>
				<hr/>
				<div class="tab-pane active" id="member-list">
					<div id="member-list-content">
						@include('property_members.customer.customer-list-page')
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

    <div class="modal fade" id="modal-member">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.customer_detail') }}</h4>
                </div>
                <div class="modal-body">

                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                </div>
            </div>
        </div>
    </div>

	<div class="modal fade" id="modal-member-edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{ trans('messages.Member.customer_detail') }}</h4>
				</div>
				<div class="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="save-edit">{{ trans('messages.save') }}</button>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('script')
<script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/random-password.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(function (){
		var tmp;

		$("#unit-id").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });


		$('.member-list-content').on('click','.paginate-link', function (e) {
			e.preventDefault();
			searchMember ($(this).data('page'));
		});

		$('.member-list-content').on('change','.paginate-select', function (e) {
			e.preventDefault();
			searchMember ($(this).val());
		});

		$('.search-member').on('click', function () {
			searchMember (1)
		});

		$('#search-member-form').on('keydown', function(e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				searchMember (1);
			}
		});

		$('.member-list-content').on('click','.view-member', function (e){
			e.preventDefault();
			var _this = $(this);
			_this.html('<i class="fa-spin fa-spinner"></i>');
			var uid = _this.attr('data-uid');
			$.ajax({
				url     : $('#root-url').val()+"/admin/property/customer/get",
                method	: "POST",
                data 	: ({uid:uid}),
                dataType: "html",
                success: function (t) {
                	$('#modal-member .modal-body').html(t);
                	$('#modal-member').modal('show');
                	_this.html('<i class="fa-eye"></i>');
                }
			});
		});

		$('.member-list-content').on('click','.edit-member', function (e){
			e.preventDefault();
			var _this = $(this);
			_this.html('<i class="fa-spin fa-spinner"></i>');
			var uid = _this.attr('data-uid');
			$.ajax({
				url     : $('#root-url').val()+"/admin/property/customer/edit",
				method	: "POST",
				data 	: ({uid:uid}),
				dataType: "html",
				success: function (t) {
					$('#modal-member-edit .modal-body').html(t);
					$('#modal-member-edit').modal('show');
					_this.html('<i class="fa-pencil"></i>');
				}
			});
		});

		$('.reset-s-btn').on('click',function () {
			$(this).closest('form').find("input").val("");
			$(this).closest('form').find("select option:selected").removeAttr('selected');
			$('#unit-id').val('').trigger('change');
			searchMember (1);
		});

		$('#save-edit').on('click', function (e){
			//alert('save');
			if( $('#edit-customer-form').valid() ) {
				let btn = $(this);
				btn.prepend('<i class="fa-spin fa-spinner"></i>').attr('disabled','disabled');
				$.ajax({
					url     : $('#root-url').val()+"/admin/property/customer/save",
					method	: "POST",
					data 	: $('#edit-customer-form').serialize(),
					dataType: "html",
					success: function (t) {
						btn.removeAttr('disabled').find('.fa-spin').remove();
						$('#modal-member-edit').modal('hide');
						searchMember(1);
						//$('[data-toggle="tooltip"]').tooltip();
					}
				});
			}
		});
	});

	function searchMember (page) {
		var data = $('#search-member-form').serialize();
		data+= "&page="+page;
        $('#member-list-content').css('opacity','0.6');
		$.ajax({
			url     : $('#root-url').val()+"/admin/property/customers/page",
            method	: "POST",
            data 	: data,
            dataType: "html",
            success: function (t) {
            	$('#member-list-content').html(t);
                $('#member-list-content').css('opacity','1');
				$('[data-toggle="tooltip"]').tooltip();
            }
		});
	}
</script>
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
@endsection
