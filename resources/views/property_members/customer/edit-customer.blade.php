{!! Form::model($customer,array('url' => '#', 'id' => 'edit-customer-form','class' => 'form-horizontal')) !!}
{!! Form::hidden('id') !!}
<div class="row form-group">
    <div class="col-sm-4">คำนำหน้า</div>
    <div class="col-sm-8">{!! Form::text('prefix_name',null,array('class' => 'form-control')) !!}</div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ชื่อ</div>
    <div class="col-sm-8">{!! Form::text('name',null,array('class' => 'form-control')) !!}</div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ประเภทการยืนยันตัวตน</div>
    <div class="col-sm-8">
        {!! Form::select('type_id_card',['1' => 'บัตรประจำตัวประชาชน', '2' => 'หนังสือเดินทาง'],null,array('class' => 'form-control')) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ID Card/Passport</div>
    <div class="col-sm-8">
        {!! Form::text('id_card',null,array('class' => 'form-control')) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ประเภทบุคคล</div>
    <div class="col-sm-8">
        {!! Form::select('person_type',['1' => 'บุคคลธรรมดา', '2' => 'นิติบุคคล'],null,array('class' => 'form-control')) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">สัญชาติ</div>
    <div class="col-sm-8">
        {!! Form::text('nationality',null,array('class' => 'form-control')) !!}
    </div>
</div>

<div class="row form-group">
    <div class="col-sm-4">อาชีพ</div>
    <div class="col-sm-8">
        {!! Form::text('job',null,array('class' => 'form-control')) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">หมายเลขโทรศัพท์</div>
    <div class="col-sm-8">
        {!! Form::text('phone',null,array('class' => 'form-control')) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">อีเมล</div>
    <div class="col-sm-8">
        {!! Form::text('email',null,array('class' => 'form-control')) !!}
    </div>
</div>
<?php /*
<hr/>
<h4>ข้อมูลที่พักอาศัย</h4>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>{{ trans('messages.unit_no_s') }}</th>
            <th width="180px">{{ trans('messages.Member.member_type') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($customer->customerProperty as $key => $cp )
        <tr>
            <td>{{ $cp->unit->unit_number }}</td>
            <td>
                @switch ($cp->customer_type)
                    @case(0) {{ trans('messages.Member.member_owner') }} @break
                    @case(1) {{ trans('messages.Member.member_crew') }} @break
                    @case(2) {{ trans('messages.Member.member_tenant') }} @break
                    @default - @break
                @endswitch
            </td>
        </tr>
    @endforeach
    </tbody>
</table> */ ?>
{!! Form::close() !!}
<script>
    $('#edit-customer-form').validate({
        rules: {
            name        : "required",
            id_card     : "required"
        }
    })
</script>
