@extends('layouts.blank')
@section('content')
    <?php $lang = App::getLocale(); ?>
    <tr>
        <td colspan="9" border="0"></td>
    </tr>
    <tr>
        <td style="font-size: 18; font-weight: bold;" colspan="9" border="0">
            {{ trans('messages.Member.customer_page_head') }}
        </td>
    </tr>
    <tr>
        <td colspan="9">{{ trans('messages.property_name') }} : {{ $property->{'property_name_'.$lang} }}</td>
    </tr>
    <tr>
        <td colspan="9" border="0"></td>
    </tr>
    <tr class="head">
        <th>{{ trans('messages.Report.sequence') }}</th>
        <th>{{ trans('messages.name') }}</th>
        <th>ประเภทการยืนยันตัวตน</th>
        <th>ID Card/Passport</th>
        <th>ประเภทบุคคล</th>
        <th>สัญชาติ</th>
        <th>อาชีพ</th>
        <th>หมายเลขโทรศัพท์</th>
        <th>{{ trans('messages.email') }}</th>
    </tr>

    @foreach($customers as $key => $customer)
        <tr class="content">
            <td style="text-align: center;">{{ $key+1 }}</td>
            <td>{{ $customer->prefix_name." ".$customer->name }}</td>
            <td>
                @switch ($customer->type_id_card)
                    @case(1) บัตรประจำตัวประชาชน @break
                    @case(2) หนังสือเดินทาง @break
                    @default - @break
                @endswitch
            </td>
            <td>
                {{ $customer->id_card }}&nbsp;
            </td>
            <td>
                @switch ($customer->person_type)
                    @case(1) บุคคลธรรมดา @break
                    @case(2) นิติบุคคล @break
                    @default - @break
                @endswitch
            </td>
            <td> {{ $customer->nationality }} </td>
            <td> {{ $customer->job }} </td>
            <td> {{ $customer->phone }} </td>
            <td> {{ $customer->email }} </td>
            <td>
                {{ localDateShortNotime( $customer->created_at) }}
            </td>
        </tr>
    @endforeach
@endsection