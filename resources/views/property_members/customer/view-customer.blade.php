<div class="row form-group">
    <div class="col-sm-4">ชื่อ</div>
    <div class="col-sm-8">{{ $customer->prefix_name." ".$customer->name }}</div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ประเภทการยืนยันตัวตน</div>
    <div class="col-sm-8">
        @switch ($customer->type_id_card)
            @case(1) บัตรประจำตัวประชาชน @break
            @case(2) หนังสือเดินทาง @break
            @default - @break
        @endswitch
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ID Card/Passport</div>
    <div class="col-sm-8">
        {{ $customer->id_card }}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">ประเภทบุคคล</div>
    <div class="col-sm-8">
        @switch ($customer->person_type)
            @case(1) บุคคลธรรมดา @break
            @case(2) นิติบุคคล @break
            @default - @break
        @endswitch
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">สัญชาติ</div>
    <div class="col-sm-8">
        {{ $customer->nationality }}
    </div>
</div>

<div class="row form-group">
    <div class="col-sm-4">อาชีพ</div>
    <div class="col-sm-8">
        {{ $customer->job }}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">หมายเลขโทรศัพท์</div>
    <div class="col-sm-8">
        {{ $customer->phone }}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-4">อีเมล</div>
    <div class="col-sm-8">
        {{ $customer->email }}
    </div>
</div>
<hr/>
<h4>ข้อมูลที่พักอาศัย</h4>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>{{ trans('messages.unit_no_s') }}</th>
            <th width="180px">{{ trans('messages.Member.member_type') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($customer->customerProperty as $key => $cp )
        <tr>
            <td>{{ $cp->unit->unit_number }}</td>
            <td>
                @switch ($cp->customer_type)
                    @case(0) {{ trans('messages.Member.member_owner') }} @break
                    @case(1) {{ trans('messages.Member.member_crew') }} @break
                    @case(2) {{ trans('messages.Member.member_tenant') }} @break
                    @default - @break
                @endswitch
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
