<div class="user-data-block">
	{!! Form::hidden('id',$member->id) !!}
	{!! Form::hidden('up_id',$up->id) !!}
	@if($member->profile_pic_name)
    <img src="{{ env('URL_S3')."/profile-img/".$member->profile_pic_path.$member->profile_pic_name }}" class="img-circle" alt="user-pic" />
    @else
    <img src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
    @endif
    <div class="user-detail">
		<div class="row" style="margin-bottom: 20px;">
			<div class="col-md-4"><b>{{ trans('messages.name') }} :</b></div>
			<div class="col-md-8">{{ $member->name }}</div>
		</div>
		<div class="row" style="margin-bottom: 20px;">
			<div class="col-md-4"><b>{{ trans('messages.email') }} :</b></div>
			<div class="col-md-8">{{ $member->email }}</div>
		</div>
		<div class="row" style="margin-bottom: 20px;">
			<div class="col-md-4"><b>{{ trans('messages.regis_date') }} :</b></div>
			<div class="col-md-8"><?php echo localDate($member->created_at); ?></div>
		</div>
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-md-4"><b>{{ trans('messages.tel') }} :</b></div>
			<div class="col-md-8">
				{!! Form::text('phone',$member->phone,['class' => 'form-control']) !!}
			</div>
		</div>
		@if( $up->user_type !== null )
		<div class="row">
			<div class="col-md-4"><b>{{ trans('messages.Member.member_type') }} :</b></div>
			<div class="col-md-8">
				@php
				$u_type = [
					"-" => trans('messages.Member.member_type'),
					0 => trans('messages.Member.member_owner'),
					1 => trans('messages.Member.member_crew'),
					2 => trans('messages.Member.member_tenant'),
					99 => 'Ordinary user',
				];
				if( $member->user_type == 2 || $member->user_type == 99) {
					Arr::forget($u_type,0);
					Arr::forget($u_type,1);
				} else {
					Arr::forget($u_type,2);
				}
				@endphp
				{!! Form::select('user_type',$u_type, $up->user_type, array('class' => 'form-control','id' => 'user-type')) !!}
			</div>
		</div>
		@endif
		@php
			if( $up->user_type === 0) {
				$d = "";
			} else {
				$d = 'display:none;';
			}
			if( $up->is_committee_member ) {
				$c = 'checked';
			} else {
				$c = "";
			}
		@endphp

		<div class="row" style="margin-top: 15px;{{ $d }}" id="c-member-check">
			<div class="col-md-4"><b>คณะกรรมการ :</b></div>
			<div class="col-md-8">
				<label>

				{!! Form::checkbox('is_committee_member',1,null,[$c]) !!} เป็นคณะกรรมการ
				</label>
			</div>
		</div>

	</div>
	<div style="clear:both"></div>
</div>
