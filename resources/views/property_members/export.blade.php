@extends('layouts.blank')
@section('content')
    <?php $lang = App::getLocale(); ?>
    <tr>
        <td colspan="7" border="0"></td>
    </tr>
    <tr>
        <td style="font-size: 18; font-weight: bold;" colspan="7" border="0">
            {{ trans('messages.Member.page_head') }}
        </td>
    </tr>
    <tr>
        <td colspan="7">{{ trans('messages.property_name') }} : {{ $property->{'property_name_'.$lang} }}</td>
    </tr>
    <tr>
        <td colspan="7" border="0"></td>
    </tr>
    <tr class="head">
        <th>{{ trans('messages.Report.sequence') }}</th>
        <th>{{ trans('messages.name') }}</th>
        <th>{{ trans('messages.email') }}</th>
        <th>{{ trans('messages.Member.member_type') }}</th>
        <th>{{ trans('messages.unit_no') }}</th>
        <th>{{ trans('messages.Member.account_status') }}</th>
        <th>{{ trans('messages.regis_date') }}</th>
    </tr>

    @foreach($members as $key => $member)
        <tr class="content">
            <td style="text-align: center;">{{ $key+1 }}</td>
            <td>{{ $member->ofUser->name }}</td>
            <td>{{ $member->ofUser->email }}</td>
            <td>
                @if( $member->user_type === null )
                    Unknown
                @else
                    @if($member->user_type == 0)
                    {{ trans('messages.Member.member_owner') }}
                    @elseif($member->user_type == 1)
                    {{ trans('messages.Member.member_crew') }}
                    @else
                    {{ trans('messages.Member.member_tenant') }}
                    @endif
                @endif
            </td>
            <td>
                @if($member->unit)
                {{ $member->unit->unit_number }}
                @endif
            </td>
            <td>
                @if($member->active_status)
                    {{ trans('messages.Member.account_active') }}
                @else
                    {{ trans('messages.Member.account_inactive') }}
                @endif
            </td>
            <td>
                {{ localDateShortNotime( $member->created_at) }}
            </td>
        </tr>
    @endforeach
@endsection