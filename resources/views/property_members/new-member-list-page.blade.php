@if($users->count())
<?php
    $allpage = $users->lastPage();
    $curPage = $users->currentPage();
    $lang = App::getLocale();
	$from   = (($users->currentPage()-1)*$users->perPage())+1;
	$to     = (($users->currentPage()-1)*$users->perPage())+$users->perPage();
	$to     = ($to > $users->total()) ? $users->total() : $to;
 ?>
<div class="row">
	<div class="col-sm-6" style="margin-bottom: 10px;"><p> {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$users->total()]) !!}</p></div>
	@if($allpage > 1)
		<div class="col-md-6 text-right">
			@if($curPage > 1)
				<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
			@endif
			@if($allpage  > 1)
				<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
			@endif
			@if($users->hasMorePages())
				<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
			@endif
		</div>
	@endif
</div>
<table class="table table-striped">
	<thead>
	<tr>
		<th width="180px" class="text-center">{{ trans('messages.unit_no') }}</th>
		<th width="*">Name</th>
		<th width="100px">Type</th>
		<th width="200px">ID Card</th>
		<th width="150px">Joined</th>
		<th width="200px">Action</th>
	</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr>
			<td class="text-center">{{ $user->unit->unit_number }}</td>
			{{--<td>img() {{ $user->OfUser->name }}</td>--}}
			<td class="user-image">
				@if($user->OfUser->profile_pic_name)
					<img style="margin-top:2px;" src="{{ env('URL_S3')."/profile-img/".$user->OfUser->profile_pic_path.$user->OfUser->profile_pic_name }}" class="img-circle" alt="user-pic" />
				@else
					<img style="margin-top:2px;" src="{{url('/')}}/images/user-1.png" alt="user-pic" class="img-circle" />
				@endif
				<div class="user-detail">
					<span style="display: block;margin-top: 5px;" class="name">{{ $user->OfUser->name }}</span>
					<span class="email" style="font-size: 12px;color: #8a8a8a;">{{ $user->OfUser->email }}</span>
				</div>
			</td>
			<td>
				@if( $user->user_type === null )
					Unknown
				@else
					@switch( $user->user_type )
						@case(0) Owner @break
						@case(1) Resident @break
						@default Tenant @break
					@endswitch
				@endif
			</td>
			<td>
				{{ $user->ofUser->id_card }}
			</td>
			<td>{{ localDate($user->created_at) }}</td>
			<td>
				<a href="#" class="btn btn-danger reject-user" data-upid="{{ $user->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.reject_new_user') }}">
					Reject
				</a>
				<a href="#" class="btn btn-success approve-user" data-upid="{{ $user->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.approve_new_user') }}" data-unknown="@if( $user->user_type === null ){{'true'}}@else{{'false'}}@endif">
					Approve
				</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
<?php /*@foreach($users as $user)
 <div class="invoice-row">
        <div class="invoice-head">
            <b>{{ $user->OfUser->name }} </b>
        </div>
        <div class="col-md-3">
            {{ trans('messages.unit_no') }} :
            {{ $user->OfUser->property_unit->unit_number }}
        </div>
        <div class="col-md-3">
           {{ trans('messages.email') }} :
           {{ $user->OfUser->email }}
       </div>
        <div class="col-md-3">
            {{ trans('messages.AdminUser.reg_date') }} : 
            {{ localDate($user->created_at) }}
       </div>
	 <div class="col-md-3 action-links text-right">
		 <a href="#" class="btn btn-danger reject-user" data-uid="{{ $user->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.reject_new_user') }}">
			 <i class="fa-ban"></i>
		 </a>
		 <a href="#" class="btn btn-success approve-user" data-uid="{{ $user->id }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.approve_new_user') }}">
			 <i class="fa-check"></i>
		 </a>
	 </div>
 </div>
 @endforeach */ ?>
<div class="clearfix"></div>
<div class="row">
	<div class="col-sm-6" style="margin-bottom: 10px;"><p> {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$users->total()]) !!}</p></div>
	@if($allpage > 1)
		<div class="col-md-6 text-right">
			@if($curPage > 1)
				<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage-1 }}">{{ trans('messages.prev') }}</a>
			@endif
			@if($allpage  > 1)
				<?php echo Form::selectRange('page', 1, $allpage, $curPage, ['class'=>'form-control paginate-select']); ?>
			@endif
			@if($users->hasMorePages())
				<a class="btn btn-white paginate-link" href="#" data-page="{{ $curPage+1 }}">{{ trans('messages.next') }}</a>
			@endif
		</div>
	@endif
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.AdminUser.user_not_found') }}</div><div class="clearfix"></div>
@endif
