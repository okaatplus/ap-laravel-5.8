@extends('layouts.base-admin')
@section('content')
<div class="page-title">
	<div class="title-env">
		<h1 class="title">{{ trans('messages.AdminUser.new_page_waiting_head') }}</h1>
	</div>
	<div class="breadcrumb-env">
		<ol class="breadcrumb bc-1" >
			<li>
				<a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
			</li>
			<li>
				<a href="#"><i class="fa-group"></i> {{ trans('messages.Member.page_head_group') }}</a>
			</li>
			<li class="active"><a href="#"> {{ trans('messages.AdminUser.new_page_waiting_head') }}</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		@include('property_units.tab-menu')
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<form method="POST" id="search-user-form" action="{{ url('admin/property/new-members/export') }}" accept-charset="UTF-8">
					<div class="row">
						<div class="col-sm-3 block-input">
							<input class="form-control datepicker" size="25" placeholder="{{ trans('messages.name') }}" name="name">
						</div>

						<div class="col-sm-3 block-input">
							{!! Form::select('property_unit_id', $property_unit_list,null,['id'=>'property-list','class'=>'form-control select2']) !!}
						</div>

						<div class="col-sm-6 pull-right text-right">
							<button type="reset" class="btn btn-single btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
							<button type="button" class="btn btn-single btn-secondary search-user">{{ trans('messages.search') }}</button>
							<button type="submit" class="btn btn-secondary btn-single">{{ trans('messages.Report.download') }}</button>
						</div>
					</div>
				</form>
				<hr/>
				<div id="users-list-content">
					@include('property_members.new-member-list-page')
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-user-reject">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-ban"></i> {{ trans('messages.AdminUser.confirm_reject_head') }}</h4>
			</div>
			<div class="modal-body"> {{ trans('messages.AdminUser.confirm_reject_new_user') }} </div>
			<div class="modal-footer">
				<button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="botton" id="confirm-reject-user" class="btn btn-primary">{{ trans('messages.confirm') }}</button>
			</div>
		</div>
	 </div>
</div>

<div class="modal fade" id="modal-user-approve">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa-check"></i> {{ trans('messages.AdminUser.confirm_approve_head') }}</h4>
			</div>
			<div class="modal-body">
				<div class="row form-group">
					<div class="col-md-12">
						{{ trans('messages.AdminUser.confirm_approve_new_user') }}
						<div class="unknown-user-block" style="display: none;">และเนื่องจากยังไม่มีการระบุสถานะของผู้ใช้งาน ก่อนทำการอนุมัติกรุณาเลือกสถานะผู้ใช้งานการใช้ระบบจากตัวเลือกด้านล่าง</div>
					</div>
				</div>
				<div class="row form-group unknown-user-block" style="display: none;">
					<label class="col-sm-4 control-label">{{ trans('messages.Member.member_type') }} </label>
					<div class="col-md-8">
						{!! Form::select('user_type',[
								'-' => trans('messages.Member.member_type'),
                                0 => trans('messages.Member.member_owner'),
                                1 => trans('messages.Member.member_crew'),
                                2 => trans('messages.Member.member_tenant')
                            ],null,array('class' => 'form-control', 'id' => 'role-select'))
                        !!}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
				<button type="botton" id="confirm-approve-user" class="btn btn-primary">{{ trans('messages.confirm') }}</button>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
	<script type="text/javascript">
	$(function (){
		var target;
		var flag_unknown = false;
		$('#users-list-content').on('click','.user-pagination li a', function (e) {
			e.preventDefault();
			searchUser ($(this).data('page'));
		});

		$("#property-list").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });

		$('.search-user').on('click',function () {
			searchUser (1);
		});

		$('.reset-s-btn').on('click',function () {
			$(this).closest('form').find("input").val("");
			$(this).closest('form').find("select option:selected").removeAttr('selected');
			$('#property-list').val('-').trigger('change');
			searchUser (1);
		});

		$('#users-list-content').on('click','.paginate-link', function (e) {
			e.preventDefault();
			searchUser ($(this).data('page'));
		});

		$('#users-list-content').on('change','.paginate-select', function (e) {
			e.preventDefault();
			searchUser ($(this).val());
		});

		$('.search-member').on('click', function () {
			searchUser (1)
		});

		$('#search-user-form').on('keydown', function(e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				searchUser (1);
			}
		});

		$('.btn-delete-user').on('click', function () {
			target = $(this).data('id');
		});

		$('body').on('click', '.reject-user', function (e){
			e.preventDefault();
			tmp = $(this);
			$('#modal-user-reject .modal-body span').html(name);
			$('#modal-user-reject').modal('show');
		});

		$('#confirm-reject-user').on('click', function () {
			$(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
			rejectNewUser(tmp.attr('data-upid'));
		});

        $('body').on('click', '.approve-user', function (e){
            e.preventDefault();
            tmp = $(this);
			$('#role-select').removeClass('error');
            if( $(this).attr('data-unknown') == 'true') {
            	$('.unknown-user-block').show();
				flag_unknown = true;
			} else {
				$('.unknown-user-block').hide();
				flag_unknown = false;
			}
            $('#modal-user-approve .modal-body span').html(name);
            $('#modal-user-approve').modal('show');
        });

        $('#confirm-approve-user').on('click', function () {

            if( flag_unknown && $('#role-select').val() == '-') {
				$('#role-select').addClass('error');
				return false;
			}
			$(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            approveNewUser(tmp.attr('data-upid'));
        });

		function searchUser (page) {
			$('#users-list-content').css('opacity','0.6');
			var data = $('#search-user-form').serialize();
			data+= "&page="+page;
			$.ajax({
				url     : $('#root-url').val()+"/admin/property/new-members",
	            method	: "POST",
	            data 	: data,
	            dataType: "html",
	            success: function (t) {
	            	$('#users-list-content').html(t);
	            	$('#users-list-content').css('opacity','1');
					$('[data-toggle="tooltip"]').tooltip();
	            }
			});
		}

		function rejectNewUser (upid) {
			$.ajax({
				url     : $('#root-url').val()+"/admin/property/members/reject",
				method	: "POST",
				data 	: ({upid:upid}),
				dataType: "json",
				success: function (r) {
					if(r.result) {
						$('#confirm-reject-user').removeAttr('disabled').find('.fa-spin').remove();
						$('#modal-user-reject').modal('hide');
						searchUser(1);
					}
				}
			});
		}

        function approveNewUser (upid) {
            $.ajax({
                url     : $('#root-url').val()+"/admin/property/members/approve",
                method	: "POST",
                data 	: ({upid:upid,type:$('#role-select').val()}),
                dataType: "json",
                success: function (r) {
                    if(r.result) {
						$('#confirm-approve-user').removeAttr('disabled').find('.fa-spin').remove();
						$('#modal-user-approve').modal('hide');
						searchUser(1);
                    }
                }
            });
        }
	});
	</script>
	<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
	<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
@endsection
