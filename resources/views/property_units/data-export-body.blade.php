<tr class="head">
    <th>{{ trans('messages.unit_no') }}</th>
    <th>{{ trans('messages.Prop_unit.type') }}</th>
    <th>{{ trans('messages.Prop_unit.owner_name') }}</th>
    <th>{{ trans('messages.Prop_unit.unit_area') }}</th>
    <th>Asset ID</th>
    <th>Asset Project ID</th>
    <th>{{ trans('messages.Prop_unit.member_amont') }}</th>
    <th>{{ trans('messages.Prop_unit.transferred_date') }}</th>
    <th>{{ trans('messages.Prop_unit.insurance_expire_date') }}</th>
    <th>{{ trans('messages.Prop_unit.emergency_cal') }}</th>
    <th>{{ trans('messages.Prop_unit.delivery_address') }}</th>
    <th>{{ trans('messages.email') }}</th>
    <th>{{ trans('messages.Prop_unit.building') }}</th>
    <th>{{ trans('messages.Prop_unit.unit_floor') }}</th>
</tr>
@foreach($units as $unit)
    <tr class="content">
        <td style="text-align: center;">{{ $unit->unit_number }}</td>
        <td style="text-align: center;">
            @if($unit->type == 0)
                {{ trans('messages.Prop_unit.property') }}
            @elseif($unit->type == 1)
                {{ trans('messages.Prop_unit.none') }}
            @else
                {{ trans('messages.Prop_unit.hold_by_developer') }}
            @endif
        </td>
        <td>@if($unit->{'owner_name_'.$lang}) {{ $unit->{'owner_name_'.$lang} }} @else - @endif</td>
        <td style="text-align: right;">@if($unit->property_size) {{ $unit->property_size }} @else - @endif</td>
        <td style="text-align: right;">@if($unit->asset_id) {{ $unit->asset_id }} @else - @endif</td>
        <td style="text-align: right;">@if($unit->asset_project_id) {{ $unit->asset_project_id }} @else - @endif</td>
        <td style="text-align: right;">@if($unit->resident_count) {{ $unit->resident_count}} @else - @endif</td>
        <td style="text-align: left;">{{($unit->transferred_date != null) ? localDate($unit->transferred_date) : '-' }}</td>
        <td style="text-align: left;">{{($unit->insurance_expire_date != null) ? localDate($unit->insurance_expire_date) : '-' }}</td>
        <td style="text-align: left;">@if($unit->phone) {{$unit->phone}}  @else - @endif</td>
        <td style="text-align: left;">@if($unit->delivery_address) {{ $unit->delivery_address }} @else - @endif</td>
        <td style="text-align: left;">@if($unit->email) {{ $unit->email }} @else - @endif</td>
        <td style="text-align: left;">@if($unit->building) {{ $unit->building }} @else - @endif</td>
        <td style="text-align: left;">@if($unit->unit_floor) {{ $unit->unit_floor }} @else - @endif</td>
    </tr>

@endforeach