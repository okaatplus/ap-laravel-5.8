@extends('layouts.blank')
@section('content')
    <?php $lang = App::getLocale();?>
    <tr>
        <td colspan="5"><h2>{{ trans('messages.Prop_unit.page_head') }} {{ $property->{'juristic_person_name_'.$lang} }}</h2></td>
    </tr>
    @include('property_units.data-export-body')

    <style>
    td .label-danger   {color:#ff0000;}
    td .label-warning  {color:#ffba00;}
    td .label-blue     {color:#0e62c7;}
    td .label-secondary {color:#68b828;}
    td .label-default   {color:#777;}
    </style>
@endsection
