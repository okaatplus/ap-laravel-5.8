<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <label class="col-sm-4 control-label text-bold" for="number">{{ trans('messages.unit_no') }}</label>
            <div class="col-sm-3">
                {{ $unit->unit_number }}
            </div>
            <label class="col-sm-2 control-label text-bold" for="number">{{ trans('messages.Prop_unit.building') }}</label>
            <div class="col-sm-3">
                @if( $unit->building )
                {{ $unit->building }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold" for="number">{{ trans('messages.Prop_unit.unit_floor') }}</label>
            <div class="col-sm-3">
                @if( $unit->unit_floor )
                    {{ $unit->unit_floor }}
                @else - @endif
            </div>
            <label class="col-sm-2 control-label text-bold" for="number">{{ trans('messages.Prop_unit.unit_soi') }}</label>
            <div class="col-sm-3">
                @if( $unit->unit_soi )
                    {{ $unit->unit_soi }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold" for="number">Project Code</label>
            <div class="col-sm-3">
                @if( $unit->asset_project_id )
                    {{ $unit->asset_project_id }}
                @else - @endif
            </div>
            <label class="col-sm-2 control-label text-bold" for="number">Asset ID</label>
            <div class="col-sm-3">
                @if( $unit->asset_id )
                    {{ $unit->asset_id }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold">Land Number</label>
            <div class="col-sm-8">
                @if( $unit->land_no )
                    {{ $unit->land_no }}
                @else - @endif
            </div>
        </div>

        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.owner_name_th') }}</label>
            <div class="col-sm-8">
                @if( $unit->owner_name_th )
                    {{ $unit->owner_name_th }}
                @else - @endif
            </div>
        </div>

        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.owner_name_en') }}</label>
            <div class="col-sm-8">
                @if( $unit->owner_name_en )
                    {{ $unit->owner_name_en }}
                @else - @endif
            </div>
        </div>

        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.type') }}</label>
            <div class="col-sm-8">
                @if($unit->type == 0)
                    {{ trans('messages.Prop_unit.property') }}
                @elseif($unit->type == 1)
                    {{ trans('messages.Prop_unit.none') }}
                @else
                    {{ trans('messages.Prop_unit.hold_by_developer') }}
                @endif
            </div>
        </div>

        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.unit_area') }}</label>
            <div class="col-sm-8">
                @if( $unit->property_size )
                    {{ $unit->property_size }}
                @else - @endif
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.member_amont') }}</label>
            <div class="col-sm-8">
                @if( $unit->resident_count )
                    {{ $unit->resident_count }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.emergency_cal') }}</label>
            <div class="col-sm-8">
                @if( $unit->phone )
                    {{ $unit->phone }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.email') }}</label>
            <div class="col-sm-8">
                @if( $unit->email )
                    {{ $unit->email }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.transferred_date') }}</label>
            <div class="col-sm-8">
                @if( $unit->transferred_date ) {{ localDate( $unit->transferred_date ) }} @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.insurance_expire_date') }}</label>
            <div class="col-sm-8">
                @if( $unit->insurance_expire_date ) {{ localDate( $unit->insurance_expire_date ) }} @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold">{{ trans('messages.Prop_unit.delivery_address') }}</label>
            <div class="col-sm-8">
                @if( $unit->delivery_address )
                    {{ $unit->delivery_address }}
                @else - @endif
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4 control-label text-bold" for="urgent-phone">{{ trans('messages.Prop_unit.contact_lang') }}</label>
            <div class="col-sm-8">
                @if($unit->contact_lang == 'th')
                    ไทย
                @else
                    English
                @endif
            </div>
        </div>
    </div>
</div>
@if( $unit->PropertyUnitCustomer->count() )
<hr/>
<h3>เจ้าบ้านและผู้อาศัย</h3>
<div class="table-responsive dataTables_wrapper ">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="*">{{ trans('messages.name') }}</th>
                <th width="200px">{{ trans('messages.type') }}</th>
                <th width="400px">{{ trans('messages.Customer.identify_type') }}</th>
                <th width="150px">{{ trans('messages.Customer.customer_code') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach( $unit->PropertyUnitCustomer as $PropCus )
            <tr>
                <td>{{ $PropCus->customer->name }}</td>
                <td>
                    @switch( $PropCus->customer_type )
                        @case(0) เจ้าของร่วม @break
                        @default ผู้พักอาศัย @break
                    @endswitch
                </td>
                <td>
                    @switch ($PropCus->customer->type_id_card)
                    @case(1) บัตรประจำตัวประชาชน : @break
                    @case(2) หนังสือเดินทาง : @break
                    @default - @break
                    @endswitch
                    {{ $PropCus->customer->id_card }}
                </td>
                <td>{{ $PropCus->customer->customer_code }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@if( $unit->PropertyUnitTenant->count() )
    <hr/>
    <h3>ผู้เช่า</h3>
    <div class="table-responsive dataTables_wrapper ">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th width="*">{{ trans('messages.name') }}</th>
                <th width="400px;">{{ trans('messages.Customer.identify_type') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $unit->PropertyUnitTenant as $PropTen )
                <tr>
                    <td>{{ $PropTen->tenant->name }}</td>
                    <td>
                        @switch ($PropTen->tenant->type_id_card)
                        @case(1) บัตรประจำตัวประชาชน : @break
                        @case(2) หนังสือเดินทาง : @break
                        @default - @break
                        @endswitch
                        {{ $PropTen->tenant->id_card }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
