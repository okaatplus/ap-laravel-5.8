<ul class="nav nav-tabs">
    <li @if(isset($active_menu) && $active_menu == "units") class="active" @endif>
        <a href="{{ url('admin/property/units') }}">
            <span>{{ trans('messages.Prop_unit.page_head') }}</span>
        </a>
    </li>
    <li @if(Request::is('admin/property/members')) class="active" @endif>
        <a href="{{ url('admin/property/members') }}">
            <span>{{ trans('messages.Member.page_head') }}</span>
        </a>
    </li>
    <li @if(Request::is('admin/property/new-members')) class="active" @endif>
        <a href="{{ url('admin/property/new-members') }}">
            <span>{{ trans('messages.AdminUser.new_page_head') }}</span>
        </a>
    </li>
    <li @if(Request::is('admin/property/customers')) class="active" @endif>
        <a href="{{ url('admin/property/customers') }}">
            <span>{{ trans('messages.Member.customer_page_head') }}</span>
        </a>
    </li>
</ul>