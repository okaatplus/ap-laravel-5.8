@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Prop_unit.page_head') }}</h1>
			{{--<p class="description">{{ trans('messages.Prop_unit.page_sub_head') }}</p>--}}
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
				<li class="active">
					<strong>{{ trans('messages.Prop_unit.page_head') }}</strong>
				</li>
			</ol>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
            @include('property_units.tab-menu')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php /*{!! Form::open(array('url' => array('admin/property/units/export'),'id' => 'property-unit-report','class'=>'form-horizontal','method'=>'post')) !!}
                <button type="submit" class="action-float-right btn btn-primary"><i class="fa fa-download"></i> {{ trans('messages.Report.download') }}</button>
            {!! Form::close() !!} */ ?>
            <div class="panel panel-default">
                <div class="panel-body unit-list-content">
                    <div class="tab-pane active" id="unit-list">
                        <form method="POST" id="search-member-form" action="{{ url('admin/property/units/export') }}" accept-charset="UTF-8" class="form-horizontal">
                            <div class="row">
                                <div class="col-sm-3 block-input">
                                    {!! Form::select('unit_id', $unit_list,null,['class'=>'form-control select2','id' => 'unit-id']) !!}
                                </div>

                                <div class="col-sm-9 block-input text-right">
                                    <button type="reset" id="reset-search" class="btn btn-single btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
                                    <button type="button" class="btn btn-secondary btn-single search-member">{{ trans('messages.search') }}</button>
                                    <button type="submit" class="btn btn-single btn-primary">{{ trans('messages.Report.download') }}</button>
                                </div>
                            </div>
                        </form>
                        <hr/>
                        <div id="unit-list-content">
                            @include('property_units.unit-list-page')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	 <div class="modal fade" id="get-unit">
        <div class="modal-dialog" style="min-width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Prop_unit.unit_detail')}}</h4>
                </div>
                <div class="modal-body">
                    <div id="unit-content">

                    </div>
                   <span class="u-loading">{{ trans('messages.loading') }}...</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-unit-modal">
        <div class="modal-dialog" style="min-width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Prop_unit.edit_unit') }}</h4>
                </div>
                {!! Form::open(array('url'=>'admin/property/units/edit','method'=>'post','id'=>'form-edit-unit','class'=>'form-horizontal')) !!}
                <div class="modal-body">
                	<div id="unit-edit-content">

                    </div>
                   <span class="ue-loading">{{ trans('messages.loading') }}...</span>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                	<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="save-edit-unit">{{ trans('messages.ok') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

	<div class="modal fade" id="clear-unit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Prop_unit.clear_unit') }}</h4>
                </div>
                {!! Form::open(array('url'=>'admin/property/units/clear','method'=>'post','id'=>'form-clear-unit','class'=>'form-horizontal')) !!}
				{!! Form::hidden('id',null,['id'=>'clear-unit-id']) !!}
                <div class="modal-body">
					{!! trans('messages.Prop_unit.clear_unit_label') !!} <b id="clear-unit-no-label"></b>
					<div style="margin:9px 0;"><b>{{ trans('messages.Prop_unit.clear_unit_form_head') }}</b></div>
					<div class="form-group">
					    <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.owner_name_th') }}</label>
					    <div class="col-sm-8">
					        {!! Form::text('owner_name_th',null,array('class'=>'form-control','maxlength'=>100)) !!}
					    </div>
					</div>

					<div class="form-group">
					    <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.owner_name_en') }}</label>
					    <div class="col-sm-8">
					        {!! Form::text('owner_name_en',null,array('class'=>'form-control','maxlength'=>100)) !!}
					    </div>
					</div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">ประเภทการยืนยันตัวตน</label>
                        <div class="col-sm-8">
                            {!! Form::select('identify_type',['1' => 'หมายเลขบัตรประจำตัวประชาชน','2' => 'หมายเลข Passport'],null,array('class'=>'form-control','id' => 'id-card-type')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">ID Card/Passport</label>
                        <div class="col-sm-8">
                            {!! Form::text('id_card',null,array('class'=>'form-control','maxlength'=>100,'id' => 'id-card-input')) !!}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                	<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="comfirm-clear-unit">{{ trans('messages.confirm') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.th.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/number.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
<script type="text/javascript">
	$(function (){
        $("#unit-id").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });

        $('#form-clear-unit').validate({
            ignore: ".ignore-validate",
            rules: {
                owner_name_th   : "required",
                owner_name_en   : "required",
                id_card         : "required"
            },
            errorPlacement: function(error, element) {}
        });
        
        $("#unit-area-create,#extra-charge-create").number(true ,2);
        $("[rel='tooltip']").tooltip();


        $('#save-edit-unit').on('click',function () {
            if($('#form-edit-unit').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#form-edit-unit').submit();
            }
        });


        $('#get-unit').on('shown.bs.modal', function() {
		    $(window).resize();
		});

		$('body').on('click','.unit-view-button',function (e){
			e.preventDefault();
			$('#get-unit').modal('show')
			getUnit($(this).attr('data-unit'));
		});

		$('body').on('click','.unit-edit-button',function (e){
			e.preventDefault();
			$('#edit-unit-modal').modal('show')
			getUnitForm($(this).attr('data-unit'));
		});
        
		$('body').on('click','.unit-clear-button',function (e){
			e.preventDefault();
			$('#clear-unit-id').val($(this).data('unit'));
			$('#clear-unit-no-label').html($(this).data('unit-no'));
			$('#clear-unit-modal').modal('show')
		});

		$('#comfirm-clear-unit').on('click',function () {
            var valid_id_card = checkValidIDCard ();
            if( $('#form-clear-unit').valid() && valid_id_card) {
                $(this).attr('disabled', 'disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#form-clear-unit').submit();
            }
		});

        $('#unit-list-content').on('click','.paginate-link', function (e){
            e.preventDefault();
            var page = $(this).data('page');
            getPropertyUnitPage (page);
        });

        $('#unit-list-content').on('change','.paginate-select', function (e){
            getPropertyUnitPage ($(this).val());
        });

        $('#reset-search').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            $('#unit-id').val('-').trigger('change');
            searchMember (1);
        });

        $('.search-member').on('click', function () {
            searchMember (1)
        });

        $('#search-member-form').on('keydown', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                searchMember (1);
            }
        });
	});

	function  checkValidIDCard () {
        var valid = true;
        if( $('#id-card-type').val() == 1) {
            if( $('#id-card-input').val().length != 13 ) {

                $('#id-card-input').addClass('error__');
                valid = false;
            } else {
                if( !$('#id-card-input').val().match(/^\d+$/) ) {

                    $('#id-card-input').addClass('error__');
                    valid = false;
                } else {
                    $('#id-card-input').removeClass('error__');
                }
            }
        }

        return valid;
    }

	function getUnit (id) {
		$('#unit-content').hide();
		$('.u-loading').show();
		$.ajax({
			url     : $('#root-url').val()+"/admin/property/units/getUnit",
            method	: "POST",
            data 	: ({unit_id:id}),
            dataType: "html",
            success: function (t) {
            	$('#unit-content').html(t).fadeIn(300,function () { $(window).resize(); });
				$('.u-loading').hide();
            	$("#unit-area").number(true ,2);
            	$(window).resize();
            }
		});
	}

    function getPropertyUnitPage (page) {
        $('#unit-list-content').css('opacity','0.6');
        //var page = $(this).data('page');
        var r = $.ajax({
            url:  $('#root-url').val()+"/admin/property/units",
            data:({'page':page}),
            dataType: "html"
        });
        r.done(function( result ) {
            $('#unit-list-content').html( result ).css('opacity','1');
            $('[data-toggle="tooltip"],[rel="tooltip"]').tooltip();
        });
    }

	function getUnitForm (id) {
		$('#unit-edit-content').hide();
		$('.ue-loading').show();
		$.ajax({
			url     : $('#root-url').val()+"/admin/property/units/edit/form",
            method	: "POST",
            data 	: ({unit_id:id}),
            dataType: "html",
            success: function (t) {
            	$('#unit-edit-content').html(t).fadeIn(300,function () {
                    $('.edit_picker_transferred_date').datepicker({
                        'format' : "yyyy/mm/dd",
                        'language' : "{{ App::getLocale() }}"
                    });
                    $('.edit_picker_insurance_expire_date').datepicker({
                        'format' : "yyyy/mm/dd",
                        'language' : "{{ App::getLocale() }}"
                    });
            		$(window).resize();
            		$('#form-edit-unit').validate({
			            rules: {
			                unit_number : "required"
			            },
			            errorPlacement: function(error, element) {}
			        })
                    $("#unit-area,.input-number").number(true,2);

                    $("#form-edit-unit select").selectBoxIt().on('open', function(){
                        $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
                    });
                    $(window).resize();
            	});
				$('.ue-loading').hide();
            }
		});
	}

    function searchMember (page) {
        var data = $('#search-member-form').serialize();
        data+= "&page="+page;
        $('#unit-list-content').css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/admin/property/units",
            method	: "POST",
            data 	: data,
            dataType: "html",
            success: function (t) {
                $('#unit-list-content').html(t);
                $('#unit-list-content').css('opacity','1');

                $('[data-toggle="tooltip"]').each(function(i, el)
                {
                    var $this = $(el),
                            placement = attrDefault($this, 'placement', 'top'),
                            trigger = attrDefault($this, 'trigger', 'hover'),
                            tooltip_class = $this.get(0).className.match(/(tooltip-[a-z0-9]+)/i);

                    $this.tooltip({
                        placement: placement,
                        trigger: trigger
                    });

                    if(tooltip_class)
                    {
                        $this.removeClass(tooltip_class[1]);

                        $this.on('show.bs.tooltip', function(ev)
                        {
                            setTimeout(function()
                            {
                                var $tooltip = $this.next();
                                $tooltip.addClass(tooltip_class[1]);

                            }, 0);
                        });
                    }
                });
            }
        });
    }
</script>


<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
@endsection
