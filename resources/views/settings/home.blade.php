@extends('layouts.base-admin')
@section('content')
<?php

    $lang = App::getLocale();
    $vehicle_type   = unserialize(constant('VEHICLE_TYPE_'.strtoupper($lang)));
    $pet_type       = unserialize(constant('PET_TYPE_'.strtoupper($lang)));
?>
    <div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Settings.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li>
                    <a href="{{ url('settings') }}"><i class="fa-gears"></i>{{ trans('messages.Settings.page_head') }}</a>
                </li>
				<li class="active">
                    <strong>{{ trans('messages.Settings.home_settings') }}</strong>
				</li>
			</ol>
    	</div>
    </div>
     <section class="mailbox-env">
        <div class="row">
            <div class="col-sm-9 mailbox-right">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('messages.Settings.home_address') }}</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            <h4>{{ $home->unit_number }}</h4>
                            <h4>{{ $property->{'property_name_'.$lang} }}</h4>
                        </p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('messages.Settings.home_settings') }}</h3>
                    </div>
                    <div class="panel-body">

                        <form role="form" class="form-horizontal" method="post" action="{{ url('settings/home/save') }}">

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="number">{{ trans('messages.Prop_unit.owner_name_th') }}</label>

                                <div class="col-sm-8">
                                    {!! Form::text('owner_name_th',$home->owner_name_th,array('class'=>'form-control','maxlength'=>100)) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="number">{{ trans('messages.Prop_unit.owner_name_en') }}</label>

                                <div class="col-sm-8">
                                    {!! Form::text('owner_name_en',$home->owner_name_en,array('class'=>'form-control','maxlength'=>100)) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="number">{{ trans('messages.Prop_unit.member_amont') }}</label>

                                <div class="col-sm-8">
                                    {!! Form::selectRange('resident_count',1,10,$home->resident_count,array('class'=>'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="urgent-phone">{{ trans('messages.Prop_unit.emergency_cal') }}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="phone" id="urgent-phone" value="{{$home->phone}}" placeholder="{{ trans('messages.tel') }}" maxlength=10>
                                </div>
                            </div>

                            <div class="form-group-separator"></div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-primary btn-single pull-right" id="save-settings">{{ trans('messages.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('messages.Prop_unit.vehicle') }}</h3>
                    </div>
                    <div class="panel-body search-form">
                        <form method="post" class="form-inline" id="add-vehicle-form" action="{{ url('settings/home/vehicle/add') }}">
                            <div class="form-group" style="min-width:150px;">
                            	{!! Form::select('type',$vehicle_type,null,array('class'=>'form-control','id'=>'v-type')) !!}
                            </div>
                            <div class="form-group" id="other_brand">
                                <input type="text" name="o_brand" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_brand') }}" maxlength=100 />
                            </div>
                            <div class="form-group" id="specific_brand" style="display:none;">
                                <select name="s_brand" id="s_brand_input" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <input type="text" name="model" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_model') }}" maxlength=200 />
                            </div>

                            <div class="form-group-separator-noline"></div>

                            <div class="form-group">
                                <input type="text" name="color" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_color') }}" maxlength=100 />
                            </div>

                            <div class="form-group" style="min-width:80px;">
                               {!! Form::select('year',selectYearOption (),null,array('class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <input type="text" name="lisence_plate" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_plate_serial') }}" maxlength=200 />
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-single" id="add-vehicle-btn">{{ trans('messages.add') }}</button>
                            </div>

                            <div class="form-group-separator-noline"></div>
                            @if(!$home->home_vehicle->isEmpty())
                            @foreach( $home->home_vehicle as $vehicle )
                            <?php
                                $exp = false;
                                if($vehicle->sticker_expire_date) {
                                    $exp = laterThanNow_ ($vehicle->sticker_expire_date);
                                }
                            ?>
                            <div class="invoice-row">
                                <div class="invoice-head">
                                    <b>{{ trans('messages.Prop_unit.veh_plate_serial') }}  : {{ $vehicle->lisence_plate }} </b>
                                </div>
                                <div class="col-md-3">
                                   {{ trans('messages.type') }} :
                                   {{ $vehicle_type[$vehicle->type] }}
                               </div>
                                <div class="col-md-3">
                                    {{ trans('messages.Prop_unit.veh_brand') }} :
                                    {{ $vehicle->brand }}
                                </div>
                                <div class="col-md-3">
                                    {{ trans('messages.Prop_unit.veh_model') }} :
                                    {{ $vehicle->model }}
                               </div>
                               <div class="col-md-3">
                                   {{ trans('messages.Prop_unit.veh_color') }} :
                                   {{ $vehicle->color }}
                              </div>
                              <div class="col-md-3">
                                  {{ trans_choice('messages.year',1) }} :
                                  {{ $vehicle->year }}
                             </div>
                               <div class="col-md-6">
                                   {{ trans('messages.Prop_unit.veh_sticker_status') }} :
                                   @if($exp)
                                   {{ trans('messages.Vehicle.sticker_status_4') }}
                                   @else
                                   {{ trans('messages.Vehicle.sticker_status_'.$vehicle->sticker_status) }}
                                   @endif
                              </div>
                              <div class="col-md-3 action-links text-right">
                                    <a href="#" class="edit get-vehicle btn btn-blue" data-toggle="modal" data-target="#get-vehicle" data-vehicle-id="{{ $vehicle->id }}">
                                        <i class="fa-eye"></i>
                                    </a>
                                    @if($featured_menu && $featured_menu['menu_vehicle'])
                                        @if($vehicle->sticker_status == 0 || $exp)
                                        <a href="#" data-original-title="{{ trans('messages.Prop_unit.veh_sticker_request') }}" class="delete request-sticker btn btn-success" data-toggle="tooltip" data-placement="top" data-vehicle-id="{{ $vehicle->id }}" data-plate="{{ $vehicle->lisence_plate }}">
                                            <i class="fa-road"></i>
                                        </a>
                                      @endif
                                    @endif
                                    <a href="#" class="delete delete-vehicle btn btn-danger" data-toggle="modal" data-target="#confirm-delete-vehicle" data-vehicle-id="{{ $vehicle->id }}" data-plate="{{ $vehicle->lisence_plate }}">
                                        <i class="fa-trash"></i>
                                    </a>
                              </div>
                          </div>
                          @endforeach
                            <?php /*
                            <div class="table-responsive dataTables_wrapper">
                                <table class="table table-hover middle-align" style="min-width:855px;">
                                    <thead>
                                    <tr>
                                    	<th>{{ trans('messages.Prop_unit.veh_plate_serial') }}</th>
                                        <th>{{ trans('messages.type') }}</th>
                                        <th>{{ trans('messages.Prop_unit.veh_brand') }}</th>
                                        <th>{{ trans('messages.Prop_unit.veh_model') }}</th>
                                        <th>{{ trans('messages.Prop_unit.veh_color') }}</th>
                                        <th>{{ trans_choice('messages.year',1) }}</th>
                                        <th>{{ trans('messages.Prop_unit.veh_sticker_status') }}</th>
                                        <th>{{ trans('messages.action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $home->home_vehicle as $vehicle )
                                    <?php
                                        $exp = false;
                                        if($vehicle->sticker_expire_date) {
                                            $exp = laterThanNow_ ($vehicle->sticker_expire_date);
                                        }
                                    ?>
                                    <tr>
                                    	<td class="vehicle-lisence-plate">
                                            <a href="#" class="name">{{ $vehicle->lisence_plate }}</a>
                                        </td>
                                        <td class="vehicle-type">
                                            {{ $vehicle_type[$vehicle->type] }}
                                        </td>
                                        <td class="vehicle-brand">
                                            {{ $vehicle->brand }}
                                        </td>
                                        <td class="vehicle-name">
                                            {{ $vehicle->model }}
                                        </td>
                                        <td class="vehicle-name">
                                            {{ $vehicle->color }}
                                        </td>
                                        <td class="vehicle-year">
                                            {{ $vehicle->year }}
                                        </td>
                                        <td class="vehicle-sticker">
                                            @if($exp)
                                            {{ trans('messages.Vehicle.sticker_status_4') }}
                                            @else
                                            {{ trans('messages.Vehicle.sticker_status_'.$vehicle->sticker_status) }}
                                            @endif
                                        </td>
                                        <td class="action-links">
                                            <a href="#" class="edit get-vehicle btn btn-blue" data-toggle="modal" data-target="#get-vehicle" data-vehicle-id="{{ $vehicle->id }}">
                                                <i class="fa-eye"></i>
                                            </a>
                                            @if($vehicle->sticker_status == 0 || $exp)
                                             <a href="#" data-original-title="{{ trans('messages.Prop_unit.veh_sticker_request') }}" class="delete request-sticker btn btn-success" data-toggle="tooltip" data-placement="top" data-vehicle-id="{{ $vehicle->id }}" data-plate="{{ $vehicle->lisence_plate }}">
                                                <i class="fa-road"></i>
                                            </a>
                                            @endif
                                            <a href="#" class="delete delete-vehicle btn btn-danger" data-toggle="modal" data-target="#confirm-delete-vehicle" data-vehicle-id="{{ $vehicle->id }}" data-plate="{{ $vehicle->lisence_plate }}">
                                                <i class="fa-trash"></i>
                                            </a>

                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>*/ ?>
                            @endif
                        </form>
                    </div>
                </div>

                <!-- Keycard -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Keycard</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" class="form-inline" id="add-keycard-form" action="{{ url('settings/home/key-card/add') }}">
                            <div class="form-group">
                                <input type="text" name="serial_number" class="form-control" placeholder="Serial Card" />
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-single" id="add-keycard-btn">{{ trans('messages.add') }}</button>
                            </div>
                            <div class="form-group-separator-noline"></div>
                            @if(!$home->home_keycard->isEmpty())
                                <div class="table-responsive dataTables_wrapper">
                                    <table class="table table-hover" style="min-width:500px;">
                                        <thead>
                                        <tr>
                                            <th>หมายเลขคีย์การ์ด</th>
                                            <th>สถานะ</th>
                                            <th>วันที่อัพเดท</th>
                                            <th width="100px">{{ trans('messages.action') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach( $home->home_keycard as $key_card )
                                            <tr>
                                                <td>
                                                    {{ $key_card->serial_number }}
                                                </td>
                                                <td>
                                                    @if($key_card->status == "0")
                                                        <div class="label label-info">Active</div>
                                                    @else
                                                        <div class="label label-danger">Disable</div>
                                                    @endif
                                                </td>
                                                <th>{{ $key_card->updated_at }}</th>
                                                <td class="action-links" width="100px">
                                                    <a href="#" class="delete delete-keycard btn btn-danger" data-toggle="modal" data-target="#confirm-delete-keycard" data-pet-id="{{ $key_card->id }}" data-pet-type="{{ $key_card->serial_number }}">
                                                        <i class="fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('messages.Prop_unit.pet') }}</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" class="form-inline" id="add-pet-form" action="{{ url('settings/home/pet/add') }}">
                            <div class="form-group">
                                {!! Form::select('type',$pet_type,null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <input type="text" name="breed" class="form-control" placeholder="{{ trans('messages.Prop_unit.pet_degree') }}" maxlength=50 />
                            </div>

                            <div class="form-group">
                                <input type="text" name="quantity" class="form-control" placeholder="{{ trans('messages.amount') }}" maxlength=3 />
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-single" id="add-pet-btn">{{ trans('messages.add') }}</button>
                            </div>
                            <div class="form-group-separator-noline"></div>
                            @if(!$home->home_pet->isEmpty())
                            <div class="table-responsive dataTables_wrapper">
                                <table class="table table-hover" style="min-width:500px;">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('messages.type') }}</th>
                                            <th>{{ trans('messages.Prop_unit.pet_degree') }}</th>
                                            <th>{{ trans('messages.amount') }}</th>
                                            <th width="100px">{{ trans('messages.action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach( $home->home_pet as $pet )
                                        <tr>
                                            <td class="user-name">
                                                <a href="#" class="name">{{ $pet_type[$pet->type]}}</a>
                                            </td>
                                            <td>
                                                {{ $pet->breed }}
                                            </td>
                                            <td>
                                               {{ $pet->quantity }}
                                            </td>
                                            <td class="action-links" width="100px">
                                                <a href="#" class="delete delete-pet btn btn-danger" data-toggle="modal" data-target="#confirm-delete-pet" data-pet-id="{{ $pet->id }}" data-pet-type="{{ $pet_type[$pet->type]}}">
                                                    <i class="fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
           	<div class="col-sm-3 mailbox-left">
                <div class="mailbox-sidebar">
                    <ul class="list-unstyled mailbox-list no-top-margin">
                       <li>
                            <a href="{{ url('settings') }}">
                                {{ trans('messages.Settings.page_profile_head') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('settings/password') }}">
                                {{ trans('messages.Settings.change_password') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('settings/notification') }}">
                                {{ trans('messages.Settings.notification') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('settings/language') }}">
                                {{ trans('messages.Settings.language') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="confirm-delete-vehicle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Settings.confirm_delete_veh_head') }}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('messages.Settings.confirm_delete_veh_msg') }} <span></span>.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" id="confirm-delete-vehicle-btn" class="btn btn-primary">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-delete-pet">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Settings.confirm_delete_pet_head') }}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('messages.Settings.confirm_delete_pet_msg') }} <span></span>.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" id="confirm-delete-pet-btn" class="btn btn-primary">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-delete-keycard">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">ยืนยันการลบข้อมูล KeyCard</h4>
                </div>
                <div class="modal-body">
                    คุณแน่ใจว่าต้องการลบข้อมูล keycard หมายเลข : <span></span>.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" id="confirm-delete-keycard-btn" class="btn btn-primary">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="get-vehicle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Settings.veh_detail') }}</h4>
                </div>
                <div class="modal-body">
                    <div id="vehicle-content">

                    </div>
                   <span class="v-loading">{{ trans('messages.loading') }}...</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.ok') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-request-sticker">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Prop_unit.veh_sticker_request') }}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('messages.Prop_unit.veh_sticker_request_msg') }} <span></span>.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" id="confirm-request-sticker-btn" class="btn btn-primary">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/vehicle.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
<script type="text/javascript">
$(function () {
	var tmp_id;
    $('#save-settings').on('click',function () {
        $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        $(this).parents('form').submit();
    })

    $('.request-sticker').on('click',function (e) {
        e.preventDefault();
        $('#confirm-request-sticker').modal('show');
        var plate = $(this).attr('data-plate');
        $('#confirm-request-sticker .modal-body span').html(plate);
        tmp_id = $(this);
    });

    $('#confirm-request-sticker-btn').on('click',function () {
        var vid = tmp_id.data('vehicle-id');
        $.ajax({
            url : $('#root-url').val()+"/settings/home/vehicle/request-sticker",
            method : 'post',
            dataType: 'json',
            data : ({'id':vid}),
            success: function (r) {
                if(r.result) {
                    tmp_id.parents('tr').find('td.vehicle-sticker').html(r.msg);
                    tmp_id.remove();
                }
                $('#confirm-request-sticker').modal('hide');
            },
            error : function () {

            }
        })
    })

    $("#add-pet-form").validate({
        rules: {
        	type   	: {notEqual:0},
            breed   : 'required',
            quantity    : {required:true,digits:true,notEqual:0},
            lisence_plate   	: 'required',
        },
        errorPlacement: function(error, element) {}
    });

    $('#add-pet-btn').on('click',function () {
        if($("#add-pet-form").valid()) {
            $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            $("#add-pet-form").submit();
        }
    })

    $('.delete-pet').on('click',function (){
    	tmp_id = $(this).data('pet-id');
    	var type = $(this).data('pet-type');
    	$('#confirm-delete-pet .modal-body span').html(type);
    });

    $('#confirm-delete-pet-btn').on('click',function () {
        $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
    	window.location.href = $('#root-url').val()+'/settings/home/pet/delete/'+tmp_id;
    })

    $("#add-keycard-form").validate({
        rules: {
            serial_number   : 'required'
        },
        errorPlacement: function(error, element) {}
    });

    $('#add-keycard-btn').on('click',function () {
        if($("#add-keycard-form").valid()) {
            $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            $("#add-keycard-form").submit();
        }
    })

    $('.delete-keycard').on('click',function (){
        tmp_id = $(this).data('pet-id');
        var type = $(this).data('pet-type');
        $('#confirm-delete-keycard .modal-body span').html(type);
    });

    $('#confirm-delete-keycard-btn').on('click',function () {
        $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        window.location.href = $('#root-url').val()+'/settings/home/key-card/delete/'+tmp_id;
    })

    $("#add-vehicle-form").validate({
        rules: {
            type    : {notEqual:0},
            o_brand : {
                required:  function (element) {
                                var type = $("select[name='type']").val();
                                 if( $.inArray(type,[0,3,4])) return true;
                                 else return false;
                            }
            },
            s_brand : 'checkIsCM',
            model   : 'required',
            color   : 'required',
            year    : {required:true,digits:true},
            lisence_plate       : 'required',
        },
        errorPlacement: function(error, element) {}
    });

    $('#add-vehicle-btn').on('click',function () {
        if($("#add-vehicle-form").valid()) {
            $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            $("#add-vehicle-form").submit();
        }
    });

    $('.delete-vehicle').on('click',function (){
        tmp_id = $(this).data('vehicle-id');
        var plate = $(this).attr('data-plate');
        $('#confirm-delete-vehicle .modal-body span').html(plate);
    });

    $('#confirm-delete-vehicle-btn').on('click',function () {
        $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
        window.location.href = $('#root-url').val()+'/settings/home/vehicle/delete/'+tmp_id;
    });

    $('.get-vehicle').on('click',function (){
        var id = $(this).data('vehicle-id');
        $('.v-loading').show();
        $('#vehicle-content').empty();
        $.ajax({
            url : $('#root-url').val()+"/settings/home/vehicle/detail",
            method : 'post',
            dataType: 'html',
            data : ({'id':id}),
            success: function (r) {
                $('.v-loading').hide();
                $('#vehicle-content').html(r);
            },
            error : function () {

            }
        })
    })
})
</script>
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
<style>
#other_brand,#specific_brand {
    min-width: 150px;
}
</style>
@endsection
