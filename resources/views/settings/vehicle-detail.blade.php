<?php
    $vehicle_type = unserialize(constant('VEHICLE_TYPE_'.strtoupper(App::getLocale())));
?>
<div class="row">
    <div class="col-md-6">
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_plate_serial') }}</strong></div>
        <div class="right"> {{ $vehicle->lisence_plate }} </div>
        <div class="clearfix"></div>
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_brand') }}</strong></div>
        <div class="right"> {{ $vehicle->brand }} </div>
        <div class="clearfix"></div>
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_color') }}</strong></div>
        <div class="right"> {{ $vehicle->color }} </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6">
        <div class="left"><strong>{{ trans('messages.type') }}</strong></div>
        <div class="right"> {{ $vehicle_type[$vehicle->type] }} </div>
        <div class="clearfix"></div>
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_model') }}</strong></div>
        <div class="right"> {{ $vehicle->model }} </div>
        <div class="clearfix"></div>
        <div class="left"><strong>{{ trans_choice('messages.year',1) }}</strong></div>
        <div class="right"> {{ $vehicle->year }} </div>
    </div>
</div>
<?php
    $exp = false;
    if($vehicle->sticker_expire_date) {
        $exp = laterThanNow_ ($vehicle->sticker_expire_date);
    }
    if($exp) {
        $status = trans('messages.Vehicle.sticker_status_4');
    } else {
        $status = trans('messages.Vehicle.sticker_status_'.$vehicle->sticker_status);
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_sticker_status') }}</strong></div>
        <div class="right"> {{ $status }} </div>
    </div>
</div>
@if($vehicle->sticker_status == 3)
<div class="row">
    <div class="col-md-12">
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_sticker_req_dt') }}</strong></div>
        <div class="right"> @if($vehicle->sticker_request_date) {{ localDate($vehicle->sticker_request_date) }} @else - @endif  </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="left"><strong>{{ trans('messages.Prop_unit.veh_sticker_exp_dt') }}</strong></div>
        <div class="right">
            @if(!$vehicle->not_expired)
            {{ localDate($vehicle->sticker_expire_date) }}
            @else
            {{ trans('messages.Vehicle.no_expired_date') }}
            @endif
        </div>
    </div>
</div>
@endif
