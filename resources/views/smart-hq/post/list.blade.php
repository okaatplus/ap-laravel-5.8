@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Post.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
            <a href="{{ url('smart-hq-admin/admin/news-announcement/add') }}" data-toggle="modal"  class="top-action btn btn-primary">
                <i class="fa fa-newspaper-o"></i> {{ trans('messages.Post.post_create') }}
            </a>
		</div>
	</div>
    <section class="bills-env">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(array('url' => array('smart-hq-admin/admin/news-announcement/export'),'class'=>'form-horizontal','method'=>'post','id'=>'search-form','target' => "_blank")) !!}
                    <div class="row">
                        <div class="col-sm-3 block-input">
                            {!! Form::text('keyword',null,array('class'=>'form-control','size'=>25, 'placeholder'=> trans('messages.news.key_search') )) !!}
                        </div>
                        <div class="col-sm-3 block-input">
                            {!! Form::text('created_date',null,array('class'=>'form-control datepicker','size'=>25, 'placeholder'=> trans('messages.Post.created_at'),'data-language'=>App::getLocale() )) !!}
                        </div>
                        <div class="col-sm-3 block-input">
                            {!! Form::select('post_type', [
                                '-' => trans('messages.Post.post_type'),
                                'a' => trans('messages.Post.type.a'),
                                'n' => trans('messages.Post.type.n'),
                            ], null, ["class" => "form-control"]) !!}
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" name="publish_status">
                                <option value="-">{{ trans('messages.Post.publish_status') }}</option>
                                <option value="0">{{ trans('messages.Post.draft') }}</option>
                                <option value="1">{{ trans('messages.Post.publish') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <button type="reset"  class="btn btn-white btn-single reset-s-btn">{{ trans('messages.reset') }}</button>
                            <button type="button" id="submit-search" class="btn btn-secondary btn-single">{{ trans('messages.search') }}</button>
                            <button type="submit" class="btn btn-secondary btn-single">{{ trans('messages.Report.download') }}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <hr/>
                    <div id="panel-data-list">
                        @include('smart-hq.post.list-element')
                    </div>
                </div>
            </div>
    </section>

<div class="modal fade" id="modal-delete-post" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ trans('messages.Post.confirm_del_post_head') }}</h4>
            </div>
            <div class="modal-body">
                {{ trans('messages.Post.confirm_del_post_msg') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.no') }}</button>
                <button class="btn btn-primary confirm-delete" >{{ trans('messages.yes') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-send-approve-post" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ trans('messages.Post.confirm_send_approve_head') }}</h4>
            </div>
            <div class="modal-body">
                {{ trans('messages.Post.confirm_send_approve_msg') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                <button class="btn btn-primary confirm-send-approval" >{{ trans('messages.confirm') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-recall-post" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ trans('messages.Post.confirm_recall_head') }}</h4>
            </div>
            <div class="modal-body">
                {{ trans('messages.Post.confirm_recall_msg') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                <button class="btn btn-primary confirm-recall" >{{ trans('messages.confirm') }}</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
    <?php $t=time();?>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.th.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/smart-hq-announcement.js"></script>

<script type="text/javascript">
    $(function () {

        $('#submit-search').on('click',function (){
            searchPage(1);
        });

        $('#search-form').on('keydown', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                searchPage (1);
            }
        });

        $('.reset-s-btn').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            searchPage (1);
        });

		$('.panel-body').on('click','.paginate-link', function (e){
            e.preventDefault();
            searchPage($(this).attr('data-page'));
        });

        $('.panel-body').on('change','.paginate-select', function (e){
            e.preventDefault();
            searchPage($(this).val());
        });

    });

    function searchPage (page) {
        var data = $('#search-form').serialize()+'&page='+page;
        $('#panel-data-list').css('opacity','0.6');
        $.ajax({
            url     : $('#root-url').val()+"/smart-hq-admin/admin/news-announcement",
            data    : data,
            dataType: "html",
            method: 'post',
            success: function (h) {
                $('#panel-data-list').html(h);
                $('#panel-data-list').css('opacity','1');
                $('[data-toggle="tooltip"]').tooltip();
                cbr_replace();
            }
        })
    }
</script>

@endsection
