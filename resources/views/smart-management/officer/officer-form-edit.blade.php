{!! Form::model($officer,array('url'=>'#','method'=>'post','class'=>'form-horizontal', 'autocomplete' => 'off')) !!}
<div class="modal-body">
    <div class="form-group @if($errors->has('name')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.name') }}</label>
        <div class="col-sm-8">
            {!! Form::text('name',null,array('class' => 'form-control')) !!}
            <?php echo $errors->first('name','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.Prop_unit.emergency_cal') }}</label>
        <div class="col-sm-8">
            {!! Form::text('phone',null,array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">{{ trans('messages.AdminUser.username') }}</label>
        <div class="col-sm-8" style="padding-top:7px;">
            {{ $officer->email }}
        </div>
    </div>
    <div class="form-group @if($errors->has('password')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.Verify.password') }}</label>
        <div class="col-sm-8">
            <div class="input-group">
                {!! Form::password('password',array('class' => 'form-control', 'rel' => 'gp',
                'data-character-set' => 'a-z,A-Z,0-9',
                'data-input-confirm' => 'confirm-edit-admin-pass',
                'autocomplete' => 'new-password'
                )) !!}
                <div class="input-group-btn">
                    <button type="button" class="btn btn-info getNewPass" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.generate_password') }}">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </div>
            <?php echo $errors->first('password','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>
    <div class="form-group @if($errors->has('password_confirm')) validate-has-error @endif">
        <label class="col-sm-4 control-label">{{ trans('messages.Verify.confirm_password') }}</label>
        <div class="col-sm-8">
            {!! Form::password('password_confirm',array('class' => 'form-control','id' => 'confirm-edit-admin-pass')) !!}
            <?php echo $errors->first('password_confirm','<span class="validate-has-error">:message</span>'); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">ฝ่ายงาน</label>
        <div class="col-sm-8">
            {!! Form::select('role',[
                '2' => trans('messages.Officer.officer_role_2'),
                '3' => trans('messages.Officer.officer_role_3'),
                '4' => trans('messages.Officer.officer_role_4'),
                '5' => trans('messages.Officer.officer_role_5'),
                '6' => trans('messages.Officer.officer_role_6'),
                '7' => trans('messages.Officer.officer_role_7')
                ],null,array('class' => 'form-control','id' => 'select-role-edit')) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">แนวโครงการที่ดูแล</label>
        <div class="col-sm-8">
            {!! Form::select('admin_property_dimension',['0' => 'ทั้งหมด', 'v' => 'แนวสูง', 'h' => 'แนวราบ'],null,array('class'=>'form-control','id'=>'property-dimension-edit')) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label">นิติบุคคลที่ดูแล</label>
        <div class="col-sm-8">
            <div class="edit-manage-property" @if($fixed_account) style="display: none;" @endif>
            {!! Form::select('property_id_[]',$p_list,$selected_p,array('class'=>'form-control','multiple','id'=>'property-list-edit')) !!}
            </div>
            <div class="edit-user-property" @if(!$fixed_account) style="display: none;" @endif>
            {!! Form::select('user_property_id',$p_list,$officer->property_id,array('class'=>'form-control','id'=>'user-property-list-edit')) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::hidden('id') !!}
{!! Form::close() !!}
<script>
    $(function () {
        $("#property-list-edit").select2({
            allowClear: true
        });

        $('#select-role-edit').on('change', function () {
            var fixed_account_index = $.inArray( parseInt($(this).val()),[3,4,5]);
            if( fixed_account_index != -1) {
                $('.edit-manage-property').hide();
                $('.edit-user-property').show();
            } else {
                $('.edit-manage-property').show();
                $('.edit-user-property').hide();
            }
        });
        $('[data-toggle="tooltip"]').tooltip()
        $('#property-dimension-edit').on('change', function () {
            var type = $('#property-dimension-edit').val();
            $.ajax({
                type: 'GET',
                url: $('#root-url').val()+'/get/property/dimension/'+type,
                dataType: "json",
            }).then(function (r) {
                $('#property-list-edit,#user-property-list-edit').html('');
                $.each(r.data, function(i, val){
                    $('#property-list-edit,#user-property-list-edit').append("<option value='"+i+"'>"+val+"</option>");
                });
                $('#property-list-edit,#user-property-list-edit').trigger('change');
            });
        })
    })
</script>
