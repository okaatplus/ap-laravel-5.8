@extends('layouts.blank')
@section('content')
    <?php $lang = App::getLocale(); ?>
    <tr>
        <td colspan="10" border="0"></td>
    </tr>
    <tr>
        <td style="font-size: 18; font-weight: bold;" colspan="10" border="0">
            ข่าวประชาสัมพันธ์
        </td>
    </tr>
    <tr>
        <td colspan="10" border="0"></td>
    </tr>
    <tr class="head">
        <th>{{ trans('messages.Report.sequence') }}</th>
        <th>{{ trans('messages.Post.page_head') }}</th>
        <th>{{ trans('messages.Post.post_description') }}</th>
        <th>{{ trans('messages.Post.post_type') }}</th>
        <th>{{ trans('messages.Post.created_at') }}</th>
        <th>{{ trans('messages.Post.publish_status') }}</th>
        <th>{{ trans('messages.Post.video_link') }}</th>
        <th>{{ trans('messages.Post.external_link') }}</th>
        <th>{{ trans('messages.Post.post_category') }}</th>
        <th>{{ trans('messages.Post.audience') }}</th>
        <th>{{ trans('messages.Post.expired_date') }}</th>
        {{--<th>Reached</th>--}}
    </tr>

    @foreach($posts as $key => $post)
        <tr class="content">
            <td style="text-align: center;">{{ $key+1 }}</td>
            <td>{{ $post->title_th }}</td>
            <td>{{ strip_tags(htmlspecialchars_decode($post->description_th)) }}</td>
            <td>{{ trans('messages.Post.type.'.$post->post_type) }}</td>
            <td>
                {{ localDateShortNotime( $post->created_at) }}
            </td>
            <td>
                @if( $post->publish_status )
                    {{ trans('messages.Post.publish') }}
                @else
                    {{ trans('messages.Post.draft') }}
                @endif
            </td>
            <td>{{ $post->video_link }}</td>
            <td>{{ $post->external_link }}</td>
            <td>{{ trans('messages.Post.category.'.$post->post_category) }}</td>
            <td>{{ trans('messages.Post.audience_type.'.$post->audience) }}</td>
            <td>{{ $post->expired_date }}</td>
        </tr>
    @endforeach
@endsection