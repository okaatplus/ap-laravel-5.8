{!! Form::hidden('id') !!}
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.title') }} (TH)</label>
        <div class="col-sm-9">
            {!! Form::text('title_th', null, ["class" => "form-control", "maxlength" => "200"]) !!}
        </div>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.title') }} (EN)</label>
        <div class="col-sm-9">
            {!! Form::text('title_en', null, ["class" => "form-control", "maxlength" => "200"]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="profile-image">{{ trans('messages.img.insertion_img') }}</label>
        <div class="col-md-4">
            <div id="attachment-banner-crop" class="drop-property-file dz-clickable" style="margin-bottom: 0; @if($post->banner_url) display: none; @endif">
                <i class="fa fa-camera"></i>
            </div>
            <div class="preview-crop"></div>
            <div class="post-banner" id="previews-img-banner-crop">
                @if($post->banner_url != null)
                    <div class="preview-img-item" style="margin: 0;">
                        <img src="{{ env('URL_S3')."/post-file".$post->banner_url }}" width="100%" alt="post-image" />
                        <span class="remove-img-preview remove-banner">×</span>
                    </div>
                @endif
            </div>

        </div>
        <div class="col-sm-5">
            <div class="img-container">
                <img class="img-responsive" />
            </div>
        </div>
        <input type="hidden" id="img-x" name="img-x"/>
        <input type="hidden" id="img-y" name="img-y"/>
        <input type="hidden" id="img-w" name="img-w"/>
        <input type="hidden" id="img-h" name="img-h"/>
        <input type="hidden" id="img-tw" name="img-tw"/>
        <input type="hidden" id="img-th" name="img-th"/>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="profile-image">{{ trans('messages.img.thumbnail') }}</label>
        <div class="col-md-4 parent-set-thumbnail" @if($post->thumbnail_url) style="display: none;" @endif>
            <div id="attachment-thumbnail-crop" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
                <i class="fa fa-camera"></i>
            </div>
        </div>
        <div class="col-md-2">
            <div class="preview-thumbnail-crop" style="width: 100px;"></div>
            <div class="post-thumbnail" id="previews-img-thumbnail-crop">
                @if($post->thumbnail_url != null)
                    <div class="preview-img-thumbnail-item" style="margin: 0;">
                        <img src="{{ env('URL_S3')."/post-file".$post->thumbnail_url }}" width="100%" alt="post-image" />
                        <span class="remove-img-preview remove-thumbnail">×</span>
                    </div>
                @endif
            </div>

        </div>
        <div class="col-sm-5">
            <div class="thumbnail-img-container">
                <img class="thumbnail-img-responsive" />
            </div>
        </div>
        <input type="hidden" id="thumbnail-img-x" name="thumbnail-img-x"/>
        <input type="hidden" id="thumbnail-img-y" name="thumbnail-img-y"/>
        <input type="hidden" id="thumbnail-img-w" name="thumbnail-img-w"/>
        <input type="hidden" id="thumbnail-img-h" name="thumbnail-img-h"/>
        <input type="hidden" id="thumbnail-img-tw" name="thumbnail-img-tw"/>
        <input type="hidden" id="thumbnail-img-th" name="thumbnail-img-th"/>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">{{trans('messages.Post.post_description')}} (TH)</label>
        <div class="col-sm-9">
            {!! Form::textarea('description_th',null,array('class'=>'form-control','id'=>'description','placeholder'=> trans('messages.news.description'), 'rows'=>5)) !!}
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{trans('messages.Post.post_description')}} (EN)</label>
        <div class="col-sm-9">
            {!! Form::textarea('description_en',null,array('class'=>'form-control','id'=>'description_en','placeholder'=> trans('messages.news.description_en'), 'rows'=>5)) !!}
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.attach') }}</label>
        <div class="col-sm-9">
            <ul class="list-unstyled list-inline form-action-buttons" style="margin-top: 6px;">
                <li>
                    <button type="button" id="attachment" class="btn btn-unstyled upload">
                        <i class="fa fa-camera"></i> {{ trans('messages.attach') }}
                    </button>
                </li>
                <li>
                    <div class="field-hint">{{ trans('messages.upload_file_description') }}</div>
                </li>
            </ul>
            <div id="previews">
                @if($post->postFile && $post->postFile->count() > 0)
                    @foreach($post->postFile as $file)
                        <div class="preview-img-item">
                            @if($file->is_image)
                                <img width="80px" src="{{env('URL_S3')}}/post-file/{{$file->url.$file->name}}" />
                            @else
                                <img width="80px" src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                                <span class="file-label">{{ $file->original_name }}</span>
                            @endif
                            <span class="remove-img-preview remove-element" data-e-type="post-file" data-e-id="{{ $file->id }}">×</span>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>



    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.Post.post_category') }}</label>
        <div class="col-sm-9">
            {!! Form::select('post_category', [
                0 => trans('messages.Post.category.0'),
                1 => trans('messages.Post.category.1'),
                2 => trans('messages.Post.category.2'),
                3 => trans('messages.Post.category.3'),
                99 => trans('messages.Post.category.99'),
            ], null, ["class" => "form-control"]) !!}
        </div>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">
            {{ trans('messages.Post.p_participation') }}
        </label>
        <div class="col-sm-9">
            {!! Form::select('property_applying',[
                'A' => trans('messages.Post.participation.A'),
                'S' => trans('messages.Post.participation.S'),
                'AE' => trans('messages.Post.participation.AE')
            ],null,array('class'=>'form-control','id' => 'i-pm-p-par-status')) !!}
        </div>
    </div>

    <div class="row form-group" id="p-par-block" @if( !$post->property_applying || $post->property_applying == 'A') style="display: none;" @endif>
        <label class="col-sm-3 control-label" id="p-par-s-label" @if( $post->property_applying == 'S' ) style="display: none;" @endif>
            {{ trans('messages.Post.included_p') }}
        </label>
        <label class="col-sm-3 control-label" id="p-par-ae-label"  @if( $post->property_applying == 'AE' ) style="display: none;" @endif>
            {{ trans('messages.Post.excluded_p') }}
        </label>
        <div class="col-sm-9 p-auto-search">
<!--                --><?php //dump($p_list);?>
            {!! Form::select('property_id[]',$p_list,$selected_p,array('class'=>'form-control','multiple','id'=>'property-list')) !!}
        </div>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.Post.audience') }}</label>
        <div class="col-sm-9">
            {!! Form::select('audience', [
                0 => trans('messages.Post.audience_type.0'),
                1 => trans('messages.Post.audience_type.1'),
                2 => trans('messages.Post.audience_type.2'),
                3 => trans('messages.Post.audience_type.3')
            ], null, ["class" => "form-control"]) !!}
        </div>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.Post.important') }}</label>
        <div class="col-sm-9">
            <label>
                {!! Form::checkbox('flag_important',1, null, ["class" => ""]) !!}
                {{ trans('messages.Post.important_dscpt') }}
            </label>
        </div>
    </div>
    @if( $post->publish_state != 2 )
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.Post.notification') }}</label>
        <div class="col-sm-9">
            <label>
                {!! Form::checkbox('push_notification',1, null, ["class" => ""]) !!}
                {{ trans('messages.Post.notification_dscpt') }}
            </label>
        </div>
    </div>
    @endif
    @include('post.post-footer')
    <div class="delete-zone"></div>
    <input type="hidden" name="remove-banner-flag" id="remove-banner-flag">
    <input type="hidden" name="remove-thumbnail-flag" id="remove-thumbnail-flag">
    <input type="hidden" name="approve-flag" id="approve-flag">

    <div class="modal fade" id="modal-approve-post" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.confirm_approve_head') }}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('messages.Post.confirm_approve_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button class="btn btn-primary confirm-approve" >{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

@section('script')
    <script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/app-scripts/upload-file.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/select2/select2.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/crop-img.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/app-scripts/crop-thumbnail-img.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="{{url('/')}}/collagePlus/jquery.collageCaption.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>


    <script type="text/javascript">
        $(function () {

            $("#property-list").select2({
                allowClear: true
            }).on('select2-open', function()
            {
                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
            });

            tinymce.init({
                selector:"textarea",
                height: 300,
                theme: 'modern',
                content_css : ['{{ url('css/custom.css') }}','{{ url('font/ap/font-wys.css') }}'],
                plugins: [
                    'advlist autolink lists link charmap print preview hr',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'table contextmenu directionality',
                    'template paste textcolor colorpicker textpattern'
                ],
                toolbar: 'insertfile undo redo | styleselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link |',
            });
            $("form").validate({
                rules: {
                    title_th: 'required',
                    //title_en: 'required',
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('error');
                    $( window ).resize();
                },
                errorPlacement: function(error, element) {}
            });

            $('#save-draft-form').on('click', function () {
                var _valid = checkDetail();
                if( $("form").valid() && _valid) {
                    $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                    $('form').submit();
                } else {
                    var top_;
                    top_ = $('.error').first().offset().top;
                    $('html,body').animate({scrollTop: top_-100}, 1000);
                }
            });

            $('#approve-form').on('click', function () {
                var _valid = checkDetail();
                if( $("form").valid() && _valid) {
                    //$('form').submit();
                    $('#modal-approve-post').modal('show');
                } else {
                    var top_;
                    top_ = $('.error').first().offset().top;
                    $('html,body').animate({scrollTop: top_-100}, 1000);
                }
            }); //approve-flag

            $('.confirm-approve').on('click', function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#approve-flag').val(1);
                $('form').submit();
            })


            $('#i-pm-p-par-status').on('change', function () {
                var c = $(this).val();
                if(c == 'A') {
                    $('#p-par-block').hide();
                } else {
                    $('#p-par-block').show();
                    if(c == 'S') {
                        $('#p-par-s-label').show();
                        $('#p-par-ae-label').hide();
                    } else {
                        $('#p-par-ae-label').show();
                        $('#p-par-s-label').hide();
                    }
                }
                $(window).resize();
            });
        })

        function checkDetail () {
            var valid = true;

            if( tinyMCE.get('description').getContent() == "" ) {
                $('#description').prev().addClass('error mce-error');
                valid = false;
            }

            //if ( tinyMCE.get('description_en').getContent() == "") {
                //$('#description_en').prev().addClass('error mce-error');
                //valid = false;
            //}
            return valid;
        }
    </script>
    <link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
    <link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="{{url('/')}}/js/cropper/cropper.min.css">
@endsection