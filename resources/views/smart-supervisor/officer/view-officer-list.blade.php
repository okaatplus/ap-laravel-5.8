@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">ผู้ดูแลระบบ</h1>
		</div>
		<div class="breadcrumb-env">
            <a href="#" data-toggle="modal" data-target="#add-officer-modal" class="top-action create-invoice-btn btn btn-primary">+ เพิ่มผู้ดูแลระบบ </a>
		</div>
	</div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true" style="margin-top: 0">×</span>
            <span class="sr-only">Close</span>
        </button>
        {{ Session::get('message') }}
    </div>
    @endif

    <?php 
    $role = [
        '-' => trans('messages.Officer.position'),
        3   => trans('messages.Officer.officer_role_3'),
        4   => trans('messages.Officer.officer_role_4'),
        5   => trans('messages.Officer.officer_role_5'),
        6   => trans('messages.Officer.officer_role_6'),
        7   => trans('messages.Officer.officer_role_7'),
    ]
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST" id="search-form" action="#" accept-charset="UTF-8" class="search-form form-horizontal">
                <div class="row">
                    <div class="col-sm-3 block-input">
                        <input class="form-control" size="25" placeholder="{{ trans('messages.name') }}" name="name">
                    </div>
                    <div class="col-sm-3">
                        {!! Form::select('role', $role, null,['class'=>'form-control']) !!}
                    </div>
                    <div class="col-sm-3">
                        <?php
                        $__p = ["" => trans('messages.property') ] + $p_list;
                        ?>
                        {!! Form::select('property', $__p, null,['class'=>'form-control select2','id' => 'p-id-search']) !!}
                    </div>
                    <div class="col-sm-3 text-right">
                        {!! Form::select('active', ['-' => trans('messages.Member.account_status'), 't' => trans('messages.Member.account_active'), 'f' => trans('messages.Member.account_inactive')], null,['class'=>'form-control']) !!}
                    </div>
                    <div class="col-sm-12 text-right">
                        <button type="reset" class="btn btn-white reset-s-btn">{{ trans('messages.reset') }}</button>
                        <button type="button" class="btn btn-secondary p-search-officer">{{ trans('messages.search') }}</button>
                    </div>
                </div>
            </form>
            <hr/>
            <div id="member-list-content" class="member-list-content">
                @include('smart-supervisor.officer.officer-list')
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-active" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.confirm_ban_head') }}</h4>
                </div>
                <div class="modal-body">
                	{!! trans('messages.Member.confirm_ban_msg') !!}
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-inactive" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Member.confirm_active_head') }}</h4>
                </div>
                <div class="modal-body">
                	{!! trans('messages.Member.confirm_active_msg') !!}
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="submit" class="btn btn-primary change-active-status-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-member">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">ข้อมูลผู้ดูแลระบบ</h4>
                </div>
                <div class="modal-body">

                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.close') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-officer-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">เพิ่มผู้ดูแลระบบ</h4>
                </div>
				<div id="form-add-officer">
					<div class="form">
						@include('smart-supervisor.officer.officer-form')
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
						<button type="button" class="btn btn-primary save-officer">{{ trans('messages.save') }}</button>
					</div>
				</div>
            </div>
        </div>
    </div>

	<div class="modal fade" id="edit-officer-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">แก้ไขผู้ดูแลระบบ</h4>
                </div>
				<div id="form-edit-officer">
					<div class="form">
					</div>
					<div class="modal-footer">
					    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
					    <button type="button" class="btn btn-primary save-officer">{{ trans('messages.save') }}</button>
					</div>
				</div>
            </div>
        </div>
    </div>

	<div class="modal fade" id="modal-delete" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{ trans('messages.Officer.confirm_delete_head') }}</h4>
				</div>
				<div class="modal-body">
					{!! trans('messages.Officer.confirm_delete_msg') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
					<button type="submit" class="btn btn-primary" id="delete-member-btn">{{ trans('messages.confirm') }}</button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="password-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center" id="add-member-form-block">
                    {{ trans('messages.AdminUser.generate_password_msg_1') }}
                    <div class="display-password-block">
                        <span id="generated-password"></span>
                        <i class="copy-clipboard fa-copy" data-toggle="tooltip" data-placement="top" data-original-title="{{ trans('messages.AdminUser.copy_password') }}"></i>
                    </div>
                    {{ trans('messages.AdminUser.generate_password_msg_2') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('messages.ok') }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/toastr/toastr.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/random-password.js"></script>
<script type="text/javascript">
	$(function (){
        $('.panel-body').on('click','.p-paginate-link', function (e){
            e.preventDefault();
            officerPage($(this).attr('data-page'));
        })

        $('.panel-body').on('change','.p-paginate-select', function (e){
            e.preventDefault();
            officerPage($(this).val());
        });

        $('.p-search-officer').on('click', function (e){
            e.preventDefault();
            officerPage(1);
        });

        $('.reset-s-btn').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            $('#p-id-search').val('').trigger('change');
            officerPage (1);
        });

	    $('body').on('change', '#select-role-add', function () {
	        if( $(this).val() == 2) $('#add-marketing-role').show();
	        else $('#add-marketing-role').hide();
		});

        $('body').on('change', '#select-role-edit', function () {
            if( $(this).val() == 2) $('#edit-marketing-role').show();
            else $('#edit-marketing-role').hide();
        });

        $('#form-add-officer .save-officer').on('click',function () {
			var btn = $(this);
            btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
			$.ajax({
				url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/add",
                method	: "POST",
                data 	: $('#form-add-officer form').serialize(),
                dataType: "html",
                success: function (t) {
                	if(t == 'saved') {
						location.reload(true);
					} else {
						 $('#form-add-officer .form').html(t);
						 btn.removeAttr('disabled').find('i').remove();
					}
                }
			});
        });

		$('#form-edit-officer .save-officer').on('click',function () {
			var btn = $(this);
            btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
			$.ajax({
				url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/edit",
                method	: "POST",
                data 	: $('#form-edit-officer form').serialize(),
                dataType: "html",
                success: function (t) {
                	if(t == 'saved') {
						 location.reload(true);
					} else {
						 $('#form-edit-officer .form').html(t);
					}
					 btn.removeAttr('disabled').find('i').remove();
                }
			});
        });

		var tmp;

		$('.member-list-content').on('click','.active-status', function (e){
			e.preventDefault();
			tmp = $(this);

			var name = tmp.parents('tr').find('td.user-image .name').html();
			if(tmp.attr('data-status') == 1) {
				$('#modal-inactive .modal-body span').html(name);
				$('#modal-inactive').modal('show');
			} else {
				$('#modal-active .modal-body span').html(name);
				$('#modal-active').modal('show');
			}
		});

		$('.change-active-status-btn').on('click', function () {
			$(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
			changeActiveStatus (tmp.attr('data-uid'),parseInt(tmp.attr('data-status')));
		});

		$('.member-list-content').on('click','.view-member', function (e){
			e.preventDefault();
			var _this = $(this);
			_this.html('<i class="fa-spin fa-spinner"></i>');
			var uid = _this.attr('data-uid');
			$.ajax({
				url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/view",
                method	: "POST",
                data 	: ({uid:uid}),
                dataType: "html",
                success: function (t) {
                	$('#modal-member .modal-body').html(t);
                	$('#modal-member').modal('show');
                	_this.html('<i class="fa-eye"></i>');
                }
			});
		})

		$('.member-list-content').on('click','.edit-member', function (e){
			e.preventDefault();
			var _this = $(this);
			_this.html('<i class="fa-spin fa-spinner"></i>').attr('disabled','disabled');
			var uid = _this.attr('data-uid');
			$.ajax({
				url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/edit/get",
                method	: "POST",
                data 	: ({uid:uid}),
                dataType: "html",
                success: function (t) {
					_this.html('<i class="fa-edit"></i>').removeAttr('disabled','disabled');;
                	$('#form-edit-officer .form').html(t);
					$('#edit-officer-modal').modal('show');
                }
			});
		});

		function changeActiveStatus (uid,flag) {
			$.ajax({
				url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/active",
                method	: "POST",
                data 	: ({uid:uid,status:flag}),
                dataType: "json",
                success: function (r) {
                	if(r.result) {
                		location.reload();
                	}
                }
			});
		}

        $('.panel-body').on('click','.member-pagination a', function (e){
            e.preventDefault();
            adminPage($(this).attr('data-page'));
        });

        function adminPage (page) {
            var data = 'page='+page;
            $('#member-list').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/list",
                data    : data,
                dataType: "html",
                method: 'post',
                success: function (h) {
                    $('#member-list').css('opacity','1').html(h);
                    $('[data-toggle="tooltip"],[rel="tooltip"]').tooltip();
                }
            })
        }

        $('#member-list').on('click','.delete-member',function (e) {
            e.preventDefault();
            tmp = $(this).attr('data-uid');
            $('#modal-delete').modal('show');
        })

        $('#delete-member-btn').on('click',function () {
            var btn = $(this);
            btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
            $.ajax({
                url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/delete",
                method	: "POST",
                data 	: ({id:tmp}),
                dataType: "json",
                success: function (t) {
                    if(t.result) {
                        adminPage (1);
                    } else {

                    }
                    btn.removeAttr('disabled').find('i').remove();
                    $('#modal-delete').modal('hide');
                }
            });
        });

        function officerPage (page) {
            var data = $('#search-form').serialize()+'&page='+page;
            $('#member-list-content').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/smart-supervisor/admin/admin-system/list",
                data    : data,
                dataType: "html",
                method: 'post',
                success: function (h) {
                    $('#member-list-content').css('opacity','1').html(h);
                }
            })
        }

        $('.copy-clipboard').on('click', function () {
            copyToClipboard('#generated-password');
            toastr.info("{{ trans('messages.AdminUser.password_was_copied') }}",null,{"closeButton": true});
        })
	});
</script>

<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
@endsection
