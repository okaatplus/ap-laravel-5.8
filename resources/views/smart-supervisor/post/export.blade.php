@extends('layouts.blank')
@section('content')
    <?php $lang = App::getLocale(); ?>
    <tr>
        <td colspan="6" border="0"></td>
    </tr>
    <tr>
        <td style="font-size: 18; font-weight: bold;" colspan="6" border="0">
            รายการข่าวสารในโครงการ
        </td>
    </tr>
    <tr>
        <td colspan="6">{{ trans('messages.property_name') }} : {{ $property->{'property_name_'.$lang} }}</td>
    </tr>
    <tr>
        <td colspan="6" border="0"></td>
    </tr>
    <tr class="head">
        <th>{{ trans('messages.Report.sequence') }}</th>
        <th>{{ trans('messages.Post.page_head') }}</th>
        <th>{{ trans('messages.Post.post_description') }}</th>
        <th>{{ trans('messages.Post.created_at') }}</th>
        <th>{{ trans('messages.Post.publish_status') }}</th>
        {{--<th>Reached</th>--}}
    </tr>

    @foreach($posts as $key => $post)
        <tr class="content">
            <td style="text-align: center;">{{ $key+1 }}</td>
            <td>{{ $post->title_th }}</td>
            <td>{{ strip_tags(htmlspecialchars_decode($post->description_th)) }}</td>
            <td>
                {{ localDateShortNotime( $post->created_at) }}
            </td>
            <td>
                @if( $post->publish_status )
                    {{ trans('messages.Post.publish') }}
                @else
                    {{ trans('messages.Post.draft') }}
                @endif
            </td>
            <?php /*<td>
                <?php $track = $post->trackingCount->toArray(); ?>
                @if( count($track) )
                    {{ $track[0]['count'] }}
                @else
                0
                @endif
            </td> */ ?>
        </tr>
    @endforeach
@endsection