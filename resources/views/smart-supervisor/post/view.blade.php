@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Post.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Post.page_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <section class="bills-env">

        <ul class="nav nav-tabs">
            <li class="active"> <a href="#th" data-toggle="tab">ภาษาไทย</a> </li>
            <li> <a href="#en" data-toggle="tab">English</a> </li>
        </ul>

        <div class="panel panel-default">
            <div class="panel-body">


                <div class="row">
                    <div class="col-sm-4">
                        @if( !$post->banner_url )

                            <div class="default-banner">
                                Announcement
                            </div>
                        @else
                            <img src="{{ env('URL_S3')."/post-file".$post->banner_url }}"  width="100%">
                        @endif
                    </div>
                    <div class="col-sm-8 tab-content">

                        <div class="tab-pane active" id="th">
                            <h2 class="no-top-margin">{{ $post->title_th }}</h2>
                            <div style="margin-top: 10px;" >{!!  $post->description_th  !!}</div>
                        </div>

                        <div class="tab-pane" id="en">
                            <h2 class="no-top-margin">{{ $post->title_en }}</h2>
                            <div style="margin-top: 10px;" >{!!  $post->description_en  !!}</div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-3"><b>{{ trans('messages.Post.publish_status') }} :</b></div>
                            <div class="col-sm-9">
                                @if( $post->publish_status )
                                    {{ trans('messages.Post.publish') }}
                                @else
                                    {{ trans('messages.Post.draft') }}
                                @endif
                            </div>
                        </div>

                        @if($post->postFile->count() > 0)
                            <div class="row" style="margin-top: 10px;">
                                <hr/>
                                <div class="col-sm-3">
                                    <b>{{trans('messages.attachment')}}</b>
                                </div>
                                <div class="col-sm-9">
                                    <ul class="list-unstyled list-inline">
                                        @foreach($post->postFile as $file)
                                            <li>
                                                @if($file->is_image)
                                                    <a href="{{ env('URL_S3')."/post-file/".$file->url.$file->name }}" class="thumb gallery" rel="gal-1">
                                                        <img width="145px" src="{{ env('URL_S3')."/post-file/".$file->url.$file->name }}" class="img-thumbnail" />
                                                    </a>
                                                @else
                                                    <div>
                                                        <a target="_blank" href="{{ url("/smart-supervisor/admin/news-announcement/get-attachment/".$file->id) }}" class="thumb">
                                                            <img src="{{url('/')}}/images/file.png" class="img-thumbnail" />
                                                        </a>
                                                        <div class="file-name">{{ $file->original_name }}</div>
                                                    </div>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @include('post.post-footer')
                    </div>
                </div>
            </div>
            <div class="panel-footer" style="margin-top: 35px; background: #fff; padding-top: 29px;">
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <a class="btn btn-gray" href="{{url('smart-supervisor/admin/news-announcement')}}">{{trans('messages.back')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script>
        $(function () {
            $(".gallery").fancybox({
                helpers: {
                    overlay: {
                        locked: false
                    }
                },
                beforeLoad: function(){
                    $('html').css('overflow','hidden');
                },
                afterClose: function(){
                    $('html').css('overflow','auto');
                },
                'padding'           : 0,
                'transitionIn'      : 'elastic',
                'transitionOut'     : 'elastic',
                'changeFade'        : 0
            });
        });
    </script>
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{url('/')}}/collagePlus/effect.css" type="text/css" media="screen" />

    <style>
        ul, ol {
            margin-top: inherit;
        }
    </style>
@endsection
