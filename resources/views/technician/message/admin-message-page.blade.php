
@if( isset($messageTexts) )
    @if( $messageTexts->hasMorePages() )
    <div id="load-old" data-page="{{ $messageTexts->currentPage() +1 }}">  {{ trans('messages.Message.load_message') }} </div>
    @endif
    @foreach($messageTexts->reverse() as $message)
        <div class="row">
            <div class="col-md-12">
                <div class="message @if($message->is_admin_reply) admin-reply @else user-reply @endif">
                    <p>{!! nl2br(e($message->text)) !!}</p>
                    @if( $message->messageTextFile )
                        <div>
                        @foreach( $message->messageTextFile as $file )
                        <a class="fancybox" rel="gal-{{$message->message_id}}" href="{{ env('URL_S3') }}/messages-file/{{$file->url.$file->name}}">
                            <img src="{{ env('URL_S3') }}/messages-file/{{ $file->url.$file->name }}" alt="album-image" />
                        </a>
                        @endforeach
                        </div>
                    @endif
                    <time>{{ chatTime($message->created_at)  }}</time>
                </div>
            </div>
        </div>
    @endforeach
@endif
