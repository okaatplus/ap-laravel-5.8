@if($latest_message->count() > 0)
<?php
   $from   = (($latest_message->currentPage()-1)*$latest_message->perPage())+1;
   $to     = (($latest_message->currentPage()-1)*$latest_message->perPage())+$latest_message->perPage();
   $to     = ($to > $latest_message->total()) ? $latest_message->total() : $to;
   $allpage = $latest_message->lastPage();
?>
<div class="row">
    <div class="col-md-6">
       <div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
           {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$latest_message->total()]) !!}<br/><br/>
       </div>
    </div>
    @if($allpage > 1)
        <div class="col-md-6 text-right">
            @if($latest_message->currentPage() > 1)
                <a class="btn btn-white paginate-link" href="#" data-page="{{ $latest_message->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
            @endif
            @if($latest_message->lastPage() > 1)
                <?php echo Form::selectRange('page', 1, $latest_message->lastPage(),$latest_message->currentPage(),['class'=>'form-control paginate-select']); ?>
            @endif
            @if($latest_message->hasMorePages())
                <a class="btn btn-white paginate-link" href="#" data-page="{{ $latest_message->currentPage()+1 }}">{{ trans('messages.next') }}</a>
            @endif
        </div>
    @endif
</div>
@foreach ($latest_message as $message)
<?php $last_message = $message->hasText()->orderBy('updated_at','desc')->first(); ?>
<div class="story-row">
    <a href="{{ url('technician/admin/messages/view/'.$message->id) }}">
        @if($message->owner->profile_pic_name)
        <img src="{{ env('URL_S3')."/profile-img/".$message->owner->profile_pic_path.$message->owner->profile_pic_name }}" alt="user-image" class="img-circle s-profile-img"/>
        @else
        <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle s-profile-img" />
        @endif
        <div class="row">
            <div class="col-sm-4">
                <div class="t-name">
                    <?php
                    $unit_number_arr = [];
                    foreach ($message->owner->ActiveUnit as $au) {
                        $unit_number_arr[] = $au->unit->unit_number;
                    }
                    ?>
                    <b><span class="name">{{ $message->owner->name }}</span></b><br>
                    <span style="font-size: 12px;">{{ trans('messages.unit_no') }} : {{ implode(',',$unit_number_arr) }}</span>
                    @if($message->flag_new_from_user)
                    <span class="m-active"></span>
                    @endif
                    <div style="font-size: 12px;">{{ chatTime($last_message->created_at) }}</div>
                </div>
            </div>
            <div class="col-sm-8 message-latest">
                @if($last_message->is_admin_reply)
                <i class="fa-mail-forward"></i>
                @else
                <i class="fa-mail-reply"></i>
                @endif
                {{ $last_message->text }}
            </div>
        </div>
    </a>
</div>
@endforeach
<div class="clearfix"></div>
<div class="row" style="margin-top: 15px;">
	<div class="col-md-6">
		<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
			{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$latest_message->total()]) !!}
		</div>
	</div>
	@if($allpage > 1)
	<div class="col-md-6 text-right">
        @if($latest_message->currentPage() > 1)
            <a class="btn btn-white paginate-link" href="#" data-page="{{ $latest_message->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
        @endif
        @if($latest_message->lastPage() > 1)
        <?php echo Form::selectRange('page', 1, $latest_message->lastPage(),$latest_message->currentPage(),['class'=>'form-control paginate-select']); ?>
        @endif
        @if($latest_message->hasMorePages())
            <a class="btn btn-white paginate-link" href="#" data-page="{{ $latest_message->currentPage()+1 }}">{{ trans('messages.next') }}</a>
        @endif
	</div>
	@endif
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Message.no_message') }}</div><div class="clearfix"></div>
@endif
