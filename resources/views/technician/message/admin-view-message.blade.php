@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Message.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Message.page_head') }}</strong>
                </li>
            </ol>
      </div>
    </div>
    <section class="mailbox-env">
        <div class="row">
            <div class="col-md-8 mailbox-left">
                <div class="panel panel-default">
                    @include('technician.message.message-block')
                </div>
            </div>
            <div class="col-md-4 mailbox-right">
                @include('message.user-profile')
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{url('/')}}/js/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/app-scripts/chat.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/autosize.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script>
    $(function () {
        autosize($('#txt-msg'));
        $('#send-message').on('click',function () {
            var btn = $(this);
            if( $("#input-message").val() !== "" || $('.preview-img-item').length ) {
                btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : $('#root-url').val()+"/technician/admin/message/send",
                    data    : $('.message-form').serialize(),//({ 'mid':$('#hidden-mid').val(),'text':$("#input-message").val() }),
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        btn.removeAttr('disabled').find('.fa-spinner').remove();
                        $("#input-message").val('');
                        getMessagePage(1,true);
                    }
                });
            }
        })
    });

    function getMessagePage (page,loadnew) {
        if(!loadnew)
            $('#load-old').prepend('<i class="fa-spin fa-spinner"></i> ');
        $.ajax({
            url     : $('#root-url').val()+"/technician/admin/messages/page",
            data    : ({ 'mid':$('#hidden-mid').val(), 'page':page }),
            method  : 'post',
            dataType: 'html',
            success: function(h){
                if(loadnew)
                    $('#landing-text').html(h);
                else {
                    $('#landing-text').find('#load-old').remove();
                    $('#landing-text').prepend(h);
                }
                clearMessageBox();
                activateFancy ();
            }
        });
    }
</script>
    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
