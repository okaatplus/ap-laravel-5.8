<?php
$unit_number = $user->property_unit->unit_number;
?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="py-3 mr-3 float-left">
            <div class="avatar">
                <img src="http://localhost/ap-laravel-5.8/public/images/user-4.png" alt="user-image" class="img-avatar">
                <span class="avatar-status badge-success"></span>
            </div>
        </div>
        <div class="text-truncate font-weight-bold">
            <b style="font-size: 18px;">{{ $user->name }}</b>
        </div>
        <div class="small text-muted text-truncate">
            {{ trans('messages.unit_no') }} : {{ $unit_number }}
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-3">
                <b>{{ trans('messages.Member.member_account_status') }} :</b>
            </div>
            <div class="col-xs-9">
                @if($user->user_type == 1)
                    {{ trans('messages.Member.member_owner') }}
                @elseif($user->user_type == 2)
                    {{ trans('messages.Member.member_crew') }}
                @else
                    {{ trans('messages.Member.member_tenant') }}
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <b>{{ trans('messages.email') }} :</b>
            </div>
            <div class="col-xs-9">
                {{ $user->email }}
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <b>{{ trans('messages.tel') }} :</b>
            </div>
            <div class="col-xs-9">
                {{ $user->phone }}
            </div>
        </div>
    </div>
</div>