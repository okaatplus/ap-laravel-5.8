@if($post_parcel)
<?php
    $parcel_type   = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
    $parcel_size   = unserialize(constant('POST_PARCEL_SIZE_'.strtoupper(App::getLocale())));
?>
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.receive_code') }}</b></div>
        <div class="col-sm-7">{{ receivedNumber($post_parcel->receive_code) }}</div>
    </div>
<div class="form-group">
    <div class="col-sm-5"><b>{{ trans('messages.PostParcel.ref_code') }}</b></div>
    <div class="col-sm-7">{{ receivedNumber($post_parcel->ref_code) }}</div>
</div>
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.date_received') }}</b></div>
        <div class="col-sm-7">{{ localDate($post_parcel->date_received) }}</div>
    </div>

    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.unit_no') }}</b></div>
        <div class="col-sm-7">{{ $post_parcel->forUnit->unit_number }}</div>
    </div>

    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.from_name') }}</b></div>
        <div class="col-sm-7">{{ $post_parcel->from_name }}</div>
    </div>

    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.to_name') }}</b></div>
        <div class="col-sm-7">
            @if( $post_parcel->to_resident_id )
                @if( $post_parcel->to_resident_type =='c' )
                    {{ $post_parcel->toCustomer->prefix_name." ".$post_parcel->toCustomer->name }}
                @else
                    {{ $post_parcel->toTenant->prefix_name." ".$post_parcel->toTenant->name }}
                @endif
            @else
                {{ $post_parcel->to_name }}
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.ems_code') }}</b></div>
        <div class="col-sm-7">{{ $post_parcel->ems_code }}</div>
    </div>
    @if($post_parcel->type)
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.type') }}</b></div>
        <div class="col-sm-7">{{ $parcel_type[$post_parcel->type] }}</div>
    </div>
    @endif
    @if($post_parcel->size)
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.size') }}</b></div>
        <div class="col-sm-7">{{ $parcel_size[$post_parcel->size] }}</div>
    </div>
    @endif
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.status') }}</b></div>
        <div class="col-sm-7">
            @if($post_parcel->status == 0)
                {{ trans('messages.PostParcel.status_0') }}
            @elseif($post_parcel->status == 1)
                {{ trans('messages.PostParcel.status_1') }}
            @else
                {{ trans('messages.PostParcel.status_2') }}
            @endif
        </div>
    </div>
    @if($post_parcel->status == 1)
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.receiver_name') }}</b></div>
        <div class="col-sm-7">{{ $post_parcel->receiver_name }}</div>
    </div>

    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.receive_date') }}</b></div>
        <div class="col-sm-7">{{ localDate($post_parcel->updated_at) }}</div>
    </div>
    @endif
    <div class="form-group">
        <div class="col-sm-5"><b>{{ trans('messages.PostParcel.note') }}</b></div>
        <div class="col-sm-7">{{ $post_parcel->note != null ? $post_parcel->note : "-" }}</div>
    </div>

    @if( $post_parcel->created_by )
    <hr/>
    <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.created_by') }}</b> {{ $post_parcel->createdBy->name }}</li>
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.created_at') }}</b> {{ showDateTime($post_parcel->created_at) }}</li>
    </ul>
    @endif
    @if( $post_parcel->delivered_by )
    <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.delivered_by') }}</b> {{ $post_parcel->deliveredBy->name }}</li>
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.delivered_at') }}</b> {{ showDateTime($post_parcel->delivered_date) }}</li>
    </ul>
    @endif
    @if( $post_parcel->canceled_by && $post_parcel->status == 2 )
    <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.canceled_by') }}</b> {{ $post_parcel->canceledBy->name }}</li>
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.canceled_at') }}</b> {{ showDateTime($post_parcel->canceled_at) }}</li>
        <li class="field-hint" ><b>{{ trans('messages.PostParcel.canceled_reason') }}</b> {{ $post_parcel->cancel_reason }}</li>
    </ul>
    @endif
@else
{{ trans('messages.PostParcel.not_found') }}
@endif
