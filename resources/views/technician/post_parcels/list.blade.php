@extends('layouts.base-admin')
@section('content')
<?php
    $parcel_type    = unserialize(constant('POST_PARCEL_TYPE_'.strtoupper(App::getLocale())));
    $parcel_size    = array('' => trans('messages.PostParcel.size'));
    $parcel_size   += unserialize(constant('POST_PARCEL_SIZE_'.strtoupper(App::getLocale())));
?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.PostParcel.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
		</div>
	</div>
    <ul class="nav nav-tabs">
        <li @if($tab == "uncollected") class="active" @endif>
            <a href="{{ url('admin/parcel-tracking?tab=uncollected') }}">
                <span>Uncollected</span>
            </a>
        </li>
    </ul>
    <section>
        <div class="panel panel-default">
             <div class="panel-body">
                 {!! Form::open(array('url' => array('admin/parcel-tracking'),'class'=>'form-horizontal','method'=>'post','id'=>'pp-search')) !!}
                 <div class="row">
                     <div class="col-sm-3 block-input">
                         <div class="input-group">
                             <span class="input-group-addon">#</span>
                             {!! Form::text('ref_code',null,array('class'=>'form-control','placeholder'=>trans('messages.PostParcel.ref_code'),'style'=>'position:static;')) !!}
                         </div>
                     </div>
                     <div class="col-sm-3 block-input">
                         {!! Form::text('date_received',null,array('class'=>'form-control datepicker','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','placeholder'=>trans('messages.PostParcel.date_received'),'data-language'=>App::getLocale())) !!}
                     </div>
                     <div class="col-sm-3 block-input">
                         {!! Form::select('property_unit_id',$unit_list,null,array('class'=>'form-control select2', 'id' => 'unit-id')) !!}
                     </div>
                     <div class="col-sm-3 block-input">
                         {!! Form::select('type',$parcel_type,null,array('class'=>'form-control')) !!}
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-3 block-input">
                         <input type="text" name="ems_code" class="form-control" placeholder="{{ trans('messages.PostParcel.ems_code') }}" maxlength=100 />
                     </div>
                     <div class="col-sm-9 text-right">
                         <button type="reset"  class="btn btn-white btn-single reset-s-btn">{{ trans('messages.reset') }}</button>
                         <button type="button" id="submit-search" class="btn btn-secondary btn-single">{{ trans('messages.search') }}</button>
                     </div>
                 </div>
                 {!! Form::hidden('tab',$tab) !!}
                 {!! Form::close() !!}
                 <hr/>
                 <div id="panel-post-parcel-list">
               @include('technician.post_parcels.list-element')
                 </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="add-post-parcel-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('url' => array('admin/parcel-tracking/add'),'class'=>'form-horizontal','id'=>'create-post-parcel-form','method'=>'post')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.PostParcel.add_post_parcel_label') }}</h4>
                </div>
                <div class="modal-body">
                    <div style="padding:0px 15px;">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.unit_no') }}</label>
                                <div class="col-sm-9">
                                    {!! Form::select('property_unit_id',$unit_list,null,array('class'=>'form-control select2','id' => 'add-pp-unit-select')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row" id="to-resident-block" style="display: none">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.PostParcel.to_name') }}</label>
                                <div class="col-sm-9">
                                    {!! Form::select('to_resident',[],null,array('class'=>'form-control','maxlength'=>50,'id' => 'to-resident-select')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row" id="to-name" style="display: none">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.PostParcel.receiver') }}</label>
                                <div class="col-sm-9">
                                    {!! Form::text('to_name',null,array('class'=>'form-control ignore-validate','maxlength'=>50,'id' => 'to-name-input')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.PostParcel.from_name') }}</label>
                                <div class="col-sm-9">
                                    {!! Form::text('from_name',null,array('class'=>'form-control','maxlength'=>50)) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.PostParcel.ems_code') }}</label>
                                <div class="col-sm-9">
                                     {!! Form::text('ems_code',null,array('class'=>'form-control','maxlength'=>15)) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.PostParcel.type') }}</label>
                                <div class="col-sm-5">
                                     {!! Form::select('type',$parcel_type,null,array('class'=>'form-control')) !!}
                                </div>
                                <div class="col-sm-4">
                                     {!! Form::select('size',$parcel_size,null,array('class'=>'form-control')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="control-label col-sm-3">
                                    {{ trans('messages.PostParcel.date_received') }}
                                </div>
                                <div class="col-sm-5">
                                    {!! Form::text('date_received',null,array('class'=>'form-control datepicker','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','data-language'=>App::getLocale())) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.PostParcel.note') }}</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="note" id="note" maxlength="2000" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="add-post-parcel-btn">{{ trans('messages.add') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-create-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ trans('messages.PostParcel.confirm_create_head') }}</h4>
            </div>
            <div class="modal-body">
                {{ trans('messages.PostParcel.confirm_create_msg') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                <button type="button" class="btn btn-primary" id="confirm-create-btn">{{ trans('messages.confirm') }}</button>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="cancel-post-parcel-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-ban"></i> {{ trans('messages.PostParcel.confirm_cancel_head') }}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('messages.PostParcel.confirm_cancel_msg') }}
                    <div style="margin-top: 10px;">
                    {{ Form::textarea('cancel_reason',null,array('class' => 'form-control', 'id' => 'cancel-reason')) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="confirm-cancel-btn">{{ trans('messages.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="change-post-parcel-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('url'=>'admin/parcel-tracking/delivered','method'=>'post','id'=>'cpp-form','class'=>'form-horizontal')) !!}
                <input type="hidden" id="pp-id" name="id" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.PostParcel.confirm_change_head') }}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('messages.PostParcel.confirm_change_msg') }}
                    <br/><br/>
                    <div class="form-group">
                        <label class="control-label col-sm-5">{{ trans('messages.PostParcel.receiver_name') }}</label>
                        <div class="col-sm-6">
                            {!! Form::text('receiver_name',null,array('class'=>'form-control','maxlength'=>50)) !!}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="confirm-change-btn">{{ trans('messages.ok') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="view-post-parcel-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.PostParcel.page_head') }}</h4>
                </div>
                <div class="modal-body form-horizontal content" id="post-parcel-content">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.ok') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="print1-post-parcel-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.PostParcel.print_receive_form_head') }}</h4>
                </div>
                {!! Form::open(array('url' => array('admin/parcel-tracking/print/new'),'class'=>'form-horizontal','method'=>'post','target' => '_blank')) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 block-input">
                           {{ trans('messages.PostParcel.print_receive_form_label') }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            {{ trans('messages.PostParcel.date_received') }}
                        </div>
                        <div class="col-sm-8">
                            {!! Form::text('date_received',null,array('class'=>'form-control datepicker date-report-input','data-format' => "yyyy-mm-dd",'size'=>25,'readonly','placeholder'=>trans('messages.PostParcel.date_received'),'data-language'=>App::getLocale())) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary submit-print">{{ trans('messages.print') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="print2-post-parcel-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.PostParcel.inform_card_head') }}</h4>
                </div>
                {!! Form::open(array('url' => array('admin/parcel-tracking/print/label'),'class'=>'form-horizontal','method'=>'post','target' => '_blank')) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 block-input">
                            {{ trans('messages.PostParcel.inform_card_label') }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            {{ trans('messages.PostParcel.date_received') }}
                        </div>
                        <div class="col-sm-8">
                            {!! Form::text('date_received',null,array('class'=>'form-control datepicker date-report-input','data-format' => "yyyy-mm-dd",'size'=>25,'readonly','placeholder'=>trans('messages.PostParcel.date_received'),'data-language'=>App::getLocale())) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary submit-print">{{ trans('messages.print') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.th.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
<script type="text/javascript">
    $(function () {
        var target;
        $.validator.addMethod("notEqual", function(value, element, param) {
          return this.optional(element) || value != param;
        });

        $("#unit-id").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });

        $('#create-post-parcel-form').validate({
            ignore: ".ignore-validate",
            rules: {
                type            : {notEqual:0},
                size            : "required",
                receiver_name   : "required",
                date_received   : "required"
            },
            errorPlacement: function(error, element) {}
        });

        $('#cpp-form').validate({
            rules: {
                receiver_name        : "required"
            },
            errorPlacement: function(error, element) {}
        });

        $('#add-post-parcel-btn').on('click',function () {
            var $c = checkToUser ();
            if($('#create-post-parcel-form').valid() && $c) {

                $('#confirm-create-modal').modal('show');
                //$(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');

               // $('#create-post-parcel-form').submit();
            }
        });

        $('#confirm-create-btn').on('click', function () {
            var $c = checkToUser ();
            if($('#create-post-parcel-form').valid() && $c) {
                $(this).attr('disabled', 'disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#create-post-parcel-form').submit();
            }
        })


        $('#submit-search').on('click',function () {
            searchPostParcel (1);
        });

        $('#pp-search').on('keydown', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                searchPostParcel (1);
            }
        });

        $('.reset-s-btn').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            $('#unit-id').val('-').trigger('change');
            searchPostParcel (1);
        });


        $('.panel-body').on('click','.paginate-link', function (e){
            e.preventDefault();
            searchPostParcel($(this).attr('data-page'));
        });

        $('.panel-body').on('change','.paginate-select', function (e){
            e.preventDefault();
            searchPostParcel($(this).val());
        });

        $('#panel-post-parcel-list').on('click', '.mark-as-delivered', function (e) {
            e.preventDefault();
            $('#pp-id').val($(this).data('id'));
            $('#change-post-parcel-modal').modal('show');
        });

        $('#confirm-change-btn').on('click', function () {
            if($('#cpp-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#cpp-form').submit();
            }
        });

        $('#panel-post-parcel-list').on('click', '.delete-post-parcel', function () {
            target = $(this).data('id');
        });

        $('#confirm-cancel-btn').on('click', function () {
            if( $('#cancel-reason').val() ) {
                $('#cancel-reason').removeClass('error');
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                cancelParcel();
            } else {
                $('#cancel-reason').addClass('error');
            }
        });

        $('#panel-post-parcel-list').on('click', '.view_pp_detail', function (e) {
            e.preventDefault();
            target = $(this).data('id');
            getPostParcel ($(this),target);
        });

        $('.submit-print').on('click', function () {
            var p = $(this).parents('form');
            var date = p.find('.date-report-input');
            if(date.val() == '') {
                date.addClass('error');
            } else {
                date.removeClass('error');
                p.submit();
            }
        })

        function searchPostParcel (page) {
            var data = $('#pp-search').serialize();
            data+= "&page="+page;
            $('#panel-post-parcel-list').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/technician/admin/parcel-tracking",
                method  : "POST",
                data    : data,
                dataType: "html",
                success: function (t) {
                    $('#panel-post-parcel-list').html(t).css('opacity','1');
                    $('[data-toggle="tooltip"]').tooltip();
                }
            })
        }

        function cancelParcel () {
            var data = "id="+target+'&reason='+$('#cancel-reason').val();
            $('#panel-post-parcel-list').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/technician/admin/parcel-tracking/cancel",
                method  : "POST",
                data    : data,
                dataType: "json",
                success: function (r) {
                    if( r.result ) {
                        $('#cancel-reason').val('');
                        $('#cancel-post-parcel-modal').modal('hide');
                        searchPostParcel (1);
                    }
                    $('#confirm-cancel-btn').removeAttr('disabled').find('.fa-spin').remove();
                }
            })
        }

        function getPostParcel (btn,id) {
            btn.find('.fa-eye').remove();
            btn.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i>');
            $.ajax({
                url     : $('#root-url').val()+"/technician/admin/post-and-parcel/view",
                method  : "POST",
                data    : ({id:id}),
                dataType: "html",
                success: function (t) {
                    $('#post-parcel-content').html(t);
                    $('#view-post-parcel-modal').modal('show');
                    btn.removeAttr('disabled').find('.fa-spin').remove();
                    btn.append('<i class="fa-eye"></i>');
                }
            })
        }

        $('#add-pp-unit-select').on('change', function (e){
            e.preventDefault();
            if( $(this).val() == '-' ) {
                $('#to-name').hide();
                $('#to-resident-block').hide();
            } else {
                getCustomerTenant($(this).val());
                $('#s2id_add-pp-unit-select').removeClass('error');
            }
            $(window).resize();
        });

        $('#to-resident-select').on('change', function () {
            if( $(this).val() == 'other') {
                $('#to-name').show();
            } else {
                $('#to-name').hide();
            }
        });

        function getCustomerTenant (uid) {
            $.ajax({
                url     : $('#root-url').val()+"/technician/admin/parcel-tracking/get-resident",
                method  : "POST",
                data    : ({uid:uid}),
                dataType: "json",
                success: function (r) {
                    $('#to-resident-select').html('');
                    if(r.result == true) {
                        $.each(r.data, function (i,v) {
                            $('#to-resident-select').append("<option value='"+i+"'>"+v+"</option>");
                        });
                        $('#to-resident-block').show();
                        $('#to-resident-select').append("<option value='other'>{{ trans('messages.PostParcel.other_receiver') }}</option>");
                    } else {
                        $('#to-name').show();
                        $('#to-resident-select').append("<option value='other' selected>{{ trans('messages.PostParcel.other_receiver') }}</option>");
                    }
                    $(window).resize();
                }
            })
        }

        function checkToUser () {
            var valid = true;
            if( $('#add-pp-unit-select').val() == "-" ) {
                $('#s2id_add-pp-unit-select').addClass('error');
                valid = false;
            } else {
                $('#s2id_add-pp-unit-select').removeClass('error');
            }
            $('#to-name-input').removeClass('error');
            if( $('#to-resident-select').val() == 'other' ) {
                var target = $('#to-name-input').val();
                if( !target ) {
                    $('#to-name-input').addClass('error');
                    valid = false;
                }
            }
            return valid;
        }
    })
</script>
<style type="text/css">
.cbr-replaced {
    margin-top: -5px;
}
</style>
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
@endsection
