@extends('layouts.base-admin')
@section('content')
<?php
    $vehicle_type   = unserialize(constant('VEHICLE_TYPE_'.strtoupper(App::getLocale())));
?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Vehicle.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{url('/')}}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
				<li class="active">
					<strong>{{ trans('messages.Vehicle.page_head') }}</strong>
				</li>
			</ol>
		</div>
	</div>
    <section>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('messages.search') }}</h3>
            </div>
            <div class="panel-body search-form">
                {!! Form::open(array('url' => '#','class'=>'form-horizontal','method'=>'post','id'=>'v-search')) !!}
                <div class="col-sm-3 block-input">
                    {!! Form::select('unit_id',$unit_list,null,array('class'=>'form-control select2', 'id' => 'unit-id')) !!}
                </div>
                <div class="col-sm-3 block-input">
                    {!! Form::select('type',$vehicle_type,null,array('class'=>'form-control','id'=>'sv-type')) !!}
                </div>
                <div class="col-sm-3 block-input" id="specific_brand_search" style="display:none;">
                    <select name="brand" id="s_brand_search_input" class="form-control"></select>
                </div>
                <div class="col-sm-3 block-input">
                    <input type="text" name="plate" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_plate_serial') }}" maxlength=100 />
                </div>
                <div class="col-sm-6 text-right pull-right">
                    <button type="reset" id="reset-search" class="btn btn-white btn-single reset-s-btn">{{ trans('messages.reset') }}</button>
                    <button type="button" id="submit-search" class="btn btn-secondary btn-single">{{ trans('messages.search') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    <a href="#" data-toggle="modal" data-target="#add-vehicle-modal" class="action-float-right btn btn-primary">{{ trans('messages.Vehicle.add_vehicle_label') }} </a><span></span>
    <section>
        <div class="panel panel-default">
             <div class="panel-body" id="panel-vehicle-list">
               @include('vehicle.vehicle-list-element')
            </div>
        </div>
    </section>

    <div class="modal fade" id="get-vehicle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Settings.veh_detail') }}</h4>
                </div>
                <div class="modal-body">
                    <div id="vehicle-content">

                    </div>
                   <span class="v-loading">{{ trans('messages.loading') }}...</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.ok') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="create-sticker-invoice" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Vehicle.create_sticker_head') }}</h4>
                </div>
                {!! Form::open(array('url' => array('admin/vehicle/sticker/invoice/create'),'class'=>'form-horizontal','id'=>'create-sticker-invoice-form','method'=>'post')) !!}
                {!! Form::hidden('id',null,array('id'=>'vehicle-id')) !!}
                <div class="modal-body">
                    {{ trans('messages.Vehicle.create_sticker_msg') }}<span id="plate-span"></span>
                    <h5><strong>{{ trans('messages.Vehicle.create_sticker_label') }}</strong></h5>
                    <div style="padding:0px 15px;">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.feesBills.invoice_name') }}</label>
                                <div class="col-sm-8">
                                    {!! Form::text('name',null,array('class'=>'form-control')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.feesBills.amount') }}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class=""><b>฿</b></i>
                                        </div>
                                        {!! Form::text('amount',null,array('class'=>'form-control','id'=>'amount','maxlength'=>7)) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.feesBills.due_date') }}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>
                                        {!! Form::text('due_date',null,array('class'=>'form-control datepicker','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','data-start-date'=>'0d','data-language'=>App::getLocale())) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4">{{ trans('messages.Vehicle.sticker_expired_date') }}</label>
                                <div class="col-sm-8">
                                    <label>
                                        <div style="margin-top:8px;">
                                            <input id="flag-exp-dt" type="checkbox" name="no_exp_flag" class="cbr cbr-turquoise" checked="checked">
                                            {{ trans('messages.Vehicle.no_expired_date') }}
                                        </div>
                                    </label>
                                    <div class="input-group exp-dt" style="display:none;">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>
                                        {!! Form::text('expired_date',null,array('class'=>'form-control datepicker','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','data-start-date'=>'0d','data-language'=>App::getLocale())) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-sm-offset-4 field-hint">{{ trans('messages.Vehicle.expired_date_desc') }}</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="create-sticker-invoice-btn">{{ trans('messages.ok') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade modal-inline-form" id="add-vehicle-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('url' => array('admin/vehicle/add'),'class'=>'form-horizontal','id'=>'create-vehicle-form','method'=>'post')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Vehicle.add_vehicle_label') }}</h4>
                </div>
                <div class="modal-body">
                    <div style="padding:0px 15px;">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.unit_no') }}</label>
                                <div class="col-sm-9">
                                    <?php unset($unit_list['-']); ?>
                                    {!! Form::select('property_unit_id',$unit_list,null,array('class'=>'form-control')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3 hidden-xs">&nbsp;</label>
                                <div class="col-sm-4">
                                    <?php //unset($vehicle_type[0]); ?>
                                     {!! Form::select('type',$vehicle_type,null,array('class'=>'form-control','id'=>'v-type')) !!}
                                </div>

                                <!--<div class="col-sm-5 row-2">
                                     <input type="text" name="brand" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_brand') }}" maxlength=100 />
                                </div>-->

                                <div class="col-sm-5 row-2" id="other_brand">
                                    <input type="text" name="o_brand" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_brand') }}" maxlength=100 />
                                </div>
                                <div class="col-sm-5 row-2" id="specific_brand" style="display:none;">
                                    <select name="s_brand" id="s_brand_input" class="form-control"></select>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3 hidden-xs">&nbsp;</label>
                                <div class="col-sm-4">
                                     <input type="text" name="model" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_model') }}" maxlength=200 />
                                </div>

                                <div class="col-sm-5 row-2">
                                     <input type="text" name="color" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_color') }}" maxlength=100 />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3 hidden-xs">&nbsp;</label>
                                <div class="col-sm-4">
                                    {!! Form::select('year',selectYearOption (),null,array('class'=>'form-control')) !!}
                                </div>

                                <!-- <label class="control-label col-sm-1">&nbsp;</label> -->
                                <div class="col-sm-5 row-2">
                                    <input type="text" name="lisence_plate" class="form-control" placeholder="{{ trans('messages.Prop_unit.veh_plate_serial') }}" maxlength=200 />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.Vehicle.sticker') }}</label>
                                <div class="col-sm-9">
                                    {!! Form::select('sticker_status',[0=>trans('messages.Vehicle.no_have_sticker'),3=>trans('messages.Vehicle.have_sticker')],null,array('class'=>'form-control','id'=>'sticker_status')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row sticker-exp-date" style="display:none;">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{{ trans('messages.Vehicle.sticker_expired_date') }}</label>
                                <div class="col-sm-9">
                                    <label>
                                        <div style="margin-top:8px;margin-bottom:10px;">
                                            <input id="add-v-flag-exp-dt" type="checkbox" name="no_exp_flag" class="cbr cbr-turquoise" checked="checked">
                                            {{ trans('messages.Vehicle.no_expired_date') }}
                                        </div>
                                    </label>
                                    <div class="input-group add-v-exp-dt" style="display:none;">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="linecons-calendar"></i></a>
                                        </div>
                                        {!! Form::text('expired_date',null,array('class'=>'form-control datepicker','data-format' => "yyyy/mm/dd",'size'=>25,'readonly','data-start-date'=>'0d','data-language'=>App::getLocale())) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="add-vehicle-btn">{{ trans('messages.add') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('/')}}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/datepicker/bootstrap-datepicker.th.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/number.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/vehicle.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{url('/')}}/js/app-scripts/search-form.js"></script>
<script type="text/javascript">
    $(function () {

        var select = $("#create-vehicle-form select").selectBoxIt().on('open', function(){
            $(this).data('selectBoxSelectBoxIt').list.perfectScrollbar();
        });

        $("#unit-id").select2({
            placeholder: "{{ trans('messages.unit_number') }}",
            allowClear: true,
            dropdownAutoWidth: true
        });

        $.validator.addMethod("notEqual", function(value, element, param) {
          return this.optional(element) || value != param;
        });
        $('#create-sticker-invoice-form').validate({
            rules: {
                name        : "required",
                amount      : "required",
                due_date    : "required",
                expired_date    : { required:function(){ return (!$("#flag-exp-dt").is(':checked'))?true:false; }}},
            errorPlacement: function(error, element) {}
        });

        $('#create-sticker-invoice-btn').on('click',function () {
            if($('#create-sticker-invoice-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#create-sticker-invoice-form').submit();
            }
        });

        $('#create-vehicle-form').validate({
            rules: {
                type    : {notEqual:0},
                o_brand : {
                    required:  function (element) {
                                    var type = $("select[name='type']").val();
                                     if( $.inArray(type,[0,3,4]) ){ return true; }
                                     else return false;
                                }
                },
                s_brand : 'checkIsCM',
                model   : 'required',
                color   : 'required',
                year    : {required:true,digits:true},
                lisence_plate       : 'required',
                due_date    : "required",
                expired_date    : { required: function () {
                        if($('#sticker_status').val() == 0) {
                            return false;
                        } else {
                            return (!$("#add-v-flag-exp-dt").is(':checked'))?true:false;
                        }
                    }
                }
            },
            errorPlacement: function(error, element) {}
        });

        $('#add-vehicle-btn').on('click',function () {
            if($('#create-vehicle-form').valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('#create-vehicle-form').submit();
            }
        });

        $("#amount").number( true ,2 );
        $('#panel-vehicle-list').on('click','.get-vehicle' ,function (){
            var id = $(this).data('vehicle-id');
            $('.v-loading').show();
            $('#vehicle-content').empty();
            $.ajax({
                url : $('#root-url').val()+"/settings/home/vehicle/detail",
                method : 'post',
                dataType: 'html',
                data : ({'id':id}),
                success: function (r) {
                    $('.v-loading').hide();
                    $('#vehicle-content').html(r);
                },
                error : function () {

                }
            })
        });

        $('#panel-vehicle-list').on('click','.create-sticker' ,function (e){
            $(".error").removeClass("error");
            $('#create-sticker-invoice-form').find("input[type=text]").val("");
            e.preventDefault();
            var plate = $(this).data('plate');
            $('#vehicle-id').val($(this).data('vehicle-id'));
            $('#plate-span').html(plate);
            $('#create-sticker-invoice').modal('show');
        });

        $('#flag-exp-dt').on('change',function () {
            if($(this).is(':checked')) {
                $('.exp-dt').hide();
            } else {
                $('.exp-dt').show();
            }
        });

         $('#sticker_status').on('change',function () {
            if($(this).val() == 0) {
                $('.sticker-exp-date').hide();
            } else {
                $('.sticker-exp-date').show();
            }
        });

        $('#add-v-flag-exp-dt').on('change',function () {
            if($(this).is(':checked')) {
                $('.add-v-exp-dt').hide();
            } else {
                $('.add-v-exp-dt').show();
            }
        });

        $('#submit-search').on('click',function () {
            searchVehicle (1);
        });

        $('#reset-search').on('click',function () {
            $(this).closest('form').find("input").val("");
            $(this).closest('form').find("select option:selected").removeAttr('selected');
            $('#unit-id').val('-').trigger('change');
            searchVehicle (1);
        });

        $('#panel-vehicle-list').on('click','.vehicle-pagination li a', function (e) {
            e.preventDefault();
            searchVehicle ($(this).data('page'));
        });

        $('#v-search').on('keydown', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                searchVehicle (1);
            }
        });

        function searchVehicle (page) {
            var data = $('#v-search').serialize();
            data+= "&page="+page;
            $('#panel-vehicle-list').css('opacity','0.6');
            $.ajax({
                url     : $('#root-url').val()+"/admin/vehicle",
                method  : "POST",
                data    : data,
                dataType: "html",
                success: function (t) {
                    $('#panel-vehicle-list').html(t).css('opacity','1');
                    $('[data-toggle="tooltip"]').tooltip();
                }
            })
        }
    })
</script>
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2.css">
<link rel="stylesheet" href="{{url('/')}}/js/select2/select2-bootstrap.css">
@endsection
