<?php
    $vehicle_type = unserialize(constant('VEHICLE_TYPE_'.strtoupper(App::getLocale())));
?>
@if($vehicles->count())
<?php
    $allpage = $vehicles->lastPage();
    $curPage = $vehicles->currentPage();
 ?>
 <p>{{ trans('messages.Vehicle.vehicle_total',['no' => $vehicles->total()]) }}</p><br/>
@foreach( $vehicles as $vehicle )
<?php
    $exp = false;
    if($vehicle->sticker_expire_date) {
        $exp = laterThanNow_ ($vehicle->sticker_expire_date);
    }
?>
    <div class="invoice-row">
        <div class="invoice-head">
            <b>{{ trans('messages.Prop_unit.veh_plate_serial') }}  : {{ $vehicle->lisence_plate }} </b>
        </div>
        <div class="col-md-4">
            {{ trans('messages.unit_no') }} : {{ $vehicle->property_unit->unit_number }}
        </div>
        <div class="col-md-4">
           {{ trans('messages.type') }} :
           {{ $vehicle_type[$vehicle->type] }}
       </div>
        <div class="col-md-4">
            {{ trans('messages.Prop_unit.veh_brand') }} :
            {{ $vehicle->brand }}
        </div>
        <div class="col-md-4">
            {{ trans('messages.Prop_unit.veh_model') }} :
            {{ $vehicle->model }}
       </div>
       <div class="col-md-4">
           {{ trans('messages.Prop_unit.veh_sticker_status') }} :
           @if($exp)
           {{ trans('messages.Vehicle.sticker_status_4') }}
           @else
           {{ trans('messages.Vehicle.sticker_status_'.$vehicle->sticker_status) }}
           @endif
      </div>
      <div class="col-md-4 action-links text-right">
          <a href="#" class="edit get-vehicle btn btn-blue" data-toggle="modal" data-target="#get-vehicle" data-vehicle-id="{{ $vehicle->id }}">
              <i class="fa-eye"></i>
          </a>
          @if($vehicle->sticker_status == 1 && !$exp)
           <a href="#" data-original-title="{{ trans('messages.Vehicle.make_invoice') }}" class="create-sticker btn btn-warning" data-toggle="tooltip" data-placement="top" data-vehicle-id="{{ $vehicle->id }}" data-plate="{{ $vehicle->lisence_plate }}">
              <i class="fa-money"></i>
          </a>
          @endif
      </div>
 </div>
 @endforeach
<div class="row">
    @if($allpage > 1)
    <div class="col-sm-12 text-right">
        <ul class="pagination no-margin vehicle-pagination">
            @if($curPage > 1)
            <li>
                <a href="#" data-page="{{ $curPage-1 }}">
                    <i class="fa-angle-left"></i>
                </a>
            </li>
            @endif

            @for($i = 1; $i <= $allpage; $i++)
            <li @if($curPage == $i) class="active" @endif>
                <a href="#" data-page="{{ $i }}">{{$i}}</a>
            </li>
            @endfor
            @if($vehicles->hasMorePages())
            <li>
                <a href="#" data-page="{{ $curPage+1 }}">
                    <i class="fa-angle-right"></i>
                </a>
            </li>
            @endif
        </ul>
    </div>
    @endif
</div>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Vehicle.vehicle_not_found') }}</div><div class="clearfix"></div>
@endif
