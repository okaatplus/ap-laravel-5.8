
@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="breadcrumb-env">

            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{  url('/')  }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Vote.page_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    {!! Form::model($vote,array('url' => array('admin/survey/save'),'class'=>'form-horizontal')) !!}
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ trans('messages.Vote.create_vote') }}</h3>
                </div>
                <div class="panel-body">
                    @include('vote.form')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default text-right">
                <a class="btn btn-gray" href="{{url('admin/survey')}}">{{trans('messages.back')}}</a>
                {!! Form::button(trans('messages.save'), ['class'=>'btn btn-primary','id'=>'submit-form']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection
