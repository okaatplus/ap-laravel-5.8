@extends('layouts.blank')
@section('content')
    <?php $lang = App::getLocale(); ?>
    <tr>
        <td colspan="9" border="0"></td>
    </tr>
    <tr>
        <td style="font-size: 20; font-weight: bold;" colspan="9" border="0">
            รายการแบบสอบถามในโครงการ
        </td>
    </tr>
    <tr>
        <td colspan="9">{{ trans('messages.property_name') }} : {{ $property->{'property_name_'.$lang} }}</td>
    </tr>
    <tr>
        <td colspan="9" border="0"></td>
    </tr>
    <tr class="head">
        <th>{{ trans('messages.Report.sequence') }}</th>
        <th>{{ trans('messages.Vote.page_head') }}</th>
        <th>{{ trans('messages.detail') }}</th>
        <th>{{ trans('messages.Event.start_date') }}</th>
        <th>{{ trans('messages.Event.end_date') }}</th>
        <th>{{ trans('messages.Post.created_at') }}</th>
        <th>{{ trans('messages.Post.publish_status') }}</th>
        <th>{{ trans('messages.Vote.choice') }}</th>
        <th>{{ trans('messages.Vote.result') }}</th>
    </tr>

    @foreach($votes as $key => $vote)
        <tr class="content">
            <td style="text-align: center;">{{ $key+1 }}</td>
            <td>{{ $vote->title_th }}</td>
            <td>{{ strip_tags(htmlspecialchars_decode($vote->description_th)) }}</td>
            <td>
                <?php

                $vote->start_time  = date("g:i A", strtotime($vote->start_time) );
                $vote->end_time    = date("g:i A", strtotime($vote->end_time) );

                ?>

                {{ localDateShortNotime( $vote->start_date ) . " " . $vote->start_time }}

            </td>
            <td>
                {{ localDateShortNotime( $vote->end_date )  . " " . $vote->end_time }}
            </td>
            <td>{{ localDateShortNotime($vote->created_at) }}</td>
            <td>
                @if( $vote->publish_status )
                    {{ trans('messages.Post.publish') }}
                @else
                    {{ trans('messages.Post.draft') }}
                @endif
            </td>
            <?php $choice = $vote->voteChoice; ?>
            <td>{{ $choice[0]['title'] }}</td>
            <td>{{ $choice[0]['choice_count'] }}</td>
            <?php array_forget($choice,0); ?>
            <?php if( count($choice) ) : ?>
                @foreach( $choice as $c)
                    <tr>
                        <td class="td-bg"></td>
                        <td class="td-bg"></td>
                        <td class="td-bg"></td>
                        <td class="td-bg"></td>
                        <td class="td-bg"></td>
                        <td class="td-bg"></td>
                        <td class="td-bg"></td>
                        <td>{{ $c['title'] }}</td>
                        <td>{{ $c['choice_count'] }}</td>
                    </tr>
                @endforeach
            <?php endif; ?>
    @endforeach
@endsection