{!! Form::hidden('id') !!}
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.title') }} (TH)</label>
        <div class="col-sm-9">
            {!! Form::text('title_th',null,array('class'=>'form-control','maxlength' => 200 )) !!}
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.title') }} (EN)</label>
        <div class="col-sm-9">
            {!! Form::text('title_en',null,array('class'=>'form-control','maxlength' => 200 )) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="profile-image">{{ trans('messages.img.insertion_img') }}</label>
        <div class="col-sm-4">
            <div id="attachment-banner-crop" class="drop-property-file dz-clickable" style="margin-bottom: 0; @if($vote->banner_url) display: none; @endif">
                <i class="fa fa-camera"></i>
            </div>
            <div class="preview-crop"></div>
            <div class="post-banner" id="previews-img-banner-crop">
                @if($vote->banner_url != null)
                    <div class="preview-img-item" style="margin: 0;">
                        <img src="{{ env('URL_S3')."/vote-file".$vote->banner_url }}" width="100%" alt="post-image" />
                        <span class="remove-img-preview remove-banner">×</span>
                    </div>
                @endif
            </div>

        </div>
        <div class="col-sm-5">
            <div class="img-container">
                <img class="img-responsive" />
            </div>
        </div>
        <input type="hidden" id="img-x" name="img-x"/>
        <input type="hidden" id="img-y" name="img-y"/>
        <input type="hidden" id="img-w" name="img-w"/>
        <input type="hidden" id="img-h" name="img-h"/>
        <input type="hidden" id="img-tw" name="img-tw"/>
        <input type="hidden" id="img-th" name="img-th"/>
    </div>

    <div class="row form-group">
        <label class="control-label col-md-3">{{ trans('messages.Event.start_date') }}</label>
        <div class="col-md-9">
            <div class="date-and-time">
                <?php
                if($vote->start_date) $sd = date('Y-m-d',strtotime($vote->start_date));
                else $sd = null;
                ?>
                {!! Form::text('start_date', $sd, ["class" => "form-control datepicker", "readonly", "data-start-date" => "0d", "data-format" => "yyyy-mm-dd",'data-language'=>App::getLocale(),'id' => 'start-date']) !!}

                <?php
                if($vote->start_time) $st = date('h:i A',strtotime($vote->start_time));
                else $st = date('h:i A');
                ?>
                {!! Form::text('start_time', $st, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5",'id' => 'start-time']) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="row form-group">
        <label class="control-label col-md-3">{{ trans('messages.Event.end_date') }}</label>
        <div class="col-md-9">
            <div class="date-and-time">
                <?php
                if($vote->end_date) $ed = date('Y-m-d',strtotime($vote->end_date));
                else $ed = null;
                ?>
                {!! Form::text('end_date', $ed, ["class" => "form-control datepicker", "readonly", "data-start-date" => "0d", "data-format" => "yyyy-mm-dd",'data-language'=>App::getLocale(),'id' => 'end-date']) !!}

                <?php
                if($vote->end_time) $et = date('h:i A',strtotime($vote->end_time));
                else $et = NULL;
                ?>
                {!! Form::text('end_time', $et, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5",'id' => 'end-time']) !!}
            </div>
        </div>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.detail') }} (TH)</label>
        <div class="col-sm-9">
            {!! Form::textarea('description_th',null,array('class'=>'form-control', 'id' => 'description_th')) !!}
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.detail') }} (EN)</label>
        <div class="col-sm-9">
            {!! Form::textarea('description_en',null,array('class'=>'form-control', 'id' => 'description_en')) !!}
        </div>
    </div>

    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.Vote.choice') }}</label>
        <div class="col-md-9">
            <div class="form-inline">
                <input type="text" class="form-control" name="choice_title" id="input-choice" tabindex="1" placeholder="{{ trans('messages.Vote.add_choice') }}" maxlength="100" />
                <input type="button" class="form-control btn btn-white btn-single" id="add-choice" value="{{ trans('messages.Vote.add') }}">
            </div>
            <div class="form-group-separator-noline"></div>
            <div class="list-item choice-item">
                @foreach( $vote->voteChoice as $choice)
                    <div class="alert alert-white alert-choice vote-choice">
                        <a data-dismiss="alert" class="close remove-element" data-e-type="choice" data-e-id="{{ $choice->id }}">
                            <span aria-hidden="true">×</span>
                        </a>
                        {{ $choice->title }}
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    <div class="row form-group">
        <label class="col-sm-3 control-label">{{ trans('messages.attach') }}</label>
        <div class="col-sm-9">
            <ul class="list-unstyled list-inline form-action-buttons" style="margin-top: 6px;">
                <li>
                    <button type="button" id="attachment" class="btn btn-unstyled upload">
                        <i class="fa fa-camera"></i> {{ trans('messages.attach') }}
                    </button>
                </li>
                <li>
                    <div class="field-hint">{{ trans('messages.upload_file_description') }}</div>
                </li>
            </ul>
            <div id="previews">
                @if($vote->voteFile && $vote->voteFile->count() > 0)
                    @foreach($vote->voteFile as $file)
                        <div class="preview-img-item">
                            <img src="{{ env('URL_S3')."/vote-file/".$file->url.$file->name }}" width="80px" alt="post-image" />
                            <span class="remove-img-preview remove-element" data-e-type="event-file" data-e-id="{{ $file->id }}">×</span>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-3 control-label" for="field-2">{{ trans('messages.news.status') }}</label>
        <div class="col-sm-4">
            {!! Form::select('publish_status', ['0'=>trans('messages.news.draft'),'1'=>trans('messages.news.publish')],null,['id'=>'status','class'=>'form-control']) !!}
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            @if($vote && $vote->created_user)
                <ul class="list-unstyled list-inline form-action-buttons text-right">
                    <li class="field-hint"><b>{{ trans('messages.created_by') }}</b> {{ $vote->created_user->name }}</li>
                    <li class="field-hint" ><b>{{ trans('messages.created_at') }}</b> {{ showDateTime($vote->created_at) }}</li>
                </ul>
            @endif
            @if($vote && $vote->updated_user)
                <ul class="list-unstyled list-inline form-action-buttons text-right">
                    <li class="field-hint" ><b>{{ trans('messages.updated_by') }}</b> {{ $vote->updated_user->name }}</li>
                    <li class="field-hint" ><b>{{ trans('messages.updated_at') }}</b> {{ showDateTime($vote->updated_at) }}</li>
                </ul>
            @endif
        </div>
    </div>
    <div class="delete-zone"></div>
    <input type="hidden" name="remove-banner-flag" id="remove-banner-flag">

@section('script')
<script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/app-scripts/upload-file.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/select2/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/crop-img.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/timepicker/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.th.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/validate-date-from-to.js"></script>

    <script type="text/javascript">
    $(function () {

        var root = $('#root-url').val();

        $.validator.addMethod("ExistedChoice", function() {
            return ($('.choice-item .vote-choice').length > 0);
        });

        //makeUploadZone('#attachment','#previews');
        tinymce.init({
            selector:"textarea",
            height: 300,
            theme: 'modern',
            content_css : ['{{ url('css/custom.css') }}','{{ url('font/ap/font-wys.css') }}'],
            plugins: [
                'advlist autolink lists link charmap print preview hr',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'table contextmenu directionality',
                'template paste textcolor colorpicker textpattern'
            ],
            toolbar: 'insertfile undo redo | styleselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link |',
        });
        $("form").validate({
           rules: {
                title_th: 'required',
                //title_en: 'required',
                start_date: 'required',
                start_time: 'required',
                choice_title  : 'ExistedChoice',
                end_date: {
                    //dateAfter: ['#start-date','#start-time'],
                    required: true
                }
           },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('error');
                $( window ).resize();
            },
            errorPlacement: function(error, element) {}
        })


        $('#submit-form').on('click', function () {
            var _valid = checkDetail();
            if( $("form").valid() && _valid ) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $('form').submit();
            } else {
                var top_;
                top_ = $('.error').first().offset().top;
                $('html,body').animate({scrollTop: top_-100}, 1000);
            }
        });

        $('#add-choice').on('click',function () {
            if($('#input-choice').val() != "") {
                var text = $('#input-choice').val();
                $('.list-item').append(
                    $('<div/>').addClass('alert alert-white alert-choice vote-choice').html(text).prepend(
                        $('<a/>').attr({'class':'close','data-dismiss':'alert'}).append(
                            '<span aria-hidden="true">×</span><span class="sr-only">Close</span>'
                        )
                    ).append($('<input/>').attr({'name':'choice[]','value':text,'type':'hidden'}))
                );
                $(window).resize();
                $('#input-choice').val('');
            }
            $('#input-choice').removeClass('error');
        });

        $('.remove-element').on('click', function () {
            var model = $(this).data('e-type');
            var id = $(this).data('e-id');
            $('.delete-zone').append('<input type="hidden" name="remove['+model+'][]" value="'+id+'" />');
            $(this).parent().remove();
        });
    });

        
    function checkDetail () {
        var valid = true;
        if( tinyMCE.get('description_th').getContent() == "" ) {
            $('#description_th').prev().addClass('error mce-error');
            valid = false;
        } else {
            $('#description_th').prev().removeClass('error mce-error');
        }

        if ( tinyMCE.get('description_en').getContent() == "") {
            $('#description_en').prev().addClass('error mce-error');
            valid = false;
        } else {
            $('#description_en').prev().removeClass('error mce-error');
        }
        return valid;
    }
</script>
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
<link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="{{url('/')}}/js/cropper/cropper.min.css">
@endsection
