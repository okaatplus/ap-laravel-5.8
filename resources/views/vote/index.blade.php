<?php $allow_post_vote = (Auth::user()->property->allow_user_add_vote || Auth::user()->role == 1 || Auth::user()->role == 3); ?>
@extends('layouts.base-admin')
@section('content')
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">{{ trans('messages.Vote.page_head') }}</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="{{  url('/')  }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
				</li>
				<li class="active">
					<strong>{{ trans('messages.Vote.page_head') }}</strong>
				</li>
			</ol>
		</div>
	</div>
    <section class="mailbox-env">
        <div class="row">
            <div class="col-sm-9 mailbox-right" style="position:relative;">
                @if($allow_post_vote)
				<div class="text-right">
					<a href="#" data-toggle="modal" data-target="#modal-vote-add" type="button" class="btn btn-primary bg-lg"><i class="fa fa-pie-chart"></i> {{ trans('messages.Vote.create_vote') }}</a>
				</div>
                @endif
                <div class="mail-env" id="vote-list-content">
                    @include('vote.vote-list')
                </div>
            </div>
            <!-- Mailbox Sidebar -->
             <!-- Mailbox Sidebar -->
            <div class="col-sm-3 mailbox-left">
                <div class="mailbox-sidebar">
                    <ul id="vote-type-list" class="list-unstyled mailbox-list">
                        <li class="active">
                            <a href="#" data-type="0">
                                {{ trans('messages.all') }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-type="voted">
                                {{ trans('messages.Vote.voted_result') }}
                            </a>
                        </li>
                        <li>
                            <a href="#" data-type="my">
                                {{ trans('messages.Vote.created') }}
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-vote-add" data-backdrop="static">
        <div class="modal-dialog">
            <form method="post" action="{{url('admin/survey/add')}}" role="form" id="create-vote-form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Vote.create_vote') }}</h4>
                </div>
                <div class="modal-body" id="vote-content">
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" id="title" tabindex="1" placeholder="{{ trans('messages.title') }}"  maxlength="200"/>
                        </div>

                        <div class="compose-message-editor">
                            <textarea name="description" class="form-control wysihtml5" data-html="false" data-color="false" data-stylesheet-url="{{ url('/') }}/css/other/wysihtml5-color.css" placeholder="{{ trans('messages.detail') }}" maxlength="2000"></textarea>
                        </div>
                        <div class="form-group-separator-noline"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label text-bold">{{ trans('messages.Event.start_date') }}</label>
                                    <div class="date-and-time">
                                        <input type="text" name="start_date" class="form-control datepicker" value="{{date('Y/m/d')}}" data-format="yyyy/mm/dd" readonly data-start-date="0d" data-language="{{ App::getLocale() }}"/>
                                        <input type="text" name="start_time" class="form-control timepicker"placeholder="Time" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="5" readonly />
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label text-bold">{{ trans('messages.Event.end_date') }}</label>
                                    <div class="date-and-time">
                                        <input type="text" name="end_date" class="form-control datepicker" value="{{date('Y/m/d')}}" data-format="yyyy/mm/dd" width="25" readonly data-start-date="0d" data-language="{{ App::getLocale() }}" />
                                        <input type="text" name="end_time" class="form-control timepicker"placeholder="Time" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="5" readonly />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group-separator-noline"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group-separator"></div>
                                <label class="control-label text-bold">{{ trans('messages.Vote.choice') }}</label>
                                <div class="form-group-separator-noline"></div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control novalidate" name="choice_title" id="input-choice" tabindex="1" placeholder="{{ trans('messages.Vote.add_choice') }}" maxlength="100" />
                                    </div>
                                    <div class="form-group">
                                        <input type="button" class="form-control btn btn-white btn-single" id="add-choice" value="{{ trans('messages.Vote.add') }}">
                                    </div>
                                </div>
                                <div class="form-group-separator-noline"></div>
                                <div class="list-item"></div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group-separator"></div>
                                <label class="control-label text-bold">{{ trans('messages.attachment') }}</label>
                                <div class="form-group-separator-noline"></div>
                                <span id="attachment"><i class="fa fa-camera"></i> {{ trans('messages.attach') }}</span>
                                <div class="field-hint">{{ trans('messages.upload_file_description') }}</div>
                                <div id="previews"></div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="button" class="btn btn-primary btn-icon pull-righ" id="create-vote-btn">{{ trans('messages.Vote.create_vote') }}</button>
                </div>
                </form>
            </div>
         </div>
    </div>

    <div class="modal fade" id="modal-vote-delete" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-trash"></i> {{ trans('messages.Vote.confirm_delete_head') }}</h4>
                </div>
                <div class="modal-body"> {{ trans('messages.Vote.confirm_delete_msg') }} </div>
                <div class="modal-footer">
                    <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="botton" id="confirm-delete-vote" class="btn btn-primary">{{ trans('messages.delete') }}</button>
                </div>
            </div>
         </div>
    </div>

    <div class="modal fade" id="modal-report-vote" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.report')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="vote-report-form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-12" for="reason">{{trans('messages.Post.report_reason_label')}}</label>
                                    <div class="col-sm-12">
                                        <textarea id="input-report" name="reason" class="form-control"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.cancel')}}</button>
                    <button type="button" class="btn btn-primary" id="report-vote-btn">{{trans('messages.Post.report_btn')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-report-vote-result">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Post.report')}}</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">{{trans('messages.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-vote-edit" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Vote.edit_vote') }} </h4>
                </div>
                <div class="landing-edit-form">
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript" src="{{ url('/') }}/js/dropzone/dropzone.min.js"></script>
	<script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.th.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/upload-file.js"></script>
    <script type="text/javascript">
        $(function () {
            jQuery.validator.addMethod("hasChoice", function() {
                return ($('input[name="choice[]"]').length > 0);
            });

            var target_temp;
            var root = $('#root-url').val();
            $("#vote-report-form").validate({
               rules: {
                 reason: "required"
               },
               messages: {
                 reason: "{{trans('messages.Post.report_validate')}}"

               }
            });
             validator = $("#create-vote-form").validate({
                ignore : '.novalidate',
                   rules: {
                        title       : "required",
                        description : "required",
                        start_date  : "required",
                        start_time  : "required",
                        end_date    : "required",
                        end_time    : "required",
                        ///choice_title  : 'hasChoice'
                   },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass('error');
                        $( window ).resize();
                    },
                    errorPlacement: function(error, element) {}
                })

            $('#create-vote-btn').on('click', function () {
                var cc = checkChoice();
                if( $("#create-vote-form").valid() && cc ) {
                    $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                    $('#create-vote-form').submit();
                }
            })

            $('#add-choice').on('click',function () {
                if($('#input-choice').val() != "") {
                    var text = $('#input-choice').val();
                    $('.list-item').append(
                        $('<div/>').addClass('alert alert-white alert-choice vote-choice').html(text).prepend(
                            $('<a/>').attr({'class':'close','data-dismiss':'alert'}).append(
                                '<span aria-hidden="true">×</span><span class="sr-only">Close</span>'
                            )
                        ).append($('<input/>').attr({'name':'choice[]','value':text,'type':'hidden'}))
                    );
                    $(window).resize();
                    $('#input-choice').val('');
                }
                $('#input-choice').removeClass('error');
            })


            $('#vote-type-list a').on('click',function (e) {
                e.preventDefault();
                $('.mail-env').css('opacity','0.6');
                $('#vote-type-list li').removeClass('active');
                $(this).parent().addClass('active');
                var type = $(this).data('type');
                var r = $.ajax({
                    url:  root+"admin/survey",
                    data:({'type':type}),
                    dataType: "html",
                    method:'POST'
                });
                r.done(function( result ) {
                    $('.mail-env').html( result );
                    $('.mail-env').css('opacity','1');
                });
            })

            $('.mail-env').on('click','.next-page,.prev-page', function (e){
                e.preventDefault();
                if(!$(this).hasClass('disabled')) {
                    $('.mail-env').css('opacity','0.6');
                    var page = $(this).data('page');
                    var type = $('#vote-type-list li.active a').data('type');
                    var r = $.ajax({
                        url:  root+"admin/survey",
                        data:({'type':type,'page':page}),
                        dataType: "html",
                        method:'POST'
                    });
                    r.done(function( result ) {
                        $('.mail-env').html( result );
                        $('.mail-env').css('opacity','1');
                    });
                }
            });

            $('#vote-list-content').on('click','.link-delete-vote',function () {
                target_temp = $(this).attr('data-vote-id');
            });

            $('#confirm-delete-vote').on('click',function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                window.location.href = root+"admin/survey/delete/"+target_temp;
            })

            $('#vote-list-content').on('click','.vote-report', function (){
            target_tmp      = $(this).data('id');
            target_element  = $(this).children('i');
            checkReport ();
        });

        $('#report-vote-btn').on('click',function () {
            if($("#vote-report-form").valid()) {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : root+"/report",
                    data    : 'id='+target_tmp+"&reason="+$('#input-report').val()+'&type=3',
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        $('#modal-report-vote').modal('hide');
                        $('#modal-report-vote-result .modal-body').html(r.msg);
                        $('#modal-report-vote-result').modal('show');
                        $('#input-report').val('');
                        $('#report-vote-btn').removeAttr('disabled').children('.fa-spinner').remove();
                    }
                });
            }

        })

        $('.mail-env').on('click','.link-edit-vote', function (e) {
            e.preventDefault();
            var _btn = $(this);
            _btn.html('<i class="fa-spin fa-spinner"></i>');
            var vote_id = $(this).attr('data-vote-id');
            $.ajax({
                    url     : root+"admin/survey/form/get",
                    data    : 'id='+vote_id,
                    method  : 'post',
                    dataType: 'html',
                    success: function(r){
                        $('#modal-vote-edit .landing-edit-form').html(r);
                        $('#modal-vote-edit').modal('show');
                        $(window).resize();
                        _btn.html('<i class="fa-pencil"></i>');
                    }
                });
        })

        function checkReport () {
            var loader = $('<i class="fa-spin fa-spinner"></i>');
            target_element.after(loader);
            target_element.hide();
            $.ajax({
                    url     : root+"/report-check",
                    data    : 'id='+target_tmp+'&type=3',
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        if(r.status) {
                            validator.resetForm();
                            $("label.error").hide();
                            $(".error").removeClass("error");
                            $('#modal-report-vote').modal('show');
                        } else {
                            $('#modal-report-vote-result .modal-body').html(r.msg);
                            $('#modal-report-vote-result').modal('show');
                        }
                        loader.remove();
                        target_element.show();
                    }
                });
        }
        function checkChoice () {
            if($('input[name="choice[]"]').length > 0) {
                $('#input-choice').removeClass('error');
                return true;
            } else {
                $('#input-choice').addClass('error');
                return false;
            }
        }
        })
    </script>
@endsection
