@if($votes->count() > 0)
    <?php
    $from   = $start = (($votes->currentPage()-1)*$votes->perPage())+1;
    $to     = (($votes->currentPage()-1)*$votes->perPage())+$votes->perPage();
    $to     = ($to > $votes->total()) ? $votes->total() : $to;
    $allpage = $votes->lastPage();
    $is_p_admin = Auth::user()->role == 1;
    ?>

	<div class="row">
		<div class="col-md-4" style="margin-bottom: 10px;">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite" style="padding:0 0 10px 0;">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$votes->total()]) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="text-right" >
				@if($allpage > 1)
					@if($votes->currentPage() > 1)
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $votes->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
					@endif
					@if($votes->lastPage() > 1)
                        <?php echo Form::selectRange('page', 1, $votes->lastPage(),$votes->currentPage(),['class'=>'form-control paginate-select']); ?>
					@endif
					@if($votes->hasMorePages())
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $votes->currentPage()+1 }}">{{ trans('messages.next') }}</a>
					@endif
				@endif
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="p-list" width="100%">
				<thead>
				<tr>
					<th width="7%" style="vertical-align: middle">{{ trans('messages.Report.sequence') }}</th>
					<th width="*" style="vertical-align: middle">{{ trans('messages.Vote.page_head') }}</th>
					<th width="14%" style="vertical-align: middle">{{ trans('messages.Event.start_date') }}</th>
					<th width="14%" style="vertical-align: middle">{{ trans('messages.Event.end_date') }}</th>
					<th width="10%" style="vertical-align: middle">{{ trans('messages.Post.created_at') }}</th>
					<th width="13%" style="vertical-align: middle">{{ trans('messages.Post.publish_status') }}</th>
					<th width="15%" style="vertical-align: middle">{{ trans('messages.action') }}</th>
				</tr>
				</thead>
			@foreach($votes as $vote)
					<td class="text-center">{{ $start++ }}</td>
					<td>{{ $vote->title_th }}</td>
					<td>
                        <?php

                        $vote->start_time  = date("g:i A", strtotime($vote->start_time) );
                        $vote->end_time    = date("g:i A", strtotime($vote->end_time) );

                        ?>

						{{ localDateShortNotime( $vote->start_date ) . " " . $vote->start_time }}

					</td>
					<td>
						{{ localDateShortNotime( $vote->end_date )  . " " . $vote->end_time }}
					</td>
					<td>{{ localDateShortNotime($vote->created_at) }}</td>
					<td class="text-center">
						@if( $vote->publish_status )
							{{ trans('messages.Post.publish') }}
						@else
							{{ trans('messages.Post.draft') }}
						@endif
					</td>
					<td  class="action-links text-center">
						<a class="btn btn-info" target="_blank" href="{{ url('admin/survey/view') }}/{{ $vote->id }}">
							<i class="fa fa-eye"></i>
						</a>
						@if( !$vote->publish_status || greaterThanNow ($vote->start_date,$vote->start_time) )
							<a class="btn btn-warning" href="{{ url('admin/survey/edit') }}/{{ $vote->id }}"><i class="fa-pencil"></i></a>
						@else
						<a class="btn disabled">
							<i class="fa fa-pencil"></i>
						</a>
						@endif
						<a class="btn btn-danger vote-delete" href="#" data-id="{{ $vote->id }}">
							<i class="fa fa-trash-o"></i>
						</a>
					</td>
					</tr>
			@endforeach
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="dataTables_info" id="example-1_info" role="status" aria-live="polite">
				{!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$votes->total()]) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="text-right" >
				@if($allpage > 1)
					@if($votes->currentPage() > 1)
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $votes->currentPage()-1 }}">{{ trans('messages.prev') }}</a>
					@endif
					@if($votes->lastPage() > 1)
                        <?php echo Form::selectRange('page', 1, $votes->lastPage(),$votes->currentPage(),['class'=>'form-control paginate-select']); ?>
					@endif
					@if($votes->hasMorePages())
						<a class="btn btn-white paginate-link" href="#" data-page="{{ $votes->currentPage()+1 }}">{{ trans('messages.next') }}</a>
					@endif
				@endif
			</div>
		</div>
	</div>
@else
	<div class="col-sm-12 text-center">{{ trans('messages.no_data') }}</div><div class="clearfix"></div>
@endif
