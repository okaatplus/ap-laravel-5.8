@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Vote.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{url('admin/survey/add')}}" class="top-action btn btn-primary">
                <i class="fa fa-pie-chart"></i> {{ trans('messages.Vote.create_vote') }}
            </a>
        </div>
    </div>
    <section class="bills-env">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('url' => array('admin/survey/export'),'class'=>'search-form form-horizontal','method'=>'post','id'=>'search-form')) !!}
                <div class="row">
                    <div class="col-sm-3 block-input">
                        {!! Form::text('created_date',null,array('class'=>'form-control datepicker','size'=>25,'data-format'=>'yyyy-mm-dd','placeholder'=> trans('messages.Post.created_at'),'data-language'=>App::getLocale() )) !!}
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" name="publish_status">
                            <option value="-">{{ trans('messages.Post.publish_status') }}</option>
                            <option value="0">{{ trans('messages.Post.draft') }}</option>
                            <option value="1">{{ trans('messages.Post.publish') }}</option>
                        </select>
                    </div>
                    <div class="col-sm-offset-3 col-sm-3 text-right">
                        <button type="reset"  class="btn btn-white btn-single reset-s-btn">{{ trans('messages.reset') }}</button>
                        <button type="button" id="submit-search" class="btn btn-secondary btn-single">{{ trans('messages.search') }}</button>
                        <button type="submit" class="btn btn-secondary btn-single">{{ trans('messages.Report.download') }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
                <hr/>
                <div id="panel-data-list">
                    @include('vote.list-element')
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-vote-delete" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-trash"></i> {{ trans('messages.Vote.confirm_delete_head') }}</h4>
                </div>
                <div class="modal-body"> {{ trans('messages.Vote.confirm_delete_msg') }} </div>
                <div class="modal-footer">
                    <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="botton" id="confirm-delete-vote" class="btn btn-primary">{{ trans('messages.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-vote-add" data-backdrop="static">
        <div class="modal-dialog">
            <form method="post" action="{{url('admin/survey/add')}}" role="form" id="create-vote-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">{{ trans('messages.Vote.create_vote') }}</h4>
                    </div>
                    <div class="modal-body" id="vote-content">
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" id="title" tabindex="1" placeholder="{{ trans('messages.title') }}"  maxlength="200"/>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <div id="attachment-banner" class="drop-property-file dz-clickable" style="margin-bottom: 0;">
                                    <i class="fa fa-camera"></i> {{ trans('messages.insertion_img') }}
                                </div>
                                <div class="post-banner" id="previews-img-banner"></div>
                            </div>
                        </div>

                        <div class="compose-message-editor">
                            <textarea name="description" class="form-control wysihtml5" data-html="false" data-color="false" data-stylesheet-url="{{ url('/') }}/css/other/wysihtml5-color.css" placeholder="{{ trans('messages.detail') }}" maxlength="2000"></textarea>
                        </div>
                        <div class="form-group-separator-noline"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label text-bold">{{ trans('messages.Event.start_date') }}</label>
                                    <div class="date-and-time">
                                        <input type="text" name="start_date" class="form-control datepicker" value="{{date('Y/m/d')}}" data-format="yyyy/mm/dd" readonly data-start-date="0d" data-language="{{ App::getLocale() }}"/>
                                        <input type="text" name="start_time" class="form-control timepicker"placeholder="Time" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="5" readonly />
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label text-bold">{{ trans('messages.Event.end_date') }}</label>
                                    <div class="date-and-time">
                                        <input type="text" name="end_date" class="form-control datepicker" value="{{date('Y/m/d')}}" data-format="yyyy/mm/dd" width="25" readonly data-start-date="0d" data-language="{{ App::getLocale() }}" />
                                        <input type="text" name="end_time" class="form-control timepicker"placeholder="Time" data-template="dropdown" data-show-meridian="true" data-minute-step="1" data-second-step="5" readonly />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group-separator-noline"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group-separator"></div>
                                <label class="control-label text-bold">{{ trans('messages.Vote.choice') }}</label>
                                <div class="form-group-separator-noline"></div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control novalidate" name="choice_title" id="input-choice" tabindex="1" placeholder="{{ trans('messages.Vote.add_choice') }}" maxlength="100" />
                                    </div>
                                    <div class="form-group">
                                        <input type="button" class="form-control btn btn-white btn-single" id="add-choice" value="{{ trans('messages.Vote.add') }}">
                                    </div>
                                </div>
                                <div class="form-group-separator-noline"></div>
                                <div class="list-item"></div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group-separator"></div>
                                <label class="control-label text-bold">{{ trans('messages.attachment') }}</label>
                                <div class="form-group-separator-noline"></div>
                                <span id="attachment"><i class="fa fa-camera"></i> {{ trans('messages.attach') }}</span>
                                <div class="field-hint">{{ trans('messages.upload_file_description') }}</div>
                                <div id="previews"></div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-md-12 text-bold">{{ trans('messages.Post.publish_status') }}</label>
                            <div class="col-md-12">
                                <select class="form-control" name="publish_status">
                                    <option value="0">{{ trans('messages.Post.draft') }}</option>
                                    <option value="1">{{ trans('messages.Post.publish') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                        <button type="button" class="btn btn-primary btn-icon pull-righ" id="create-vote-btn">{{ trans('messages.Vote.create_vote') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modal-vote-edit" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.Vote.edit_vote') }} </h4>
                </div>
                <div class="landing-edit-form">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/datepicker/bootstrap-datepicker.th.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/select2/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/app-scripts/search-form.js"></script>
    <script type="text/javascript">
        $(function () {
            var target_tmp;
            var root = $('#root-url').val();

            $('#submit-search').on('click',function (){
                searchPage(1);
            });

            $('#search-form').on('keydown', function(e) {
                if(e.keyCode == 13) {
                    e.preventDefault();
                    searchPage (1);
                }
            });

            $('.reset-s-btn').on('click',function () {
                $(this).closest('form').find("input").val("");
                $(this).closest('form').find("select option:selected").removeAttr('selected');
                searchPage (1);
            });

            $('.panel-body').on('click','.paginate-link', function (e){
                e.preventDefault();
                searchPage($(this).attr('data-page'));
            });

            $('.panel-body').on('change','.paginate-select', function (e){
                e.preventDefault();
                searchPage($(this).val());
            });

            $('body').on('click','.vote-delete',function (e) {
                e.preventDefault();
                target_tmp = $(this).attr('data-id');
                $('#modal-vote-delete').modal('show');
            });

            $('#confirm-delete-vote').on('click',function (e) {
                e.preventDefault();
                var _this = $(this);
                _this.attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $.ajax({
                    url     : root+"/admin/survey/delete",
                    data    : 'vid='+target_tmp,
                    method  : 'post',
                    dataType: 'json',
                    success: function(r){
                        if(r.status){
                            _this.removeAttr('disabled').find('.fa-spinner').remove();
                            $('#modal-vote-delete').modal('hide');
                            searchPage (1);
                        }
                    }
                });
            });

            function searchPage (page) {
                var data = $('#search-form').serialize()+'&page='+page;
                $('#panel-data-list').css('opacity','0.6');
                $.ajax({
                    url     : $('#root-url').val()+"/admin/survey",
                    data    : data,
                    dataType: "html",
                    method: 'post',
                    success: function (h) {
                        $('#panel-data-list').html(h);
                        $('#panel-data-list').css('opacity','1');
                        $('[data-toggle="tooltip"]').tooltip();
                        cbr_replace();
                    }
                })
            }
        })
    </script>
    <link rel="stylesheet" href="{{ url('/') }}/js/select2/select2.css">
    <link rel="stylesheet" href="{{ url('/') }}/js/select2/select2-bootstrap.css">
    <style>
        #b-label-no {
            font-weight: bold;
        }
    </style>
@endsection
