@extends('layouts.base-admin')
@section('content')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">{{ trans('messages.Vote.page_head') }}</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1" >
                <li>
                    <a href="{{ url('/') }}"><i class="fa-home"></i>{{ trans('messages.page_home') }}</a>
                </li>
                <li class="active">
                    <strong>{{ trans('messages.Vote.page_head') }}</strong>
                </li>
            </ol>
        </div>
    </div>

    <section class="bills-env">

        <ul class="nav nav-tabs">
            <li class="active"> <a href="#th" data-toggle="tab">ภาษาไทย</a> </li>
            <li> <a href="#en" data-toggle="tab">English</a> </li>
        </ul>

        <div class="panel panel-default">
            <div class="panel-body">


                <div class="row">
                    <div class="col-sm-4">
                        @if( !$vote->banner_url )

                            <div class="default-banner">
                                Survey
                            </div>
                        @else
                            <img src="{{ env('URL_S3')."/vote-file".$vote->banner_url }}"  width="100%">
                        @endif
                    </div>
                    <div class="col-sm-8 tab-content">

                        <div class="tab-pane active" id="th">
                            <h2 class="no-top-margin">{{ $vote->title_th }}</h2>
                            <div style="margin-top: 10px;" >{!!  $vote->description_th  !!}</div>
                        </div>

                        <div class="tab-pane" id="en">
                            <h2 class="no-top-margin">{{ $vote->title_en }}</h2>
                            <div style="margin-top: 10px;" >{!!  $vote->description_en  !!}</div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-3"><b>{{ trans('messages.Vote.time') }}</b></div>
                            <div class="col-md-9">
                                <p>{!! trans('messages.Vote.open_at',['date' =>  localDate($vote->start_date),'time'=>date('H:i',strtotime($vote->start_time))]) !!} </p>
                                <p>{!! trans('messages.Vote.close_at',['date' => localDate($vote->end_date),'time'=>date('H:i',strtotime($vote->end_time))]) !!} </p>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-3"><b>{{ trans('messages.status') }}</b></div>
                            <div class="col-sm-9">
                            @if( laterThanNow($vote->end_date,$vote->end_time) )
                                <span class="label label-default">{{ trans('messages.Vote.overdue') }}</span>
                            @else
                                <span class="label label-danger">{{ trans('messages.new') }}</span>
                            @endif
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-3"><b>{{ trans('messages.Post.publish_status') }} :</b></div>
                            <div class="col-sm-9">
                                @if( $vote->publish_status )
                                    {{ trans('messages.Post.publish') }}
                                @else
                                    {{ trans('messages.Post.draft') }}
                                @endif
                            </div>
                        </div>

                        @if($vote->voteFile->count() > 0)
                            <hr/>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-3">
                                    <b>{{trans('messages.attachment')}}</b>
                                </div>
                                <div class="col-sm-9">
                                    <ul class="list-unstyled list-inline">
                                        @foreach($vote->voteFile as $file)
                                            <li>
                                                @if($file->is_image)
                                                    <a href="{{ env('URL_S3')."/vote-file/".$file->url.$file->name }}" class="thumb gallery" rel="gal-1">
                                                        <img width="145px" src="{{ env('URL_S3')."/vote-file/".$file->url.$file->name }}" class="img-thumbnail" />
                                                    </a>
                                                @else
                                                    <div>
                                                        <a target="_blank" href="{{ url("/admin/posts/get-attachment/".$file->id) }}" class="thumb">
                                                            <img src="{{ url('/') }}/images/file.png" class="img-thumbnail" />
                                                        </a>
                                                        <div class="file-name">{{ $file->original_name }}</div>
                                                    </div>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <hr/>
                        @if(laterThanNow($vote->end_date,$vote->end_time))
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12"><b>{{ trans('messages.Vote.result') }}</b></div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <div id="bar-5" style="height: 200px; width: 100%;"></div>
                            </div>
                        </div>
                        <hr/>
                        @endif


                        <div class="row form-group">
                            <div class="col-md-12">
                                @if($vote && $vote->created_user)
                                    <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                                        <li class="field-hint"><b>{{ trans('messages.created_by') }}</b> {{ $vote->created_user->name }}</li>
                                        <li class="field-hint" ><b>{{ trans('messages.created_at') }}</b> {{ showDateTime($vote->created_at) }}</li>
                                    </ul>
                                @endif
                                @if($vote && $vote->updated_user)
                                    <ul class="list-unstyled list-inline form-action-buttons text-right">
                                        <li class="field-hint" ><b>{{ trans('messages.updated_by') }}</b> {{ $vote->updated_user->name }}</li>
                                        <li class="field-hint" ><b>{{ trans('messages.updated_at') }}</b> {{ showDateTime($vote->updated_at) }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="panel-footer" style="margin-top: 35px; background: #fff; padding-top: 29px;">
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <a class="btn btn-gray" href="{{url('admin/survey')}}">{{trans('messages.back')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/devexpress-web-14.1/js/globalize.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/devexpress-web-14.1/js/dx.chartjs.js"></script>
    <script>
        $(function () {

            var dataSource = [
                    @foreach ($vote->voteChoice->reverse() as $choice)
                { choice: "{{$choice->title}}", count: {{ (!$choice->choice_count)?"0":$choice->choice_count }} },
                @endforeach
            ];

            $(".gallery").fancybox({
                helpers: {
                    overlay: {
                        locked: false
                    }
                },
                beforeLoad: function(){
                    $('html').css('overflow','hidden');
                },
                afterClose: function(){
                    $('html').css('overflow','auto');
                },
                'padding'           : 0,
                'transitionIn'      : 'elastic',
                'transitionOut'     : 'elastic',
                'changeFade'        : 0
            });

            $("#bar-5").dxChart({
                rotated: true,
                pointSelectionMode: "multiple",
                dataSource: dataSource,
                commonSeriesSettings: {
                    argumentField: "choice",
                    type: "stackedbar",
                    selectionStyle: {
                        hatching: {
                            direction: "left"
                        }
                    }
                },
                series: [
                        @if(Auth::user()->role == 2)
                    { valueField: "count", color: "#16a085" }
                        @else
                    { valueField: "count", color: "#2E7DA2" }
                    @endif
                ],
                title: false,
                legend: {
                    visible:false
                },
                pointClick: function(point) {
                    point.isSelected() ? point.clearSelection() : point.select();
                }
            });

            $('#show-vote').on('click', function (){
                $('.panel-body').toggle();
                $('#bar-5').dxChart('instance').render();
            })

        });
    </script>
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{url('/')}}/collagePlus/effect.css" type="text/css" media="screen" />

    <style>
        ul, ol {
            margin-top: inherit;
        }
    </style>
@endsection
