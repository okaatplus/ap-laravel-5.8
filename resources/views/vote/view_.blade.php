@extends('layouts.base-admin')
@section('content')
<section class="mailbox-env">
        <div class="row">
            <!-- Email Single -->
            <div class="col-sm-12">
                <div class="mail-single">
                    <!-- Email Title and Button Options -->
                    <div class="mail-single-header">
                        <h2>
                            {{ $vote->title_th }}
                            @if( laterThanNow($vote->end_date,$vote->end_time) )
                                @if( !$vote->userChoose )
                                    <span class="label label-default">{{ trans('messages.Vote.overdue') }}</span>
                                @else
                                    <span class="label label-info">{{ trans('messages.Vote.voted') }}</span>
                                @endif
                            @else
                                @if(!$vote->userChoose)
                                    <span class="label label-danger">{{ trans('messages.new') }}</span>
                                @else
                                    <span class="label label-info">{{ trans('messages.Vote.voted') }}</span>
                                @endif
                            @endif
                            <a href="{{ URL::previous() }}" class="go-back">
                                <i class="fa-angle-left"></i>
                                {{ trans('messages.back') }}
                            </a>
                        </h2>
                    </div>
                    <!-- Email Info From/Reply -->
                    <div class="mail-single-info">

                        <div class="mail-single-info-user dropdown">
                            <div class="dropdown-toggle" data-toggle="dropdown">
                                @if($vote->creator->profile_pic_name)
                                <img src="{{ env('URL_S3') . "/profile-img/" . $vote->creator->profile_pic_path . $vote->creator->profile_pic_name }}" alt="user-image" class="img-circle" width="38" />
                                @else
                                <img src="{{  url('/')  }}/images/user-3.png" class="img-circle" width="38" />
                                @endif
                                <span>{{ $vote->creator->name }}</span>
                                <em class="time">
                                    {{ localDate($vote->created_at) }}
                                </em>
                            </div>
                        </div>

                        <div class="mail-single-info-options">
                            @if($vote->creator->id == Auth::user()->id)
                            <a class="btn btn-danger link-delete-vote" href="#" data-toggle="modal" data-target="#modal-vote-delete" data-vote-id="{{$vote->id}}">
                                <i class="fa-trash"></i> {{ trans('messages.Vote.delete') }}
                            </a>
                            @endif
                        </div>
                    </div>
                    <!-- Email Body -->
                    <div class="mail-single-body">
                        <p>
                            <?php echo nl2br(e($vote->description)) ?>
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="no-margin">
                                <i class="fa fa-clock-o"></i> {{ trans('messages.Vote.time') }}
                            </h3><br/>
                            <p>{!! trans('messages.Vote.open_at',['date' =>  localDate($vote->start_date),'time'=>date('H:i',strtotime($vote->start_time))]) !!} </p>
                            <p>{!! trans('messages.Vote.close_at',['date' => localDate($vote->end_date),'time'=>date('H:i',strtotime($vote->end_time))]) !!} </p>
                        </div>
                    </div>
                    <hr/>

                    <!-- Email Attachments -->
                    <div class="mail-single-attachments">
                        @if(!$vote->voteFile->isEmpty())
                            <h3>
                                <i class="linecons-attach"></i> {{ trans('messages.attachment') }}
                            </h3>
                            <ul class="list-unstyled list-inline">
                                @foreach($vote->voteFile as $file)
                                <li>
                                    @if($file->is_image)
                                    <a href="{{env('URL_S3')}}/vote-file/{{$file->url.$file->name}}" class="thumb gallery" rel="gal-1">
                                        <img width="145px" src="{{env('URL_S3')}}/vote-file/{{$file->url.$file->name}}" class="img-thumbnail" />
                                    </a>
                                    @else
                                    <div>
                                        <a target="_blank" href="{{ url("admin/survey/get-attachment/".$file->id) }}" class="thumb">
                                            <img src="{{ url('/') }}/images/file.png" class="img-thumbnail" />
                                        </a>
                                        <div class="file-name">{{ $file->original_name }}</div>
                                    </div>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        @endif
                        <?php $allowVote = (greaterThanNow($vote->end_date,$vote->end_time) && laterThanNow($vote->start_date,$vote->start_time)); ?>
                        @if( $allowVote )
                            <div class="text-right">
                                @if( !$vote->userChoose )
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal-vote" type="button"> {{ trans('messages.Vote.vote') }}</button>
                                @endif
                                @if($vote->userChoose && greaterThanNow($vote->end_date,$vote->end_time) )
                                    <button id="show-vote" type="button" class="btn btn-primary"> {{ trans('messages.Vote.show_result') }}</button>
                                @endif
                            </div>
                                @if( $vote->userChoose )
                                <h3 class="no-margin">
                                    <i class="fa fa-thumbs-o-up"></i> {{ trans('messages.Vote.result') }}
                                </h3>
                                @endif
                        @endif
                        </div>
                        <div class="panel-body" @if(greaterThanNow($vote->end_date,$vote->end_time)) @if(!$vote->userChoose) style="display:none;" @endif @endif>
                            <div id="bar-5" style="height: 200px; width: 100%;"></div>
                        </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if($vote && $vote->created_user)
                                <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                                    <li class="field-hint"><b>{{ trans('messages.created_by') }}</b> {{ $vote->created_user->name }}</li>
                                    <li class="field-hint" ><b>{{ trans('messages.created_at') }}</b> {{ showDateTime($vote->created_at) }}</li>
                                </ul>
                            @endif
                            @if($vote && $vote->updated_user)
                                <ul class="list-unstyled list-inline text-right">
                                    <li class="field-hint" ><b>{{ trans('messages.updated_by') }}</b> {{ $vote->updated_user->name }}</li>
                                    <li class="field-hint" ><b>{{ trans('messages.updated_at') }}</b> {{ showDateTime($vote->updated_at) }}</li>
                                </ul>
                            @endif
                        </div>
                    </div>

                    </div>
                </div>
        </div>

    </section>
    <div class="modal fade" id="modal-vote-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa-trash"></i> {{ trans('messages.Vote.confirm_delete_head') }}</h4>
                </div>
                <div class="modal-body"> {{ trans('messages.Vote.confirm_delete_msg') }} </div>
                <div class="modal-footer">
                    <button type="botton" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                    <button type="botton" id="confirm-delete-vote" class="btn btn-primary">{{ trans('messages.delete') }}</button>
                </div>
            </div>
         </div>
    </div>

    @if($allowVote && !$vote->userChoose)
    <div class="modal fade" id="modal-vote">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{url('votes/vote')}}" method="post" id="vote-form" class="form-horizontal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">{{ $vote->title }}</h4>
                    </div>
                    <div class="modal-body  text-center">
                        <input type="hidden" name="vote_id" value="{{$vote->id}}"/>
                        @foreach ($vote->voteChoice as $choice)
                        <label>
                            <input type="radio" name="vote_choice" value="{{$choice->id}}">
                            {{ $choice->title }}
                        </label><br/>
                        @endforeach
                        <span class="choice-error error" style="display:none;">{{ trans('messages.Vote.vote_validate') }}</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                        <button type="button" id="vote-btn" class="btn btn-primary">{{ trans('messages.Vote.vote') }}</button>
                    </div>
                </form>
            </div>
         </div>
    </div>
    @endif
@endsection
@section('script')
 <!-- Imported scripts on this page -->
    <script type="text/javascript" src="{{ url('/') }}/js/devexpress-web-14.1/js/globalize.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/devexpress-web-14.1/js/dx.chartjs.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.mousewheel.pack.js"></script>
    <script type="text/javascript" src="{{url('/')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="{{ url('/') }}/js/jquery-validate/jquery.validate.min.js"></script>
    <script type="text/javascript">
        var target_temp;
        jQuery(document).ready(function($) {
            $('#vote-btn').on('click',function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                $(this).parents('form').submit();
            })
            validator = $("#vote-form").validate({
                   rules: {
                        vote_choice       : "required",
                    },
                    highlight: function(element, errorClass, validClass) { },
                    errorPlacement: function(error, element) {
                        $('.choice-error').show();
                    },
                    success: function () {
                        $('.choice-error').hide();
                    }
                });


            var dataSource = [
            @foreach ($vote->voteChoice->reverse() as $choice)
                { choice: "{{$choice->title}}", count: {{ (!$choice->choice_count)?"0":$choice->choice_count }} },
            @endforeach
            ];
            $("#bar-5").dxChart({
                rotated: true,
                pointSelectionMode: "multiple",
                dataSource: dataSource,
                commonSeriesSettings: {
                    argumentField: "choice",
                    type: "stackedbar",
                    selectionStyle: {
                        hatching: {
                            direction: "left"
                        }
                    }
                },
                series: [
                    @if(Auth::user()->role == 2)
                    { valueField: "count", color: "#16a085" }
                    @else
                    { valueField: "count", color: "#2E7DA2" }
                    @endif
                ],
                title: false,
                legend: {
                    visible:false
                },
                pointClick: function(point) {
                    point.isSelected() ? point.clearSelection() : point.select();
                }
            });

            $('#show-vote').on('click', function (){
                $('.panel-body').toggle();
                $('#bar-5').dxChart('instance').render();
            })

            $(".gallery").fancybox({
                helpers: {
                    overlay: {
                      locked: false
                    }
                },
                beforeLoad: function(){
                        $('html').css('overflow','hidden');
                },
                afterClose: function(){
                   $('html').css('overflow','auto');
                },
                'padding'           : 0,
                'transitionIn'      : 'elastic',
                'transitionOut'     : 'elastic',
                'changeFade'        : 0
            });

            $('.link-delete-vote').on('click',function () {
                target_temp = $(this).data('vote-id');
            });

            $('#confirm-delete-vote').on('click',function () {
                $(this).attr('disabled','disabled').prepend('<i class="fa-spin fa-spinner"></i> ');
                window.location.href = $('#root-url').val()+"admin/survey/delete/"+target_temp;
            })
        });
    </script>

    <link rel="stylesheet" href="{{url('/')}}/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
@endsection
