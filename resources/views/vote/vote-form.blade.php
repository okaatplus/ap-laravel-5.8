    <div class="form-group">
        {!! Form::text('title', null, ["class" => "form-control", "placeholder" => trans('messages.title'), "maxlength" => "200"]) !!}
    </div>

    <div class="row form-group">
        <div class="col-md-12">
            <div id="attachment-edit-banner" class="drop-property-file dz-clickable" style="margin-bottom: 0; @if($vote->banner_url) display: none; @endif">
                <i class="fa fa-camera"></i> {{ trans('messages.insertion_img') }}
            </div>
            <div class="post-banner" id="previews-edit-img-banner">
                @if($vote->banner_url != null)
                    <div class="preview-img-item">
                        <img src="{{ env('URL_S3')."/vote-file".$vote->banner_url }}" width="100%" alt="post-image" />
                        <span class="remove-img-preview remove-banner">×</span>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <input type="hidden" name="remove-banner-flag" id="remove-banner-flag">

    <div class="compose-message-editor">
        {!! Form::textarea('description',null,["class" => "form-control wysihtml5", "data-html" => "false", "data-color" => "false", "data-stylesheet-url" =>  url('/') ."/css/other/wysihtml5-color.css", "placeholder" => trans('messages.detail'), "maxlength" => "2000"]) !!}
    </div>
    <div class="form-group-separator-noline"></div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label text-bold">{{ trans('messages.Event.start_date') }}</label>
                <div class="date-and-time">
                    <?php
                        if($vote->start_date) $sd = date('Y/m/d',strtotime($vote->start_date));
                        else $sd = date('Y/m/d');
                    ?>
                    {!! Form::text('start_date', $sd, ["class" => "form-control datepicker", "readonly", "data-start-date" => "0d", "data-format" => "yyyy/mm/dd",'data-language'=>App::getLocale()]) !!}

                    <?php
                        if($vote->start_time) $st = date('h:i A',strtotime($vote->start_time));
                        else $st = date('h:i A');
                    ?>
                    {!! Form::text('start_time', $st, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5"]) !!}
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label text-bold">{{ trans('messages.Event.end_date') }}</label>
                <div class="date-and-time">

                    <?php
                        if($vote->end_date) $ed = date('Y/m/d',strtotime($vote->end_date));
                        else $ed = date('Y/m/d');
                    ?>
                    {!! Form::text('end_date', $ed, ["class" => "form-control datepicker", "readonly", "data-start-date" => "0d", "data-format" => "yyyy/mm/dd",'data-language'=>App::getLocale()]) !!}

                    <?php
                        if($vote->end_time) $et = date('h:i A',strtotime($vote->end_time));
                        else $et = date('h:i A');
                    ?>
                    {!! Form::text('end_time', $et, ["class" => "form-control timepicker", "readonly", "data-template" => "dropdown", "data-show-meridian" => "true", "data-minute-step" => "1", "data-second-step" => "5"]) !!}
                </div>
            </div>

        </div>
    </div>
    <div class="form-group-separator-noline"></div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group-separator"></div>
            <label class="control-label text-bold">{{ trans('messages.Vote.choice') }}</label>
            <div class="form-group-separator-noline"></div>
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control" name="choice_title" id="input-choice-edit" tabindex="1" placeholder="{{ trans('messages.Vote.add_choice') }}" maxlength="100" />
                </div>
                <div class="form-group">
                    <input type="button" class="form-control btn btn-white btn-single" id="add-choice-edit" value="{{ trans('messages.Vote.add') }}">
                </div>
            </div>
            <div class="form-group-separator-noline"></div>
            <div class="list-item choice-item-edit">
                @foreach( $vote->voteChoice as $choice)
                <div class="alert alert-white alert-choice vote-choice">
                    <a data-dismiss="alert" class="close remove-element" data-e-type="choice" data-e-id="{{ $choice->id }}">
                        <span aria-hidden="true">×</span>
                    </a>
                    {{ $choice->title }}
                </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group-separator"></div>
            <label class="control-label text-bold">{{ trans('messages.attachment') }}</label>
            <div class="form-group-separator-noline"></div>
            <span id="attachment_edit"><i class="fa fa-camera"></i> {{ trans('messages.attach') }}</span>
            <div class="field-hint">{{ trans('messages.upload_file_description') }}</div>
            <div id="previews-edit-form">
               @foreach($vote->voteFile as $file)
               <div class="preview-img-item">
                    @if($file->is_image)
                     <img width="80px" src="{{env('URL_S3')}}/vote-file/{{$file->url.$file->name}}" />
                    @else
                    <img width="80px" src="{{ url('/') }}/images/file.png" class="img-thumbnail" />
                    <span class="file-label">{{ $file->original_name }}</span>
                    @endif
                    <span class="remove-img-preview remove-element" data-e-type="vote-file" data-e-id="{{ $file->id }}">×</span>
                </div>
               @endforeach
            </div>
        </div>
    </div>
    <div class="row form-group" style="margin-top: 25px;">
        <label class="control-label col-md-12 text-bold">{{ trans('messages.Post.publish_status') }}</label>
        <div class="col-md-12">
            <select class="form-control" name="publish_status">
                <option value="0">{{ trans('messages.Post.draft') }}</option>
                <option value="1" @if($vote->publish_status) selected @endif>{{ trans('messages.Post.publish') }}</option>
            </select>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-12">
            @if($vote && $vote->created_user)
                <ul class="list-unstyled list-inline text-right" style="margin-bottom: 0;">
                    <li class="field-hint"><b>{{ trans('messages.created_by') }}</b> {{ $vote->created_user->name }}</li>
                    <li class="field-hint" ><b>{{ trans('messages.created_at') }}</b> {{ showDateTime($vote->created_at) }}</li>
                </ul>
            @endif
            @if($vote && $vote->updated_user)
                <ul class="list-unstyled list-inline text-right">
                    <li class="field-hint" ><b>{{ trans('messages.updated_by') }}</b> {{ $vote->updated_user->name }}</li>
                    <li class="field-hint" ><b>{{ trans('messages.updated_at') }}</b> {{ showDateTime($vote->updated_at) }}</li>
                </ul>
            @endif
        </div>
    </div>