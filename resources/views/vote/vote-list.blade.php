 @if($votes->count() > 0)
 <?php
    $from   = (($votes->currentPage()-1)*$votes->perPage())+1;
    $to     = (($votes->currentPage()-1)*$votes->perPage())+$votes->perPage();
    $to     = ($to > $votes->total()) ? $votes->total() : $to;
 ?>

 <table class="table mail-table">
      <thead>
     <tr>
         <th class="col-header-options">
             <div class="mail-pagination admin-discussion-pagination">
                 {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$votes->total()]) !!}
                 <div class="next-prev">
                    <a href="#" class="prev-page @if($votes->currentPage() == 1) disabled @endif" data-page="{{($votes->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                     <a href="#" class="next-page @if(!$votes->hasMorePages()) disabled @endif" data-page="{{($votes->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                 </div>
             </div>
         </th>
     </tr>
     </thead>
 </table>
@foreach($votes as $vote)
 <div class="story-row">
     @if($vote->creator->profile_pic_name)
     <img src="{{ env('URL_S3')."/profile-img/".$vote->creator->profile_pic_path.$vote->creator->profile_pic_name }}" alt="user-image" class="img-circle s-profile-img"/>
     @else
     <img src="{{url('/')}}/images/user-4.png" alt="user-img" class="img-circle s-profile-img" />
     @endif
     <div class="row">
         <div class="col-sm-7">
             <div class="t-name">
                 <a href="{{ url('votes/view/'.$vote->id) }}" @if($vote->userChoose) class="mark-as-read" @endif data-target="modal-votefication-detail" data-vote-id="{{$vote->id}}"><b>{{$vote->title}}</b></a>
             </div>
         </div>
         <div class="col-sm-3">
             @if( laterThanNow($vote->end_date,$vote->end_time) )
                 @if( !$vote->userChoose )
                     <span class="label label-default">{{ trans('messages.Vote.overdue') }}</span>
                 @else
                     <span class="label label-info">{{ trans('messages.Vote.voted') }}</span>
                 @endif
             @else
                 @if(!$vote->userChoose)
                     <span class="label label-danger">{{ trans('messages.new') }}</span>
                 @else
                     <span class="label label-info">{{ trans('messages.Vote.voted') }}</span>
                 @endif
             @endif
        </div>
        <div class="col-sm-2 text-right shrink-up">
            <b>{{ localDate($vote->created_at) }}</b>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="t-owner-name">{{ $vote->creator->name }}</div>
        </div>
        <div class="col-sm-8 text-right t-action shrink-up">
            <div class="t-owner-name">
                @if($vote->creator->id == Auth::user()->id && greaterThanNow ($vote->start_date,$vote->start_time) )
                <a class="link-edit-vote" href="#" data-vote-id="{{$vote->id}}"><i class="fa-pencil"></i></a>
                @endif
                @if($vote->creator->id == Auth::user()->id)
                <a class="link-delete-vote" href="#" data-toggle="modal" data-target="#modal-vote-delete" data-vote-id="{{$vote->id}}"><i class="fa-trash"></i></a>
                @else
                <a class="vote-report" data-toggle="tooltip" data-placement="left" data-id="{{$vote->id}}" title="" data-original-title="{{ trans('messages.Post.report')}}"><i class="fa-bullhorn "></i></a>
                @endif
            </div>
        </div>
    </div>
</div>
@endforeach
<table class="table mail-table">
         <!-- mail table footer -->
        <thead>
        <tr>
            <th class="col-header-options" style="border-bottom:0px;">
                <div class="mail-pagination">
                    {!! trans('messages.showing',['from'=>$from,'to'=>$to,'total'=>$votes->total()]) !!}
                    <div class="next-prev">
                        <a href="#" class="prev-page @if($votes->currentPage() == 1) disabled @endif" data-page="{{($votes->currentPage())-1}}"><i class="fa-angle-left"></i></a>
                        <a href="#" class="next-page @if(!$votes->hasMorePages()) disabled @endif" data-page="{{($votes->currentPage())+1}}"><i class="fa-angle-right"></i></a>
                    </div>
                </div>
            </th>
        </tr>
        </thead>
    </table>
@else
<div class="col-sm-12 text-center">{{ trans('messages.Vote.no_vote_found') }}</div><div class="clearfix"></div>
@endif
