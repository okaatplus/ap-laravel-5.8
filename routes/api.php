<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::get('testapi', 'API\RegisterController@testapi');

Route::post('check-email', 'API\RegisterController@checkEmail');
Route::post('register', 'API\RegisterController@register');
Route::post('register-set-passcode', 'API\RegisterController@registerAndPasscode');
Route::post('request-otp', 'API\RegisterController@otp');
Route::post('verify-otp', 'API\RegisterController@verifyOTP');

Route::get('term', 'API\RegisterController@getTermCondition');
Route::get('privacy', 'API\RegisterController@getPrivacy');
//Test APITEL SMS
Route::get('test-sms', 'API\RegisterController@getTermCondition');

Route::post('update-installation', 'API\SettingController@updateInstall');
//Route::post('update-passcode', 'API\SettingController@updatePasscode');

// Add Property
Route::get('list-property-type', 'API\RegisterController@listPropertyType');
Route::post('list-brand', 'API\RegisterController@listBrand');
Route::post('list-property', 'API\RegisterController@listProperty');
Route::post('add-property', 'API\RegisterController@AddProperty');

Route::post('auth', 'AuthenticateController@authenticate');
Route::post('auth-mdm', 'AuthenticateController@authenticateMdm');
Route::post('auth-passcode', 'AuthenticateController@authenticateByPasscode');
Route::post('forget-passcode', 'AuthenticateController@forgetPasscode');
Route::post('logout', 'AuthenticateController@logoutApi');

// Forget Password
Route::post('forgot', 'Auth\PasswordAPIController@sendResetLinkEmail');

// AIS Call Back
Route::get('ais', 'API\AISPlayCallbackController@callbackFunction');

Route::group(['middleware' => ['before' => 'jwt.auth']], function() {

    Route::post('check-id-card', 'API\RegisterController@checkIdCard');

    Route::post('check-setting-passcode', 'API\SettingController@checkPasscodeAlready');
    Route::post('update-passcode', 'API\SettingController@updatePasscode');
    Route::post('update-installation', 'API\SettingController@updateInstall');
    Route::post('customer-check', 'API\SettingController@customerCheck');
    Route::post('request-add-property', 'API\SettingController@requestAddProperty');

    // User Profile
    Route::get('profile-view', 'API\SettingController@profileView');
    Route::post('setting/user-profile-image-upload', 'API\SettingController@profileImageUpload');
    Route::post('setting/profile-update', 'API\SettingController@profileUpdate');
    Route::post('setting/language', 'API\SettingController@language');
    Route::post('setting/phone-update', 'API\SettingController@phoneUpdate');
    Route::post('setting/notification', 'API\SettingController@notificationSetting');

    // Feature
    Route::get('feature-setting', 'API\CustomerDashboardController@featureProperty');
    Route::get('feature-ais-play', 'API\CustomerDashboardController@featurePropertyAISPlay');

    // Notification
    Route::get('notification-list', 'API\NotificationController@list');
    Route::post('notification-list-by-property', 'API\NotificationController@listByProperty');
    Route::post('notification/mark-as-read', 'API\NotificationController@markAsRead');

    // Dashboard
    Route::get('dashboard/all', 'API\CustomerDashboardController@allPropertyDashboard');
    Route::post('dashboard/owner', 'API\CustomerDashboardController@ownerDashboard');
    Route::post('dashboard/resident', 'API\CustomerDashboardController@residentDashboard');
    Route::post('dashboard/tenant', 'API\CustomerDashboardController@residentDashboard');
    Route::get('get-ordinary-dashboard', 'API\NewsController@ordinaryDashboard'); // ordinary dashboard = news&announcement page

    // News Feeds
    Route::post('get-news-announcement', 'API\NewsController@customerNewsAnnouncement'); // news&announcement page
    Route::post('news/view','API\NewsController@details'); // View details
    Route::post('news/pin','API\NewsController@postPin'); // pin news
    Route::post('news/unpin','API\NewsController@postUnpin'); // unpin news
    Route::post('news/search','API\NewsController@searchNewsAnnouncement'); // search news

    // Personal Message
    Route::get('message-index', 'API\MessageController@messageIndex');
    Route::post('message', 'API\MessageController@message');
    Route::post('message-all', 'API\MessageController@listAllMessage');
    Route::get('message-flag', 'API\MessageController@checkHaveNewMessage');
    Route::get('message-new-counter', 'API\MessageController@counterNewMessage');
    Route::post('message/add', 'API\MessageController@sendMessage');
    Route::post('message/attach', 'API\MessageController@attachImage');

    // User Management
    Route::post('waiting-approve', 'API\UserManagementController@listWaitingApproveUser');
    Route::post('list-resident', 'API\UserManagementController@listResidentUser');
    Route::post('approve-resident', 'API\UserManagementController@approveResident');
    Route::post('set-expire-resident', 'API\UserManagementController@expireDateResidentUser');
    Route::post('set-active-resident', 'API\UserManagementController@setActiveResidentUser');

    // Post Parcel
    Route::post('new-parcel-count', 'API\PostParcelController@countNewPostParcel');
    Route::post('new-parcel-list', 'API\PostParcelController@newPostParcelList');
    Route::post('history-parcel-list', 'API\PostParcelController@historyPostParcelList');
    Route::post('parcel-detail', 'API\PostParcelController@postParcelDetail');

    // Vote/Survey
    Route::post('votes', 'API\VoteController@index');
    Route::post('votes/add', 'API\VoteController@add');
    Route::post('votes/edit', 'API\VoteController@edit');
    Route::get('votes/view/{id}', 'API\VoteController@view');
    Route::post('votes/add-file-vote', 'API\VoteController@addFileVote');
    Route::post('votes/vote', 'API\VoteController@vote');
    Route::post('votes/delete', 'API\VoteController@delete');
    Route::post('votes/delete-file-vote', 'API\VoteController@deleteFile');

    // Complain
    Route::get('complain', 'API\ComplainController@complain');
    Route::post('complain-list-all', 'API\ComplainController@complainListAll');
    Route::post('complain/add', 'API\ComplainController@add');
    Route::post('complain/add-file-complain', 'API\ComplainController@addFileComplain');
    Route::post('complain/comment/add', 'API\ComplainController@addComment');
    Route::post('complain/change-status', 'API\ComplainController@chiefChangeStatus');
    Route::post('complain/confirm', 'API\ComplainController@userConfirm');
    Route::get('complain/detail/{id}', 'API\ComplainController@detail');
    Route::post('complain/get-attachment', 'API\ComplainController@getAttach');

    // AIS Play
    Route::post('ais-check-activate', 'API\AISPlayController@aisActivate');

    ///// For sale force API ///////
    Route::get('property/list','apAPIController@apWorldProperty');
    Route::get('property/unit/list','apAPIController@apWorldPropertyUnit');
    Route::get('customer/list','apAPIController@apWorldCustomer');
    Route::get('customer/property/list','apAPIController@apWorldCustomerProperty');
    Route::get('tenant/list','apAPIController@apWorldTenant');
    Route::get('tenant/property/list','apAPIController@apWorldTenantProperty');
});

// AIS Package encode url
Route::get('ais-encode', 'API\AISPlayController@aisEncode');

// AIS Apply Package encode url
Route::post('ais-package-encode', 'API\AISPlayController@aisApplyEncode');

// Test Pusher
Route::get('pusher', 'API\SettingController@sendPusher');
Route::get('test-push', 'API\NotificationController@testPushNotification');
Route::get('test-pusher', 'API\MessageController@testPusher');
Route::get('test-pusher-chat', 'API\MessageController@testPusherChat');


/*Route::group(['prefix' => 'api'], function() {


    Route::group(['middleware' => ['before' => 'jwt.auth']], function() {

    });

    Route::group(['prefix' => 'marketing'], function() {
        Route::get('recent/all', 'API\MarketingController@recent');

    });
});*/