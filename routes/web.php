<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('auth/logout', 'Auth\LoginController@logout');
Route::get('/auth/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('setLang', 'HomeController@lang');
Route::get('terms', function () {
    return view('home.terms');
});
Route::get('policy', function () {
    return view('home.policy');
});

Route::get('terms/{locale}', function ($locale) {
    App::setLocale($locale);
    return view('home.terms');
});
Route::get('policy/{locale}', function ($locale) {
    App::setLocale($locale);
    return view('home.policy');
});
//File route...
Route::any('upload-file', 'FileController@upload');
Route::any('upload-profile-pic', 'FileController@uploadProfileImg');

// get property by dimension
Route::get('get/property/dimension/{type}', 'AboutPropertyController@getByDimension');


Route::get('/', 'HomeController@login');
// Password reset link request routes...
Route::get('reset/password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
Route::post('reset/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::post('reset/password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\PasswordController@postReset')->name('password.update');
Route::get('password/reset-done', function () {
    return view('auth.passwords.reset-done');
});

Route::any('notification', 'User\NotificationController@index');
Route::post('notification/get', 'User\NotificationController@get');
Route::post('notification/markasread', 'User\NotificationController@markAsRead');
Route::post('notification/page', 'User\NotificationController@notiPage');
Route::post('notification/last', 'User\NotificationController@notiLast');

// User Profile
Route::any('settings', 'User\SettingsController@index');
Route::any('settings/password', 'User\SettingsController@password');
Route::any('settings/notification', 'User\SettingsController@notification');
Route::any('settings/home', 'User\SettingsController@home');
Route::any('settings/language', 'User\SettingsController@language');

// Management user direct login
Route::get('admin/property/directlogin/{id}', 'PropertySessionController@directLogin');
Route::get('admin/property/restoresession', 'PropertySessionController@restoreSession');

/// Manager property
// Admin
Route::any('root/admin/admin-system/list', 'RootAdmin\AdminSystemController@adminList');
Route::post('root/admin/admin-system/add', 'RootAdmin\AdminSystemController@addAdmin');
Route::post('root/admin/admin-system/view', 'RootAdmin\AdminSystemController@viewAdmin');
Route::post('root/admin/admin-system/edit/get', 'RootAdmin\AdminSystemController@getAdmin');
Route::post('root/admin/admin-system/edit', 'RootAdmin\AdminSystemController@editAdmin');
Route::any('root/admin/admin-system/active', 'RootAdmin\AdminSystemController@setActive');
Route::post('root/admin/admin-system/delete', 'RootAdmin\AdminSystemController@deleteAdmin');
//////////////
Route::any('root/admin/property/list', 'RootAdmin\PropertyController@index');
Route::any('root/admin/property/edit/{id}', 'TopManager\PropertyController@edit');
Route::any('root/admin/property/view/{id}', 'TopManager\PropertyController@view');
Route::any('root/admin/property/add', 'TopManager\PropertyController@add');
Route::any('root/admin/property/status', 'TopManager\PropertyController@status');
Route::post('root/admin/property/addunit', 'TopManager\PropertyController@createUnitCsv');
Route::post('root/admin/property/update-unit', 'TopManager\PropertyController@addUnitCsvAfter');
Route::post('root/admin/property/edit-data', 'TopManager\PropertyController@updatePropertyUnitCsv');
Route::post('root/admin/property/unit/check-data', 'TopManager\PropertyUnitController@checkUnitData');
Route::post('root/admin/property/unit/start-import', 'TopManager\PropertyUnitController@startImportUnitData');
Route::post('root/admin/property/customer/check-data', 'TopManager\CustomerController@checkCustomerData');
Route::post('root/admin/property/customer/start-import', 'TopManager\CustomerController@startImportCustomerData');
Route::post('root/admin/select/district', 'TopManager\PropertyController@selectDistrict');
Route::post('root/admin/select/subdistrict','TopManager\PropertyController@Subdistrict');
Route::post('root/admin/select/zip_code','TopManager\PropertyController@zip_code');
Route::post('root/admin/select/district/edit','TopManager\PropertyController@selectDistrictEdit');
Route::post('root/admin/select/editSubDis','TopManager\PropertyController@editSubDis');
Route::post('root/admin/property/reset', 'TopManager\PropertyController@resetData');
Route::post('root/admin/property/unit/check-update-data', 'RootAdmin\UpdatePropertyUnitController@checkUpdateUnitData');
Route::post('root/admin/property/unit/start-update', 'RootAdmin\UpdatePropertyUnitController@startUpdateUnitData');

//Terms and Conditions
Route::get('root/admin/page/{alias}','RootAdmin\PagesController@editPage');
Route::post('root/admin/page/edit','RootAdmin\PagesController@edit');

//Property Brand
Route::any('root/admin/brand/list','PropertyAdmin\PropertyBrandController@index');
Route::post('root/admin/brand/view_brand','PropertyAdmin\PropertyBrandController@view');
Route::post('root/admin/brand/add_brand','PropertyAdmin\PropertyBrandController@create');
Route::get('/root/admin/brand/edit_brand/{id?}','PropertyAdmin\PropertyBrandController@edit');
Route::post('root/admin/brand/edit_brand_file','PropertyAdmin\PropertyBrandController@update');
Route::post('root/admin/brand/delete_brand','PropertyAdmin\PropertyBrandController@destroy');

Route::get('root/admin/mdm/index/{pid}','RootAdmin\PropertyController@generateCsvIndex');
Route::get('root/admin/mdm/property-csv/','RootAdmin\PropertyController@generatePropertyCsv');
Route::get('root/admin/mdm/property-unit-csv/{pid}','RootAdmin\PropertyController@generatePropertyUnitCsv');
Route::get('root/admin/mdm/customer-csv/{pid}','RootAdmin\PropertyController@generateCustomerCsv');
Route::get('root/admin/mdm/customer-property-csv/{pid}','RootAdmin\PropertyController@generateCustomerPropertyCsv');
Route::get('root/admin/mdm/tenant-csv/{pid}','RootAdmin\PropertyController@generateTenantCsv');
Route::get('root/admin/mdm/tenant-property-csv/{pid}','RootAdmin\PropertyController@generateTenantPropertyCsv');
Route::get('root/admin/mdm/customer-ref','RootAdmin\PropertyController@joinCustomerRef');

//---------------------------  SMART User Route --------------------------------------------//
Route::any('smart/admin/dashboard', 'Smart\DashboardController@unApprovalUserList');
Route::any('smart/admin/dashboard/new-messages', 'Smart\DashboardController@unResponseMessage');
Route::any('smart/admin/dashboard/new-messages/list', 'Smart\DashboardController@unResponseMessageList');
Route::any('smart/admin/dashboard/posts-parcels', 'Smart\DashboardController@overduePostsParcels');
Route::any('smart/admin/dashboard/posts-parcels/list', 'Smart\DashboardController@overduePostsParcelsList');
Route::any('smart/admin/dashboard/users', 'Smart\DashboardController@unregisterUsers');
Route::any('smart/admin/dashboard/users/list', 'Smart\DashboardController@unregisterUsersList');

//---------------------------  SMART Managament Route --------------------------------------------//
// Dash board
Route::any('management/admin/dashboard', 'Smart\DashboardController@index');

Route::any('management/admin/property/list', 'Management\PropertyController@index');
// Admin
Route::any('management/admin/admin-system/list', 'Management\AdminSystemController@adminList');
Route::post('management/admin/admin-system/add', 'Management\AdminSystemController@addAdmin');
Route::post('management/admin/admin-system/view', 'Management\AdminSystemController@viewAdmin');
Route::post('management/admin/admin-system/edit/get', 'Management\AdminSystemController@getAdmin');
Route::post('management/admin/admin-system/edit', 'Management\AdminSystemController@editAdmin');
Route::post('management/admin/admin-system/active', 'Management\AdminSystemController@setActive');
Route::post('management/admin/admin-system/delete', 'Management\AdminSystemController@deleteAdmin');

//Post
Route::any('management/admin/news-announcement', 'Management\PostController@feed');
Route::post('management/admin/news-announcement/gettext', 'Management\PostController@gettext');
Route::get('management/admin/news-announcement/add', 'Management\PostController@add');
Route::get('management/admin/news-announcement/edit/{id}', 'Management\PostController@edit');
Route::post('management/admin/news-announcement/save', 'Management\PostController@save');
Route::post('management/admin/news-announcement/remove', 'Management\PostController@delete');
Route::any('management/admin/news-announcement/view/{id}', 'Management\PostController@viewPost');
Route::any('management/admin/news-announcement/get-attachment', 'Management\PostController@getAttach');
Route::any('management/admin/news-announcement/export', 'Management\PostController@exportPosts');
Route::any('management/admin/news-announcement/send-approve', 'Management\PostController@sendApprovePost');
Route::any('management/admin/news-announcement/recall', 'Management\PostController@recallPost');
Route::any('management/admin/news-announcement/approve', 'Management\PostController@approvePost');
Route::any('management/admin/news-announcement/reject', 'Management\PostController@rejectPost');

// Cover Image
Route::any('management/admin/app-cover-image', 'Management\AppCoverImageController@coverImageList');
Route::any('management/admin/app-cover-image/save', 'Management\AppCoverImageController@saveCoverImage');
Route::post('management/admin/app-cover-image/remove', 'Management\AppCoverImageController@removeCoverImage');

//---------------------------  SMART Supervisor Route --------------------------------------------//
// Dash board
Route::any('smart-supervisor/admin/dashboard', 'Smart\DashboardController@index');

Route::any('smart-supervisor/admin/property/list', 'SmartSupervisor\PropertyController@index');
// Admin
Route::any('smart-supervisor/admin/admin-system/list', 'SmartSupervisor\AdminSystemController@adminList');
Route::post('smart-supervisor/admin/admin-system/add', 'SmartSupervisor\AdminSystemController@addAdmin');
Route::post('smart-supervisor/admin/admin-system/view', 'SmartSupervisor\AdminSystemController@viewAdmin');
Route::post('smart-supervisor/admin/admin-system/edit/get', 'SmartSupervisor\AdminSystemController@getAdmin');
Route::post('smart-supervisor/admin/admin-system/edit', 'SmartSupervisor\AdminSystemController@editAdmin');
Route::post('smart-supervisor/admin/admin-system/active', 'SmartSupervisor\AdminSystemController@setActive');
Route::post('smart-supervisor/admin/admin-system/delete', 'SmartSupervisor\AdminSystemController@deleteAdmin');
//Post
Route::any('smart-supervisor/admin/news-announcement', 'SmartSupervisor\PostController@feed');
Route::post('smart-supervisor/admin/news-announcement/gettext', 'SmartSupervisor\PostController@gettext');
Route::get('smart-supervisor/admin/news-announcement/add', 'SmartSupervisor\PostController@add');
Route::get('smart-supervisor/admin/news-announcement/edit/{id}', 'SmartSupervisor\PostController@edit');
Route::post('smart-supervisor/admin/news-announcement/save', 'SmartSupervisor\PostController@save');
Route::post('smart-supervisor/admin/news-announcement/remove', 'SmartSupervisor\PostController@delete');
Route::any('smart-supervisor/admin/news-announcement/view/{id}', 'SmartSupervisor\PostController@viewPost');
Route::any('smart-supervisor/admin/news-announcement/get-attachment', 'SmartSupervisor\PostController@getAttach');
Route::any('smart-supervisor/admin/news-announcement/export', 'SmartSupervisor\PostController@exportPosts');
Route::any('smart-supervisor/admin/news-announcement/send-approve', 'SmartSupervisor\PostController@sendApprovePost');
Route::any('smart-supervisor/admin/news-announcement/recall', 'SmartSupervisor\PostController@recallPost');
Route::any('smart-supervisor/admin/news-announcement/approve', 'SmartSupervisor\PostController@approvePost');
Route::any('smart-supervisor/admin/news-announcement/reject', 'SmartSupervisor\PostController@rejectPost');

//---------------------------  SMART Admin Route --------------------------------------------//
//Dashboard
Route::any('admin/dashboard', 'PropertyAdmin\DashboardController@index');
// Property settings
Route::any('admin/property/about', 'PropertyAdmin\AboutPropertyController@about');
Route::any('admin/property/settings', 'PropertyAdmin\AboutPropertyController@settings');
//Message
Route::any('admin/messages', 'PropertyAdmin\MessageController@index');
Route::get('admin/messages/view/{id}', 'PropertyAdmin\MessageController@view');
Route::post('admin/message/send', 'PropertyAdmin\MessageController@sendMessage');
Route::post('admin/message/receive', 'PropertyAdmin\MessageController@renderMessageClient');
Route::get('admin/message/send/{uid}', 'PropertyAdmin\MessageController@sendNewMessage');
Route::post('admin/messages/page/old', 'PropertyAdmin\MessageController@oldMessagePage');
Route::post('admin/messages/page/new', 'PropertyAdmin\MessageController@messagePage');
Route::post('admin/message/user/send', 'PropertyAdmin\MessageController@sendUserMessage');
Route::post('admin/message/member/get', 'PropertyAdmin\MessageController@memberlistPage');

// Member Route
Route::any('admin/property/members', 'PropertyAdmin\PropertyMemberController@memberlist');
Route::post('admin/property/members/add', 'PropertyAdmin\PropertyMemberController@addMember');
Route::any('admin/property/members/cheif', 'PropertyAdmin\PropertyMemberController@setChief');
Route::any('admin/property/members/active', 'PropertyAdmin\PropertyMemberController@setActive');
Route::any('admin/property/members/get', 'PropertyAdmin\PropertyMemberController@getMember');
Route::post('admin/property/members/edit', 'PropertyAdmin\PropertyMemberController@editMember');
Route::post('admin/property/members/page', 'PropertyAdmin\PropertyMemberController@memberlistPage');
Route::any('admin/property/new-members', 'PropertyAdmin\PropertyMemberController@newMembers');
Route::post('admin/property/members/export', 'PropertyAdmin\PropertyMemberController@exportMembers');
Route::any('admin/property/new-members/export', 'PropertyAdmin\PropertyMemberController@exportNewMembers');
Route::post('admin/property/members/reject', 'PropertyAdmin\PropertyMemberController@rejectNewMember');
Route::post('admin/property/members/approve', 'PropertyAdmin\PropertyMemberController@approveNewMember');

// Units Route
Route::any('admin/property/units', 'PropertyAdmin\PropertyUnitController@unitList');
Route::post('admin/property/units/getUnit', 'PropertyAdmin\PropertyUnitController@getUnit');
Route::post('admin/property/units/edit/form', 'PropertyAdmin\PropertyUnitController@editForm');
Route::post('admin/property/units/edit-tenant/form', 'PropertyAdmin\PropertyUnitController@editTenantForm');
Route::post('admin/property/units/add', 'PropertyAdmin\PropertyUnitController@add');
Route::post('admin/property/units/edit', 'PropertyAdmin\PropertyUnitController@edit');
Route::post('admin/property/units/edit-tenant', 'PropertyAdmin\PropertyUnitController@editTenant');
Route::post('admin/property/units/clear', 'PropertyAdmin\PropertyUnitController@clearUnit');
Route::post('admin/property/unit/check-balance', 'PropertyAdmin\PropertyUnitController@checkBalance');
Route::post('admin/property/units/delete-tenant', 'PropertyAdmin\PropertyUnitController@deleteTenant');
Route::any('admin/property/units-invite', 'PropertyAdmin\PropertyUnitController@inviteCodeList');
Route::post('admin/property/units/export', 'PropertyAdmin\PropertyUnitController@exportPropertyUnit');

// Customer
Route::any('admin/property/customers', 'PropertyAdmin\PropertyCustomerController@customerList');
Route::any('admin/property/customers/page', 'PropertyAdmin\PropertyCustomerController@customerListPage');
Route::any('admin/property/customer/get', 'PropertyAdmin\PropertyCustomerController@getCustomer');
Route::post('admin/property/customer/edit', 'PropertyAdmin\PropertyCustomerController@editCustomer');
Route::post('admin/property/customer/save', 'PropertyAdmin\PropertyCustomerController@saveCustomer');
Route::post('admin/property/customer/export', 'PropertyAdmin\PropertyCustomerController@exportCustomer');
// Post and Parcel
Route::any('admin/parcel-tracking', 'PropertyAdmin\PostParcelController@postParcelList');
Route::post('admin/parcel-tracking/add', 'PropertyAdmin\PostParcelController@add');
Route::get('admin/parcel-tracking/delete/{id}', 'PropertyAdmin\PostParcelController@delete');
Route::post('admin/parcel-tracking/delivered', 'PropertyAdmin\PostParcelController@delivered');
Route::post('admin/parcel-tracking/print/new', 'PropertyAdmin\PostParcelController@printNewList');
Route::post('admin/parcel-tracking/print/label', 'PropertyAdmin\PostParcelController@printLabel');
Route::get('admin/parcel-tracking/print/not-received', 'PropertyAdmin\PostParcelController@printNotReceived');
Route::post('admin/post-and-parcel/view','PropertyAdmin\PostParcelController@viewPostParcel');
Route::post('admin/parcel-tracking/cancel','PropertyAdmin\PostParcelController@cancelPostParcel');
Route::post('admin/parcel-tracking/get-resident','PropertyAdmin\PostParcelController@getResident');
// News & Announcement
Route::any('admin/news-announcement', 'PropertyAdmin\PostController@feed');
Route::post('admin/news-announcement/gettext', 'PropertyAdmin\PostController@gettext');
Route::get('admin/news-announcement/add', 'PropertyAdmin\PostController@add');
Route::get('admin/news-announcement/edit/{id}', 'PropertyAdmin\PostController@edit');
Route::post('admin/news-announcement/save', 'PropertyAdmin\PostController@save');
Route::post('admin/news-announcement/remove', 'PropertyAdmin\PostController@delete');
Route::any('admin/news-announcement/view/{id}', 'PropertyAdmin\PostController@viewPost');
Route::any('admin/news-announcement/get-attachment', 'PropertyAdmin\PostController@getAttach');
Route::any('admin/news-announcement/export', 'PropertyAdmin\PostController@exportPosts');
Route::any('admin/news-announcement/send-approve', 'PropertyAdmin\PostController@sendApprovePost');
Route::any('admin/news-announcement/recall', 'PropertyAdmin\PostController@recallPost');
Route::any('admin/news-announcement/tracking-page', 'PostController@trackingPage');
Route::any('admin/news-announcement/print/{id}', 'PostController@printPost');

// Admin Complain
Route::any('admin/maintainance', 'PropertyAdmin\ComplainController@complain');
Route::any('admin/maintainance/juristic', 'PropertyAdmin\ComplainController@juristicComplain');
Route::get('admin/maintainance/view/{id}', 'PropertyAdmin\ComplainController@view');
Route::post('admin/maintainance/status', 'PropertyAdmin\ComplainController@changeStatus');
Route::post('admin/maintainance/user/add', 'PropertyAdmin\ComplainController@addForUser');
Route::post('admin/maintainance/juristic/add', 'PropertyAdmin\ComplainController@addForJuristic');
Route::any('admin/maintainance/juristic/view', 'PropertyAdmin\ComplainController@JcView');
Route::any('admin/maintainance/report', 'PropertyAdmin\ComplainController@complainReport');
Route::post('admin/maintainance/report/month', 'PropertyAdmin\ComplainController@complainReportMonth');
Route::any('admin/maintainance/report/year', 'PropertyAdmin\ComplainController@complainReportYear');
Route::any('admin/maintainance/export', 'PropertyAdmin\ComplainController@exportReport');
Route::post('admin/maintainance/update-detail', 'PropertyAdmin\ComplainController@updateComplainDetail');

// Vote
Route::any('admin/survey', 'PropertyAdmin\VoteController@index');
Route::get('admin/survey/add', 'PropertyAdmin\VoteController@add');
Route::get('admin/survey/view/{id}', 'PropertyAdmin\VoteController@view');
Route::post('admin/survey/delete', 'PropertyAdmin\VoteController@delete');
Route::any('admin/survey/get-attachment', 'PropertyAdmin\VoteController@getAttach');
Route::get('admin/survey/edit/{id}', 'PropertyAdmin\VoteController@edit');
Route::post('admin/survey/save', 'PropertyAdmin\VoteController@save');
Route::any('admin/survey/export', 'PropertyAdmin\VoteController@exportVotes');


//---------------------------  Technician Route --------------------------------------------//
Route::any('technician/admin/property/list', 'Technician\PropertyController@index');
// Parcel
Route::any('technician/admin/parcel-tracking', 'Technician\PostParcelController@postParcelList');
Route::post('technician/admin/post-and-parcel/view','Technician\PostParcelController@viewPostParcel');
//Message
Route::any('technician/admin/messages', 'Technician\MessageController@index');
Route::get('technician/admin/messages/view/{id}', 'Technician\MessageController@view');
Route::post('technician/admin/messages/page', 'Technician\MessageController@messagePage');

//---------------------------  Outsource Route --------------------------------------------//
Route::any('outsource/admin/property/list', 'Outsource\PropertyController@index');
//Message
Route::any('outsource/admin/messages', 'Outsource\MessageController@index');
Route::get('outsource/admin/messages/view/{id}', 'Outsource\MessageController@view');
Route::post('outsource/admin/messages/page', 'Outsource\MessageController@messagePage');
//---------------------------  SMART HQ Admin Route --------------------------------------------//
//Dashboard
Route::any('smart-hq-admin/admin/dashboard', 'Smart\DashboardController@index');
// Property
Route::any('smart-hq-admin/admin/property/list', 'SmartHQAdmin\PropertyController@index');
// News & Announcement
Route::any('smart-hq-admin/admin/news-announcement', 'SmartHQAdmin\PostController@feed');
Route::post('smart-hq-admin/admin/news-announcement/gettext', 'SmartHQAdmin\PostController@gettext');
Route::get('smart-hq-admin/admin/news-announcement/add', 'SmartHQAdmin\PostController@add');
Route::get('smart-hq-admin/admin/news-announcement/edit/{id}', 'SmartHQAdmin\PostController@edit');
Route::post('smart-hq-admin/admin/news-announcement/save', 'SmartHQAdmin\PostController@save');
Route::post('smart-hq-admin/admin/news-announcement/remove', 'SmartHQAdmin\PostController@delete');
Route::any('smart-hq-admin/admin/news-announcement/view/{id}', 'SmartHQAdmin\PostController@viewPost');
Route::any('smart-hq-admin/admin/news-announcement/get-attachment', 'SmartHQAdmin\PostController@getAttach');
Route::any('smart-hq-admin/admin/news-announcement/export', 'SmartHQAdmin\PostController@exportPosts');
Route::any('smart-hq-admin/admin/news-announcement/send-approve', 'SmartHQAdmin\PostController@sendApprovePost');
Route::any('smart-hq-admin/admin/news-announcement/recall', 'SmartHQAdmin\PostController@recallPost');

// Admin
Route::any('smart-hq-admin/admin/admin-system/list', 'SmartHQAdmin\AdminSystemController@adminList');
Route::post('smart-hq-admin/admin/admin-system/add', 'SmartHQAdmin\AdminSystemController@addAdmin');
Route::post('smart-hq-admin/admin/admin-system/view', 'SmartHQAdmin\AdminSystemController@viewAdmin');
Route::post('smart-hq-admin/admin/admin-system/edit/get', 'SmartHQAdmin\AdminSystemController@getAdmin');
Route::post('smart-hq-admin/admin/admin-system/edit', 'SmartHQAdmin\AdminSystemController@editAdmin');
Route::post('smart-hq-admin/admin/admin-system/active', 'SmartHQAdmin\AdminSystemController@setActive');
Route::post('smart-hq-admin/admin/admin-system/delete', 'SmartHQAdmin\AdminSystemController@deleteAdmin');
//---------------------------  Call center Route --------------------------------------------//
Route::any('call-center/admin/property/list', 'CallCenter\PropertyController@index');
// Property unit
Route::any('call-center/admin/property/units', 'CallCenter\PropertyUnitController@unitList');
Route::post('call-center/admin/property/units/getUnit', 'CallCenter\PropertyUnitController@getUnit');
// Member
Route::any('call-center/admin/property/members', 'CallCenter\PropertyMemberController@memberlist');
Route::any('call-center/admin/property/members/get', 'CallCenter\PropertyMemberController@getMember');
Route::post('call-center/admin/property/members/edit', 'CallCenter\PropertyMemberController@editMember');
Route::post('call-center/admin/property/members/page', 'CallCenter\PropertyMemberController@memberlistPage');

///// For sale force API ///////
Route::get('api/property/list','apAPIController@apWorldProperty');
Route::get('api/property/unit/list','apAPIController@apWorldPropertyUnit');
Route::get('api/customer/list','apAPIController@apWorldCustomer');
Route::get('api/customer/property/list','apAPIController@apWorldCustomerProperty');
Route::get('api/tenant/list','apAPIController@apWorldTenant');
Route::get('api/tenant/property/list','apAPIController@apWorldTenantProperty');

